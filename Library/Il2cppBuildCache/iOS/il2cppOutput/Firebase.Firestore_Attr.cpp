﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4;
// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7;
// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// Firebase.Firestore.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FirestoreExceptionDelegate_t23DD44F00FB944A72F9A658AD89B7D184CC00DE7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ListenerDelegate_t05DB81640E0AEF7D4113B5B3864549481883728B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ListenerDelegate_tFF2B00998B1E8B648A95E1D71659A94533A149FE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LoadBundleTaskProgressDelegate_t60ACA182A262832D37D1016C9AC51EC2A7D56EB5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SWIGStringDelegate_t6C6497EA2E6335D59B30F590C25DCBE05D13FE19_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SWIG_CompletionDelegate_tB96830FCFDA0AA17F1448CE59E8FF6F9C8BA915B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SWIG_CompletionDelegate_tE34F0B1165EE96A2CD3FB6EC0CD5C22FDC39F5E3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SWIG_CompletionDelegate_tE3C5845DE9C296FE13B9D4BA56163E5F96591A70_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SnapshotsInSyncDelegate_t15AB3AA05B369E46B21BD4D6E91E6C8111595436_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCompanyAttribute::m_company
	String_t* ___m_company_0;

public:
	inline static int32_t get_offset_of_m_company_0() { return static_cast<int32_t>(offsetof(AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4, ___m_company_0)); }
	inline String_t* get_m_company_0() const { return ___m_company_0; }
	inline String_t** get_address_of_m_company_0() { return &___m_company_0; }
	inline void set_m_company_0(String_t* value)
	{
		___m_company_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_company_0), (void*)value);
	}
};


// System.Reflection.AssemblyConfigurationAttribute
struct AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyConfigurationAttribute::m_configuration
	String_t* ___m_configuration_0;

public:
	inline static int32_t get_offset_of_m_configuration_0() { return static_cast<int32_t>(offsetof(AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C, ___m_configuration_0)); }
	inline String_t* get_m_configuration_0() const { return ___m_configuration_0; }
	inline String_t** get_address_of_m_configuration_0() { return &___m_configuration_0; }
	inline void set_m_configuration_0(String_t* value)
	{
		___m_configuration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_configuration_0), (void*)value);
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyDescriptionAttribute::m_description
	String_t* ___m_description_0;

public:
	inline static int32_t get_offset_of_m_description_0() { return static_cast<int32_t>(offsetof(AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3, ___m_description_0)); }
	inline String_t* get_m_description_0() const { return ___m_description_0; }
	inline String_t** get_address_of_m_description_0() { return &___m_description_0; }
	inline void set_m_description_0(String_t* value)
	{
		___m_description_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_description_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTitleAttribute::m_title
	String_t* ___m_title_0;

public:
	inline static int32_t get_offset_of_m_title_0() { return static_cast<int32_t>(offsetof(AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7, ___m_title_0)); }
	inline String_t* get_m_title_0() const { return ___m_title_0; }
	inline String_t** get_address_of_m_title_0() { return &___m_title_0; }
	inline void set_m_title_0(String_t* value)
	{
		___m_title_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_title_0), (void*)value);
	}
};


// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyTrademarkAttribute::m_trademark
	String_t* ___m_trademark_0;

public:
	inline static int32_t get_offset_of_m_trademark_0() { return static_cast<int32_t>(offsetof(AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2, ___m_trademark_0)); }
	inline String_t* get_m_trademark_0() const { return ___m_trademark_0; }
	inline String_t** get_address_of_m_trademark_0() { return &___m_trademark_0; }
	inline void set_m_trademark_0(String_t* value)
	{
		___m_trademark_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_trademark_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// Firebase.Firestore.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02 (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * __this, String_t* ___trademark0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0 (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * __this, String_t* ___company0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyConfigurationAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757 (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * __this, String_t* ___configuration0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25 (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * __this, String_t* ___description0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * __this, String_t* ___title0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void Firebase.Firestore.MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * __this, Type_t * ___t0, const RuntimeMethod* method);
// System.Void System.ThreadStaticAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
static void Firebase_Firestore_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[0];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[2];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x72\x65\x62\x61\x73\x65\x2E\x46\x69\x72\x65\x73\x74\x6F\x72\x65\x2E\x54\x65\x73\x74"), NULL);
	}
	{
		AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 * tmp = (AssemblyTrademarkAttribute_t0602679435F8EBECC5DDB55CFE3A7A4A4CA2B5E2 *)cache->attributes[3];
		AssemblyTrademarkAttribute__ctor_m6FBD5AAE48F00120043AD8BECF2586896CFB6C02(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[4];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\x32\x30\x31\x36\x20\x47\x6F\x6F\x67\x6C\x65\x20\x49\x6E\x63\x2E\x20\x41\x6C\x6C\x20\x52\x69\x67\x68\x74\x73\x20\x52\x65\x73\x65\x72\x76\x65\x64\x2E"), NULL);
	}
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[5];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x72\x65\x62\x61\x73\x65"), NULL);
	}
	{
		AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 * tmp = (AssemblyCompanyAttribute_t642AAB097D7DEAAB623BEBE4664327E9B01D1DE4 *)cache->attributes[6];
		AssemblyCompanyAttribute__ctor_m435C9FEC405646617645636E67860598A0C46FF0(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x6F\x67\x6C\x65\x20\x49\x6E\x63\x2E"), NULL);
	}
	{
		AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C * tmp = (AssemblyConfigurationAttribute_t071B324A83314FBA14A43F37BE7206C420218B7C *)cache->attributes[7];
		AssemblyConfigurationAttribute__ctor_m6EE76F5A155EDEA71967A32F78D777038ADD0757(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 * tmp = (AssemblyDescriptionAttribute_tF4460CCB289F6E2F71841792BBC7E6907DF612B3 *)cache->attributes[8];
		AssemblyDescriptionAttribute__ctor_m3A0BD500FF352A67235FBA499FBA58EFF15B1F25(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 * tmp = (AssemblyTitleAttribute_tABB894D0792C7F307694CC796C8AE5D6A20382E7 *)cache->attributes[9];
		AssemblyTitleAttribute__ctor_mE239F206B3B369C48AE1F3B4211688778FE99E8D(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x72\x65\x62\x61\x73\x65\x2E\x46\x69\x72\x65\x73\x74\x6F\x72\x65"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[10];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingApplicationException_mB20451B96AA024E56B5325467C34A15CEC959E87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArithmeticException_m4AAC766ED9C73575CC085C73FCC661F43C2D7F65(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingDivideByZeroException_m263C630ED5A41469D5E31BC020027F428E59174E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20647810331F9CC799CDEAEDD239B3730BEE3F77(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingInvalidCastException_mA61F666E4AED681248244DB6A9047C62F63BC1B8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingInvalidOperationException_mC6DFD5948DE2DE41FA017176552480B0C6C26939(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingIOException_m6F3EB38289D6864D5789210984D060820C7AD141(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingNullReferenceException_m0BAA3A7F9DA1568808372316443B60B5CF600A6C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingOutOfMemoryException_mB31C5160DC4EC5BBDF0C365FC366F21454E4CD02(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingOverflowException_m5BED0B7D1D3082ADECC855BF1A19139CFDD1944B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingSystemException_mAEB312AC1D9580CC76BCF931FE3E88F6F4E65022(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionDelegate_tFE28D6CA9CCE3B6D4CACA24A85A1D1415FD9B975_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArgumentException_mE80C3E925C567EE9676B2CEA49B52FC659F6D536(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArgumentNullException_m91AB03C21D9FE1FED84FCA071C7559E9D6497345(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var), NULL);
	}
}
static void SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mE248E293DA7897F2D84BC85FF34092C7A17FB365(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ExceptionArgumentDelegate_t2CBE994B629D37B2B68BDE8F5CFA23D043769C5F_0_0_0_var), NULL);
	}
}
static void SWIGPendingException_t454637D968F9CEA916B55F1C41F27239BFF41384_CustomAttributesCacheGenerator_pendingException(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * tmp = (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A(tmp, NULL);
	}
}
static void SWIGStringHelper_t240ED862E7F77D0B0BAC434CC8BE5E658C478BBC_CustomAttributesCacheGenerator_SWIGStringHelper_CreateString_m96A00549D4DAAA8C0E10DCB21F5474A25613135A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIGStringDelegate_t6C6497EA2E6335D59B30F590C25DCBE05D13FE19_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(SWIGStringDelegate_t6C6497EA2E6335D59B30F590C25DCBE05D13FE19_0_0_0_var), NULL);
	}
}
static void FirestoreExceptionHelper_t7EE47EA26BFDAA29803A09009C6B2239F04BFD7E_CustomAttributesCacheGenerator_FirestoreExceptionHelper_SetPendingFirestoreException_m5AEA053CF616B39EF2A7841604DFAC64FF9022D0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FirestoreExceptionDelegate_t23DD44F00FB944A72F9A658AD89B7D184CC00DE7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(FirestoreExceptionDelegate_t23DD44F00FB944A72F9A658AD89B7D184CC00DE7_0_0_0_var), NULL);
	}
}
static void Future_FirestoreVoid_t546F9D19D9D89387712E0696FF6DB0D9564AD0BC_CustomAttributesCacheGenerator_Future_FirestoreVoid_SWIG_CompletionDispatcher_mA6530F04BC3CFFCC77F9F11F3F75C9A897B8A26A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIG_CompletionDelegate_tE34F0B1165EE96A2CD3FB6EC0CD5C22FDC39F5E3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(SWIG_CompletionDelegate_tE34F0B1165EE96A2CD3FB6EC0CD5C22FDC39F5E3_0_0_0_var), NULL);
	}
}
static void U3CGetTaskU3Ec__AnonStorey0_t3A53F170D34E34952D49C6AD5137F58927186371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Future_DocumentSnapshot_t0DA322F938832D3EC743B0FA9EBBDAB0A7453F2F_CustomAttributesCacheGenerator_Future_DocumentSnapshot_SWIG_CompletionDispatcher_mA4595454AABBA85D981ADC65299D23624CC60B25(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIG_CompletionDelegate_tB96830FCFDA0AA17F1448CE59E8FF6F9C8BA915B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(SWIG_CompletionDelegate_tB96830FCFDA0AA17F1448CE59E8FF6F9C8BA915B_0_0_0_var), NULL);
	}
}
static void U3CGetTaskU3Ec__AnonStorey0_tA4BEB3EA8614EF3459E0DFF5A8910EF4F6510C36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Future_QuerySnapshot_tD736EFCA14F38F1109706CAC198D94C5D6067975_CustomAttributesCacheGenerator_Future_QuerySnapshot_SWIG_CompletionDispatcher_mEA2F8DA0EA29D327EB2BE5CC9FF6F16DD7F9A38E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SWIG_CompletionDelegate_tE3C5845DE9C296FE13B9D4BA56163E5F96591A70_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(SWIG_CompletionDelegate_tE3C5845DE9C296FE13B9D4BA56163E5F96591A70_0_0_0_var), NULL);
	}
}
static void U3CGetTaskU3Ec__AnonStorey0_t850CFF279F3C96B3401E206F937B90DCC8778BE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMapResultU3Ec__AnonStorey0_2_t7B239EF587A364ABF3499A90A74850FD2596E460_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TransactionManager_t1B981BFCCDEF1532ACBD64A3754BE3AF2F113BB8_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializationContext_t20350A887CBE3EFA64D48AE7F1EBE57B63F53D6B_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void SerializationContext_t20350A887CBE3EFA64D48AE7F1EBE57B63F53D6B_CustomAttributesCacheGenerator_SerializationContext_get_Default_mC6C980DFCEC97497FC3A0A037D849AA9543A3EE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 64LL, NULL);
	}
}
static void DeserializationContext_t7EF01EEBFF8332FC7F93C011E2F178D239ACCA6F_CustomAttributesCacheGenerator_DeserializationContext_get_Firestore_m6F4B1BD282265D05849E6825E960F93DBE1DE089(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DeserializationContext_t7EF01EEBFF8332FC7F93C011E2F178D239ACCA6F_CustomAttributesCacheGenerator_DeserializationContext_get_DocumentReference_m778B513CA64B14CEF8D926620E348A6775F56157(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumerableConverter_tE1C061E23CA743B40B4E859D535F4FECD5EB54BF_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumerableConverter_tE1C061E23CA743B40B4E859D535F4FECD5EB54BF_CustomAttributesCacheGenerator_EnumerableConverter_U3CEnumerableConverterU3Em__0_m1F3010DB9D29A18C583EAF8C1C3D8F9ECE351204(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConverterCache_t74CFEAA0CF15FE0A5CBE9C02FDB228736FE1B9A4_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConverterCache_t74CFEAA0CF15FE0A5CBE9C02FDB228736FE1B9A4_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ConverterCache_t74CFEAA0CF15FE0A5CBE9C02FDB228736FE1B9A4_CustomAttributesCacheGenerator_ConverterCache_U3CTryGetStringDictionaryValueTypeU3Em__0_mC8256372F5430C0B4D9630ACB9C70E4E57ECB469(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AttributedTypeConverter_t10583B6CF6C4D0999D21FA41A7B562D1869A55A2_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AttributedTypeConverter_t10583B6CF6C4D0999D21FA41A7B562D1869A55A2_CustomAttributesCacheGenerator_AttributedTypeConverter_U3CCreateObjectCreatorU3Em__0_m7418D1F501B455A19E5ED2556383B2FB0861F9C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AttributedProperty_tD6B4948342BC73E42FD149CEE5A94410A39A318E_CustomAttributesCacheGenerator_AttributedProperty_get_CanRead_m5A6FD022ADDF423946F4B2D239972ED91A7735D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AttributedProperty_tD6B4948342BC73E42FD149CEE5A94410A39A318E_CustomAttributesCacheGenerator_AttributedProperty_get_CanWrite_mEFEAA6057CE55195C16093D74B0142FDC24365F7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AttributedProperty_tD6B4948342BC73E42FD149CEE5A94410A39A318E_CustomAttributesCacheGenerator_AttributedProperty_get_IsNullableValue_m7CAAD755E5D02AEA6DDD72400801878ADE11EE23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttributedTypeConverterU3Ec__AnonStorey0_t35067A3F26B36586CA80D626D45AC16BB012A19C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateObjectCreatorU3Ec__AnonStorey1_tAFBC05310CBF01CB288BFB62DC9E4D8D1DC7B02A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateObjectCreatorU3Ec__AnonStorey2_t235D98995C9DC57102E83663C0399B4888060A01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_SetOptions_U3CToStringU3Em__1_m2FBC1E0D85E0717DA39709B4CFACBFF10ACA3498(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_SetOptions_U3CGetHashCodeU3Em__2_m8986765E579506460C7E2B021B3813547D5DD81B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ServerTimestampBehaviorConverter_t5D3853C646DF1BB6C5B7DCC379E5897078620BFB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ServerTimestampBehaviorConverter_t5D3853C646DF1BB6C5B7DCC379E5897078620BFB_CustomAttributesCacheGenerator_ServerTimestampBehaviorConverter_ConvertToProxy_m265228CB4797736EE3C6E0C6B70B5A2710FCD724(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ServerTimestampAttribute_t434A38EAD0AD34C54CD0C78B6FE1C55BFB086B23_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 128LL, NULL);
	}
}
static void QuerySnapshot_t6749A2739B7AEF7EB2EBC4C24950037AB9FF70AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void Query_t0CBF0AA35571026E7DE72A8CC154CBF97E1C273B_CustomAttributesCacheGenerator_Query_QuerySnapshotsHandler_mA299680852F7C4311215EC469EE2673C4DE66288(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListenerDelegate_tFF2B00998B1E8B648A95E1D71659A94533A149FE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ListenerDelegate_tFF2B00998B1E8B648A95E1D71659A94533A149FE_0_0_0_var), NULL);
	}
}
static void Query_t0CBF0AA35571026E7DE72A8CC154CBF97E1C273B_CustomAttributesCacheGenerator_Query_U3CGetSnapshotAsyncU3Em__0_m6B59C78908E622F80E54D0F24DC94842F12B66E7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CDocumentsLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CTotalDocumentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CBytesLoadedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CTotalBytesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CStateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_DocumentsLoaded_m8839EAD136AE2BEA4B312E87D986945CE7AEE4F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_DocumentsLoaded_m932B2143A953D9F29A7636FEDEEE35801BE64F42(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_TotalDocuments_m7BFAD5081680678A137B2DFFFE66406CD3A21855(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_TotalDocuments_m1269F3735C25EDD0B16E7B78675E7993F9054ABB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_BytesLoaded_mCB5B3F70068A7B228320AD570E6800383006FB85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_BytesLoaded_m7419D259E350CDD2A497ADD3138B3CAB7862F0D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_TotalBytes_m8D3CACABF7968242099415E1D9171ACB1453EAF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_TotalBytes_m1172CA8BF8AFC451A45A483ADEEB07D87CCC95BF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_State_m12341ACF7E9FC596C50C6FCA8EEA2B11BAE96C47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_State_mE06615EF5BCDC47C6EC7EAF034909C6A6EAC75C0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 128LL, NULL);
	}
}
static void FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_U3CConverterTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_FirestorePropertyAttribute_get_Name_mCCD84B1BF0D33D421D51580048BD8111597C6C01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_FirestorePropertyAttribute_get_ConverterType_mC42B9FEF3FE61AC4C783D7B7326770F1D7DC8236(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestoreException_t8FFB8FB9EC66DEE85083CD7E1A71A44521CA4648_CustomAttributesCacheGenerator_U3CErrorCodeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void FirestoreException_t8FFB8FB9EC66DEE85083CD7E1A71A44521CA4648_CustomAttributesCacheGenerator_FirestoreException_set_ErrorCode_m1A486DA1C3583D6996FD29A4801D8ECE619295B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestoreDocumentIdAttribute_t3910A8AF8EEB7875135BD7C68048A9D143E85A88_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 128LL, NULL);
	}
}
static void FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 5148LL, NULL);
	}
}
static void FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_U3CUnknownPropertyHandlingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_U3CConverterTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_FirestoreDataAttribute_get_UnknownPropertyHandling_mC057F50BA2B91C91314107F104E06CC3B3205574(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_FirestoreDataAttribute_get_ConverterType_m8341BA8D3E3A314B0160681DC91E76A839A5A024(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_HostU3Em__0_m3515008F3E929EC30CE397D9B9A923FD7015939A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_SslEnabledU3Em__1_mDA46D057D3E4114BC03CAD20DF57F3905ACB6364(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_PersistenceEnabledU3Em__2_m8D40BE69D183CF1903BB5F61FAA0ECE361649206(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_CacheSizeBytesU3Em__3_mCF439AD0B2082666AF1B4CB733B8E413BD906BF6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CAppU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_get_App_mC85D3E35B88B1BFD9BAD554799992C1890AD37A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_set_App_m940FE948474E4420E33F4C3887B739076A2F7D53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_SnapshotsInSyncHandler_mBA0CABA78B423DA08D795370117308D6DCD8083D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SnapshotsInSyncDelegate_t15AB3AA05B369E46B21BD4D6E91E6C8111595436_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(SnapshotsInSyncDelegate_t15AB3AA05B369E46B21BD4D6E91E6C8111595436_0_0_0_var), NULL);
	}
}
static void FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_LoadBundleTaskProgressHandler_m45EA4653105DBCCDBFF21220D79D28900BE3C36E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LoadBundleTaskProgressDelegate_t60ACA182A262832D37D1016C9AC51EC2A7D56EB5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(LoadBundleTaskProgressDelegate_t60ACA182A262832D37D1016C9AC51EC2A7D56EB5_0_0_0_var), NULL);
	}
}
static void U3CCollectionU3Ec__AnonStorey0_tBB240C50A5F62F6E512674C9F2BCE93C6B04A8D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSnapshotsInSyncHandlerU3Ec__AnonStorey5_t49FC4C80271D767C1407A49C6C34D8618EDCABD5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_U3CDocumentIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_FieldPath_get_EncodedPath_m2461510324F17EFEC455CDADF0690D8DF978A86D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_FieldPath_U3CFieldPathU3Em__0_m004A73324B8981818914311905F92219AF397231(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DocumentSnapshot_tB305C1842D43A27CC2A39D706DBFC4A88FDAEE01_CustomAttributesCacheGenerator_DocumentSnapshot_get_Reference_m00AD2FE77FD3CE9C0D8F44DAE0D0B25146602C91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DocumentSnapshot_tB305C1842D43A27CC2A39D706DBFC4A88FDAEE01_CustomAttributesCacheGenerator_DocumentSnapshot_get_Id_m38C69BA36A85F8FB57947515C28C719A96FEB24B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DocumentSnapshot_tB305C1842D43A27CC2A39D706DBFC4A88FDAEE01_CustomAttributesCacheGenerator_DocumentSnapshot_get_Exists_m40B28BFC8155D9C200E233CC4A081F0D0142665E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_get_Id_mEC4CA5D0801BF718D5A7C7254068E620A26BFEA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_get_Path_mBA04D43BA56B94451273BF532389076F73C6CBB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_DocumentSnapshotsHandler_m6A52B999C8587325EFA89784246DED8408BCFE04(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ListenerDelegate_t05DB81640E0AEF7D4113B5B3864549481883728B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 * tmp = (MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2 *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mD67CCA699BE119AD47BCF1D10211F624C8DB7ABE(tmp, il2cpp_codegen_type_get_object(ListenerDelegate_t05DB81640E0AEF7D4113B5B3864549481883728B_0_0_0_var), NULL);
	}
}
static void DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_U3CGetSnapshotAsyncU3Em__0_m53E10E71482EDCE5B169DAF3B176AC7870037483(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CollectionReference_t828E1C4A114818D2685F74F94080A0FF32E62390_CustomAttributesCacheGenerator_CollectionReference_get_Path_m44DC8DD2D692D947A696B94452F19304920AEE5C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Blob_t2CA1BBD4822EE3696AECB87CA9E2C3D3F48089BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void Blob_t2CA1BBD4822EE3696AECB87CA9E2C3D3F48089BB_CustomAttributesCacheGenerator_Blob_get_Length_m5176FBD5E23AABBCBAF49D12839E1DAB946547FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3EU7Be3691089U2D0518U2D465dU2Da0c9U2D4730f834a433U7D_t85686BDC693BBE86936CB06B3319AB4AA34227C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Firebase_Firestore_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Firebase_Firestore_AttributeGenerators[113] = 
{
	U3CGetTaskU3Ec__AnonStorey0_t3A53F170D34E34952D49C6AD5137F58927186371_CustomAttributesCacheGenerator,
	U3CGetTaskU3Ec__AnonStorey0_tA4BEB3EA8614EF3459E0DFF5A8910EF4F6510C36_CustomAttributesCacheGenerator,
	U3CGetTaskU3Ec__AnonStorey0_t850CFF279F3C96B3401E206F937B90DCC8778BE3_CustomAttributesCacheGenerator,
	U3CMapResultU3Ec__AnonStorey0_2_t7B239EF587A364ABF3499A90A74850FD2596E460_CustomAttributesCacheGenerator,
	MonoPInvokeCallbackAttribute_t697E4DC0FC19413F6E10FBA91C35CC30D1379DE2_CustomAttributesCacheGenerator,
	U3CAttributedTypeConverterU3Ec__AnonStorey0_t35067A3F26B36586CA80D626D45AC16BB012A19C_CustomAttributesCacheGenerator,
	U3CCreateObjectCreatorU3Ec__AnonStorey1_tAFBC05310CBF01CB288BFB62DC9E4D8D1DC7B02A_CustomAttributesCacheGenerator,
	U3CCreateObjectCreatorU3Ec__AnonStorey2_t235D98995C9DC57102E83663C0399B4888060A01_CustomAttributesCacheGenerator,
	ServerTimestampBehaviorConverter_t5D3853C646DF1BB6C5B7DCC379E5897078620BFB_CustomAttributesCacheGenerator,
	ServerTimestampAttribute_t434A38EAD0AD34C54CD0C78B6FE1C55BFB086B23_CustomAttributesCacheGenerator,
	QuerySnapshot_t6749A2739B7AEF7EB2EBC4C24950037AB9FF70AA_CustomAttributesCacheGenerator,
	FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator,
	FirestoreDocumentIdAttribute_t3910A8AF8EEB7875135BD7C68048A9D143E85A88_CustomAttributesCacheGenerator,
	FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator,
	U3CCollectionU3Ec__AnonStorey0_tBB240C50A5F62F6E512674C9F2BCE93C6B04A8D9_CustomAttributesCacheGenerator,
	U3CSnapshotsInSyncHandlerU3Ec__AnonStorey5_t49FC4C80271D767C1407A49C6C34D8618EDCABD5_CustomAttributesCacheGenerator,
	Blob_t2CA1BBD4822EE3696AECB87CA9E2C3D3F48089BB_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3EU7Be3691089U2D0518U2D465dU2Da0c9U2D4730f834a433U7D_t85686BDC693BBE86936CB06B3319AB4AA34227C6_CustomAttributesCacheGenerator,
	SWIGPendingException_t454637D968F9CEA916B55F1C41F27239BFF41384_CustomAttributesCacheGenerator_pendingException,
	TransactionManager_t1B981BFCCDEF1532ACBD64A3754BE3AF2F113BB8_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0,
	SerializationContext_t20350A887CBE3EFA64D48AE7F1EBE57B63F53D6B_CustomAttributesCacheGenerator_U3CDefaultU3Ek__BackingField,
	EnumerableConverter_tE1C061E23CA743B40B4E859D535F4FECD5EB54BF_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	ConverterCache_t74CFEAA0CF15FE0A5CBE9C02FDB228736FE1B9A4_CustomAttributesCacheGenerator_U3CU3Ef__mgU24cache0,
	ConverterCache_t74CFEAA0CF15FE0A5CBE9C02FDB228736FE1B9A4_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	AttributedTypeConverter_t10583B6CF6C4D0999D21FA41A7B562D1869A55A2_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CDocumentsLoadedU3Ek__BackingField,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CTotalDocumentsU3Ek__BackingField,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CBytesLoadedU3Ek__BackingField,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CTotalBytesU3Ek__BackingField,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_U3CStateU3Ek__BackingField,
	FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_U3CConverterTypeU3Ek__BackingField,
	FirestoreException_t8FFB8FB9EC66DEE85083CD7E1A71A44521CA4648_CustomAttributesCacheGenerator_U3CErrorCodeU3Ek__BackingField,
	FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_U3CUnknownPropertyHandlingU3Ek__BackingField,
	FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_U3CConverterTypeU3Ek__BackingField,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CAppU3Ek__BackingField,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_U3CDocumentIdU3Ek__BackingField,
	FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingApplicationException_mB20451B96AA024E56B5325467C34A15CEC959E87,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArithmeticException_m4AAC766ED9C73575CC085C73FCC661F43C2D7F65,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingDivideByZeroException_m263C630ED5A41469D5E31BC020027F428E59174E,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m20647810331F9CC799CDEAEDD239B3730BEE3F77,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingInvalidCastException_mA61F666E4AED681248244DB6A9047C62F63BC1B8,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingInvalidOperationException_mC6DFD5948DE2DE41FA017176552480B0C6C26939,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingIOException_m6F3EB38289D6864D5789210984D060820C7AD141,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingNullReferenceException_m0BAA3A7F9DA1568808372316443B60B5CF600A6C,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingOutOfMemoryException_mB31C5160DC4EC5BBDF0C365FC366F21454E4CD02,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingOverflowException_m5BED0B7D1D3082ADECC855BF1A19139CFDD1944B,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingSystemException_mAEB312AC1D9580CC76BCF931FE3E88F6F4E65022,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArgumentException_mE80C3E925C567EE9676B2CEA49B52FC659F6D536,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArgumentNullException_m91AB03C21D9FE1FED84FCA071C7559E9D6497345,
	SWIGExceptionHelper_t8CAA86CD64EE90FC08813DF4F940CC2CD81BE0E1_CustomAttributesCacheGenerator_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_mE248E293DA7897F2D84BC85FF34092C7A17FB365,
	SWIGStringHelper_t240ED862E7F77D0B0BAC434CC8BE5E658C478BBC_CustomAttributesCacheGenerator_SWIGStringHelper_CreateString_m96A00549D4DAAA8C0E10DCB21F5474A25613135A,
	FirestoreExceptionHelper_t7EE47EA26BFDAA29803A09009C6B2239F04BFD7E_CustomAttributesCacheGenerator_FirestoreExceptionHelper_SetPendingFirestoreException_m5AEA053CF616B39EF2A7841604DFAC64FF9022D0,
	Future_FirestoreVoid_t546F9D19D9D89387712E0696FF6DB0D9564AD0BC_CustomAttributesCacheGenerator_Future_FirestoreVoid_SWIG_CompletionDispatcher_mA6530F04BC3CFFCC77F9F11F3F75C9A897B8A26A,
	Future_DocumentSnapshot_t0DA322F938832D3EC743B0FA9EBBDAB0A7453F2F_CustomAttributesCacheGenerator_Future_DocumentSnapshot_SWIG_CompletionDispatcher_mA4595454AABBA85D981ADC65299D23624CC60B25,
	Future_QuerySnapshot_tD736EFCA14F38F1109706CAC198D94C5D6067975_CustomAttributesCacheGenerator_Future_QuerySnapshot_SWIG_CompletionDispatcher_mEA2F8DA0EA29D327EB2BE5CC9FF6F16DD7F9A38E,
	SerializationContext_t20350A887CBE3EFA64D48AE7F1EBE57B63F53D6B_CustomAttributesCacheGenerator_SerializationContext_get_Default_mC6C980DFCEC97497FC3A0A037D849AA9543A3EE9,
	DeserializationContext_t7EF01EEBFF8332FC7F93C011E2F178D239ACCA6F_CustomAttributesCacheGenerator_DeserializationContext_get_Firestore_m6F4B1BD282265D05849E6825E960F93DBE1DE089,
	DeserializationContext_t7EF01EEBFF8332FC7F93C011E2F178D239ACCA6F_CustomAttributesCacheGenerator_DeserializationContext_get_DocumentReference_m778B513CA64B14CEF8D926620E348A6775F56157,
	EnumerableConverter_tE1C061E23CA743B40B4E859D535F4FECD5EB54BF_CustomAttributesCacheGenerator_EnumerableConverter_U3CEnumerableConverterU3Em__0_m1F3010DB9D29A18C583EAF8C1C3D8F9ECE351204,
	ConverterCache_t74CFEAA0CF15FE0A5CBE9C02FDB228736FE1B9A4_CustomAttributesCacheGenerator_ConverterCache_U3CTryGetStringDictionaryValueTypeU3Em__0_mC8256372F5430C0B4D9630ACB9C70E4E57ECB469,
	AttributedTypeConverter_t10583B6CF6C4D0999D21FA41A7B562D1869A55A2_CustomAttributesCacheGenerator_AttributedTypeConverter_U3CCreateObjectCreatorU3Em__0_m7418D1F501B455A19E5ED2556383B2FB0861F9C1,
	AttributedProperty_tD6B4948342BC73E42FD149CEE5A94410A39A318E_CustomAttributesCacheGenerator_AttributedProperty_get_CanRead_m5A6FD022ADDF423946F4B2D239972ED91A7735D8,
	AttributedProperty_tD6B4948342BC73E42FD149CEE5A94410A39A318E_CustomAttributesCacheGenerator_AttributedProperty_get_CanWrite_mEFEAA6057CE55195C16093D74B0142FDC24365F7,
	AttributedProperty_tD6B4948342BC73E42FD149CEE5A94410A39A318E_CustomAttributesCacheGenerator_AttributedProperty_get_IsNullableValue_m7CAAD755E5D02AEA6DDD72400801878ADE11EE23,
	SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_SetOptions_U3CToStringU3Em__1_m2FBC1E0D85E0717DA39709B4CFACBFF10ACA3498,
	SetOptions_tEFBAAC36D4A226ECD00A271F5D215170F2219687_CustomAttributesCacheGenerator_SetOptions_U3CGetHashCodeU3Em__2_m8986765E579506460C7E2B021B3813547D5DD81B,
	ServerTimestampBehaviorConverter_t5D3853C646DF1BB6C5B7DCC379E5897078620BFB_CustomAttributesCacheGenerator_ServerTimestampBehaviorConverter_ConvertToProxy_m265228CB4797736EE3C6E0C6B70B5A2710FCD724,
	Query_t0CBF0AA35571026E7DE72A8CC154CBF97E1C273B_CustomAttributesCacheGenerator_Query_QuerySnapshotsHandler_mA299680852F7C4311215EC469EE2673C4DE66288,
	Query_t0CBF0AA35571026E7DE72A8CC154CBF97E1C273B_CustomAttributesCacheGenerator_Query_U3CGetSnapshotAsyncU3Em__0_m6B59C78908E622F80E54D0F24DC94842F12B66E7,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_DocumentsLoaded_m8839EAD136AE2BEA4B312E87D986945CE7AEE4F3,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_DocumentsLoaded_m932B2143A953D9F29A7636FEDEEE35801BE64F42,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_TotalDocuments_m7BFAD5081680678A137B2DFFFE66406CD3A21855,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_TotalDocuments_m1269F3735C25EDD0B16E7B78675E7993F9054ABB,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_BytesLoaded_mCB5B3F70068A7B228320AD570E6800383006FB85,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_BytesLoaded_m7419D259E350CDD2A497ADD3138B3CAB7862F0D7,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_TotalBytes_m8D3CACABF7968242099415E1D9171ACB1453EAF2,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_TotalBytes_m1172CA8BF8AFC451A45A483ADEEB07D87CCC95BF,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_get_State_m12341ACF7E9FC596C50C6FCA8EEA2B11BAE96C47,
	LoadBundleTaskProgress_t833CB950900F612B25D40EEA10E6ABEDD5D2A7B8_CustomAttributesCacheGenerator_LoadBundleTaskProgress_set_State_mE06615EF5BCDC47C6EC7EAF034909C6A6EAC75C0,
	FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_FirestorePropertyAttribute_get_Name_mCCD84B1BF0D33D421D51580048BD8111597C6C01,
	FirestorePropertyAttribute_t1C5F34E744FE6F91721F8D8651934C77F65B1391_CustomAttributesCacheGenerator_FirestorePropertyAttribute_get_ConverterType_mC42B9FEF3FE61AC4C783D7B7326770F1D7DC8236,
	FirestoreException_t8FFB8FB9EC66DEE85083CD7E1A71A44521CA4648_CustomAttributesCacheGenerator_FirestoreException_set_ErrorCode_m1A486DA1C3583D6996FD29A4801D8ECE619295B5,
	FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_FirestoreDataAttribute_get_UnknownPropertyHandling_mC057F50BA2B91C91314107F104E06CC3B3205574,
	FirestoreDataAttribute_t5176613152C4330F782AEF1520BA76F5C0AEB973_CustomAttributesCacheGenerator_FirestoreDataAttribute_get_ConverterType_m8341BA8D3E3A314B0160681DC91E76A839A5A024,
	FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_HostU3Em__0_m3515008F3E929EC30CE397D9B9A923FD7015939A,
	FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_SslEnabledU3Em__1_mDA46D057D3E4114BC03CAD20DF57F3905ACB6364,
	FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_PersistenceEnabledU3Em__2_m8D40BE69D183CF1903BB5F61FAA0ECE361649206,
	FirebaseFirestoreSettings_tABE72EAC9A48AEBD73ED44AA32F1C308910AD015_CustomAttributesCacheGenerator_FirebaseFirestoreSettings_U3Cget_CacheSizeBytesU3Em__3_mCF439AD0B2082666AF1B4CB733B8E413BD906BF6,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_get_App_mC85D3E35B88B1BFD9BAD554799992C1890AD37A6,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_set_App_m940FE948474E4420E33F4C3887B739076A2F7D53,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_SnapshotsInSyncHandler_mBA0CABA78B423DA08D795370117308D6DCD8083D,
	FirebaseFirestore_t7BBE5EBF7AC75CE9AA35B9487E68920D68CFDEB0_CustomAttributesCacheGenerator_FirebaseFirestore_LoadBundleTaskProgressHandler_m45EA4653105DBCCDBFF21220D79D28900BE3C36E,
	FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_FieldPath_get_EncodedPath_m2461510324F17EFEC455CDADF0690D8DF978A86D,
	FieldPath_t633ADA2FEECECAB136CF9A73D0F2D159B36A785D_CustomAttributesCacheGenerator_FieldPath_U3CFieldPathU3Em__0_m004A73324B8981818914311905F92219AF397231,
	DocumentSnapshot_tB305C1842D43A27CC2A39D706DBFC4A88FDAEE01_CustomAttributesCacheGenerator_DocumentSnapshot_get_Reference_m00AD2FE77FD3CE9C0D8F44DAE0D0B25146602C91,
	DocumentSnapshot_tB305C1842D43A27CC2A39D706DBFC4A88FDAEE01_CustomAttributesCacheGenerator_DocumentSnapshot_get_Id_m38C69BA36A85F8FB57947515C28C719A96FEB24B,
	DocumentSnapshot_tB305C1842D43A27CC2A39D706DBFC4A88FDAEE01_CustomAttributesCacheGenerator_DocumentSnapshot_get_Exists_m40B28BFC8155D9C200E233CC4A081F0D0142665E,
	DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_get_Id_mEC4CA5D0801BF718D5A7C7254068E620A26BFEA9,
	DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_get_Path_mBA04D43BA56B94451273BF532389076F73C6CBB9,
	DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_DocumentSnapshotsHandler_m6A52B999C8587325EFA89784246DED8408BCFE04,
	DocumentReference_tC1E2EEE089B504CB389EEC2E3A4962FF85D7482A_CustomAttributesCacheGenerator_DocumentReference_U3CGetSnapshotAsyncU3Em__0_m53E10E71482EDCE5B169DAF3B176AC7870037483,
	CollectionReference_t828E1C4A114818D2685F74F94080A0FF32E62390_CustomAttributesCacheGenerator_CollectionReference_get_Path_m44DC8DD2D692D947A696B94452F19304920AEE5C,
	Blob_t2CA1BBD4822EE3696AECB87CA9E2C3D3F48089BB_CustomAttributesCacheGenerator_Blob_get_Length_m5176FBD5E23AABBCBAF49D12839E1DAB946547FD,
	Firebase_Firestore_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
