﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IFormatProvider
struct IFormatProvider_tF2AECC4B14F41D36718920D67F930CED940412DF;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ShuffleComponent_t6796DE67FD5D2B89865C06815789C5F33EB1D07A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float2_tCB7B81181978EDE17722C533A55E345D9A413274_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* uint2_t31B88562B6681D249453803230869FBE9ED565E7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E;
IL2CPP_EXTERN_C String_t* _stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A;
IL2CPP_EXTERN_C String_t* _stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A;
IL2CPP_EXTERN_C String_t* _stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6;
IL2CPP_EXTERN_C String_t* _stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033;
IL2CPP_EXTERN_C String_t* _stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58;
IL2CPP_EXTERN_C String_t* _stringLiteral579BEF9273F6F30843464D2DA589060200686F6A;
IL2CPP_EXTERN_C String_t* _stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB;
IL2CPP_EXTERN_C String_t* _stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA;
IL2CPP_EXTERN_C String_t* _stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945;
IL2CPP_EXTERN_C String_t* _stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E;
IL2CPP_EXTERN_C String_t* _stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E;
IL2CPP_EXTERN_C String_t* _stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2;
IL2CPP_EXTERN_C const RuntimeMethod* math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t916C2921FF960F72004923FC1240DBADFB79491C 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Unity.Mathematics.math
struct math_t4C8D40FC641C7FE575DD19ED65B962B3E792AF5C  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.float2/DebuggerProxy
struct DebuggerProxy_t3A175858992CDE4C80C7D76CCD40E6CE50B04766  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.float3/DebuggerProxy
struct DebuggerProxy_tF241B21D166E001A0FB777E403C2D7939743EB1B  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.float4/DebuggerProxy
struct DebuggerProxy_t496313EE9F0BCD4DFDE932CFB00FA8D1390778CF  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.int2/DebuggerProxy
struct DebuggerProxy_t472F3FCB6123ABBB13289D94E8648E38E38AF9A4  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.int3/DebuggerProxy
struct DebuggerProxy_t9AADC5329B709B684F917BE197AD9741C8D17275  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.int4/DebuggerProxy
struct DebuggerProxy_t7CE2166FE467013D04E1554BAAA20173D6E1CE75  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.uint2/DebuggerProxy
struct DebuggerProxy_t7E1A6191A0211F31BD9D1DD3746CC85CA9128090  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.uint3/DebuggerProxy
struct DebuggerProxy_t5454684BB1A7BCDF062804DC4C8C5BD77525881C  : public RuntimeObject
{
public:

public:
};


// Unity.Mathematics.uint4/DebuggerProxy
struct DebuggerProxy_t004DFEB9E37EC428BC11273322DF51330F83AE41  : public RuntimeObject
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Unity.Mathematics.float2
struct float2_tCB7B81181978EDE17722C533A55E345D9A413274 
{
public:
	// System.Single Unity.Mathematics.float2::x
	float ___x_0;
	// System.Single Unity.Mathematics.float2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float2_tCB7B81181978EDE17722C533A55E345D9A413274, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float2_tCB7B81181978EDE17722C533A55E345D9A413274, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct float2_tCB7B81181978EDE17722C533A55E345D9A413274_StaticFields
{
public:
	// Unity.Mathematics.float2 Unity.Mathematics.float2::zero
	float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___zero_2;

public:
	inline static int32_t get_offset_of_zero_2() { return static_cast<int32_t>(offsetof(float2_tCB7B81181978EDE17722C533A55E345D9A413274_StaticFields, ___zero_2)); }
	inline float2_tCB7B81181978EDE17722C533A55E345D9A413274  get_zero_2() const { return ___zero_2; }
	inline float2_tCB7B81181978EDE17722C533A55E345D9A413274 * get_address_of_zero_2() { return &___zero_2; }
	inline void set_zero_2(float2_tCB7B81181978EDE17722C533A55E345D9A413274  value)
	{
		___zero_2 = value;
	}
};


// Unity.Mathematics.float3
struct float3_t9500D105F273B3D86BD354142E891C48FFF9F71D 
{
public:
	// System.Single Unity.Mathematics.float3::x
	float ___x_0;
	// System.Single Unity.Mathematics.float3::y
	float ___y_1;
	// System.Single Unity.Mathematics.float3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

struct float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_StaticFields
{
public:
	// Unity.Mathematics.float3 Unity.Mathematics.float3::zero
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___zero_3;

public:
	inline static int32_t get_offset_of_zero_3() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_StaticFields, ___zero_3)); }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  get_zero_3() const { return ___zero_3; }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * get_address_of_zero_3() { return &___zero_3; }
	inline void set_zero_3(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  value)
	{
		___zero_3 = value;
	}
};


// Unity.Mathematics.float4
struct float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 
{
public:
	// System.Single Unity.Mathematics.float4::x
	float ___x_0;
	// System.Single Unity.Mathematics.float4::y
	float ___y_1;
	// System.Single Unity.Mathematics.float4::z
	float ___z_2;
	// System.Single Unity.Mathematics.float4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.float4::zero
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___zero_4;

public:
	inline static int32_t get_offset_of_zero_4() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields, ___zero_4)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_zero_4() const { return ___zero_4; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_zero_4() { return &___zero_4; }
	inline void set_zero_4(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___zero_4 = value;
	}
};


// Unity.Mathematics.int2
struct int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 
{
public:
	// System.Int32 Unity.Mathematics.int2::x
	int32_t ___x_0;
	// System.Int32 Unity.Mathematics.int2::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};


// Unity.Mathematics.int3
struct int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 
{
public:
	// System.Int32 Unity.Mathematics.int3::x
	int32_t ___x_0;
	// System.Int32 Unity.Mathematics.int3::y
	int32_t ___y_1;
	// System.Int32 Unity.Mathematics.int3::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};


// Unity.Mathematics.int4
struct int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 
{
public:
	// System.Int32 Unity.Mathematics.int4::x
	int32_t ___x_0;
	// System.Int32 Unity.Mathematics.int4::y
	int32_t ___y_1;
	// System.Int32 Unity.Mathematics.int4::z
	int32_t ___z_2;
	// System.Int32 Unity.Mathematics.int4::w
	int32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___w_3)); }
	inline int32_t get_w_3() const { return ___w_3; }
	inline int32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(int32_t value)
	{
		___w_3 = value;
	}
};

struct int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_StaticFields
{
public:
	// Unity.Mathematics.int4 Unity.Mathematics.int4::zero
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___zero_4;

public:
	inline static int32_t get_offset_of_zero_4() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_StaticFields, ___zero_4)); }
	inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  get_zero_4() const { return ___zero_4; }
	inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * get_address_of_zero_4() { return &___zero_4; }
	inline void set_zero_4(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  value)
	{
		___zero_4 = value;
	}
};


// Unity.Mathematics.uint2
struct uint2_t31B88562B6681D249453803230869FBE9ED565E7 
{
public:
	// System.UInt32 Unity.Mathematics.uint2::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint2::y
	uint32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(uint2_t31B88562B6681D249453803230869FBE9ED565E7, ___x_0)); }
	inline uint32_t get_x_0() const { return ___x_0; }
	inline uint32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(uint2_t31B88562B6681D249453803230869FBE9ED565E7, ___y_1)); }
	inline uint32_t get_y_1() const { return ___y_1; }
	inline uint32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint32_t value)
	{
		___y_1 = value;
	}
};


// Unity.Mathematics.uint3
struct uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A 
{
public:
	// System.UInt32 Unity.Mathematics.uint3::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint3::y
	uint32_t ___y_1;
	// System.UInt32 Unity.Mathematics.uint3::z
	uint32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A, ___x_0)); }
	inline uint32_t get_x_0() const { return ___x_0; }
	inline uint32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A, ___y_1)); }
	inline uint32_t get_y_1() const { return ___y_1; }
	inline uint32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A, ___z_2)); }
	inline uint32_t get_z_2() const { return ___z_2; }
	inline uint32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint32_t value)
	{
		___z_2 = value;
	}
};


// Unity.Mathematics.uint4
struct uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 
{
public:
	// System.UInt32 Unity.Mathematics.uint4::x
	uint32_t ___x_0;
	// System.UInt32 Unity.Mathematics.uint4::y
	uint32_t ___y_1;
	// System.UInt32 Unity.Mathematics.uint4::z
	uint32_t ___z_2;
	// System.UInt32 Unity.Mathematics.uint4::w
	uint32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635, ___x_0)); }
	inline uint32_t get_x_0() const { return ___x_0; }
	inline uint32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(uint32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635, ___y_1)); }
	inline uint32_t get_y_1() const { return ___y_1; }
	inline uint32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(uint32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635, ___z_2)); }
	inline uint32_t get_z_2() const { return ___z_2; }
	inline uint32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(uint32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635, ___w_3)); }
	inline uint32_t get_w_3() const { return ___w_3; }
	inline uint32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(uint32_t value)
	{
		___w_3 = value;
	}
};


// Unity.Mathematics.math/IntFloatUnion
struct IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 Unity.Mathematics.math/IntFloatUnion::intValue
			int32_t ___intValue_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___intValue_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single Unity.Mathematics.math/IntFloatUnion::floatValue
			float ___floatValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___floatValue_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_intValue_0() { return static_cast<int32_t>(offsetof(IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463, ___intValue_0)); }
	inline int32_t get_intValue_0() const { return ___intValue_0; }
	inline int32_t* get_address_of_intValue_0() { return &___intValue_0; }
	inline void set_intValue_0(int32_t value)
	{
		___intValue_0 = value;
	}

	inline static int32_t get_offset_of_floatValue_1() { return static_cast<int32_t>(offsetof(IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463, ___floatValue_1)); }
	inline float get_floatValue_1() const { return ___floatValue_1; }
	inline float* get_address_of_floatValue_1() { return &___floatValue_1; }
	inline void set_floatValue_1(float value)
	{
		___floatValue_1 = value;
	}
};


// Unity.Mathematics.math/LongDoubleUnion
struct LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 Unity.Mathematics.math/LongDoubleUnion::longValue
			int64_t ___longValue_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___longValue_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double Unity.Mathematics.math/LongDoubleUnion::doubleValue
			double ___doubleValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___doubleValue_1_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_longValue_0() { return static_cast<int32_t>(offsetof(LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B, ___longValue_0)); }
	inline int64_t get_longValue_0() const { return ___longValue_0; }
	inline int64_t* get_address_of_longValue_0() { return &___longValue_0; }
	inline void set_longValue_0(int64_t value)
	{
		___longValue_0 = value;
	}

	inline static int32_t get_offset_of_doubleValue_1() { return static_cast<int32_t>(offsetof(LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B, ___doubleValue_1)); }
	inline double get_doubleValue_1() const { return ___doubleValue_1; }
	inline double* get_address_of_doubleValue_1() { return &___doubleValue_1; }
	inline void set_doubleValue_1(double value)
	{
		___doubleValue_1 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// Unity.Mathematics.float3x3
struct float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B 
{
public:
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c0
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c0_0;
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c1
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c1_1;
	// Unity.Mathematics.float3 Unity.Mathematics.float3x3::c2
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c2_2;

public:
	inline static int32_t get_offset_of_c0_0() { return static_cast<int32_t>(offsetof(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B, ___c0_0)); }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  get_c0_0() const { return ___c0_0; }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * get_address_of_c0_0() { return &___c0_0; }
	inline void set_c0_0(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  value)
	{
		___c0_0 = value;
	}

	inline static int32_t get_offset_of_c1_1() { return static_cast<int32_t>(offsetof(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B, ___c1_1)); }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  get_c1_1() const { return ___c1_1; }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * get_address_of_c1_1() { return &___c1_1; }
	inline void set_c1_1(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  value)
	{
		___c1_1 = value;
	}

	inline static int32_t get_offset_of_c2_2() { return static_cast<int32_t>(offsetof(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B, ___c2_2)); }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  get_c2_2() const { return ___c2_2; }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * get_address_of_c2_2() { return &___c2_2; }
	inline void set_c2_2(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  value)
	{
		___c2_2 = value;
	}
};

struct float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_StaticFields
{
public:
	// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::identity
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___identity_3;

public:
	inline static int32_t get_offset_of_identity_3() { return static_cast<int32_t>(offsetof(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_StaticFields, ___identity_3)); }
	inline float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  get_identity_3() const { return ___identity_3; }
	inline float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * get_address_of_identity_3() { return &___identity_3; }
	inline void set_identity_3(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  value)
	{
		___identity_3 = value;
	}
};


// Unity.Mathematics.float4x4
struct float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c0
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c0_0;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c1
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c1_1;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c2
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c2_2;
	// Unity.Mathematics.float4 Unity.Mathematics.float4x4::c3
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c3_3;

public:
	inline static int32_t get_offset_of_c0_0() { return static_cast<int32_t>(offsetof(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7, ___c0_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_c0_0() const { return ___c0_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_c0_0() { return &___c0_0; }
	inline void set_c0_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___c0_0 = value;
	}

	inline static int32_t get_offset_of_c1_1() { return static_cast<int32_t>(offsetof(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7, ___c1_1)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_c1_1() const { return ___c1_1; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_c1_1() { return &___c1_1; }
	inline void set_c1_1(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___c1_1 = value;
	}

	inline static int32_t get_offset_of_c2_2() { return static_cast<int32_t>(offsetof(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7, ___c2_2)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_c2_2() const { return ___c2_2; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_c2_2() { return &___c2_2; }
	inline void set_c2_2(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___c2_2 = value;
	}

	inline static int32_t get_offset_of_c3_3() { return static_cast<int32_t>(offsetof(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7, ___c3_3)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_c3_3() const { return ___c3_3; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_c3_3() { return &___c3_3; }
	inline void set_c3_3(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___c3_3 = value;
	}
};

struct float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_StaticFields
{
public:
	// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::identity
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___identity_4;
	// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::zero
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___zero_5;

public:
	inline static int32_t get_offset_of_identity_4() { return static_cast<int32_t>(offsetof(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_StaticFields, ___identity_4)); }
	inline float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  get_identity_4() const { return ___identity_4; }
	inline float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * get_address_of_identity_4() { return &___identity_4; }
	inline void set_identity_4(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  value)
	{
		___identity_4 = value;
	}

	inline static int32_t get_offset_of_zero_5() { return static_cast<int32_t>(offsetof(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_StaticFields, ___zero_5)); }
	inline float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  get_zero_5() const { return ___zero_5; }
	inline float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * get_address_of_zero_5() { return &___zero_5; }
	inline void set_zero_5(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  value)
	{
		___zero_5 = value;
	}
};


// Unity.Mathematics.quaternion
struct quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F 
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.quaternion::value
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F, ___value_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_value_0() const { return ___value_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___value_0 = value;
	}
};

struct quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_StaticFields
{
public:
	// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::identity
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___identity_1;

public:
	inline static int32_t get_offset_of_identity_1() { return static_cast<int32_t>(offsetof(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_StaticFields, ___identity_1)); }
	inline quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  get_identity_1() const { return ___identity_1; }
	inline quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * get_address_of_identity_1() { return &___identity_1; }
	inline void set_identity_1(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  value)
	{
		___identity_1 = value;
	}
};


// Unity.Mathematics.math/ShuffleComponent
struct ShuffleComponent_t6796DE67FD5D2B89865C06815789C5F33EB1D07A 
{
public:
	// System.Byte Unity.Mathematics.math/ShuffleComponent::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShuffleComponent_t6796DE67FD5D2B89865C06815789C5F33EB1D07A, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void Unity.Mathematics.float2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float2::.ctor(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___v0, const RuntimeMethod* method);
// Unity.Mathematics.float2 Unity.Mathematics.float2::get_xy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method);
// System.Single Unity.Mathematics.float2::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float2::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float2::Equals(Unity.Mathematics.float2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float2::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float2::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.String Unity.Mathematics.float2::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method);
// System.String System.Single::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B (float* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.String Unity.Mathematics.float2::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3::.ctor(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___v0, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::get_xyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::get_yzx()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method);
// Unity.Mathematics.float2 Unity.Mathematics.float3::get_xy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method);
// Unity.Mathematics.float2 Unity.Mathematics.float3::get_yz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method);
// System.Single Unity.Mathematics.float3::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float3::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, RuntimeObject * ___arg23, const RuntimeMethod* method);
// System.String Unity.Mathematics.float3::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.float3,Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c00, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c11, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c22, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3x3::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float ___m000, float ___m011, float ___m022, float ___m103, float ___m114, float ___m125, float ___m206, float ___m217, float ___m228, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Addition(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(Unity.Mathematics.float3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float3x3::Equals(Unity.Mathematics.float3x3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float3x3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3x3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float3x3::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B (String_t* ___format0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.String Unity.Mathematics.float3x3::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float3x3::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_yxw()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_ExclusiveOr(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.math::asfloat(Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline (float ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_zwx()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Subtraction(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.math::float3(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_wzy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float2,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float ___z1, float ___w2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___zw1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___xyzw0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___v0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.int4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_xyzx()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_yzxy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_yzxz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_zxyy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_zxyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_wwwx()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_wwww()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::set_xyz(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___value0, const RuntimeMethod* method);
// Unity.Mathematics.float2 Unity.Mathematics.float4::get_xy()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_xy_mC5A971D69E3278438139480A25313449133571E0_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float2 Unity.Mathematics.float4::get_zw()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// System.Single Unity.Mathematics.float4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float4::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2 (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4x4::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4x4::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(System.Single,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_UnaryNegation(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___val0, const RuntimeMethod* method);
// Unity.Mathematics.float4& Unity.Mathematics.float4x4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4x4::Equals(Unity.Mathematics.float4x4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.float4x4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4x4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m16C61605FBE531F8EB703823255407075EA5A464_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.float4x4::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4x4::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.float4x4::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF_inline (float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method);
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::Scale(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_Scale_mEE04A811A295C99F280056A51ABC83356CB0A4A3_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// Unity.Mathematics.float3x3 Unity.Mathematics.math::float3x3(Unity.Mathematics.quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  math_float3x3_m083898B3E74368DC86FB3D19406C3ED60AF63583_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___rotation0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::float4(Unity.Mathematics.float3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method);
// Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int2::.ctor(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int2::.ctor(Unity.Mathematics.float2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.int2::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.int2::Equals(Unity.Mathematics.int2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.int2::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.int2::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.int2::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8 (int32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.String Unity.Mathematics.int2::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int3::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int3::.ctor(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___v0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int3::.ctor(Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.int3::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.int3::Equals(Unity.Mathematics.int3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.int3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.int3::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.int3::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.int3::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___w3, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.int3,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___xyz0, int32_t ___w1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method);
// Unity.Mathematics.int3 Unity.Mathematics.int4::get_xyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.int4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::set_Item(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.int4::Equals(Unity.Mathematics.int4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.int4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.int4::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.int4::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.int4::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.float2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint2 Unity.Mathematics.math::uint2(System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline (uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method);
// Unity.Mathematics.uint2 Unity.Mathematics.uint2::op_Multiply(Unity.Mathematics.uint2,Unity.Mathematics.uint2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___lhs0, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs1, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Addition(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs1, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::select_shuffle_component(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.math/ShuffleComponent)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, uint8_t ___component2, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::float4(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline (float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::movelh(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::movehl(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::shuffle(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, uint8_t ___x2, uint8_t ___y3, uint8_t ___z4, uint8_t ___w5, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::float4(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_m6D1066B47EE7BC7A47A136D969B5A3CA41902224_inline (float ___v0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m489385B46636C7B5CCE01DEECA0A603A2EE807AA_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.int2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.int3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.int4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___x0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::asint(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86_inline (float ___x0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::asuint(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::asfloat(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181_inline (int32_t ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::asfloat(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline (uint32_t ___x0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::min(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A_inline (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// System.Boolean System.Single::IsNaN(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599 (float ___f0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::min(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::max(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::max(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF_inline (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::min(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_min_mC45C7024F6850EB1D382A2B2F6F31CCA4B2750E6_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::max(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_max_m5A2AD47597A69870F6E5A8A2ABEE9A1EAC771393_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::clamp(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_clamp_m3FDC8AAE05842733E193852C929DAAC6F5E8C931_inline (float ___x0, float ___a1, float ___b2, const RuntimeMethod* method);
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_BitwiseAnd(Unity.Mathematics.uint4,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_BitwiseAnd_m8BDB3A45AD663DDFBC3E18A0EA84B3C902767C36_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::asfloat(Unity.Mathematics.uint4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_asfloat_mD9B4C5BBB6AC641027D94C2045C4C3AF416DFF6D_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::floor(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline (float ___x0, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(System.Single,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_mE2739F8C6E0ADC06D8DCD923F9828B75EACB3ECF_inline (float ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(System.Single,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m847947B86BE29A43C35E74AB168A525866AA4B15_inline (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::sign(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline (float ___x0, const RuntimeMethod* method);
// System.Double System.Math::Pow(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F (double ___x0, double ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::sqrt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::rsqrt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline (float ___x0, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.math::select(Unity.Mathematics.float3,Unity.Mathematics.float3,System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_select_mD6522E9D958653C41F4B2E3463B79F7A011E60AB_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___a0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___b1, bool ___c2, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::select(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_select_m3FF6FBC5429C94D76C025979A6D3DCA2B2B62602_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, bool ___c2, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_m6238E071DBA53F376473FFEC28706C9B537E6BAE_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___y1, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::sin(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE_inline (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::cos(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424_inline (float ___x0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::lzcnt(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_lzcnt_m69123CF8445E0B67FA30F24BA42F663C105BE7EB_inline (uint32_t ___x0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::tzcnt(System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_tzcnt_m3A483A42E55BE961B589C59F99EE7972D0D296E8_inline (uint32_t ___x0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.quaternion::.ctor(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method);
// Unity.Mathematics.quaternion Unity.Mathematics.math::quaternion(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::rcp(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_rcp_mBBCDD8F05AFE2E307F898E81A06B64BF0E2625D0_inline (float ___x0, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.math::cross(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___a0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___b1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::lerp(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, float ___s2, const RuntimeMethod* method);
// Unity.Mathematics.quaternion Unity.Mathematics.math::normalize(Unity.Mathematics.quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::acos(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_acos_mDF4948B8CF18EF8052C38D7914802E7869B79B6C_inline (float ___x0, const RuntimeMethod* method);
// Unity.Mathematics.quaternion Unity.Mathematics.math::nlerp(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_nlerp_mB7BD6C0F6F0AD1411F206E3BFDCC4161F1DF5D54_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q10, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q21, float ___t2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.uint2::.ctor(System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method);
// System.Void Unity.Mathematics.quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void Unity.Mathematics.math::sincos(System.Single,System.Single&,System.Single&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void math_sincos_m62FF0B31557448DAF3C3ED1EBCA171BD524BD96B_inline (float ___x0, float* ___s1, float* ___c2, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.quaternion::Equals(Unity.Mathematics.quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___x0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.quaternion::Equals(System.Object)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, RuntimeObject * ___x0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.quaternion)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.quaternion::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.quaternion::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.quaternion::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint2::Equals(Unity.Mathematics.uint2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint2::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.uint2::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint2::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, const RuntimeMethod* method);
// System.String System.UInt32::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B (uint32_t* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint2::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.uint3::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint3::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs0, const RuntimeMethod* method);
// System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, RuntimeObject * ___o0, const RuntimeMethod* method);
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___v0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.uint4::GetHashCode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint4::ToString()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, const RuntimeMethod* method);
// System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline(_thisAdjusted, ___x0, ___y1, method);
}
// System.Void Unity.Mathematics.float2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		int32_t L_0 = ___v0;
		__this->set_x_0(((float)((float)L_0)));
		// this.y = v;
		int32_t L_1 = ___v0;
		__this->set_y_1(((float)((float)L_1)));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_AdjustorThunk (RuntimeObject * __this, int32_t ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_inline(_thisAdjusted, ___v0, method);
}
// Unity.Mathematics.float2 Unity.Mathematics.float2::op_Implicit(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_op_Implicit_mB01382E876FA2AFC63701F493121DEA95D2677E1 (int32_t ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float2(int v) { return new float2(v); }
		int32_t L_0 = ___v0;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.float2::op_Multiply(Unity.Mathematics.float2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_op_Multiply_m7262BDAFAD6CB1D6D980929C82826C82C86F5039 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float2 operator * (float2 lhs, float rhs) { return new float2 (lhs.x * rhs, lhs.y * rhs); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6;
		memset((&L_6), 0, sizeof(L_6));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.float2::op_Addition(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_op_Addition_m1FCEA54B4EB9E2E5BBF9E2C5A6466589B486CF81 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___lhs0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float2 operator + (float2 lhs, float2 rhs) { return new float2 (lhs.x + rhs.x, lhs.y + rhs.y); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_8), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.float2::op_Subtraction(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_op_Subtraction_mD7E1752FB29F54A98A4A52140E8BCFD659471E19 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___lhs0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float2 operator - (float2 lhs, float2 rhs) { return new float2 (lhs.x - rhs.x, lhs.y - rhs.y); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_8), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.float2::op_Division(Unity.Mathematics.float2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_op_Division_m122E66E69678386034C53B7D9B89A551628473FD (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float2 operator / (float2 lhs, float rhs) { return new float2 (lhs.x / rhs, lhs.y / rhs); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6;
		memset((&L_6), 0, sizeof(L_6));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_6), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.float2::get_xy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(x, y); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	float2_tCB7B81181978EDE17722C533A55E345D9A413274  _returnValue;
	_returnValue = float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Single Unity.Mathematics.float2::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * V_0 = NULL;
	{
		// {
		V_0 = (float2_tCB7B81181978EDE17722C533A55E345D9A413274 *)__this;
		// fixed (float2* array = &this) { return ((float*)array)[index]; }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274 * L_0 = V_0;
		// fixed (float2* array = &this) { return ((float*)array)[index]; }
		int32_t L_1 = ___index0;
		float L_2 = *((float*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)4)))));
		return L_2;
	}
}
IL2CPP_EXTERN_C  float float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	float _returnValue;
	_returnValue = float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.float2::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	float* V_0 = NULL;
	{
		// {
		float* L_0 = __this->get_address_of_x_0();
		V_0 = (float*)L_0;
		// fixed (float* array = &x) { array[index] = value; }
		float* L_1 = V_0;
		// fixed (float* array = &x) { array[index] = value; }
		int32_t L_2 = ___index0;
		float L_3 = ___value1;
		*((float*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_1), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_2), (int32_t)4))))) = (float)L_3;
		V_0 = (float*)((uintptr_t)0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092(_thisAdjusted, ___index0, ___value1, method);
}
// System.Boolean Unity.Mathematics.float2::Equals(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float2 rhs) { return x == rhs.x && y == rhs.y; }
		float L_0 = __this->get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		return (bool)((((float)L_3) == ((float)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_AdjustorThunk (RuntimeObject * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	bool _returnValue;
	_returnValue = float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.float2::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float2_tCB7B81181978EDE17722C533A55E345D9A413274_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float2)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_inline((float2_tCB7B81181978EDE17722C533A55E345D9A413274 *)__this, ((*(float2_tCB7B81181978EDE17722C533A55E345D9A413274 *)((float2_tCB7B81181978EDE17722C533A55E345D9A413274 *)UnBox(L_0, float2_tCB7B81181978EDE17722C533A55E345D9A413274_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	bool _returnValue;
	_returnValue = float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.float2::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = (*(float2_tCB7B81181978EDE17722C533A55E345D9A413274 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float2::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float2({0}f, {1}f)", x, y);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_EXTERN_C  String_t* float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float2::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78 (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float2({0}f, {1}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider));
		float* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_0, L_1, L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_4, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_EXTERN_C  String_t* float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float2_tCB7B81181978EDE17722C533A55E345D9A413274 * _thisAdjusted = reinterpret_cast<float2_tCB7B81181978EDE17722C533A55E345D9A413274 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void Unity.Mathematics.float3::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		float L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		float L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		float L_2 = ___v0;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C_AdjustorThunk (RuntimeObject * __this, float ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C_inline(_thisAdjusted, ___v0, method);
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float3 lhs, float3 rhs) { return new float3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float3 lhs, float rhs) { return new float3 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE (float ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float lhs, float3 rhs) { return new float3 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z); }
		float L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Addition(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator + (float3 lhs, float3 rhs) { return new float3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Subtraction(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator - (float3 lhs, float3 rhs) { return new float3 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_m84488830E369A20FBB0BE95A05FD94DE1F4DDBB2 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator / (float3 lhs, float3 rhs) { return new float3 (lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)((float)L_1/(float)L_3)), ((float)((float)L_5/(float)L_7)), ((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator / (float3 lhs, float rhs) { return new float3 (lhs.x / rhs, lhs.y / rhs, lhs.z / rhs); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(System.Single,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_mE2739F8C6E0ADC06D8DCD923F9828B75EACB3ECF (float ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator / (float lhs, float3 rhs) { return new float3 (lhs / rhs.x, lhs / rhs.y, lhs / rhs.z); }
		float L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)((float)L_0/(float)L_2)), ((float)((float)L_3/(float)L_5)), ((float)((float)L_6/(float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::get_xyz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  _returnValue;
	_returnValue = float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::get_yzx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(y, z, x); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  _returnValue;
	_returnValue = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float2 Unity.Mathematics.float3::get_xy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(x, y); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float2_tCB7B81181978EDE17722C533A55E345D9A413274  _returnValue;
	_returnValue = float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float2 Unity.Mathematics.float3::get_yz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(y, z); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float2_tCB7B81181978EDE17722C533A55E345D9A413274  _returnValue;
	_returnValue = float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Single Unity.Mathematics.float3::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * V_0 = NULL;
	{
		// {
		V_0 = (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)__this;
		// fixed (float3* array = &this) { return ((float*)array)[index]; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_0 = V_0;
		// fixed (float3* array = &this) { return ((float*)array)[index]; }
		int32_t L_1 = ___index0;
		float L_2 = *((float*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)4)))));
		return L_2;
	}
}
IL2CPP_EXTERN_C  float float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float _returnValue;
	_returnValue = float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.float3::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	float* V_0 = NULL;
	{
		// {
		float* L_0 = __this->get_address_of_x_0();
		V_0 = (float*)L_0;
		// fixed (float* array = &x) { array[index] = value; }
		float* L_1 = V_0;
		// fixed (float* array = &x) { array[index] = value; }
		int32_t L_2 = ___index0;
		float L_3 = ___value1;
		*((float*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_1), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_2), (int32_t)4))))) = (float)L_3;
		V_0 = (float*)((uintptr_t)0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A(_thisAdjusted, ___index0, ___value1, method);
}
// System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		float L_0 = __this->get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		return (bool)((((float)L_6) == ((float)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_AdjustorThunk (RuntimeObject * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	bool _returnValue;
	_returnValue = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.float3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float3)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)__this, ((*(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)UnBox(L_0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	bool _returnValue;
	_returnValue = float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.float3::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = (*(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)__this);
		uint32_t L_1;
		L_1 = math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float3::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x, y, z);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		float L_6 = __this->get_z_2();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C  String_t* float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		float* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_0, L_1, L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_4, L_5, L_6, /*hidden argument*/NULL);
		float* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11;
		L_11 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C  String_t* float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * _thisAdjusted = reinterpret_cast<float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
// UnityEngine.Vector3 Unity.Mathematics.float3::op_Implicit(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  float3_op_Implicit_m10DFE908DCDD18D73FA11E4C9CDB23E16FA3ABE8 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator Vector3(float3 v)     { return new Vector3(v.x, v.y, v.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.float3::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Implicit_mC2BC870EF4246C5C3A2FC27EE9ABEDAFF49DC1EF (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float3(Vector3 v)     { return new float3(v.x, v.y, v.z); }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___v0;
		float L_5 = L_4.get_z_4();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6;
		memset((&L_6), 0, sizeof(L_6));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_6), L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.float3,Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c00, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c11, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c22, const RuntimeMethod* method)
{
	{
		// this.c0 = c0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___c00;
		__this->set_c0_0(L_0);
		// this.c1 = c1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___c11;
		__this->set_c1_1(L_1);
		// this.c2 = c2;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___c22;
		__this->set_c2_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_AdjustorThunk (RuntimeObject * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c00, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c11, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c22, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_inline(_thisAdjusted, ___c00, ___c11, ___c22, method);
}
// System.Void Unity.Mathematics.float3x3::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float ___m000, float ___m011, float ___m022, float ___m103, float ___m114, float ___m125, float ___m206, float ___m217, float ___m228, const RuntimeMethod* method)
{
	{
		// this.c0 = new float3(m00, m10, m20);
		float L_0 = ___m000;
		float L_1 = ___m103;
		float L_2 = ___m206;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_c0_0(L_3);
		// this.c1 = new float3(m01, m11, m21);
		float L_4 = ___m011;
		float L_5 = ___m114;
		float L_6 = ___m217;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		memset((&L_7), 0, sizeof(L_7));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_7), L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_c1_1(L_7);
		// this.c2 = new float3(m02, m12, m22);
		float L_8 = ___m022;
		float L_9 = ___m125;
		float L_10 = ___m228;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		memset((&L_11), 0, sizeof(L_11));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_11), L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->set_c2_2(L_11);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_AdjustorThunk (RuntimeObject * __this, float ___m000, float ___m011, float ___m022, float ___m103, float ___m114, float ___m125, float ___m206, float ___m217, float ___m228, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_inline(_thisAdjusted, ___m000, ___m011, ___m022, ___m103, ___m114, ___m125, ___m206, ___m217, ___m228, method);
}
// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Multiply(Unity.Mathematics.float3x3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  float3x3_op_Multiply_m84830685A93039297DC893DA1B6A28D675822961 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3x3 operator * (float3x3 lhs, float rhs) { return new float3x3 (lhs.c0 * rhs, lhs.c1 * rhs, lhs.c2 * rhs); }
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = L_0.get_c0_0();
		float L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_1, L_2, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_4 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = L_4.get_c1_1();
		float L_6 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		L_7 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_5, L_6, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_8 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9 = L_8.get_c2_2();
		float L_10 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		L_11 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_9, L_10, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_inline((&L_12), L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Addition(Unity.Mathematics.float3x3,Unity.Mathematics.float3x3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  float3x3_op_Addition_mD075893A8D24978DC605DA7A415F7CE4BEE8F0F3 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___lhs0, float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3x3 operator + (float3x3 lhs, float3x3 rhs) { return new float3x3 (lhs.c0 + rhs.c0, lhs.c1 + rhs.c1, lhs.c2 + rhs.c2); }
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = L_0.get_c0_0();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = L_2.get_c0_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4;
		L_4 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_1, L_3, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_5 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = L_5.get_c1_1();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_7 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = L_7.get_c1_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		L_9 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_6, L_8, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_10 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11 = L_10.get_c2_2();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_12 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_13 = L_12.get_c2_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_14;
		L_14 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_11, L_13, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_15;
		memset((&L_15), 0, sizeof(L_15));
		float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Division(Unity.Mathematics.float3x3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  float3x3_op_Division_m92B0F2EBB2E77BF84C675C513DC3F84445F2CB69 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3x3 operator / (float3x3 lhs, float rhs) { return new float3x3 (lhs.c0 / rhs, lhs.c1 / rhs, lhs.c2 / rhs); }
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = L_0.get_c0_0();
		float L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A_inline(L_1, L_2, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_4 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = L_4.get_c1_1();
		float L_6 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		L_7 = float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A_inline(L_5, L_6, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_8 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9 = L_8.get_c2_2();
		float L_10 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		L_11 = float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A_inline(L_9, L_10, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_inline((&L_12), L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean Unity.Mathematics.float3x3::Equals(Unity.Mathematics.float3x3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float3x3 rhs) { return c0.Equals(rhs.c0) && c1.Equals(rhs.c1) && c2.Equals(rhs.c2); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_0 = __this->get_address_of_c0_0();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_1 = ___rhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = L_1.get_c0_0();
		bool L_3;
		L_3 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_4 = __this->get_address_of_c1_1();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_5 = ___rhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = L_5.get_c1_1();
		bool L_7;
		L_7 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_8 = __this->get_address_of_c2_2();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_9 = ___rhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = L_9.get_c2_2();
		bool L_11;
		L_11 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0038:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_AdjustorThunk (RuntimeObject * __this, float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	bool _returnValue;
	_returnValue = float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.float3x3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float3x3)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_inline((float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *)__this, ((*(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *)((float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *)UnBox(L_0, float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	bool _returnValue;
	_returnValue = float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.float3x3::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = (*(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *)__this);
		uint32_t L_1;
		L_1 = math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float3x3::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3x3({0}f, {1}f, {2}f,  {3}f, {4}f, {5}f,  {6}f, {7}f, {8}f)", c0.x, c1.x, c2.x, c0.y, c1.y, c2.y, c0.z, c1.z, c2.z);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_2 = __this->get_address_of_c0_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_7 = __this->get_address_of_c1_1();
		float L_8 = L_7->get_x_0();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_12 = __this->get_address_of_c2_2();
		float L_13 = L_12->get_x_0();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_17 = __this->get_address_of_c0_0();
		float L_18 = L_17->get_y_1();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = L_16;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_22 = __this->get_address_of_c1_1();
		float L_23 = L_22->get_y_1();
		float L_24 = L_23;
		RuntimeObject * L_25 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_25);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_26 = L_21;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_27 = __this->get_address_of_c2_2();
		float L_28 = L_27->get_y_1();
		float L_29 = L_28;
		RuntimeObject * L_30 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_26;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_32 = __this->get_address_of_c0_0();
		float L_33 = L_32->get_z_2();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_35);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_35);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_36 = L_31;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_37 = __this->get_address_of_c1_1();
		float L_38 = L_37->get_z_2();
		float L_39 = L_38;
		RuntimeObject * L_40 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_40);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_41 = L_36;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_42 = __this->get_address_of_c2_2();
		float L_43 = L_42->get_z_2();
		float L_44 = L_43;
		RuntimeObject * L_45 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_45);
		String_t* L_46;
		L_46 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E, L_41, /*hidden argument*/NULL);
		return L_46;
	}
}
IL2CPP_EXTERN_C  String_t* float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float3x3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3x3({0}f, {1}f, {2}f,  {3}f, {4}f, {5}f,  {6}f, {7}f, {8}f)", c0.x.ToString(format, formatProvider), c1.x.ToString(format, formatProvider), c2.x.ToString(format, formatProvider), c0.y.ToString(format, formatProvider), c1.y.ToString(format, formatProvider), c2.y.ToString(format, formatProvider), c0.z.ToString(format, formatProvider), c1.z.ToString(format, formatProvider), c2.z.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_2 = __this->get_address_of_c0_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6;
		L_6 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_8 = __this->get_address_of_c1_1();
		float* L_9 = L_8->get_address_of_x_0();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12;
		L_12 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_7;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_14 = __this->get_address_of_c2_2();
		float* L_15 = L_14->get_address_of_x_0();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18;
		L_18 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = L_13;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_20 = __this->get_address_of_c0_0();
		float* L_21 = L_20->get_address_of_y_1();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24;
		L_24 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_25 = L_19;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_26 = __this->get_address_of_c1_1();
		float* L_27 = L_26->get_address_of_y_1();
		String_t* L_28 = ___format0;
		RuntimeObject* L_29 = ___formatProvider1;
		String_t* L_30;
		L_30 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_25;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_32 = __this->get_address_of_c2_2();
		float* L_33 = L_32->get_address_of_y_1();
		String_t* L_34 = ___format0;
		RuntimeObject* L_35 = ___formatProvider1;
		String_t* L_36;
		L_36 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_36);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_36);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_37 = L_31;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_38 = __this->get_address_of_c0_0();
		float* L_39 = L_38->get_address_of_z_2();
		String_t* L_40 = ___format0;
		RuntimeObject* L_41 = ___formatProvider1;
		String_t* L_42;
		L_42 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_39, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_42);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_43 = L_37;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_44 = __this->get_address_of_c1_1();
		float* L_45 = L_44->get_address_of_z_2();
		String_t* L_46 = ___format0;
		RuntimeObject* L_47 = ___formatProvider1;
		String_t* L_48;
		L_48 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_48);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_49 = L_43;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_50 = __this->get_address_of_c2_2();
		float* L_51 = L_50->get_address_of_z_2();
		String_t* L_52 = ___format0;
		RuntimeObject* L_53 = ___formatProvider1;
		String_t* L_54;
		L_54 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_54);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_54);
		String_t* L_55;
		L_55 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E, L_49, /*hidden argument*/NULL);
		return L_55;
	}
}
IL2CPP_EXTERN_C  String_t* float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_1;
	memset((&V_1), 0, sizeof(V_1));
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  V_2;
	memset((&V_2), 0, sizeof(V_2));
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  V_3;
	memset((&V_3), 0, sizeof(V_3));
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// float4 v = q.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		V_0 = L_1;
		// float4 v2 = v + v;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// uint3 npn = uint3(0x80000000, 0x00000000, 0x80000000);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_5;
		L_5 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-2147483648LL), 0, ((int32_t)-2147483648LL), /*hidden argument*/NULL);
		V_2 = L_5;
		// uint3 nnp = uint3(0x80000000, 0x80000000, 0x00000000);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6;
		L_6 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-2147483648LL), ((int32_t)-2147483648LL), 0, /*hidden argument*/NULL);
		V_3 = L_6;
		// uint3 pnn = uint3(0x00000000, 0x80000000, 0x80000000);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_7;
		L_7 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(0, ((int32_t)-2147483648LL), ((int32_t)-2147483648LL), /*hidden argument*/NULL);
		V_4 = L_7;
		// c0 = v2.y * asfloat(asuint(v.yxw) ^ npn) - v2.z * asfloat(asuint(v.zwx) ^ pnn) + float3(1, 0, 0);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = V_1;
		float L_9 = L_8.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10;
		L_10 = float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_0), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_11;
		L_11 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_10, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12 = V_2;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_13;
		L_13 = uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline(L_11, L_12, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_14;
		L_14 = math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline(L_13, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_15;
		L_15 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_9, L_14, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16 = V_1;
		float L_17 = L_16.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_18;
		L_18 = float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_0), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_19;
		L_19 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_18, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_20 = V_4;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_21;
		L_21 = uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline(L_19, L_20, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_22;
		L_22 = math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline(L_21, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_23;
		L_23 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_17, L_22, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_24;
		L_24 = float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline(L_15, L_23, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_25;
		L_25 = math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline((1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_26;
		L_26 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_24, L_25, /*hidden argument*/NULL);
		__this->set_c0_0(L_26);
		// c1 = v2.z * asfloat(asuint(v.wzy) ^ nnp) - v2.x * asfloat(asuint(v.yxw) ^ npn) + float3(0, 1, 0);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_27 = V_1;
		float L_28 = L_27.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_29;
		L_29 = float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_0), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_30;
		L_30 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_29, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_31 = V_3;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_32;
		L_32 = uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline(L_30, L_31, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_33;
		L_33 = math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline(L_32, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_34;
		L_34 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_28, L_33, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_35 = V_1;
		float L_36 = L_35.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_37;
		L_37 = float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_0), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_38;
		L_38 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_37, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_39 = V_2;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_40;
		L_40 = uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline(L_38, L_39, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_41;
		L_41 = math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline(L_40, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_42;
		L_42 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_36, L_41, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_43;
		L_43 = float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline(L_34, L_42, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_44;
		L_44 = math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline((0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_45;
		L_45 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_43, L_44, /*hidden argument*/NULL);
		__this->set_c1_1(L_45);
		// c2 = v2.x * asfloat(asuint(v.zwx) ^ pnn) - v2.y * asfloat(asuint(v.wzy) ^ nnp) + float3(0, 0, 1);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_46 = V_1;
		float L_47 = L_46.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_48;
		L_48 = float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_0), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_49;
		L_49 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_48, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_50 = V_4;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_51;
		L_51 = uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline(L_49, L_50, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_52;
		L_52 = math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline(L_51, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_53;
		L_53 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_47, L_52, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_54 = V_1;
		float L_55 = L_54.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_56;
		L_56 = float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_0), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_57;
		L_57 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_56, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_58 = V_3;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_59;
		L_59 = uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline(L_57, L_58, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_60;
		L_60 = math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline(L_59, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_61;
		L_61 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_55, L_60, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_62;
		L_62 = float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline(L_53, L_61, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_63;
		L_63 = math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline((0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_64;
		L_64 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_62, L_63, /*hidden argument*/NULL);
		__this->set_c2_2(L_64);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4_AdjustorThunk (RuntimeObject * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * _thisAdjusted = reinterpret_cast<float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *>(__this + _offset);
	float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4(_thisAdjusted, ___q0, method);
}
// System.Void Unity.Mathematics.float3x3::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float3x3__cctor_mF2DDB6D13785F3BF4A1D002DD387896890A8ACC4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly float3x3 identity = new float3x3(1.0f, 0.0f, 0.0f,   0.0f, 1.0f, 0.0f,   0.0f, 0.0f, 1.0f);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0;
		memset((&L_0), 0, sizeof(L_0));
		float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_inline((&L_0), (1.0f), (0.0f), (0.0f), (0.0f), (1.0f), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_StaticFields*)il2cpp_codegen_static_fields_for(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B_il2cpp_TypeInfo_var))->set_identity_3(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float2,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float ___z1, float ___w2, const RuntimeMethod* method)
{
	{
		// this.x = xy.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___xy0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xy.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___xy0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = z;
		float L_4 = ___z1;
		__this->set_z_2(L_4);
		// this.w = w;
		float L_5 = ___w2;
		__this->set_w_3(L_5);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7_AdjustorThunk (RuntimeObject * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float ___z1, float ___w2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7_inline(_thisAdjusted, ___xy0, ___z1, ___w2, method);
}
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___zw1, const RuntimeMethod* method)
{
	{
		// this.x = xy.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___xy0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xy.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___xy0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = zw.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___zw1;
		float L_5 = L_4.get_x_0();
		__this->set_z_2(L_5);
		// this.w = zw.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6 = ___zw1;
		float L_7 = L_6.get_y_1();
		__this->set_w_3(L_7);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B_AdjustorThunk (RuntimeObject * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___zw1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B_inline(_thisAdjusted, ___xy0, ___zw1, method);
}
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___xyz0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___xyz0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___xyz0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		float L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_AdjustorThunk (RuntimeObject * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_inline(_thisAdjusted, ___xyz0, ___w1, method);
}
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___xyzw0, const RuntimeMethod* method)
{
	{
		// this.x = xyzw.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___xyzw0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyzw.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___xyzw0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyzw.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___xyzw0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = xyzw.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___xyzw0;
		float L_7 = L_6.get_w_3();
		__this->set_w_3(L_7);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758_AdjustorThunk (RuntimeObject * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___xyzw0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758_inline(_thisAdjusted, ___xyzw0, method);
}
// System.Void Unity.Mathematics.float4::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		float L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		float L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		float L_2 = ___v0;
		__this->set_z_2(L_2);
		// this.w = v;
		float L_3 = ___v0;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_AdjustorThunk (RuntimeObject * __this, float ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline(_thisAdjusted, ___v0, method);
}
// System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v.x;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___v0;
		int32_t L_1 = L_0.get_x_0();
		__this->set_x_0(((float)((float)L_1)));
		// this.y = v.y;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___v0;
		int32_t L_3 = L_2.get_y_1();
		__this->set_y_1(((float)((float)L_3)));
		// this.z = v.z;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___v0;
		int32_t L_5 = L_4.get_z_2();
		__this->set_z_2(((float)((float)L_5)));
		// this.w = v.w;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___v0;
		int32_t L_7 = L_6.get_w_3();
		__this->set_w_3(((float)((float)L_7)));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_AdjustorThunk (RuntimeObject * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_inline(_thisAdjusted, ___v0, method);
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Implicit_m18F0BAF7440BF58605AA7A75CB911431ED4347DF (float ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float4(float v) { return new float4(v); }
		float L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Implicit_m4C5BAD1D894F2CFBC62675DBE2813CC48BB57F4C (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float4(int4 v) { return new float4(v); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float4 rhs) { return new float4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), ((float)il2cpp_codegen_multiply((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float rhs) { return new float4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(System.Single,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70 (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float lhs, float4 rhs) { return new float4 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w); }
		float L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float L_9 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator + (float4 lhs, float4 rhs) { return new float4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), ((float)il2cpp_codegen_add((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Addition_m50BF16260478F8BEBB56698FC677066CB2792697 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator + (float4 lhs, float rhs) { return new float4 (lhs.x + rhs, lhs.y + rhs, lhs.z + rhs, lhs.w + rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_2)), ((float)il2cpp_codegen_add((float)L_4, (float)L_5)), ((float)il2cpp_codegen_add((float)L_7, (float)L_8)), ((float)il2cpp_codegen_add((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator - (float4 lhs, float4 rhs) { return new float4 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), ((float)il2cpp_codegen_subtract((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Subtraction_mC5AB272630A81EDCFC642F4953FA24F84F03FBFB (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator - (float4 lhs, float rhs) { return new float4 (lhs.x - rhs, lhs.y - rhs, lhs.z - rhs, lhs.w - rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)), ((float)il2cpp_codegen_subtract((float)L_4, (float)L_5)), ((float)il2cpp_codegen_subtract((float)L_7, (float)L_8)), ((float)il2cpp_codegen_subtract((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m489385B46636C7B5CCE01DEECA0A603A2EE807AA (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float4 lhs, float4 rhs) { return new float4 (lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z, lhs.w / rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)((float)L_1/(float)L_3)), ((float)((float)L_5/(float)L_7)), ((float)((float)L_9/(float)L_11)), ((float)((float)L_13/(float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float4 lhs, float rhs) { return new float4 (lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), ((float)((float)L_10/(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(System.Single,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m847947B86BE29A43C35E74AB168A525866AA4B15 (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float lhs, float4 rhs) { return new float4 (lhs / rhs.x, lhs / rhs.y, lhs / rhs.z, lhs / rhs.w); }
		float L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float L_9 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)((float)L_0/(float)L_2)), ((float)((float)L_3/(float)L_5)), ((float)((float)L_6/(float)L_8)), ((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_UnaryNegation(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___val0, const RuntimeMethod* method)
{
	{
		// public static float4 operator - (float4 val) { return new float4 (-val.x, -val.y, -val.z, -val.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___val0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___val0;
		float L_3 = L_2.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___val0;
		float L_5 = L_4.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___val0;
		float L_7 = L_6.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_8), ((-L_1)), ((-L_3)), ((-L_5)), ((-L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_xyzx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(x, y, z, x); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float L_3 = __this->get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_yzxy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(y, z, x, y); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_x_0();
		float L_3 = __this->get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_yzxz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(y, z, x, z); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_x_0();
		float L_3 = __this->get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_zxyy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(z, x, y, y); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_zxyz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(z, x, y, z); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_wwwx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(w, w, w, x); }
		float L_0 = __this->get_w_3();
		float L_1 = __this->get_w_3();
		float L_2 = __this->get_w_3();
		float L_3 = __this->get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::get_wwww()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(w, w, w, w); }
		float L_0 = __this->get_w_3();
		float L_1 = __this->get_w_3();
		float L_2 = __this->get_w_3();
		float L_3 = __this->get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  _returnValue;
	_returnValue = float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  _returnValue;
	_returnValue = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.float4::set_xyz(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___value0, const RuntimeMethod* method)
{
	{
		// set { x = value.x; y = value.y; z = value.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___value0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// set { x = value.x; y = value.y; z = value.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___value0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// set { x = value.x; y = value.y; z = value.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___value0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// set { x = value.x; y = value.y; z = value.z; }
		return;
	}
}
IL2CPP_EXTERN_C  void float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91_AdjustorThunk (RuntimeObject * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91_inline(_thisAdjusted, ___value0, method);
}
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_yxw()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(y, x, w); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_w_3();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  _returnValue;
	_returnValue = float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_zwx()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(z, w, x); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_w_3();
		float L_2 = __this->get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  _returnValue;
	_returnValue = float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_wzy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(w, z, y); }
		float L_0 = __this->get_w_3();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  _returnValue;
	_returnValue = float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float2 Unity.Mathematics.float4::get_xy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_xy_mC5A971D69E3278438139480A25313449133571E0 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(x, y); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_xy_mC5A971D69E3278438139480A25313449133571E0_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float2_tCB7B81181978EDE17722C533A55E345D9A413274  _returnValue;
	_returnValue = float4_get_xy_mC5A971D69E3278438139480A25313449133571E0_inline(_thisAdjusted, method);
	return _returnValue;
}
// Unity.Mathematics.float2 Unity.Mathematics.float4::get_zw()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(z, w); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_w_3();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float2_tCB7B81181978EDE17722C533A55E345D9A413274  _returnValue;
	_returnValue = float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Single Unity.Mathematics.float4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * V_0 = NULL;
	{
		// {
		V_0 = (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)__this;
		// fixed (float4* array = &this) { return ((float*)array)[index]; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = V_0;
		// fixed (float4* array = &this) { return ((float*)array)[index]; }
		int32_t L_1 = ___index0;
		float L_2 = *((float*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)4)))));
		return L_2;
	}
}
IL2CPP_EXTERN_C  float float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float _returnValue;
	_returnValue = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.float4::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	float* V_0 = NULL;
	{
		// {
		float* L_0 = __this->get_address_of_x_0();
		V_0 = (float*)L_0;
		// fixed (float* array = &x) { array[index] = value; }
		float* L_1 = V_0;
		// fixed (float* array = &x) { array[index] = value; }
		int32_t L_2 = ___index0;
		float L_3 = ___value1;
		*((float*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_1), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_2), (int32_t)4))))) = (float)L_3;
		V_0 = (float*)((uintptr_t)0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D(_thisAdjusted, ___index0, ___value1, method);
}
// System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		float L_0 = __this->get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		if ((!(((float)L_6) == ((float)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		float L_9 = __this->get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs0;
		float L_11 = L_10.get_w_3();
		return (bool)((((float)L_9) == ((float)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_AdjustorThunk (RuntimeObject * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	bool _returnValue;
	_returnValue = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.float4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)__this, ((*(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)UnBox(L_0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	bool _returnValue;
	_returnValue = float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.float4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = (*(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)__this);
		uint32_t L_1;
		L_1 = math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x, y, z, w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float L_2 = __this->get_x_0();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_1;
		float L_6 = __this->get_y_1();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = L_5;
		float L_10 = __this->get_z_2();
		float L_11 = L_10;
		RuntimeObject * L_12 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_9;
		float L_14 = __this->get_w_3();
		float L_15 = L_14;
		RuntimeObject * L_16 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17;
		L_17 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C  String_t* float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5;
		L_5 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10;
		L_10 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15;
		L_15 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20;
		L_20 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C  String_t* float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _thisAdjusted = reinterpret_cast<float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Implicit_mADB6904960D508B7B30752A99C7013AED76065ED (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator float4(Vector4 v)     { return new float4(v.x, v.y, v.z, v.w); }
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0 = ___v0;
		float L_1 = L_0.get_x_1();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_2 = ___v0;
		float L_3 = L_2.get_y_2();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_4 = ___v0;
		float L_5 = L_4.get_z_3();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_6 = ___v0;
		float L_7 = L_6.get_w_4();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector4 Unity.Mathematics.float4::op_Implicit(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  float4_op_Implicit_mD1C84DA295349F9E668ACB153BA03F21D2B976CE (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator Vector4(float4 v)     { return new Vector4(v.x, v.y, v.z, v.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___v0;
		float L_7 = L_6.get_w_3();
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.float4x4::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method)
{
	{
		// this.c0 = c0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___c00;
		__this->set_c0_0(L_0);
		// this.c1 = c1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___c11;
		__this->set_c1_1(L_1);
		// this.c2 = c2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___c22;
		__this->set_c2_2(L_2);
		// this.c3 = c3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___c33;
		__this->set_c3_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_AdjustorThunk (RuntimeObject * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline(_thisAdjusted, ___c00, ___c11, ___c22, ___c33, method);
}
// System.Void Unity.Mathematics.float4x4::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	{
		// this.c0 = new float4(m00, m10, m20, m30);
		float L_0 = ___m000;
		float L_1 = ___m104;
		float L_2 = ___m208;
		float L_3 = ___m3012;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_c0_0(L_4);
		// this.c1 = new float4(m01, m11, m21, m31);
		float L_5 = ___m011;
		float L_6 = ___m115;
		float L_7 = ___m219;
		float L_8 = ___m3113;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_c1_1(L_9);
		// this.c2 = new float4(m02, m12, m22, m32);
		float L_10 = ___m022;
		float L_11 = ___m126;
		float L_12 = ___m2210;
		float L_13 = ___m3214;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14;
		memset((&L_14), 0, sizeof(L_14));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_14), L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->set_c2_2(L_14);
		// this.c3 = new float4(m03, m13, m23, m33);
		float L_15 = ___m033;
		float L_16 = ___m137;
		float L_17 = ___m2311;
		float L_18 = ___m3315;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		memset((&L_19), 0, sizeof(L_19));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_19), L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		__this->set_c3_3(L_19);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_AdjustorThunk (RuntimeObject * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_inline(_thisAdjusted, ___m000, ___m011, ___m022, ___m033, ___m104, ___m115, ___m126, ___m137, ___m208, ___m219, ___m2210, ___m2311, ___m3012, ___m3113, ___m3214, ___m3315, method);
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Multiply(Unity.Mathematics.float4x4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_op_Multiply_mEEE7C88A7EF269370E12BA82BDC101D83893FF96 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4x4 operator * (float4x4 lhs, float rhs) { return new float4x4 (lhs.c0 * rhs, lhs.c1 * rhs, lhs.c2 * rhs, lhs.c3 * rhs); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3;
		L_3 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_1, L_2, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_4 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = L_4.get_c1_1();
		float L_6 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_5, L_6, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_8 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = L_8.get_c2_2();
		float L_10 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_9, L_10, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_12 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_c3_3();
		float L_14 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15;
		L_15 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_13, L_14, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_16), L_3, L_7, L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Multiply(System.Single,Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_op_Multiply_mC1B6C82737F3D82CF4A5783D7B504E76CB1CF553 (float ___lhs0, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4x4 operator * (float lhs, float4x4 rhs) { return new float4x4 (lhs * rhs.c0, lhs * rhs.c1, lhs * rhs.c2, lhs * rhs.c3); }
		float L_0 = ___lhs0;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_1 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = L_1.get_c0_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3;
		L_3 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_0, L_2, /*hidden argument*/NULL);
		float L_4 = ___lhs0;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_4, L_6, /*hidden argument*/NULL);
		float L_8 = ___lhs0;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_9 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = L_9.get_c2_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_8, L_10, /*hidden argument*/NULL);
		float L_12 = ___lhs0;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_13 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = L_13.get_c3_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15;
		L_15 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_12, L_14, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_16), L_3, L_7, L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Addition(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_op_Addition_m8ADC44933E779260371FB510153637822E9BDE84 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___lhs0, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4x4 operator + (float4x4 lhs, float4x4 rhs) { return new float4x4 (lhs.c0 + rhs.c0, lhs.c1 + rhs.c1, lhs.c2 + rhs.c2, lhs.c3 + rhs.c3); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_c0_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_1, L_3, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_7 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = L_7.get_c1_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_6, L_8, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_10 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11 = L_10.get_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_12 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_c2_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14;
		L_14 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_11, L_13, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_15 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16 = L_15.get_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_17 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_c3_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		L_19 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_16, L_18, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_20;
		memset((&L_20), 0, sizeof(L_20));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_20), L_4, L_9, L_14, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Subtraction(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_op_Subtraction_mFB25888DC0C2F88D032EB8B09D219CDA84BB1D71 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___lhs0, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4x4 operator - (float4x4 lhs, float4x4 rhs) { return new float4x4 (lhs.c0 - rhs.c0, lhs.c1 - rhs.c1, lhs.c2 - rhs.c2, lhs.c3 - rhs.c3); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_c0_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_1, L_3, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_7 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = L_7.get_c1_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_6, L_8, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_10 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11 = L_10.get_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_12 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_c2_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14;
		L_14 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_11, L_13, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_15 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16 = L_15.get_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_17 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_c3_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		L_19 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_16, L_18, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_20;
		memset((&L_20), 0, sizeof(L_20));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_20), L_4, L_9, L_14, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Division(Unity.Mathematics.float4x4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_op_Division_mD54BB05F0E1C9D03F8D3F9B58EAD537168D92E37 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4x4 operator / (float4x4 lhs, float rhs) { return new float4x4 (lhs.c0 / rhs, lhs.c1 / rhs, lhs.c2 / rhs, lhs.c3 / rhs); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3;
		L_3 = float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline(L_1, L_2, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_4 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = L_4.get_c1_1();
		float L_6 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline(L_5, L_6, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_8 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = L_8.get_c2_2();
		float L_10 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline(L_9, L_10, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_12 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_c3_3();
		float L_14 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15;
		L_15 = float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline(L_13, L_14, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_16), L_3, L_7, L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_UnaryNegation(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_op_UnaryNegation_mD32379B42356BEC1149EA39D841F96A12E8B8786 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___val0, const RuntimeMethod* method)
{
	{
		// public static float4x4 operator - (float4x4 val) { return new float4x4 (-val.c0, -val.c1, -val.c2, -val.c3); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___val0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		L_2 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_1, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_3 = ___val0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = L_3.get_c1_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_4, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_6 = ___val0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_c2_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		L_8 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_7, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_9 = ___val0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = L_9.get_c3_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_10, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.float4& Unity.Mathematics.float4x4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * V_0 = NULL;
	{
		// {
		V_0 = (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *)__this;
		// fixed (float4x4* array = &this) { return ref ((float4*)array)[index]; }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * L_0 = V_0;
		// fixed (float4x4* array = &this) { return ref ((float4*)array)[index]; }
		int32_t L_1 = ___index0;
		uint32_t L_2 = sizeof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 );
		return (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)L_2)))));
	}
}
IL2CPP_EXTERN_C  float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * _returnValue;
	_returnValue = float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.float4x4::Equals(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4x4 rhs) { return c0.Equals(rhs.c0) && c1.Equals(rhs.c1) && c2.Equals(rhs.c2) && c3.Equals(rhs.c3); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = __this->get_address_of_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_1 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = L_1.get_c0_0();
		bool L_3;
		L_3 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_4 = __this->get_address_of_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		bool L_7;
		L_7 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_8 = __this->get_address_of_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_9 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = L_9.get_c2_2();
		bool L_11;
		L_11 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = __this->get_address_of_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_13 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = L_13.get_c3_3();
		bool L_15;
		L_15 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_12, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_004b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_AdjustorThunk (RuntimeObject * __this, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	bool _returnValue;
	_returnValue = float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.float4x4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((float4x4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_inline((float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *)__this, ((*(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *)((float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *)UnBox(L_0, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	bool _returnValue;
	_returnValue = float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.float4x4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = (*(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m16C61605FBE531F8EB703823255407075EA5A464_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float4x4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x, c1.x, c2.x, c3.x, c0.y, c1.y, c2.y, c3.y, c0.z, c1.z, c2.z, c3.z, c0.w, c1.w, c2.w, c3.w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_c0_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_7 = __this->get_address_of_c1_1();
		float L_8 = L_7->get_x_0();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = __this->get_address_of_c2_2();
		float L_13 = L_12->get_x_0();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_17 = __this->get_address_of_c3_3();
		float L_18 = L_17->get_x_0();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = L_16;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_22 = __this->get_address_of_c0_0();
		float L_23 = L_22->get_y_1();
		float L_24 = L_23;
		RuntimeObject * L_25 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_25);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_26 = L_21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_27 = __this->get_address_of_c1_1();
		float L_28 = L_27->get_y_1();
		float L_29 = L_28;
		RuntimeObject * L_30 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_32 = __this->get_address_of_c2_2();
		float L_33 = L_32->get_y_1();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_35);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_35);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_36 = L_31;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_37 = __this->get_address_of_c3_3();
		float L_38 = L_37->get_y_1();
		float L_39 = L_38;
		RuntimeObject * L_40 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_40);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_41 = L_36;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_42 = __this->get_address_of_c0_0();
		float L_43 = L_42->get_z_2();
		float L_44 = L_43;
		RuntimeObject * L_45 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_45);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_46 = L_41;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_47 = __this->get_address_of_c1_1();
		float L_48 = L_47->get_z_2();
		float L_49 = L_48;
		RuntimeObject * L_50 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, L_50);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_50);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_51 = L_46;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_52 = __this->get_address_of_c2_2();
		float L_53 = L_52->get_z_2();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_55);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_55);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_56 = L_51;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_57 = __this->get_address_of_c3_3();
		float L_58 = L_57->get_z_2();
		float L_59 = L_58;
		RuntimeObject * L_60 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_60);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_60);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_61 = L_56;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_62 = __this->get_address_of_c0_0();
		float L_63 = L_62->get_w_3();
		float L_64 = L_63;
		RuntimeObject * L_65 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_65);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_65);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_66 = L_61;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_67 = __this->get_address_of_c1_1();
		float L_68 = L_67->get_w_3();
		float L_69 = L_68;
		RuntimeObject * L_70 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_70);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_70);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_71 = L_66;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_72 = __this->get_address_of_c2_2();
		float L_73 = L_72->get_w_3();
		float L_74 = L_73;
		RuntimeObject * L_75 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_75);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_75);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_76 = L_71;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_77 = __this->get_address_of_c3_3();
		float L_78 = L_77->get_w_3();
		float L_79 = L_78;
		RuntimeObject * L_80 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, L_80);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_80);
		String_t* L_81;
		L_81 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E, L_76, /*hidden argument*/NULL);
		return L_81;
	}
}
IL2CPP_EXTERN_C  String_t* float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.float4x4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x.ToString(format, formatProvider), c1.x.ToString(format, formatProvider), c2.x.ToString(format, formatProvider), c3.x.ToString(format, formatProvider), c0.y.ToString(format, formatProvider), c1.y.ToString(format, formatProvider), c2.y.ToString(format, formatProvider), c3.y.ToString(format, formatProvider), c0.z.ToString(format, formatProvider), c1.z.ToString(format, formatProvider), c2.z.ToString(format, formatProvider), c3.z.ToString(format, formatProvider), c0.w.ToString(format, formatProvider), c1.w.ToString(format, formatProvider), c2.w.ToString(format, formatProvider), c3.w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_c0_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6;
		L_6 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_8 = __this->get_address_of_c1_1();
		float* L_9 = L_8->get_address_of_x_0();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12;
		L_12 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_14 = __this->get_address_of_c2_2();
		float* L_15 = L_14->get_address_of_x_0();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18;
		L_18 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = L_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_20 = __this->get_address_of_c3_3();
		float* L_21 = L_20->get_address_of_x_0();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24;
		L_24 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_25 = L_19;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_26 = __this->get_address_of_c0_0();
		float* L_27 = L_26->get_address_of_y_1();
		String_t* L_28 = ___format0;
		RuntimeObject* L_29 = ___formatProvider1;
		String_t* L_30;
		L_30 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_25;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_32 = __this->get_address_of_c1_1();
		float* L_33 = L_32->get_address_of_y_1();
		String_t* L_34 = ___format0;
		RuntimeObject* L_35 = ___formatProvider1;
		String_t* L_36;
		L_36 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_36);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_36);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_37 = L_31;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_38 = __this->get_address_of_c2_2();
		float* L_39 = L_38->get_address_of_y_1();
		String_t* L_40 = ___format0;
		RuntimeObject* L_41 = ___formatProvider1;
		String_t* L_42;
		L_42 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_39, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_42);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_43 = L_37;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_44 = __this->get_address_of_c3_3();
		float* L_45 = L_44->get_address_of_y_1();
		String_t* L_46 = ___format0;
		RuntimeObject* L_47 = ___formatProvider1;
		String_t* L_48;
		L_48 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_48);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_49 = L_43;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_50 = __this->get_address_of_c0_0();
		float* L_51 = L_50->get_address_of_z_2();
		String_t* L_52 = ___format0;
		RuntimeObject* L_53 = ___formatProvider1;
		String_t* L_54;
		L_54 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_54);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_54);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_55 = L_49;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_56 = __this->get_address_of_c1_1();
		float* L_57 = L_56->get_address_of_z_2();
		String_t* L_58 = ___format0;
		RuntimeObject* L_59 = ___formatProvider1;
		String_t* L_60;
		L_60 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_57, L_58, L_59, /*hidden argument*/NULL);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_60);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_60);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_61 = L_55;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_62 = __this->get_address_of_c2_2();
		float* L_63 = L_62->get_address_of_z_2();
		String_t* L_64 = ___format0;
		RuntimeObject* L_65 = ___formatProvider1;
		String_t* L_66;
		L_66 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_63, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_66);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_66);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_67 = L_61;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_68 = __this->get_address_of_c3_3();
		float* L_69 = L_68->get_address_of_z_2();
		String_t* L_70 = ___format0;
		RuntimeObject* L_71 = ___formatProvider1;
		String_t* L_72;
		L_72 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, L_72);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_72);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_73 = L_67;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_74 = __this->get_address_of_c0_0();
		float* L_75 = L_74->get_address_of_w_3();
		String_t* L_76 = ___format0;
		RuntimeObject* L_77 = ___formatProvider1;
		String_t* L_78;
		L_78 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_75, L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_78);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_78);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_79 = L_73;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_80 = __this->get_address_of_c1_1();
		float* L_81 = L_80->get_address_of_w_3();
		String_t* L_82 = ___format0;
		RuntimeObject* L_83 = ___formatProvider1;
		String_t* L_84;
		L_84 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_81, L_82, L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, L_84);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_84);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_85 = L_79;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_86 = __this->get_address_of_c2_2();
		float* L_87 = L_86->get_address_of_w_3();
		String_t* L_88 = ___format0;
		RuntimeObject* L_89 = ___formatProvider1;
		String_t* L_90;
		L_90 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_87, L_88, L_89, /*hidden argument*/NULL);
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, L_90);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_90);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_91 = L_85;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_92 = __this->get_address_of_c3_3();
		float* L_93 = L_92->get_address_of_w_3();
		String_t* L_94 = ___format0;
		RuntimeObject* L_95 = ___formatProvider1;
		String_t* L_96;
		L_96 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_93, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_96);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_96);
		String_t* L_97;
		L_97 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E, L_91, /*hidden argument*/NULL);
		return L_97;
	}
}
IL2CPP_EXTERN_C  String_t* float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * _thisAdjusted = reinterpret_cast<float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::Scale(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_Scale_mEE04A811A295C99F280056A51ABC83356CB0A4A3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// return float4x4(x,    0.0f, 0.0f, 0.0f,
		//                 0.0f, y,    0.0f, 0.0f,
		//                 0.0f, 0.0f, z,    0.0f,
		//                 0.0f, 0.0f, 0.0f, 1.0f);
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_3;
		L_3 = math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF_inline(L_0, (0.0f), (0.0f), (0.0f), (0.0f), L_1, (0.0f), (0.0f), (0.0f), (0.0f), L_2, (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_3;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::Scale(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_Scale_mD6E98ACC10BB3F6550EC578201C07A89B72CDC74 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___scales0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Scale(scales.x, scales.y, scales.z);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___scales0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___scales0;
		float L_3 = L_2.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___scales0;
		float L_5 = L_4.get_z_2();
		IL2CPP_RUNTIME_CLASS_INIT(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_6;
		L_6 = float4x4_Scale_mEE04A811A295C99F280056A51ABC83356CB0A4A3_inline(L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::TRS(Unity.Mathematics.float3,Unity.Mathematics.quaternion,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_TRS_m2A09A0CF1B7D774D0674577FEA3FBBAD5B25AC92 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___translation0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___rotation1, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___scale2, const RuntimeMethod* method)
{
	float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// float3x3 r = float3x3(rotation);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___rotation1;
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_1;
		L_1 = math_float3x3_m083898B3E74368DC86FB3D19406C3ED60AF63583_inline(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// return float4x4(  float4(r.c0 * scale.x, 0.0f),
		//                   float4(r.c1 * scale.y, 0.0f),
		//                   float4(r.c2 * scale.z, 0.0f),
		//                   float4(translation, 1.0f));
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_2 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = L_2.get_c0_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___scale2;
		float L_5 = L_4.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6;
		L_6 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_3, L_5, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline(L_6, (0.0f), /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_8 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9 = L_8.get_c1_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___scale2;
		float L_11 = L_10.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		L_12 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_9, L_11, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13;
		L_13 = math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline(L_12, (0.0f), /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_14 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_15 = L_14.get_c2_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_16 = ___scale2;
		float L_17 = L_16.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_18;
		L_18 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_15, L_17, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		L_19 = math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline(L_18, (0.0f), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_20 = ___translation0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_21;
		L_21 = math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline(L_20, (1.0f), /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_22;
		L_22 = math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC_inline(L_7, L_13, L_19, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void Unity.Mathematics.float4x4::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4x4__cctor_mBA99BE0167C79FA3EFFB97DBBD42249AB9894684 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly float4x4 identity = new float4x4(1.0f, 0.0f, 0.0f, 0.0f,   0.0f, 1.0f, 0.0f, 0.0f,   0.0f, 0.0f, 1.0f, 0.0f,   0.0f, 0.0f, 0.0f, 1.0f);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0;
		memset((&L_0), 0, sizeof(L_0));
		float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_inline((&L_0), (1.0f), (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_StaticFields*)il2cpp_codegen_static_fields_for(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7_il2cpp_TypeInfo_var))->set_identity_4(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.int2::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1_AdjustorThunk (RuntimeObject * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1_inline(_thisAdjusted, ___x0, ___y1, method);
}
// System.Void Unity.Mathematics.int2::.ctor(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974_AdjustorThunk (RuntimeObject * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974_inline(_thisAdjusted, ___v0, method);
}
// System.Int32 Unity.Mathematics.int2::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * V_0 = NULL;
	{
		// {
		V_0 = (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *)__this;
		// fixed (int2* array = &this) { return ((int*)array)[index]; }
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * L_0 = V_0;
		// fixed (int2* array = &this) { return ((int*)array)[index]; }
		int32_t L_1 = ___index0;
		int32_t L_2 = *((int32_t*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)4)))));
		return L_2;
	}
}
IL2CPP_EXTERN_C  int32_t int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.int2::Equals(Unity.Mathematics.int2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(int2 rhs) { return x == rhs.x && y == rhs.y; }
		int32_t L_0 = __this->get_x_0();
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_1 = ___rhs0;
		int32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_3 = __this->get_y_1();
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_4 = ___rhs0;
		int32_t L_5 = L_4.get_y_1();
		return (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_AdjustorThunk (RuntimeObject * __this, int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	bool _returnValue;
	_returnValue = int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.int2::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((int2)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_inline((int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *)__this, ((*(int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *)((int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *)UnBox(L_0, int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	bool _returnValue;
	_returnValue = int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.int2::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_0 = (*(int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.int2::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int2({0}, {1})", x, y);
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = __this->get_y_1();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_EXTERN_C  String_t* int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.int2::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int2({0}, {1})", x.ToString(format, formatProvider), y.ToString(format, formatProvider));
		int32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_EXTERN_C  String_t* int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * _thisAdjusted = reinterpret_cast<int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.int3::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		int32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_AdjustorThunk (RuntimeObject * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void Unity.Mathematics.int3::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		int32_t L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		int32_t L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		int32_t L_2 = ___v0;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF_AdjustorThunk (RuntimeObject * __this, int32_t ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF_inline(_thisAdjusted, ___v0, method);
}
// System.Void Unity.Mathematics.int3::.ctor(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// this.z = (int)v.z;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(il2cpp_codegen_cast_double_to_int<int32_t>(L_5));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618_AdjustorThunk (RuntimeObject * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618_inline(_thisAdjusted, ___v0, method);
}
// Unity.Mathematics.int3 Unity.Mathematics.int3::op_Addition(Unity.Mathematics.int3,Unity.Mathematics.int3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int3_op_Addition_m08C6B9DB7119923A27914BAFD7DCDEE50CC988A4 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___lhs0, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int3 operator + (int3 lhs, int3 rhs) { return new int3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_12;
		memset((&L_12), 0, sizeof(L_12));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_12), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.int3 Unity.Mathematics.int3::op_Subtraction(Unity.Mathematics.int3,Unity.Mathematics.int3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int3_op_Subtraction_m0AE9F07D1B4603D7CBD3AF567CAAE42CDF0501FC (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___lhs0, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int3 operator - (int3 lhs, int3 rhs) { return new int3 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_12;
		memset((&L_12), 0, sizeof(L_12));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_12), ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 Unity.Mathematics.int3::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * V_0 = NULL;
	{
		// {
		V_0 = (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *)__this;
		// fixed (int3* array = &this) { return ((int*)array)[index]; }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * L_0 = V_0;
		// fixed (int3* array = &this) { return ((int*)array)[index]; }
		int32_t L_1 = ___index0;
		int32_t L_2 = *((int32_t*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)4)))));
		return L_2;
	}
}
IL2CPP_EXTERN_C  int32_t int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.int3::Equals(Unity.Mathematics.int3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(int3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		int32_t L_0 = __this->get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_1 = ___rhs0;
		int32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = __this->get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___rhs0;
		int32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_6 = __this->get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_7 = ___rhs0;
		int32_t L_8 = L_7.get_z_2();
		return (bool)((((int32_t)L_6) == ((int32_t)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_AdjustorThunk (RuntimeObject * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	bool _returnValue;
	_returnValue = int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.int3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((int3)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_inline((int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *)__this, ((*(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *)((int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *)UnBox(L_0, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	bool _returnValue;
	_returnValue = int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.int3::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = (*(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.int3::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int3({0}, {1}, {2})", x, y, z);
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = __this->get_y_1();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_4);
		int32_t L_6 = __this->get_z_2();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C  String_t* int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.int3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int3({0}, {1}, {2})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		int32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		int32_t* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11;
		L_11 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C  String_t* int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * _thisAdjusted = reinterpret_cast<int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.int4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		int32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		int32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_AdjustorThunk (RuntimeObject * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___w3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.int3,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___xyz0, int32_t ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___xyz0;
		int32_t L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___xyz0;
		int32_t L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___xyz0;
		int32_t L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		int32_t L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_AdjustorThunk (RuntimeObject * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___xyz0, int32_t ___w1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline(_thisAdjusted, ___xyz0, ___w1, method);
}
// System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// this.z = (int)v.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(il2cpp_codegen_cast_double_to_int<int32_t>(L_5));
		// this.w = (int)v.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___v0;
		float L_7 = L_6.get_w_3();
		__this->set_w_3(il2cpp_codegen_cast_double_to_int<int32_t>(L_7));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_AdjustorThunk (RuntimeObject * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline(_thisAdjusted, ___v0, method);
}
// Unity.Mathematics.int4 Unity.Mathematics.int4::op_Explicit(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// public static explicit operator int4(float4 v) { return new int4(v); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_1;
		memset((&L_1), 0, sizeof(L_1));
		int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.int4 Unity.Mathematics.int4::op_Multiply(Unity.Mathematics.int4,Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Multiply_m5FC679C570C40C6A3CCF3E600DACD56D48063524 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___lhs0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int4 operator * (int4 lhs, int4 rhs) { return new int4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_12 = ___lhs0;
		int32_t L_13 = L_12.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_14 = ___rhs1;
		int32_t L_15 = L_14.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_16;
		memset((&L_16), 0, sizeof(L_16));
		int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline((&L_16), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.int4 Unity.Mathematics.int4::op_Addition(Unity.Mathematics.int4,Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___lhs0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int4 operator + (int4 lhs, int4 rhs) { return new int4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_12 = ___lhs0;
		int32_t L_13 = L_12.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_14 = ___rhs1;
		int32_t L_15 = L_14.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_16;
		memset((&L_16), 0, sizeof(L_16));
		int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline((&L_16), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.int3 Unity.Mathematics.int4::get_xyz()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	{
		// get { return new int3(x, y, z); }
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = __this->get_y_1();
		int32_t L_2 = __this->get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_3;
		memset((&L_3), 0, sizeof(L_3));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  _returnValue;
	_returnValue = int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.int4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * V_0 = NULL;
	{
		// {
		V_0 = (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)__this;
		// fixed (int4* array = &this) { return ((int*)array)[index]; }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * L_0 = V_0;
		// fixed (int4* array = &this) { return ((int*)array)[index]; }
		int32_t L_1 = ___index0;
		int32_t L_2 = *((int32_t*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_0), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_1), (int32_t)4)))));
		return L_2;
	}
}
IL2CPP_EXTERN_C  int32_t int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC(_thisAdjusted, ___index0, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.int4::set_Item(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	int32_t* V_0 = NULL;
	{
		// {
		int32_t* L_0 = __this->get_address_of_x_0();
		V_0 = (int32_t*)L_0;
		// fixed (int* array = &x) { array[index] = value; }
		int32_t* L_1 = V_0;
		// fixed (int* array = &x) { array[index] = value; }
		int32_t L_2 = ___index0;
		int32_t L_3 = ___value1;
		*((int32_t*)((intptr_t)il2cpp_codegen_add((intptr_t)((uintptr_t)L_1), (intptr_t)((intptr_t)il2cpp_codegen_multiply((intptr_t)((intptr_t)L_2), (int32_t)4))))) = (int32_t)L_3;
		V_0 = (int32_t*)((uintptr_t)0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5(_thisAdjusted, ___index0, ___value1, method);
}
// System.Boolean Unity.Mathematics.int4::Equals(Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(int4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		int32_t L_0 = __this->get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_1 = ___rhs0;
		int32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = __this->get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___rhs0;
		int32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_6 = __this->get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_7 = ___rhs0;
		int32_t L_8 = L_7.get_z_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_9 = __this->get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_10 = ___rhs0;
		int32_t L_11 = L_10.get_w_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_AdjustorThunk (RuntimeObject * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	bool _returnValue;
	_returnValue = int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.int4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((int4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_inline((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)__this, ((*(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)UnBox(L_0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	bool _returnValue;
	_returnValue = int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.int4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = (*(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)__this);
		uint32_t L_1;
		L_1 = math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.int4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int4({0}, {1}, {2}, {3})", x, y, z, w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		int32_t L_2 = __this->get_x_0();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_1;
		int32_t L_6 = __this->get_y_1();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = L_5;
		int32_t L_10 = __this->get_z_2();
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_9;
		int32_t L_14 = __this->get_w_3();
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17;
		L_17 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C  String_t* int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.int4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int4({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		int32_t* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5;
		L_5 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		int32_t* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10;
		L_10 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		int32_t* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15;
		L_15 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		int32_t* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20;
		L_20 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C  String_t* int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * _thisAdjusted = reinterpret_cast<int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint2(0xFA3A3285u, 0xAD55999Du)) + 0xDCDD5341u;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___v0;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1;
		L_1 = math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863_inline(L_0, /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		L_2 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(((int32_t)-96849275), ((int32_t)-1386899043), /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_3;
		L_3 = uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-589475007)));
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::float3(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// public static float3 float3(float x, float y, float z) { return new float3(x, y, z); }
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint3(0x9B13B92Du, 0x4ABF0813u, 0x86068063u)) + 0xD75513F9u;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___v0;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1;
		L_1 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_0, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-1693206227), ((int32_t)1254033427), ((int32_t)-2046394269), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		L_3 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-682290183)));
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3x3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855 (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v.c0) * uint3(0x713BD06Fu, 0x753AD6ADu, 0xD19764C7u) +
		//             asuint(v.c1) * uint3(0xB5D0BF63u, 0xF9102C5Fu, 0x9881FB9Fu) +
		//             asuint(v.c2) * uint3(0x56A1530Du, 0x804B722Du, 0x738E50E5u)) + 0x4FC93C25u;
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = ___v0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = L_0.get_c0_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_1, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		L_3 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)1899745391), ((int32_t)1966790317), ((int32_t)-778607417), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4;
		L_4 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_2, L_3, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_5 = ___v0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = L_5.get_c1_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_7;
		L_7 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_6, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8;
		L_8 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-1244610717), ((int32_t)-116380577), ((int32_t)-1736311905), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_9;
		L_9 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_7, L_8, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10;
		L_10 = uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3_inline(L_4, L_9, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_11 = ___v0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12 = L_11.get_c2_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_13;
		L_13 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_12, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_14;
		L_14 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)1453413133), ((int32_t)-2142539219), ((int32_t)1938706661), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_15;
		L_15 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_13, L_14, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_16;
		L_16 = uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3_inline(L_10, L_15, /*hidden argument*/NULL);
		uint32_t L_17;
		L_17 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_16, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)((int32_t)1338588197)));
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::float4(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6 (float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// public static float4 float4(float x, float y, float z, float w) { return new float4(x, y, z, w); }
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		float L_3 = ___w3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::float4(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	{
		// public static float4 float4(float3 xyz, float w) { return new float4(xyz, w); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___xyz0;
		float L_1 = ___w1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::float4(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_m6D1066B47EE7BC7A47A136D969B5A3CA41902224 (float ___v0, const RuntimeMethod* method)
{
	{
		// public static float4 float4(float v) { return new float4(v); }
		float L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint4(0xE69626FFu, 0xBD010EEBu, 0x9CEDE1D1u, 0x43BE0B51u)) + 0xAF836EE1u;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_0, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-426367233), ((int32_t)-1124004117), ((int32_t)-1662131759), ((int32_t)1136528209), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3;
		L_3 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-1350340895)));
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::shuffle(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, uint8_t ___x2, uint8_t ___y3, uint8_t ___z4, uint8_t ___w5, const RuntimeMethod* method)
{
	{
		// return float4(
		//     select_shuffle_component(a, b, x),
		//     select_shuffle_component(a, b, y),
		//     select_shuffle_component(a, b, z),
		//     select_shuffle_component(a, b, w));
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___b1;
		uint8_t L_2 = ___x2;
		float L_3;
		L_3 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_0, L_1, L_2, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___b1;
		uint8_t L_6 = ___y3;
		float L_7;
		L_7 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_4, L_5, L_6, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___b1;
		uint8_t L_10 = ___z4;
		float L_11;
		L_11 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_8, L_9, L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = ___b1;
		uint8_t L_14 = ___w5;
		float L_15;
		L_15 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_12, L_13, L_14, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		L_16 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline(L_3, L_7, L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Single Unity.Mathematics.math::select_shuffle_component(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.math/ShuffleComponent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, uint8_t ___component2, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___component2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_002f;
			}
			case 2:
			{
				goto IL_0036;
			}
			case 3:
			{
				goto IL_003d;
			}
			case 4:
			{
				goto IL_0044;
			}
			case 5:
			{
				goto IL_004b;
			}
			case 6:
			{
				goto IL_0052;
			}
			case 7:
			{
				goto IL_0059;
			}
		}
	}
	{
		goto IL_0060;
	}

IL_0028:
	{
		// return a.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___a0;
		float L_2 = L_1.get_x_0();
		return L_2;
	}

IL_002f:
	{
		// return a.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		return L_4;
	}

IL_0036:
	{
		// return a.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___a0;
		float L_6 = L_5.get_z_2();
		return L_6;
	}

IL_003d:
	{
		// return a.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___a0;
		float L_8 = L_7.get_w_3();
		return L_8;
	}

IL_0044:
	{
		// return b.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___b1;
		float L_10 = L_9.get_x_0();
		return L_10;
	}

IL_004b:
	{
		// return b.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11 = ___b1;
		float L_12 = L_11.get_y_1();
		return L_12;
	}

IL_0052:
	{
		// return b.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = ___b1;
		float L_14 = L_13.get_z_2();
		return L_14;
	}

IL_0059:
	{
		// return b.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = ___b1;
		float L_16 = L_15.get_w_3();
		return L_16;
	}

IL_0060:
	{
		// throw new System.ArgumentException("Invalid shuffle component: " + component);
		RuntimeObject * L_17 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ShuffleComponent_t6796DE67FD5D2B89865C06815789C5F33EB1D07A_il2cpp_TypeInfo_var)), (&___component2));
		NullCheck(L_17);
		String_t* L_18;
		L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		___component2 = *(uint8_t*)UnBox(L_17);
		String_t* L_19;
		L_19 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral579BEF9273F6F30843464D2DA589060200686F6A)), L_18, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_20 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_RuntimeMethod_var)));
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method)
{
	{
		// public static float4x4 float4x4(float4 c0, float4 c1, float4 c2, float4 c3) { return new float4x4(c0, c1, c2, c3); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___c00;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___c11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___c22;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___c33;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF (float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	{
		// return new float4x4(m00, m01, m02, m03,
		//                     m10, m11, m12, m13,
		//                     m20, m21, m22, m23,
		//                     m30, m31, m32, m33);
		float L_0 = ___m000;
		float L_1 = ___m011;
		float L_2 = ___m022;
		float L_3 = ___m033;
		float L_4 = ___m104;
		float L_5 = ___m115;
		float L_6 = ___m126;
		float L_7 = ___m137;
		float L_8 = ___m208;
		float L_9 = ___m219;
		float L_10 = ___m2210;
		float L_11 = ___m2311;
		float L_12 = ___m3012;
		float L_13 = ___m3113;
		float L_14 = ___m3214;
		float L_15 = ___m3315;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_inline((&L_16), L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::transpose(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_transpose_m76E580466C3B5A7D1E0A1534C8E17BD0176F51A0 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___v0, const RuntimeMethod* method)
{
	{
		// return float4x4(
		//     v.c0.x, v.c0.y, v.c0.z, v.c0.w,
		//     v.c1.x, v.c1.y, v.c1.z, v.c1.w,
		//     v.c2.x, v.c2.y, v.c2.z, v.c2.w,
		//     v.c3.x, v.c3.y, v.c3.z, v.c3.w);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float L_2 = L_1.get_x_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_3 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = L_3.get_c0_0();
		float L_5 = L_4.get_y_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_6 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_c0_0();
		float L_8 = L_7.get_z_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_9 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = L_9.get_c0_0();
		float L_11 = L_10.get_w_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_12 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_c1_1();
		float L_14 = L_13.get_x_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_15 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16 = L_15.get_c1_1();
		float L_17 = L_16.get_y_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_18 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19 = L_18.get_c1_1();
		float L_20 = L_19.get_z_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_21 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_22 = L_21.get_c1_1();
		float L_23 = L_22.get_w_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_24 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_25 = L_24.get_c2_2();
		float L_26 = L_25.get_x_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_27 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_28 = L_27.get_c2_2();
		float L_29 = L_28.get_y_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_30 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_31 = L_30.get_c2_2();
		float L_32 = L_31.get_z_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_33 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_34 = L_33.get_c2_2();
		float L_35 = L_34.get_w_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_36 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_37 = L_36.get_c3_3();
		float L_38 = L_37.get_x_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_39 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_40 = L_39.get_c3_3();
		float L_41 = L_40.get_y_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_42 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_43 = L_42.get_c3_3();
		float L_44 = L_43.get_z_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_45 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_46 = L_45.get_c3_3();
		float L_47 = L_46.get_w_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_48;
		L_48 = math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF_inline(L_2, L_5, L_8, L_11, L_14, L_17, L_20, L_23, L_26, L_29, L_32, L_35, L_38, L_41, L_44, L_47, /*hidden argument*/NULL);
		return L_48;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::inverse(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_inverse_m26EB769712BD20C07185B9ECAC5808E06DDC7BF3 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___m0, const RuntimeMethod* method)
{
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_2;
	memset((&V_2), 0, sizeof(V_2));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_4;
	memset((&V_4), 0, sizeof(V_4));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_5;
	memset((&V_5), 0, sizeof(V_5));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_6;
	memset((&V_6), 0, sizeof(V_6));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_7;
	memset((&V_7), 0, sizeof(V_7));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_8;
	memset((&V_8), 0, sizeof(V_8));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_9;
	memset((&V_9), 0, sizeof(V_9));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_10;
	memset((&V_10), 0, sizeof(V_10));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_11;
	memset((&V_11), 0, sizeof(V_11));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_12;
	memset((&V_12), 0, sizeof(V_12));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_13;
	memset((&V_13), 0, sizeof(V_13));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_14;
	memset((&V_14), 0, sizeof(V_14));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_15;
	memset((&V_15), 0, sizeof(V_15));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_16;
	memset((&V_16), 0, sizeof(V_16));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_17;
	memset((&V_17), 0, sizeof(V_17));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_18;
	memset((&V_18), 0, sizeof(V_18));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_19;
	memset((&V_19), 0, sizeof(V_19));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_20;
	memset((&V_20), 0, sizeof(V_20));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_21;
	memset((&V_21), 0, sizeof(V_21));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_22;
	memset((&V_22), 0, sizeof(V_22));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_23;
	memset((&V_23), 0, sizeof(V_23));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_24;
	memset((&V_24), 0, sizeof(V_24));
	float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  V_25;
	memset((&V_25), 0, sizeof(V_25));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_26;
	memset((&V_26), 0, sizeof(V_26));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_27;
	memset((&V_27), 0, sizeof(V_27));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_28;
	memset((&V_28), 0, sizeof(V_28));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_29;
	memset((&V_29), 0, sizeof(V_29));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_30;
	memset((&V_30), 0, sizeof(V_30));
	{
		// float4 c0 = m.c0;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___m0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		V_0 = L_1;
		// float4 c1 = m.c1;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_2 = ___m0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_c1_1();
		V_1 = L_3;
		// float4 c2 = m.c2;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_4 = ___m0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = L_4.get_c2_2();
		V_2 = L_5;
		// float4 c3 = m.c3;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_6 = ___m0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_c3_3();
		V_3 = L_7;
		// float4 r0y_r1y_r0x_r1x = movelh(c1, c0);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = V_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10;
		L_10 = math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C_inline(L_8, L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		// float4 r0z_r1z_r0w_r1w = movelh(c2, c3);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11 = V_2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = V_3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13;
		L_13 = math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C_inline(L_11, L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		// float4 r2y_r3y_r2x_r3x = movehl(c0, c1);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = V_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		L_16 = math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553_inline(L_14, L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		// float4 r2z_r3z_r2w_r3w = movehl(c3, c2);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17 = V_3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = V_2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		L_19 = math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553_inline(L_17, L_18, /*hidden argument*/NULL);
		V_7 = L_19;
		// float4 r1y_r2y_r1x_r2x = shuffle(c1, c0, ShuffleComponent.LeftY, ShuffleComponent.LeftZ, ShuffleComponent.RightY, ShuffleComponent.RightZ);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20 = V_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_21 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_22;
		L_22 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_20, L_21, 1, 2, 5, 6, /*hidden argument*/NULL);
		// float4 r1z_r2z_r1w_r2w = shuffle(c2, c3, ShuffleComponent.LeftY, ShuffleComponent.LeftZ, ShuffleComponent.RightY, ShuffleComponent.RightZ);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_23 = V_2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_24 = V_3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_25;
		L_25 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_23, L_24, 1, 2, 5, 6, /*hidden argument*/NULL);
		V_8 = L_25;
		// float4 r3y_r0y_r3x_r0x = shuffle(c1, c0, ShuffleComponent.LeftW, ShuffleComponent.LeftX, ShuffleComponent.RightW, ShuffleComponent.RightX);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_26 = V_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_27 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_28;
		L_28 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_26, L_27, 3, 0, 7, 4, /*hidden argument*/NULL);
		V_9 = L_28;
		// float4 r3z_r0z_r3w_r0w = shuffle(c2, c3, ShuffleComponent.LeftW, ShuffleComponent.LeftX, ShuffleComponent.RightW, ShuffleComponent.RightX);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_29 = V_2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_30 = V_3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_31;
		L_31 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_29, L_30, 3, 0, 7, 4, /*hidden argument*/NULL);
		V_10 = L_31;
		// float4 r0_wzyx = shuffle(r0z_r1z_r0w_r1w, r0y_r1y_r0x_r1x, ShuffleComponent.LeftZ, ShuffleComponent.LeftX, ShuffleComponent.RightX, ShuffleComponent.RightZ);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_32 = V_5;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_33 = V_4;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_34;
		L_34 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_32, L_33, 2, 0, 4, 6, /*hidden argument*/NULL);
		V_11 = L_34;
		// float4 r1_wzyx = shuffle(r0z_r1z_r0w_r1w, r0y_r1y_r0x_r1x, ShuffleComponent.LeftW, ShuffleComponent.LeftY, ShuffleComponent.RightY, ShuffleComponent.RightW);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_35 = V_5;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_36 = V_4;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_37;
		L_37 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_35, L_36, 3, 1, 5, 7, /*hidden argument*/NULL);
		V_12 = L_37;
		// float4 r2_wzyx = shuffle(r2z_r3z_r2w_r3w, r2y_r3y_r2x_r3x, ShuffleComponent.LeftZ, ShuffleComponent.LeftX, ShuffleComponent.RightX, ShuffleComponent.RightZ);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_38 = V_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_39 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_40;
		L_40 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_38, L_39, 2, 0, 4, 6, /*hidden argument*/NULL);
		V_13 = L_40;
		// float4 r3_wzyx = shuffle(r2z_r3z_r2w_r3w, r2y_r3y_r2x_r3x, ShuffleComponent.LeftW, ShuffleComponent.LeftY, ShuffleComponent.RightY, ShuffleComponent.RightW);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_41 = V_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_42 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_43;
		L_43 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_41, L_42, 3, 1, 5, 7, /*hidden argument*/NULL);
		V_14 = L_43;
		// float4 r0_xyzw = shuffle(r0y_r1y_r0x_r1x, r0z_r1z_r0w_r1w, ShuffleComponent.LeftZ, ShuffleComponent.LeftX, ShuffleComponent.RightX, ShuffleComponent.RightZ);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_44 = V_4;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_45 = V_5;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_46;
		L_46 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_44, L_45, 2, 0, 4, 6, /*hidden argument*/NULL);
		V_15 = L_46;
		// float4 inner12_23 = r1y_r2y_r1x_r2x * r2z_r3z_r2w_r3w - r1z_r2z_r1w_r2w * r2y_r3y_r2x_r3x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_47 = V_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_48;
		L_48 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_22, L_47, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_49 = V_8;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_50 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_51;
		L_51 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_49, L_50, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_52;
		L_52 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_48, L_51, /*hidden argument*/NULL);
		// float4 inner02_13 = r0y_r1y_r0x_r1x * r2z_r3z_r2w_r3w - r0z_r1z_r0w_r1w * r2y_r3y_r2x_r3x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_53 = V_4;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_54 = V_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_55;
		L_55 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_53, L_54, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_56 = V_5;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_57 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_58;
		L_58 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_56, L_57, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_59;
		L_59 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_55, L_58, /*hidden argument*/NULL);
		V_16 = L_59;
		// float4 inner30_01 = r3z_r0z_r3w_r0w * r0y_r1y_r0x_r1x - r3y_r0y_r3x_r0x * r0z_r1z_r0w_r1w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_60 = V_10;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_61 = V_4;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_62;
		L_62 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_60, L_61, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_63 = V_9;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_64 = V_5;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_65;
		L_65 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_63, L_64, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_66;
		L_66 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_62, L_65, /*hidden argument*/NULL);
		V_17 = L_66;
		// float4 inner12 = shuffle(inner12_23, inner12_23, ShuffleComponent.LeftX, ShuffleComponent.LeftZ, ShuffleComponent.RightZ, ShuffleComponent.RightX);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_67 = L_52;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_68 = L_67;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_69;
		L_69 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_68, L_68, 0, 2, 6, 4, /*hidden argument*/NULL);
		V_18 = L_69;
		// float4 inner23 = shuffle(inner12_23, inner12_23, ShuffleComponent.LeftY, ShuffleComponent.LeftW, ShuffleComponent.RightW, ShuffleComponent.RightY);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_70 = L_67;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_71;
		L_71 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_70, L_70, 1, 3, 7, 5, /*hidden argument*/NULL);
		V_19 = L_71;
		// float4 inner02 = shuffle(inner02_13, inner02_13, ShuffleComponent.LeftX, ShuffleComponent.LeftZ, ShuffleComponent.RightZ, ShuffleComponent.RightX);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_72 = V_16;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_73 = V_16;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_74;
		L_74 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_72, L_73, 0, 2, 6, 4, /*hidden argument*/NULL);
		V_20 = L_74;
		// float4 inner13 = shuffle(inner02_13, inner02_13, ShuffleComponent.LeftY, ShuffleComponent.LeftW, ShuffleComponent.RightW, ShuffleComponent.RightY);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_75 = V_16;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_76 = V_16;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_77;
		L_77 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_75, L_76, 1, 3, 7, 5, /*hidden argument*/NULL);
		V_21 = L_77;
		// float4 minors0 = r3_wzyx * inner12 - r2_wzyx * inner13 + r1_wzyx * inner23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_78 = V_14;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_79 = V_18;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_80;
		L_80 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_78, L_79, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_81 = V_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_82 = V_21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_83;
		L_83 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_81, L_82, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_84;
		L_84 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_80, L_83, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_85 = V_12;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_86 = V_19;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_87;
		L_87 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_85, L_86, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_88;
		L_88 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_84, L_87, /*hidden argument*/NULL);
		V_22 = L_88;
		// float4 denom = r0_xyzw * minors0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_89 = V_15;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_90 = V_22;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_91;
		L_91 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_89, L_90, /*hidden argument*/NULL);
		V_23 = L_91;
		// denom = denom + shuffle(denom, denom, ShuffleComponent.LeftY, ShuffleComponent.LeftX, ShuffleComponent.RightW, ShuffleComponent.RightZ);   // x+y        x+y            z+w            z+w
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_92 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_93 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_94 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_95;
		L_95 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_93, L_94, 1, 0, 7, 6, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_96;
		L_96 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_92, L_95, /*hidden argument*/NULL);
		V_23 = L_96;
		// denom = denom - shuffle(denom, denom, ShuffleComponent.LeftZ, ShuffleComponent.LeftZ, ShuffleComponent.RightX, ShuffleComponent.RightX);   // x+y-z-w  x+y-z-w        z+w-x-y        z+w-x-y
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_97 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_98 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_99 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_100;
		L_100 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_98, L_99, 2, 2, 4, 4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_101;
		L_101 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_97, L_100, /*hidden argument*/NULL);
		V_23 = L_101;
		// float4 rcp_denom_ppnn = float4(1.0f) / denom;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_102;
		L_102 = math_float4_m6D1066B47EE7BC7A47A136D969B5A3CA41902224_inline((1.0f), /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_103 = V_23;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_104;
		L_104 = float4_op_Division_m489385B46636C7B5CCE01DEECA0A603A2EE807AA_inline(L_102, L_103, /*hidden argument*/NULL);
		V_24 = L_104;
		// res.c0 = minors0 * rcp_denom_ppnn;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_105 = V_22;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_106 = V_24;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_107;
		L_107 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_105, L_106, /*hidden argument*/NULL);
		(&V_25)->set_c0_0(L_107);
		// float4 inner30 = shuffle(inner30_01, inner30_01, ShuffleComponent.LeftX, ShuffleComponent.LeftZ, ShuffleComponent.RightZ, ShuffleComponent.RightX);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_108 = V_17;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_109 = V_17;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_110;
		L_110 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_108, L_109, 0, 2, 6, 4, /*hidden argument*/NULL);
		V_26 = L_110;
		// float4 inner01 = shuffle(inner30_01, inner30_01, ShuffleComponent.LeftY, ShuffleComponent.LeftW, ShuffleComponent.RightW, ShuffleComponent.RightY);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_111 = V_17;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_112 = V_17;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_113;
		L_113 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_111, L_112, 1, 3, 7, 5, /*hidden argument*/NULL);
		V_27 = L_113;
		// float4 minors1 = r2_wzyx * inner30 - r0_wzyx * inner23 - r3_wzyx * inner02;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_114 = V_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_115 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_116;
		L_116 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_114, L_115, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_117 = V_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_118 = V_19;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_119;
		L_119 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_117, L_118, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_120;
		L_120 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_116, L_119, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_121 = V_14;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_122 = V_20;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_123;
		L_123 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_121, L_122, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_124;
		L_124 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_120, L_123, /*hidden argument*/NULL);
		V_28 = L_124;
		// res.c1 = minors1 * rcp_denom_ppnn;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_125 = V_28;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_126 = V_24;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_127;
		L_127 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_125, L_126, /*hidden argument*/NULL);
		(&V_25)->set_c1_1(L_127);
		// float4 minors2 = r0_wzyx * inner13 - r1_wzyx * inner30 - r3_wzyx * inner01;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_128 = V_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_129 = V_21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_130;
		L_130 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_128, L_129, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_131 = V_12;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_132 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_133;
		L_133 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_131, L_132, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_134;
		L_134 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_130, L_133, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_135 = V_14;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_136 = V_27;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_137;
		L_137 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_135, L_136, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_138;
		L_138 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_134, L_137, /*hidden argument*/NULL);
		V_29 = L_138;
		// res.c2 = minors2 * rcp_denom_ppnn;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_139 = V_29;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_140 = V_24;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_141;
		L_141 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_139, L_140, /*hidden argument*/NULL);
		(&V_25)->set_c2_2(L_141);
		// float4 minors3 = r1_wzyx * inner02 - r0_wzyx * inner12 + r2_wzyx * inner01;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_142 = V_12;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_143 = V_20;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_144;
		L_144 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_142, L_143, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_145 = V_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_146 = V_18;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_147;
		L_147 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_145, L_146, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_148;
		L_148 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_144, L_147, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_149 = V_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_150 = V_27;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_151;
		L_151 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_149, L_150, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_152;
		L_152 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_148, L_151, /*hidden argument*/NULL);
		V_30 = L_152;
		// res.c3 = minors3 * rcp_denom_ppnn;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_153 = V_30;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_154 = V_24;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_155;
		L_155 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_153, L_154, /*hidden argument*/NULL);
		(&V_25)->set_c3_3(L_155);
		// return res;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_156 = V_25;
		return L_156;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m16C61605FBE531F8EB703823255407075EA5A464 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v.c0) * uint4(0xC4B1493Fu, 0xBA0966D3u, 0xAFBEE253u, 0x5B419C01u) +
		//             asuint(v.c1) * uint4(0x515D90F5u, 0xEC9F68F3u, 0xF9EA92D5u, 0xC2FAFCB9u) +
		//             asuint(v.c2) * uint4(0x616E9CA1u, 0xC5C5394Bu, 0xCAE78587u, 0x7A1541C9u) +
		//             asuint(v.c3) * uint4(0xF83BD927u, 0x6A243BCBu, 0x509B84C9u, 0x91D13847u)) + 0x52F7230Fu;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_1, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3;
		L_3 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-995014337), ((int32_t)-1173788973), ((int32_t)-1346444717), ((int32_t)1531026433), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4;
		L_4 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_2, L_3, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_7;
		L_7 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_6, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8;
		L_8 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)1365086453), ((int32_t)-325097229), ((int32_t)-102067499), ((int32_t)-1023738695), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_9;
		L_9 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_7, L_8, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10;
		L_10 = uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_11 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = L_11.get_c2_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_13;
		L_13 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_12, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_14;
		L_14 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)1634639009), ((int32_t)-976930485), ((int32_t)-890796665), ((int32_t)2048213449), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_15;
		L_15 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_13, L_14, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_16;
		L_16 = uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_17 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_c3_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_19;
		L_19 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_18, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_20;
		L_20 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-130295513), ((int32_t)1780759499), ((int32_t)1352369353), ((int32_t)-1848559545), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_21;
		L_21 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_19, L_20, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_22;
		L_22 = uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline(L_16, L_21, /*hidden argument*/NULL);
		uint32_t L_23;
		L_23 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_22, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)((int32_t)1391928079)));
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6 (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint2(0x83B58237u, 0x833E3E29u)) + 0xA9D919BFu;
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_0 = ___v0;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1;
		L_1 = math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B_inline(L_0, /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		L_2 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(((int32_t)-2085256649), ((int32_t)-2093072855), /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_3;
		L_3 = uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-1445389889)));
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint3(0x4C7F6DD1u, 0x4822A3E9u, 0xAAC3C25Du)) + 0xD21D0945u;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___v0;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1;
		L_1 = math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B_inline(L_0, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)1283419601), ((int32_t)1210229737), ((int32_t)-1430011299), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		L_3 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-769849019)));
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint4(0x6E050B01u, 0x750FDBF5u, 0x7F3DD499u, 0x52EAAEBBu)) + 0x4599C793u;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___v0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D_inline(L_0, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)1845824257), ((int32_t)1963973621), ((int32_t)2134758553), ((int32_t)1391111867), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3;
		L_3 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)1167706003)));
	}
}
// System.Int32 Unity.Mathematics.math::asint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86 (float ___x0, const RuntimeMethod* method)
{
	IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// u.intValue = 0;
		(&V_0)->set_intValue_0(0);
		// u.floatValue = x;
		float L_0 = ___x0;
		(&V_0)->set_floatValue_1(L_0);
		// return u.intValue;
		IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  L_1 = V_0;
		int32_t L_2 = L_1.get_intValue_0();
		return L_2;
	}
}
// Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.int2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint2 asuint(int2 x) { return uint2((uint)x.x, (uint)x.y); }
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_2 = ___x0;
		int32_t L_3 = L_2.get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_4;
		L_4 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.int3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint3 asuint(int3 x) { return uint3((uint)x.x, (uint)x.y, (uint)x.z); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___x0;
		int32_t L_3 = L_2.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___x0;
		int32_t L_5 = L_4.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6;
		L_6 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint4 asuint(int4 x) { return uint4((uint)x.x, (uint)x.y, (uint)x.z, (uint)x.w); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___x0;
		int32_t L_3 = L_2.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___x0;
		int32_t L_5 = L_4.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___x0;
		int32_t L_7 = L_6.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8;
		L_8 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.UInt32 Unity.Mathematics.math::asuint(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC (float ___x0, const RuntimeMethod* method)
{
	{
		// public static uint asuint(float x) { return (uint)asint(x); }
		float L_0 = ___x0;
		int32_t L_1;
		L_1 = math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint2 asuint(float2 x) { return uint2(asuint(x.x), asuint(x.y)); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2;
		L_2 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_1, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5;
		L_5 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_4, /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_6;
		L_6 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint3 asuint(float3 x) { return uint3(asuint(x.x), asuint(x.y), asuint(x.z)); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2;
		L_2 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_1, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5;
		L_5 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_4, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8;
		L_8 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_7, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_9;
		L_9 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint4 asuint(float4 x) { return uint4(asuint(x.x), asuint(x.y), asuint(x.z), asuint(x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2;
		L_2 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_1, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5;
		L_5 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8;
		L_8 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		uint32_t L_11;
		L_11 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_10, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12;
		L_12 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single Unity.Mathematics.math::asfloat(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181 (int32_t ___x0, const RuntimeMethod* method)
{
	IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// u.floatValue = 0;
		(&V_0)->set_floatValue_1((0.0f));
		// u.intValue = x;
		int32_t L_0 = ___x0;
		(&V_0)->set_intValue_0(L_0);
		// return u.floatValue;
		IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  L_1 = V_0;
		float L_2 = L_1.get_floatValue_1();
		return L_2;
	}
}
// System.Single Unity.Mathematics.math::asfloat(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF (uint32_t ___x0, const RuntimeMethod* method)
{
	{
		// public static float  asfloat(uint x) { return asfloat((int)x); }
		uint32_t L_0 = ___x0;
		float L_1;
		L_1 = math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::asfloat(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___x0, const RuntimeMethod* method)
{
	{
		// public static float3 asfloat(uint3 x) { return float3(asfloat(x.x), asfloat(x.y), asfloat(x.z)); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_1, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_4, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_7, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		L_9 = math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline(L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::asfloat(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_asfloat_mD9B4C5BBB6AC641027D94C2045C4C3AF416DFF6D (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 asfloat(uint4 x) { return float4(asfloat(x.x), asfloat(x.y), asfloat(x.z), asfloat(x.w)); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_1, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_4, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_7, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_9 = ___x0;
		uint32_t L_10 = L_9.get_w_3();
		float L_11;
		L_11 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		L_12 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Int32 Unity.Mathematics.math::min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static int min(int x, int y) { return x < y ? x : y; }
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int32_t L_3 = ___x0;
		return L_3;
	}
}
// Unity.Mathematics.int3 Unity.Mathematics.math::min(Unity.Mathematics.int3,Unity.Mathematics.int3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  math_min_m661F28A20439E739FAB5DA0FF11854BEAF7EC951 (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___x0, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___y1, const RuntimeMethod* method)
{
	{
		// public static int3 min(int3 x, int3 y) { return new int3(min(x.x, y.x), min(x.y, y.y), min(x.z, y.z)); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___y1;
		int32_t L_3 = L_2.get_x_0();
		int32_t L_4;
		L_4 = math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A_inline(L_1, L_3, /*hidden argument*/NULL);
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_5 = ___x0;
		int32_t L_6 = L_5.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_7 = ___y1;
		int32_t L_8 = L_7.get_y_1();
		int32_t L_9;
		L_9 = math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A_inline(L_6, L_8, /*hidden argument*/NULL);
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_10 = ___x0;
		int32_t L_11 = L_10.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_12 = ___y1;
		int32_t L_13 = L_12.get_z_2();
		int32_t L_14;
		L_14 = math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A_inline(L_11, L_13, /*hidden argument*/NULL);
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_15;
		memset((&L_15), 0, sizeof(L_15));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// System.Int64 Unity.Mathematics.math::min(System.Int64,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t math_min_m65ACBB9005609DE8184015662C758B23A85C794E (int64_t ___x0, int64_t ___y1, const RuntimeMethod* method)
{
	{
		// public static long min(long x, long y) { return x < y ? x : y; }
		int64_t L_0 = ___x0;
		int64_t L_1 = ___y1;
		if ((((int64_t)L_0) < ((int64_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int64_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int64_t L_3 = ___x0;
		return L_3;
	}
}
// System.Single Unity.Mathematics.math::min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89 (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float min(float x, float y) { return float.IsNaN(y) || x < y ? x : y; }
		float L_0 = ___y1;
		bool L_1;
		L_1 = Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) < ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.math::min(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  math_min_mDCA05EE5BF99F3753F8AF682B3C5B85C4620C60C (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___y1, const RuntimeMethod* method)
{
	{
		// public static float2 min(float2 x, float2 y) { return new float2(min(x.x, y.x), min(x.y, y.y)); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_3, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_6, L_8, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_10;
		memset((&L_10), 0, sizeof(L_10));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_10), L_4, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::min(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_min_m6BF29A95DEDC36F4E386F4C02122671FCC271BC8 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	{
		// public static float3 min(float3 x, float3 y) { return new float3(min(x.x, y.x), min(x.y, y.y), min(x.z, y.z)); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_3, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_6, L_8, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14;
		L_14 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_11, L_13, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_15;
		memset((&L_15), 0, sizeof(L_15));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::min(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_min_mC45C7024F6850EB1D382A2B2F6F31CCA4B2750E6 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float4 min(float4 x, float4 y) { return new float4(min(x.x, y.x), min(x.y, y.y), min(x.z, y.z), min(x.w, y.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14;
		L_14 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_11, L_13, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = ___x0;
		float L_16 = L_15.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17 = ___y1;
		float L_18 = L_17.get_w_3();
		float L_19;
		L_19 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_16, L_18, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20;
		memset((&L_20), 0, sizeof(L_20));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_20), L_4, L_9, L_14, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Int32 Unity.Mathematics.math::max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static int max(int x, int y) { return x > y ? x : y; }
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int32_t L_3 = ___x0;
		return L_3;
	}
}
// System.Int64 Unity.Mathematics.math::max(System.Int64,System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t math_max_mAED5DA82BD5495A15A2365FCE7CA4B775F6E1132 (int64_t ___x0, int64_t ___y1, const RuntimeMethod* method)
{
	{
		// public static long max(long x, long y) { return x > y ? x : y; }
		int64_t L_0 = ___x0;
		int64_t L_1 = ___y1;
		if ((((int64_t)L_0) > ((int64_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int64_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int64_t L_3 = ___x0;
		return L_3;
	}
}
// System.Single Unity.Mathematics.math::max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_max_mD8541933650D81292540BAFF46DE531FA1B333FC (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float max(float x, float y) { return float.IsNaN(y) || x > y ? x : y; }
		float L_0 = ___y1;
		bool L_1;
		L_1 = Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) > ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.math::max(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  math_max_m3082575385F8F5B755FF5D4399B1CD43A17550EA (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___y1, const RuntimeMethod* method)
{
	{
		// public static float2 max(float2 x, float2 y) { return new float2(max(x.x, y.x), max(x.y, y.y)); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_6, L_8, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_10;
		memset((&L_10), 0, sizeof(L_10));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_10), L_4, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::max(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_max_m7E3D48AE84E0E23DD089176B4C0CD6886303ABCF (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	{
		// public static float3 max(float3 x, float3 y) { return new float3(max(x.x, y.x), max(x.y, y.y), max(x.z, y.z)); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_6, L_8, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14;
		L_14 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_11, L_13, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_15;
		memset((&L_15), 0, sizeof(L_15));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_15), L_4, L_9, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::max(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_max_m5A2AD47597A69870F6E5A8A2ABEE9A1EAC771393 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float4 max(float4 x, float4 y) { return new float4(max(x.x, y.x), max(x.y, y.y), max(x.z, y.z), max(x.w, y.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14;
		L_14 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_11, L_13, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = ___x0;
		float L_16 = L_15.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17 = ___y1;
		float L_18 = L_17.get_w_3();
		float L_19;
		L_19 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_16, L_18, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20;
		memset((&L_20), 0, sizeof(L_20));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_20), L_4, L_9, L_14, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::lerp(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, float ___s2, const RuntimeMethod* method)
{
	{
		// public static float4 lerp(float4 x, float4 y, float s) { return x + s * (y - x); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = ___s2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_2, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_1, L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6;
		L_6 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Int32 Unity.Mathematics.math::clamp(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_clamp_mAC2AAD9592C2C59601A5CF30D601D645033E2569 (int32_t ___x0, int32_t ___a1, int32_t ___b2, const RuntimeMethod* method)
{
	{
		// public static int clamp(int x, int a, int b) { return max(a, min(b, x)); }
		int32_t L_0 = ___a1;
		int32_t L_1 = ___b2;
		int32_t L_2 = ___x0;
		int32_t L_3;
		L_3 = math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A_inline(L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF_inline(L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single Unity.Mathematics.math::clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_clamp_m3FDC8AAE05842733E193852C929DAAC6F5E8C931 (float ___x0, float ___a1, float ___b2, const RuntimeMethod* method)
{
	{
		// public static float clamp(float x, float a, float b) { return max(a, min(b, x)); }
		float L_0 = ___a1;
		float L_1 = ___b2;
		float L_2 = ___x0;
		float L_3;
		L_3 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_2, /*hidden argument*/NULL);
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::clamp(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_clamp_m4A3A6B869C607E006DBC5EB2A50C0812F40E09A4 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a1, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b2, const RuntimeMethod* method)
{
	{
		// public static float4 clamp(float4 x, float4 a, float4 b) { return max(a, min(b, x)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___a1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___b2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3;
		L_3 = math_min_mC45C7024F6850EB1D382A2B2F6F31CCA4B2750E6_inline(L_1, L_2, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = math_max_m5A2AD47597A69870F6E5A8A2ABEE9A1EAC771393_inline(L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single Unity.Mathematics.math::saturate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_saturate_mA17845866D2189A354872A777FF9EE7D7AD0C7FC (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float saturate(float x) { return clamp(x, 0.0f, 1.0f); }
		float L_0 = ___x0;
		float L_1;
		L_1 = math_clamp_m3FDC8AAE05842733E193852C929DAAC6F5E8C931_inline(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single Unity.Mathematics.math::abs(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_abs_m684426E813FD887C103114B2E15FCBAFFF32D3B0 (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float abs(float x) { return asfloat(asuint(x) & 0x7FFFFFFF); }
		float L_0 = ___x0;
		uint32_t L_1;
		L_1 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_0, /*hidden argument*/NULL);
		float L_2;
		L_2 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(((int32_t)((int32_t)L_1&(int32_t)((int32_t)2147483647LL))), /*hidden argument*/NULL);
		return L_2;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::abs(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_abs_m3842FE0B152ABEA4BDD3985EF8C38B1E13BE6273 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 abs(float4 x) { return asfloat(asuint(x) & 0x7FFFFFFF); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_0, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = uint4_op_BitwiseAnd_m8BDB3A45AD663DDFBC3E18A0EA84B3C902767C36_inline(L_1, ((int32_t)2147483647LL), /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3;
		L_3 = math_asfloat_mD9B4C5BBB6AC641027D94C2045C4C3AF416DFF6D_inline(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float2,Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_dot_m6238E071DBA53F376473FFEC28706C9B537E6BAE (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float2 x, float2 y) { return x.x * y.x + x.y * y.y; }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7))));
	}
}
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float3 x, float3 y) { return x.x * y.x + x.y * y.y + x.z * y.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
	}
}
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float4 x, float4 y) { return x.x * y.x + x.y * y.y + x.z * y.z + x.w * y.w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___x0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___y1;
		float L_15 = L_14.get_w_3();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)))), (float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_15))));
	}
}
// System.Single Unity.Mathematics.math::atan2(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_atan2_mFFA7942F9F41993E3F752E630DB45D0B4104034C (float ___y0, float ___x1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float atan2(float y, float x) { return (float)System.Math.Atan2(y, x); }
		float L_0 = ___y0;
		float L_1 = ___x1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_2;
		L_2 = atan2(((double)((double)L_0)), ((double)((double)L_1)));
		return ((float)((float)L_2));
	}
}
// System.Single Unity.Mathematics.math::cos(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424 (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float cos(float x) { return (float)System.Math.Cos(x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = cos(((double)((double)L_0)));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::acos(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_acos_mDF4948B8CF18EF8052C38D7914802E7869B79B6C (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float acos(float x) { return (float)System.Math.Acos((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = acos(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::sin(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float sin(float x) { return (float)System.Math.Sin((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sin(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::floor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float floor(float x) { return (float)System.Math.Floor((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = floor(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
// Unity.Mathematics.float2 Unity.Mathematics.math::floor(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  math_floor_mCADCD88A3E295AEB16A2356DACBA00FDD8DFA9B9 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method)
{
	{
		// public static float2 floor(float2 x) { return new float2(floor(x.x), floor(x.y)); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_1, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_4, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6;
		memset((&L_6), 0, sizeof(L_6));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_6), L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::floor(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_floor_m1595239B5E0B9CA2944C200E1ACADE9CEED6EA2D (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float3 floor(float3 x) { return new float3(floor(x.x), floor(x.y), floor(x.z)); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_1, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_4, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_7, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::floor(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_floor_m6F1A809C667676D8CDF372A24CC318F90943AAC7 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 floor(float4 x) { return new float4(floor(x.x), floor(x.y), floor(x.z), floor(x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_1, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		float L_11;
		L_11 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single Unity.Mathematics.math::ceil(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_ceil_m09F7522539A79883D220617019957CCF0CB4DA8B (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float ceil(float x) { return (float)System.Math.Ceiling((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = ceil(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::rcp(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_rcp_mBBCDD8F05AFE2E307F898E81A06B64BF0E2625D0 (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float rcp(float x) { return 1.0f / x; }
		float L_0 = ___x0;
		return ((float)((float)(1.0f)/(float)L_0));
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::rcp(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_rcp_m60294695218F02BCB25674A2FA40A86499DD6F38 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float3 rcp(float3 x) { return 1.0f / x; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1;
		L_1 = float3_op_Division_mE2739F8C6E0ADC06D8DCD923F9828B75EACB3ECF_inline((1.0f), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::rcp(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_rcp_m3D4A844DEF4B4531007D5E1A17BF493690FEBB8C (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 rcp(float4 x) { return 1.0f / x; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1;
		L_1 = float4_op_Division_m847947B86BE29A43C35E74AB168A525866AA4B15_inline((1.0f), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single Unity.Mathematics.math::sign(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611 (float ___x0, const RuntimeMethod* method)
{
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	{
		// public static float sign(float x) { return (x > 0.0f ? 1.0f : 0.0f) - (x < 0.0f ? 1.0f : 0.0f); }
		float L_0 = ___x0;
		if ((((float)L_0) > ((float)(0.0f))))
		{
			goto IL_000f;
		}
	}
	{
		G_B3_0 = (0.0f);
		goto IL_0014;
	}

IL_000f:
	{
		G_B3_0 = (1.0f);
	}

IL_0014:
	{
		float L_1 = ___x0;
		G_B4_0 = G_B3_0;
		if ((((float)L_1) < ((float)(0.0f))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0023;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		goto IL_0028;
	}

IL_0023:
	{
		G_B6_0 = (1.0f);
		G_B6_1 = G_B5_0;
	}

IL_0028:
	{
		return ((float)il2cpp_codegen_subtract((float)G_B6_1, (float)G_B6_0));
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::sign(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_sign_m63A39DE29A6BCB0E9BA152B458A0DD2EB9EFB23C (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 sign(float4 x) { return new float4(sign(x.x), sign(x.y), sign(x.z), sign(x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_1, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		float L_11;
		L_11 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single Unity.Mathematics.math::pow(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_pow_m792865DB1E3BCC792726FFB800ABDE9FF109D3B4 (float ___x0, float ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float pow(float x, float y) { return (float)System.Math.Pow((float)x, (float)y); }
		float L_0 = ___x0;
		float L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_2;
		L_2 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F(((double)((double)((float)((float)L_0)))), ((double)((double)((float)((float)L_1)))), /*hidden argument*/NULL);
		return ((float)((float)L_2));
	}
}
// System.Single Unity.Mathematics.math::exp2(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_exp2_m91B7071A3214B73A372F626DFAA243B3C65BD230 (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float exp2(float x) { return (float)System.Math.Exp((float)x * 0.69314718f); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = exp(((double)((double)((float)il2cpp_codegen_multiply((float)((float)((float)L_0)), (float)(0.693147182f))))));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::log(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_log_m32923945396EF294F36A8CA9848ED0771A77F50F (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float log(float x) { return (float)System.Math.Log((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = log(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::sqrt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float sqrt(float x) { return (float)System.Math.Sqrt((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sqrt(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
// System.Single Unity.Mathematics.math::rsqrt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896 (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float rsqrt(float x) { return 1.0f / sqrt(x); }
		float L_0 = ___x0;
		float L_1;
		L_1 = math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline(L_0, /*hidden argument*/NULL);
		return ((float)((float)(1.0f)/(float)L_1));
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::normalize(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_normalize_m06189F2D06A4C6DEB81C6623B802A878E3356975 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float3 normalize(float3 x) { return rsqrt(dot(x, x)) * x; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline(L_0, L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline(L_2, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5;
		L_5 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::normalizesafe(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_normalizesafe_mD616A562864DA7E3FF165AB6E2CE7BE30131E668 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___defaultvalue1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float len = math.dot(x, x);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// return math.select(defaultvalue, x * math.rsqrt(len), len > FLT_MIN_NORMAL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___defaultvalue1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___x0;
		float L_5 = V_0;
		float L_6;
		L_6 = math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline(L_5, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		L_7 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_4, L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		L_9 = math_select_mD6522E9D958653C41F4B2E3463B79F7A011E60AB_inline(L_3, L_7, (bool)((((float)L_8) > ((float)(1.17549435E-38f)))? 1 : 0), /*hidden argument*/NULL);
		return L_9;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::normalizesafe(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_normalizesafe_m0043348AB7956C50EFFD5572CA0D516DCB54A376 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___defaultvalue1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float len = math.dot(x, x);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// return math.select(defaultvalue, x * math.rsqrt(len), len > FLT_MIN_NORMAL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___defaultvalue1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___x0;
		float L_5 = V_0;
		float L_6;
		L_6 = math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline(L_5, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_4, L_6, /*hidden argument*/NULL);
		float L_8 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = math_select_m3FF6FBC5429C94D76C025979A6D3DCA2B2B62602_inline(L_3, L_7, (bool)((((float)L_8) > ((float)(1.17549435E-38f)))? 1 : 0), /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Single Unity.Mathematics.math::length(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_length_m25A33854A8D7F4A4B96C82171BE616376ECA25CE (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method)
{
	{
		// public static float length(float2 x) { return sqrt(dot(x, x)); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_m6238E071DBA53F376473FFEC28706C9B537E6BAE_inline(L_0, L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Unity.Mathematics.math::length(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_length_mECD912F8B5F13E8FDFEFC19DDC928AC69C9669D4 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float length(float3 x) { return sqrt(dot(x, x)); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline(L_0, L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Unity.Mathematics.math::length(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_length_mD7967FEA18B97C7AC6CFE6B0BE3D45D35D355170 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float length(float4 x) { return sqrt(dot(x, x)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_0, L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_lengthsq_mCA5F01F89CC6AAAE7F0263A8F3C34FC2C7A98561 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float lengthsq(float3 x) { return dot(x, x); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_lengthsq_mD422A214358E935793F5ED10991D70F040848F2D (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float lengthsq(float4 x) { return dot(x, x); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::cross(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// public static float3 cross(float3 x, float3 y) { return (x * y.yzx - x.yzx * y).yzx; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1;
		L_1 = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)(&___y1), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2;
		L_2 = float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1_inline(L_0, L_1, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)(&___x0), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___y1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5;
		L_5 = float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1_inline(L_3, L_4, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6;
		L_6 = float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline(L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		L_7 = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)(&V_0), /*hidden argument*/NULL);
		return L_7;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::select(Unity.Mathematics.float3,Unity.Mathematics.float3,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_select_mD6522E9D958653C41F4B2E3463B79F7A011E60AB (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___a0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___b1, bool ___c2, const RuntimeMethod* method)
{
	{
		// public static float3 select(float3 a, float3 b, bool c) { return c ? b : a; }
		bool L_0 = ___c2;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___a0;
		return L_1;
	}

IL_0005:
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___b1;
		return L_2;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::select(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_select_m3FF6FBC5429C94D76C025979A6D3DCA2B2B62602 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, bool ___c2, const RuntimeMethod* method)
{
	{
		// public static float4 select(float4 a, float4 b, bool c) { return c ? b : a; }
		bool L_0 = ___c2;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___a0;
		return L_1;
	}

IL_0005:
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___b1;
		return L_2;
	}
}
// System.Void Unity.Mathematics.math::sincos(System.Single,System.Single&,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void math_sincos_m62FF0B31557448DAF3C3ED1EBCA171BD524BD96B (float ___x0, float* ___s1, float* ___c2, const RuntimeMethod* method)
{
	{
		// public static void sincos(float x, out float s, out float c) { s = sin(x); c = cos(x); }
		float* L_0 = ___s1;
		float L_1 = ___x0;
		float L_2;
		L_2 = math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE_inline(L_1, /*hidden argument*/NULL);
		*((float*)L_0) = (float)L_2;
		// public static void sincos(float x, out float s, out float c) { s = sin(x); c = cos(x); }
		float* L_3 = ___c2;
		float L_4 = ___x0;
		float L_5;
		L_5 = math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424_inline(L_4, /*hidden argument*/NULL);
		*((float*)L_3) = (float)L_5;
		// public static void sincos(float x, out float s, out float c) { s = sin(x); c = cos(x); }
		return;
	}
}
// System.Int32 Unity.Mathematics.math::lzcnt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_lzcnt_mBB573111B33D6C11E506BA3986481FA994D201FF (int32_t ___x0, const RuntimeMethod* method)
{
	{
		// public static int lzcnt(int x) { return lzcnt((uint)x); }
		int32_t L_0 = ___x0;
		int32_t L_1;
		L_1 = math_lzcnt_m69123CF8445E0B67FA30F24BA42F663C105BE7EB_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Unity.Mathematics.math::lzcnt(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_lzcnt_m69123CF8445E0B67FA30F24BA42F663C105BE7EB (uint32_t ___x0, const RuntimeMethod* method)
{
	LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (x == 0)
		uint32_t L_0 = ___x0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		// return 32;
		return ((int32_t)32);
	}

IL_0006:
	{
		// u.doubleValue = 0.0;
		(&V_0)->set_doubleValue_1((0.0));
		// u.longValue = 0x4330000000000000L + x;
		uint32_t L_1 = ___x0;
		(&V_0)->set_longValue_0(((int64_t)il2cpp_codegen_add((int64_t)((int64_t)4841369599423283200LL), (int64_t)((int64_t)((uint64_t)L_1)))));
		// u.doubleValue -= 4503599627370496.0;
		double* L_2 = (&V_0)->get_address_of_doubleValue_1();
		double* L_3 = L_2;
		double L_4 = *((double*)L_3);
		*((double*)L_3) = (double)((double)il2cpp_codegen_subtract((double)L_4, (double)(4503599627370496.0)));
		// return 0x41E - (int)(u.longValue >> 52);
		LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  L_5 = V_0;
		int64_t L_6 = L_5.get_longValue_0();
		return ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)1054), (int32_t)((int32_t)((int32_t)((int64_t)((int64_t)L_6>>(int32_t)((int32_t)52)))))));
	}
}
// System.Int32 Unity.Mathematics.math::tzcnt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_tzcnt_mAD942678F41AC6A34E7E83752D38E40F1043FE4E (int32_t ___x0, const RuntimeMethod* method)
{
	{
		// public static int tzcnt(int x) { return tzcnt((uint)x); }
		int32_t L_0 = ___x0;
		int32_t L_1;
		L_1 = math_tzcnt_m3A483A42E55BE961B589C59F99EE7972D0D296E8_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Unity.Mathematics.math::tzcnt(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_tzcnt_m3A483A42E55BE961B589C59F99EE7972D0D296E8 (uint32_t ___x0, const RuntimeMethod* method)
{
	LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (x == 0)
		uint32_t L_0 = ___x0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		// return 32;
		return ((int32_t)32);
	}

IL_0006:
	{
		// x &= (uint)-x;
		uint32_t L_1 = ___x0;
		uint32_t L_2 = ___x0;
		___x0 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)((uint32_t)((-((int64_t)((uint64_t)L_2))))))));
		// u.doubleValue = 0.0;
		(&V_0)->set_doubleValue_1((0.0));
		// u.longValue = 0x4330000000000000L + x;
		uint32_t L_3 = ___x0;
		(&V_0)->set_longValue_0(((int64_t)il2cpp_codegen_add((int64_t)((int64_t)4841369599423283200LL), (int64_t)((int64_t)((uint64_t)L_3)))));
		// u.doubleValue -= 4503599627370496.0;
		double* L_4 = (&V_0)->get_address_of_doubleValue_1();
		double* L_5 = L_4;
		double L_6 = *((double*)L_5);
		*((double*)L_5) = (double)((double)il2cpp_codegen_subtract((double)L_6, (double)(4503599627370496.0)));
		// return (int)(u.longValue >> 52) - 0x3FF;
		LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  L_7 = V_0;
		int64_t L_8 = L_7.get_longValue_0();
		return ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)((int64_t)((int64_t)L_8>>(int32_t)((int32_t)52))))), (int32_t)((int32_t)1023)));
	}
}
// System.Int32 Unity.Mathematics.math::ceilpow2(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t math_ceilpow2_mA7B90555C48581B479A78BA3EC7EE1DC7E2F657E (int32_t ___x0, const RuntimeMethod* method)
{
	{
		// x -= 1;
		int32_t L_0 = ___x0;
		___x0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1));
		// x |= x >> 1;
		int32_t L_1 = ___x0;
		int32_t L_2 = ___x0;
		___x0 = ((int32_t)((int32_t)L_1|(int32_t)((int32_t)((int32_t)L_2>>(int32_t)1))));
		// x |= x >> 2;
		int32_t L_3 = ___x0;
		int32_t L_4 = ___x0;
		___x0 = ((int32_t)((int32_t)L_3|(int32_t)((int32_t)((int32_t)L_4>>(int32_t)2))));
		// x |= x >> 4;
		int32_t L_5 = ___x0;
		int32_t L_6 = ___x0;
		___x0 = ((int32_t)((int32_t)L_5|(int32_t)((int32_t)((int32_t)L_6>>(int32_t)4))));
		// x |= x >> 8;
		int32_t L_7 = ___x0;
		int32_t L_8 = ___x0;
		___x0 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)L_8>>(int32_t)8))));
		// x |= x >> 16;
		int32_t L_9 = ___x0;
		int32_t L_10 = ___x0;
		___x0 = ((int32_t)((int32_t)L_9|(int32_t)((int32_t)((int32_t)L_10>>(int32_t)((int32_t)16)))));
		// return x + 1;
		int32_t L_11 = ___x0;
		return ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}
}
// System.Int64 Unity.Mathematics.math::ceilpow2(System.Int64)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int64_t math_ceilpow2_mC273C4BD00A26075F4370BA1B290D42C42C97227 (int64_t ___x0, const RuntimeMethod* method)
{
	{
		// x -= 1;
		int64_t L_0 = ___x0;
		___x0 = ((int64_t)il2cpp_codegen_subtract((int64_t)L_0, (int64_t)((int64_t)((int64_t)1))));
		// x |= x >> 1;
		int64_t L_1 = ___x0;
		int64_t L_2 = ___x0;
		___x0 = ((int64_t)((int64_t)L_1|(int64_t)((int64_t)((int64_t)L_2>>(int32_t)1))));
		// x |= x >> 2;
		int64_t L_3 = ___x0;
		int64_t L_4 = ___x0;
		___x0 = ((int64_t)((int64_t)L_3|(int64_t)((int64_t)((int64_t)L_4>>(int32_t)2))));
		// x |= x >> 4;
		int64_t L_5 = ___x0;
		int64_t L_6 = ___x0;
		___x0 = ((int64_t)((int64_t)L_5|(int64_t)((int64_t)((int64_t)L_6>>(int32_t)4))));
		// x |= x >> 8;
		int64_t L_7 = ___x0;
		int64_t L_8 = ___x0;
		___x0 = ((int64_t)((int64_t)L_7|(int64_t)((int64_t)((int64_t)L_8>>(int32_t)8))));
		// x |= x >> 16;
		int64_t L_9 = ___x0;
		int64_t L_10 = ___x0;
		___x0 = ((int64_t)((int64_t)L_9|(int64_t)((int64_t)((int64_t)L_10>>(int32_t)((int32_t)16)))));
		// x |= x >> 32;
		int64_t L_11 = ___x0;
		int64_t L_12 = ___x0;
		___x0 = ((int64_t)((int64_t)L_11|(int64_t)((int64_t)((int64_t)L_12>>(int32_t)((int32_t)32)))));
		// return x + 1;
		int64_t L_13 = ___x0;
		return ((int64_t)il2cpp_codegen_add((int64_t)L_13, (int64_t)((int64_t)((int64_t)1))));
	}
}
// System.Single Unity.Mathematics.math::cmin(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_cmin_mDDC6705B504F566B700A58BBC5DBE63EE40EAE53 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method)
{
	{
		// public static float cmin(float2 x) { return min(x.x, x.y); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___x0;
		float L_3 = L_2.get_y_1();
		float L_4;
		L_4 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single Unity.Mathematics.math::cmin(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_cmin_mD6518D31DF8A9C48087287DF5B5A82A3DF280E0A (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float cmin(float3 x) { return min(min(x.x, x.y), x.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___x0;
		float L_3 = L_2.get_y_1();
		float L_4;
		L_4 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_3, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = ___x0;
		float L_6 = L_5.get_z_2();
		float L_7;
		L_7 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single Unity.Mathematics.math::cmax(Unity.Mathematics.float2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_cmax_m5B2F559332B7FD6F7B77F932F39C18D277A17888 (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method)
{
	{
		// public static float cmax(float2 x) { return max(x.x, x.y); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___x0;
		float L_3 = L_2.get_y_1();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Single Unity.Mathematics.math::cmax(Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_cmax_m468A5263FCD72A1EC99854B9477FD2FAF4ABBFFF (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static float cmax(float3 x) { return max(max(x.x, x.y), x.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___x0;
		float L_3 = L_2.get_y_1();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = ___x0;
		float L_6 = L_5.get_z_2();
		float L_7;
		L_7 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_4, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single Unity.Mathematics.math::cmax(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_cmax_mA222CC6FC5245F461911FC523F1DCC9B14D36508 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float cmax(float4 x) { return max(max(x.x, x.y), max(x.z, x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___x0;
		float L_3 = L_2.get_y_1();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___x0;
		float L_6 = L_5.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___x0;
		float L_8 = L_7.get_w_3();
		float L_9;
		L_9 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_6, L_8, /*hidden argument*/NULL);
		float L_10;
		L_10 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_4, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1 (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint2 x) { return x.x + x.y; }
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		return ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3));
	}
}
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint3 x) { return x.x + x.y + x.z; }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5));
	}
}
// System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint4 x) { return x.x + x.y + x.z + x.w; }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___x0;
		uint32_t L_7 = L_6.get_w_3();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5)), (int32_t)L_7));
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::movelh(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method)
{
	{
		// return shuffle(a, b, ShuffleComponent.LeftX, ShuffleComponent.LeftY, ShuffleComponent.RightX, ShuffleComponent.RightY);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		L_2 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_0, L_1, 0, 1, 4, 5, /*hidden argument*/NULL);
		return L_2;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::movehl(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method)
{
	{
		// return shuffle(b, a, ShuffleComponent.LeftZ, ShuffleComponent.LeftW, ShuffleComponent.RightZ, ShuffleComponent.RightW);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		L_2 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_0, L_1, 2, 3, 6, 7, /*hidden argument*/NULL);
		return L_2;
	}
}
// Unity.Mathematics.float3x3 Unity.Mathematics.math::float3x3(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  math_float3x3_m083898B3E74368DC86FB3D19406C3ED60AF63583 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___rotation0, const RuntimeMethod* method)
{
	{
		// return new float3x3(rotation);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___rotation0;
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.float4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_mul_mDBF2C1741940E16BDEB49774E0CF13AB4889A255 (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method)
{
	{
		// return a.c0 * b.x + a.c1 * b.y + a.c2 * b.z + a.c3 * b.w;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_1, L_3, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___b1;
		float L_8 = L_7.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10;
		L_10 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_11 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = L_11.get_c2_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = ___b1;
		float L_14 = L_13.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15;
		L_15 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_12, L_14, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		L_16 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_17 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_c3_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19 = ___b1;
		float L_20 = L_19.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_21;
		L_21 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_18, L_20, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_22;
		L_22 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_16, L_21, /*hidden argument*/NULL);
		return L_22;
	}
}
// Unity.Mathematics.float4x4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_mul_m5C88F4BCCEAC68636D82BCA37DFD19731E9C8B3D (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___a0, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___b1, const RuntimeMethod* method)
{
	{
		// return float4x4(
		//     a.c0 * b.c0.x + a.c1 * b.c0.y + a.c2 * b.c0.z + a.c3 * b.c0.w,
		//     a.c0 * b.c1.x + a.c1 * b.c1.y + a.c2 * b.c1.z + a.c3 * b.c1.w,
		//     a.c0 * b.c2.x + a.c1 * b.c2.y + a.c2 * b.c2.z + a.c3 * b.c2.w,
		//     a.c0 * b.c3.x + a.c1 * b.c3.y + a.c2 * b.c3.z + a.c3 * b.c3.w);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_2 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_c0_0();
		float L_4 = L_3.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_1, L_4, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_6 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_8 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = L_8.get_c0_0();
		float L_10 = L_9.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_7, L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		L_12 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_5, L_11, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_13 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = L_13.get_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_15 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16 = L_15.get_c0_0();
		float L_17 = L_16.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18;
		L_18 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_14, L_17, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		L_19 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_12, L_18, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_20 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_21 = L_20.get_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_22 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_23 = L_22.get_c0_0();
		float L_24 = L_23.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_25;
		L_25 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_21, L_24, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_26;
		L_26 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_19, L_25, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_27 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_28 = L_27.get_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_29 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_30 = L_29.get_c1_1();
		float L_31 = L_30.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_32;
		L_32 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_28, L_31, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_33 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_34 = L_33.get_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_35 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_36 = L_35.get_c1_1();
		float L_37 = L_36.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_38;
		L_38 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_34, L_37, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_39;
		L_39 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_32, L_38, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_40 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_41 = L_40.get_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_42 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_43 = L_42.get_c1_1();
		float L_44 = L_43.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_45;
		L_45 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_41, L_44, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_46;
		L_46 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_39, L_45, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_47 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_48 = L_47.get_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_49 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_50 = L_49.get_c1_1();
		float L_51 = L_50.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_52;
		L_52 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_48, L_51, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_53;
		L_53 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_46, L_52, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_54 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_55 = L_54.get_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_56 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_57 = L_56.get_c2_2();
		float L_58 = L_57.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_59;
		L_59 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_55, L_58, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_60 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_61 = L_60.get_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_62 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_63 = L_62.get_c2_2();
		float L_64 = L_63.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_65;
		L_65 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_61, L_64, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_66;
		L_66 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_59, L_65, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_67 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_68 = L_67.get_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_69 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_70 = L_69.get_c2_2();
		float L_71 = L_70.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_72;
		L_72 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_68, L_71, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_73;
		L_73 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_66, L_72, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_74 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_75 = L_74.get_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_76 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_77 = L_76.get_c2_2();
		float L_78 = L_77.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_79;
		L_79 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_75, L_78, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_80;
		L_80 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_73, L_79, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_81 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_82 = L_81.get_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_83 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_84 = L_83.get_c3_3();
		float L_85 = L_84.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_86;
		L_86 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_82, L_85, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_87 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_88 = L_87.get_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_89 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_90 = L_89.get_c3_3();
		float L_91 = L_90.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_92;
		L_92 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_88, L_91, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_93;
		L_93 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_86, L_92, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_94 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_95 = L_94.get_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_96 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_97 = L_96.get_c3_3();
		float L_98 = L_97.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_99;
		L_99 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_95, L_98, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_100;
		L_100 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_93, L_99, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_101 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_102 = L_101.get_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_103 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_104 = L_103.get_c3_3();
		float L_105 = L_104.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_106;
		L_106 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_102, L_105, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_107;
		L_107 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_100, L_106, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_108;
		L_108 = math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC_inline(L_26, L_53, L_80, L_107, /*hidden argument*/NULL);
		return L_108;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::quaternion(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method)
{
	{
		// public static quaternion quaternion(float4 value) { return new quaternion(value); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___value0;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_1;
		memset((&L_1), 0, sizeof(L_1));
		quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::conjugate(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_conjugate_mF4ABF8CC798742764982DEEC731088BF719B7935 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	{
		// return quaternion(q.value * float4(-1.0f, -1.0f, -1.0f, 1.0f));
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		L_2 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline((-1.0f), (-1.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3;
		L_3 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_1, L_2, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_4;
		L_4 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::inverse(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_inverse_m8C4E2F1AA7871C40EA7FFAB90D5F2653466E894D (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// float4 x = q.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		V_0 = L_1;
		// return quaternion(rcp(dot(x, x)) * x * float4(-1.0f, -1.0f, -1.0f, 1.0f));
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = V_0;
		float L_4;
		L_4 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_2, L_3, /*hidden argument*/NULL);
		float L_5;
		L_5 = math_rcp_mBBCDD8F05AFE2E307F898E81A06B64BF0E2625D0_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_5, L_6, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		L_8 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline((-1.0f), (-1.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_7, L_8, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_10;
		L_10 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___a0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___b1, const RuntimeMethod* method)
{
	{
		// return dot(a.value, b.value);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_2 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_value_0();
		float L_4;
		L_4 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::normalize(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// float4 x = q.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		V_0 = L_1;
		// return quaternion(rsqrt(dot(x, x)) * x);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = V_0;
		float L_4;
		L_4 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_2, L_3, /*hidden argument*/NULL);
		float L_5;
		L_5 = math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_5, L_6, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_8;
		L_8 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::mul(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_mul_m545D5B9EB2838DCDD021F61D385D8DE119422FC3 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___a0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___b1, const RuntimeMethod* method)
{
	{
		// return quaternion(a.value.wwww * b.value + (a.value.xyzx * b.value.wwwx + a.value.yzxy * b.value.zxyy) * float4(1.0f, 1.0f, 1.0f, -1.0f) - a.value.zxyz * b.value.yzxz);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = (&___a0)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1;
		L_1 = float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_0, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_2 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_1, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_5 = (&___a0)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6;
		L_6 = float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_5, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_7 = (&___b1)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		L_8 = float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_10 = (&___a0)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = (&___b1)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13;
		L_13 = float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_12, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14;
		L_14 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_11, L_13, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15;
		L_15 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_9, L_14, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		L_16 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline((1.0f), (1.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17;
		L_17 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_15, L_16, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18;
		L_18 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_4, L_17, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_19 = (&___a0)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20;
		L_20 = float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_19, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_21 = (&___b1)->get_address_of_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_22;
		L_22 = float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_21, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_23;
		L_23 = float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline(L_20, L_22, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_24;
		L_24 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_18, L_23, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_25;
		L_25 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_24, /*hidden argument*/NULL);
		return L_25;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::mul(Unity.Mathematics.quaternion,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_mul_m003A157203A82B7842A6894B3127F84D61BE3B9B (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v1, const RuntimeMethod* method)
{
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// float3 t = 2 * cross(q.value.xyz, v);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = (&___q0)->get_address_of_value_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1;
		L_1 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_0, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___v1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019_inline(L_1, L_2, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4;
		L_4 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline((2.0f), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// return v + q.value.w * t + cross(q.value.xyz, t);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = ___v1;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_6 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_value_0();
		float L_8 = L_7.get_w_3();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10;
		L_10 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_8, L_9, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		L_11 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_5, L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = (&___q0)->get_address_of_value_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_13;
		L_13 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_12, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_14 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_15;
		L_15 = math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019_inline(L_13, L_14, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_16;
		L_16 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.float3 Unity.Mathematics.math::rotate(Unity.Mathematics.quaternion,Unity.Mathematics.float3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_rotate_mD7EBD96F0C1DAD111B6A3CD7942C006539777D47 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v1, const RuntimeMethod* method)
{
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// float3 t = 2 * cross(q.value.xyz, v);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = (&___q0)->get_address_of_value_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1;
		L_1 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_0, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___v1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019_inline(L_1, L_2, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4;
		L_4 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline((2.0f), L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// return v + q.value.w * t + cross(q.value.xyz, t);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5 = ___v1;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_6 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_value_0();
		float L_8 = L_7.get_w_3();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10;
		L_10 = float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline(L_8, L_9, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		L_11 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_5, L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = (&___q0)->get_address_of_value_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_13;
		L_13 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_12, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_14 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_15;
		L_15 = math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019_inline(L_13, L_14, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_16;
		L_16 = float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline(L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::nlerp(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_nlerp_mB7BD6C0F6F0AD1411F206E3BFDCC4161F1DF5D54 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q10, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q21, float ___t2, const RuntimeMethod* method)
{
	{
		// float dt = dot(q1, q2);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q10;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_1 = ___q21;
		float L_2;
		L_2 = math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634_inline(L_0, L_1, /*hidden argument*/NULL);
		// if(dt < 0.0f)
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0020;
		}
	}
	{
		// q2.value = -q2.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_3 = ___q21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = L_3.get_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_4, /*hidden argument*/NULL);
		(&___q21)->set_value_0(L_5);
	}

IL_0020:
	{
		// return normalize(quaternion(lerp(q1.value, q2.value, t)));
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_6 = ___q10;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_value_0();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_8 = ___q21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = L_8.get_value_0();
		float L_10 = ___t2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91_inline(L_7, L_9, L_10, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_12;
		L_12 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_11, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_13;
		L_13 = math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D_inline(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.math::slerp(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_slerp_m4819ACD4A8A9FA276AE84D9EAAA2ACAD2ED54A28 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q10, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q21, float ___t2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// float dt = dot(q1, q2);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q10;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_1 = ___q21;
		float L_2;
		L_2 = math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (dt < 0.0f)
		float L_3 = V_0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0025;
		}
	}
	{
		// dt = -dt;
		float L_4 = V_0;
		V_0 = ((-L_4));
		// q2.value = -q2.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_5 = ___q21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_6, /*hidden argument*/NULL);
		(&___q21)->set_value_0(L_7);
	}

IL_0025:
	{
		// if (dt < 0.9995f)
		float L_8 = V_0;
		if ((!(((float)L_8) < ((float)(0.999499977f)))))
		{
			goto IL_0080;
		}
	}
	{
		// float angle = acos(dt);
		float L_9 = V_0;
		float L_10;
		L_10 = math_acos_mDF4948B8CF18EF8052C38D7914802E7869B79B6C_inline(L_9, /*hidden argument*/NULL);
		// float s = rsqrt(1.0f - dt * dt);    // 1.0f / sin(angle)
		float L_11 = V_0;
		float L_12 = V_0;
		float L_13;
		L_13 = math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline(((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)il2cpp_codegen_multiply((float)L_11, (float)L_12)))), /*hidden argument*/NULL);
		V_1 = L_13;
		// float w1 = sin(angle * (1.0f - t)) * s;
		float L_14 = L_10;
		float L_15 = ___t2;
		float L_16;
		L_16 = math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE_inline(((float)il2cpp_codegen_multiply((float)L_14, (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_15)))), /*hidden argument*/NULL);
		float L_17 = V_1;
		V_2 = ((float)il2cpp_codegen_multiply((float)L_16, (float)L_17));
		// float w2 = sin(angle * t) * s;
		float L_18 = ___t2;
		float L_19;
		L_19 = math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE_inline(((float)il2cpp_codegen_multiply((float)L_14, (float)L_18)), /*hidden argument*/NULL);
		float L_20 = V_1;
		V_3 = ((float)il2cpp_codegen_multiply((float)L_19, (float)L_20));
		// return quaternion(q1.value * w1 + q2.value * w2);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_21 = ___q10;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_22 = L_21.get_value_0();
		float L_23 = V_2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_24;
		L_24 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_22, L_23, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_25 = ___q21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_26 = L_25.get_value_0();
		float L_27 = V_3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_28;
		L_28 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_26, L_27, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_29;
		L_29 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_24, L_28, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_30;
		L_30 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_29, /*hidden argument*/NULL);
		return L_30;
	}

IL_0080:
	{
		// return nlerp(q1, q2, t);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_31 = ___q10;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_32 = ___q21;
		float L_33 = ___t2;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_34;
		L_34 = math_nlerp_mB7BD6C0F6F0AD1411F206E3BFDCC4161F1DF5D54_inline(L_31, L_32, L_33, /*hidden argument*/NULL);
		return L_34;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	{
		// return hash(q.value);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		uint32_t L_2;
		L_2 = math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5_inline(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// Unity.Mathematics.uint2 Unity.Mathematics.math::uint2(System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2 (uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static uint2 uint2(uint x, uint y) { return new uint2(x, y); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		memset((&L_2), 0, sizeof(L_2));
		uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90 (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint2(0x4473BBB1u, 0xCBA11D5Fu)) + 0x685835CFu;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = ___v0;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1;
		L_1 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(((int32_t)1148435377), ((int32_t)-878633633), /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		L_2 = uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3;
		L_3 = math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)1750611407)));
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729 (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// public static uint3 uint3(uint x, uint y, uint z) { return new uint3(x, y, z); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint3(0xCD266C89u, 0xF1852A33u, 0x77E35E77u)) + 0x863E3729u;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___v0;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1;
		L_1 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-853119863), ((int32_t)-242931149), ((int32_t)2011389559), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3;
		L_3 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)-2042742999)));
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777 (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// public static uint4 uint4(uint x, uint y, uint z, uint w) { return new uint4(x, y, z, w); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint32_t L_3 = ___w3;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4;
		memset((&L_4), 0, sizeof(L_4));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint4(0xB492BF15u, 0xD37220E3u, 0x7AA2C2BDu, 0xE16BC89Du)) + 0x7AA07CD3u;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___v0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-1265451243), ((int32_t)-747495197), ((int32_t)2057487037), ((int32_t)-513029987), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3;
		L_3 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)2057338067)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::op_Implicit(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  quaternion_op_Implicit_m4D19584694391AD6CBB3A0FC09D23DCEAE29E9A0 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___q0, const RuntimeMethod* method)
{
	{
		// public static implicit operator quaternion(Quaternion q)  { return new quaternion(q.x, q.y, q.z, q.w); }
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_0 = ___q0;
		float L_1 = L_0.get_x_0();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_2 = ___q0;
		float L_3 = L_2.get_y_1();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_4 = ___q0;
		float L_5 = L_4.get_z_2();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_6 = ___q0;
		float L_7 = L_6.get_w_3();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_8;
		memset((&L_8), 0, sizeof(L_8));
		quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_inline((&L_8), L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void Unity.Mathematics.quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = __this->get_address_of_value_0();
		float L_1 = ___x0;
		L_0->set_x_0(L_1);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_value_0();
		float L_3 = ___y1;
		L_2->set_y_1(L_3);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_4 = __this->get_address_of_value_0();
		float L_5 = ___z2;
		L_4->set_z_2(L_5);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_6 = __this->get_address_of_value_0();
		float L_7 = ___w3;
		L_6->set_w_3(L_7);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		return;
	}
}
IL2CPP_EXTERN_C  void quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_inline(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Void Unity.Mathematics.quaternion::.ctor(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method)
{
	{
		// public quaternion(float4 value) { this.value = value; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___value0;
		__this->set_value_0(L_0);
		// public quaternion(float4 value) { this.value = value; }
		return;
	}
}
IL2CPP_EXTERN_C  void quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_AdjustorThunk (RuntimeObject * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_inline(_thisAdjusted, ___value0, method);
}
// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::op_Implicit(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  quaternion_op_Implicit_m20E9FBF48FCB66C9F65E1174F94CBD513449A4A7 (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// public static implicit operator quaternion(float4 v) { return new quaternion(v); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_1;
		memset((&L_1), 0, sizeof(L_1));
		quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::AxisAngle(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  quaternion_AxisAngle_m983F92B5F96B2B35C8B50B91285557721362E48F (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___axis0, float ___angle1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// math.sincos(0.5f * angle, out sina, out cosa);
		float L_0 = ___angle1;
		math_sincos_m62FF0B31557448DAF3C3ED1EBCA171BD524BD96B_inline(((float)il2cpp_codegen_multiply((float)(0.5f), (float)L_0)), (float*)(&V_0), (float*)(&V_1), /*hidden argument*/NULL);
		// return quaternion(float4(axis * sina, cosa));
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___axis0;
		float L_2 = V_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline(L_1, L_2, /*hidden argument*/NULL);
		float L_4 = V_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline(L_3, L_4, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_6;
		L_6 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Boolean Unity.Mathematics.quaternion::Equals(Unity.Mathematics.quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___x0, const RuntimeMethod* method)
{
	{
		// public bool Equals(quaternion x) { return value.x == x.value.x && value.y == x.value.y && value.z == x.value.z && value.w == x.value.w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = __this->get_address_of_value_0();
		float L_1 = L_0->get_x_0();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_2 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_value_0();
		float L_4 = L_3.get_x_0();
		if ((!(((float)L_1) == ((float)L_4))))
		{
			goto IL_0061;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_5 = __this->get_address_of_value_0();
		float L_6 = L_5->get_y_1();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_7 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = L_7.get_value_0();
		float L_9 = L_8.get_y_1();
		if ((!(((float)L_6) == ((float)L_9))))
		{
			goto IL_0061;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_10 = __this->get_address_of_value_0();
		float L_11 = L_10->get_z_2();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_12 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_value_0();
		float L_14 = L_13.get_z_2();
		if ((!(((float)L_11) == ((float)L_14))))
		{
			goto IL_0061;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_15 = __this->get_address_of_value_0();
		float L_16 = L_15->get_w_3();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_17 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_value_0();
		float L_19 = L_18.get_w_3();
		return (bool)((((float)L_16) == ((float)L_19))? 1 : 0);
	}

IL_0061:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_AdjustorThunk (RuntimeObject * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___x0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	bool _returnValue;
	_returnValue = quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_inline(_thisAdjusted, ___x0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.quaternion::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, RuntimeObject * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object x) { return Equals((quaternion)x); }
		RuntimeObject * L_0 = ___x0;
		bool L_1;
		L_1 = quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_inline((quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)__this, ((*(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)((quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)UnBox(L_0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___x0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	bool _returnValue;
	_returnValue = quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011_inline(_thisAdjusted, ___x0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.quaternion::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = (*(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)__this);
		uint32_t L_1;
		L_1 = math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.quaternion::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("quaternion({0}f, {1}f, {2}f, {3}f)", value.x, value.y, value.z, value.w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_value_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_7 = __this->get_address_of_value_0();
		float L_8 = L_7->get_y_1();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = __this->get_address_of_value_0();
		float L_13 = L_12->get_z_2();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_17 = __this->get_address_of_value_0();
		float L_18 = L_17->get_w_3();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C  String_t* quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.quaternion::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057 (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("quaternion({0}f, {1}f, {2}f, {3}f)", value.x.ToString(format, formatProvider), value.y.ToString(format, formatProvider), value.z.ToString(format, formatProvider), value.w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_value_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6;
		L_6 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_8 = __this->get_address_of_value_0();
		float* L_9 = L_8->get_address_of_y_1();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12;
		L_12 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_14 = __this->get_address_of_value_0();
		float* L_15 = L_14->get_address_of_z_2();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18;
		L_18 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = L_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_20 = __this->get_address_of_value_0();
		float* L_21 = L_20->get_address_of_w_3();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24;
		L_24 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		String_t* L_25;
		L_25 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB, L_19, /*hidden argument*/NULL);
		return L_25;
	}
}
IL2CPP_EXTERN_C  String_t* quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * _thisAdjusted = reinterpret_cast<quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
// System.Void Unity.Mathematics.quaternion::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void quaternion__cctor_mA10CF9925C7F128021CBD135EEC961A6646F3527 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static readonly quaternion identity = new quaternion(0.0f, 0.0f, 0.0f, 1.0f);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0;
		memset((&L_0), 0, sizeof(L_0));
		quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_StaticFields*)il2cpp_codegen_static_fields_for(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var))->set_identity_1(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.uint2::.ctor(System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727 (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_AdjustorThunk (RuntimeObject * __this, uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint2_t31B88562B6681D249453803230869FBE9ED565E7 * _thisAdjusted = reinterpret_cast<uint2_t31B88562B6681D249453803230869FBE9ED565E7 *>(__this + _offset);
	uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline(_thisAdjusted, ___x0, ___y1, method);
}
// Unity.Mathematics.uint2 Unity.Mathematics.uint2::op_Multiply(Unity.Mathematics.uint2,Unity.Mathematics.uint2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___lhs0, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint2 operator * (uint2 lhs, uint2 rhs) { return new uint2 (lhs.x * rhs.x, lhs.y * rhs.y); }
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_8;
		memset((&L_8), 0, sizeof(L_8));
		uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline((&L_8), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Boolean Unity.Mathematics.uint2::Equals(Unity.Mathematics.uint2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint2 rhs) { return x == rhs.x && y == rhs.y; }
		uint32_t L_0 = __this->get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		return (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_AdjustorThunk (RuntimeObject * __this, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint2_t31B88562B6681D249453803230869FBE9ED565E7 * _thisAdjusted = reinterpret_cast<uint2_t31B88562B6681D249453803230869FBE9ED565E7 *>(__this + _offset);
	bool _returnValue;
	_returnValue = uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.uint2::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&uint2_t31B88562B6681D249453803230869FBE9ED565E7_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((uint2)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_inline((uint2_t31B88562B6681D249453803230869FBE9ED565E7 *)__this, ((*(uint2_t31B88562B6681D249453803230869FBE9ED565E7 *)((uint2_t31B88562B6681D249453803230869FBE9ED565E7 *)UnBox(L_0, uint2_t31B88562B6681D249453803230869FBE9ED565E7_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint2_t31B88562B6681D249453803230869FBE9ED565E7 * _thisAdjusted = reinterpret_cast<uint2_t31B88562B6681D249453803230869FBE9ED565E7 *>(__this + _offset);
	bool _returnValue;
	_returnValue = uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.uint2::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = (*(uint2_t31B88562B6681D249453803230869FBE9ED565E7 *)__this);
		uint32_t L_1;
		L_1 = math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint2_t31B88562B6681D249453803230869FBE9ED565E7 * _thisAdjusted = reinterpret_cast<uint2_t31B88562B6681D249453803230869FBE9ED565E7 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.uint2::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8 (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint2({0}, {1})", x, y);
		uint32_t L_0 = __this->get_x_0();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_y_1();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_EXTERN_C  String_t* uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint2_t31B88562B6681D249453803230869FBE9ED565E7 * _thisAdjusted = reinterpret_cast<uint2_t31B88562B6681D249453803230869FBE9ED565E7 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.uint2::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1 (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint2({0}, {1})", x.ToString(format, formatProvider), y.ToString(format, formatProvider));
		uint32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		uint32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_EXTERN_C  String_t* uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint2_t31B88562B6681D249453803230869FBE9ED565E7 * _thisAdjusted = reinterpret_cast<uint2_t31B88562B6681D249453803230869FBE9ED565E7 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_AdjustorThunk (RuntimeObject * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * _thisAdjusted = reinterpret_cast<uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *>(__this + _offset);
	uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator * (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_12), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Addition(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator + (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_12), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_ExclusiveOr(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator ^ (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x ^ rhs.x, lhs.y ^ rhs.y, lhs.z ^ rhs.z); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_12), ((int32_t)((int32_t)L_1^(int32_t)L_3)), ((int32_t)((int32_t)L_5^(int32_t)L_7)), ((int32_t)((int32_t)L_9^(int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		uint32_t L_0 = __this->get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		return (bool)((((int32_t)L_6) == ((int32_t)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_AdjustorThunk (RuntimeObject * __this, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * _thisAdjusted = reinterpret_cast<uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *>(__this + _offset);
	bool _returnValue;
	_returnValue = uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((uint3)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_inline((uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *)__this, ((*(uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *)((uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *)UnBox(L_0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * _thisAdjusted = reinterpret_cast<uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *>(__this + _offset);
	bool _returnValue;
	_returnValue = uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.uint3::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = (*(uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *)__this);
		uint32_t L_1;
		L_1 = math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * _thisAdjusted = reinterpret_cast<uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.uint3::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x, y, z);
		uint32_t L_0 = __this->get_x_0();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_y_1();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_4);
		uint32_t L_6 = __this->get_z_2();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_EXTERN_C  String_t* uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * _thisAdjusted = reinterpret_cast<uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167 (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		uint32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		uint32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		uint32_t* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11;
		L_11 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C  String_t* uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * _thisAdjusted = reinterpret_cast<uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		uint32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_AdjustorThunk (RuntimeObject * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * _thisAdjusted = reinterpret_cast<uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *>(__this + _offset);
	uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator * (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_16), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator + (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_16), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
// Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_BitwiseAnd(Unity.Mathematics.uint4,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_BitwiseAnd_m8BDB3A45AD663DDFBC3E18A0EA84B3C902767C36 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator & (uint4 lhs, uint rhs) { return new uint4 (lhs.x & rhs, lhs.y & rhs, lhs.z & rhs, lhs.w & rhs); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint32_t L_2 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3 = ___lhs0;
		uint32_t L_4 = L_3.get_y_1();
		uint32_t L_5 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___lhs0;
		uint32_t L_7 = L_6.get_z_2();
		uint32_t L_8 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_9 = ___lhs0;
		uint32_t L_10 = L_9.get_w_3();
		uint32_t L_11 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_12), ((int32_t)((int32_t)L_1&(int32_t)L_2)), ((int32_t)((int32_t)L_4&(int32_t)L_5)), ((int32_t)((int32_t)L_7&(int32_t)L_8)), ((int32_t)((int32_t)L_10&(int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		uint32_t L_0 = __this->get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_9 = __this->get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10 = ___rhs0;
		uint32_t L_11 = L_10.get_w_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_EXTERN_C  bool uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_AdjustorThunk (RuntimeObject * __this, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * _thisAdjusted = reinterpret_cast<uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *>(__this + _offset);
	bool _returnValue;
	_returnValue = uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_inline(_thisAdjusted, ___rhs0, method);
	return _returnValue;
}
// System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object o) { return Equals((uint4)o); }
		RuntimeObject * L_0 = ___o0;
		bool L_1;
		L_1 = uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_inline((uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *)__this, ((*(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *)((uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *)UnBox(L_0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * _thisAdjusted = reinterpret_cast<uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *>(__this + _offset);
	bool _returnValue;
	_returnValue = uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C(_thisAdjusted, ___o0, method);
	return _returnValue;
}
// System.Int32 Unity.Mathematics.uint4::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = (*(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  int32_t uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * _thisAdjusted = reinterpret_cast<uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.uint4::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x, y, z, w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		uint32_t L_2 = __this->get_x_0();
		uint32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_1;
		uint32_t L_6 = __this->get_y_1();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = L_5;
		uint32_t L_10 = __this->get_z_2();
		uint32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_9;
		uint32_t L_14 = __this->get_w_3();
		uint32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17;
		L_17 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_EXTERN_C  String_t* uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * _thisAdjusted = reinterpret_cast<uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_inline(_thisAdjusted, method);
	return _returnValue;
}
// System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622 (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		uint32_t* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5;
		L_5 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		uint32_t* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10;
		L_10 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		uint32_t* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15;
		L_15 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		uint32_t* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20;
		L_20 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_EXTERN_C  String_t* uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_AdjustorThunk (RuntimeObject * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * _thisAdjusted = reinterpret_cast<uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *>(__this + _offset);
	String_t* _returnValue;
	_returnValue = uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_inline(_thisAdjusted, ___format0, ___formatProvider1, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, int32_t ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		int32_t L_0 = ___v0;
		__this->set_x_0(((float)((float)L_0)));
		// this.y = v;
		int32_t L_1 = ___v0;
		__this->set_y_1(((float)((float)L_1)));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(x, y); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float2 rhs) { return x == rhs.x && y == rhs.y; }
		float L_0 = __this->get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		return (bool)((((float)L_3) == ((float)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint2(0xFA3A3285u, 0xAD55999Du)) + 0xDCDD5341u;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___v0;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1;
		L_1 = math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863_inline(L_0, /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		L_2 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(((int32_t)-96849275), ((int32_t)-1386899043), /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_3;
		L_3 = uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-589475007)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = (*(float2_tCB7B81181978EDE17722C533A55E345D9A413274 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float2({0}f, {1}f)", x, y);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float2({0}f, {1}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider));
		float* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_0, L_1, L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_4, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral50EDAD5CFB15084F3338C313ED64018C8F78CD58, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		float L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		float L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		float L_2 = ___v0;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(y, z, x); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(x, y); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(y, z); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		float L_0 = __this->get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		return (bool)((((float)L_6) == ((float)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint3(0x9B13B92Du, 0x4ABF0813u, 0x86068063u)) + 0xD75513F9u;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___v0;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1;
		L_1 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_0, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-1693206227), ((int32_t)1254033427), ((int32_t)-2046394269), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		L_3 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-682290183)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = (*(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)__this);
		uint32_t L_1;
		L_1 = math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x, y, z);
		float L_0 = __this->get_x_0();
		float L_1 = L_0;
		RuntimeObject * L_2 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_1);
		float L_3 = __this->get_y_1();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		float L_6 = __this->get_z_2();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3({0}f, {1}f, {2}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		float* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_0, L_1, L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_4, L_5, L_6, /*hidden argument*/NULL);
		float* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11;
		L_11 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral1578CABA4FF62AC9986DD5D0BA4C26A5CCB44A6A, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c00, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c11, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___c22, const RuntimeMethod* method)
{
	{
		// this.c0 = c0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___c00;
		__this->set_c0_0(L_0);
		// this.c1 = c1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___c11;
		__this->set_c1_1(L_1);
		// this.c2 = c2;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___c22;
		__this->set_c2_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float ___m000, float ___m011, float ___m022, float ___m103, float ___m114, float ___m125, float ___m206, float ___m217, float ___m228, const RuntimeMethod* method)
{
	{
		// this.c0 = new float3(m00, m10, m20);
		float L_0 = ___m000;
		float L_1 = ___m103;
		float L_2 = ___m206;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_c0_0(L_3);
		// this.c1 = new float3(m01, m11, m21);
		float L_4 = ___m011;
		float L_5 = ___m114;
		float L_6 = ___m217;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		memset((&L_7), 0, sizeof(L_7));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_7), L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_c1_1(L_7);
		// this.c2 = new float3(m02, m12, m22);
		float L_8 = ___m022;
		float L_9 = ___m125;
		float L_10 = ___m228;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		memset((&L_11), 0, sizeof(L_11));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_11), L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->set_c2_2(L_11);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float3 lhs, float rhs) { return new float3 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator + (float3 lhs, float3 rhs) { return new float3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator / (float3 lhs, float rhs) { return new float3 (lhs.x / rhs, lhs.y / rhs, lhs.z / rhs); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float3x3 rhs) { return c0.Equals(rhs.c0) && c1.Equals(rhs.c1) && c2.Equals(rhs.c2); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_0 = __this->get_address_of_c0_0();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_1 = ___rhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = L_1.get_c0_0();
		bool L_3;
		L_3 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_4 = __this->get_address_of_c1_1();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_5 = ___rhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = L_5.get_c1_1();
		bool L_7;
		L_7 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_8 = __this->get_address_of_c2_2();
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_9 = ___rhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = L_9.get_c2_2();
		bool L_11;
		L_11 = float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)L_8, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0038:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v.c0) * uint3(0x713BD06Fu, 0x753AD6ADu, 0xD19764C7u) +
		//             asuint(v.c1) * uint3(0xB5D0BF63u, 0xF9102C5Fu, 0x9881FB9Fu) +
		//             asuint(v.c2) * uint3(0x56A1530Du, 0x804B722Du, 0x738E50E5u)) + 0x4FC93C25u;
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = ___v0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = L_0.get_c0_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_1, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		L_3 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)1899745391), ((int32_t)1966790317), ((int32_t)-778607417), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4;
		L_4 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_2, L_3, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_5 = ___v0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = L_5.get_c1_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_7;
		L_7 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_6, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8;
		L_8 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-1244610717), ((int32_t)-116380577), ((int32_t)-1736311905), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_9;
		L_9 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_7, L_8, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10;
		L_10 = uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3_inline(L_4, L_9, /*hidden argument*/NULL);
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_11 = ___v0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12 = L_11.get_c2_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_13;
		L_13 = math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline(L_12, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_14;
		L_14 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)1453413133), ((int32_t)-2142539219), ((int32_t)1938706661), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_15;
		L_15 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_13, L_14, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_16;
		L_16 = uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3_inline(L_10, L_15, /*hidden argument*/NULL);
		uint32_t L_17;
		L_17 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_16, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)((int32_t)1338588197)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_0 = (*(float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B *)__this);
		uint32_t L_1;
		L_1 = math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3x3({0}f, {1}f, {2}f,  {3}f, {4}f, {5}f,  {6}f, {7}f, {8}f)", c0.x, c1.x, c2.x, c0.y, c1.y, c2.y, c0.z, c1.z, c2.z);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_2 = __this->get_address_of_c0_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_7 = __this->get_address_of_c1_1();
		float L_8 = L_7->get_x_0();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_12 = __this->get_address_of_c2_2();
		float L_13 = L_12->get_x_0();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_17 = __this->get_address_of_c0_0();
		float L_18 = L_17->get_y_1();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = L_16;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_22 = __this->get_address_of_c1_1();
		float L_23 = L_22->get_y_1();
		float L_24 = L_23;
		RuntimeObject * L_25 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_25);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_26 = L_21;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_27 = __this->get_address_of_c2_2();
		float L_28 = L_27->get_y_1();
		float L_29 = L_28;
		RuntimeObject * L_30 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_26;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_32 = __this->get_address_of_c0_0();
		float L_33 = L_32->get_z_2();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_35);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_35);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_36 = L_31;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_37 = __this->get_address_of_c1_1();
		float L_38 = L_37->get_z_2();
		float L_39 = L_38;
		RuntimeObject * L_40 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_40);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_41 = L_36;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_42 = __this->get_address_of_c2_2();
		float L_43 = L_42->get_z_2();
		float L_44 = L_43;
		RuntimeObject * L_45 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_45);
		String_t* L_46;
		L_46 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E, L_41, /*hidden argument*/NULL);
		return L_46;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24_inline (float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float3x3({0}f, {1}f, {2}f,  {3}f, {4}f, {5}f,  {6}f, {7}f, {8}f)", c0.x.ToString(format, formatProvider), c1.x.ToString(format, formatProvider), c2.x.ToString(format, formatProvider), c0.y.ToString(format, formatProvider), c1.y.ToString(format, formatProvider), c2.y.ToString(format, formatProvider), c0.z.ToString(format, formatProvider), c1.z.ToString(format, formatProvider), c2.z.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_2 = __this->get_address_of_c0_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6;
		L_6 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_8 = __this->get_address_of_c1_1();
		float* L_9 = L_8->get_address_of_x_0();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12;
		L_12 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_7;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_14 = __this->get_address_of_c2_2();
		float* L_15 = L_14->get_address_of_x_0();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18;
		L_18 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = L_13;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_20 = __this->get_address_of_c0_0();
		float* L_21 = L_20->get_address_of_y_1();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24;
		L_24 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_25 = L_19;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_26 = __this->get_address_of_c1_1();
		float* L_27 = L_26->get_address_of_y_1();
		String_t* L_28 = ___format0;
		RuntimeObject* L_29 = ___formatProvider1;
		String_t* L_30;
		L_30 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_25;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_32 = __this->get_address_of_c2_2();
		float* L_33 = L_32->get_address_of_y_1();
		String_t* L_34 = ___format0;
		RuntimeObject* L_35 = ___formatProvider1;
		String_t* L_36;
		L_36 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_36);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_36);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_37 = L_31;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_38 = __this->get_address_of_c0_0();
		float* L_39 = L_38->get_address_of_z_2();
		String_t* L_40 = ___format0;
		RuntimeObject* L_41 = ___formatProvider1;
		String_t* L_42;
		L_42 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_39, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_42);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_43 = L_37;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_44 = __this->get_address_of_c1_1();
		float* L_45 = L_44->get_address_of_z_2();
		String_t* L_46 = ___format0;
		RuntimeObject* L_47 = ___formatProvider1;
		String_t* L_48;
		L_48 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_48);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_49 = L_43;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * L_50 = __this->get_address_of_c2_2();
		float* L_51 = L_50->get_address_of_z_2();
		String_t* L_52 = ___format0;
		RuntimeObject* L_53 = ___formatProvider1;
		String_t* L_54;
		L_54 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_54);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_54);
		String_t* L_55;
		L_55 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF0F176C880F59619C10D21EBC009D1D86005352E, L_49, /*hidden argument*/NULL);
		return L_55;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator + (float4 lhs, float4 rhs) { return new float4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), ((float)il2cpp_codegen_add((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// public static uint3 uint3(uint x, uint y, uint z) { return new uint3(x, y, z); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(y, x, w); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_w_3();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint3 asuint(float3 x) { return uint3(asuint(x.x), asuint(x.y), asuint(x.z)); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2;
		L_2 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_1, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5;
		L_5 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_4, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8;
		L_8 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_7, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_9;
		L_9 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator ^ (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x ^ rhs.x, lhs.y ^ rhs.y, lhs.z ^ rhs.z); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_12), ((int32_t)((int32_t)L_1^(int32_t)L_3)), ((int32_t)((int32_t)L_5^(int32_t)L_7)), ((int32_t)((int32_t)L_9^(int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___x0, const RuntimeMethod* method)
{
	{
		// public static float3 asfloat(uint3 x) { return float3(asfloat(x.x), asfloat(x.y), asfloat(x.z)); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_1, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_4, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_7, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		L_9 = math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline(L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE_inline (float ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float lhs, float3 rhs) { return new float3 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z); }
		float L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(z, w, x); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_w_3();
		float L_2 = __this->get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator - (float3 lhs, float3 rhs) { return new float3 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// public static float3 float3(float x, float y, float z) { return new float3(x, y, z); }
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(w, z, y); }
		float L_0 = __this->get_w_3();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float ___z1, float ___w2, const RuntimeMethod* method)
{
	{
		// this.x = xy.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___xy0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xy.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___xy0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = z;
		float L_4 = ___z1;
		__this->set_z_2(L_4);
		// this.w = w;
		float L_5 = ___w2;
		__this->set_w_3(L_5);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___xy0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___zw1, const RuntimeMethod* method)
{
	{
		// this.x = xy.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___xy0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xy.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___xy0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = zw.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___zw1;
		float L_5 = L_4.get_x_0();
		__this->set_z_2(L_5);
		// this.w = zw.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6 = ___zw1;
		float L_7 = L_6.get_y_1();
		__this->set_w_3(L_7);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___xyz0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___xyz0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___xyz0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		float L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___xyzw0, const RuntimeMethod* method)
{
	{
		// this.x = xyzw.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___xyzw0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyzw.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___xyzw0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyzw.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___xyzw0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = xyzw.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___xyzw0;
		float L_7 = L_6.get_w_3();
		__this->set_w_3(L_7);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		float L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		float L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		float L_2 = ___v0;
		__this->set_z_2(L_2);
		// this.w = v;
		float L_3 = ___v0;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v.x;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___v0;
		int32_t L_1 = L_0.get_x_0();
		__this->set_x_0(((float)((float)L_1)));
		// this.y = v.y;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___v0;
		int32_t L_3 = L_2.get_y_1();
		__this->set_y_1(((float)((float)L_3)));
		// this.z = v.z;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___v0;
		int32_t L_5 = L_4.get_z_2();
		__this->set_z_2(((float)((float)L_5)));
		// this.w = v.w;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___v0;
		int32_t L_7 = L_6.get_w_3();
		__this->set_w_3(((float)((float)L_7)));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(x, y, z, x); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float L_3 = __this->get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(y, z, x, y); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_x_0();
		float L_3 = __this->get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(y, z, x, z); }
		float L_0 = __this->get_y_1();
		float L_1 = __this->get_z_2();
		float L_2 = __this->get_x_0();
		float L_3 = __this->get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(z, x, y, y); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(z, x, y, z); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(w, w, w, x); }
		float L_0 = __this->get_w_3();
		float L_1 = __this->get_w_3();
		float L_2 = __this->get_w_3();
		float L_3 = __this->get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float4(w, w, w, w); }
		float L_0 = __this->get_w_3();
		float L_1 = __this->get_w_3();
		float L_2 = __this->get_w_3();
		float L_3 = __this->get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___value0, const RuntimeMethod* method)
{
	{
		// set { x = value.x; y = value.y; z = value.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___value0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// set { x = value.x; y = value.y; z = value.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___value0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// set { x = value.x; y = value.y; z = value.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___value0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// set { x = value.x; y = value.y; z = value.z; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_xy_mC5A971D69E3278438139480A25313449133571E0_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(x, y); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float2_tCB7B81181978EDE17722C533A55E345D9A413274  float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float2(z, w); }
		float L_0 = __this->get_z_2();
		float L_1 = __this->get_w_3();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		float L_0 = __this->get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs0;
		float L_2 = L_1.get_x_0();
		if ((!(((float)L_0) == ((float)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = __this->get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_3) == ((float)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		float L_6 = __this->get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs0;
		float L_8 = L_7.get_z_2();
		if ((!(((float)L_6) == ((float)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		float L_9 = __this->get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs0;
		float L_11 = L_10.get_w_3();
		return (bool)((((float)L_9) == ((float)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint4(0xE69626FFu, 0xBD010EEBu, 0x9CEDE1D1u, 0x43BE0B51u)) + 0xAF836EE1u;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_0, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-426367233), ((int32_t)-1124004117), ((int32_t)-1662131759), ((int32_t)1136528209), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3;
		L_3 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-1350340895)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = (*(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)__this);
		uint32_t L_1;
		L_1 = math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x, y, z, w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float L_2 = __this->get_x_0();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_1;
		float L_6 = __this->get_y_1();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = L_5;
		float L_10 = __this->get_z_2();
		float L_11 = L_10;
		RuntimeObject * L_12 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_9;
		float L_14 = __this->get_w_3();
		float L_15 = L_14;
		RuntimeObject * L_16 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17;
		L_17 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4({0}f, {1}f, {2}f, {3}f)", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5;
		L_5 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10;
		L_10 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15;
		L_15 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20;
		L_20 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral43DDB8ABCD260FFF159CAB7520989AEB22E45033, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method)
{
	{
		// this.c0 = c0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___c00;
		__this->set_c0_0(L_0);
		// this.c1 = c1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___c11;
		__this->set_c1_1(L_1);
		// this.c2 = c2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___c22;
		__this->set_c2_2(L_2);
		// this.c3 = c3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___c33;
		__this->set_c3_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	{
		// this.c0 = new float4(m00, m10, m20, m30);
		float L_0 = ___m000;
		float L_1 = ___m104;
		float L_2 = ___m208;
		float L_3 = ___m3012;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		__this->set_c0_0(L_4);
		// this.c1 = new float4(m01, m11, m21, m31);
		float L_5 = ___m011;
		float L_6 = ___m115;
		float L_7 = ___m219;
		float L_8 = ___m3113;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_9), L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_c1_1(L_9);
		// this.c2 = new float4(m02, m12, m22, m32);
		float L_10 = ___m022;
		float L_11 = ___m126;
		float L_12 = ___m2210;
		float L_13 = ___m3214;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14;
		memset((&L_14), 0, sizeof(L_14));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_14), L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		__this->set_c2_2(L_14);
		// this.c3 = new float4(m03, m13, m23, m33);
		float L_15 = ___m033;
		float L_16 = ___m137;
		float L_17 = ___m2311;
		float L_18 = ___m3315;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_19;
		memset((&L_19), 0, sizeof(L_19));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_19), L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		__this->set_c3_3(L_19);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float rhs) { return new float4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float lhs, float4 rhs) { return new float4 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w); }
		float L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float L_9 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator - (float4 lhs, float4 rhs) { return new float4 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), ((float)il2cpp_codegen_subtract((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float4 lhs, float rhs) { return new float4 (lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), ((float)((float)L_10/(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___val0, const RuntimeMethod* method)
{
	{
		// public static float4 operator - (float4 val) { return new float4 (-val.x, -val.y, -val.z, -val.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___val0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___val0;
		float L_3 = L_2.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___val0;
		float L_5 = L_4.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___val0;
		float L_7 = L_6.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		memset((&L_8), 0, sizeof(L_8));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_8), ((-L_1)), ((-L_3)), ((-L_5)), ((-L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(float4x4 rhs) { return c0.Equals(rhs.c0) && c1.Equals(rhs.c1) && c2.Equals(rhs.c2) && c3.Equals(rhs.c3); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = __this->get_address_of_c0_0();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_1 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = L_1.get_c0_0();
		bool L_3;
		L_3 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004b;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_4 = __this->get_address_of_c1_1();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		bool L_7;
		L_7 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004b;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_8 = __this->get_address_of_c2_2();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_9 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = L_9.get_c2_2();
		bool L_11;
		L_11 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_8, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = __this->get_address_of_c3_3();
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_13 = ___rhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = L_13.get_c3_3();
		bool L_15;
		L_15 = float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_12, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_004b:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m16C61605FBE531F8EB703823255407075EA5A464_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v.c0) * uint4(0xC4B1493Fu, 0xBA0966D3u, 0xAFBEE253u, 0x5B419C01u) +
		//             asuint(v.c1) * uint4(0x515D90F5u, 0xEC9F68F3u, 0xF9EA92D5u, 0xC2FAFCB9u) +
		//             asuint(v.c2) * uint4(0x616E9CA1u, 0xC5C5394Bu, 0xCAE78587u, 0x7A1541C9u) +
		//             asuint(v.c3) * uint4(0xF83BD927u, 0x6A243BCBu, 0x509B84C9u, 0x91D13847u)) + 0x52F7230Fu;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_c0_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_1, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3;
		L_3 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-995014337), ((int32_t)-1173788973), ((int32_t)-1346444717), ((int32_t)1531026433), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4;
		L_4 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_2, L_3, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_5 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = L_5.get_c1_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_7;
		L_7 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_6, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8;
		L_8 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)1365086453), ((int32_t)-325097229), ((int32_t)-102067499), ((int32_t)-1023738695), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_9;
		L_9 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_7, L_8, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10;
		L_10 = uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline(L_4, L_9, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_11 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = L_11.get_c2_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_13;
		L_13 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_12, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_14;
		L_14 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)1634639009), ((int32_t)-976930485), ((int32_t)-890796665), ((int32_t)2048213449), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_15;
		L_15 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_13, L_14, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_16;
		L_16 = uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline(L_10, L_15, /*hidden argument*/NULL);
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_17 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_c3_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_19;
		L_19 = math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline(L_18, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_20;
		L_20 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-130295513), ((int32_t)1780759499), ((int32_t)1352369353), ((int32_t)-1848559545), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_21;
		L_21 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_19, L_20, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_22;
		L_22 = uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline(L_16, L_21, /*hidden argument*/NULL);
		uint32_t L_23;
		L_23 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_22, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)((int32_t)1391928079)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_0 = (*(float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m16C61605FBE531F8EB703823255407075EA5A464_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x, c1.x, c2.x, c3.x, c0.y, c1.y, c2.y, c3.y, c0.z, c1.z, c2.z, c3.z, c0.w, c1.w, c2.w, c3.w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_c0_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_7 = __this->get_address_of_c1_1();
		float L_8 = L_7->get_x_0();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = __this->get_address_of_c2_2();
		float L_13 = L_12->get_x_0();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_17 = __this->get_address_of_c3_3();
		float L_18 = L_17->get_x_0();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = L_16;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_22 = __this->get_address_of_c0_0();
		float L_23 = L_22->get_y_1();
		float L_24 = L_23;
		RuntimeObject * L_25 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_25);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_25);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_26 = L_21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_27 = __this->get_address_of_c1_1();
		float L_28 = L_27->get_y_1();
		float L_29 = L_28;
		RuntimeObject * L_30 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_29);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_30);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_32 = __this->get_address_of_c2_2();
		float L_33 = L_32->get_y_1();
		float L_34 = L_33;
		RuntimeObject * L_35 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_35);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_35);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_36 = L_31;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_37 = __this->get_address_of_c3_3();
		float L_38 = L_37->get_y_1();
		float L_39 = L_38;
		RuntimeObject * L_40 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_40);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_40);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_41 = L_36;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_42 = __this->get_address_of_c0_0();
		float L_43 = L_42->get_z_2();
		float L_44 = L_43;
		RuntimeObject * L_45 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_44);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_45);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_45);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_46 = L_41;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_47 = __this->get_address_of_c1_1();
		float L_48 = L_47->get_z_2();
		float L_49 = L_48;
		RuntimeObject * L_50 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_49);
		NullCheck(L_46);
		ArrayElementTypeCheck (L_46, L_50);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_50);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_51 = L_46;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_52 = __this->get_address_of_c2_2();
		float L_53 = L_52->get_z_2();
		float L_54 = L_53;
		RuntimeObject * L_55 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_55);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_55);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_56 = L_51;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_57 = __this->get_address_of_c3_3();
		float L_58 = L_57->get_z_2();
		float L_59 = L_58;
		RuntimeObject * L_60 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_59);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_60);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_60);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_61 = L_56;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_62 = __this->get_address_of_c0_0();
		float L_63 = L_62->get_w_3();
		float L_64 = L_63;
		RuntimeObject * L_65 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_65);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_65);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_66 = L_61;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_67 = __this->get_address_of_c1_1();
		float L_68 = L_67->get_w_3();
		float L_69 = L_68;
		RuntimeObject * L_70 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_69);
		NullCheck(L_66);
		ArrayElementTypeCheck (L_66, L_70);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_70);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_71 = L_66;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_72 = __this->get_address_of_c2_2();
		float L_73 = L_72->get_w_3();
		float L_74 = L_73;
		RuntimeObject * L_75 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_74);
		NullCheck(L_71);
		ArrayElementTypeCheck (L_71, L_75);
		(L_71)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_75);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_76 = L_71;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_77 = __this->get_address_of_c3_3();
		float L_78 = L_77->get_w_3();
		float L_79 = L_78;
		RuntimeObject * L_80 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_79);
		NullCheck(L_76);
		ArrayElementTypeCheck (L_76, L_80);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_80);
		String_t* L_81;
		L_81 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E, L_76, /*hidden argument*/NULL);
		return L_81;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2_inline (float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("float4x4({0}f, {1}f, {2}f, {3}f,  {4}f, {5}f, {6}f, {7}f,  {8}f, {9}f, {10}f, {11}f,  {12}f, {13}f, {14}f, {15}f)", c0.x.ToString(format, formatProvider), c1.x.ToString(format, formatProvider), c2.x.ToString(format, formatProvider), c3.x.ToString(format, formatProvider), c0.y.ToString(format, formatProvider), c1.y.ToString(format, formatProvider), c2.y.ToString(format, formatProvider), c3.y.ToString(format, formatProvider), c0.z.ToString(format, formatProvider), c1.z.ToString(format, formatProvider), c2.z.ToString(format, formatProvider), c3.z.ToString(format, formatProvider), c0.w.ToString(format, formatProvider), c1.w.ToString(format, formatProvider), c2.w.ToString(format, formatProvider), c3.w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_c0_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6;
		L_6 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_8 = __this->get_address_of_c1_1();
		float* L_9 = L_8->get_address_of_x_0();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12;
		L_12 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_14 = __this->get_address_of_c2_2();
		float* L_15 = L_14->get_address_of_x_0();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18;
		L_18 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = L_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_20 = __this->get_address_of_c3_3();
		float* L_21 = L_20->get_address_of_x_0();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24;
		L_24 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_25 = L_19;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_26 = __this->get_address_of_c0_0();
		float* L_27 = L_26->get_address_of_y_1();
		String_t* L_28 = ___format0;
		RuntimeObject* L_29 = ___formatProvider1;
		String_t* L_30;
		L_30 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_27, L_28, L_29, /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_30);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_30);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_31 = L_25;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_32 = __this->get_address_of_c1_1();
		float* L_33 = L_32->get_address_of_y_1();
		String_t* L_34 = ___format0;
		RuntimeObject* L_35 = ___formatProvider1;
		String_t* L_36;
		L_36 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_33, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_36);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_36);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_37 = L_31;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_38 = __this->get_address_of_c2_2();
		float* L_39 = L_38->get_address_of_y_1();
		String_t* L_40 = ___format0;
		RuntimeObject* L_41 = ___formatProvider1;
		String_t* L_42;
		L_42 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_39, L_40, L_41, /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_42);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_42);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_43 = L_37;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_44 = __this->get_address_of_c3_3();
		float* L_45 = L_44->get_address_of_y_1();
		String_t* L_46 = ___format0;
		RuntimeObject* L_47 = ___formatProvider1;
		String_t* L_48;
		L_48 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_45, L_46, L_47, /*hidden argument*/NULL);
		NullCheck(L_43);
		ArrayElementTypeCheck (L_43, L_48);
		(L_43)->SetAt(static_cast<il2cpp_array_size_t>(7), (RuntimeObject *)L_48);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_49 = L_43;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_50 = __this->get_address_of_c0_0();
		float* L_51 = L_50->get_address_of_z_2();
		String_t* L_52 = ___format0;
		RuntimeObject* L_53 = ___formatProvider1;
		String_t* L_54;
		L_54 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_51, L_52, L_53, /*hidden argument*/NULL);
		NullCheck(L_49);
		ArrayElementTypeCheck (L_49, L_54);
		(L_49)->SetAt(static_cast<il2cpp_array_size_t>(8), (RuntimeObject *)L_54);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_55 = L_49;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_56 = __this->get_address_of_c1_1();
		float* L_57 = L_56->get_address_of_z_2();
		String_t* L_58 = ___format0;
		RuntimeObject* L_59 = ___formatProvider1;
		String_t* L_60;
		L_60 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_57, L_58, L_59, /*hidden argument*/NULL);
		NullCheck(L_55);
		ArrayElementTypeCheck (L_55, L_60);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)9)), (RuntimeObject *)L_60);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_61 = L_55;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_62 = __this->get_address_of_c2_2();
		float* L_63 = L_62->get_address_of_z_2();
		String_t* L_64 = ___format0;
		RuntimeObject* L_65 = ___formatProvider1;
		String_t* L_66;
		L_66 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_63, L_64, L_65, /*hidden argument*/NULL);
		NullCheck(L_61);
		ArrayElementTypeCheck (L_61, L_66);
		(L_61)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)10)), (RuntimeObject *)L_66);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_67 = L_61;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_68 = __this->get_address_of_c3_3();
		float* L_69 = L_68->get_address_of_z_2();
		String_t* L_70 = ___format0;
		RuntimeObject* L_71 = ___formatProvider1;
		String_t* L_72;
		L_72 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_69, L_70, L_71, /*hidden argument*/NULL);
		NullCheck(L_67);
		ArrayElementTypeCheck (L_67, L_72);
		(L_67)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)11)), (RuntimeObject *)L_72);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_73 = L_67;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_74 = __this->get_address_of_c0_0();
		float* L_75 = L_74->get_address_of_w_3();
		String_t* L_76 = ___format0;
		RuntimeObject* L_77 = ___formatProvider1;
		String_t* L_78;
		L_78 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_75, L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_73);
		ArrayElementTypeCheck (L_73, L_78);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)12)), (RuntimeObject *)L_78);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_79 = L_73;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_80 = __this->get_address_of_c1_1();
		float* L_81 = L_80->get_address_of_w_3();
		String_t* L_82 = ___format0;
		RuntimeObject* L_83 = ___formatProvider1;
		String_t* L_84;
		L_84 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_81, L_82, L_83, /*hidden argument*/NULL);
		NullCheck(L_79);
		ArrayElementTypeCheck (L_79, L_84);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)13)), (RuntimeObject *)L_84);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_85 = L_79;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_86 = __this->get_address_of_c2_2();
		float* L_87 = L_86->get_address_of_w_3();
		String_t* L_88 = ___format0;
		RuntimeObject* L_89 = ___formatProvider1;
		String_t* L_90;
		L_90 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_87, L_88, L_89, /*hidden argument*/NULL);
		NullCheck(L_85);
		ArrayElementTypeCheck (L_85, L_90);
		(L_85)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)14)), (RuntimeObject *)L_90);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_91 = L_85;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_92 = __this->get_address_of_c3_3();
		float* L_93 = L_92->get_address_of_w_3();
		String_t* L_94 = ___format0;
		RuntimeObject* L_95 = ___formatProvider1;
		String_t* L_96;
		L_96 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_93, L_94, L_95, /*hidden argument*/NULL);
		NullCheck(L_91);
		ArrayElementTypeCheck (L_91, L_96);
		(L_91)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)15)), (RuntimeObject *)L_96);
		String_t* L_97;
		L_97 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralF7E2930F09BF167DEB7CA0E99AC2A430763B191E, L_91, /*hidden argument*/NULL);
		return L_97;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF_inline (float ___m000, float ___m011, float ___m022, float ___m033, float ___m104, float ___m115, float ___m126, float ___m137, float ___m208, float ___m219, float ___m2210, float ___m2311, float ___m3012, float ___m3113, float ___m3214, float ___m3315, const RuntimeMethod* method)
{
	{
		// return new float4x4(m00, m01, m02, m03,
		//                     m10, m11, m12, m13,
		//                     m20, m21, m22, m23,
		//                     m30, m31, m32, m33);
		float L_0 = ___m000;
		float L_1 = ___m011;
		float L_2 = ___m022;
		float L_3 = ___m033;
		float L_4 = ___m104;
		float L_5 = ___m115;
		float L_6 = ___m126;
		float L_7 = ___m137;
		float L_8 = ___m208;
		float L_9 = ___m219;
		float L_10 = ___m2210;
		float L_11 = ___m2311;
		float L_12 = ___m3012;
		float L_13 = ___m3113;
		float L_14 = ___m3214;
		float L_15 = ___m3315;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_inline((&L_16), L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  float4x4_Scale_mEE04A811A295C99F280056A51ABC83356CB0A4A3_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// return float4x4(x,    0.0f, 0.0f, 0.0f,
		//                 0.0f, y,    0.0f, 0.0f,
		//                 0.0f, 0.0f, z,    0.0f,
		//                 0.0f, 0.0f, 0.0f, 1.0f);
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_3;
		L_3 = math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF_inline(L_0, (0.0f), (0.0f), (0.0f), (0.0f), L_1, (0.0f), (0.0f), (0.0f), (0.0f), L_2, (0.0f), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  math_float3x3_m083898B3E74368DC86FB3D19406C3ED60AF63583_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___rotation0, const RuntimeMethod* method)
{
	{
		// return new float3x3(rotation);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___rotation0;
		float3x3_tD413D3D05958554CE3C4802B5DC267A4D279286B  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___xyz0, float ___w1, const RuntimeMethod* method)
{
	{
		// public static float4 float4(float3 xyz, float w) { return new float4(xyz, w); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___xyz0;
		float L_1 = ___w1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		memset((&L_2), 0, sizeof(L_2));
		float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c00, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c11, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c22, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___c33, const RuntimeMethod* method)
{
	{
		// public static float4x4 float4x4(float4 c0, float4 c1, float4 c2, float4 c3) { return new float4x4(c0, c1, c2, c3); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___c00;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___c11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___c22;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___c33;
		float4x4_tFC2111BDB9B27BAD783B2B962A5C45D560D074A7  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(int2 rhs) { return x == rhs.x && y == rhs.y; }
		int32_t L_0 = __this->get_x_0();
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_1 = ___rhs0;
		int32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_3 = __this->get_y_1();
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_4 = ___rhs0;
		int32_t L_5 = L_4.get_y_1();
		return (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint2(0x83B58237u, 0x833E3E29u)) + 0xA9D919BFu;
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_0 = ___v0;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1;
		L_1 = math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B_inline(L_0, /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		L_2 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(((int32_t)-2085256649), ((int32_t)-2093072855), /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_3;
		L_3 = uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-1445389889)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_0 = (*(int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int2({0}, {1})", x, y);
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = __this->get_y_1();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int2({0}, {1})", x.ToString(format, formatProvider), y.ToString(format, formatProvider));
		int32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteral039B19E6A63E9AA22F1AF2817559D285768A7B4A, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		int32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		int32_t L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		int32_t L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		int32_t L_2 = ___v0;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// this.z = (int)v.z;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(il2cpp_codegen_cast_double_to_int<int32_t>(L_5));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(int3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		int32_t L_0 = __this->get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_1 = ___rhs0;
		int32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = __this->get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___rhs0;
		int32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_6 = __this->get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_7 = ___rhs0;
		int32_t L_8 = L_7.get_z_2();
		return (bool)((((int32_t)L_6) == ((int32_t)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint3(0x4C7F6DD1u, 0x4822A3E9u, 0xAAC3C25Du)) + 0xD21D0945u;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___v0;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1;
		L_1 = math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B_inline(L_0, /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)1283419601), ((int32_t)1210229737), ((int32_t)-1430011299), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_3;
		L_3 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)-769849019)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = (*(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int3({0}, {1}, {2})", x, y, z);
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = __this->get_y_1();
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_4);
		int32_t L_6 = __this->get_z_2();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int3({0}, {1}, {2})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		int32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		int32_t* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11;
		L_11 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteral0245A598901314C98F3EE8F1FABA26B281081D8E, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		int32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		int32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___xyz0, int32_t ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___xyz0;
		int32_t L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___xyz0;
		int32_t L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___xyz0;
		int32_t L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		int32_t L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// this.z = (int)v.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(il2cpp_codegen_cast_double_to_int<int32_t>(L_5));
		// this.w = (int)v.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___v0;
		float L_7 = L_6.get_w_3();
		__this->set_w_3(il2cpp_codegen_cast_double_to_int<int32_t>(L_7));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	{
		// get { return new int3(x, y, z); }
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = __this->get_y_1();
		int32_t L_2 = __this->get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_3;
		memset((&L_3), 0, sizeof(L_3));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(int4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		int32_t L_0 = __this->get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_1 = ___rhs0;
		int32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = __this->get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___rhs0;
		int32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_6 = __this->get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_7 = ___rhs0;
		int32_t L_8 = L_7.get_z_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_9 = __this->get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_10 = ___rhs0;
		int32_t L_11 = L_10.get_w_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(asuint(v) * uint4(0x6E050B01u, 0x750FDBF5u, 0x7F3DD499u, 0x52EAAEBBu)) + 0x4599C793u;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___v0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D_inline(L_0, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)1845824257), ((int32_t)1963973621), ((int32_t)2134758553), ((int32_t)1391111867), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3;
		L_3 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_1, L_2, /*hidden argument*/NULL);
		uint32_t L_4;
		L_4 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_3, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)((int32_t)1167706003)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = (*(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)__this);
		uint32_t L_1;
		L_1 = math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int4({0}, {1}, {2}, {3})", x, y, z, w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		int32_t L_2 = __this->get_x_0();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_1;
		int32_t L_6 = __this->get_y_1();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = L_5;
		int32_t L_10 = __this->get_z_2();
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_9;
		int32_t L_14 = __this->get_w_3();
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17;
		L_17 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("int4({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		int32_t* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5;
		L_5 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		int32_t* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10;
		L_10 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		int32_t* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15;
		L_15 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		int32_t* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20;
		L_20 = Int32_ToString_m246774E1922012AE787EB97743F42CB798B70CD8((int32_t*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral1A3E5FB6190816CC196322B8E72BE4E64C6811E6, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint2 asuint(float2 x) { return uint2(asuint(x.x), asuint(x.y)); }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2;
		L_2 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_1, /*hidden argument*/NULL);
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5;
		L_5 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_4, /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_6;
		L_6 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline (uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static uint2 uint2(uint x, uint y) { return new uint2(x, y); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		memset((&L_2), 0, sizeof(L_2));
		uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___lhs0, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint2 operator * (uint2 lhs, uint2 rhs) { return new uint2 (lhs.x * rhs.x, lhs.y * rhs.y); }
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_8;
		memset((&L_8), 0, sizeof(L_8));
		uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline((&L_8), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint2 x) { return x.x + x.y; }
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		return ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator * (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_12), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint3 x) { return x.x + x.y + x.z; }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___lhs0, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint3 operator + (uint3 lhs, uint3 rhs) { return new uint3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline((&L_12), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint4 asuint(float4 x) { return uint4(asuint(x.x), asuint(x.y), asuint(x.z), asuint(x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		uint32_t L_2;
		L_2 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_1, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		uint32_t L_5;
		L_5 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		uint32_t L_8;
		L_8 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		uint32_t L_11;
		L_11 = math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline(L_10, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12;
		L_12 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline (uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// public static uint4 uint4(uint x, uint y, uint z, uint w) { return new uint4(x, y, z, w); }
		uint32_t L_0 = ___x0;
		uint32_t L_1 = ___y1;
		uint32_t L_2 = ___z2;
		uint32_t L_3 = ___w3;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4;
		memset((&L_4), 0, sizeof(L_4));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator * (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_16), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint csum(uint4 x) { return x.x + x.y + x.z + x.w; }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2 = ___x0;
		uint32_t L_3 = L_2.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___x0;
		uint32_t L_5 = L_4.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___x0;
		uint32_t L_7 = L_6.get_w_3();
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), (int32_t)L_5)), (int32_t)L_7));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, uint8_t ___component2, const RuntimeMethod* method)
{
	{
		uint8_t L_0 = ___component2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_002f;
			}
			case 2:
			{
				goto IL_0036;
			}
			case 3:
			{
				goto IL_003d;
			}
			case 4:
			{
				goto IL_0044;
			}
			case 5:
			{
				goto IL_004b;
			}
			case 6:
			{
				goto IL_0052;
			}
			case 7:
			{
				goto IL_0059;
			}
		}
	}
	{
		goto IL_0060;
	}

IL_0028:
	{
		// return a.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___a0;
		float L_2 = L_1.get_x_0();
		return L_2;
	}

IL_002f:
	{
		// return a.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		return L_4;
	}

IL_0036:
	{
		// return a.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___a0;
		float L_6 = L_5.get_z_2();
		return L_6;
	}

IL_003d:
	{
		// return a.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___a0;
		float L_8 = L_7.get_w_3();
		return L_8;
	}

IL_0044:
	{
		// return b.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___b1;
		float L_10 = L_9.get_x_0();
		return L_10;
	}

IL_004b:
	{
		// return b.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11 = ___b1;
		float L_12 = L_11.get_y_1();
		return L_12;
	}

IL_0052:
	{
		// return b.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = ___b1;
		float L_14 = L_13.get_z_2();
		return L_14;
	}

IL_0059:
	{
		// return b.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = ___b1;
		float L_16 = L_15.get_w_3();
		return L_16;
	}

IL_0060:
	{
		// throw new System.ArgumentException("Invalid shuffle component: " + component);
		RuntimeObject * L_17 = Box(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ShuffleComponent_t6796DE67FD5D2B89865C06815789C5F33EB1D07A_il2cpp_TypeInfo_var)), (&___component2));
		NullCheck(L_17);
		String_t* L_18;
		L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		___component2 = *(uint8_t*)UnBox(L_17);
		String_t* L_19;
		L_19 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral579BEF9273F6F30843464D2DA589060200686F6A)), L_18, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_20 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_20, L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_RuntimeMethod_var)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline (float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// public static float4 float4(float x, float y, float z, float w) { return new float4(x, y, z, w); }
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		float L_3 = ___w3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		memset((&L_4), 0, sizeof(L_4));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_4), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method)
{
	{
		// return shuffle(a, b, ShuffleComponent.LeftX, ShuffleComponent.LeftY, ShuffleComponent.RightX, ShuffleComponent.RightY);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		L_2 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_0, L_1, 0, 1, 4, 5, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, const RuntimeMethod* method)
{
	{
		// return shuffle(b, a, ShuffleComponent.LeftZ, ShuffleComponent.LeftW, ShuffleComponent.RightZ, ShuffleComponent.RightW);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2;
		L_2 = math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline(L_0, L_1, 2, 3, 6, 7, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, uint8_t ___x2, uint8_t ___y3, uint8_t ___z4, uint8_t ___w5, const RuntimeMethod* method)
{
	{
		// return float4(
		//     select_shuffle_component(a, b, x),
		//     select_shuffle_component(a, b, y),
		//     select_shuffle_component(a, b, z),
		//     select_shuffle_component(a, b, w));
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___b1;
		uint8_t L_2 = ___x2;
		float L_3;
		L_3 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_0, L_1, L_2, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___b1;
		uint8_t L_6 = ___y3;
		float L_7;
		L_7 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_4, L_5, L_6, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___b1;
		uint8_t L_10 = ___z4;
		float L_11;
		L_11 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_8, L_9, L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = ___b1;
		uint8_t L_14 = ___w5;
		float L_15;
		L_15 = math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC_inline(L_12, L_13, L_14, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		L_16 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline(L_3, L_7, L_11, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float4 rhs) { return new float4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), ((float)il2cpp_codegen_multiply((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_float4_m6D1066B47EE7BC7A47A136D969B5A3CA41902224_inline (float ___v0, const RuntimeMethod* method)
{
	{
		// public static float4 float4(float v) { return new float4(v); }
		float L_0 = ___v0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1;
		memset((&L_1), 0, sizeof(L_1));
		float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m489385B46636C7B5CCE01DEECA0A603A2EE807AA_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float4 lhs, float4 rhs) { return new float4 (lhs.x / rhs.x, lhs.y / rhs.y, lhs.z / rhs.z, lhs.w / rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)((float)L_1/(float)L_3)), ((float)((float)L_5/(float)L_7)), ((float)((float)L_9/(float)L_11)), ((float)((float)L_13/(float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator + (uint4 lhs, uint4 rhs) { return new uint4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2 = ___rhs1;
		uint32_t L_3 = L_2.get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___lhs0;
		uint32_t L_5 = L_4.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___rhs1;
		uint32_t L_7 = L_6.get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8 = ___lhs0;
		uint32_t L_9 = L_8.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10 = ___rhs1;
		uint32_t L_11 = L_10.get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12 = ___lhs0;
		uint32_t L_13 = L_12.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_14 = ___rhs1;
		uint32_t L_15 = L_14.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_16;
		memset((&L_16), 0, sizeof(L_16));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_16), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint2_t31B88562B6681D249453803230869FBE9ED565E7  math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B_inline (int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint2 asuint(int2 x) { return uint2((uint)x.x, (uint)x.y); }
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int2_t40DB794B47A2943AB7E9BC67442B6B8173FBB568  L_2 = ___x0;
		int32_t L_3 = L_2.get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_4;
		L_4 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint3 asuint(int3 x) { return uint3((uint)x.x, (uint)x.y, (uint)x.z); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___x0;
		int32_t L_3 = L_2.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___x0;
		int32_t L_5 = L_4.get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_6;
		L_6 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(L_1, L_3, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___x0, const RuntimeMethod* method)
{
	{
		// public static uint4 asuint(int4 x) { return uint4((uint)x.x, (uint)x.y, (uint)x.z, (uint)x.w); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___x0;
		int32_t L_1 = L_0.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___x0;
		int32_t L_3 = L_2.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___x0;
		int32_t L_5 = L_4.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___x0;
		int32_t L_7 = L_6.get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_8;
		L_8 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(L_1, L_3, L_5, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86_inline (float ___x0, const RuntimeMethod* method)
{
	IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// u.intValue = 0;
		(&V_0)->set_intValue_0(0);
		// u.floatValue = x;
		float L_0 = ___x0;
		(&V_0)->set_floatValue_1(L_0);
		// return u.intValue;
		IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  L_1 = V_0;
		int32_t L_2 = L_1.get_intValue_0();
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC_inline (float ___x0, const RuntimeMethod* method)
{
	{
		// public static uint asuint(float x) { return (uint)asint(x); }
		float L_0 = ___x0;
		int32_t L_1;
		L_1 = math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181_inline (int32_t ___x0, const RuntimeMethod* method)
{
	IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// u.floatValue = 0;
		(&V_0)->set_floatValue_1((0.0f));
		// u.intValue = x;
		int32_t L_0 = ___x0;
		(&V_0)->set_intValue_0(L_0);
		// return u.floatValue;
		IntFloatUnion_t72279F27DE421D0251D66DEE108164593D88F463  L_1 = V_0;
		float L_2 = L_1.get_floatValue_1();
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline (uint32_t ___x0, const RuntimeMethod* method)
{
	{
		// public static float  asfloat(uint x) { return asfloat((int)x); }
		uint32_t L_0 = ___x0;
		float L_1;
		L_1 = math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A_inline (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static int min(int x, int y) { return x < y ? x : y; }
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int32_t L_3 = ___x0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float min(float x, float y) { return float.IsNaN(y) || x < y ? x : y; }
		float L_0 = ___y1;
		bool L_1;
		L_1 = Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) < ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float max(float x, float y) { return float.IsNaN(y) || x > y ? x : y; }
		float L_0 = ___y1;
		bool L_1;
		L_1 = Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) > ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF_inline (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static int max(int x, int y) { return x > y ? x : y; }
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int32_t L_3 = ___x0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_min_mC45C7024F6850EB1D382A2B2F6F31CCA4B2750E6_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float4 min(float4 x, float4 y) { return new float4(min(x.x, y.x), min(x.y, y.y), min(x.z, y.z), min(x.w, y.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14;
		L_14 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_11, L_13, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = ___x0;
		float L_16 = L_15.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17 = ___y1;
		float L_18 = L_17.get_w_3();
		float L_19;
		L_19 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_16, L_18, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20;
		memset((&L_20), 0, sizeof(L_20));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_20), L_4, L_9, L_14, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_max_m5A2AD47597A69870F6E5A8A2ABEE9A1EAC771393_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float4 max(float4 x, float4 y) { return new float4(max(x.x, y.x), max(x.y, y.y), max(x.z, y.z), max(x.w, y.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_1, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5 = ___x0;
		float L_6 = L_5.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___y1;
		float L_8 = L_7.get_y_1();
		float L_9;
		L_9 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_6, L_8, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___x0;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___y1;
		float L_13 = L_12.get_z_2();
		float L_14;
		L_14 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_11, L_13, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = ___x0;
		float L_16 = L_15.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17 = ___y1;
		float L_18 = L_17.get_w_3();
		float L_19;
		L_19 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_16, L_18, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20;
		memset((&L_20), 0, sizeof(L_20));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_20), L_4, L_9, L_14, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_clamp_m3FDC8AAE05842733E193852C929DAAC6F5E8C931_inline (float ___x0, float ___a1, float ___b2, const RuntimeMethod* method)
{
	{
		// public static float clamp(float x, float a, float b) { return max(a, min(b, x)); }
		float L_0 = ___a1;
		float L_1 = ___b2;
		float L_2 = ___x0;
		float L_3;
		L_3 = math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89_inline(L_1, L_2, /*hidden argument*/NULL);
		float L_4;
		L_4 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_0, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  uint4_op_BitwiseAnd_m8BDB3A45AD663DDFBC3E18A0EA84B3C902767C36_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___lhs0, uint32_t ___rhs1, const RuntimeMethod* method)
{
	{
		// public static uint4 operator & (uint4 lhs, uint rhs) { return new uint4 (lhs.x & rhs, lhs.y & rhs, lhs.z & rhs, lhs.w & rhs); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___lhs0;
		uint32_t L_1 = L_0.get_x_0();
		uint32_t L_2 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3 = ___lhs0;
		uint32_t L_4 = L_3.get_y_1();
		uint32_t L_5 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___lhs0;
		uint32_t L_7 = L_6.get_z_2();
		uint32_t L_8 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_9 = ___lhs0;
		uint32_t L_10 = L_9.get_w_3();
		uint32_t L_11 = ___rhs1;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_12;
		memset((&L_12), 0, sizeof(L_12));
		uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline((&L_12), ((int32_t)((int32_t)L_1&(int32_t)L_2)), ((int32_t)((int32_t)L_4&(int32_t)L_5)), ((int32_t)((int32_t)L_7&(int32_t)L_8)), ((int32_t)((int32_t)L_10&(int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_asfloat_mD9B4C5BBB6AC641027D94C2045C4C3AF416DFF6D_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 asfloat(uint4 x) { return float4(asfloat(x.x), asfloat(x.y), asfloat(x.z), asfloat(x.w)); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___x0;
		uint32_t L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_1, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_3 = ___x0;
		uint32_t L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_4, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_6 = ___x0;
		uint32_t L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_7, /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_9 = ___x0;
		uint32_t L_10 = L_9.get_w_3();
		float L_11;
		L_11 = math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF_inline(L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		L_12 = math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6_inline(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float floor(float x) { return (float)System.Math.Floor((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = floor(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Division_mE2739F8C6E0ADC06D8DCD923F9828B75EACB3ECF_inline (float ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator / (float lhs, float3 rhs) { return new float3 (lhs / rhs.x, lhs / rhs.y, lhs / rhs.z); }
		float L_0 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_9;
		memset((&L_9), 0, sizeof(L_9));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_9), ((float)((float)L_0/(float)L_2)), ((float)((float)L_3/(float)L_5)), ((float)((float)L_6/(float)L_8)), /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m847947B86BE29A43C35E74AB168A525866AA4B15_inline (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float lhs, float4 rhs) { return new float4 (lhs / rhs.x, lhs / rhs.y, lhs / rhs.z, lhs / rhs.w); }
		float L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float L_9 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)((float)L_0/(float)L_2)), ((float)((float)L_3/(float)L_5)), ((float)((float)L_6/(float)L_8)), ((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline (float ___x0, const RuntimeMethod* method)
{
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	{
		// public static float sign(float x) { return (x > 0.0f ? 1.0f : 0.0f) - (x < 0.0f ? 1.0f : 0.0f); }
		float L_0 = ___x0;
		if ((((float)L_0) > ((float)(0.0f))))
		{
			goto IL_000f;
		}
	}
	{
		G_B3_0 = (0.0f);
		goto IL_0014;
	}

IL_000f:
	{
		G_B3_0 = (1.0f);
	}

IL_0014:
	{
		float L_1 = ___x0;
		G_B4_0 = G_B3_0;
		if ((((float)L_1) < ((float)(0.0f))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0023;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		goto IL_0028;
	}

IL_0023:
	{
		G_B6_0 = (1.0f);
		G_B6_1 = G_B5_0;
	}

IL_0028:
	{
		return ((float)il2cpp_codegen_subtract((float)G_B6_1, (float)G_B6_0));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float sqrt(float x) { return (float)System.Math.Sqrt((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sqrt(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float3 x, float3 y) { return x.x * y.x + x.y * y.y + x.z * y.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float rsqrt(float x) { return 1.0f / sqrt(x); }
		float L_0 = ___x0;
		float L_1;
		L_1 = math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline(L_0, /*hidden argument*/NULL);
		return ((float)((float)(1.0f)/(float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_select_mD6522E9D958653C41F4B2E3463B79F7A011E60AB_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___a0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___b1, bool ___c2, const RuntimeMethod* method)
{
	{
		// public static float3 select(float3 a, float3 b, bool c) { return c ? b : a; }
		bool L_0 = ___c2;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1 = ___a0;
		return L_1;
	}

IL_0005:
	{
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___b1;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float4 x, float4 y) { return x.x * y.x + x.y * y.y + x.z * y.z + x.w * y.w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___x0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___y1;
		float L_15 = L_14.get_w_3();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)))), (float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_15))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_select_m3FF6FBC5429C94D76C025979A6D3DCA2B2B62602_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___a0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___b1, bool ___c2, const RuntimeMethod* method)
{
	{
		// public static float4 select(float4 a, float4 b, bool c) { return c ? b : a; }
		bool L_0 = ___c2;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___a0;
		return L_1;
	}

IL_0005:
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___b1;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_m6238E071DBA53F376473FFEC28706C9B537E6BAE_inline (float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___x0, float2_tCB7B81181978EDE17722C533A55E345D9A413274  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float2 x, float2 y) { return x.x * y.x + x.y * y.y; }
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float2_tCB7B81181978EDE17722C533A55E345D9A413274  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___lhs0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float3 operator * (float3 lhs, float3 rhs) { return new float3 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z); }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float sin(float x) { return (float)System.Math.Sin((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sin(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float cos(float x) { return (float)System.Math.Cos(x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = cos(((double)((double)L_0)));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_lzcnt_m69123CF8445E0B67FA30F24BA42F663C105BE7EB_inline (uint32_t ___x0, const RuntimeMethod* method)
{
	LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (x == 0)
		uint32_t L_0 = ___x0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		// return 32;
		return ((int32_t)32);
	}

IL_0006:
	{
		// u.doubleValue = 0.0;
		(&V_0)->set_doubleValue_1((0.0));
		// u.longValue = 0x4330000000000000L + x;
		uint32_t L_1 = ___x0;
		(&V_0)->set_longValue_0(((int64_t)il2cpp_codegen_add((int64_t)((int64_t)4841369599423283200LL), (int64_t)((int64_t)((uint64_t)L_1)))));
		// u.doubleValue -= 4503599627370496.0;
		double* L_2 = (&V_0)->get_address_of_doubleValue_1();
		double* L_3 = L_2;
		double L_4 = *((double*)L_3);
		*((double*)L_3) = (double)((double)il2cpp_codegen_subtract((double)L_4, (double)(4503599627370496.0)));
		// return 0x41E - (int)(u.longValue >> 52);
		LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  L_5 = V_0;
		int64_t L_6 = L_5.get_longValue_0();
		return ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)1054), (int32_t)((int32_t)((int32_t)((int64_t)((int64_t)L_6>>(int32_t)((int32_t)52)))))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_tzcnt_m3A483A42E55BE961B589C59F99EE7972D0D296E8_inline (uint32_t ___x0, const RuntimeMethod* method)
{
	LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (x == 0)
		uint32_t L_0 = ___x0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		// return 32;
		return ((int32_t)32);
	}

IL_0006:
	{
		// x &= (uint)-x;
		uint32_t L_1 = ___x0;
		uint32_t L_2 = ___x0;
		___x0 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)((uint32_t)((-((int64_t)((uint64_t)L_2))))))));
		// u.doubleValue = 0.0;
		(&V_0)->set_doubleValue_1((0.0));
		// u.longValue = 0x4330000000000000L + x;
		uint32_t L_3 = ___x0;
		(&V_0)->set_longValue_0(((int64_t)il2cpp_codegen_add((int64_t)((int64_t)4841369599423283200LL), (int64_t)((int64_t)((uint64_t)L_3)))));
		// u.doubleValue -= 4503599627370496.0;
		double* L_4 = (&V_0)->get_address_of_doubleValue_1();
		double* L_5 = L_4;
		double L_6 = *((double*)L_5);
		*((double*)L_5) = (double)((double)il2cpp_codegen_subtract((double)L_6, (double)(4503599627370496.0)));
		// return (int)(u.longValue >> 52) - 0x3FF;
		LongDoubleUnion_t4E62DA692E4065B3CC4025A9F719F76DC90D170B  L_7 = V_0;
		int64_t L_8 = L_7.get_longValue_0();
		return ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)((int64_t)((int64_t)L_8>>(int32_t)((int32_t)52))))), (int32_t)((int32_t)1023)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method)
{
	{
		// public quaternion(float4 value) { this.value = value; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___value0;
		__this->set_value_0(L_0);
		// public quaternion(float4 value) { this.value = value; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value0, const RuntimeMethod* method)
{
	{
		// public static quaternion quaternion(float4 value) { return new quaternion(value); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___value0;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_1;
		memset((&L_1), 0, sizeof(L_1));
		quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_rcp_mBBCDD8F05AFE2E307F898E81A06B64BF0E2625D0_inline (float ___x0, const RuntimeMethod* method)
{
	{
		// public static float rcp(float x) { return 1.0f / x; }
		float L_0 = ___x0;
		return ((float)((float)(1.0f)/(float)L_0));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// public static float3 cross(float3 x, float3 y) { return (x * y.yzx - x.yzx * y).yzx; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1;
		L_1 = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)(&___y1), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2;
		L_2 = float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1_inline(L_0, L_1, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		L_3 = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)(&___x0), /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___y1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_5;
		L_5 = float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1_inline(L_3, L_4, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6;
		L_6 = float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2_inline(L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_7;
		L_7 = float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_inline((float3_t9500D105F273B3D86BD354142E891C48FFF9F71D *)(&V_0), /*hidden argument*/NULL);
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___a0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___b1, const RuntimeMethod* method)
{
	{
		// return dot(a.value, b.value);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___a0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_2 = ___b1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_value_0();
		float L_4;
		L_4 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, float ___s2, const RuntimeMethod* method)
{
	{
		// public static float4 lerp(float4 x, float4 y, float s) { return x + s * (y - x); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = ___s2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4;
		L_4 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_2, L_3, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_1, L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6;
		L_6 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_0, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// float4 x = q.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		V_0 = L_1;
		// return quaternion(rsqrt(dot(x, x)) * x);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = V_0;
		float L_4;
		L_4 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_2, L_3, /*hidden argument*/NULL);
		float L_5;
		L_5 = math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7;
		L_7 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(L_5, L_6, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_8;
		L_8 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_acos_mDF4948B8CF18EF8052C38D7914802E7869B79B6C_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float acos(float x) { return (float)System.Math.Acos((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = acos(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  math_nlerp_mB7BD6C0F6F0AD1411F206E3BFDCC4161F1DF5D54_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q10, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q21, float ___t2, const RuntimeMethod* method)
{
	{
		// float dt = dot(q1, q2);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q10;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_1 = ___q21;
		float L_2;
		L_2 = math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634_inline(L_0, L_1, /*hidden argument*/NULL);
		// if(dt < 0.0f)
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0020;
		}
	}
	{
		// q2.value = -q2.value;
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_3 = ___q21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = L_3.get_value_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_5;
		L_5 = float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA_inline(L_4, /*hidden argument*/NULL);
		(&___q21)->set_value_0(L_5);
	}

IL_0020:
	{
		// return normalize(quaternion(lerp(q1.value, q2.value, t)));
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_6 = ___q10;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = L_6.get_value_0();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_8 = ___q21;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = L_8.get_value_0();
		float L_10 = ___t2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91_inline(L_7, L_9, L_10, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_12;
		L_12 = math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD_inline(L_11, /*hidden argument*/NULL);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_13;
		L_13 = math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D_inline(L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, uint32_t ___x0, uint32_t ___y1, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, uint32_t ___x0, uint32_t ___y1, uint32_t ___z2, uint32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		uint32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		uint32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		uint32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		uint32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = __this->get_address_of_value_0();
		float L_1 = ___x0;
		L_0->set_x_0(L_1);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_value_0();
		float L_3 = ___y1;
		L_2->set_y_1(L_3);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_4 = __this->get_address_of_value_0();
		float L_5 = ___z2;
		L_4->set_z_2(L_5);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_6 = __this->get_address_of_value_0();
		float L_7 = ___w3;
		L_6->set_w_3(L_7);
		// public quaternion(float x, float y, float z, float w) { value.x = x; value.y = y; value.z = z; value.w = w; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void math_sincos_m62FF0B31557448DAF3C3ED1EBCA171BD524BD96B_inline (float ___x0, float* ___s1, float* ___c2, const RuntimeMethod* method)
{
	{
		// public static void sincos(float x, out float s, out float c) { s = sin(x); c = cos(x); }
		float* L_0 = ___s1;
		float L_1 = ___x0;
		float L_2;
		L_2 = math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE_inline(L_1, /*hidden argument*/NULL);
		*((float*)L_0) = (float)L_2;
		// public static void sincos(float x, out float s, out float c) { s = sin(x); c = cos(x); }
		float* L_3 = ___c2;
		float L_4 = ___x0;
		float L_5;
		L_5 = math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424_inline(L_4, /*hidden argument*/NULL);
		*((float*)L_3) = (float)L_5;
		// public static void sincos(float x, out float s, out float c) { s = sin(x); c = cos(x); }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___x0, const RuntimeMethod* method)
{
	{
		// public bool Equals(quaternion x) { return value.x == x.value.x && value.y == x.value.y && value.z == x.value.z && value.w == x.value.w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = __this->get_address_of_value_0();
		float L_1 = L_0->get_x_0();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_2 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = L_2.get_value_0();
		float L_4 = L_3.get_x_0();
		if ((!(((float)L_1) == ((float)L_4))))
		{
			goto IL_0061;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_5 = __this->get_address_of_value_0();
		float L_6 = L_5->get_y_1();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_7 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = L_7.get_value_0();
		float L_9 = L_8.get_y_1();
		if ((!(((float)L_6) == ((float)L_9))))
		{
			goto IL_0061;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_10 = __this->get_address_of_value_0();
		float L_11 = L_10->get_z_2();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_12 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_13 = L_12.get_value_0();
		float L_14 = L_13.get_z_2();
		if ((!(((float)L_11) == ((float)L_14))))
		{
			goto IL_0061;
		}
	}
	{
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_15 = __this->get_address_of_value_0();
		float L_16 = L_15->get_w_3();
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_17 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18 = L_17.get_value_0();
		float L_19 = L_18.get_w_3();
		return (bool)((((float)L_16) == ((float)L_19))? 1 : 0);
	}

IL_0061:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, RuntimeObject * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Equals(object x) { return Equals((quaternion)x); }
		RuntimeObject * L_0 = ___x0;
		bool L_1;
		L_1 = quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_inline((quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)__this, ((*(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)((quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)UnBox(L_0, quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___q0, const RuntimeMethod* method)
{
	{
		// return hash(q.value);
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = ___q0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = L_0.get_value_0();
		uint32_t L_2;
		L_2 = math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5_inline(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  L_0 = (*(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F *)__this);
		uint32_t L_1;
		L_1 = math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("quaternion({0}f, {1}f, {2}f, {3}f)", value.x, value.y, value.z, value.w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_value_0();
		float L_3 = L_2->get_x_0();
		float L_4 = L_3;
		RuntimeObject * L_5 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_7 = __this->get_address_of_value_0();
		float L_8 = L_7->get_y_1();
		float L_9 = L_8;
		RuntimeObject * L_10 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_12 = __this->get_address_of_value_0();
		float L_13 = L_12->get_z_2();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_17 = __this->get_address_of_value_0();
		float L_18 = L_17->get_w_3();
		float L_19 = L_18;
		RuntimeObject * L_20 = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057_inline (quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("quaternion({0}f, {1}f, {2}f, {3}f)", value.x.ToString(format, formatProvider), value.y.ToString(format, formatProvider), value.z.ToString(format, formatProvider), value.w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_2 = __this->get_address_of_value_0();
		float* L_3 = L_2->get_address_of_x_0();
		String_t* L_4 = ___format0;
		RuntimeObject* L_5 = ___formatProvider1;
		String_t* L_6;
		L_6 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_6);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = L_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_8 = __this->get_address_of_value_0();
		float* L_9 = L_8->get_address_of_y_1();
		String_t* L_10 = ___format0;
		RuntimeObject* L_11 = ___formatProvider1;
		String_t* L_12;
		L_12 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_9, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_12);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_7;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_14 = __this->get_address_of_value_0();
		float* L_15 = L_14->get_address_of_z_2();
		String_t* L_16 = ___format0;
		RuntimeObject* L_17 = ___formatProvider1;
		String_t* L_18;
		L_18 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_15, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_18);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = L_13;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_20 = __this->get_address_of_value_0();
		float* L_21 = L_20->get_address_of_w_3();
		String_t* L_22 = ___format0;
		RuntimeObject* L_23 = ___formatProvider1;
		String_t* L_24;
		L_24 = Single_ToString_m7631D332703B4197EAA7DC0BA067CE7E16334D8B((float*)L_21, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_24);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_24);
		String_t* L_25;
		L_25 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralB053C5834963E3619E9D2CE282B5B20239AF72CB, L_19, /*hidden argument*/NULL);
		return L_25;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint2 rhs) { return x == rhs.x && y == rhs.y; }
		uint32_t L_0 = __this->get_x_0();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_001d;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		return (bool)((((int32_t)L_3) == ((int32_t)L_5))? 1 : 0);
	}

IL_001d:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint2(0x4473BBB1u, 0xCBA11D5Fu)) + 0x685835CFu;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = ___v0;
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_1;
		L_1 = math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2_inline(((int32_t)1148435377), ((int32_t)-878633633), /*hidden argument*/NULL);
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_2;
		L_2 = uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3;
		L_3 = math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)1750611407)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint2_t31B88562B6681D249453803230869FBE9ED565E7  L_0 = (*(uint2_t31B88562B6681D249453803230869FBE9ED565E7 *)__this);
		uint32_t L_1;
		L_1 = math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint2({0}, {1})", x, y);
		uint32_t L_0 = __this->get_x_0();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_y_1();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6;
		L_6 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945, L_2, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_inline (uint2_t31B88562B6681D249453803230869FBE9ED565E7 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint2({0}, {1})", x.ToString(format, formatProvider), y.ToString(format, formatProvider));
		uint32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		uint32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(_stringLiteralE368123B559BBC0EC2335D3AF9EC37C25C8B5945, L_3, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint3 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z; }
		uint32_t L_0 = __this->get_x_0();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		return (bool)((((int32_t)L_6) == ((int32_t)L_8))? 1 : 0);
	}

IL_002b:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint3(0xCD266C89u, 0xF1852A33u, 0x77E35E77u)) + 0x863E3729u;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = ___v0;
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_1;
		L_1 = math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729_inline(((int32_t)-853119863), ((int32_t)-242931149), ((int32_t)2011389559), /*hidden argument*/NULL);
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_2;
		L_2 = uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3;
		L_3 = math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)-2042742999)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A  L_0 = (*(uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A *)__this);
		uint32_t L_1;
		L_1 = math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x, y, z);
		uint32_t L_0 = __this->get_x_0();
		uint32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_1);
		uint32_t L_3 = __this->get_y_1();
		uint32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_4);
		uint32_t L_6 = __this->get_z_2();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2, L_2, L_5, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_inline (uint3_tDE8894AE6A23EC78B6580A66D1CF9A526095AF4A * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint3({0}, {1}, {2})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider));
		uint32_t* L_0 = __this->get_address_of_x_0();
		String_t* L_1 = ___format0;
		RuntimeObject* L_2 = ___formatProvider1;
		String_t* L_3;
		L_3 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_0, L_1, L_2, /*hidden argument*/NULL);
		uint32_t* L_4 = __this->get_address_of_y_1();
		String_t* L_5 = ___format0;
		RuntimeObject* L_6 = ___formatProvider1;
		String_t* L_7;
		L_7 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_4, L_5, L_6, /*hidden argument*/NULL);
		uint32_t* L_8 = __this->get_address_of_z_2();
		String_t* L_9 = ___format0;
		RuntimeObject* L_10 = ___formatProvider1;
		String_t* L_11;
		L_11 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_8, L_9, L_10, /*hidden argument*/NULL);
		String_t* L_12;
		L_12 = String_Format_m039737CCD992C5BFC8D16DFD681F5E8786E87FA6(_stringLiteralFEC52A4F763D4C868968D018DEC2AF8064D4C8E2, L_3, L_7, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___rhs0, const RuntimeMethod* method)
{
	{
		// public bool Equals(uint4 rhs) { return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w; }
		uint32_t L_0 = __this->get_x_0();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1 = ___rhs0;
		uint32_t L_2 = L_1.get_x_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_2))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_3 = __this->get_y_1();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_4 = ___rhs0;
		uint32_t L_5 = L_4.get_y_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_5))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_6 = __this->get_z_2();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_7 = ___rhs0;
		uint32_t L_8 = L_7.get_z_2();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0039;
		}
	}
	{
		uint32_t L_9 = __this->get_w_3();
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_10 = ___rhs0;
		uint32_t L_11 = L_10.get_w_3();
		return (bool)((((int32_t)L_9) == ((int32_t)L_11))? 1 : 0);
	}

IL_0039:
	{
		return (bool)0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR uint32_t math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  ___v0, const RuntimeMethod* method)
{
	{
		// return csum(v * uint4(0xB492BF15u, 0xD37220E3u, 0x7AA2C2BDu, 0xE16BC89Du)) + 0x7AA07CD3u;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = ___v0;
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_1;
		L_1 = math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777_inline(((int32_t)-1265451243), ((int32_t)-747495197), ((int32_t)2057487037), ((int32_t)-513029987), /*hidden argument*/NULL);
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_2;
		L_2 = uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6_inline(L_0, L_1, /*hidden argument*/NULL);
		uint32_t L_3;
		L_3 = math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5_inline(L_2, /*hidden argument*/NULL);
		return ((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)2057338067)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, const RuntimeMethod* method)
{
	{
		// public override int GetHashCode() { return (int)math.hash(this); }
		uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635  L_0 = (*(uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 *)__this);
		uint32_t L_1;
		L_1 = math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x, y, z, w);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		uint32_t L_2 = __this->get_x_0();
		uint32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = L_1;
		uint32_t L_6 = __this->get_y_1();
		uint32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = L_5;
		uint32_t L_10 = __this->get_z_2();
		uint32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = L_9;
		uint32_t L_14 = __this->get_w_3();
		uint32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		String_t* L_17;
		L_17 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_inline (uint4_t646D1C6030F449510629C1CDBC418BC46ABE3635 * __this, String_t* ___format0, RuntimeObject* ___formatProvider1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return string.Format("uint4({0}, {1}, {2}, {3})", x.ToString(format, formatProvider), y.ToString(format, formatProvider), z.ToString(format, formatProvider), w.ToString(format, formatProvider));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = L_0;
		uint32_t* L_2 = __this->get_address_of_x_0();
		String_t* L_3 = ___format0;
		RuntimeObject* L_4 = ___formatProvider1;
		String_t* L_5;
		L_5 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_5);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_1;
		uint32_t* L_7 = __this->get_address_of_y_1();
		String_t* L_8 = ___format0;
		RuntimeObject* L_9 = ___formatProvider1;
		String_t* L_10;
		L_10 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_7, L_8, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = L_6;
		uint32_t* L_12 = __this->get_address_of_z_2();
		String_t* L_13 = ___format0;
		RuntimeObject* L_14 = ___formatProvider1;
		String_t* L_15;
		L_15 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_12, L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_15);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_15);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_16 = L_11;
		uint32_t* L_17 = __this->get_address_of_w_3();
		String_t* L_18 = ___format0;
		RuntimeObject* L_19 = ___formatProvider1;
		String_t* L_20;
		L_20 = UInt32_ToString_mAF0A46E9EC70EA43A02EBE522FF287E20DEE691B((uint32_t*)L_17, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_20);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_20);
		String_t* L_21;
		L_21 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralE134155080D59FDE2A83A55D0BB359039175A4EA, L_16, /*hidden argument*/NULL);
		return L_21;
	}
}
