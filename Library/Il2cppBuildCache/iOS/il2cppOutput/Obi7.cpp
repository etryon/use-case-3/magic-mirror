﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Converter`2<Obi.IBounded,Obi.Triangle>
struct Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2;
// System.Func`2<UnityEngine.Vector3Int,System.Single>
struct Func_2_t60C15697969143DDD791767187830BED28B48143;
// System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Face>
struct IEnumerable_1_tAE941B6A20BC1EB442179E6FD3406ED39D7851BA;
// System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/HalfEdge>
struct IEnumerable_1_t347675EB2D2584E885712756F4F5A222D13817BE;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<Obi.ObiParticleGroup>
struct List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t2558DEC96F7E6007750607B083ADB3AC48A30CCB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>
struct List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C;
// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/HalfEdge>
struct List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B;
// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex>
struct List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30;
// System.Collections.Generic.List`1<Obi.ObiTriangleSkinMap/SlaveVertex>
struct List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA;
// Obi.PriorityQueue`1<Obi.VoxelPathFinder/TargetVoxel>
struct PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03;
// Unity.Collections.LowLevel.Unsafe.UnsafeList`1<Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>>
struct UnsafeList_1_t6961AC7E63FB8EACD1584397FB52AC37807B8EEF;
// Unity.Collections.LowLevel.Unsafe.UnsafeList`1<System.Int32>
struct UnsafeList_1_t0160F425C28D7D8E9DA796AD7C293EE00D425DF9;
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
// UnityEngine.Vector2Int[]
struct Vector2IntU5BU5D_tA91A00C258BDBF38BD76CF790B67CB344A126E9E;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.Vector3Int[]
struct Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
// Unity.Mathematics.int2[]
struct int2U5BU5D_t5B28DA25BC7C47BEAF4BDBB7F78B1BA7E9CBA49E;
// Unity.Mathematics.int3[]
struct int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2;
// Obi.HalfEdgeMesh/Face[]
struct FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595;
// Obi.HalfEdgeMesh/Vertex[]
struct VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC;
// Obi.MeshVoxelizer/Voxel[]
struct VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55;
// Obi.ObiTriangleSkinMap/MasterFace[]
struct MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4;
// Obi.ObiTriangleSkinMap/SlaveVertex[]
struct SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70;
// System.Boolean[0...,0...,0...]
struct BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B;
// UnityEngine.Vector3Int[0...,0...,0...]
struct Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9;
// Obi.HalfEdgeMesh
struct HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7;
// Obi.IBounded
struct IBounded_tD15153C6212B0516F2F6B5BD9CB4A8D81658FB1E;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// Obi.MeshVoxelizer
struct MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D;
// Unity.Collections.NativeQueueBlockPoolData
struct NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2;
// Unity.Collections.NativeQueueData
struct NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// Obi.ObiActorBlueprint
struct ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD;
// Obi.ObiAerodynamicConstraintsData
struct ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563;
// Obi.ObiBendConstraintsData
struct ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED;
// Obi.ObiBendTwistConstraintsData
struct ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE;
// Obi.ObiChainConstraintsData
struct ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0;
// Obi.ObiClothBlueprintBase
struct ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD;
// Obi.ObiDistanceConstraintsData
struct ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB;
// Obi.ObiShapeMatchingConstraintsData
struct ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8;
// Obi.ObiSkinConstraintsData
struct ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353;
// Obi.ObiStretchShearConstraintsData
struct ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC;
// Obi.ObiTearableClothBlueprint
struct ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB;
// Obi.ObiTetherConstraintsData
struct ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE;
// Obi.ObiTriangleSkinMap
struct ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F;
// Obi.ObiVolumeConstraintsData
struct ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Obi.Poly6Kernel
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData
struct UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Obi.VoxelDistanceField
struct VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA;
// Obi.VoxelPathFinder
struct VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF;
// Obi.CoroutineJob/ProgressInfo
struct ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74;
// Obi.ObiActorBlueprint/BlueprintCallback
struct BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6;
// Obi.ObiTearableClothBlueprint/<Initialize>d__8
struct U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627;
// Obi.ObiTriangleMeshContainer/<>c
struct U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232;
// Obi.ObiTriangleSkinMap/<Bind>d__31
struct U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54;
// Obi.ObiTriangleSkinMap/MasterFace
struct MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46;
// Obi.ObiTriangleSkinMap/SlaveVertex
struct SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB;
// Obi.ObiUtils/<BilateralInterleaved>d__40
struct U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB;
// Obi.VoxelDistanceField/<JumpFlood>d__5
struct U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401;
// Obi.VoxelPathFinder/<>c
struct U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816;
// Obi.VoxelPathFinder/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E;

IL2CPP_EXTERN_C RuntimeClass* BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_t347675EB2D2584E885712756F4F5A222D13817BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_tAE941B6A20BC1EB442179E6FD3406ED39D7851BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t6B3EEC19B84F0BC1D3DD815258B1A2031E0296E1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t9C685EA70A84E3D46C2B5D05491B7B985EB524C9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2951FBF2856E4DBA8EE992C727853EDB0D92574C;
IL2CPP_EXTERN_C String_t* _stringLiteral2FC07AE370700D8F7FAC2B165583F6E0EE73B3D3;
IL2CPP_EXTERN_C String_t* _stringLiteral55C7FCB65C82500AE3D491E82F447D633911057E;
IL2CPP_EXTERN_C String_t* _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1;
IL2CPP_EXTERN_C String_t* _stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94;
IL2CPP_EXTERN_C String_t* _stringLiteralBB6ACC004563D2B5AF708C3AC17178DFCC58F122;
IL2CPP_EXTERN_C String_t* _stringLiteralFA6A4202087E8A0CD5992B98096167C9B5835DEF;
IL2CPP_EXTERN_C const RuntimeMethod* BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m208B981A048B9BF9FB11152E3517D21A8D4CD593_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ObiUtils_Swap_TisParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_mDA8D45AAE52ED72F3C11941CBD419D8C7AD4463D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235;;
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com;
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com;;
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke;
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke;;

struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF;
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871;
struct int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2;
struct VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC;
struct MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4;
struct Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>
struct List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C, ____items_1)); }
	inline FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595* get__items_1() const { return ____items_1; }
	inline FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C_StaticFields, ____emptyArray_5)); }
	inline FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595* get__emptyArray_5() const { return ____emptyArray_5; }
	inline FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(FaceU5BU5D_t1EFE492C8B548272BEF40E090A9382D1DCD0E595* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex>
struct List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30, ____items_1)); }
	inline VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* get__items_1() const { return ____items_1; }
	inline VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30_StaticFields, ____emptyArray_5)); }
	inline VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* get__emptyArray_5() const { return ____emptyArray_5; }
	inline VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Obi.ObiTriangleSkinMap/SlaveVertex>
struct List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA, ____items_1)); }
	inline SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70* get__items_1() const { return ____items_1; }
	inline SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA_StaticFields, ____emptyArray_5)); }
	inline SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SlaveVertexU5BU5D_tDAD34D15ADD475C401DCB490DA95C9B004191D70* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Obi.VoxelDistanceField
struct VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA  : public RuntimeObject
{
public:
	// UnityEngine.Vector3Int[0...,0...,0...] Obi.VoxelDistanceField::distanceField
	Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___distanceField_0;
	// Obi.MeshVoxelizer Obi.VoxelDistanceField::voxelizer
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * ___voxelizer_1;

public:
	inline static int32_t get_offset_of_distanceField_0() { return static_cast<int32_t>(offsetof(VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA, ___distanceField_0)); }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* get_distanceField_0() const { return ___distanceField_0; }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9** get_address_of_distanceField_0() { return &___distanceField_0; }
	inline void set_distanceField_0(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* value)
	{
		___distanceField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distanceField_0), (void*)value);
	}

	inline static int32_t get_offset_of_voxelizer_1() { return static_cast<int32_t>(offsetof(VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA, ___voxelizer_1)); }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * get_voxelizer_1() const { return ___voxelizer_1; }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D ** get_address_of_voxelizer_1() { return &___voxelizer_1; }
	inline void set_voxelizer_1(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * value)
	{
		___voxelizer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voxelizer_1), (void*)value);
	}
};


// Obi.VoxelPathFinder
struct VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF  : public RuntimeObject
{
public:
	// Obi.MeshVoxelizer Obi.VoxelPathFinder::voxelizer
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * ___voxelizer_0;
	// System.Boolean[0...,0...,0...] Obi.VoxelPathFinder::closed
	BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B* ___closed_1;
	// Obi.PriorityQueue`1<Obi.VoxelPathFinder/TargetVoxel> Obi.VoxelPathFinder::open
	PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 * ___open_2;

public:
	inline static int32_t get_offset_of_voxelizer_0() { return static_cast<int32_t>(offsetof(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF, ___voxelizer_0)); }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * get_voxelizer_0() const { return ___voxelizer_0; }
	inline MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D ** get_address_of_voxelizer_0() { return &___voxelizer_0; }
	inline void set_voxelizer_0(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * value)
	{
		___voxelizer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voxelizer_0), (void*)value);
	}

	inline static int32_t get_offset_of_closed_1() { return static_cast<int32_t>(offsetof(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF, ___closed_1)); }
	inline BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B* get_closed_1() const { return ___closed_1; }
	inline BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B** get_address_of_closed_1() { return &___closed_1; }
	inline void set_closed_1(BooleanU5BU2CU2CU5D_tB8F56EB24D5C42F188A3C72BFF48D9AC9B78E58B* value)
	{
		___closed_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___closed_1), (void*)value);
	}

	inline static int32_t get_offset_of_open_2() { return static_cast<int32_t>(offsetof(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF, ___open_2)); }
	inline PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 * get_open_2() const { return ___open_2; }
	inline PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 ** get_address_of_open_2() { return &___open_2; }
	inline void set_open_2(PriorityQueue_1_t0510E3608062DAF6C39185E22673183229DB4D03 * value)
	{
		___open_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___open_2), (void*)value);
	}
};


// Obi.CoroutineJob/ProgressInfo
struct ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74  : public RuntimeObject
{
public:
	// System.String Obi.CoroutineJob/ProgressInfo::userReadableInfo
	String_t* ___userReadableInfo_0;
	// System.Single Obi.CoroutineJob/ProgressInfo::progress
	float ___progress_1;

public:
	inline static int32_t get_offset_of_userReadableInfo_0() { return static_cast<int32_t>(offsetof(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74, ___userReadableInfo_0)); }
	inline String_t* get_userReadableInfo_0() const { return ___userReadableInfo_0; }
	inline String_t** get_address_of_userReadableInfo_0() { return &___userReadableInfo_0; }
	inline void set_userReadableInfo_0(String_t* value)
	{
		___userReadableInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___userReadableInfo_0), (void*)value);
	}

	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74, ___progress_1)); }
	inline float get_progress_1() const { return ___progress_1; }
	inline float* get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(float value)
	{
		___progress_1 = value;
	}
};


// Obi.ObiTearableClothBlueprint/<Initialize>d__8
struct U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiTearableClothBlueprint/<Initialize>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiTearableClothBlueprint Obi.ObiTearableClothBlueprint/<Initialize>d__8::<>4__this
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * ___U3CU3E4__this_2;
	// System.Collections.IEnumerator Obi.ObiTearableClothBlueprint/<Initialize>d__8::<dt>5__2
	RuntimeObject* ___U3CdtU3E5__2_3;
	// System.Collections.IEnumerator Obi.ObiTearableClothBlueprint/<Initialize>d__8::<dc>5__3
	RuntimeObject* ___U3CdcU3E5__3_4;
	// System.Collections.IEnumerator Obi.ObiTearableClothBlueprint/<Initialize>d__8::<ac>5__4
	RuntimeObject* ___U3CacU3E5__4_5;
	// System.Collections.IEnumerator Obi.ObiTearableClothBlueprint/<Initialize>d__8::<bc>5__5
	RuntimeObject* ___U3CbcU3E5__5_6;
	// System.Int32 Obi.ObiTearableClothBlueprint/<Initialize>d__8::<i>5__6
	int32_t ___U3CiU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CU3E4__this_2)); }
	inline ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CdtU3E5__2_3)); }
	inline RuntimeObject* get_U3CdtU3E5__2_3() const { return ___U3CdtU3E5__2_3; }
	inline RuntimeObject** get_address_of_U3CdtU3E5__2_3() { return &___U3CdtU3E5__2_3; }
	inline void set_U3CdtU3E5__2_3(RuntimeObject* value)
	{
		___U3CdtU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdtU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdcU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CdcU3E5__3_4)); }
	inline RuntimeObject* get_U3CdcU3E5__3_4() const { return ___U3CdcU3E5__3_4; }
	inline RuntimeObject** get_address_of_U3CdcU3E5__3_4() { return &___U3CdcU3E5__3_4; }
	inline void set_U3CdcU3E5__3_4(RuntimeObject* value)
	{
		___U3CdcU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdcU3E5__3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CacU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CacU3E5__4_5)); }
	inline RuntimeObject* get_U3CacU3E5__4_5() const { return ___U3CacU3E5__4_5; }
	inline RuntimeObject** get_address_of_U3CacU3E5__4_5() { return &___U3CacU3E5__4_5; }
	inline void set_U3CacU3E5__4_5(RuntimeObject* value)
	{
		___U3CacU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CacU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CbcU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CbcU3E5__5_6)); }
	inline RuntimeObject* get_U3CbcU3E5__5_6() const { return ___U3CbcU3E5__5_6; }
	inline RuntimeObject** get_address_of_U3CbcU3E5__5_6() { return &___U3CbcU3E5__5_6; }
	inline void set_U3CbcU3E5__5_6(RuntimeObject* value)
	{
		___U3CbcU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CbcU3E5__5_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627, ___U3CiU3E5__6_7)); }
	inline int32_t get_U3CiU3E5__6_7() const { return ___U3CiU3E5__6_7; }
	inline int32_t* get_address_of_U3CiU3E5__6_7() { return &___U3CiU3E5__6_7; }
	inline void set_U3CiU3E5__6_7(int32_t value)
	{
		___U3CiU3E5__6_7 = value;
	}
};


// Obi.ObiTriangleMeshContainer/<>c
struct U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields
{
public:
	// Obi.ObiTriangleMeshContainer/<>c Obi.ObiTriangleMeshContainer/<>c::<>9
	U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * ___U3CU3E9_0;
	// System.Converter`2<Obi.IBounded,Obi.Triangle> Obi.ObiTriangleMeshContainer/<>c::<>9__6_0
	Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Converter_2_tA5CFCDD2DDF09EC21725B24DD40CB2E6C928F1F2 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_0_1), (void*)value);
	}
};


// Obi.ObiUtils/<BilateralInterleaved>d__40
struct U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::count
	int32_t ___count_3;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<>3__count
	int32_t ___U3CU3E3__count_4;
	// System.Int32 Obi.ObiUtils/<BilateralInterleaved>d__40::<i>5__2
	int32_t ___U3CiU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__count_4() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CU3E3__count_4)); }
	inline int32_t get_U3CU3E3__count_4() const { return ___U3CU3E3__count_4; }
	inline int32_t* get_address_of_U3CU3E3__count_4() { return &___U3CU3E3__count_4; }
	inline void set_U3CU3E3__count_4(int32_t value)
	{
		___U3CU3E3__count_4 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB, ___U3CiU3E5__2_5)); }
	inline int32_t get_U3CiU3E5__2_5() const { return ___U3CiU3E5__2_5; }
	inline int32_t* get_address_of_U3CiU3E5__2_5() { return &___U3CiU3E5__2_5; }
	inline void set_U3CiU3E5__2_5(int32_t value)
	{
		___U3CiU3E5__2_5 = value;
	}
};


// Obi.VoxelDistanceField/<JumpFlood>d__5
struct U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401  : public RuntimeObject
{
public:
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.VoxelDistanceField Obi.VoxelDistanceField/<JumpFlood>d__5::<>4__this
	VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * ___U3CU3E4__this_2;
	// UnityEngine.Vector3Int[0...,0...,0...] Obi.VoxelDistanceField/<JumpFlood>d__5::<auxBuffer>5__2
	Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___U3CauxBufferU3E5__2_3;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<size>5__3
	int32_t ___U3CsizeU3E5__3_4;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<step>5__4
	int32_t ___U3CstepU3E5__4_5;
	// System.Single Obi.VoxelDistanceField/<JumpFlood>d__5::<numPasses>5__5
	float ___U3CnumPassesU3E5__5_6;
	// System.Int32 Obi.VoxelDistanceField/<JumpFlood>d__5::<i>5__6
	int32_t ___U3CiU3E5__6_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CU3E4__this_2)); }
	inline VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CauxBufferU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CauxBufferU3E5__2_3)); }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* get_U3CauxBufferU3E5__2_3() const { return ___U3CauxBufferU3E5__2_3; }
	inline Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9** get_address_of_U3CauxBufferU3E5__2_3() { return &___U3CauxBufferU3E5__2_3; }
	inline void set_U3CauxBufferU3E5__2_3(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* value)
	{
		___U3CauxBufferU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CauxBufferU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CsizeU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CsizeU3E5__3_4)); }
	inline int32_t get_U3CsizeU3E5__3_4() const { return ___U3CsizeU3E5__3_4; }
	inline int32_t* get_address_of_U3CsizeU3E5__3_4() { return &___U3CsizeU3E5__3_4; }
	inline void set_U3CsizeU3E5__3_4(int32_t value)
	{
		___U3CsizeU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CstepU3E5__4_5)); }
	inline int32_t get_U3CstepU3E5__4_5() const { return ___U3CstepU3E5__4_5; }
	inline int32_t* get_address_of_U3CstepU3E5__4_5() { return &___U3CstepU3E5__4_5; }
	inline void set_U3CstepU3E5__4_5(int32_t value)
	{
		___U3CstepU3E5__4_5 = value;
	}

	inline static int32_t get_offset_of_U3CnumPassesU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CnumPassesU3E5__5_6)); }
	inline float get_U3CnumPassesU3E5__5_6() const { return ___U3CnumPassesU3E5__5_6; }
	inline float* get_address_of_U3CnumPassesU3E5__5_6() { return &___U3CnumPassesU3E5__5_6; }
	inline void set_U3CnumPassesU3E5__5_6(float value)
	{
		___U3CnumPassesU3E5__5_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401, ___U3CiU3E5__6_7)); }
	inline int32_t get_U3CiU3E5__6_7() const { return ___U3CiU3E5__6_7; }
	inline int32_t* get_address_of_U3CiU3E5__6_7() { return &___U3CiU3E5__6_7; }
	inline void set_U3CiU3E5__6_7(int32_t value)
	{
		___U3CiU3E5__6_7 = value;
	}
};


// Obi.VoxelPathFinder/<>c
struct U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields
{
public:
	// Obi.VoxelPathFinder/<>c Obi.VoxelPathFinder/<>c::<>9
	U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Vector3Int,System.Single> Obi.VoxelPathFinder/<>c::<>9__6_1
	Func_2_t60C15697969143DDD791767187830BED28B48143 * ___U3CU3E9__6_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields, ___U3CU3E9__6_1_1)); }
	inline Func_2_t60C15697969143DDD791767187830BED28B48143 * get_U3CU3E9__6_1_1() const { return ___U3CU3E9__6_1_1; }
	inline Func_2_t60C15697969143DDD791767187830BED28B48143 ** get_address_of_U3CU3E9__6_1_1() { return &___U3CU3E9__6_1_1; }
	inline void set_U3CU3E9__6_1_1(Func_2_t60C15697969143DDD791767187830BED28B48143 * value)
	{
		___U3CU3E9__6_1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__6_1_1), (void*)value);
	}
};


// Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>
struct ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 
{
public:
	// Unity.Collections.NativeQueueData* Unity.Collections.NativeQueue`1/ParallelWriter::m_Buffer
	NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA * ___m_Buffer_0;
	// Unity.Collections.NativeQueueBlockPoolData* Unity.Collections.NativeQueue`1/ParallelWriter::m_QueuePool
	NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 * ___m_QueuePool_1;
	// System.Int32 Unity.Collections.NativeQueue`1/ParallelWriter::m_ThreadIndex
	int32_t ___m_ThreadIndex_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05, ___m_Buffer_0)); }
	inline NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA * get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA ** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA * value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_QueuePool_1() { return static_cast<int32_t>(offsetof(ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05, ___m_QueuePool_1)); }
	inline NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 * get_m_QueuePool_1() const { return ___m_QueuePool_1; }
	inline NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 ** get_address_of_m_QueuePool_1() { return &___m_QueuePool_1; }
	inline void set_m_QueuePool_1(NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 * value)
	{
		___m_QueuePool_1 = value;
	}

	inline static int32_t get_offset_of_m_ThreadIndex_2() { return static_cast<int32_t>(offsetof(ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05, ___m_ThreadIndex_2)); }
	inline int32_t get_m_ThreadIndex_2() const { return ___m_ThreadIndex_2; }
	inline int32_t* get_address_of_m_ThreadIndex_2() { return &___m_ThreadIndex_2; }
	inline void set_m_ThreadIndex_2(int32_t value)
	{
		___m_ThreadIndex_2 = value;
	}
};


// Unity.Collections.NativeQueue`1/ParallelWriter<Obi.FluidInteraction>
struct ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 
{
public:
	// Unity.Collections.NativeQueueData* Unity.Collections.NativeQueue`1/ParallelWriter::m_Buffer
	NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA * ___m_Buffer_0;
	// Unity.Collections.NativeQueueBlockPoolData* Unity.Collections.NativeQueue`1/ParallelWriter::m_QueuePool
	NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 * ___m_QueuePool_1;
	// System.Int32 Unity.Collections.NativeQueue`1/ParallelWriter::m_ThreadIndex
	int32_t ___m_ThreadIndex_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878, ___m_Buffer_0)); }
	inline NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA * get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA ** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA * value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_QueuePool_1() { return static_cast<int32_t>(offsetof(ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878, ___m_QueuePool_1)); }
	inline NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 * get_m_QueuePool_1() const { return ___m_QueuePool_1; }
	inline NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 ** get_address_of_m_QueuePool_1() { return &___m_QueuePool_1; }
	inline void set_m_QueuePool_1(NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2 * value)
	{
		___m_QueuePool_1 = value;
	}

	inline static int32_t get_offset_of_m_ThreadIndex_2() { return static_cast<int32_t>(offsetof(ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878, ___m_ThreadIndex_2)); }
	inline int32_t get_m_ThreadIndex_2() const { return ___m_ThreadIndex_2; }
	inline int32_t* get_address_of_m_ThreadIndex_2() { return &___m_ThreadIndex_2; }
	inline void set_m_ThreadIndex_2(int32_t value)
	{
		___m_ThreadIndex_2 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// GridHash
struct GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7 
{
public:
	union
	{
		struct
		{
		};
		uint8_t GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7__padding[1];
	};

public:
};

struct GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_StaticFields
{
public:
	// Unity.Mathematics.int3[] GridHash::cellOffsets3D
	int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* ___cellOffsets3D_0;
	// Unity.Mathematics.int3[] GridHash::cellOffsets
	int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* ___cellOffsets_1;
	// Unity.Mathematics.int2[] GridHash::cell2DOffsets
	int2U5BU5D_t5B28DA25BC7C47BEAF4BDBB7F78B1BA7E9CBA49E* ___cell2DOffsets_2;

public:
	inline static int32_t get_offset_of_cellOffsets3D_0() { return static_cast<int32_t>(offsetof(GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_StaticFields, ___cellOffsets3D_0)); }
	inline int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* get_cellOffsets3D_0() const { return ___cellOffsets3D_0; }
	inline int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2** get_address_of_cellOffsets3D_0() { return &___cellOffsets3D_0; }
	inline void set_cellOffsets3D_0(int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* value)
	{
		___cellOffsets3D_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellOffsets3D_0), (void*)value);
	}

	inline static int32_t get_offset_of_cellOffsets_1() { return static_cast<int32_t>(offsetof(GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_StaticFields, ___cellOffsets_1)); }
	inline int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* get_cellOffsets_1() const { return ___cellOffsets_1; }
	inline int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2** get_address_of_cellOffsets_1() { return &___cellOffsets_1; }
	inline void set_cellOffsets_1(int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* value)
	{
		___cellOffsets_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cellOffsets_1), (void*)value);
	}

	inline static int32_t get_offset_of_cell2DOffsets_2() { return static_cast<int32_t>(offsetof(GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_StaticFields, ___cell2DOffsets_2)); }
	inline int2U5BU5D_t5B28DA25BC7C47BEAF4BDBB7F78B1BA7E9CBA49E* get_cell2DOffsets_2() const { return ___cell2DOffsets_2; }
	inline int2U5BU5D_t5B28DA25BC7C47BEAF4BDBB7F78B1BA7E9CBA49E** get_address_of_cell2DOffsets_2() { return &___cell2DOffsets_2; }
	inline void set_cell2DOffsets_2(int2U5BU5D_t5B28DA25BC7C47BEAF4BDBB7F78B1BA7E9CBA49E* value)
	{
		___cell2DOffsets_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cell2DOffsets_2), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// Obi.Poly6Kernel
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235 
{
public:
	// System.Single Obi.Poly6Kernel::norm
	float ___norm_0;
	// System.Boolean Obi.Poly6Kernel::norm2D
	bool ___norm2D_1;

public:
	inline static int32_t get_offset_of_norm_0() { return static_cast<int32_t>(offsetof(Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235, ___norm_0)); }
	inline float get_norm_0() const { return ___norm_0; }
	inline float* get_address_of_norm_0() { return &___norm_0; }
	inline void set_norm_0(float value)
	{
		___norm_0 = value;
	}

	inline static int32_t get_offset_of_norm2D_1() { return static_cast<int32_t>(offsetof(Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235, ___norm2D_1)); }
	inline bool get_norm2D_1() const { return ___norm2D_1; }
	inline bool* get_address_of_norm2D_1() { return &___norm2D_1; }
	inline void set_norm2D_1(bool value)
	{
		___norm2D_1 = value;
	}
};

// Native definition for P/Invoke marshalling of Obi.Poly6Kernel
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke
{
	float ___norm_0;
	int32_t ___norm2D_1;
};
// Native definition for COM marshalling of Obi.Poly6Kernel
struct Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com
{
	float ___norm_0;
	int32_t ___norm2D_1;
};

// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// Obi.SimplexCounts
struct SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA 
{
public:
	// System.Int32 Obi.SimplexCounts::pointCount
	int32_t ___pointCount_0;
	// System.Int32 Obi.SimplexCounts::edgeCount
	int32_t ___edgeCount_1;
	// System.Int32 Obi.SimplexCounts::triangleCount
	int32_t ___triangleCount_2;

public:
	inline static int32_t get_offset_of_pointCount_0() { return static_cast<int32_t>(offsetof(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA, ___pointCount_0)); }
	inline int32_t get_pointCount_0() const { return ___pointCount_0; }
	inline int32_t* get_address_of_pointCount_0() { return &___pointCount_0; }
	inline void set_pointCount_0(int32_t value)
	{
		___pointCount_0 = value;
	}

	inline static int32_t get_offset_of_edgeCount_1() { return static_cast<int32_t>(offsetof(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA, ___edgeCount_1)); }
	inline int32_t get_edgeCount_1() const { return ___edgeCount_1; }
	inline int32_t* get_address_of_edgeCount_1() { return &___edgeCount_1; }
	inline void set_edgeCount_1(int32_t value)
	{
		___edgeCount_1 = value;
	}

	inline static int32_t get_offset_of_triangleCount_2() { return static_cast<int32_t>(offsetof(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA, ___triangleCount_2)); }
	inline int32_t get_triangleCount_2() const { return ___triangleCount_2; }
	inline int32_t* get_address_of_triangleCount_2() { return &___triangleCount_2; }
	inline void set_triangleCount_2(int32_t value)
	{
		___triangleCount_2 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector3Int
struct Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA 
{
public:
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Z_2() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA, ___m_Z_2)); }
	inline int32_t get_m_Z_2() const { return ___m_Z_2; }
	inline int32_t* get_address_of_m_Z_2() { return &___m_Z_2; }
	inline void set_m_Z_2(int32_t value)
	{
		___m_Z_2 = value;
	}
};

struct Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields
{
public:
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Right_8;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Forward
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Forward_9;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Back
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___s_Back_10;

public:
	inline static int32_t get_offset_of_s_Zero_3() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Zero_3)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Zero_3() const { return ___s_Zero_3; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Zero_3() { return &___s_Zero_3; }
	inline void set_s_Zero_3(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Zero_3 = value;
	}

	inline static int32_t get_offset_of_s_One_4() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_One_4)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_One_4() const { return ___s_One_4; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_One_4() { return &___s_One_4; }
	inline void set_s_One_4(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_One_4 = value;
	}

	inline static int32_t get_offset_of_s_Up_5() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Up_5)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Up_5() const { return ___s_Up_5; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Up_5() { return &___s_Up_5; }
	inline void set_s_Up_5(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Up_5 = value;
	}

	inline static int32_t get_offset_of_s_Down_6() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Down_6)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Down_6() const { return ___s_Down_6; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Down_6() { return &___s_Down_6; }
	inline void set_s_Down_6(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Down_6 = value;
	}

	inline static int32_t get_offset_of_s_Left_7() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Left_7)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Left_7() const { return ___s_Left_7; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Left_7() { return &___s_Left_7; }
	inline void set_s_Left_7(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Left_7 = value;
	}

	inline static int32_t get_offset_of_s_Right_8() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Right_8)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Right_8() const { return ___s_Right_8; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Right_8() { return &___s_Right_8; }
	inline void set_s_Right_8(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Right_8 = value;
	}

	inline static int32_t get_offset_of_s_Forward_9() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Forward_9)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Forward_9() const { return ___s_Forward_9; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Forward_9() { return &___s_Forward_9; }
	inline void set_s_Forward_9(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Forward_9 = value;
	}

	inline static int32_t get_offset_of_s_Back_10() { return static_cast<int32_t>(offsetof(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA_StaticFields, ___s_Back_10)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_s_Back_10() const { return ___s_Back_10; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_s_Back_10() { return &___s_Back_10; }
	inline void set_s_Back_10(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___s_Back_10 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Unity.Mathematics.float3
struct float3_t9500D105F273B3D86BD354142E891C48FFF9F71D 
{
public:
	// System.Single Unity.Mathematics.float3::x
	float ___x_0;
	// System.Single Unity.Mathematics.float3::y
	float ___y_1;
	// System.Single Unity.Mathematics.float3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

struct float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_StaticFields
{
public:
	// Unity.Mathematics.float3 Unity.Mathematics.float3::zero
	float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___zero_3;

public:
	inline static int32_t get_offset_of_zero_3() { return static_cast<int32_t>(offsetof(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D_StaticFields, ___zero_3)); }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  get_zero_3() const { return ___zero_3; }
	inline float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * get_address_of_zero_3() { return &___zero_3; }
	inline void set_zero_3(float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  value)
	{
		___zero_3 = value;
	}
};


// Unity.Mathematics.float4
struct float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 
{
public:
	// System.Single Unity.Mathematics.float4::x
	float ___x_0;
	// System.Single Unity.Mathematics.float4::y
	float ___y_1;
	// System.Single Unity.Mathematics.float4::z
	float ___z_2;
	// System.Single Unity.Mathematics.float4::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.float4::zero
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___zero_4;

public:
	inline static int32_t get_offset_of_zero_4() { return static_cast<int32_t>(offsetof(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields, ___zero_4)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_zero_4() const { return ___zero_4; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_zero_4() { return &___zero_4; }
	inline void set_zero_4(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___zero_4 = value;
	}
};


// Unity.Mathematics.int3
struct int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 
{
public:
	// System.Int32 Unity.Mathematics.int3::x
	int32_t ___x_0;
	// System.Int32 Unity.Mathematics.int3::y
	int32_t ___y_1;
	// System.Int32 Unity.Mathematics.int3::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};


// Unity.Mathematics.int4
struct int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 
{
public:
	// System.Int32 Unity.Mathematics.int4::x
	int32_t ___x_0;
	// System.Int32 Unity.Mathematics.int4::y
	int32_t ___y_1;
	// System.Int32 Unity.Mathematics.int4::z
	int32_t ___z_2;
	// System.Int32 Unity.Mathematics.int4::w
	int32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067, ___w_3)); }
	inline int32_t get_w_3() const { return ___w_3; }
	inline int32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(int32_t value)
	{
		___w_3 = value;
	}
};

struct int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_StaticFields
{
public:
	// Unity.Mathematics.int4 Unity.Mathematics.int4::zero
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___zero_4;

public:
	inline static int32_t get_offset_of_zero_4() { return static_cast<int32_t>(offsetof(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067_StaticFields, ___zero_4)); }
	inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  get_zero_4() const { return ___zero_4; }
	inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * get_address_of_zero_4() { return &___zero_4; }
	inline void set_zero_4(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  value)
	{
		___zero_4 = value;
	}
};


// Unity.Collections.AllocatorManager/AllocatorHandle
struct AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A 
{
public:
	// System.UInt16 Unity.Collections.AllocatorManager/AllocatorHandle::Index
	uint16_t ___Index_0;
	// System.UInt16 Unity.Collections.AllocatorManager/AllocatorHandle::Version
	uint16_t ___Version_1;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A, ___Index_0)); }
	inline uint16_t get_Index_0() const { return ___Index_0; }
	inline uint16_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(uint16_t value)
	{
		___Index_0 = value;
	}

	inline static int32_t get_offset_of_Version_1() { return static_cast<int32_t>(offsetof(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A, ___Version_1)); }
	inline uint16_t get_Version_1() const { return ___Version_1; }
	inline uint16_t* get_address_of_Version_1() { return &___Version_1; }
	inline void set_Version_1(uint16_t value)
	{
		___Version_1 = value;
	}
};


// Obi.HalfEdgeMesh/Face
struct Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2 
{
public:
	// System.Int32 Obi.HalfEdgeMesh/Face::index
	int32_t ___index_0;
	// System.Int32 Obi.HalfEdgeMesh/Face::halfEdge
	int32_t ___halfEdge_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_halfEdge_1() { return static_cast<int32_t>(offsetof(Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2, ___halfEdge_1)); }
	inline int32_t get_halfEdge_1() const { return ___halfEdge_1; }
	inline int32_t* get_address_of_halfEdge_1() { return &___halfEdge_1; }
	inline void set_halfEdge_1(int32_t value)
	{
		___halfEdge_1 = value;
	}
};


// Obi.HalfEdgeMesh/HalfEdge
struct HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7 
{
public:
	// System.Int32 Obi.HalfEdgeMesh/HalfEdge::index
	int32_t ___index_0;
	// System.Int32 Obi.HalfEdgeMesh/HalfEdge::indexInFace
	int32_t ___indexInFace_1;
	// System.Int32 Obi.HalfEdgeMesh/HalfEdge::face
	int32_t ___face_2;
	// System.Int32 Obi.HalfEdgeMesh/HalfEdge::nextHalfEdge
	int32_t ___nextHalfEdge_3;
	// System.Int32 Obi.HalfEdgeMesh/HalfEdge::pair
	int32_t ___pair_4;
	// System.Int32 Obi.HalfEdgeMesh/HalfEdge::endVertex
	int32_t ___endVertex_5;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_indexInFace_1() { return static_cast<int32_t>(offsetof(HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7, ___indexInFace_1)); }
	inline int32_t get_indexInFace_1() const { return ___indexInFace_1; }
	inline int32_t* get_address_of_indexInFace_1() { return &___indexInFace_1; }
	inline void set_indexInFace_1(int32_t value)
	{
		___indexInFace_1 = value;
	}

	inline static int32_t get_offset_of_face_2() { return static_cast<int32_t>(offsetof(HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7, ___face_2)); }
	inline int32_t get_face_2() const { return ___face_2; }
	inline int32_t* get_address_of_face_2() { return &___face_2; }
	inline void set_face_2(int32_t value)
	{
		___face_2 = value;
	}

	inline static int32_t get_offset_of_nextHalfEdge_3() { return static_cast<int32_t>(offsetof(HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7, ___nextHalfEdge_3)); }
	inline int32_t get_nextHalfEdge_3() const { return ___nextHalfEdge_3; }
	inline int32_t* get_address_of_nextHalfEdge_3() { return &___nextHalfEdge_3; }
	inline void set_nextHalfEdge_3(int32_t value)
	{
		___nextHalfEdge_3 = value;
	}

	inline static int32_t get_offset_of_pair_4() { return static_cast<int32_t>(offsetof(HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7, ___pair_4)); }
	inline int32_t get_pair_4() const { return ___pair_4; }
	inline int32_t* get_address_of_pair_4() { return &___pair_4; }
	inline void set_pair_4(int32_t value)
	{
		___pair_4 = value;
	}

	inline static int32_t get_offset_of_endVertex_5() { return static_cast<int32_t>(offsetof(HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7, ___endVertex_5)); }
	inline int32_t get_endVertex_5() const { return ___endVertex_5; }
	inline int32_t* get_address_of_endVertex_5() { return &___endVertex_5; }
	inline void set_endVertex_5(int32_t value)
	{
		___endVertex_5 = value;
	}
};


// Oni/ProfileInfo
struct ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A 
{
public:
	// System.Double Oni/ProfileInfo::start
	double ___start_0;
	// System.Double Oni/ProfileInfo::end
	double ___end_1;
	// System.UInt32 Oni/ProfileInfo::info
	uint32_t ___info_2;
	// System.Int32 Oni/ProfileInfo::pad
	int32_t ___pad_3;
	// System.String Oni/ProfileInfo::name
	String_t* ___name_4;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___start_0)); }
	inline double get_start_0() const { return ___start_0; }
	inline double* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(double value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_end_1() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___end_1)); }
	inline double get_end_1() const { return ___end_1; }
	inline double* get_address_of_end_1() { return &___end_1; }
	inline void set_end_1(double value)
	{
		___end_1 = value;
	}

	inline static int32_t get_offset_of_info_2() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___info_2)); }
	inline uint32_t get_info_2() const { return ___info_2; }
	inline uint32_t* get_address_of_info_2() { return &___info_2; }
	inline void set_info_2(uint32_t value)
	{
		___info_2 = value;
	}

	inline static int32_t get_offset_of_pad_3() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___pad_3)); }
	inline int32_t get_pad_3() const { return ___pad_3; }
	inline int32_t* get_address_of_pad_3() { return &___pad_3; }
	inline void set_pad_3(int32_t value)
	{
		___pad_3 = value;
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Oni/ProfileInfo
struct ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke
{
	double ___start_0;
	double ___end_1;
	uint32_t ___info_2;
	int32_t ___pad_3;
	char ___name_4[64];
};
// Native definition for COM marshalling of Oni/ProfileInfo
struct ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com
{
	double ___start_0;
	double ___end_1;
	uint32_t ___info_2;
	int32_t ___pad_3;
	char ___name_4[64];
};

// Obi.ConstraintBatcher/WorkItem/<constraints>e__FixedBuffer
struct U3CconstraintsU3Ee__FixedBuffer_tCECED784172DA0B4B7B94438FFDA15C8004E3A8E 
{
public:
	union
	{
		struct
		{
			// System.Int32 Obi.ConstraintBatcher/WorkItem/<constraints>e__FixedBuffer::FixedElementField
			int32_t ___FixedElementField_0;
		};
		uint8_t U3CconstraintsU3Ee__FixedBuffer_tCECED784172DA0B4B7B94438FFDA15C8004E3A8E__padding[256];
	};

public:
	inline static int32_t get_offset_of_FixedElementField_0() { return static_cast<int32_t>(offsetof(U3CconstraintsU3Ee__FixedBuffer_tCECED784172DA0B4B7B94438FFDA15C8004E3A8E, ___FixedElementField_0)); }
	inline int32_t get_FixedElementField_0() const { return ___FixedElementField_0; }
	inline int32_t* get_address_of_FixedElementField_0() { return &___FixedElementField_0; }
	inline void set_FixedElementField_0(int32_t value)
	{
		___FixedElementField_0 = value;
	}
};


// Unity.Collections.NativeList`1<Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>>
struct NativeList_1_t37C384457963A144915FA46915001309FA309A59 
{
public:
	// Unity.Collections.LowLevel.Unsafe.UnsafeList`1<T>* Unity.Collections.NativeList`1::m_ListData
	UnsafeList_1_t6961AC7E63FB8EACD1584397FB52AC37807B8EEF * ___m_ListData_0;
	// Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.NativeList`1::m_DeprecatedAllocator
	AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  ___m_DeprecatedAllocator_1;

public:
	inline static int32_t get_offset_of_m_ListData_0() { return static_cast<int32_t>(offsetof(NativeList_1_t37C384457963A144915FA46915001309FA309A59, ___m_ListData_0)); }
	inline UnsafeList_1_t6961AC7E63FB8EACD1584397FB52AC37807B8EEF * get_m_ListData_0() const { return ___m_ListData_0; }
	inline UnsafeList_1_t6961AC7E63FB8EACD1584397FB52AC37807B8EEF ** get_address_of_m_ListData_0() { return &___m_ListData_0; }
	inline void set_m_ListData_0(UnsafeList_1_t6961AC7E63FB8EACD1584397FB52AC37807B8EEF * value)
	{
		___m_ListData_0 = value;
	}

	inline static int32_t get_offset_of_m_DeprecatedAllocator_1() { return static_cast<int32_t>(offsetof(NativeList_1_t37C384457963A144915FA46915001309FA309A59, ___m_DeprecatedAllocator_1)); }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  get_m_DeprecatedAllocator_1() const { return ___m_DeprecatedAllocator_1; }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A * get_address_of_m_DeprecatedAllocator_1() { return &___m_DeprecatedAllocator_1; }
	inline void set_m_DeprecatedAllocator_1(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  value)
	{
		___m_DeprecatedAllocator_1 = value;
	}
};


// Unity.Collections.NativeList`1<System.Int32>
struct NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 
{
public:
	// Unity.Collections.LowLevel.Unsafe.UnsafeList`1<T>* Unity.Collections.NativeList`1::m_ListData
	UnsafeList_1_t0160F425C28D7D8E9DA796AD7C293EE00D425DF9 * ___m_ListData_0;
	// Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.NativeList`1::m_DeprecatedAllocator
	AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  ___m_DeprecatedAllocator_1;

public:
	inline static int32_t get_offset_of_m_ListData_0() { return static_cast<int32_t>(offsetof(NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80, ___m_ListData_0)); }
	inline UnsafeList_1_t0160F425C28D7D8E9DA796AD7C293EE00D425DF9 * get_m_ListData_0() const { return ___m_ListData_0; }
	inline UnsafeList_1_t0160F425C28D7D8E9DA796AD7C293EE00D425DF9 ** get_address_of_m_ListData_0() { return &___m_ListData_0; }
	inline void set_m_ListData_0(UnsafeList_1_t0160F425C28D7D8E9DA796AD7C293EE00D425DF9 * value)
	{
		___m_ListData_0 = value;
	}

	inline static int32_t get_offset_of_m_DeprecatedAllocator_1() { return static_cast<int32_t>(offsetof(NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80, ___m_DeprecatedAllocator_1)); }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  get_m_DeprecatedAllocator_1() const { return ___m_DeprecatedAllocator_1; }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A * get_address_of_m_DeprecatedAllocator_1() { return &___m_DeprecatedAllocator_1; }
	inline void set_m_DeprecatedAllocator_1(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  value)
	{
		___m_DeprecatedAllocator_1 = value;
	}
};


// Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2<System.Int32,System.Int32>
struct UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33 
{
public:
	// Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData* Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::m_Buffer
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 * ___m_Buffer_0;
	// Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::m_AllocatorLabel
	AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  ___m_AllocatorLabel_1;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33, ___m_Buffer_0)); }
	inline UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 * get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 ** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 * value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_1() { return static_cast<int32_t>(offsetof(UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33, ___m_AllocatorLabel_1)); }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  get_m_AllocatorLabel_1() const { return ___m_AllocatorLabel_1; }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A * get_address_of_m_AllocatorLabel_1() { return &___m_AllocatorLabel_1; }
	inline void set_m_AllocatorLabel_1(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  value)
	{
		___m_AllocatorLabel_1 = value;
	}
};


// Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2<Unity.Mathematics.int4,System.Int32>
struct UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83 
{
public:
	// Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData* Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::m_Buffer
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 * ___m_Buffer_0;
	// Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::m_AllocatorLabel
	AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  ___m_AllocatorLabel_1;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83, ___m_Buffer_0)); }
	inline UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 * get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 ** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82 * value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_1() { return static_cast<int32_t>(offsetof(UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83, ___m_AllocatorLabel_1)); }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  get_m_AllocatorLabel_1() const { return ___m_AllocatorLabel_1; }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A * get_address_of_m_AllocatorLabel_1() { return &___m_AllocatorLabel_1; }
	inline void set_m_AllocatorLabel_1(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  value)
	{
		___m_AllocatorLabel_1 = value;
	}
};


// Obi.Aabb
struct Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF 
{
public:
	// UnityEngine.Vector4 Obi.Aabb::min
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___min_0;
	// UnityEngine.Vector4 Obi.Aabb::max
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF, ___min_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_min_0() const { return ___min_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF, ___max_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_max_1() const { return ___max_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___max_1 = value;
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// Obi.BurstAabb
struct BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 
{
public:
	// Unity.Mathematics.float4 Obi.BurstAabb::min
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___min_0;
	// Unity.Mathematics.float4 Obi.BurstAabb::max
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87, ___min_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_min_0() const { return ___min_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87, ___max_1)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_max_1() const { return ___max_1; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___max_1 = value;
	}
};


// Obi.BurstContact
struct BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C 
{
public:
	// Unity.Mathematics.float4 Obi.BurstContact::pointA
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___pointA_0;
	// Unity.Mathematics.float4 Obi.BurstContact::pointB
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___pointB_1;
	// Unity.Mathematics.float4 Obi.BurstContact::normal
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___normal_2;
	// Unity.Mathematics.float4 Obi.BurstContact::tangent
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___tangent_3;
	// Unity.Mathematics.float4 Obi.BurstContact::bitangent
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___bitangent_4;
	// System.Single Obi.BurstContact::distance
	float ___distance_5;
	// System.Single Obi.BurstContact::normalLambda
	float ___normalLambda_6;
	// System.Single Obi.BurstContact::tangentLambda
	float ___tangentLambda_7;
	// System.Single Obi.BurstContact::bitangentLambda
	float ___bitangentLambda_8;
	// System.Single Obi.BurstContact::stickLambda
	float ___stickLambda_9;
	// System.Single Obi.BurstContact::rollingFrictionImpulse
	float ___rollingFrictionImpulse_10;
	// System.Int32 Obi.BurstContact::bodyA
	int32_t ___bodyA_11;
	// System.Int32 Obi.BurstContact::bodyB
	int32_t ___bodyB_12;
	// System.Single Obi.BurstContact::normalInvMassA
	float ___normalInvMassA_13;
	// System.Single Obi.BurstContact::tangentInvMassA
	float ___tangentInvMassA_14;
	// System.Single Obi.BurstContact::bitangentInvMassA
	float ___bitangentInvMassA_15;
	// System.Single Obi.BurstContact::normalInvMassB
	float ___normalInvMassB_16;
	// System.Single Obi.BurstContact::tangentInvMassB
	float ___tangentInvMassB_17;
	// System.Single Obi.BurstContact::bitangentInvMassB
	float ___bitangentInvMassB_18;
	// System.Double Obi.BurstContact::pad0
	double ___pad0_19;

public:
	inline static int32_t get_offset_of_pointA_0() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___pointA_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_pointA_0() const { return ___pointA_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_pointA_0() { return &___pointA_0; }
	inline void set_pointA_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___pointA_0 = value;
	}

	inline static int32_t get_offset_of_pointB_1() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___pointB_1)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_pointB_1() const { return ___pointB_1; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_pointB_1() { return &___pointB_1; }
	inline void set_pointB_1(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___pointB_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___normal_2)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_normal_2() const { return ___normal_2; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_tangent_3() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___tangent_3)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_tangent_3() const { return ___tangent_3; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_tangent_3() { return &___tangent_3; }
	inline void set_tangent_3(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___tangent_3 = value;
	}

	inline static int32_t get_offset_of_bitangent_4() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___bitangent_4)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_bitangent_4() const { return ___bitangent_4; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_bitangent_4() { return &___bitangent_4; }
	inline void set_bitangent_4(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___bitangent_4 = value;
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_normalLambda_6() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___normalLambda_6)); }
	inline float get_normalLambda_6() const { return ___normalLambda_6; }
	inline float* get_address_of_normalLambda_6() { return &___normalLambda_6; }
	inline void set_normalLambda_6(float value)
	{
		___normalLambda_6 = value;
	}

	inline static int32_t get_offset_of_tangentLambda_7() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___tangentLambda_7)); }
	inline float get_tangentLambda_7() const { return ___tangentLambda_7; }
	inline float* get_address_of_tangentLambda_7() { return &___tangentLambda_7; }
	inline void set_tangentLambda_7(float value)
	{
		___tangentLambda_7 = value;
	}

	inline static int32_t get_offset_of_bitangentLambda_8() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___bitangentLambda_8)); }
	inline float get_bitangentLambda_8() const { return ___bitangentLambda_8; }
	inline float* get_address_of_bitangentLambda_8() { return &___bitangentLambda_8; }
	inline void set_bitangentLambda_8(float value)
	{
		___bitangentLambda_8 = value;
	}

	inline static int32_t get_offset_of_stickLambda_9() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___stickLambda_9)); }
	inline float get_stickLambda_9() const { return ___stickLambda_9; }
	inline float* get_address_of_stickLambda_9() { return &___stickLambda_9; }
	inline void set_stickLambda_9(float value)
	{
		___stickLambda_9 = value;
	}

	inline static int32_t get_offset_of_rollingFrictionImpulse_10() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___rollingFrictionImpulse_10)); }
	inline float get_rollingFrictionImpulse_10() const { return ___rollingFrictionImpulse_10; }
	inline float* get_address_of_rollingFrictionImpulse_10() { return &___rollingFrictionImpulse_10; }
	inline void set_rollingFrictionImpulse_10(float value)
	{
		___rollingFrictionImpulse_10 = value;
	}

	inline static int32_t get_offset_of_bodyA_11() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___bodyA_11)); }
	inline int32_t get_bodyA_11() const { return ___bodyA_11; }
	inline int32_t* get_address_of_bodyA_11() { return &___bodyA_11; }
	inline void set_bodyA_11(int32_t value)
	{
		___bodyA_11 = value;
	}

	inline static int32_t get_offset_of_bodyB_12() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___bodyB_12)); }
	inline int32_t get_bodyB_12() const { return ___bodyB_12; }
	inline int32_t* get_address_of_bodyB_12() { return &___bodyB_12; }
	inline void set_bodyB_12(int32_t value)
	{
		___bodyB_12 = value;
	}

	inline static int32_t get_offset_of_normalInvMassA_13() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___normalInvMassA_13)); }
	inline float get_normalInvMassA_13() const { return ___normalInvMassA_13; }
	inline float* get_address_of_normalInvMassA_13() { return &___normalInvMassA_13; }
	inline void set_normalInvMassA_13(float value)
	{
		___normalInvMassA_13 = value;
	}

	inline static int32_t get_offset_of_tangentInvMassA_14() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___tangentInvMassA_14)); }
	inline float get_tangentInvMassA_14() const { return ___tangentInvMassA_14; }
	inline float* get_address_of_tangentInvMassA_14() { return &___tangentInvMassA_14; }
	inline void set_tangentInvMassA_14(float value)
	{
		___tangentInvMassA_14 = value;
	}

	inline static int32_t get_offset_of_bitangentInvMassA_15() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___bitangentInvMassA_15)); }
	inline float get_bitangentInvMassA_15() const { return ___bitangentInvMassA_15; }
	inline float* get_address_of_bitangentInvMassA_15() { return &___bitangentInvMassA_15; }
	inline void set_bitangentInvMassA_15(float value)
	{
		___bitangentInvMassA_15 = value;
	}

	inline static int32_t get_offset_of_normalInvMassB_16() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___normalInvMassB_16)); }
	inline float get_normalInvMassB_16() const { return ___normalInvMassB_16; }
	inline float* get_address_of_normalInvMassB_16() { return &___normalInvMassB_16; }
	inline void set_normalInvMassB_16(float value)
	{
		___normalInvMassB_16 = value;
	}

	inline static int32_t get_offset_of_tangentInvMassB_17() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___tangentInvMassB_17)); }
	inline float get_tangentInvMassB_17() const { return ___tangentInvMassB_17; }
	inline float* get_address_of_tangentInvMassB_17() { return &___tangentInvMassB_17; }
	inline void set_tangentInvMassB_17(float value)
	{
		___tangentInvMassB_17 = value;
	}

	inline static int32_t get_offset_of_bitangentInvMassB_18() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___bitangentInvMassB_18)); }
	inline float get_bitangentInvMassB_18() const { return ___bitangentInvMassB_18; }
	inline float* get_address_of_bitangentInvMassB_18() { return &___bitangentInvMassB_18; }
	inline void set_bitangentInvMassB_18(float value)
	{
		___bitangentInvMassB_18 = value;
	}

	inline static int32_t get_offset_of_pad0_19() { return static_cast<int32_t>(offsetof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C, ___pad0_19)); }
	inline double get_pad0_19() const { return ___pad0_19; }
	inline double* get_address_of_pad0_19() { return &___pad0_19; }
	inline void set_pad0_19(double value)
	{
		___pad0_19 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// Obi.FluidInteraction
struct FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D 
{
public:
	// Unity.Mathematics.float4 Obi.FluidInteraction::gradient
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___gradient_0;
	// System.Single Obi.FluidInteraction::avgKernel
	float ___avgKernel_1;
	// System.Single Obi.FluidInteraction::avgGradient
	float ___avgGradient_2;
	// System.Int32 Obi.FluidInteraction::particleA
	int32_t ___particleA_3;
	// System.Int32 Obi.FluidInteraction::particleB
	int32_t ___particleB_4;

public:
	inline static int32_t get_offset_of_gradient_0() { return static_cast<int32_t>(offsetof(FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D, ___gradient_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_gradient_0() const { return ___gradient_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_gradient_0() { return &___gradient_0; }
	inline void set_gradient_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___gradient_0 = value;
	}

	inline static int32_t get_offset_of_avgKernel_1() { return static_cast<int32_t>(offsetof(FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D, ___avgKernel_1)); }
	inline float get_avgKernel_1() const { return ___avgKernel_1; }
	inline float* get_address_of_avgKernel_1() { return &___avgKernel_1; }
	inline void set_avgKernel_1(float value)
	{
		___avgKernel_1 = value;
	}

	inline static int32_t get_offset_of_avgGradient_2() { return static_cast<int32_t>(offsetof(FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D, ___avgGradient_2)); }
	inline float get_avgGradient_2() const { return ___avgGradient_2; }
	inline float* get_address_of_avgGradient_2() { return &___avgGradient_2; }
	inline void set_avgGradient_2(float value)
	{
		___avgGradient_2 = value;
	}

	inline static int32_t get_offset_of_particleA_3() { return static_cast<int32_t>(offsetof(FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D, ___particleA_3)); }
	inline int32_t get_particleA_3() const { return ___particleA_3; }
	inline int32_t* get_address_of_particleA_3() { return &___particleA_3; }
	inline void set_particleA_3(int32_t value)
	{
		___particleA_3 = value;
	}

	inline static int32_t get_offset_of_particleB_4() { return static_cast<int32_t>(offsetof(FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D, ___particleB_4)); }
	inline int32_t get_particleB_4() const { return ___particleB_4; }
	inline int32_t* get_address_of_particleB_4() { return &___particleB_4; }
	inline void set_particleB_4(int32_t value)
	{
		___particleB_4 = value;
	}
};


// Obi.HalfEdgeMesh
struct HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Obi.HalfEdgeMesh::inputMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___inputMesh_0;
	// UnityEngine.Vector3 Obi.HalfEdgeMesh::scale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale_1;
	// System.Single Obi.HalfEdgeMesh::_area
	float ____area_2;
	// System.Single Obi.HalfEdgeMesh::_volume
	float ____volume_3;
	// System.Boolean Obi.HalfEdgeMesh::containsData
	bool ___containsData_4;
	// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex> Obi.HalfEdgeMesh::vertices
	List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * ___vertices_5;
	// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh::halfEdges
	List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B * ___halfEdges_6;
	// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh::borderEdges
	List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B * ___borderEdges_7;
	// System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face> Obi.HalfEdgeMesh::faces
	List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * ___faces_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Obi.HalfEdgeMesh::restNormals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___restNormals_9;
	// System.Collections.Generic.List`1<UnityEngine.Quaternion> Obi.HalfEdgeMesh::restOrientations
	List_1_t2558DEC96F7E6007750607B083ADB3AC48A30CCB * ___restOrientations_10;
	// System.Collections.Generic.List`1<System.Int32> Obi.HalfEdgeMesh::rawToWelded
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___rawToWelded_11;

public:
	inline static int32_t get_offset_of_inputMesh_0() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___inputMesh_0)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_inputMesh_0() const { return ___inputMesh_0; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_inputMesh_0() { return &___inputMesh_0; }
	inline void set_inputMesh_0(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___inputMesh_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputMesh_0), (void*)value);
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___scale_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scale_1() const { return ___scale_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of__area_2() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ____area_2)); }
	inline float get__area_2() const { return ____area_2; }
	inline float* get_address_of__area_2() { return &____area_2; }
	inline void set__area_2(float value)
	{
		____area_2 = value;
	}

	inline static int32_t get_offset_of__volume_3() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ____volume_3)); }
	inline float get__volume_3() const { return ____volume_3; }
	inline float* get_address_of__volume_3() { return &____volume_3; }
	inline void set__volume_3(float value)
	{
		____volume_3 = value;
	}

	inline static int32_t get_offset_of_containsData_4() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___containsData_4)); }
	inline bool get_containsData_4() const { return ___containsData_4; }
	inline bool* get_address_of_containsData_4() { return &___containsData_4; }
	inline void set_containsData_4(bool value)
	{
		___containsData_4 = value;
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___vertices_5)); }
	inline List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * get_vertices_5() const { return ___vertices_5; }
	inline List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 ** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vertices_5), (void*)value);
	}

	inline static int32_t get_offset_of_halfEdges_6() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___halfEdges_6)); }
	inline List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B * get_halfEdges_6() const { return ___halfEdges_6; }
	inline List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B ** get_address_of_halfEdges_6() { return &___halfEdges_6; }
	inline void set_halfEdges_6(List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B * value)
	{
		___halfEdges_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___halfEdges_6), (void*)value);
	}

	inline static int32_t get_offset_of_borderEdges_7() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___borderEdges_7)); }
	inline List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B * get_borderEdges_7() const { return ___borderEdges_7; }
	inline List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B ** get_address_of_borderEdges_7() { return &___borderEdges_7; }
	inline void set_borderEdges_7(List_1_tF956F2D8EB3EA3903D3CDC902107E8035179BB1B * value)
	{
		___borderEdges_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___borderEdges_7), (void*)value);
	}

	inline static int32_t get_offset_of_faces_8() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___faces_8)); }
	inline List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * get_faces_8() const { return ___faces_8; }
	inline List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C ** get_address_of_faces_8() { return &___faces_8; }
	inline void set_faces_8(List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * value)
	{
		___faces_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___faces_8), (void*)value);
	}

	inline static int32_t get_offset_of_restNormals_9() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___restNormals_9)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_restNormals_9() const { return ___restNormals_9; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_restNormals_9() { return &___restNormals_9; }
	inline void set_restNormals_9(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___restNormals_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restNormals_9), (void*)value);
	}

	inline static int32_t get_offset_of_restOrientations_10() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___restOrientations_10)); }
	inline List_1_t2558DEC96F7E6007750607B083ADB3AC48A30CCB * get_restOrientations_10() const { return ___restOrientations_10; }
	inline List_1_t2558DEC96F7E6007750607B083ADB3AC48A30CCB ** get_address_of_restOrientations_10() { return &___restOrientations_10; }
	inline void set_restOrientations_10(List_1_t2558DEC96F7E6007750607B083ADB3AC48A30CCB * value)
	{
		___restOrientations_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restOrientations_10), (void*)value);
	}

	inline static int32_t get_offset_of_rawToWelded_11() { return static_cast<int32_t>(offsetof(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7, ___rawToWelded_11)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_rawToWelded_11() const { return ___rawToWelded_11; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_rawToWelded_11() { return &___rawToWelded_11; }
	inline void set_rawToWelded_11(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___rawToWelded_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rawToWelded_11), (void*)value);
	}
};


// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.MeshVoxelizer
struct MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Obi.MeshVoxelizer::input
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___input_5;
	// Obi.MeshVoxelizer/Voxel[] Obi.MeshVoxelizer::voxels
	VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55* ___voxels_6;
	// System.Single Obi.MeshVoxelizer::voxelSize
	float ___voxelSize_7;
	// UnityEngine.Vector3Int Obi.MeshVoxelizer::resolution
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___resolution_8;
	// System.Collections.Generic.List`1<System.Int32>[] Obi.MeshVoxelizer::triangleIndices
	List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* ___triangleIndices_9;
	// UnityEngine.Vector3Int Obi.MeshVoxelizer::origin
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___origin_10;

public:
	inline static int32_t get_offset_of_input_5() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___input_5)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_input_5() const { return ___input_5; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_input_5() { return &___input_5; }
	inline void set_input_5(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___input_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___input_5), (void*)value);
	}

	inline static int32_t get_offset_of_voxels_6() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___voxels_6)); }
	inline VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55* get_voxels_6() const { return ___voxels_6; }
	inline VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55** get_address_of_voxels_6() { return &___voxels_6; }
	inline void set_voxels_6(VoxelU5BU5D_tA0FDC07F9F3BB9F86147BF6E063718D1BAAD3E55* value)
	{
		___voxels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voxels_6), (void*)value);
	}

	inline static int32_t get_offset_of_voxelSize_7() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___voxelSize_7)); }
	inline float get_voxelSize_7() const { return ___voxelSize_7; }
	inline float* get_address_of_voxelSize_7() { return &___voxelSize_7; }
	inline void set_voxelSize_7(float value)
	{
		___voxelSize_7 = value;
	}

	inline static int32_t get_offset_of_resolution_8() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___resolution_8)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_resolution_8() const { return ___resolution_8; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_resolution_8() { return &___resolution_8; }
	inline void set_resolution_8(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___resolution_8 = value;
	}

	inline static int32_t get_offset_of_triangleIndices_9() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___triangleIndices_9)); }
	inline List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* get_triangleIndices_9() const { return ___triangleIndices_9; }
	inline List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C** get_address_of_triangleIndices_9() { return &___triangleIndices_9; }
	inline void set_triangleIndices_9(List_1U5BU5D_tABF499D891C900D1171501E0545AD96030B3877C* value)
	{
		___triangleIndices_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triangleIndices_9), (void*)value);
	}

	inline static int32_t get_offset_of_origin_10() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D, ___origin_10)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_origin_10() const { return ___origin_10; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_origin_10() { return &___origin_10; }
	inline void set_origin_10(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___origin_10 = value;
	}
};

struct MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields
{
public:
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::fullNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___fullNeighborhood_0;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::edgefaceNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___edgefaceNeighborhood_1;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::faceNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___faceNeighborhood_2;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::edgeNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___edgeNeighborhood_3;
	// UnityEngine.Vector3Int[] Obi.MeshVoxelizer::vertexNeighborhood
	Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* ___vertexNeighborhood_4;

public:
	inline static int32_t get_offset_of_fullNeighborhood_0() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___fullNeighborhood_0)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_fullNeighborhood_0() const { return ___fullNeighborhood_0; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_fullNeighborhood_0() { return &___fullNeighborhood_0; }
	inline void set_fullNeighborhood_0(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___fullNeighborhood_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fullNeighborhood_0), (void*)value);
	}

	inline static int32_t get_offset_of_edgefaceNeighborhood_1() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___edgefaceNeighborhood_1)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_edgefaceNeighborhood_1() const { return ___edgefaceNeighborhood_1; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_edgefaceNeighborhood_1() { return &___edgefaceNeighborhood_1; }
	inline void set_edgefaceNeighborhood_1(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___edgefaceNeighborhood_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edgefaceNeighborhood_1), (void*)value);
	}

	inline static int32_t get_offset_of_faceNeighborhood_2() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___faceNeighborhood_2)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_faceNeighborhood_2() const { return ___faceNeighborhood_2; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_faceNeighborhood_2() { return &___faceNeighborhood_2; }
	inline void set_faceNeighborhood_2(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___faceNeighborhood_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___faceNeighborhood_2), (void*)value);
	}

	inline static int32_t get_offset_of_edgeNeighborhood_3() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___edgeNeighborhood_3)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_edgeNeighborhood_3() const { return ___edgeNeighborhood_3; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_edgeNeighborhood_3() { return &___edgeNeighborhood_3; }
	inline void set_edgeNeighborhood_3(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___edgeNeighborhood_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edgeNeighborhood_3), (void*)value);
	}

	inline static int32_t get_offset_of_vertexNeighborhood_4() { return static_cast<int32_t>(offsetof(MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_StaticFields, ___vertexNeighborhood_4)); }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* get_vertexNeighborhood_4() const { return ___vertexNeighborhood_4; }
	inline Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D** get_address_of_vertexNeighborhood_4() { return &___vertexNeighborhood_4; }
	inline void set_vertexNeighborhood_4(Vector3IntU5BU5D_t7DAC6D862D51B078659528745F8248658F262D7D* value)
	{
		___vertexNeighborhood_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vertexNeighborhood_4), (void*)value);
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Unity.Collections.LowLevel.Unsafe.UnsafeList
struct UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA 
{
public:
	// System.Void* Unity.Collections.LowLevel.Unsafe.UnsafeList::Ptr
	void* ___Ptr_0;
	// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeList::Length
	int32_t ___Length_1;
	// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeList::unused
	int32_t ___unused_2;
	// System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeList::Capacity
	int32_t ___Capacity_3;
	// Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.LowLevel.Unsafe.UnsafeList::Allocator
	AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  ___Allocator_4;

public:
	inline static int32_t get_offset_of_Ptr_0() { return static_cast<int32_t>(offsetof(UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA, ___Ptr_0)); }
	inline void* get_Ptr_0() const { return ___Ptr_0; }
	inline void** get_address_of_Ptr_0() { return &___Ptr_0; }
	inline void set_Ptr_0(void* value)
	{
		___Ptr_0 = value;
	}

	inline static int32_t get_offset_of_Length_1() { return static_cast<int32_t>(offsetof(UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA, ___Length_1)); }
	inline int32_t get_Length_1() const { return ___Length_1; }
	inline int32_t* get_address_of_Length_1() { return &___Length_1; }
	inline void set_Length_1(int32_t value)
	{
		___Length_1 = value;
	}

	inline static int32_t get_offset_of_unused_2() { return static_cast<int32_t>(offsetof(UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA, ___unused_2)); }
	inline int32_t get_unused_2() const { return ___unused_2; }
	inline int32_t* get_address_of_unused_2() { return &___unused_2; }
	inline void set_unused_2(int32_t value)
	{
		___unused_2 = value;
	}

	inline static int32_t get_offset_of_Capacity_3() { return static_cast<int32_t>(offsetof(UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA, ___Capacity_3)); }
	inline int32_t get_Capacity_3() const { return ___Capacity_3; }
	inline int32_t* get_address_of_Capacity_3() { return &___Capacity_3; }
	inline void set_Capacity_3(int32_t value)
	{
		___Capacity_3 = value;
	}

	inline static int32_t get_offset_of_Allocator_4() { return static_cast<int32_t>(offsetof(UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA, ___Allocator_4)); }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  get_Allocator_4() const { return ___Allocator_4; }
	inline AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A * get_address_of_Allocator_4() { return &___Allocator_4; }
	inline void set_Allocator_4(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A  value)
	{
		___Allocator_4 = value;
	}
};


// Unity.Mathematics.quaternion
struct quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F 
{
public:
	// Unity.Mathematics.float4 Unity.Mathematics.quaternion::value
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F, ___value_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_value_0() const { return ___value_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___value_0 = value;
	}
};

struct quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_StaticFields
{
public:
	// Unity.Mathematics.quaternion Unity.Mathematics.quaternion::identity
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___identity_1;

public:
	inline static int32_t get_offset_of_identity_1() { return static_cast<int32_t>(offsetof(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F_StaticFields, ___identity_1)); }
	inline quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  get_identity_1() const { return ___identity_1; }
	inline quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * get_address_of_identity_1() { return &___identity_1; }
	inline void set_identity_1(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  value)
	{
		___identity_1 = value;
	}
};


// Obi.BurstLocalOptimization/SurfacePoint
struct SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED 
{
public:
	// Unity.Mathematics.float4 Obi.BurstLocalOptimization/SurfacePoint::bary
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___bary_0;
	// Unity.Mathematics.float4 Obi.BurstLocalOptimization/SurfacePoint::point
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___point_1;
	// Unity.Mathematics.float4 Obi.BurstLocalOptimization/SurfacePoint::normal
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___normal_2;

public:
	inline static int32_t get_offset_of_bary_0() { return static_cast<int32_t>(offsetof(SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED, ___bary_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_bary_0() const { return ___bary_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_bary_0() { return &___bary_0; }
	inline void set_bary_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___bary_0 = value;
	}

	inline static int32_t get_offset_of_point_1() { return static_cast<int32_t>(offsetof(SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED, ___point_1)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_point_1() const { return ___point_1; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_point_1() { return &___point_1; }
	inline void set_point_1(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___point_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED, ___normal_2)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_normal_2() const { return ___normal_2; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___normal_2 = value;
	}
};


// Obi.BurstMath/CachedTri
struct CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B 
{
public:
	// Unity.Mathematics.float4 Obi.BurstMath/CachedTri::vertex
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___vertex_0;
	// Unity.Mathematics.float4 Obi.BurstMath/CachedTri::edge0
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___edge0_1;
	// Unity.Mathematics.float4 Obi.BurstMath/CachedTri::edge1
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___edge1_2;
	// Unity.Mathematics.float4 Obi.BurstMath/CachedTri::data
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___data_3;

public:
	inline static int32_t get_offset_of_vertex_0() { return static_cast<int32_t>(offsetof(CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B, ___vertex_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_vertex_0() const { return ___vertex_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_vertex_0() { return &___vertex_0; }
	inline void set_vertex_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___vertex_0 = value;
	}

	inline static int32_t get_offset_of_edge0_1() { return static_cast<int32_t>(offsetof(CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B, ___edge0_1)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_edge0_1() const { return ___edge0_1; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_edge0_1() { return &___edge0_1; }
	inline void set_edge0_1(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___edge0_1 = value;
	}

	inline static int32_t get_offset_of_edge1_2() { return static_cast<int32_t>(offsetof(CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B, ___edge1_2)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_edge1_2() const { return ___edge1_2; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_edge1_2() { return &___edge1_2; }
	inline void set_edge1_2(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___edge1_2 = value;
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B, ___data_3)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_data_3() const { return ___data_3; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___data_3 = value;
	}
};


// Obi.HalfEdgeMesh/Vertex
struct Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD 
{
public:
	// System.Int32 Obi.HalfEdgeMesh/Vertex::index
	int32_t ___index_0;
	// System.Int32 Obi.HalfEdgeMesh/Vertex::halfEdge
	int32_t ___halfEdge_1;
	// UnityEngine.Vector3 Obi.HalfEdgeMesh/Vertex::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_halfEdge_1() { return static_cast<int32_t>(offsetof(Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD, ___halfEdge_1)); }
	inline int32_t get_halfEdge_1() const { return ___halfEdge_1; }
	inline int32_t* get_address_of_halfEdge_1() { return &___halfEdge_1; }
	inline void set_halfEdge_1(int32_t value)
	{
		___halfEdge_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD, ___position_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_2() const { return ___position_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_2 = value;
	}
};


// Obi.MeshVoxelizer/Voxel
struct Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3 
{
public:
	// System.Int32 Obi.MeshVoxelizer/Voxel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.ObiTriangleSkinMap/<Bind>d__31
struct U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiTriangleSkinMap/<Bind>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Obi.ObiTriangleSkinMap Obi.ObiTriangleSkinMap/<Bind>d__31::<>4__this
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * ___U3CU3E4__this_2;
	// UnityEngine.Vector3[] Obi.ObiTriangleSkinMap/<Bind>d__31::<slavePositions>5__2
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___U3CslavePositionsU3E5__2_3;
	// UnityEngine.Vector3[] Obi.ObiTriangleSkinMap/<Bind>d__31::<slaveNormals>5__3
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___U3CslaveNormalsU3E5__3_4;
	// UnityEngine.Vector4[] Obi.ObiTriangleSkinMap/<Bind>d__31::<slaveTangents>5__4
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___U3CslaveTangentsU3E5__4_5;
	// UnityEngine.Matrix4x4 Obi.ObiTriangleSkinMap/<Bind>d__31::<s2world>5__5
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___U3Cs2worldU3E5__5_6;
	// UnityEngine.Matrix4x4 Obi.ObiTriangleSkinMap/<Bind>d__31::<s2worldNormal>5__6
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___U3Cs2worldNormalU3E5__6_7;
	// Obi.ObiTriangleSkinMap/MasterFace[] Obi.ObiTriangleSkinMap/<Bind>d__31::<masterFaces>5__7
	MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* ___U3CmasterFacesU3E5__7_8;
	// System.Int32 Obi.ObiTriangleSkinMap/<Bind>d__31::<count>5__8
	int32_t ___U3CcountU3E5__8_9;
	// System.Int32 Obi.ObiTriangleSkinMap/<Bind>d__31::<i>5__9
	int32_t ___U3CiU3E5__9_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CU3E4__this_2)); }
	inline ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CslavePositionsU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CslavePositionsU3E5__2_3)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_U3CslavePositionsU3E5__2_3() const { return ___U3CslavePositionsU3E5__2_3; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_U3CslavePositionsU3E5__2_3() { return &___U3CslavePositionsU3E5__2_3; }
	inline void set_U3CslavePositionsU3E5__2_3(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___U3CslavePositionsU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CslavePositionsU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CslaveNormalsU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CslaveNormalsU3E5__3_4)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_U3CslaveNormalsU3E5__3_4() const { return ___U3CslaveNormalsU3E5__3_4; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_U3CslaveNormalsU3E5__3_4() { return &___U3CslaveNormalsU3E5__3_4; }
	inline void set_U3CslaveNormalsU3E5__3_4(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___U3CslaveNormalsU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CslaveNormalsU3E5__3_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CslaveTangentsU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CslaveTangentsU3E5__4_5)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get_U3CslaveTangentsU3E5__4_5() const { return ___U3CslaveTangentsU3E5__4_5; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of_U3CslaveTangentsU3E5__4_5() { return &___U3CslaveTangentsU3E5__4_5; }
	inline void set_U3CslaveTangentsU3E5__4_5(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		___U3CslaveTangentsU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CslaveTangentsU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cs2worldU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3Cs2worldU3E5__5_6)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_U3Cs2worldU3E5__5_6() const { return ___U3Cs2worldU3E5__5_6; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_U3Cs2worldU3E5__5_6() { return &___U3Cs2worldU3E5__5_6; }
	inline void set_U3Cs2worldU3E5__5_6(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___U3Cs2worldU3E5__5_6 = value;
	}

	inline static int32_t get_offset_of_U3Cs2worldNormalU3E5__6_7() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3Cs2worldNormalU3E5__6_7)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_U3Cs2worldNormalU3E5__6_7() const { return ___U3Cs2worldNormalU3E5__6_7; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_U3Cs2worldNormalU3E5__6_7() { return &___U3Cs2worldNormalU3E5__6_7; }
	inline void set_U3Cs2worldNormalU3E5__6_7(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___U3Cs2worldNormalU3E5__6_7 = value;
	}

	inline static int32_t get_offset_of_U3CmasterFacesU3E5__7_8() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CmasterFacesU3E5__7_8)); }
	inline MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* get_U3CmasterFacesU3E5__7_8() const { return ___U3CmasterFacesU3E5__7_8; }
	inline MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4** get_address_of_U3CmasterFacesU3E5__7_8() { return &___U3CmasterFacesU3E5__7_8; }
	inline void set_U3CmasterFacesU3E5__7_8(MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* value)
	{
		___U3CmasterFacesU3E5__7_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmasterFacesU3E5__7_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountU3E5__8_9() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CcountU3E5__8_9)); }
	inline int32_t get_U3CcountU3E5__8_9() const { return ___U3CcountU3E5__8_9; }
	inline int32_t* get_address_of_U3CcountU3E5__8_9() { return &___U3CcountU3E5__8_9; }
	inline void set_U3CcountU3E5__8_9(int32_t value)
	{
		___U3CcountU3E5__8_9 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E5__9_10() { return static_cast<int32_t>(offsetof(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54, ___U3CiU3E5__9_10)); }
	inline int32_t get_U3CiU3E5__9_10() const { return ___U3CiU3E5__9_10; }
	inline int32_t* get_address_of_U3CiU3E5__9_10() { return &___U3CiU3E5__9_10; }
	inline void set_U3CiU3E5__9_10(int32_t value)
	{
		___U3CiU3E5__9_10 = value;
	}
};


// Obi.ObiTriangleSkinMap/BarycentricPoint
struct BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 
{
public:
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/BarycentricPoint::barycentricCoords
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___barycentricCoords_0;
	// System.Single Obi.ObiTriangleSkinMap/BarycentricPoint::height
	float ___height_1;

public:
	inline static int32_t get_offset_of_barycentricCoords_0() { return static_cast<int32_t>(offsetof(BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0, ___barycentricCoords_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_barycentricCoords_0() const { return ___barycentricCoords_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_barycentricCoords_0() { return &___barycentricCoords_0; }
	inline void set_barycentricCoords_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___barycentricCoords_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}
};


// Obi.ObiTriangleSkinMap/MasterFace
struct MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::p1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___p1_0;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::p2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___p2_1;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::p3
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___p3_2;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::n1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___n1_3;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::n2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___n2_4;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::n3
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___n3_5;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::v0
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0_6;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::v1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v1_7;
	// System.Single Obi.ObiTriangleSkinMap/MasterFace::dot00
	float ___dot00_8;
	// System.Single Obi.ObiTriangleSkinMap/MasterFace::dot01
	float ___dot01_9;
	// System.Single Obi.ObiTriangleSkinMap/MasterFace::dot11
	float ___dot11_10;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/MasterFace::faceNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___faceNormal_11;
	// System.Single Obi.ObiTriangleSkinMap/MasterFace::size
	float ___size_12;
	// System.Int32 Obi.ObiTriangleSkinMap/MasterFace::index
	int32_t ___index_13;
	// System.UInt32 Obi.ObiTriangleSkinMap/MasterFace::master
	uint32_t ___master_14;

public:
	inline static int32_t get_offset_of_p1_0() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___p1_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_p1_0() const { return ___p1_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_p1_0() { return &___p1_0; }
	inline void set_p1_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___p1_0 = value;
	}

	inline static int32_t get_offset_of_p2_1() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___p2_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_p2_1() const { return ___p2_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_p2_1() { return &___p2_1; }
	inline void set_p2_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___p2_1 = value;
	}

	inline static int32_t get_offset_of_p3_2() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___p3_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_p3_2() const { return ___p3_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_p3_2() { return &___p3_2; }
	inline void set_p3_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___p3_2 = value;
	}

	inline static int32_t get_offset_of_n1_3() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___n1_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_n1_3() const { return ___n1_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_n1_3() { return &___n1_3; }
	inline void set_n1_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___n1_3 = value;
	}

	inline static int32_t get_offset_of_n2_4() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___n2_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_n2_4() const { return ___n2_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_n2_4() { return &___n2_4; }
	inline void set_n2_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___n2_4 = value;
	}

	inline static int32_t get_offset_of_n3_5() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___n3_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_n3_5() const { return ___n3_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_n3_5() { return &___n3_5; }
	inline void set_n3_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___n3_5 = value;
	}

	inline static int32_t get_offset_of_v0_6() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___v0_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_v0_6() const { return ___v0_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_v0_6() { return &___v0_6; }
	inline void set_v0_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___v0_6 = value;
	}

	inline static int32_t get_offset_of_v1_7() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___v1_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_v1_7() const { return ___v1_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_v1_7() { return &___v1_7; }
	inline void set_v1_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___v1_7 = value;
	}

	inline static int32_t get_offset_of_dot00_8() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___dot00_8)); }
	inline float get_dot00_8() const { return ___dot00_8; }
	inline float* get_address_of_dot00_8() { return &___dot00_8; }
	inline void set_dot00_8(float value)
	{
		___dot00_8 = value;
	}

	inline static int32_t get_offset_of_dot01_9() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___dot01_9)); }
	inline float get_dot01_9() const { return ___dot01_9; }
	inline float* get_address_of_dot01_9() { return &___dot01_9; }
	inline void set_dot01_9(float value)
	{
		___dot01_9 = value;
	}

	inline static int32_t get_offset_of_dot11_10() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___dot11_10)); }
	inline float get_dot11_10() const { return ___dot11_10; }
	inline float* get_address_of_dot11_10() { return &___dot11_10; }
	inline void set_dot11_10(float value)
	{
		___dot11_10 = value;
	}

	inline static int32_t get_offset_of_faceNormal_11() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___faceNormal_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_faceNormal_11() const { return ___faceNormal_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_faceNormal_11() { return &___faceNormal_11; }
	inline void set_faceNormal_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___faceNormal_11 = value;
	}

	inline static int32_t get_offset_of_size_12() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___size_12)); }
	inline float get_size_12() const { return ___size_12; }
	inline float* get_address_of_size_12() { return &___size_12; }
	inline void set_size_12(float value)
	{
		___size_12 = value;
	}

	inline static int32_t get_offset_of_index_13() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___index_13)); }
	inline int32_t get_index_13() const { return ___index_13; }
	inline int32_t* get_address_of_index_13() { return &___index_13; }
	inline void set_index_13(int32_t value)
	{
		___index_13 = value;
	}

	inline static int32_t get_offset_of_master_14() { return static_cast<int32_t>(offsetof(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46, ___master_14)); }
	inline uint32_t get_master_14() const { return ___master_14; }
	inline uint32_t* get_address_of_master_14() { return &___master_14; }
	inline void set_master_14(uint32_t value)
	{
		___master_14 = value;
	}
};


// Obi.ObiTriangleSkinMap/SkinTransform
struct SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 
{
public:
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/SkinTransform::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Quaternion Obi.ObiTriangleSkinMap/SkinTransform::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_1;
	// UnityEngine.Vector3 Obi.ObiTriangleSkinMap/SkinTransform::scale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale_2;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4, ___rotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_1 = value;
	}

	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4, ___scale_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scale_2() const { return ___scale_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scale_2 = value;
	}
};


// Obi.ObiUtils/ParticleFlags
struct ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F 
{
public:
	// System.Int32 Obi.ObiUtils/ParticleFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/ConstraintType
struct ConstraintType_t9E2A1C0D5B41982E641515858185BA43DEE0F5CC 
{
public:
	// System.Int32 Oni/ConstraintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstraintType_t9E2A1C0D5B41982E641515858185BA43DEE0F5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/Contact
struct Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756 
{
public:
	union
	{
		struct
		{
			// UnityEngine.Vector4 Oni/Contact::pointA
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___pointA_0;
			// UnityEngine.Vector4 Oni/Contact::pointB
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___pointB_1;
			// UnityEngine.Vector4 Oni/Contact::normal
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___normal_2;
			// UnityEngine.Vector4 Oni/Contact::tangent
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___tangent_3;
			// UnityEngine.Vector4 Oni/Contact::bitangent
			Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___bitangent_4;
			// System.Single Oni/Contact::distance
			float ___distance_5;
			// System.Single Oni/Contact::normalImpulse
			float ___normalImpulse_6;
			// System.Single Oni/Contact::tangentImpulse
			float ___tangentImpulse_7;
			// System.Single Oni/Contact::bitangentImpulse
			float ___bitangentImpulse_8;
			// System.Single Oni/Contact::stickImpulse
			float ___stickImpulse_9;
			// System.Single Oni/Contact::rollingFrictionImpulse
			float ___rollingFrictionImpulse_10;
			// System.Int32 Oni/Contact::bodyA
			int32_t ___bodyA_11;
			// System.Int32 Oni/Contact::bodyB
			int32_t ___bodyB_12;
		};
		uint8_t Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756__padding[144];
	};

public:
	inline static int32_t get_offset_of_pointA_0() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___pointA_0)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_pointA_0() const { return ___pointA_0; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_pointA_0() { return &___pointA_0; }
	inline void set_pointA_0(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___pointA_0 = value;
	}

	inline static int32_t get_offset_of_pointB_1() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___pointB_1)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_pointB_1() const { return ___pointB_1; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_pointB_1() { return &___pointB_1; }
	inline void set_pointB_1(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___pointB_1 = value;
	}

	inline static int32_t get_offset_of_normal_2() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___normal_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_normal_2() const { return ___normal_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_normal_2() { return &___normal_2; }
	inline void set_normal_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___normal_2 = value;
	}

	inline static int32_t get_offset_of_tangent_3() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___tangent_3)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_tangent_3() const { return ___tangent_3; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_tangent_3() { return &___tangent_3; }
	inline void set_tangent_3(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___tangent_3 = value;
	}

	inline static int32_t get_offset_of_bitangent_4() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bitangent_4)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_bitangent_4() const { return ___bitangent_4; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_bitangent_4() { return &___bitangent_4; }
	inline void set_bitangent_4(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___bitangent_4 = value;
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_normalImpulse_6() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___normalImpulse_6)); }
	inline float get_normalImpulse_6() const { return ___normalImpulse_6; }
	inline float* get_address_of_normalImpulse_6() { return &___normalImpulse_6; }
	inline void set_normalImpulse_6(float value)
	{
		___normalImpulse_6 = value;
	}

	inline static int32_t get_offset_of_tangentImpulse_7() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___tangentImpulse_7)); }
	inline float get_tangentImpulse_7() const { return ___tangentImpulse_7; }
	inline float* get_address_of_tangentImpulse_7() { return &___tangentImpulse_7; }
	inline void set_tangentImpulse_7(float value)
	{
		___tangentImpulse_7 = value;
	}

	inline static int32_t get_offset_of_bitangentImpulse_8() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bitangentImpulse_8)); }
	inline float get_bitangentImpulse_8() const { return ___bitangentImpulse_8; }
	inline float* get_address_of_bitangentImpulse_8() { return &___bitangentImpulse_8; }
	inline void set_bitangentImpulse_8(float value)
	{
		___bitangentImpulse_8 = value;
	}

	inline static int32_t get_offset_of_stickImpulse_9() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___stickImpulse_9)); }
	inline float get_stickImpulse_9() const { return ___stickImpulse_9; }
	inline float* get_address_of_stickImpulse_9() { return &___stickImpulse_9; }
	inline void set_stickImpulse_9(float value)
	{
		___stickImpulse_9 = value;
	}

	inline static int32_t get_offset_of_rollingFrictionImpulse_10() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___rollingFrictionImpulse_10)); }
	inline float get_rollingFrictionImpulse_10() const { return ___rollingFrictionImpulse_10; }
	inline float* get_address_of_rollingFrictionImpulse_10() { return &___rollingFrictionImpulse_10; }
	inline void set_rollingFrictionImpulse_10(float value)
	{
		___rollingFrictionImpulse_10 = value;
	}

	inline static int32_t get_offset_of_bodyA_11() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bodyA_11)); }
	inline int32_t get_bodyA_11() const { return ___bodyA_11; }
	inline int32_t* get_address_of_bodyA_11() { return &___bodyA_11; }
	inline void set_bodyA_11(int32_t value)
	{
		___bodyA_11 = value;
	}

	inline static int32_t get_offset_of_bodyB_12() { return static_cast<int32_t>(offsetof(Contact_tA38E76CD5E9A53DE837B07BBB25CABE68B2AF756, ___bodyB_12)); }
	inline int32_t get_bodyB_12() const { return ___bodyB_12; }
	inline int32_t* get_address_of_bodyB_12() { return &___bodyB_12; }
	inline void set_bodyB_12(int32_t value)
	{
		___bodyB_12 = value;
	}
};


// Oni/GridCell
struct GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70 
{
public:
	// UnityEngine.Vector3 Oni/GridCell::center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___center_0;
	// UnityEngine.Vector3 Oni/GridCell::size
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___size_1;
	// System.Int32 Oni/GridCell::count
	int32_t ___count_2;

public:
	inline static int32_t get_offset_of_center_0() { return static_cast<int32_t>(offsetof(GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70, ___center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_center_0() const { return ___center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_center_0() { return &___center_0; }
	inline void set_center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___center_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70, ___size_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_size_1() const { return ___size_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(GridCell_t909A29E34A16B3EA378BA3BE26180874A91F5D70, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}
};


// Oni/MaterialCombineMode
struct MaterialCombineMode_t6D9889E6299865FDA792A7E3A202B4FCC7E347CD 
{
public:
	// System.Int32 Oni/MaterialCombineMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaterialCombineMode_t6D9889E6299865FDA792A7E3A202B4FCC7E347CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/ProfileMask
struct ProfileMask_tA62CF5C1CE62FE5BA04842425ABD12094B674555 
{
public:
	// System.UInt32 Oni/ProfileMask::value__
	uint32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProfileMask_tA62CF5C1CE62FE5BA04842425ABD12094B674555, ___value___2)); }
	inline uint32_t get_value___2() const { return ___value___2; }
	inline uint32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint32_t value)
	{
		___value___2 = value;
	}
};


// Oni/ShapeType
struct ShapeType_tA2E1C053BF689089A60C921625DA69D7FE71AFE5 
{
public:
	// System.Int32 Oni/ShapeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShapeType_tA2E1C053BF689089A60C921625DA69D7FE71AFE5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.QueryShape/QueryType
struct QueryType_tB7ABA496561EDA432C6A12A5DDB8E54184413CD2 
{
public:
	// System.Int32 Obi.QueryShape/QueryType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryType_tB7ABA496561EDA432C6A12A5DDB8E54184413CD2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.VoxelPathFinder/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E  : public RuntimeObject
{
public:
	// UnityEngine.Vector3Int Obi.VoxelPathFinder/<>c__DisplayClass7_0::end
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___end_0;
	// Obi.VoxelPathFinder Obi.VoxelPathFinder/<>c__DisplayClass7_0::<>4__this
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_end_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E, ___end_0)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_end_0() const { return ___end_0; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_end_0() { return &___end_0; }
	inline void set_end_0(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___end_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E, ___U3CU3E4__this_1)); }
	inline VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// Obi.VoxelPathFinder/TargetVoxel
struct TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 
{
public:
	// UnityEngine.Vector3Int Obi.VoxelPathFinder/TargetVoxel::coordinates
	Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates_0;
	// System.Single Obi.VoxelPathFinder/TargetVoxel::distance
	float ___distance_1;
	// System.Single Obi.VoxelPathFinder/TargetVoxel::heuristic
	float ___heuristic_2;

public:
	inline static int32_t get_offset_of_coordinates_0() { return static_cast<int32_t>(offsetof(TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564, ___coordinates_0)); }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  get_coordinates_0() const { return ___coordinates_0; }
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * get_address_of_coordinates_0() { return &___coordinates_0; }
	inline void set_coordinates_0(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		___coordinates_0 = value;
	}

	inline static int32_t get_offset_of_distance_1() { return static_cast<int32_t>(offsetof(TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564, ___distance_1)); }
	inline float get_distance_1() const { return ___distance_1; }
	inline float* get_address_of_distance_1() { return &___distance_1; }
	inline void set_distance_1(float value)
	{
		___distance_1 = value;
	}

	inline static int32_t get_offset_of_heuristic_2() { return static_cast<int32_t>(offsetof(TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564, ___heuristic_2)); }
	inline float get_heuristic_2() const { return ___heuristic_2; }
	inline float* get_address_of_heuristic_2() { return &___heuristic_2; }
	inline void set_heuristic_2(float value)
	{
		___heuristic_2 = value;
	}
};


// Oni/ConstraintParameters/EvaluationOrder
struct EvaluationOrder_tD7DE4467E3DE5F4E5FFC0117442FCA2E42BB8C59 
{
public:
	// System.Int32 Oni/ConstraintParameters/EvaluationOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EvaluationOrder_tD7DE4467E3DE5F4E5FFC0117442FCA2E42BB8C59, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/SolverParameters/Interpolation
struct Interpolation_tC0EB8CECD4C0862D9C195BC65FC4B2220998E35F 
{
public:
	// System.Int32 Oni/SolverParameters/Interpolation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Interpolation_tC0EB8CECD4C0862D9C195BC65FC4B2220998E35F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Oni/SolverParameters/Mode
struct Mode_t9545552DF2C676020D2CAEFB2B3E7E3C22ABF0EC 
{
public:
	// System.Int32 Oni/SolverParameters/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t9545552DF2C676020D2CAEFB2B3E7E3C22ABF0EC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>
struct Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B 
{
public:
	// Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1/Cell`1::coords
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___coords_0;
	// Unity.Collections.LowLevel.Unsafe.UnsafeList Obi.NativeMultilevelGrid`1/Cell`1::contents
	UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA  ___contents_1;

public:
	inline static int32_t get_offset_of_coords_0() { return static_cast<int32_t>(offsetof(Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B, ___coords_0)); }
	inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  get_coords_0() const { return ___coords_0; }
	inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * get_address_of_coords_0() { return &___coords_0; }
	inline void set_coords_0(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  value)
	{
		___coords_0 = value;
	}

	inline static int32_t get_offset_of_contents_1() { return static_cast<int32_t>(offsetof(Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B, ___contents_1)); }
	inline UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA  get_contents_1() const { return ___contents_1; }
	inline UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA * get_address_of_contents_1() { return &___contents_1; }
	inline void set_contents_1(UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA  value)
	{
		___contents_1 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.BurstAabb>
struct NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Obi.BurstCollisionMaterial>
struct NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Int32>
struct NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Single>
struct NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Unity.Mathematics.float4>
struct NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Unity.Mathematics.int4>
struct NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>
struct NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeHashMap`2<System.Int32,System.Int32>
struct NativeHashMap_2_tA2C896278E1577EA31B2632511A058B9731235C2 
{
public:
	// Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2<TKey,TValue> Unity.Collections.NativeHashMap`2::m_HashMapData
	UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33  ___m_HashMapData_0;

public:
	inline static int32_t get_offset_of_m_HashMapData_0() { return static_cast<int32_t>(offsetof(NativeHashMap_2_tA2C896278E1577EA31B2632511A058B9731235C2, ___m_HashMapData_0)); }
	inline UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33  get_m_HashMapData_0() const { return ___m_HashMapData_0; }
	inline UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33 * get_address_of_m_HashMapData_0() { return &___m_HashMapData_0; }
	inline void set_m_HashMapData_0(UnsafeHashMap_2_tEDA27A37C890E43DCEF57D0ED98675E874D3FC33  value)
	{
		___m_HashMapData_0 = value;
	}
};


// Unity.Collections.NativeHashMap`2<Unity.Mathematics.int4,System.Int32>
struct NativeHashMap_2_tB57B3606BA2B8BB27FDA49AE73BF80FA3827D9BB 
{
public:
	// Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2<TKey,TValue> Unity.Collections.NativeHashMap`2::m_HashMapData
	UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83  ___m_HashMapData_0;

public:
	inline static int32_t get_offset_of_m_HashMapData_0() { return static_cast<int32_t>(offsetof(NativeHashMap_2_tB57B3606BA2B8BB27FDA49AE73BF80FA3827D9BB, ___m_HashMapData_0)); }
	inline UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83  get_m_HashMapData_0() const { return ___m_HashMapData_0; }
	inline UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83 * get_address_of_m_HashMapData_0() { return &___m_HashMapData_0; }
	inline void set_m_HashMapData_0(UnsafeHashMap_2_t078A9DDDD2BD062433F7294D2B542E558531EB83  value)
	{
		___m_HashMapData_0 = value;
	}
};


// Obi.BurstAffineTransform
struct BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6 
{
public:
	// Unity.Mathematics.float4 Obi.BurstAffineTransform::translation
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___translation_0;
	// Unity.Mathematics.float4 Obi.BurstAffineTransform::scale
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___scale_1;
	// Unity.Mathematics.quaternion Obi.BurstAffineTransform::rotation
	quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  ___rotation_2;

public:
	inline static int32_t get_offset_of_translation_0() { return static_cast<int32_t>(offsetof(BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6, ___translation_0)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_translation_0() const { return ___translation_0; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_translation_0() { return &___translation_0; }
	inline void set_translation_0(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___translation_0 = value;
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6, ___scale_1)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_scale_1() const { return ___scale_1; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6, ___rotation_2)); }
	inline quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  get_rotation_2() const { return ___rotation_2; }
	inline quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F * get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(quaternion_t190AE5AEC157A886A37FED9C3BAFFB0DDDBCB39F  value)
	{
		___rotation_2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// Obi.Triangle
struct Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 
{
public:
	// System.Int32 Obi.Triangle::i1
	int32_t ___i1_0;
	// System.Int32 Obi.Triangle::i2
	int32_t ___i2_1;
	// System.Int32 Obi.Triangle::i3
	int32_t ___i3_2;
	// Obi.Aabb Obi.Triangle::b
	Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  ___b_3;

public:
	inline static int32_t get_offset_of_i1_0() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i1_0)); }
	inline int32_t get_i1_0() const { return ___i1_0; }
	inline int32_t* get_address_of_i1_0() { return &___i1_0; }
	inline void set_i1_0(int32_t value)
	{
		___i1_0 = value;
	}

	inline static int32_t get_offset_of_i2_1() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i2_1)); }
	inline int32_t get_i2_1() const { return ___i2_1; }
	inline int32_t* get_address_of_i2_1() { return &___i2_1; }
	inline void set_i2_1(int32_t value)
	{
		___i2_1 = value;
	}

	inline static int32_t get_offset_of_i3_2() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___i3_2)); }
	inline int32_t get_i3_2() const { return ___i3_2; }
	inline int32_t* get_address_of_i3_2() { return &___i3_2; }
	inline void set_i3_2(int32_t value)
	{
		___i3_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8, ___b_3)); }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  get_b_3() const { return ___b_3; }
	inline Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF * get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(Aabb_t4DF77A399A9E4CB5D52707E4B8B53D3483CD84CF  value)
	{
		___b_3 = value;
	}
};


// Obi.ObiTriangleSkinMap/SlaveVertex
struct SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB  : public RuntimeObject
{
public:
	// System.Int32 Obi.ObiTriangleSkinMap/SlaveVertex::slaveIndex
	int32_t ___slaveIndex_0;
	// System.Int32 Obi.ObiTriangleSkinMap/SlaveVertex::masterTriangleIndex
	int32_t ___masterTriangleIndex_1;
	// Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/SlaveVertex::position
	BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___position_2;
	// Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/SlaveVertex::normal
	BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___normal_3;
	// Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/SlaveVertex::tangent
	BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___tangent_4;

public:
	inline static int32_t get_offset_of_slaveIndex_0() { return static_cast<int32_t>(offsetof(SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB, ___slaveIndex_0)); }
	inline int32_t get_slaveIndex_0() const { return ___slaveIndex_0; }
	inline int32_t* get_address_of_slaveIndex_0() { return &___slaveIndex_0; }
	inline void set_slaveIndex_0(int32_t value)
	{
		___slaveIndex_0 = value;
	}

	inline static int32_t get_offset_of_masterTriangleIndex_1() { return static_cast<int32_t>(offsetof(SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB, ___masterTriangleIndex_1)); }
	inline int32_t get_masterTriangleIndex_1() const { return ___masterTriangleIndex_1; }
	inline int32_t* get_address_of_masterTriangleIndex_1() { return &___masterTriangleIndex_1; }
	inline void set_masterTriangleIndex_1(int32_t value)
	{
		___masterTriangleIndex_1 = value;
	}

	inline static int32_t get_offset_of_position_2() { return static_cast<int32_t>(offsetof(SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB, ___position_2)); }
	inline BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  get_position_2() const { return ___position_2; }
	inline BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 * get_address_of_position_2() { return &___position_2; }
	inline void set_position_2(BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  value)
	{
		___position_2 = value;
	}

	inline static int32_t get_offset_of_normal_3() { return static_cast<int32_t>(offsetof(SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB, ___normal_3)); }
	inline BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  get_normal_3() const { return ___normal_3; }
	inline BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 * get_address_of_normal_3() { return &___normal_3; }
	inline void set_normal_3(BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  value)
	{
		___normal_3 = value;
	}

	inline static int32_t get_offset_of_tangent_4() { return static_cast<int32_t>(offsetof(SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB, ___tangent_4)); }
	inline BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  get_tangent_4() const { return ___tangent_4; }
	inline BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 * get_address_of_tangent_4() { return &___tangent_4; }
	inline void set_tangent_4(BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  value)
	{
		___tangent_4 = value;
	}
};


// Oni/ConstraintParameters
struct ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 
{
public:
	// Oni/ConstraintParameters/EvaluationOrder Oni/ConstraintParameters::evaluationOrder
	int32_t ___evaluationOrder_0;
	// System.Int32 Oni/ConstraintParameters::iterations
	int32_t ___iterations_1;
	// System.Single Oni/ConstraintParameters::SORFactor
	float ___SORFactor_2;
	// System.Boolean Oni/ConstraintParameters::enabled
	bool ___enabled_3;

public:
	inline static int32_t get_offset_of_evaluationOrder_0() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___evaluationOrder_0)); }
	inline int32_t get_evaluationOrder_0() const { return ___evaluationOrder_0; }
	inline int32_t* get_address_of_evaluationOrder_0() { return &___evaluationOrder_0; }
	inline void set_evaluationOrder_0(int32_t value)
	{
		___evaluationOrder_0 = value;
	}

	inline static int32_t get_offset_of_iterations_1() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___iterations_1)); }
	inline int32_t get_iterations_1() const { return ___iterations_1; }
	inline int32_t* get_address_of_iterations_1() { return &___iterations_1; }
	inline void set_iterations_1(int32_t value)
	{
		___iterations_1 = value;
	}

	inline static int32_t get_offset_of_SORFactor_2() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___SORFactor_2)); }
	inline float get_SORFactor_2() const { return ___SORFactor_2; }
	inline float* get_address_of_SORFactor_2() { return &___SORFactor_2; }
	inline void set_SORFactor_2(float value)
	{
		___SORFactor_2 = value;
	}

	inline static int32_t get_offset_of_enabled_3() { return static_cast<int32_t>(offsetof(ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22, ___enabled_3)); }
	inline bool get_enabled_3() const { return ___enabled_3; }
	inline bool* get_address_of_enabled_3() { return &___enabled_3; }
	inline void set_enabled_3(bool value)
	{
		___enabled_3 = value;
	}
};


// Oni/SolverParameters
struct SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 
{
public:
	// Oni/SolverParameters/Mode Oni/SolverParameters::mode
	int32_t ___mode_0;
	// Oni/SolverParameters/Interpolation Oni/SolverParameters::interpolation
	int32_t ___interpolation_1;
	// UnityEngine.Vector3 Oni/SolverParameters::gravity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___gravity_2;
	// System.Single Oni/SolverParameters::damping
	float ___damping_3;
	// System.Single Oni/SolverParameters::maxAnisotropy
	float ___maxAnisotropy_4;
	// System.Single Oni/SolverParameters::sleepThreshold
	float ___sleepThreshold_5;
	// System.Single Oni/SolverParameters::collisionMargin
	float ___collisionMargin_6;
	// System.Single Oni/SolverParameters::maxDepenetration
	float ___maxDepenetration_7;
	// System.Single Oni/SolverParameters::continuousCollisionDetection
	float ___continuousCollisionDetection_8;
	// System.Single Oni/SolverParameters::shockPropagation
	float ___shockPropagation_9;
	// System.Int32 Oni/SolverParameters::surfaceCollisionIterations
	int32_t ___surfaceCollisionIterations_10;
	// System.Single Oni/SolverParameters::surfaceCollisionTolerance
	float ___surfaceCollisionTolerance_11;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_interpolation_1() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___interpolation_1)); }
	inline int32_t get_interpolation_1() const { return ___interpolation_1; }
	inline int32_t* get_address_of_interpolation_1() { return &___interpolation_1; }
	inline void set_interpolation_1(int32_t value)
	{
		___interpolation_1 = value;
	}

	inline static int32_t get_offset_of_gravity_2() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___gravity_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_gravity_2() const { return ___gravity_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_gravity_2() { return &___gravity_2; }
	inline void set_gravity_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___gravity_2 = value;
	}

	inline static int32_t get_offset_of_damping_3() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___damping_3)); }
	inline float get_damping_3() const { return ___damping_3; }
	inline float* get_address_of_damping_3() { return &___damping_3; }
	inline void set_damping_3(float value)
	{
		___damping_3 = value;
	}

	inline static int32_t get_offset_of_maxAnisotropy_4() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___maxAnisotropy_4)); }
	inline float get_maxAnisotropy_4() const { return ___maxAnisotropy_4; }
	inline float* get_address_of_maxAnisotropy_4() { return &___maxAnisotropy_4; }
	inline void set_maxAnisotropy_4(float value)
	{
		___maxAnisotropy_4 = value;
	}

	inline static int32_t get_offset_of_sleepThreshold_5() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___sleepThreshold_5)); }
	inline float get_sleepThreshold_5() const { return ___sleepThreshold_5; }
	inline float* get_address_of_sleepThreshold_5() { return &___sleepThreshold_5; }
	inline void set_sleepThreshold_5(float value)
	{
		___sleepThreshold_5 = value;
	}

	inline static int32_t get_offset_of_collisionMargin_6() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___collisionMargin_6)); }
	inline float get_collisionMargin_6() const { return ___collisionMargin_6; }
	inline float* get_address_of_collisionMargin_6() { return &___collisionMargin_6; }
	inline void set_collisionMargin_6(float value)
	{
		___collisionMargin_6 = value;
	}

	inline static int32_t get_offset_of_maxDepenetration_7() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___maxDepenetration_7)); }
	inline float get_maxDepenetration_7() const { return ___maxDepenetration_7; }
	inline float* get_address_of_maxDepenetration_7() { return &___maxDepenetration_7; }
	inline void set_maxDepenetration_7(float value)
	{
		___maxDepenetration_7 = value;
	}

	inline static int32_t get_offset_of_continuousCollisionDetection_8() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___continuousCollisionDetection_8)); }
	inline float get_continuousCollisionDetection_8() const { return ___continuousCollisionDetection_8; }
	inline float* get_address_of_continuousCollisionDetection_8() { return &___continuousCollisionDetection_8; }
	inline void set_continuousCollisionDetection_8(float value)
	{
		___continuousCollisionDetection_8 = value;
	}

	inline static int32_t get_offset_of_shockPropagation_9() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___shockPropagation_9)); }
	inline float get_shockPropagation_9() const { return ___shockPropagation_9; }
	inline float* get_address_of_shockPropagation_9() { return &___shockPropagation_9; }
	inline void set_shockPropagation_9(float value)
	{
		___shockPropagation_9 = value;
	}

	inline static int32_t get_offset_of_surfaceCollisionIterations_10() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___surfaceCollisionIterations_10)); }
	inline int32_t get_surfaceCollisionIterations_10() const { return ___surfaceCollisionIterations_10; }
	inline int32_t* get_address_of_surfaceCollisionIterations_10() { return &___surfaceCollisionIterations_10; }
	inline void set_surfaceCollisionIterations_10(int32_t value)
	{
		___surfaceCollisionIterations_10 = value;
	}

	inline static int32_t get_offset_of_surfaceCollisionTolerance_11() { return static_cast<int32_t>(offsetof(SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6, ___surfaceCollisionTolerance_11)); }
	inline float get_surfaceCollisionTolerance_11() const { return ___surfaceCollisionTolerance_11; }
	inline float* get_address_of_surfaceCollisionTolerance_11() { return &___surfaceCollisionTolerance_11; }
	inline void set_surfaceCollisionTolerance_11(float value)
	{
		___surfaceCollisionTolerance_11 = value;
	}
};


// Obi.NativeMultilevelGrid`1<System.Int32>
struct NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 
{
public:
	// Unity.Collections.NativeHashMap`2<Unity.Mathematics.int4,System.Int32> Obi.NativeMultilevelGrid`1::grid
	NativeHashMap_2_tB57B3606BA2B8BB27FDA49AE73BF80FA3827D9BB  ___grid_1;
	// Unity.Collections.NativeList`1<Obi.NativeMultilevelGrid`1/Cell`1<T,T>> Obi.NativeMultilevelGrid`1::usedCells
	NativeList_1_t37C384457963A144915FA46915001309FA309A59  ___usedCells_2;
	// Unity.Collections.NativeHashMap`2<System.Int32,System.Int32> Obi.NativeMultilevelGrid`1::populatedLevels
	NativeHashMap_2_tA2C896278E1577EA31B2632511A058B9731235C2  ___populatedLevels_3;

public:
	inline static int32_t get_offset_of_grid_1() { return static_cast<int32_t>(offsetof(NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7, ___grid_1)); }
	inline NativeHashMap_2_tB57B3606BA2B8BB27FDA49AE73BF80FA3827D9BB  get_grid_1() const { return ___grid_1; }
	inline NativeHashMap_2_tB57B3606BA2B8BB27FDA49AE73BF80FA3827D9BB * get_address_of_grid_1() { return &___grid_1; }
	inline void set_grid_1(NativeHashMap_2_tB57B3606BA2B8BB27FDA49AE73BF80FA3827D9BB  value)
	{
		___grid_1 = value;
	}

	inline static int32_t get_offset_of_usedCells_2() { return static_cast<int32_t>(offsetof(NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7, ___usedCells_2)); }
	inline NativeList_1_t37C384457963A144915FA46915001309FA309A59  get_usedCells_2() const { return ___usedCells_2; }
	inline NativeList_1_t37C384457963A144915FA46915001309FA309A59 * get_address_of_usedCells_2() { return &___usedCells_2; }
	inline void set_usedCells_2(NativeList_1_t37C384457963A144915FA46915001309FA309A59  value)
	{
		___usedCells_2 = value;
	}

	inline static int32_t get_offset_of_populatedLevels_3() { return static_cast<int32_t>(offsetof(NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7, ___populatedLevels_3)); }
	inline NativeHashMap_2_tA2C896278E1577EA31B2632511A058B9731235C2  get_populatedLevels_3() const { return ___populatedLevels_3; }
	inline NativeHashMap_2_tA2C896278E1577EA31B2632511A058B9731235C2 * get_address_of_populatedLevels_3() { return &___populatedLevels_3; }
	inline void set_populatedLevels_3(NativeHashMap_2_tA2C896278E1577EA31B2632511A058B9731235C2  value)
	{
		___populatedLevels_3 = value;
	}
};


// Obi.BurstInertialFrame
struct BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551 
{
public:
	// Obi.BurstAffineTransform Obi.BurstInertialFrame::frame
	BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6  ___frame_0;
	// Obi.BurstAffineTransform Obi.BurstInertialFrame::prevFrame
	BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6  ___prevFrame_1;
	// Unity.Mathematics.float4 Obi.BurstInertialFrame::velocity
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___velocity_2;
	// Unity.Mathematics.float4 Obi.BurstInertialFrame::angularVelocity
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___angularVelocity_3;
	// Unity.Mathematics.float4 Obi.BurstInertialFrame::acceleration
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___acceleration_4;
	// Unity.Mathematics.float4 Obi.BurstInertialFrame::angularAcceleration
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___angularAcceleration_5;

public:
	inline static int32_t get_offset_of_frame_0() { return static_cast<int32_t>(offsetof(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551, ___frame_0)); }
	inline BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6  get_frame_0() const { return ___frame_0; }
	inline BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6 * get_address_of_frame_0() { return &___frame_0; }
	inline void set_frame_0(BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6  value)
	{
		___frame_0 = value;
	}

	inline static int32_t get_offset_of_prevFrame_1() { return static_cast<int32_t>(offsetof(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551, ___prevFrame_1)); }
	inline BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6  get_prevFrame_1() const { return ___prevFrame_1; }
	inline BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6 * get_address_of_prevFrame_1() { return &___prevFrame_1; }
	inline void set_prevFrame_1(BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6  value)
	{
		___prevFrame_1 = value;
	}

	inline static int32_t get_offset_of_velocity_2() { return static_cast<int32_t>(offsetof(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551, ___velocity_2)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_velocity_2() const { return ___velocity_2; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_velocity_2() { return &___velocity_2; }
	inline void set_velocity_2(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___velocity_2 = value;
	}

	inline static int32_t get_offset_of_angularVelocity_3() { return static_cast<int32_t>(offsetof(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551, ___angularVelocity_3)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_angularVelocity_3() const { return ___angularVelocity_3; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_angularVelocity_3() { return &___angularVelocity_3; }
	inline void set_angularVelocity_3(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___angularVelocity_3 = value;
	}

	inline static int32_t get_offset_of_acceleration_4() { return static_cast<int32_t>(offsetof(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551, ___acceleration_4)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_acceleration_4() const { return ___acceleration_4; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_acceleration_4() { return &___acceleration_4; }
	inline void set_acceleration_4(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___acceleration_4 = value;
	}

	inline static int32_t get_offset_of_angularAcceleration_5() { return static_cast<int32_t>(offsetof(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551, ___angularAcceleration_5)); }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  get_angularAcceleration_5() const { return ___angularAcceleration_5; }
	inline float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * get_address_of_angularAcceleration_5() { return &___angularAcceleration_5; }
	inline void set_angularAcceleration_5(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  value)
	{
		___angularAcceleration_5 = value;
	}
};


// Obi.BurstSimplex
struct BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D 
{
public:
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.BurstSimplex::positions
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_0;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.BurstSimplex::radii
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___radii_1;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.BurstSimplex::simplices
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___simplices_2;
	// System.Int32 Obi.BurstSimplex::simplexStart
	int32_t ___simplexStart_3;
	// System.Int32 Obi.BurstSimplex::simplexSize
	int32_t ___simplexSize_4;
	// Obi.BurstMath/CachedTri Obi.BurstSimplex::tri
	CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B  ___tri_5;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D, ___positions_0)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_positions_0() const { return ___positions_0; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___positions_0 = value;
	}

	inline static int32_t get_offset_of_radii_1() { return static_cast<int32_t>(offsetof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D, ___radii_1)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_radii_1() const { return ___radii_1; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_radii_1() { return &___radii_1; }
	inline void set_radii_1(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___radii_1 = value;
	}

	inline static int32_t get_offset_of_simplices_2() { return static_cast<int32_t>(offsetof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D, ___simplices_2)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_simplices_2() const { return ___simplices_2; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_simplices_2() { return &___simplices_2; }
	inline void set_simplices_2(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___simplices_2 = value;
	}

	inline static int32_t get_offset_of_simplexStart_3() { return static_cast<int32_t>(offsetof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D, ___simplexStart_3)); }
	inline int32_t get_simplexStart_3() const { return ___simplexStart_3; }
	inline int32_t* get_address_of_simplexStart_3() { return &___simplexStart_3; }
	inline void set_simplexStart_3(int32_t value)
	{
		___simplexStart_3 = value;
	}

	inline static int32_t get_offset_of_simplexSize_4() { return static_cast<int32_t>(offsetof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D, ___simplexSize_4)); }
	inline int32_t get_simplexSize_4() const { return ___simplexSize_4; }
	inline int32_t* get_address_of_simplexSize_4() { return &___simplexSize_4; }
	inline void set_simplexSize_4(int32_t value)
	{
		___simplexSize_4 = value;
	}

	inline static int32_t get_offset_of_tri_5() { return static_cast<int32_t>(offsetof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D, ___tri_5)); }
	inline CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B  get_tri_5() const { return ___tri_5; }
	inline CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B * get_address_of_tri_5() { return &___tri_5; }
	inline void set_tri_5(CachedTri_t1B4A23C3EDC979B08FD44477A2A8F4F6B0B3F95B  value)
	{
		___tri_5 = value;
	}
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// Obi.ObiActorBlueprint
struct ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// Obi.ObiActorBlueprint/BlueprintCallback Obi.ObiActorBlueprint::OnBlueprintGenerate
	BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 * ___OnBlueprintGenerate_4;
	// System.Boolean Obi.ObiActorBlueprint::m_Empty
	bool ___m_Empty_5;
	// System.Int32 Obi.ObiActorBlueprint::m_ActiveParticleCount
	int32_t ___m_ActiveParticleCount_6;
	// System.Int32 Obi.ObiActorBlueprint::m_InitialActiveParticleCount
	int32_t ___m_InitialActiveParticleCount_7;
	// UnityEngine.Bounds Obi.ObiActorBlueprint::_bounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ____bounds_8;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::positions
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___positions_9;
	// UnityEngine.Vector4[] Obi.ObiActorBlueprint::restPositions
	Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* ___restPositions_10;
	// UnityEngine.Quaternion[] Obi.ObiActorBlueprint::orientations
	QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* ___orientations_11;
	// UnityEngine.Quaternion[] Obi.ObiActorBlueprint::restOrientations
	QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* ___restOrientations_12;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::velocities
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___velocities_13;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::angularVelocities
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___angularVelocities_14;
	// System.Single[] Obi.ObiActorBlueprint::invMasses
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___invMasses_15;
	// System.Single[] Obi.ObiActorBlueprint::invRotationalMasses
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___invRotationalMasses_16;
	// System.Int32[] Obi.ObiActorBlueprint::filters
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___filters_17;
	// UnityEngine.Vector3[] Obi.ObiActorBlueprint::principalRadii
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___principalRadii_18;
	// UnityEngine.Color[] Obi.ObiActorBlueprint::colors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___colors_19;
	// System.Int32[] Obi.ObiActorBlueprint::points
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___points_20;
	// System.Int32[] Obi.ObiActorBlueprint::edges
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___edges_21;
	// System.Int32[] Obi.ObiActorBlueprint::triangles
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___triangles_22;
	// Obi.ObiDistanceConstraintsData Obi.ObiActorBlueprint::distanceConstraintsData
	ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * ___distanceConstraintsData_23;
	// Obi.ObiBendConstraintsData Obi.ObiActorBlueprint::bendConstraintsData
	ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * ___bendConstraintsData_24;
	// Obi.ObiSkinConstraintsData Obi.ObiActorBlueprint::skinConstraintsData
	ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 * ___skinConstraintsData_25;
	// Obi.ObiTetherConstraintsData Obi.ObiActorBlueprint::tetherConstraintsData
	ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE * ___tetherConstraintsData_26;
	// Obi.ObiStretchShearConstraintsData Obi.ObiActorBlueprint::stretchShearConstraintsData
	ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * ___stretchShearConstraintsData_27;
	// Obi.ObiBendTwistConstraintsData Obi.ObiActorBlueprint::bendTwistConstraintsData
	ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE * ___bendTwistConstraintsData_28;
	// Obi.ObiShapeMatchingConstraintsData Obi.ObiActorBlueprint::shapeMatchingConstraintsData
	ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 * ___shapeMatchingConstraintsData_29;
	// Obi.ObiAerodynamicConstraintsData Obi.ObiActorBlueprint::aerodynamicConstraintsData
	ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 * ___aerodynamicConstraintsData_30;
	// Obi.ObiChainConstraintsData Obi.ObiActorBlueprint::chainConstraintsData
	ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * ___chainConstraintsData_31;
	// Obi.ObiVolumeConstraintsData Obi.ObiActorBlueprint::volumeConstraintsData
	ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 * ___volumeConstraintsData_32;
	// System.Collections.Generic.List`1<Obi.ObiParticleGroup> Obi.ObiActorBlueprint::groups
	List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * ___groups_33;

public:
	inline static int32_t get_offset_of_OnBlueprintGenerate_4() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___OnBlueprintGenerate_4)); }
	inline BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 * get_OnBlueprintGenerate_4() const { return ___OnBlueprintGenerate_4; }
	inline BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 ** get_address_of_OnBlueprintGenerate_4() { return &___OnBlueprintGenerate_4; }
	inline void set_OnBlueprintGenerate_4(BlueprintCallback_t6040FFC664749D28E3A88E72308B277F69B9D3A6 * value)
	{
		___OnBlueprintGenerate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnBlueprintGenerate_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Empty_5() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___m_Empty_5)); }
	inline bool get_m_Empty_5() const { return ___m_Empty_5; }
	inline bool* get_address_of_m_Empty_5() { return &___m_Empty_5; }
	inline void set_m_Empty_5(bool value)
	{
		___m_Empty_5 = value;
	}

	inline static int32_t get_offset_of_m_ActiveParticleCount_6() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___m_ActiveParticleCount_6)); }
	inline int32_t get_m_ActiveParticleCount_6() const { return ___m_ActiveParticleCount_6; }
	inline int32_t* get_address_of_m_ActiveParticleCount_6() { return &___m_ActiveParticleCount_6; }
	inline void set_m_ActiveParticleCount_6(int32_t value)
	{
		___m_ActiveParticleCount_6 = value;
	}

	inline static int32_t get_offset_of_m_InitialActiveParticleCount_7() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___m_InitialActiveParticleCount_7)); }
	inline int32_t get_m_InitialActiveParticleCount_7() const { return ___m_InitialActiveParticleCount_7; }
	inline int32_t* get_address_of_m_InitialActiveParticleCount_7() { return &___m_InitialActiveParticleCount_7; }
	inline void set_m_InitialActiveParticleCount_7(int32_t value)
	{
		___m_InitialActiveParticleCount_7 = value;
	}

	inline static int32_t get_offset_of__bounds_8() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ____bounds_8)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get__bounds_8() const { return ____bounds_8; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of__bounds_8() { return &____bounds_8; }
	inline void set__bounds_8(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		____bounds_8 = value;
	}

	inline static int32_t get_offset_of_positions_9() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___positions_9)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_positions_9() const { return ___positions_9; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_positions_9() { return &___positions_9; }
	inline void set_positions_9(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___positions_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___positions_9), (void*)value);
	}

	inline static int32_t get_offset_of_restPositions_10() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___restPositions_10)); }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* get_restPositions_10() const { return ___restPositions_10; }
	inline Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871** get_address_of_restPositions_10() { return &___restPositions_10; }
	inline void set_restPositions_10(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* value)
	{
		___restPositions_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restPositions_10), (void*)value);
	}

	inline static int32_t get_offset_of_orientations_11() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___orientations_11)); }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* get_orientations_11() const { return ___orientations_11; }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6** get_address_of_orientations_11() { return &___orientations_11; }
	inline void set_orientations_11(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* value)
	{
		___orientations_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___orientations_11), (void*)value);
	}

	inline static int32_t get_offset_of_restOrientations_12() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___restOrientations_12)); }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* get_restOrientations_12() const { return ___restOrientations_12; }
	inline QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6** get_address_of_restOrientations_12() { return &___restOrientations_12; }
	inline void set_restOrientations_12(QuaternionU5BU5D_t584B1CC68E95071898E32F34DB2CC1E4A726FAA6* value)
	{
		___restOrientations_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restOrientations_12), (void*)value);
	}

	inline static int32_t get_offset_of_velocities_13() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___velocities_13)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_velocities_13() const { return ___velocities_13; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_velocities_13() { return &___velocities_13; }
	inline void set_velocities_13(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___velocities_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___velocities_13), (void*)value);
	}

	inline static int32_t get_offset_of_angularVelocities_14() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___angularVelocities_14)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_angularVelocities_14() const { return ___angularVelocities_14; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_angularVelocities_14() { return &___angularVelocities_14; }
	inline void set_angularVelocities_14(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___angularVelocities_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___angularVelocities_14), (void*)value);
	}

	inline static int32_t get_offset_of_invMasses_15() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___invMasses_15)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_invMasses_15() const { return ___invMasses_15; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_invMasses_15() { return &___invMasses_15; }
	inline void set_invMasses_15(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___invMasses_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invMasses_15), (void*)value);
	}

	inline static int32_t get_offset_of_invRotationalMasses_16() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___invRotationalMasses_16)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_invRotationalMasses_16() const { return ___invRotationalMasses_16; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_invRotationalMasses_16() { return &___invRotationalMasses_16; }
	inline void set_invRotationalMasses_16(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___invRotationalMasses_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invRotationalMasses_16), (void*)value);
	}

	inline static int32_t get_offset_of_filters_17() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___filters_17)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_filters_17() const { return ___filters_17; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_filters_17() { return &___filters_17; }
	inline void set_filters_17(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___filters_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filters_17), (void*)value);
	}

	inline static int32_t get_offset_of_principalRadii_18() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___principalRadii_18)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_principalRadii_18() const { return ___principalRadii_18; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_principalRadii_18() { return &___principalRadii_18; }
	inline void set_principalRadii_18(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___principalRadii_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___principalRadii_18), (void*)value);
	}

	inline static int32_t get_offset_of_colors_19() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___colors_19)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_colors_19() const { return ___colors_19; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_colors_19() { return &___colors_19; }
	inline void set_colors_19(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___colors_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_19), (void*)value);
	}

	inline static int32_t get_offset_of_points_20() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___points_20)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_points_20() const { return ___points_20; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_points_20() { return &___points_20; }
	inline void set_points_20(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___points_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___points_20), (void*)value);
	}

	inline static int32_t get_offset_of_edges_21() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___edges_21)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_edges_21() const { return ___edges_21; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_edges_21() { return &___edges_21; }
	inline void set_edges_21(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___edges_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___edges_21), (void*)value);
	}

	inline static int32_t get_offset_of_triangles_22() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___triangles_22)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_triangles_22() const { return ___triangles_22; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_triangles_22() { return &___triangles_22; }
	inline void set_triangles_22(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___triangles_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___triangles_22), (void*)value);
	}

	inline static int32_t get_offset_of_distanceConstraintsData_23() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___distanceConstraintsData_23)); }
	inline ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * get_distanceConstraintsData_23() const { return ___distanceConstraintsData_23; }
	inline ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB ** get_address_of_distanceConstraintsData_23() { return &___distanceConstraintsData_23; }
	inline void set_distanceConstraintsData_23(ObiDistanceConstraintsData_t55365F00525010A50F24AB7B971A02635203C6DB * value)
	{
		___distanceConstraintsData_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distanceConstraintsData_23), (void*)value);
	}

	inline static int32_t get_offset_of_bendConstraintsData_24() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___bendConstraintsData_24)); }
	inline ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * get_bendConstraintsData_24() const { return ___bendConstraintsData_24; }
	inline ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED ** get_address_of_bendConstraintsData_24() { return &___bendConstraintsData_24; }
	inline void set_bendConstraintsData_24(ObiBendConstraintsData_t0228C688C2AC59A8BFC7D2A5AD3A76088B46D0ED * value)
	{
		___bendConstraintsData_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bendConstraintsData_24), (void*)value);
	}

	inline static int32_t get_offset_of_skinConstraintsData_25() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___skinConstraintsData_25)); }
	inline ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 * get_skinConstraintsData_25() const { return ___skinConstraintsData_25; }
	inline ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 ** get_address_of_skinConstraintsData_25() { return &___skinConstraintsData_25; }
	inline void set_skinConstraintsData_25(ObiSkinConstraintsData_t7F610D65CCE6B7699A70337524422428212F7353 * value)
	{
		___skinConstraintsData_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinConstraintsData_25), (void*)value);
	}

	inline static int32_t get_offset_of_tetherConstraintsData_26() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___tetherConstraintsData_26)); }
	inline ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE * get_tetherConstraintsData_26() const { return ___tetherConstraintsData_26; }
	inline ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE ** get_address_of_tetherConstraintsData_26() { return &___tetherConstraintsData_26; }
	inline void set_tetherConstraintsData_26(ObiTetherConstraintsData_t071A4A85316CD0F23DA05223C15E3EE1B28626CE * value)
	{
		___tetherConstraintsData_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tetherConstraintsData_26), (void*)value);
	}

	inline static int32_t get_offset_of_stretchShearConstraintsData_27() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___stretchShearConstraintsData_27)); }
	inline ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * get_stretchShearConstraintsData_27() const { return ___stretchShearConstraintsData_27; }
	inline ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC ** get_address_of_stretchShearConstraintsData_27() { return &___stretchShearConstraintsData_27; }
	inline void set_stretchShearConstraintsData_27(ObiStretchShearConstraintsData_t01A2D558CCC6780B50B8FAEED0C7A9FD743A60BC * value)
	{
		___stretchShearConstraintsData_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stretchShearConstraintsData_27), (void*)value);
	}

	inline static int32_t get_offset_of_bendTwistConstraintsData_28() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___bendTwistConstraintsData_28)); }
	inline ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE * get_bendTwistConstraintsData_28() const { return ___bendTwistConstraintsData_28; }
	inline ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE ** get_address_of_bendTwistConstraintsData_28() { return &___bendTwistConstraintsData_28; }
	inline void set_bendTwistConstraintsData_28(ObiBendTwistConstraintsData_t681D2A3964FDCF4E12E05CED3F7C62E640440CEE * value)
	{
		___bendTwistConstraintsData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bendTwistConstraintsData_28), (void*)value);
	}

	inline static int32_t get_offset_of_shapeMatchingConstraintsData_29() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___shapeMatchingConstraintsData_29)); }
	inline ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 * get_shapeMatchingConstraintsData_29() const { return ___shapeMatchingConstraintsData_29; }
	inline ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 ** get_address_of_shapeMatchingConstraintsData_29() { return &___shapeMatchingConstraintsData_29; }
	inline void set_shapeMatchingConstraintsData_29(ObiShapeMatchingConstraintsData_t5CC282CC712A59B303FB502754F122D4C14801C8 * value)
	{
		___shapeMatchingConstraintsData_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shapeMatchingConstraintsData_29), (void*)value);
	}

	inline static int32_t get_offset_of_aerodynamicConstraintsData_30() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___aerodynamicConstraintsData_30)); }
	inline ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 * get_aerodynamicConstraintsData_30() const { return ___aerodynamicConstraintsData_30; }
	inline ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 ** get_address_of_aerodynamicConstraintsData_30() { return &___aerodynamicConstraintsData_30; }
	inline void set_aerodynamicConstraintsData_30(ObiAerodynamicConstraintsData_t0C7593E41FF1ECD984BD4D9FF8C84C96DDBA5563 * value)
	{
		___aerodynamicConstraintsData_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aerodynamicConstraintsData_30), (void*)value);
	}

	inline static int32_t get_offset_of_chainConstraintsData_31() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___chainConstraintsData_31)); }
	inline ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * get_chainConstraintsData_31() const { return ___chainConstraintsData_31; }
	inline ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 ** get_address_of_chainConstraintsData_31() { return &___chainConstraintsData_31; }
	inline void set_chainConstraintsData_31(ObiChainConstraintsData_t23E8975F7DDDF80307ABFDBF0168B36C23BAC1C0 * value)
	{
		___chainConstraintsData_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chainConstraintsData_31), (void*)value);
	}

	inline static int32_t get_offset_of_volumeConstraintsData_32() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___volumeConstraintsData_32)); }
	inline ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 * get_volumeConstraintsData_32() const { return ___volumeConstraintsData_32; }
	inline ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 ** get_address_of_volumeConstraintsData_32() { return &___volumeConstraintsData_32; }
	inline void set_volumeConstraintsData_32(ObiVolumeConstraintsData_t12899715EEA78BB09F3BAC3CCC6EB012AF0E4D26 * value)
	{
		___volumeConstraintsData_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___volumeConstraintsData_32), (void*)value);
	}

	inline static int32_t get_offset_of_groups_33() { return static_cast<int32_t>(offsetof(ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD, ___groups_33)); }
	inline List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * get_groups_33() const { return ___groups_33; }
	inline List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF ** get_address_of_groups_33() { return &___groups_33; }
	inline void set_groups_33(List_1_t3C124912DBEE82AB6258DD118B27A96D633F42BF * value)
	{
		___groups_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___groups_33), (void*)value);
	}
};


// Obi.ObiTriangleSkinMap
struct ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Boolean Obi.ObiTriangleSkinMap::bound
	bool ___bound_4;
	// System.Single Obi.ObiTriangleSkinMap::barycentricWeight
	float ___barycentricWeight_5;
	// System.Single Obi.ObiTriangleSkinMap::normalAlignmentWeight
	float ___normalAlignmentWeight_6;
	// System.Single Obi.ObiTriangleSkinMap::elevationWeight
	float ___elevationWeight_7;
	// System.UInt32[] Obi.ObiTriangleSkinMap::m_MasterChannels
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___m_MasterChannels_8;
	// System.UInt32[] Obi.ObiTriangleSkinMap::m_SlaveChannels
	UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* ___m_SlaveChannels_9;
	// Obi.ObiTriangleSkinMap/SkinTransform Obi.ObiTriangleSkinMap::m_SlaveTransform
	SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4  ___m_SlaveTransform_10;
	// Obi.ObiClothBlueprintBase Obi.ObiTriangleSkinMap::m_Master
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * ___m_Master_11;
	// UnityEngine.Mesh Obi.ObiTriangleSkinMap::m_Slave
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_Slave_12;
	// System.Collections.Generic.List`1<Obi.ObiTriangleSkinMap/SlaveVertex> Obi.ObiTriangleSkinMap::skinnedVertices
	List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA * ___skinnedVertices_13;

public:
	inline static int32_t get_offset_of_bound_4() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___bound_4)); }
	inline bool get_bound_4() const { return ___bound_4; }
	inline bool* get_address_of_bound_4() { return &___bound_4; }
	inline void set_bound_4(bool value)
	{
		___bound_4 = value;
	}

	inline static int32_t get_offset_of_barycentricWeight_5() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___barycentricWeight_5)); }
	inline float get_barycentricWeight_5() const { return ___barycentricWeight_5; }
	inline float* get_address_of_barycentricWeight_5() { return &___barycentricWeight_5; }
	inline void set_barycentricWeight_5(float value)
	{
		___barycentricWeight_5 = value;
	}

	inline static int32_t get_offset_of_normalAlignmentWeight_6() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___normalAlignmentWeight_6)); }
	inline float get_normalAlignmentWeight_6() const { return ___normalAlignmentWeight_6; }
	inline float* get_address_of_normalAlignmentWeight_6() { return &___normalAlignmentWeight_6; }
	inline void set_normalAlignmentWeight_6(float value)
	{
		___normalAlignmentWeight_6 = value;
	}

	inline static int32_t get_offset_of_elevationWeight_7() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___elevationWeight_7)); }
	inline float get_elevationWeight_7() const { return ___elevationWeight_7; }
	inline float* get_address_of_elevationWeight_7() { return &___elevationWeight_7; }
	inline void set_elevationWeight_7(float value)
	{
		___elevationWeight_7 = value;
	}

	inline static int32_t get_offset_of_m_MasterChannels_8() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___m_MasterChannels_8)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get_m_MasterChannels_8() const { return ___m_MasterChannels_8; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of_m_MasterChannels_8() { return &___m_MasterChannels_8; }
	inline void set_m_MasterChannels_8(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		___m_MasterChannels_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MasterChannels_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_SlaveChannels_9() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___m_SlaveChannels_9)); }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* get_m_SlaveChannels_9() const { return ___m_SlaveChannels_9; }
	inline UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF** get_address_of_m_SlaveChannels_9() { return &___m_SlaveChannels_9; }
	inline void set_m_SlaveChannels_9(UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* value)
	{
		___m_SlaveChannels_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SlaveChannels_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_SlaveTransform_10() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___m_SlaveTransform_10)); }
	inline SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4  get_m_SlaveTransform_10() const { return ___m_SlaveTransform_10; }
	inline SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * get_address_of_m_SlaveTransform_10() { return &___m_SlaveTransform_10; }
	inline void set_m_SlaveTransform_10(SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4  value)
	{
		___m_SlaveTransform_10 = value;
	}

	inline static int32_t get_offset_of_m_Master_11() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___m_Master_11)); }
	inline ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * get_m_Master_11() const { return ___m_Master_11; }
	inline ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD ** get_address_of_m_Master_11() { return &___m_Master_11; }
	inline void set_m_Master_11(ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * value)
	{
		___m_Master_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Master_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Slave_12() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___m_Slave_12)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_Slave_12() const { return ___m_Slave_12; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_Slave_12() { return &___m_Slave_12; }
	inline void set_m_Slave_12(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_Slave_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Slave_12), (void*)value);
	}

	inline static int32_t get_offset_of_skinnedVertices_13() { return static_cast<int32_t>(offsetof(ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F, ___skinnedVertices_13)); }
	inline List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA * get_skinnedVertices_13() const { return ___skinnedVertices_13; }
	inline List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA ** get_address_of_skinnedVertices_13() { return &___skinnedVertices_13; }
	inline void set_skinnedVertices_13(List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA * value)
	{
		___skinnedVertices_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skinnedVertices_13), (void*)value);
	}
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// Obi.ParticleGrid/CalculateCellCoords
struct CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B 
{
public:
	// Unity.Collections.NativeArray`1<Obi.BurstAabb> Obi.ParticleGrid/CalculateCellCoords::simplexBounds
	NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  ___simplexBounds_0;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.int4> Obi.ParticleGrid/CalculateCellCoords::cellCoords
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellCoords_1;
	// System.Boolean Obi.ParticleGrid/CalculateCellCoords::is2D
	bool ___is2D_2;

public:
	inline static int32_t get_offset_of_simplexBounds_0() { return static_cast<int32_t>(offsetof(CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B, ___simplexBounds_0)); }
	inline NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  get_simplexBounds_0() const { return ___simplexBounds_0; }
	inline NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 * get_address_of_simplexBounds_0() { return &___simplexBounds_0; }
	inline void set_simplexBounds_0(NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  value)
	{
		___simplexBounds_0 = value;
	}

	inline static int32_t get_offset_of_cellCoords_1() { return static_cast<int32_t>(offsetof(CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B, ___cellCoords_1)); }
	inline NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  get_cellCoords_1() const { return ___cellCoords_1; }
	inline NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C * get_address_of_cellCoords_1() { return &___cellCoords_1; }
	inline void set_cellCoords_1(NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  value)
	{
		___cellCoords_1 = value;
	}

	inline static int32_t get_offset_of_is2D_2() { return static_cast<int32_t>(offsetof(CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B, ___is2D_2)); }
	inline bool get_is2D_2() const { return ___is2D_2; }
	inline bool* get_address_of_is2D_2() { return &___is2D_2; }
	inline void set_is2D_2(bool value)
	{
		___is2D_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Obi.ParticleGrid/CalculateCellCoords
struct CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_pinvoke
{
	NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  ___simplexBounds_0;
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellCoords_1;
	int32_t ___is2D_2;
};
// Native definition for COM marshalling of Obi.ParticleGrid/CalculateCellCoords
struct CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_com
{
	NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  ___simplexBounds_0;
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellCoords_1;
	int32_t ___is2D_2;
};

// Obi.ObiMeshBasedActorBlueprint
struct ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B  : public ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD
{
public:
	// UnityEngine.Mesh Obi.ObiMeshBasedActorBlueprint::inputMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___inputMesh_34;
	// UnityEngine.Vector3 Obi.ObiMeshBasedActorBlueprint::scale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale_35;
	// UnityEngine.Quaternion Obi.ObiMeshBasedActorBlueprint::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_36;

public:
	inline static int32_t get_offset_of_inputMesh_34() { return static_cast<int32_t>(offsetof(ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B, ___inputMesh_34)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_inputMesh_34() const { return ___inputMesh_34; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_inputMesh_34() { return &___inputMesh_34; }
	inline void set_inputMesh_34(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___inputMesh_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputMesh_34), (void*)value);
	}

	inline static int32_t get_offset_of_scale_35() { return static_cast<int32_t>(offsetof(ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B, ___scale_35)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_scale_35() const { return ___scale_35; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_scale_35() { return &___scale_35; }
	inline void set_scale_35(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___scale_35 = value;
	}

	inline static int32_t get_offset_of_rotation_36() { return static_cast<int32_t>(offsetof(ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B, ___rotation_36)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_36() const { return ___rotation_36; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_36() { return &___rotation_36; }
	inline void set_rotation_36(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_36 = value;
	}
};


// Obi.ParticleGrid/GenerateParticleParticleContactsJob
struct GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 
{
public:
	// Obi.NativeMultilevelGrid`1<System.Int32> Obi.ParticleGrid/GenerateParticleParticleContactsJob::grid
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.ParticleGrid/GenerateParticleParticleContactsJob::gridLevels
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___gridLevels_1;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/GenerateParticleParticleContactsJob::positions
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_2;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion> Obi.ParticleGrid/GenerateParticleParticleContactsJob::orientations
	NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___orientations_3;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/GenerateParticleParticleContactsJob::restPositions
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___restPositions_4;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion> Obi.ParticleGrid/GenerateParticleParticleContactsJob::restOrientations
	NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___restOrientations_5;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/GenerateParticleParticleContactsJob::velocities
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___velocities_6;
	// Unity.Collections.NativeArray`1<System.Single> Obi.ParticleGrid/GenerateParticleParticleContactsJob::invMasses
	NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ___invMasses_7;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/GenerateParticleParticleContactsJob::radii
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___radii_8;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/GenerateParticleParticleContactsJob::normals
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___normals_9;
	// Unity.Collections.NativeArray`1<System.Single> Obi.ParticleGrid/GenerateParticleParticleContactsJob::fluidRadii
	NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ___fluidRadii_10;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.ParticleGrid/GenerateParticleParticleContactsJob::phases
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___phases_11;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.ParticleGrid/GenerateParticleParticleContactsJob::filters
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___filters_12;
	// Unity.Collections.NativeList`1<System.Int32> Obi.ParticleGrid/GenerateParticleParticleContactsJob::simplices
	NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  ___simplices_13;
	// Obi.SimplexCounts Obi.ParticleGrid/GenerateParticleParticleContactsJob::simplexCounts
	SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  ___simplexCounts_14;
	// Unity.Collections.NativeArray`1<Obi.BurstAabb> Obi.ParticleGrid/GenerateParticleParticleContactsJob::simplexBounds
	NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  ___simplexBounds_15;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.ParticleGrid/GenerateParticleParticleContactsJob::particleMaterialIndices
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___particleMaterialIndices_16;
	// Unity.Collections.NativeArray`1<Obi.BurstCollisionMaterial> Obi.ParticleGrid/GenerateParticleParticleContactsJob::collisionMaterials
	NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8  ___collisionMaterials_17;
	// Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact> Obi.ParticleGrid/GenerateParticleParticleContactsJob::contactsQueue
	ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05  ___contactsQueue_18;
	// Unity.Collections.NativeQueue`1/ParallelWriter<Obi.FluidInteraction> Obi.ParticleGrid/GenerateParticleParticleContactsJob::fluidInteractionsQueue
	ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878  ___fluidInteractionsQueue_19;
	// System.Single Obi.ParticleGrid/GenerateParticleParticleContactsJob::dt
	float ___dt_20;
	// System.Single Obi.ParticleGrid/GenerateParticleParticleContactsJob::collisionMargin
	float ___collisionMargin_21;
	// System.Int32 Obi.ParticleGrid/GenerateParticleParticleContactsJob::optimizationIterations
	int32_t ___optimizationIterations_22;
	// System.Single Obi.ParticleGrid/GenerateParticleParticleContactsJob::optimizationTolerance
	float ___optimizationTolerance_23;

public:
	inline static int32_t get_offset_of_grid_0() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___grid_0)); }
	inline NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  get_grid_0() const { return ___grid_0; }
	inline NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * get_address_of_grid_0() { return &___grid_0; }
	inline void set_grid_0(NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  value)
	{
		___grid_0 = value;
	}

	inline static int32_t get_offset_of_gridLevels_1() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___gridLevels_1)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_gridLevels_1() const { return ___gridLevels_1; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_gridLevels_1() { return &___gridLevels_1; }
	inline void set_gridLevels_1(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___gridLevels_1 = value;
	}

	inline static int32_t get_offset_of_positions_2() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___positions_2)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_positions_2() const { return ___positions_2; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_positions_2() { return &___positions_2; }
	inline void set_positions_2(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___positions_2 = value;
	}

	inline static int32_t get_offset_of_orientations_3() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___orientations_3)); }
	inline NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  get_orientations_3() const { return ___orientations_3; }
	inline NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739 * get_address_of_orientations_3() { return &___orientations_3; }
	inline void set_orientations_3(NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  value)
	{
		___orientations_3 = value;
	}

	inline static int32_t get_offset_of_restPositions_4() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___restPositions_4)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_restPositions_4() const { return ___restPositions_4; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_restPositions_4() { return &___restPositions_4; }
	inline void set_restPositions_4(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___restPositions_4 = value;
	}

	inline static int32_t get_offset_of_restOrientations_5() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___restOrientations_5)); }
	inline NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  get_restOrientations_5() const { return ___restOrientations_5; }
	inline NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739 * get_address_of_restOrientations_5() { return &___restOrientations_5; }
	inline void set_restOrientations_5(NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  value)
	{
		___restOrientations_5 = value;
	}

	inline static int32_t get_offset_of_velocities_6() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___velocities_6)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_velocities_6() const { return ___velocities_6; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_velocities_6() { return &___velocities_6; }
	inline void set_velocities_6(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___velocities_6 = value;
	}

	inline static int32_t get_offset_of_invMasses_7() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___invMasses_7)); }
	inline NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  get_invMasses_7() const { return ___invMasses_7; }
	inline NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 * get_address_of_invMasses_7() { return &___invMasses_7; }
	inline void set_invMasses_7(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  value)
	{
		___invMasses_7 = value;
	}

	inline static int32_t get_offset_of_radii_8() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___radii_8)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_radii_8() const { return ___radii_8; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_radii_8() { return &___radii_8; }
	inline void set_radii_8(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___radii_8 = value;
	}

	inline static int32_t get_offset_of_normals_9() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___normals_9)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_normals_9() const { return ___normals_9; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_normals_9() { return &___normals_9; }
	inline void set_normals_9(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___normals_9 = value;
	}

	inline static int32_t get_offset_of_fluidRadii_10() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___fluidRadii_10)); }
	inline NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  get_fluidRadii_10() const { return ___fluidRadii_10; }
	inline NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 * get_address_of_fluidRadii_10() { return &___fluidRadii_10; }
	inline void set_fluidRadii_10(NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  value)
	{
		___fluidRadii_10 = value;
	}

	inline static int32_t get_offset_of_phases_11() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___phases_11)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_phases_11() const { return ___phases_11; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_phases_11() { return &___phases_11; }
	inline void set_phases_11(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___phases_11 = value;
	}

	inline static int32_t get_offset_of_filters_12() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___filters_12)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_filters_12() const { return ___filters_12; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_filters_12() { return &___filters_12; }
	inline void set_filters_12(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___filters_12 = value;
	}

	inline static int32_t get_offset_of_simplices_13() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___simplices_13)); }
	inline NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  get_simplices_13() const { return ___simplices_13; }
	inline NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * get_address_of_simplices_13() { return &___simplices_13; }
	inline void set_simplices_13(NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  value)
	{
		___simplices_13 = value;
	}

	inline static int32_t get_offset_of_simplexCounts_14() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___simplexCounts_14)); }
	inline SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  get_simplexCounts_14() const { return ___simplexCounts_14; }
	inline SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA * get_address_of_simplexCounts_14() { return &___simplexCounts_14; }
	inline void set_simplexCounts_14(SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  value)
	{
		___simplexCounts_14 = value;
	}

	inline static int32_t get_offset_of_simplexBounds_15() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___simplexBounds_15)); }
	inline NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  get_simplexBounds_15() const { return ___simplexBounds_15; }
	inline NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 * get_address_of_simplexBounds_15() { return &___simplexBounds_15; }
	inline void set_simplexBounds_15(NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  value)
	{
		___simplexBounds_15 = value;
	}

	inline static int32_t get_offset_of_particleMaterialIndices_16() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___particleMaterialIndices_16)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_particleMaterialIndices_16() const { return ___particleMaterialIndices_16; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_particleMaterialIndices_16() { return &___particleMaterialIndices_16; }
	inline void set_particleMaterialIndices_16(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___particleMaterialIndices_16 = value;
	}

	inline static int32_t get_offset_of_collisionMaterials_17() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___collisionMaterials_17)); }
	inline NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8  get_collisionMaterials_17() const { return ___collisionMaterials_17; }
	inline NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8 * get_address_of_collisionMaterials_17() { return &___collisionMaterials_17; }
	inline void set_collisionMaterials_17(NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8  value)
	{
		___collisionMaterials_17 = value;
	}

	inline static int32_t get_offset_of_contactsQueue_18() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___contactsQueue_18)); }
	inline ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05  get_contactsQueue_18() const { return ___contactsQueue_18; }
	inline ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 * get_address_of_contactsQueue_18() { return &___contactsQueue_18; }
	inline void set_contactsQueue_18(ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05  value)
	{
		___contactsQueue_18 = value;
	}

	inline static int32_t get_offset_of_fluidInteractionsQueue_19() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___fluidInteractionsQueue_19)); }
	inline ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878  get_fluidInteractionsQueue_19() const { return ___fluidInteractionsQueue_19; }
	inline ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 * get_address_of_fluidInteractionsQueue_19() { return &___fluidInteractionsQueue_19; }
	inline void set_fluidInteractionsQueue_19(ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878  value)
	{
		___fluidInteractionsQueue_19 = value;
	}

	inline static int32_t get_offset_of_dt_20() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___dt_20)); }
	inline float get_dt_20() const { return ___dt_20; }
	inline float* get_address_of_dt_20() { return &___dt_20; }
	inline void set_dt_20(float value)
	{
		___dt_20 = value;
	}

	inline static int32_t get_offset_of_collisionMargin_21() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___collisionMargin_21)); }
	inline float get_collisionMargin_21() const { return ___collisionMargin_21; }
	inline float* get_address_of_collisionMargin_21() { return &___collisionMargin_21; }
	inline void set_collisionMargin_21(float value)
	{
		___collisionMargin_21 = value;
	}

	inline static int32_t get_offset_of_optimizationIterations_22() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___optimizationIterations_22)); }
	inline int32_t get_optimizationIterations_22() const { return ___optimizationIterations_22; }
	inline int32_t* get_address_of_optimizationIterations_22() { return &___optimizationIterations_22; }
	inline void set_optimizationIterations_22(int32_t value)
	{
		___optimizationIterations_22 = value;
	}

	inline static int32_t get_offset_of_optimizationTolerance_23() { return static_cast<int32_t>(offsetof(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5, ___optimizationTolerance_23)); }
	inline float get_optimizationTolerance_23() const { return ___optimizationTolerance_23; }
	inline float* get_address_of_optimizationTolerance_23() { return &___optimizationTolerance_23; }
	inline void set_optimizationTolerance_23(float value)
	{
		___optimizationTolerance_23 = value;
	}
};

// Native definition for P/Invoke marshalling of Obi.ParticleGrid/GenerateParticleParticleContactsJob
struct GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_pinvoke
{
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___gridLevels_1;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_2;
	NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___orientations_3;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___restPositions_4;
	NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___restOrientations_5;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___velocities_6;
	NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ___invMasses_7;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___radii_8;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___normals_9;
	NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ___fluidRadii_10;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___phases_11;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___filters_12;
	NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  ___simplices_13;
	SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  ___simplexCounts_14;
	NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  ___simplexBounds_15;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___particleMaterialIndices_16;
	NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8  ___collisionMaterials_17;
	ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05  ___contactsQueue_18;
	ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878  ___fluidInteractionsQueue_19;
	float ___dt_20;
	float ___collisionMargin_21;
	int32_t ___optimizationIterations_22;
	float ___optimizationTolerance_23;
};
// Native definition for COM marshalling of Obi.ParticleGrid/GenerateParticleParticleContactsJob
struct GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_com
{
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___gridLevels_1;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_2;
	NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___orientations_3;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___restPositions_4;
	NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___restOrientations_5;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___velocities_6;
	NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ___invMasses_7;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___radii_8;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___normals_9;
	NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232  ___fluidRadii_10;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___phases_11;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___filters_12;
	NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  ___simplices_13;
	SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA  ___simplexCounts_14;
	NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8  ___simplexBounds_15;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___particleMaterialIndices_16;
	NativeArray_1_t98196C34C62109D4415FE3803C13E7A86AE6EBA8  ___collisionMaterials_17;
	ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05  ___contactsQueue_18;
	ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878  ___fluidInteractionsQueue_19;
	float ___dt_20;
	float ___collisionMargin_21;
	int32_t ___optimizationIterations_22;
	float ___optimizationTolerance_23;
};

// Obi.ParticleGrid/InterpolateDiffusePropertiesJob
struct InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826 
{
public:
	// Obi.NativeMultilevelGrid`1<System.Int32> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::grid
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.int4> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::cellOffsets
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellOffsets_1;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::positions
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_2;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::properties
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___properties_3;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::diffusePositions
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___diffusePositions_4;
	// Obi.Poly6Kernel Obi.ParticleGrid/InterpolateDiffusePropertiesJob::densityKernel
	Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235  ___densityKernel_5;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.float4> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::diffuseProperties
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___diffuseProperties_6;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::neighbourCount
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___neighbourCount_7;
	// Unity.Collections.NativeArray`1<System.Int32> Obi.ParticleGrid/InterpolateDiffusePropertiesJob::gridLevels
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___gridLevels_8;
	// Obi.BurstInertialFrame Obi.ParticleGrid/InterpolateDiffusePropertiesJob::inertialFrame
	BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551  ___inertialFrame_9;
	// System.Boolean Obi.ParticleGrid/InterpolateDiffusePropertiesJob::mode2D
	bool ___mode2D_10;

public:
	inline static int32_t get_offset_of_grid_0() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___grid_0)); }
	inline NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  get_grid_0() const { return ___grid_0; }
	inline NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * get_address_of_grid_0() { return &___grid_0; }
	inline void set_grid_0(NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  value)
	{
		___grid_0 = value;
	}

	inline static int32_t get_offset_of_cellOffsets_1() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___cellOffsets_1)); }
	inline NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  get_cellOffsets_1() const { return ___cellOffsets_1; }
	inline NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C * get_address_of_cellOffsets_1() { return &___cellOffsets_1; }
	inline void set_cellOffsets_1(NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  value)
	{
		___cellOffsets_1 = value;
	}

	inline static int32_t get_offset_of_positions_2() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___positions_2)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_positions_2() const { return ___positions_2; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_positions_2() { return &___positions_2; }
	inline void set_positions_2(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___positions_2 = value;
	}

	inline static int32_t get_offset_of_properties_3() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___properties_3)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_properties_3() const { return ___properties_3; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_properties_3() { return &___properties_3; }
	inline void set_properties_3(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___properties_3 = value;
	}

	inline static int32_t get_offset_of_diffusePositions_4() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___diffusePositions_4)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_diffusePositions_4() const { return ___diffusePositions_4; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_diffusePositions_4() { return &___diffusePositions_4; }
	inline void set_diffusePositions_4(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___diffusePositions_4 = value;
	}

	inline static int32_t get_offset_of_densityKernel_5() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___densityKernel_5)); }
	inline Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235  get_densityKernel_5() const { return ___densityKernel_5; }
	inline Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235 * get_address_of_densityKernel_5() { return &___densityKernel_5; }
	inline void set_densityKernel_5(Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235  value)
	{
		___densityKernel_5 = value;
	}

	inline static int32_t get_offset_of_diffuseProperties_6() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___diffuseProperties_6)); }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  get_diffuseProperties_6() const { return ___diffuseProperties_6; }
	inline NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * get_address_of_diffuseProperties_6() { return &___diffuseProperties_6; }
	inline void set_diffuseProperties_6(NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  value)
	{
		___diffuseProperties_6 = value;
	}

	inline static int32_t get_offset_of_neighbourCount_7() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___neighbourCount_7)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_neighbourCount_7() const { return ___neighbourCount_7; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_neighbourCount_7() { return &___neighbourCount_7; }
	inline void set_neighbourCount_7(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___neighbourCount_7 = value;
	}

	inline static int32_t get_offset_of_gridLevels_8() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___gridLevels_8)); }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  get_gridLevels_8() const { return ___gridLevels_8; }
	inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * get_address_of_gridLevels_8() { return &___gridLevels_8; }
	inline void set_gridLevels_8(NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  value)
	{
		___gridLevels_8 = value;
	}

	inline static int32_t get_offset_of_inertialFrame_9() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___inertialFrame_9)); }
	inline BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551  get_inertialFrame_9() const { return ___inertialFrame_9; }
	inline BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551 * get_address_of_inertialFrame_9() { return &___inertialFrame_9; }
	inline void set_inertialFrame_9(BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551  value)
	{
		___inertialFrame_9 = value;
	}

	inline static int32_t get_offset_of_mode2D_10() { return static_cast<int32_t>(offsetof(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826, ___mode2D_10)); }
	inline bool get_mode2D_10() const { return ___mode2D_10; }
	inline bool* get_address_of_mode2D_10() { return &___mode2D_10; }
	inline void set_mode2D_10(bool value)
	{
		___mode2D_10 = value;
	}
};

// Native definition for P/Invoke marshalling of Obi.ParticleGrid/InterpolateDiffusePropertiesJob
struct InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_pinvoke
{
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellOffsets_1;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_2;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___properties_3;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___diffusePositions_4;
	Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke ___densityKernel_5;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___diffuseProperties_6;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___neighbourCount_7;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___gridLevels_8;
	BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551  ___inertialFrame_9;
	int32_t ___mode2D_10;
};
// Native definition for COM marshalling of Obi.ParticleGrid/InterpolateDiffusePropertiesJob
struct InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_com
{
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellOffsets_1;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions_2;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___properties_3;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___diffusePositions_4;
	Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com ___densityKernel_5;
	NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___diffuseProperties_6;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___neighbourCount_7;
	NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___gridLevels_8;
	BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551  ___inertialFrame_9;
	int32_t ___mode2D_10;
};

// Obi.ParticleGrid/UpdateGrid
struct UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA 
{
public:
	// Obi.NativeMultilevelGrid`1<System.Int32> Obi.ParticleGrid/UpdateGrid::grid
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	// Unity.Collections.NativeArray`1<Unity.Mathematics.int4> Obi.ParticleGrid/UpdateGrid::cellCoords
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellCoords_1;
	// System.Int32 Obi.ParticleGrid/UpdateGrid::simplexCount
	int32_t ___simplexCount_2;

public:
	inline static int32_t get_offset_of_grid_0() { return static_cast<int32_t>(offsetof(UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA, ___grid_0)); }
	inline NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  get_grid_0() const { return ___grid_0; }
	inline NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * get_address_of_grid_0() { return &___grid_0; }
	inline void set_grid_0(NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  value)
	{
		___grid_0 = value;
	}

	inline static int32_t get_offset_of_cellCoords_1() { return static_cast<int32_t>(offsetof(UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA, ___cellCoords_1)); }
	inline NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  get_cellCoords_1() const { return ___cellCoords_1; }
	inline NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C * get_address_of_cellCoords_1() { return &___cellCoords_1; }
	inline void set_cellCoords_1(NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  value)
	{
		___cellCoords_1 = value;
	}

	inline static int32_t get_offset_of_simplexCount_2() { return static_cast<int32_t>(offsetof(UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA, ___simplexCount_2)); }
	inline int32_t get_simplexCount_2() const { return ___simplexCount_2; }
	inline int32_t* get_address_of_simplexCount_2() { return &___simplexCount_2; }
	inline void set_simplexCount_2(int32_t value)
	{
		___simplexCount_2 = value;
	}
};

// Native definition for P/Invoke marshalling of Obi.ParticleGrid/UpdateGrid
struct UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_pinvoke
{
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellCoords_1;
	int32_t ___simplexCount_2;
};
// Native definition for COM marshalling of Obi.ParticleGrid/UpdateGrid
struct UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_com
{
	NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7  ___grid_0;
	NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C  ___cellCoords_1;
	int32_t ___simplexCount_2;
};

// Obi.ObiClothBlueprintBase
struct ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD  : public ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B
{
public:
	// Obi.HalfEdgeMesh Obi.ObiClothBlueprintBase::topology
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * ___topology_37;
	// System.Int32[] Obi.ObiClothBlueprintBase::deformableTriangles
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___deformableTriangles_38;
	// UnityEngine.Vector3[] Obi.ObiClothBlueprintBase::restNormals
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___restNormals_39;
	// System.Single[] Obi.ObiClothBlueprintBase::areaContribution
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___areaContribution_40;

public:
	inline static int32_t get_offset_of_topology_37() { return static_cast<int32_t>(offsetof(ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD, ___topology_37)); }
	inline HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * get_topology_37() const { return ___topology_37; }
	inline HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 ** get_address_of_topology_37() { return &___topology_37; }
	inline void set_topology_37(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * value)
	{
		___topology_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___topology_37), (void*)value);
	}

	inline static int32_t get_offset_of_deformableTriangles_38() { return static_cast<int32_t>(offsetof(ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD, ___deformableTriangles_38)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_deformableTriangles_38() const { return ___deformableTriangles_38; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_deformableTriangles_38() { return &___deformableTriangles_38; }
	inline void set_deformableTriangles_38(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___deformableTriangles_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___deformableTriangles_38), (void*)value);
	}

	inline static int32_t get_offset_of_restNormals_39() { return static_cast<int32_t>(offsetof(ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD, ___restNormals_39)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_restNormals_39() const { return ___restNormals_39; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_restNormals_39() { return &___restNormals_39; }
	inline void set_restNormals_39(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___restNormals_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___restNormals_39), (void*)value);
	}

	inline static int32_t get_offset_of_areaContribution_40() { return static_cast<int32_t>(offsetof(ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD, ___areaContribution_40)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_areaContribution_40() const { return ___areaContribution_40; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_areaContribution_40() { return &___areaContribution_40; }
	inline void set_areaContribution_40(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___areaContribution_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___areaContribution_40), (void*)value);
	}
};


// Obi.ObiClothBlueprint
struct ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89  : public ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD
{
public:

public:
};


// Obi.ObiTearableClothBlueprint
struct ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB  : public ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89
{
public:
	// System.Single Obi.ObiTearableClothBlueprint::tearCapacity
	float ___tearCapacity_42;
	// System.Int32 Obi.ObiTearableClothBlueprint::pooledParticles
	int32_t ___pooledParticles_43;
	// System.Single[] Obi.ObiTearableClothBlueprint::tearResistance
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___tearResistance_44;
	// UnityEngine.Vector2Int[] Obi.ObiTearableClothBlueprint::distanceConstraintMap
	Vector2IntU5BU5D_tA91A00C258BDBF38BD76CF790B67CB344A126E9E* ___distanceConstraintMap_45;

public:
	inline static int32_t get_offset_of_tearCapacity_42() { return static_cast<int32_t>(offsetof(ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB, ___tearCapacity_42)); }
	inline float get_tearCapacity_42() const { return ___tearCapacity_42; }
	inline float* get_address_of_tearCapacity_42() { return &___tearCapacity_42; }
	inline void set_tearCapacity_42(float value)
	{
		___tearCapacity_42 = value;
	}

	inline static int32_t get_offset_of_pooledParticles_43() { return static_cast<int32_t>(offsetof(ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB, ___pooledParticles_43)); }
	inline int32_t get_pooledParticles_43() const { return ___pooledParticles_43; }
	inline int32_t* get_address_of_pooledParticles_43() { return &___pooledParticles_43; }
	inline void set_pooledParticles_43(int32_t value)
	{
		___pooledParticles_43 = value;
	}

	inline static int32_t get_offset_of_tearResistance_44() { return static_cast<int32_t>(offsetof(ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB, ___tearResistance_44)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_tearResistance_44() const { return ___tearResistance_44; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_tearResistance_44() { return &___tearResistance_44; }
	inline void set_tearResistance_44(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___tearResistance_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tearResistance_44), (void*)value);
	}

	inline static int32_t get_offset_of_distanceConstraintMap_45() { return static_cast<int32_t>(offsetof(ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB, ___distanceConstraintMap_45)); }
	inline Vector2IntU5BU5D_tA91A00C258BDBF38BD76CF790B67CB344A126E9E* get_distanceConstraintMap_45() const { return ___distanceConstraintMap_45; }
	inline Vector2IntU5BU5D_tA91A00C258BDBF38BD76CF790B67CB344A126E9E** get_address_of_distanceConstraintMap_45() { return &___distanceConstraintMap_45; }
	inline void set_distanceConstraintMap_45(Vector2IntU5BU5D_tA91A00C258BDBF38BD76CF790B67CB344A126E9E* value)
	{
		___distanceConstraintMap_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___distanceConstraintMap_45), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  m_Items[1];

public:
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector4[]
struct Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  m_Items[1];

public:
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  m_Items[1];

public:
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		m_Items[index] = value;
	}
};
// Obi.ObiTriangleSkinMap/MasterFace[]
struct MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * m_Items[1];

public:
	inline MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.UInt32[]
struct UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Unity.Mathematics.int3[]
struct int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  m_Items[1];

public:
	inline int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3Int[0...,0...,0...]
struct Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  m_Items[1];

public:
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		m_Items[index] = value;
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
};
// Obi.HalfEdgeMesh/Vertex[]
struct VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  m_Items[1];

public:
	inline Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  value)
	{
		m_Items[index] = value;
	}
};

IL2CPP_EXTERN_C void Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshal_pinvoke(const Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235& unmarshaled, Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshal_pinvoke_back(const Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke& marshaled, Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235& unmarshaled);
IL2CPP_EXTERN_C void Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshal_pinvoke_cleanup(Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshal_com(const Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235& unmarshaled, Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com& marshaled);
IL2CPP_EXTERN_C void Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshal_com_back(const Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com& marshaled, Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235& unmarshaled);
IL2CPP_EXTERN_C void Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshal_com_cleanup(Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235_marshaled_com& marshaled);

// System.Int32 System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_gshared_inline (List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_gshared_inline (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_gshared_inline (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 Obi.NativeMultilevelGrid`1<System.Int32>::GridLevelForSize(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_gshared_inline (float ___size0, const RuntimeMethod* method);
// System.Single Obi.NativeMultilevelGrid`1<System.Int32>::CellSizeOfLevel(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_gshared_inline (int32_t ___level0, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!0> Unity.Collections.NativeList`1<System.Int32>::op_Implicit(Unity.Collections.NativeList`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_gshared (NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  ___nativeList0, const RuntimeMethod* method);
// !0 Unity.Collections.NativeList`1<Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_gshared (NativeList_1_t37C384457963A144915FA46915001309FA309A59 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_gshared (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, const RuntimeMethod* method);
// K Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_gshared (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, int32_t ___index0, const RuntimeMethod* method);
// Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::get_Coords()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_gshared_inline (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, const RuntimeMethod* method);
// System.Boolean Obi.NativeMultilevelGrid`1<System.Int32>::TryGetCellIndex(Unity.Mathematics.int4,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_gshared (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___cellCoords0, int32_t* ___cellIndex1, const RuntimeMethod* method);
// System.Int32 Unity.Collections.NativeArrayExtensions::IndexOf<System.Int32,System.Int32>(Unity.Collections.NativeArray`1<!!0>,!!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610_gshared (NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___array0, int32_t ___value1, const RuntimeMethod* method);
// Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1<System.Int32>::GetParentCellCoords(Unity.Mathematics.int4,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA_gshared (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___cellCoords0, int32_t ___level1, const RuntimeMethod* method);
// !0 Unity.Collections.NativeList`1<System.Int32>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_gshared (NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeQueue`1/ParallelWriter<Obi.FluidInteraction>::Enqueue(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01_gshared (ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 * __this, FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D  ___value0, const RuntimeMethod* method);
// System.Void Obi.ObiUtils::Swap<System.Int32>(T&,T&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_gshared_inline (int32_t* ___lhs0, int32_t* ___rhs1, const RuntimeMethod* method);
// System.Void Obi.ObiUtils::Swap<System.Int32Enum>(T&,T&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ObiUtils_Swap_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_mAB90511ADDFFC04F5B65A8553B95D50B7D75DFA1_gshared_inline (int32_t* ___lhs0, int32_t* ___rhs1, const RuntimeMethod* method);
// Obi.BurstLocalOptimization/SurfacePoint Obi.BurstLocalOptimization::Optimize<Obi.BurstSimplex>(T&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32,System.Int32,Unity.Mathematics.float4&,Unity.Mathematics.float4&,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63_gshared (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___function0, NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions1, NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___orientations2, NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___radii3, NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___simplices4, int32_t ___simplexStart5, int32_t ___simplexSize6, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * ___convexBary7, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * ___convexPoint8, int32_t ___maxIterations9, float ___tolerance10, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>::Enqueue(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108_gshared (ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 * __this, BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C  ___value0, const RuntimeMethod* method);
// System.Void Obi.NativeMultilevelGrid`1<System.Int32>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036_gshared (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * __this, const RuntimeMethod* method);
// System.Int32 Obi.NativeMultilevelGrid`1<System.Int32>::GetOrCreateCell(Unity.Mathematics.int4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8_gshared (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___cellCoords0, const RuntimeMethod* method);
// System.Void Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::Add(K)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9_gshared (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, int32_t ___entity0, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeList`1<Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118_gshared (NativeList_1_t37C384457963A144915FA46915001309FA309A59 * __this, int32_t ___index0, Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  ___value1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Mesh::get_isReadable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Mesh_get_isReadable_m7F09EA53D0B8F385A3D4BE15199B4C2B91A42B1A (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Obi.ObiActorBlueprint::ClearParticleGroups(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18 (ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * __this, bool ___saveImmediately0, const RuntimeMethod* method);
// System.Void Obi.HalfEdgeMesh::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalfEdgeMesh__ctor_m641C71B020EB960E23164DEABD3019B25C6F5404 (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * __this, const RuntimeMethod* method);
// System.Void Obi.HalfEdgeMesh::Generate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HalfEdgeMesh_Generate_mD60A36F035F35EBE9F282926F0C8E06962265CC7 (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>::get_Count()
inline int32_t List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_inline (List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C *, const RuntimeMethod*))List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_gshared_inline)(__this, method);
}
// System.Int32 System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex>::get_Count()
inline int32_t List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_inline (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 *, const RuntimeMethod*))List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Vertex>::get_Item(System.Int32)
inline Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_inline (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  (*) (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 *, int32_t, const RuntimeMethod*))List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_gshared_inline)(__this, ___index0, method);
}
// System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Face> Obi.HalfEdgeMesh::GetNeighbourFacesEnumerator(Obi.HalfEdgeMesh/Vertex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HalfEdgeMesh_GetNeighbourFacesEnumerator_m055B1E4B59B1C20B1C6F542EE88DA13DBE7DF26D (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * __this, Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  ___vertex0, const RuntimeMethod* method);
// System.Single Obi.HalfEdgeMesh::GetFaceArea(Obi.HalfEdgeMesh/Face)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HalfEdgeMesh_GetFaceArea_m27C2ADF35DAB7C31E951DE1EED24C9CA6472FB6E (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * __this, Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2  ___face0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh::GetNeighbourEdgesEnumerator(Obi.HalfEdgeMesh/Vertex)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HalfEdgeMesh_GetNeighbourEdgesEnumerator_m46006FB9A047B16085C6EF70CC20209053622818 (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * __this, Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  ___vertex0, const RuntimeMethod* method);
// System.Int32 Obi.HalfEdgeMesh::GetHalfEdgeStartVertex(Obi.HalfEdgeMesh/HalfEdge)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t HalfEdgeMesh_GetHalfEdgeStartVertex_m4BB747F2E34E9DAA7686C4EBE135F0ACF5CC96EC (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * __this, HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7  ___edge0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14 (float ___a0, float ___b1, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Int32 Obi.ObiUtils::MakeFilter(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline (int32_t ___mask0, int32_t ___category1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E (const RuntimeMethod* method);
// System.Void Obi.CoroutineJob/ProgressInfo::.ctor(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1 (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * __this, String_t* ___userReadableInfo0, float ___progress1, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObiTriangleSkinMap_Clear_mAE6BED8941D24054DD5ACAF5C13431FF25515143 (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, const RuntimeMethod* method);
// Obi.ObiClothBlueprintBase Obi.ObiTriangleSkinMap::get_master()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, const RuntimeMethod* method);
// UnityEngine.Mesh Obi.ObiTriangleSkinMap::get_slave()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37_inline (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* Mesh_get_vertices_mB7A79698792B3CBA0E7E6EACDA6C031E496FB595 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* Mesh_get_tangents_m278A41721D47A627367F3F8E2B722B80A949A0F3 (Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 Obi.ObiTriangleSkinMap/SkinTransform::GetMatrix4X4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_get_inverse_mFA34ECC790B269522F60FC32370D628DAFCAE225 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_transpose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_get_transpose_mE5DC808B366ADAE2406A4A198332B12AEC900F05 (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, const RuntimeMethod* method);
// System.Int32 Obi.ObiActorBlueprint::get_activeParticleCount()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline (ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * __this, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/MasterFace::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MasterFace__ctor_mB4AB2008F0F78D8A27CBCD816423C06862A6510E (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Cross_m63414F0C545EBB616F339FF8830D37F9230736A4 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/MasterFace::CacheBarycentricData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MasterFace_CacheBarycentricData_m05DB432E4FAE84E0A5D863267B948CCDDE4FB120 (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * __this, const RuntimeMethod* method);
// Obi.ObiTriangleSkinMap/SlaveVertex Obi.ObiTriangleSkinMap/SlaveVertex::get_empty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * SlaveVertex_get_empty_m6B4C7246C11E79FAFFF65C981540626F82F4C5E5 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C (Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___vector0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Boolean Obi.ObiTriangleSkinMap::BindToFace(System.Int32,Obi.ObiTriangleSkinMap/MasterFace,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,Obi.ObiTriangleSkinMap/SlaveVertex&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ObiTriangleSkinMap_BindToFace_mC6D3795B2BEA297D2DE57744229DBEEA82E3DEF3 (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, int32_t ___slaveIndex0, MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * ___triangle1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___normalPoint3, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___tangentPoint4, SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB ** ___skinning5, const RuntimeMethod* method);
// System.Single Obi.ObiTriangleSkinMap::GetFaceMappingError(Obi.ObiTriangleSkinMap/MasterFace,Obi.ObiTriangleSkinMap/SlaveVertex,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ObiTriangleSkinMap_GetFaceMappingError_mEE274B631EC04C834C233252FFA299316F01F697 (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * ___triangle0, SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * ___vertex1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___normal2, const RuntimeMethod* method);
// System.Boolean Obi.ObiTriangleSkinMap/SlaveVertex::get_isEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SlaveVertex_get_isEmpty_m1B2229CA5E3375CCEDB4898EEBDB72F0EAF2432F (SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Obi.ObiTriangleSkinMap/SlaveVertex>::Add(!0)
inline void List_1_Add_m208B981A048B9BF9FB11152E3517D21A8D4CD593 (List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA * __this, SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA *, SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9 (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___values0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/BarycentricPoint::.ctor(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F (BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___height1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895 (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80 (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::Apply(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0 (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___pos0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___q1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, const RuntimeMethod* method);
// Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/BarycentricPoint::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97 (const RuntimeMethod* method);
// System.Void Obi.ObiTriangleSkinMap/SlaveVertex::.ctor(System.Int32,System.Int32,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlaveVertex__ctor_mB7C3D7EBC3A235D3D58769095378AE46286BC7A7 (SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * __this, int32_t ___slaveIndex0, int32_t ___masterTriangleIndex1, BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___position2, BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___normal3, BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___tangent4, const RuntimeMethod* method);
// System.Int32 System.Environment::get_CurrentManagedThreadId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D (const RuntimeMethod* method);
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method);
// System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566 (ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * __this, bool ___enabled0, int32_t ___order1, int32_t ___iterations2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method);
// System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3 (SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * __this, int32_t ___interpolation0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___gravity1, const RuntimeMethod* method);
// System.Single Obi.BurstAabb::AverageAxisLength()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float BurstAabb_AverageAxisLength_mC3D10DA8BEC0F0BA5B418EDB620BDCD4A2951C8E (BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 * __this, const RuntimeMethod* method);
// System.Int32 Obi.NativeMultilevelGrid`1<System.Int32>::GridLevelForSize(System.Single)
inline int32_t NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_inline (float ___size0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (float, const RuntimeMethod*))NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_gshared_inline)(___size0, method);
}
// System.Single Obi.NativeMultilevelGrid`1<System.Int32>::CellSizeOfLevel(System.Int32)
inline float NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_inline (int32_t ___level0, const RuntimeMethod* method)
{
	return ((  float (*) (int32_t, const RuntimeMethod*))NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_gshared_inline)(___level0, method);
}
// Unity.Mathematics.float4 Obi.BurstAabb::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  BurstAabb_get_center_mEF7385D666BE5EE77B04D4C1B507CC277498DF83 (BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 * __this, const RuntimeMethod* method);
// Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method);
// Unity.Mathematics.int3 GridHash::Quantize(Unity.Mathematics.float3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  GridHash_Quantize_m67808299EA16142EAEEEAF0E4D4B9B4CADA9E320 (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___v0, float ___cellSize1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.int3,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___xyz0, int32_t ___w1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::set_Item(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5 (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___index0, int32_t ___value1, const RuntimeMethod* method);
// System.Void Obi.ParticleGrid/CalculateCellCoords::Execute(System.Int32)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955 (CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___i0, const RuntimeMethod* method);
// Unity.Collections.NativeArray`1<!0> Unity.Collections.NativeList`1<System.Int32>::op_Implicit(Unity.Collections.NativeList`1<!0>)
inline NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A (NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  ___nativeList0, const RuntimeMethod* method)
{
	return ((  NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  (*) (NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 , const RuntimeMethod*))NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_gshared)(___nativeList0, method);
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::IntraCellSearch(System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape1, const RuntimeMethod* method);
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::IntraLevelSearch(System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape1, const RuntimeMethod* method);
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::Execute(System.Int32)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___i0, const RuntimeMethod* method);
// !0 Unity.Collections.NativeList`1<Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>>::get_Item(System.Int32)
inline Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3 (NativeList_1_t37C384457963A144915FA46915001309FA309A59 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  (*) (NativeList_1_t37C384457963A144915FA46915001309FA309A59 *, int32_t, const RuntimeMethod*))NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_gshared)(__this, ___index0, method);
}
// System.Int32 Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::get_Length()
inline int32_t Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03 (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *, const RuntimeMethod*))Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_gshared)(__this, method);
}
// K Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::get_Item(System.Int32)
inline int32_t Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389 (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *, int32_t, const RuntimeMethod*))Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_gshared)(__this, ___index0, method);
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::InteractionTest(System.Int32,System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___A0, int32_t ___B1, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape2, const RuntimeMethod* method);
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::InterCellSearch(System.Int32,System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, int32_t ___neighborCellIndex1, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape2, const RuntimeMethod* method);
// Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::get_Coords()
inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_inline (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, const RuntimeMethod* method)
{
	return ((  int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  (*) (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *, const RuntimeMethod*))Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_gshared_inline)(__this, method);
}
// Unity.Mathematics.int3 Unity.Mathematics.int4::get_xyz()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method);
// Unity.Mathematics.int3 Unity.Mathematics.int3::op_Addition(Unity.Mathematics.int3,Unity.Mathematics.int3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int3_op_Addition_m08C6B9DB7119923A27914BAFD7DCDEE50CC988A4_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___lhs0, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs1, const RuntimeMethod* method);
// System.Boolean Obi.NativeMultilevelGrid`1<System.Int32>::TryGetCellIndex(Unity.Mathematics.int4,System.Int32&)
inline bool NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___cellCoords0, int32_t* ___cellIndex1, const RuntimeMethod* method)
{
	return ((  bool (*) (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 , int32_t*, const RuntimeMethod*))NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_gshared)(__this, ___cellCoords0, ___cellIndex1, method);
}
// System.Int32 Unity.Collections.NativeArrayExtensions::IndexOf<System.Int32,System.Int32>(Unity.Collections.NativeArray`1<!!0>,!!1)
inline int32_t NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610 (NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___array0, int32_t ___value1, const RuntimeMethod* method)
{
	return ((  int32_t (*) (NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 , int32_t, const RuntimeMethod*))NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610_gshared)(___array0, ___value1, method);
}
// Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1<System.Int32>::GetParentCellCoords(Unity.Mathematics.int4,System.Int32)
inline int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___cellCoords0, int32_t ___level1, const RuntimeMethod* method)
{
	return ((  int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  (*) (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 , int32_t, const RuntimeMethod*))NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA_gshared)(___cellCoords0, ___level1, method);
}
// System.Void Unity.Mathematics.int4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___w3, const RuntimeMethod* method);
// Unity.Mathematics.int4 Unity.Mathematics.int4::op_Addition(Unity.Mathematics.int4,Unity.Mathematics.int4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___lhs0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs1, const RuntimeMethod* method);
// !0 Unity.Collections.NativeList`1<System.Int32>::get_Item(System.Int32)
inline int32_t NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD (NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *, int32_t, const RuntimeMethod*))NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_gshared)(__this, ___index0, method);
}
// Obi.ObiUtils/ParticleFlags Obi.ObiUtils::GetFlagsFromPhase(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57_inline (int32_t ___phase0, const RuntimeMethod* method);
// System.Int32 Obi.ObiUtils::GetGroupFromPhase(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054_inline (int32_t ___phase0, const RuntimeMethod* method);
// System.Int32 Unity.Mathematics.math::max(System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF_inline (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method);
// System.Int32 Obi.ParticleGrid/GenerateParticleParticleContactsJob::GetSimplexGroup(System.Int32,System.Int32,Obi.ObiUtils/ParticleFlags&,System.Int32&,System.Int32&,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR int32_t GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___simplexStart0, int32_t ___simplexSize1, int32_t* ___flags2, int32_t* ___category3, int32_t* ___mask4, bool* ___restPositionsEnabled5, const RuntimeMethod* method);
// System.Boolean Obi.BurstAabb::IntersectsAabb(Obi.BurstAabb&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6 (BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 * __this, BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 * ___bounds0, bool ___in2D1, const RuntimeMethod* method);
// System.Int32 Obi.SimplexCounts::GetSimplexStartAndSize(System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95 (SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA * __this, int32_t ___index0, int32_t* ___size1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_lengthsq_mD422A214358E935793F5ED10991D70F040848F2D_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::max(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeQueue`1/ParallelWriter<Obi.FluidInteraction>::Enqueue(!0)
inline void ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01 (ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 * __this, FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D  ___value0, const RuntimeMethod* method)
{
	((  void (*) (ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 *, FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D , const RuntimeMethod*))ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01_gshared)(__this, ___value0, method);
}
// System.Void Obi.ObiUtils::Swap<System.Int32>(T&,T&)
inline void ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_inline (int32_t* ___lhs0, int32_t* ___rhs1, const RuntimeMethod* method)
{
	((  void (*) (int32_t*, int32_t*, const RuntimeMethod*))ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_gshared_inline)(___lhs0, ___rhs1, method);
}
// System.Void Obi.ObiUtils::Swap<Obi.ObiUtils/ParticleFlags>(T&,T&)
inline void ObiUtils_Swap_TisParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_mDA8D45AAE52ED72F3C11941CBD419D8C7AD4463D_inline (int32_t* ___lhs0, int32_t* ___rhs1, const RuntimeMethod* method)
{
	((  void (*) (int32_t*, int32_t*, const RuntimeMethod*))ObiUtils_Swap_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_mAB90511ADDFFC04F5B65A8553B95D50B7D75DFA1_gshared_inline)(___lhs0, ___rhs1, method);
}
// Unity.Mathematics.float4 Obi.BurstMath::BarycenterForSimplexOfSize(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  BurstMath_BarycenterForSimplexOfSize_m26CB914EE114D2DE19C4F75C97009A695F6C076A_inline (int32_t ___simplexSize0, const RuntimeMethod* method);
// System.Void Obi.BurstSimplex::CacheData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * __this, const RuntimeMethod* method);
// Obi.BurstLocalOptimization/SurfacePoint Obi.BurstLocalOptimization::Optimize<Obi.BurstSimplex>(T&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32,System.Int32,Unity.Mathematics.float4&,Unity.Mathematics.float4&,System.Int32,System.Single)
inline SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63 (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___function0, NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___positions1, NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  ___orientations2, NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  ___radii3, NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  ___simplices4, int32_t ___simplexStart5, int32_t ___simplexSize6, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * ___convexBary7, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * ___convexPoint8, int32_t ___maxIterations9, float ___tolerance10, const RuntimeMethod* method)
{
	return ((  SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  (*) (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *, NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B , NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739 , NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B , NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 , int32_t, int32_t, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *, int32_t, float, const RuntimeMethod*))BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63_gshared)(___function0, ___positions1, ___orientations2, ___radii3, ___simplices4, ___simplexStart5, ___simplexSize6, ___convexBary7, ___convexPoint8, ___maxIterations9, ___tolerance10, method);
}
// System.Single Unity.Mathematics.float4::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float4,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method);
// System.Void Obi.BurstMath::OneSidedNormal(Unity.Mathematics.float4,Unity.Mathematics.float4&)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BurstMath_OneSidedNormal_m8551E1209D5159A5CB34E47F9D47F3FCE7186A68_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___forward0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * ___normal1, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>::Enqueue(!0)
inline void ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108 (ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 * __this, BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C  ___value0, const RuntimeMethod* method)
{
	((  void (*) (ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 *, BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C , const RuntimeMethod*))ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108_gshared)(__this, ___value0, method);
}
// Unity.Mathematics.float4 Obi.BurstAffineTransform::InverseTransformPoint(Unity.Mathematics.float4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  BurstAffineTransform_InverseTransformPoint_m62FF98D4C1F4B903143CBEBB8BE560DD8973E955 (BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___point0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::floor(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_floor_m6F1A809C667676D8CDF372A24CC318F90943AAC7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::set_Item(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___v0, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.math::sign(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_sign_m63A39DE29A6BCB0E9BA152B458A0DD2EB9EFB23C_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method);
// Unity.Mathematics.int4 Unity.Mathematics.int4::op_Explicit(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method);
// Unity.Mathematics.int4 Unity.Mathematics.int4::op_Multiply(Unity.Mathematics.int4,Unity.Mathematics.int4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Multiply_m5FC679C570C40C6A3CCF3E600DACD56D48063524_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___lhs0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::length(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_length_mD7967FEA18B97C7AC6CFE6B0BE3D45D35D355170_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method);
// System.Single Obi.Poly6Kernel::W(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Poly6Kernel_W_mA9FD1FFD1D90D74DB6803EC97F570DAB31EAE849 (Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235 * __this, float ___r0, float ___h1, const RuntimeMethod* method);
// System.Void Obi.ParticleGrid/InterpolateDiffusePropertiesJob::Execute(System.Int32)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A (InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___p0, const RuntimeMethod* method);
// System.Void Obi.NativeMultilevelGrid`1<System.Int32>::Clear()
inline void NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036 (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * __this, const RuntimeMethod* method)
{
	((  void (*) (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *, const RuntimeMethod*))NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036_gshared)(__this, method);
}
// System.Int32 Obi.NativeMultilevelGrid`1<System.Int32>::GetOrCreateCell(Unity.Mathematics.int4)
inline int32_t NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8 (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * __this, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___cellCoords0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 , const RuntimeMethod*))NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8_gshared)(__this, ___cellCoords0, method);
}
// System.Void Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>::Add(K)
inline void Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9 (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, int32_t ___entity0, const RuntimeMethod* method)
{
	((  void (*) (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *, int32_t, const RuntimeMethod*))Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9_gshared)(__this, ___entity0, method);
}
// System.Void Unity.Collections.NativeList`1<Obi.NativeMultilevelGrid`1/Cell`1<System.Int32,System.Int32>>::set_Item(System.Int32,!0)
inline void NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118 (NativeList_1_t37C384457963A144915FA46915001309FA309A59 * __this, int32_t ___index0, Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  ___value1, const RuntimeMethod* method)
{
	((  void (*) (NativeList_1_t37C384457963A144915FA46915001309FA309A59 *, int32_t, Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B , const RuntimeMethod*))NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118_gshared)(__this, ___index0, ___value1, method);
}
// System.Void Obi.ParticleGrid/UpdateGrid::Execute()
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47 (UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA * IL2CPP_PARAMETER_RESTRICT __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_x()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594 (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_y()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3 (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector3Int::get_z()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6 (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, const RuntimeMethod* method);
// Obi.MeshVoxelizer/Voxel Obi.MeshVoxelizer::get_Item(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356 (MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3Int::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method);
// System.Int32 System.Array::GetLength(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB (RuntimeArray * __this, int32_t ___dimension0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Max(System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_Max_mC299EEF1FD6084E41A182E2033790DB50DEF5B39 (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___values0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Log_mF7F3624FA030AB57AD8C1F4CAF084B2DCC99897A (float ___f0, float ___p1, const RuntimeMethod* method);
// System.Void Obi.VoxelDistanceField::JumpFloodPass(System.Int32,UnityEngine.Vector3Int[0...,0...,0...],UnityEngine.Vector3Int[0...,0...,0...])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3 (VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * __this, int32_t ___stride0, Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___input1, Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* ___output2, const RuntimeMethod* method);
// System.Void Obi.VoxelPathFinder/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888 (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3Int::op_Equality(UnityEngine.Vector3Int,UnityEngine.Vector3Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___lhs0, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3Int::op_Implicit(UnityEngine.Vector3Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___v0, const RuntimeMethod* method);
// System.Single Obi.VoxelPathFinder/TargetVoxel::get_cost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, const RuntimeMethod* method);
// System.Void Obi.VoxelPathFinder/TargetVoxel::.ctor(UnityEngine.Vector3Int,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates0, float ___distance1, float ___heuristic2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3Int::Equals(UnityEngine.Vector3Int)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___other0, const RuntimeMethod* method);
// System.Boolean Obi.VoxelPathFinder/TargetVoxel::Equals(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method);
// System.Int32 System.Single::CompareTo(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Single_CompareTo_m80B5B5A70A2343C3A8673F35635EBED4458109B4 (float* __this, float ___value0, const RuntimeMethod* method);
// System.Int32 Obi.VoxelPathFinder/TargetVoxel::CompareTo(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int3::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method);
// System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Boolean System.Single::IsNaN(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599 (float ___f0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method);
// Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(System.Single,Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::floor(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::sign(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline (float ___x0, const RuntimeMethod* method);
// System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.float4)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::sqrt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline (float ___x0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::log(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_log_m32923945396EF294F36A8CA9848ED0771A77F50F_inline (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::ceil(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_ceil_m09F7522539A79883D220617019957CCF0CB4DA8B_inline (float ___x0, const RuntimeMethod* method);
// System.Single Unity.Mathematics.math::exp2(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_exp2_m91B7071A3214B73A372F626DFAA243B3C65BD230_inline (float ___x0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__8__ctor_mE1FD39996C2F57CF4C0B2C10C7CB001423DE2381 (U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__8_System_IDisposable_Dispose_m6A03B2E04499DB25B9E769956B700DB301ECA55A (U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiTearableClothBlueprint/<Initialize>d__8::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CInitializeU3Ed__8_MoveNext_m2028F8C3F5AB82D80D35ABCDAEF7DBF1767E1171 (U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_t347675EB2D2584E885712756F4F5A222D13817BE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_tAE941B6A20BC1EB442179E6FD3406ED39D7851BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t6B3EEC19B84F0BC1D3DD815258B1A2031E0296E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t9C685EA70A84E3D46C2B5D05491B7B985EB524C9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2951FBF2856E4DBA8EE992C727853EDB0D92574C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral55C7FCB65C82500AE3D491E82F447D633911057E);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * V_1 = NULL;
	int32_t V_2 = 0;
	Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	RuntimeObject* V_5 = NULL;
	Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2  V_6;
	memset((&V_6), 0, sizeof(V_6));
	RuntimeObject* V_7 = NULL;
	HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7  V_8;
	memset((&V_8), 0, sizeof(V_8));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_9;
	memset((&V_9), 0, sizeof(V_9));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_10;
	memset((&V_10), 0, sizeof(V_10));
	int32_t V_11 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_002e;
			}
			case 1:
			{
				goto IL_03ac;
			}
			case 2:
			{
				goto IL_0408;
			}
			case 3:
			{
				goto IL_0444;
			}
			case 4:
			{
				goto IL_0480;
			}
			case 5:
			{
				goto IL_04bc;
			}
		}
	}
	{
		return (bool)0;
	}

IL_002e:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (inputMesh == null || !inputMesh.isReadable)
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_3 = V_1;
		NullCheck(L_3);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_4 = ((ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B *)L_3)->get_inputMesh_34();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0050;
		}
	}
	{
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_6 = V_1;
		NullCheck(L_6);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_7 = ((ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B *)L_6)->get_inputMesh_34();
		NullCheck(L_7);
		bool L_8;
		L_8 = Mesh_get_isReadable_m7F09EA53D0B8F385A3D4BE15199B4C2B91A42B1A(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_005c;
		}
	}

IL_0050:
	{
		// Debug.LogError("The input mesh is null, or not readable.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485(_stringLiteral55C7FCB65C82500AE3D491E82F447D633911057E, /*hidden argument*/NULL);
		// yield break;
		return (bool)0;
	}

IL_005c:
	{
		// ClearParticleGroups();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_9 = V_1;
		NullCheck(L_9);
		ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18(L_9, (bool)1, /*hidden argument*/NULL);
		// topology = new HalfEdgeMesh();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_10 = V_1;
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_11 = (HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 *)il2cpp_codegen_object_new(HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_il2cpp_TypeInfo_var);
		HalfEdgeMesh__ctor_m641C71B020EB960E23164DEABD3019B25C6F5404(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_10)->set_topology_37(L_11);
		// topology.inputMesh = inputMesh;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_12 = V_1;
		NullCheck(L_12);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_13 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_12)->get_topology_37();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_14 = V_1;
		NullCheck(L_14);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_15 = ((ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B *)L_14)->get_inputMesh_34();
		NullCheck(L_13);
		L_13->set_inputMesh_0(L_15);
		// topology.Generate();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_16 = V_1;
		NullCheck(L_16);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_17 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_16)->get_topology_37();
		NullCheck(L_17);
		HalfEdgeMesh_Generate_mD60A36F035F35EBE9F282926F0C8E06962265CC7(L_17, /*hidden argument*/NULL);
		// pooledParticles = (int)((topology.faces.Count * 3 - topology.vertices.Count) * tearCapacity);
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_18 = V_1;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_19 = V_1;
		NullCheck(L_19);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_20 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_19)->get_topology_37();
		NullCheck(L_20);
		List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * L_21 = L_20->get_faces_8();
		NullCheck(L_21);
		int32_t L_22;
		L_22 = List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_inline(L_21, /*hidden argument*/List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_RuntimeMethod_var);
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_23 = V_1;
		NullCheck(L_23);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_24 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_23)->get_topology_37();
		NullCheck(L_24);
		List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_25 = L_24->get_vertices_5();
		NullCheck(L_25);
		int32_t L_26;
		L_26 = List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_inline(L_25, /*hidden argument*/List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var);
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_27 = V_1;
		NullCheck(L_27);
		float L_28 = L_27->get_tearCapacity_42();
		NullCheck(L_18);
		L_18->set_pooledParticles_43(il2cpp_codegen_cast_double_to_int<int32_t>(((float)il2cpp_codegen_multiply((float)((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_22, (int32_t)3)), (int32_t)L_26)))), (float)L_28))));
		// int totalParticles = topology.vertices.Count + pooledParticles;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_29 = V_1;
		NullCheck(L_29);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_30 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_29)->get_topology_37();
		NullCheck(L_30);
		List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_31 = L_30->get_vertices_5();
		NullCheck(L_31);
		int32_t L_32;
		L_32 = List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_inline(L_31, /*hidden argument*/List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var);
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_33 = V_1;
		NullCheck(L_33);
		int32_t L_34 = L_33->get_pooledParticles_43();
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)L_34));
		// positions = new Vector3[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_35 = V_1;
		int32_t L_36 = V_2;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_37 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_36);
		NullCheck(L_35);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_35)->set_positions_9(L_37);
		// restPositions = new Vector4[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_38 = V_1;
		int32_t L_39 = V_2;
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_40 = (Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871*)SZArrayNew(Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871_il2cpp_TypeInfo_var, (uint32_t)L_39);
		NullCheck(L_38);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_38)->set_restPositions_10(L_40);
		// velocities = new Vector3[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_41 = V_1;
		int32_t L_42 = V_2;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_43 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_42);
		NullCheck(L_41);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_41)->set_velocities_13(L_43);
		// invMasses = new float[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_44 = V_1;
		int32_t L_45 = V_2;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_46 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_45);
		NullCheck(L_44);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_44)->set_invMasses_15(L_46);
		// principalRadii = new Vector3[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_47 = V_1;
		int32_t L_48 = V_2;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_49 = (Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4*)SZArrayNew(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4_il2cpp_TypeInfo_var, (uint32_t)L_48);
		NullCheck(L_47);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_47)->set_principalRadii_18(L_49);
		// filters = new int[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_50 = V_1;
		int32_t L_51 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_52 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_51);
		NullCheck(L_50);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_50)->set_filters_17(L_52);
		// colors = new Color[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_53 = V_1;
		int32_t L_54 = V_2;
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_55 = (ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834*)SZArrayNew(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834_il2cpp_TypeInfo_var, (uint32_t)L_54);
		NullCheck(L_53);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_53)->set_colors_19(L_55);
		// areaContribution = new float[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_56 = V_1;
		int32_t L_57 = V_2;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_58 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_57);
		NullCheck(L_56);
		((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_56)->set_areaContribution_40(L_58);
		// tearResistance = new float[totalParticles];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_59 = V_1;
		int32_t L_60 = V_2;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_61 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_60);
		NullCheck(L_59);
		L_59->set_tearResistance_44(L_61);
		// m_ActiveParticleCount = topology.vertices.Count;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_62 = V_1;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_63 = V_1;
		NullCheck(L_63);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_64 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_63)->get_topology_37();
		NullCheck(L_64);
		List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_65 = L_64->get_vertices_5();
		NullCheck(L_65);
		int32_t L_66;
		L_66 = List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_inline(L_65, /*hidden argument*/List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var);
		NullCheck(L_62);
		((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_62)->set_m_ActiveParticleCount_6(L_66);
		// for (int i = 0; i < topology.vertices.Count; i++)
		__this->set_U3CiU3E5__6_7(0);
		goto IL_03c5;
	}

IL_0162:
	{
		// HalfEdgeMesh.Vertex vertex = topology.vertices[i];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_67 = V_1;
		NullCheck(L_67);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_68 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_67)->get_topology_37();
		NullCheck(L_68);
		List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_69 = L_68->get_vertices_5();
		int32_t L_70 = __this->get_U3CiU3E5__6_7();
		NullCheck(L_69);
		Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_71;
		L_71 = List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_inline(L_69, L_70, /*hidden argument*/List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_RuntimeMethod_var);
		V_3 = L_71;
		// areaContribution[i] = 0;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_72 = V_1;
		NullCheck(L_72);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_73 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_72)->get_areaContribution_40();
		int32_t L_74 = __this->get_U3CiU3E5__6_7();
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(L_74), (float)(0.0f));
		// foreach (HalfEdgeMesh.Face face in topology.GetNeighbourFacesEnumerator(vertex))
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_75 = V_1;
		NullCheck(L_75);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_76 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_75)->get_topology_37();
		Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_77 = V_3;
		NullCheck(L_76);
		RuntimeObject* L_78;
		L_78 = HalfEdgeMesh_GetNeighbourFacesEnumerator_m055B1E4B59B1C20B1C6F542EE88DA13DBE7DF26D(L_76, L_77, /*hidden argument*/NULL);
		NullCheck(L_78);
		RuntimeObject* L_79;
		L_79 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Face>::GetEnumerator() */, IEnumerable_1_tAE941B6A20BC1EB442179E6FD3406ED39D7851BA_il2cpp_TypeInfo_var, L_78);
		V_5 = L_79;
	}

IL_019e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01d1;
		}

IL_01a0:
		{
			// foreach (HalfEdgeMesh.Face face in topology.GetNeighbourFacesEnumerator(vertex))
			RuntimeObject* L_80 = V_5;
			NullCheck(L_80);
			Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2  L_81;
			L_81 = InterfaceFuncInvoker0< Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/Face>::get_Current() */, IEnumerator_1_t9C685EA70A84E3D46C2B5D05491B7B985EB524C9_il2cpp_TypeInfo_var, L_80);
			V_6 = L_81;
			// areaContribution[i] += topology.GetFaceArea(face) / 3;
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_82 = V_1;
			NullCheck(L_82);
			SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_83 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_82)->get_areaContribution_40();
			int32_t L_84 = __this->get_U3CiU3E5__6_7();
			NullCheck(L_83);
			float* L_85 = ((L_83)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_84)));
			float L_86 = *((float*)L_85);
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_87 = V_1;
			NullCheck(L_87);
			HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_88 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_87)->get_topology_37();
			Face_tEC55D0F17B08DE7A2856A0AF7730CB8B57D2F7C2  L_89 = V_6;
			NullCheck(L_88);
			float L_90;
			L_90 = HalfEdgeMesh_GetFaceArea_m27C2ADF35DAB7C31E951DE1EED24C9CA6472FB6E(L_88, L_89, /*hidden argument*/NULL);
			*((float*)L_85) = (float)((float)il2cpp_codegen_add((float)L_86, (float)((float)((float)L_90/(float)(3.0f)))));
		}

IL_01d1:
		{
			// foreach (HalfEdgeMesh.Face face in topology.GetNeighbourFacesEnumerator(vertex))
			RuntimeObject* L_91 = V_5;
			NullCheck(L_91);
			bool L_92;
			L_92 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_91);
			if (L_92)
			{
				goto IL_01a0;
			}
		}

IL_01da:
		{
			IL2CPP_LEAVE(0x1E8, FINALLY_01dc);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_01dc;
	}

FINALLY_01dc:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_93 = V_5;
			if (!L_93)
			{
				goto IL_01e7;
			}
		}

IL_01e0:
		{
			RuntimeObject* L_94 = V_5;
			NullCheck(L_94);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_94);
		}

IL_01e7:
		{
			IL2CPP_END_FINALLY(476)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(476)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x1E8, IL_01e8)
	}

IL_01e8:
	{
		// float minEdgeLength = Single.MaxValue;
		V_4 = ((std::numeric_limits<float>::max)());
		// foreach (HalfEdgeMesh.HalfEdge edge in topology.GetNeighbourEdgesEnumerator(vertex))
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_95 = V_1;
		NullCheck(L_95);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_96 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_95)->get_topology_37();
		Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_97 = V_3;
		NullCheck(L_96);
		RuntimeObject* L_98;
		L_98 = HalfEdgeMesh_GetNeighbourEdgesEnumerator_m46006FB9A047B16085C6EF70CC20209053622818(L_96, L_97, /*hidden argument*/NULL);
		NullCheck(L_98);
		RuntimeObject* L_99;
		L_99 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/HalfEdge>::GetEnumerator() */, IEnumerable_1_t347675EB2D2584E885712756F4F5A222D13817BE_il2cpp_TypeInfo_var, L_98);
		V_7 = L_99;
	}

IL_0202:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0277;
		}

IL_0204:
		{
			// foreach (HalfEdgeMesh.HalfEdge edge in topology.GetNeighbourEdgesEnumerator(vertex))
			RuntimeObject* L_100 = V_7;
			NullCheck(L_100);
			HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7  L_101;
			L_101 = InterfaceFuncInvoker0< HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/HalfEdge>::get_Current() */, IEnumerator_1_t6B3EEC19B84F0BC1D3DD815258B1A2031E0296E1_il2cpp_TypeInfo_var, L_100);
			V_8 = L_101;
			// Vector3 v1 = Vector3.Scale(scale, topology.vertices[topology.GetHalfEdgeStartVertex(edge)].position);
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_102 = V_1;
			NullCheck(L_102);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_103 = ((ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B *)L_102)->get_scale_35();
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_104 = V_1;
			NullCheck(L_104);
			HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_105 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_104)->get_topology_37();
			NullCheck(L_105);
			List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_106 = L_105->get_vertices_5();
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_107 = V_1;
			NullCheck(L_107);
			HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_108 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_107)->get_topology_37();
			HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7  L_109 = V_8;
			NullCheck(L_108);
			int32_t L_110;
			L_110 = HalfEdgeMesh_GetHalfEdgeStartVertex_m4BB747F2E34E9DAA7686C4EBE135F0ACF5CC96EC(L_108, L_109, /*hidden argument*/NULL);
			NullCheck(L_106);
			Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_111;
			L_111 = List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_inline(L_106, L_110, /*hidden argument*/List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_RuntimeMethod_var);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_112 = L_111.get_position_2();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_113;
			L_113 = Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline(L_103, L_112, /*hidden argument*/NULL);
			V_9 = L_113;
			// Vector3 v2 = Vector3.Scale(scale, topology.vertices[edge.endVertex].position);
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_114 = V_1;
			NullCheck(L_114);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_115 = ((ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B *)L_114)->get_scale_35();
			ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_116 = V_1;
			NullCheck(L_116);
			HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_117 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_116)->get_topology_37();
			NullCheck(L_117);
			List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_118 = L_117->get_vertices_5();
			HalfEdge_t0BA7C816CBCAD526860B20CB58375AD22E0F31F7  L_119 = V_8;
			int32_t L_120 = L_119.get_endVertex_5();
			NullCheck(L_118);
			Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_121;
			L_121 = List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_inline(L_118, L_120, /*hidden argument*/List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_RuntimeMethod_var);
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_122 = L_121.get_position_2();
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_123;
			L_123 = Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline(L_115, L_122, /*hidden argument*/NULL);
			V_10 = L_123;
			// minEdgeLength = Mathf.Min(minEdgeLength, Vector3.Distance(v1, v2));
			float L_124 = V_4;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_125 = V_9;
			Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_126 = V_10;
			float L_127;
			L_127 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_125, L_126, /*hidden argument*/NULL);
			float L_128;
			L_128 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_124, L_127, /*hidden argument*/NULL);
			V_4 = L_128;
		}

IL_0277:
		{
			// foreach (HalfEdgeMesh.HalfEdge edge in topology.GetNeighbourEdgesEnumerator(vertex))
			RuntimeObject* L_129 = V_7;
			NullCheck(L_129);
			bool L_130;
			L_130 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_129);
			if (L_130)
			{
				goto IL_0204;
			}
		}

IL_0280:
		{
			IL2CPP_LEAVE(0x28E, FINALLY_0282);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0282;
	}

FINALLY_0282:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_131 = V_7;
			if (!L_131)
			{
				goto IL_028d;
			}
		}

IL_0286:
		{
			RuntimeObject* L_132 = V_7;
			NullCheck(L_132);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_132);
		}

IL_028d:
		{
			IL2CPP_END_FINALLY(642)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(642)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x28E, IL_028e)
	}

IL_028e:
	{
		// tearResistance[i] = 1;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_133 = V_1;
		NullCheck(L_133);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_134 = L_133->get_tearResistance_44();
		int32_t L_135 = __this->get_U3CiU3E5__6_7();
		NullCheck(L_134);
		(L_134)->SetAt(static_cast<il2cpp_array_size_t>(L_135), (float)(1.0f));
		// invMasses[i] = 1;//(/*skinnedMeshRenderer == null &&*/ areaContribution[i] > 0) ? (1.0f / (DEFAULT_PARTICLE_MASS * areaContribution[i])) : 0;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_136 = V_1;
		NullCheck(L_136);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_137 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_136)->get_invMasses_15();
		int32_t L_138 = __this->get_U3CiU3E5__6_7();
		NullCheck(L_137);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(L_138), (float)(1.0f));
		// positions[i] = Vector3.Scale(scale,vertex.position);
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_139 = V_1;
		NullCheck(L_139);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_140 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_139)->get_positions_9();
		int32_t L_141 = __this->get_U3CiU3E5__6_7();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_142 = V_1;
		NullCheck(L_142);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_143 = ((ObiMeshBasedActorBlueprint_tBBBE049742C7C5E021B4AE1A70299C5436BCE66B *)L_142)->get_scale_35();
		Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_144 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_145 = L_144.get_position_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_146;
		L_146 = Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline(L_143, L_145, /*hidden argument*/NULL);
		NullCheck(L_140);
		(L_140)->SetAt(static_cast<il2cpp_array_size_t>(L_141), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_146);
		// restPositions[i] = positions[i];
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_147 = V_1;
		NullCheck(L_147);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_148 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_147)->get_restPositions_10();
		int32_t L_149 = __this->get_U3CiU3E5__6_7();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_150 = V_1;
		NullCheck(L_150);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_151 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_150)->get_positions_9();
		int32_t L_152 = __this->get_U3CiU3E5__6_7();
		NullCheck(L_151);
		int32_t L_153 = L_152;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_154 = (L_151)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_155;
		L_155 = Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB(L_154, /*hidden argument*/NULL);
		NullCheck(L_148);
		(L_148)->SetAt(static_cast<il2cpp_array_size_t>(L_149), (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 )L_155);
		// restPositions[i][3] = 1; // activate rest position.
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_156 = V_1;
		NullCheck(L_156);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_157 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_156)->get_restPositions_10();
		int32_t L_158 = __this->get_U3CiU3E5__6_7();
		NullCheck(L_157);
		Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A((Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 *)((L_157)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_158))), 3, (1.0f), /*hidden argument*/NULL);
		// principalRadii[i] = Vector3.one * minEdgeLength * 0.5f;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_159 = V_1;
		NullCheck(L_159);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_160 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_159)->get_principalRadii_18();
		int32_t L_161 = __this->get_U3CiU3E5__6_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_162;
		L_162 = Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB(/*hidden argument*/NULL);
		float L_163 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_164;
		L_164 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_162, L_163, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_165;
		L_165 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_164, (0.5f), /*hidden argument*/NULL);
		NullCheck(L_160);
		(L_160)->SetAt(static_cast<il2cpp_array_size_t>(L_161), (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E )L_165);
		// filters[i] = ObiUtils.MakeFilter(ObiUtils.CollideWithEverything, 1);
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_166 = V_1;
		NullCheck(L_166);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_167 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_166)->get_filters_17();
		int32_t L_168 = __this->get_U3CiU3E5__6_7();
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		int32_t L_169;
		L_169 = ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline(((int32_t)65535), 1, /*hidden argument*/NULL);
		NullCheck(L_167);
		(L_167)->SetAt(static_cast<il2cpp_array_size_t>(L_168), (int32_t)L_169);
		// colors[i] = Color.white;
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_170 = V_1;
		NullCheck(L_170);
		ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* L_171 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_170)->get_colors_19();
		int32_t L_172 = __this->get_U3CiU3E5__6_7();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_173;
		L_173 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		NullCheck(L_171);
		(L_171)->SetAt(static_cast<il2cpp_array_size_t>(L_172), (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 )L_173);
		// if (i % 500 == 0)
		int32_t L_174 = __this->get_U3CiU3E5__6_7();
		if (((int32_t)((int32_t)L_174%(int32_t)((int32_t)500))))
		{
			goto IL_03b3;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("ObiCloth: generating particles...", i / (float)topology.vertices.Count);
		int32_t L_175 = __this->get_U3CiU3E5__6_7();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_176 = V_1;
		NullCheck(L_176);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_177 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_176)->get_topology_37();
		NullCheck(L_177);
		List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_178 = L_177->get_vertices_5();
		NullCheck(L_178);
		int32_t L_179;
		L_179 = List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_inline(L_178, /*hidden argument*/List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var);
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_180 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_180, _stringLiteral2951FBF2856E4DBA8EE992C727853EDB0D92574C, ((float)((float)((float)((float)L_175))/(float)((float)((float)L_179)))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_180);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_03ac:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_03b3:
	{
		// for (int i = 0; i < topology.vertices.Count; i++)
		int32_t L_181 = __this->get_U3CiU3E5__6_7();
		V_11 = L_181;
		int32_t L_182 = V_11;
		__this->set_U3CiU3E5__6_7(((int32_t)il2cpp_codegen_add((int32_t)L_182, (int32_t)1)));
	}

IL_03c5:
	{
		// for (int i = 0; i < topology.vertices.Count; i++)
		int32_t L_183 = __this->get_U3CiU3E5__6_7();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_184 = V_1;
		NullCheck(L_184);
		HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7 * L_185 = ((ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD *)L_184)->get_topology_37();
		NullCheck(L_185);
		List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * L_186 = L_185->get_vertices_5();
		NullCheck(L_186);
		int32_t L_187;
		L_187 = List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_inline(L_186, /*hidden argument*/List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_RuntimeMethod_var);
		if ((((int32_t)L_183) < ((int32_t)L_187)))
		{
			goto IL_0162;
		}
	}
	{
		// IEnumerator dt = GenerateDeformableTriangles();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_188 = V_1;
		NullCheck(L_188);
		RuntimeObject* L_189;
		L_189 = VirtFuncInvoker0< RuntimeObject* >::Invoke(18 /* System.Collections.IEnumerator Obi.ObiClothBlueprintBase::GenerateDeformableTriangles() */, L_188);
		__this->set_U3CdtU3E5__2_3(L_189);
		goto IL_040f;
	}

IL_03ee:
	{
		// yield return dt.Current;
		RuntimeObject* L_190 = __this->get_U3CdtU3E5__2_3();
		NullCheck(L_190);
		RuntimeObject * L_191;
		L_191 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_190);
		__this->set_U3CU3E2__current_1(L_191);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0408:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_040f:
	{
		// while (dt.MoveNext())
		RuntimeObject* L_192 = __this->get_U3CdtU3E5__2_3();
		NullCheck(L_192);
		bool L_193;
		L_193 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_192);
		if (L_193)
		{
			goto IL_03ee;
		}
	}
	{
		// IEnumerator dc = CreateDistanceConstraints();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_194 = V_1;
		NullCheck(L_194);
		RuntimeObject* L_195;
		L_195 = VirtFuncInvoker0< RuntimeObject* >::Invoke(20 /* System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateDistanceConstraints() */, L_194);
		__this->set_U3CdcU3E5__3_4(L_195);
		goto IL_044b;
	}

IL_042a:
	{
		// yield return dc.Current;
		RuntimeObject* L_196 = __this->get_U3CdcU3E5__3_4();
		NullCheck(L_196);
		RuntimeObject * L_197;
		L_197 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_196);
		__this->set_U3CU3E2__current_1(L_197);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_0444:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_044b:
	{
		// while (dc.MoveNext())
		RuntimeObject* L_198 = __this->get_U3CdcU3E5__3_4();
		NullCheck(L_198);
		bool L_199;
		L_199 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_198);
		if (L_199)
		{
			goto IL_042a;
		}
	}
	{
		// IEnumerator ac = CreateAerodynamicConstraints();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_200 = V_1;
		NullCheck(L_200);
		RuntimeObject* L_201;
		L_201 = VirtFuncInvoker0< RuntimeObject* >::Invoke(21 /* System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateAerodynamicConstraints() */, L_200);
		__this->set_U3CacU3E5__4_5(L_201);
		goto IL_0487;
	}

IL_0466:
	{
		// yield return ac.Current;
		RuntimeObject* L_202 = __this->get_U3CacU3E5__4_5();
		NullCheck(L_202);
		RuntimeObject * L_203;
		L_203 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_202);
		__this->set_U3CU3E2__current_1(L_203);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_0480:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0487:
	{
		// while (ac.MoveNext())
		RuntimeObject* L_204 = __this->get_U3CacU3E5__4_5();
		NullCheck(L_204);
		bool L_205;
		L_205 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_204);
		if (L_205)
		{
			goto IL_0466;
		}
	}
	{
		// IEnumerator bc = CreateBendingConstraints();
		ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB * L_206 = V_1;
		NullCheck(L_206);
		RuntimeObject* L_207;
		L_207 = VirtFuncInvoker0< RuntimeObject* >::Invoke(22 /* System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateBendingConstraints() */, L_206);
		__this->set_U3CbcU3E5__5_6(L_207);
		goto IL_04c3;
	}

IL_04a2:
	{
		// yield return bc.Current;
		RuntimeObject* L_208 = __this->get_U3CbcU3E5__5_6();
		NullCheck(L_208);
		RuntimeObject * L_209;
		L_209 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_208);
		__this->set_U3CU3E2__current_1(L_209);
		__this->set_U3CU3E1__state_0(5);
		return (bool)1;
	}

IL_04bc:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_04c3:
	{
		// while (bc.MoveNext())
		RuntimeObject* L_210 = __this->get_U3CbcU3E5__5_6();
		NullCheck(L_210);
		bool L_211;
		L_211 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_210);
		if (L_211)
		{
			goto IL_04a2;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D1131F06EECDDE2B78AD00FB36522B66AD3538C (U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0 (U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m6ADA7C8F48F23CC6603E2ECDDF2999F8B85CF87F (U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiTriangleMeshContainer/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * L_0 = (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 *)il2cpp_codegen_object_new(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// Obi.Triangle Obi.ObiTriangleMeshContainer/<>c::<GetOrCreateTriangleMesh>b__6_0(Obi.IBounded)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8  U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42 (U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232 * __this, RuntimeObject* ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Triangle[] tris = Array.ConvertAll(t, x => (Triangle)x);
		RuntimeObject* L_0 = ___x0;
		return ((*(Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 *)((Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8 *)UnBox(L_0, Triangle_t6EBD8067A5E92E374FBCEE8434826BC760D9FDC8_il2cpp_TypeInfo_var))));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBindU3Ed__31__ctor_mE5C6B10FCF9C0277DFC6D366EB0FD7BE84FB77BE (U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBindU3Ed__31_System_IDisposable_Dispose_m0470FFE716CE5F6D58F2070F1980E5DEF012C319 (U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiTriangleSkinMap/<Bind>d__31::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBindU3Ed__31_MoveNext_m8C37AFBF1649B64A19A3A721B1324FAB72700C9F (U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m208B981A048B9BF9FB11152E3517D21A8D4CD593_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2FC07AE370700D8F7FAC2B165583F6E0EE73B3D3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBB6ACC004563D2B5AF708C3AC17178DFCC58F122);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFA6A4202087E8A0CD5992B98096167C9B5835DEF);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * V_1 = NULL;
	int32_t V_2 = 0;
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_11;
	memset((&V_11), 0, sizeof(V_11));
	int32_t V_12 = 0;
	float V_13 = 0.0f;
	SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * V_14 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_15;
	memset((&V_15), 0, sizeof(V_15));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_16;
	memset((&V_16), 0, sizeof(V_16));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_17;
	memset((&V_17), 0, sizeof(V_17));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_18;
	memset((&V_18), 0, sizeof(V_18));
	int32_t V_19 = 0;
	MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * V_20 = NULL;
	SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * V_21 = NULL;
	float V_22 = 0.0f;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0383;
			}
			case 2:
			{
				goto IL_059b;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// Clear();
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_3 = V_1;
		NullCheck(L_3);
		ObiTriangleSkinMap_Clear_mAE6BED8941D24054DD5ACAF5C13431FF25515143(L_3, /*hidden argument*/NULL);
		// if (master == null || slave == null)
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_4 = V_1;
		NullCheck(L_4);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_5;
		L_5 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_004b;
		}
	}
	{
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_7 = V_1;
		NullCheck(L_7);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_8;
		L_8 = ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37_inline(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_8, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_004d;
		}
	}

IL_004b:
	{
		// yield break;
		return (bool)0;
	}

IL_004d:
	{
		// Vector3[] slavePositions = slave.vertices;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_10 = V_1;
		NullCheck(L_10);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_11;
		L_11 = ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37_inline(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_12;
		L_12 = Mesh_get_vertices_mB7A79698792B3CBA0E7E6EACDA6C031E496FB595(L_11, /*hidden argument*/NULL);
		__this->set_U3CslavePositionsU3E5__2_3(L_12);
		// Vector3[] slaveNormals = slave.normals;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_13 = V_1;
		NullCheck(L_13);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_14;
		L_14 = ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37_inline(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_15;
		L_15 = Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0(L_14, /*hidden argument*/NULL);
		__this->set_U3CslaveNormalsU3E5__3_4(L_15);
		// Vector4[] slaveTangents = slave.tangents;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_16 = V_1;
		NullCheck(L_16);
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_17;
		L_17 = ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37_inline(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_18;
		L_18 = Mesh_get_tangents_m278A41721D47A627367F3F8E2B722B80A949A0F3(L_17, /*hidden argument*/NULL);
		__this->set_U3CslaveTangentsU3E5__4_5(L_18);
		// Matrix4x4 s2world = m_SlaveTransform.GetMatrix4X4();
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_19 = V_1;
		NullCheck(L_19);
		SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * L_20 = L_19->get_address_of_m_SlaveTransform_10();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_21;
		L_21 = SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE((SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 *)L_20, /*hidden argument*/NULL);
		__this->set_U3Cs2worldU3E5__5_6(L_21);
		// Matrix4x4 s2worldNormal = s2world.inverse.transpose;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_22 = __this->get_address_of_U3Cs2worldU3E5__5_6();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_23;
		L_23 = Matrix4x4_get_inverse_mFA34ECC790B269522F60FC32370D628DAFCAE225((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_24;
		L_24 = Matrix4x4_get_transpose_mE5DC808B366ADAE2406A4A198332B12AEC900F05((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)(&V_3), /*hidden argument*/NULL);
		__this->set_U3Cs2worldNormalU3E5__6_7(L_24);
		// int activeDeformableTriangles = 0;
		V_2 = 0;
		// for (int i = 0; i < master.deformableTriangles.Length; i += 3)
		V_4 = 0;
		goto IL_0118;
	}

IL_00b1:
	{
		// int t1 = master.deformableTriangles[i];
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_25 = V_1;
		NullCheck(L_25);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_26;
		L_26 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_27 = L_26->get_deformableTriangles_38();
		int32_t L_28 = V_4;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		int32_t L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		// int t2 = master.deformableTriangles[i + 1];
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_31 = V_1;
		NullCheck(L_31);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_32;
		L_32 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_33 = L_32->get_deformableTriangles_38();
		int32_t L_34 = V_4;
		NullCheck(L_33);
		int32_t L_35 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		V_5 = L_36;
		// int t3 = master.deformableTriangles[i + 2];
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_37 = V_1;
		NullCheck(L_37);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_38;
		L_38 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_39 = L_38->get_deformableTriangles_38();
		int32_t L_40 = V_4;
		NullCheck(L_39);
		int32_t L_41 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)2));
		int32_t L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_6 = L_42;
		// if (t1 >= master.activeParticleCount || t2 >= master.activeParticleCount || t3 >= master.activeParticleCount)
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_43 = V_1;
		NullCheck(L_43);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_44;
		L_44 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_43, /*hidden argument*/NULL);
		NullCheck(L_44);
		int32_t L_45;
		L_45 = ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline(L_44, /*hidden argument*/NULL);
		if ((((int32_t)L_30) >= ((int32_t)L_45)))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_46 = V_5;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_47 = V_1;
		NullCheck(L_47);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_48;
		L_48 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		int32_t L_49;
		L_49 = ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_46) >= ((int32_t)L_49)))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_50 = V_6;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_51 = V_1;
		NullCheck(L_51);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_52;
		L_52 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		int32_t L_53;
		L_53 = ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline(L_52, /*hidden argument*/NULL);
		if ((((int32_t)L_50) >= ((int32_t)L_53)))
		{
			goto IL_0112;
		}
	}
	{
		// activeDeformableTriangles++;
		int32_t L_54 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_54, (int32_t)1));
	}

IL_0112:
	{
		// for (int i = 0; i < master.deformableTriangles.Length; i += 3)
		int32_t L_55 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_55, (int32_t)3));
	}

IL_0118:
	{
		// for (int i = 0; i < master.deformableTriangles.Length; i += 3)
		int32_t L_56 = V_4;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_57 = V_1;
		NullCheck(L_57);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_58;
		L_58 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_59 = L_58->get_deformableTriangles_38();
		NullCheck(L_59);
		if ((((int32_t)L_56) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_59)->max_length))))))
		{
			goto IL_00b1;
		}
	}
	{
		// MasterFace[] masterFaces = new MasterFace[activeDeformableTriangles];
		int32_t L_60 = V_2;
		MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* L_61 = (MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4*)(MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4*)SZArrayNew(MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4_il2cpp_TypeInfo_var, (uint32_t)L_60);
		__this->set_U3CmasterFacesU3E5__7_8(L_61);
		// int count = 0;
		__this->set_U3CcountU3E5__8_9(0);
		// for (int i = 0; i < master.deformableTriangles.Length; i += 3)
		__this->set_U3CiU3E5__9_10(0);
		goto IL_0398;
	}

IL_0148:
	{
		// MasterFace face = new MasterFace();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_62 = (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 *)il2cpp_codegen_object_new(MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46_il2cpp_TypeInfo_var);
		MasterFace__ctor_mB4AB2008F0F78D8A27CBCD816423C06862A6510E(L_62, /*hidden argument*/NULL);
		V_7 = L_62;
		// int t1 = master.deformableTriangles[i];
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_63 = V_1;
		NullCheck(L_63);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_64;
		L_64 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_63, /*hidden argument*/NULL);
		NullCheck(L_64);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_65 = L_64->get_deformableTriangles_38();
		int32_t L_66 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_65);
		int32_t L_67 = L_66;
		int32_t L_68 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_67));
		V_8 = L_68;
		// int t2 = master.deformableTriangles[i + 1];
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_69 = V_1;
		NullCheck(L_69);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_70;
		L_70 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_71 = L_70->get_deformableTriangles_38();
		int32_t L_72 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_71);
		int32_t L_73 = ((int32_t)il2cpp_codegen_add((int32_t)L_72, (int32_t)1));
		int32_t L_74 = (L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		V_9 = L_74;
		// int t3 = master.deformableTriangles[i + 2];
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_75 = V_1;
		NullCheck(L_75);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_76;
		L_76 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_75, /*hidden argument*/NULL);
		NullCheck(L_76);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_77 = L_76->get_deformableTriangles_38();
		int32_t L_78 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_77);
		int32_t L_79 = ((int32_t)il2cpp_codegen_add((int32_t)L_78, (int32_t)2));
		int32_t L_80 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		V_10 = L_80;
		// if (t1 >= master.activeParticleCount || t2 >= master.activeParticleCount || t3 >= master.activeParticleCount)
		int32_t L_81 = V_8;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_82 = V_1;
		NullCheck(L_82);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_83;
		L_83 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_82, /*hidden argument*/NULL);
		NullCheck(L_83);
		int32_t L_84;
		L_84 = ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline(L_83, /*hidden argument*/NULL);
		if ((((int32_t)L_81) >= ((int32_t)L_84)))
		{
			goto IL_038a;
		}
	}
	{
		int32_t L_85 = V_9;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_86 = V_1;
		NullCheck(L_86);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_87;
		L_87 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_86, /*hidden argument*/NULL);
		NullCheck(L_87);
		int32_t L_88;
		L_88 = ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline(L_87, /*hidden argument*/NULL);
		if ((((int32_t)L_85) >= ((int32_t)L_88)))
		{
			goto IL_038a;
		}
	}
	{
		int32_t L_89 = V_10;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_90 = V_1;
		NullCheck(L_90);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_91;
		L_91 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_90, /*hidden argument*/NULL);
		NullCheck(L_91);
		int32_t L_92;
		L_92 = ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline(L_91, /*hidden argument*/NULL);
		if ((((int32_t)L_89) >= ((int32_t)L_92)))
		{
			goto IL_038a;
		}
	}
	{
		// face.p1 = master.positions[t1];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_93 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_94 = V_1;
		NullCheck(L_94);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_95;
		L_95 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_94, /*hidden argument*/NULL);
		NullCheck(L_95);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_96 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_95)->get_positions_9();
		int32_t L_97 = V_8;
		NullCheck(L_96);
		int32_t L_98 = L_97;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_99 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		NullCheck(L_93);
		L_93->set_p1_0(L_99);
		// face.p2 = master.positions[t2];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_100 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_101 = V_1;
		NullCheck(L_101);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_102;
		L_102 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_101, /*hidden argument*/NULL);
		NullCheck(L_102);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_103 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_102)->get_positions_9();
		int32_t L_104 = V_9;
		NullCheck(L_103);
		int32_t L_105 = L_104;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_106 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_105));
		NullCheck(L_100);
		L_100->set_p2_1(L_106);
		// face.p3 = master.positions[t3];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_107 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_108 = V_1;
		NullCheck(L_108);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_109;
		L_109 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_108, /*hidden argument*/NULL);
		NullCheck(L_109);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_110 = ((ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD *)L_109)->get_positions_9();
		int32_t L_111 = V_10;
		NullCheck(L_110);
		int32_t L_112 = L_111;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_113 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		NullCheck(L_107);
		L_107->set_p3_2(L_113);
		// face.n1 = master.restNormals[t1];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_114 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_115 = V_1;
		NullCheck(L_115);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_116;
		L_116 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_115, /*hidden argument*/NULL);
		NullCheck(L_116);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_117 = L_116->get_restNormals_39();
		int32_t L_118 = V_8;
		NullCheck(L_117);
		int32_t L_119 = L_118;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_120 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_119));
		NullCheck(L_114);
		L_114->set_n1_3(L_120);
		// face.n2 = master.restNormals[t2];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_121 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_122 = V_1;
		NullCheck(L_122);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_123;
		L_123 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_122, /*hidden argument*/NULL);
		NullCheck(L_123);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_124 = L_123->get_restNormals_39();
		int32_t L_125 = V_9;
		NullCheck(L_124);
		int32_t L_126 = L_125;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		NullCheck(L_121);
		L_121->set_n2_4(L_127);
		// face.n3 = master.restNormals[t3];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_128 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_129 = V_1;
		NullCheck(L_129);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_130;
		L_130 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_129, /*hidden argument*/NULL);
		NullCheck(L_130);
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_131 = L_130->get_restNormals_39();
		int32_t L_132 = V_10;
		NullCheck(L_131);
		int32_t L_133 = L_132;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_134 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		NullCheck(L_128);
		L_128->set_n3_5(L_134);
		// face.master = m_MasterChannels[t1] |
		//               m_MasterChannels[t2] |
		//               m_MasterChannels[t3];
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_135 = V_7;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_136 = V_1;
		NullCheck(L_136);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_137 = L_136->get_m_MasterChannels_8();
		int32_t L_138 = V_8;
		NullCheck(L_137);
		int32_t L_139 = L_138;
		uint32_t L_140 = (L_137)->GetAt(static_cast<il2cpp_array_size_t>(L_139));
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_141 = V_1;
		NullCheck(L_141);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_142 = L_141->get_m_MasterChannels_8();
		int32_t L_143 = V_9;
		NullCheck(L_142);
		int32_t L_144 = L_143;
		uint32_t L_145 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_146 = V_1;
		NullCheck(L_146);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_147 = L_146->get_m_MasterChannels_8();
		int32_t L_148 = V_10;
		NullCheck(L_147);
		int32_t L_149 = L_148;
		uint32_t L_150 = (L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_149));
		NullCheck(L_135);
		L_135->set_master_14(((int32_t)((int32_t)((int32_t)((int32_t)L_140|(int32_t)L_145))|(int32_t)L_150)));
		// face.size = ((face.p1 - face.p2).magnitude +
		//              (face.p1 - face.p3).magnitude +
		//              (face.p2 - face.p3).magnitude) / 3.0f;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_151 = V_7;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_152 = V_7;
		NullCheck(L_152);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_153 = L_152->get_p1_0();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_154 = V_7;
		NullCheck(L_154);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_155 = L_154->get_p2_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_156;
		L_156 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_153, L_155, /*hidden argument*/NULL);
		V_11 = L_156;
		float L_157;
		L_157 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_158 = V_7;
		NullCheck(L_158);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_159 = L_158->get_p1_0();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_160 = V_7;
		NullCheck(L_160);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_161 = L_160->get_p3_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_162;
		L_162 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_159, L_161, /*hidden argument*/NULL);
		V_11 = L_162;
		float L_163;
		L_163 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_164 = V_7;
		NullCheck(L_164);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_165 = L_164->get_p2_1();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_166 = V_7;
		NullCheck(L_166);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_167 = L_166->get_p3_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_168;
		L_168 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_165, L_167, /*hidden argument*/NULL);
		V_11 = L_168;
		float L_169;
		L_169 = Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
		NullCheck(L_151);
		L_151->set_size_12(((float)((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_157, (float)L_163)), (float)L_169))/(float)(3.0f))));
		// face.faceNormal = Vector3.Cross(face.p2 - face.p1, face.p3 - face.p1).normalized;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_170 = V_7;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_171 = V_7;
		NullCheck(L_171);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_172 = L_171->get_p2_1();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_173 = V_7;
		NullCheck(L_173);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_174 = L_173->get_p1_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_175;
		L_175 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_172, L_174, /*hidden argument*/NULL);
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_176 = V_7;
		NullCheck(L_176);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_177 = L_176->get_p3_2();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_178 = V_7;
		NullCheck(L_178);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_179 = L_178->get_p1_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_180;
		L_180 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_177, L_179, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_181;
		L_181 = Vector3_Cross_m63414F0C545EBB616F339FF8830D37F9230736A4(L_175, L_180, /*hidden argument*/NULL);
		V_11 = L_181;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_182;
		L_182 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
		NullCheck(L_170);
		L_170->set_faceNormal_11(L_182);
		// face.index = i;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_183 = V_7;
		int32_t L_184 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_183);
		L_183->set_index_13(L_184);
		// face.CacheBarycentricData();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_185 = V_7;
		NullCheck(L_185);
		MasterFace_CacheBarycentricData_m05DB432E4FAE84E0A5D863267B948CCDDE4FB120(L_185, /*hidden argument*/NULL);
		// masterFaces[count++] = face;
		MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* L_186 = __this->get_U3CmasterFacesU3E5__7_8();
		int32_t L_187 = __this->get_U3CcountU3E5__8_9();
		V_12 = L_187;
		int32_t L_188 = V_12;
		__this->set_U3CcountU3E5__8_9(((int32_t)il2cpp_codegen_add((int32_t)L_188, (int32_t)1)));
		int32_t L_189 = V_12;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_190 = V_7;
		NullCheck(L_186);
		ArrayElementTypeCheck (L_186, L_190);
		(L_186)->SetAt(static_cast<il2cpp_array_size_t>(L_189), (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 *)L_190);
		// if (i % 10 == 0)
		int32_t L_191 = __this->get_U3CiU3E5__9_10();
		if (((int32_t)((int32_t)L_191%(int32_t)((int32_t)10))))
		{
			goto IL_038a;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("Generating master faces...", count / (float)masterFaces.Length);
		int32_t L_192 = __this->get_U3CcountU3E5__8_9();
		MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* L_193 = __this->get_U3CmasterFacesU3E5__7_8();
		NullCheck(L_193);
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_194 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_194, _stringLiteralFA6A4202087E8A0CD5992B98096167C9B5835DEF, ((float)((float)((float)((float)L_192))/(float)((float)((float)((int32_t)((int32_t)(((RuntimeArray*)L_193)->max_length))))))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_194);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0383:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_038a:
	{
		// for (int i = 0; i < master.deformableTriangles.Length; i += 3)
		int32_t L_195 = __this->get_U3CiU3E5__9_10();
		__this->set_U3CiU3E5__9_10(((int32_t)il2cpp_codegen_add((int32_t)L_195, (int32_t)3)));
	}

IL_0398:
	{
		// for (int i = 0; i < master.deformableTriangles.Length; i += 3)
		int32_t L_196 = __this->get_U3CiU3E5__9_10();
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_197 = V_1;
		NullCheck(L_197);
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_198;
		L_198 = ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline(L_197, /*hidden argument*/NULL);
		NullCheck(L_198);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_199 = L_198->get_deformableTriangles_38();
		NullCheck(L_199);
		if ((((int32_t)L_196) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_199)->max_length))))))
		{
			goto IL_0148;
		}
	}
	{
		// for (int i = 0; i < slavePositions.Length; ++i)
		__this->set_U3CiU3E5__9_10(0);
		goto IL_05b4;
	}

IL_03bc:
	{
		// if (m_SlaveChannels[i] == 0) continue;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_200 = V_1;
		NullCheck(L_200);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_201 = L_200->get_m_SlaveChannels_9();
		int32_t L_202 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_201);
		int32_t L_203 = L_202;
		uint32_t L_204 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		if (!L_204)
		{
			goto IL_05a2;
		}
	}
	{
		// float bestError = float.MaxValue;
		V_13 = ((std::numeric_limits<float>::max)());
		// SlaveVertex bestSkinning = SlaveVertex.empty;
		SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * L_205;
		L_205 = SlaveVertex_get_empty_m6B4C7246C11E79FAFFF65C981540626F82F4C5E5(/*hidden argument*/NULL);
		V_14 = L_205;
		// Vector3 worldPos = s2world.MultiplyPoint3x4(slavePositions[i]);
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_206 = __this->get_address_of_U3Cs2worldU3E5__5_6();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_207 = __this->get_U3CslavePositionsU3E5__2_3();
		int32_t L_208 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_207);
		int32_t L_209 = L_208;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_210 = (L_207)->GetAt(static_cast<il2cpp_array_size_t>(L_209));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_211;
		L_211 = Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_206, L_210, /*hidden argument*/NULL);
		V_15 = L_211;
		// Vector3 worldNormalDir = s2worldNormal.MultiplyVector(slaveNormals[i]).normalized;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_212 = __this->get_address_of_U3Cs2worldNormalU3E5__6_7();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_213 = __this->get_U3CslaveNormalsU3E5__3_4();
		int32_t L_214 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_213);
		int32_t L_215 = L_214;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_216 = (L_213)->GetAt(static_cast<il2cpp_array_size_t>(L_215));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_217;
		L_217 = Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_212, L_216, /*hidden argument*/NULL);
		V_11 = L_217;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_218;
		L_218 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
		V_16 = L_218;
		// Vector3 worldNormalPoint = worldPos + worldNormalDir * 0.05f;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_219 = V_15;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_220 = V_16;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_221;
		L_221 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_220, (0.0500000007f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_222;
		L_222 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_219, L_221, /*hidden argument*/NULL);
		V_17 = L_222;
		// Vector3 worldTangentPoint = worldPos + s2worldNormal.MultiplyVector(new Vector3(slaveTangents[i].x, slaveTangents[i].y, slaveTangents[i].z).normalized) * 0.05f;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_223 = V_15;
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * L_224 = __this->get_address_of_U3Cs2worldNormalU3E5__6_7();
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_225 = __this->get_U3CslaveTangentsU3E5__4_5();
		int32_t L_226 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_225);
		float L_227 = ((L_225)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_226)))->get_x_1();
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_228 = __this->get_U3CslaveTangentsU3E5__4_5();
		int32_t L_229 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_228);
		float L_230 = ((L_228)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_229)))->get_y_2();
		Vector4U5BU5D_tCE72D928AA6FF1852BAC5E4396F6F0131ED11871* L_231 = __this->get_U3CslaveTangentsU3E5__4_5();
		int32_t L_232 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_231);
		float L_233 = ((L_231)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_232)))->get_z_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_234;
		memset((&L_234), 0, sizeof(L_234));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_234), L_227, L_230, L_233, /*hidden argument*/NULL);
		V_11 = L_234;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_235;
		L_235 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_11), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_236;
		L_236 = Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C((Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 *)L_224, L_235, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_237;
		L_237 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_236, (0.0500000007f), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_238;
		L_238 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_223, L_237, /*hidden argument*/NULL);
		V_18 = L_238;
		// for (int j = 0; j < masterFaces.Length; ++j)
		V_19 = 0;
		goto IL_0505;
	}

IL_04a9:
	{
		// MasterFace face = masterFaces[j];
		MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* L_239 = __this->get_U3CmasterFacesU3E5__7_8();
		int32_t L_240 = V_19;
		NullCheck(L_239);
		int32_t L_241 = L_240;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_242 = (L_239)->GetAt(static_cast<il2cpp_array_size_t>(L_241));
		V_20 = L_242;
		// if ((face.master & m_SlaveChannels[i]) == 0)
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_243 = V_20;
		NullCheck(L_243);
		uint32_t L_244 = L_243->get_master_14();
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_245 = V_1;
		NullCheck(L_245);
		UInt32U5BU5D_tCF06F1E9E72E0302C762578FF5358CC523F2A2CF* L_246 = L_245->get_m_SlaveChannels_9();
		int32_t L_247 = __this->get_U3CiU3E5__9_10();
		NullCheck(L_246);
		int32_t L_248 = L_247;
		uint32_t L_249 = (L_246)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		if (!((int32_t)((int32_t)L_244&(int32_t)L_249)))
		{
			goto IL_04ff;
		}
	}
	{
		// if (BindToFace(i, face, worldPos, worldNormalPoint, worldTangentPoint, out skinning))
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_250 = V_1;
		int32_t L_251 = __this->get_U3CiU3E5__9_10();
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_252 = V_20;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_253 = V_15;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_254 = V_17;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_255 = V_18;
		NullCheck(L_250);
		bool L_256;
		L_256 = ObiTriangleSkinMap_BindToFace_mC6D3795B2BEA297D2DE57744229DBEEA82E3DEF3(L_250, L_251, L_252, L_253, L_254, L_255, (SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB **)(&V_21), /*hidden argument*/NULL);
		if (!L_256)
		{
			goto IL_04ff;
		}
	}
	{
		// float error = GetFaceMappingError(face, skinning, worldNormalDir);
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_257 = V_1;
		MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * L_258 = V_20;
		SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * L_259 = V_21;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_260 = V_16;
		NullCheck(L_257);
		float L_261;
		L_261 = ObiTriangleSkinMap_GetFaceMappingError_mEE274B631EC04C834C233252FFA299316F01F697(L_257, L_258, L_259, L_260, /*hidden argument*/NULL);
		V_22 = L_261;
		// if (error < bestError)
		float L_262 = V_22;
		float L_263 = V_13;
		if ((!(((float)L_262) < ((float)L_263))))
		{
			goto IL_04ff;
		}
	}
	{
		// bestError = error;
		float L_264 = V_22;
		V_13 = L_264;
		// bestSkinning = skinning;
		SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * L_265 = V_21;
		V_14 = L_265;
	}

IL_04ff:
	{
		// for (int j = 0; j < masterFaces.Length; ++j)
		int32_t L_266 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_266, (int32_t)1));
	}

IL_0505:
	{
		// for (int j = 0; j < masterFaces.Length; ++j)
		int32_t L_267 = V_19;
		MasterFaceU5BU5D_t170AC48B11DF243E55894C284CC581F460B544D4* L_268 = __this->get_U3CmasterFacesU3E5__7_8();
		NullCheck(L_268);
		if ((((int32_t)L_267) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_268)->max_length))))))
		{
			goto IL_04a9;
		}
	}
	{
		// if (!bestSkinning.isEmpty)
		SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * L_269 = V_14;
		NullCheck(L_269);
		bool L_270;
		L_270 = SlaveVertex_get_isEmpty_m1B2229CA5E3375CCEDB4898EEBDB72F0EAF2432F(L_269, /*hidden argument*/NULL);
		if (L_270)
		{
			goto IL_0527;
		}
	}
	{
		// skinnedVertices.Add(bestSkinning);
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_271 = V_1;
		NullCheck(L_271);
		List_1_t9F7A85717888FC673B935C9E2DEAE5AD8B0BC2DA * L_272 = L_271->get_skinnedVertices_13();
		SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * L_273 = V_14;
		NullCheck(L_272);
		List_1_Add_m208B981A048B9BF9FB11152E3517D21A8D4CD593(L_272, L_273, /*hidden argument*/List_1_Add_m208B981A048B9BF9FB11152E3517D21A8D4CD593_RuntimeMethod_var);
	}

IL_0527:
	{
		// if (i % 5 == 0)
		int32_t L_274 = __this->get_U3CiU3E5__9_10();
		if (((int32_t)((int32_t)L_274%(int32_t)5)))
		{
			goto IL_05a2;
		}
	}
	{
		// yield return new CoroutineJob.ProgressInfo("Skinning slave vertices (" + i + "/" + slavePositions.Length + ")...", i / (float)slavePositions.Length);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_275 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_276 = L_275;
		NullCheck(L_276);
		ArrayElementTypeCheck (L_276, _stringLiteralBB6ACC004563D2B5AF708C3AC17178DFCC58F122);
		(L_276)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteralBB6ACC004563D2B5AF708C3AC17178DFCC58F122);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_277 = L_276;
		int32_t* L_278 = __this->get_address_of_U3CiU3E5__9_10();
		String_t* L_279;
		L_279 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_278, /*hidden argument*/NULL);
		NullCheck(L_277);
		ArrayElementTypeCheck (L_277, L_279);
		(L_277)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_279);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_280 = L_277;
		NullCheck(L_280);
		ArrayElementTypeCheck (L_280, _stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		(L_280)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral86BBAACC00198DBB3046818AD3FC2AA10AE48DE1);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_281 = L_280;
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_282 = __this->get_U3CslavePositionsU3E5__2_3();
		NullCheck(L_282);
		V_12 = ((int32_t)((int32_t)(((RuntimeArray*)L_282)->max_length)));
		String_t* L_283;
		L_283 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&V_12), /*hidden argument*/NULL);
		NullCheck(L_281);
		ArrayElementTypeCheck (L_281, L_283);
		(L_281)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_283);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_284 = L_281;
		NullCheck(L_284);
		ArrayElementTypeCheck (L_284, _stringLiteral2FC07AE370700D8F7FAC2B165583F6E0EE73B3D3);
		(L_284)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral2FC07AE370700D8F7FAC2B165583F6E0EE73B3D3);
		String_t* L_285;
		L_285 = String_Concat_mFEA7EFA1A6E75B96B1B7BC4526AAC864BFF83CC9(L_284, /*hidden argument*/NULL);
		int32_t L_286 = __this->get_U3CiU3E5__9_10();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_287 = __this->get_U3CslavePositionsU3E5__2_3();
		NullCheck(L_287);
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_288 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_288, L_285, ((float)((float)((float)((float)L_286))/(float)((float)((float)((int32_t)((int32_t)(((RuntimeArray*)L_287)->max_length))))))), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_288);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_059b:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_05a2:
	{
		// for (int i = 0; i < slavePositions.Length; ++i)
		int32_t L_289 = __this->get_U3CiU3E5__9_10();
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_289, (int32_t)1));
		int32_t L_290 = V_12;
		__this->set_U3CiU3E5__9_10(L_290);
	}

IL_05b4:
	{
		// for (int i = 0; i < slavePositions.Length; ++i)
		int32_t L_291 = __this->get_U3CiU3E5__9_10();
		Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* L_292 = __this->get_U3CslavePositionsU3E5__2_3();
		NullCheck(L_292);
		if ((((int32_t)L_291) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_292)->max_length))))))
		{
			goto IL_03bc;
		}
	}
	{
		// bound = true;
		ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * L_293 = V_1;
		NullCheck(L_293);
		L_293->set_bound_4((bool)1);
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m799C177A7037BA730C8BC7776C400B5065B64A16 (U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50 (U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_m937FC6C298B8CA31EA931CA6B7205A747ABFC996 (U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/BarycentricPoint::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97 (const RuntimeMethod* method)
{
	{
		// return new BarycentricPoint(Vector3.zero, 0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_1;
		memset((&L_1), 0, sizeof(L_1));
		BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F((&L_1), L_0, (0.0f), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Obi.ObiTriangleSkinMap/BarycentricPoint::.ctor(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F (BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___height1, const RuntimeMethod* method)
{
	{
		// this.barycentricCoords = position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		__this->set_barycentricCoords_0(L_0);
		// this.height = height;
		float L_1 = ___height1;
		__this->set_height_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F_AdjustorThunk (RuntimeObject * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___height1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 * _thisAdjusted = reinterpret_cast<BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0 *>(__this + _offset);
	BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F(_thisAdjusted, ___position0, ___height1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiTriangleSkinMap/MasterFace::CacheBarycentricData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MasterFace_CacheBarycentricData_m05DB432E4FAE84E0A5D863267B948CCDDE4FB120 (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * __this, const RuntimeMethod* method)
{
	{
		// v0 = p3 - p1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_p3_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_p1_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_0, L_1, /*hidden argument*/NULL);
		__this->set_v0_6(L_2);
		// v1 = p2 - p1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_p2_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = __this->get_p1_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_3, L_4, /*hidden argument*/NULL);
		__this->set_v1_7(L_5);
		// dot00 = Vector3.Dot(v0, v0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = __this->get_v0_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = __this->get_v0_6();
		float L_8;
		L_8 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_6, L_7, /*hidden argument*/NULL);
		__this->set_dot00_8(L_8);
		// dot01 = Vector3.Dot(v0, v1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = __this->get_v0_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = __this->get_v1_7();
		float L_11;
		L_11 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_9, L_10, /*hidden argument*/NULL);
		__this->set_dot01_9(L_11);
		// dot11 = Vector3.Dot(v1, v1);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = __this->get_v1_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = __this->get_v1_7();
		float L_14;
		L_14 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_12, L_13, /*hidden argument*/NULL);
		__this->set_dot11_10(L_14);
		// }
		return;
	}
}
// System.Boolean Obi.ObiTriangleSkinMap/MasterFace::BarycentricCoords(UnityEngine.Vector3,UnityEngine.Vector3&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MasterFace_BarycentricCoords_m9D062C8071B60CDBB21459F906FCB9EF23E5EC46 (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___coords1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	{
		// Vector3 v2 = point - p1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___point0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_p1_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// float dot02 = Vector3.Dot(v0, v2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_v0_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		float L_5;
		L_5 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		// float dot12 = Vector3.Dot(v1, v2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = __this->get_v1_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		float L_8;
		L_8 = Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline(L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		// float det = dot00 * dot11 - dot01 * dot01;
		float L_9 = __this->get_dot00_8();
		float L_10 = __this->get_dot11_10();
		float L_11 = __this->get_dot01_9();
		float L_12 = __this->get_dot01_9();
		V_3 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), (float)((float)il2cpp_codegen_multiply((float)L_11, (float)L_12))));
		// if (!Mathf.Approximately(det, 0))
		float L_13 = V_3;
		bool L_14;
		L_14 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_13, (0.0f), /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		// float u = (dot11 * dot02 - dot01 * dot12) / det;
		float L_15 = __this->get_dot11_10();
		float L_16 = V_1;
		float L_17 = __this->get_dot01_9();
		float L_18 = V_2;
		float L_19 = V_3;
		V_4 = ((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_16)), (float)((float)il2cpp_codegen_multiply((float)L_17, (float)L_18))))/(float)L_19));
		// float v = (dot00 * dot12 - dot01 * dot02) / det;
		float L_20 = __this->get_dot00_8();
		float L_21 = V_2;
		float L_22 = __this->get_dot01_9();
		float L_23 = V_1;
		float L_24 = V_3;
		V_5 = ((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_20, (float)L_21)), (float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_23))))/(float)L_24));
		// coords = new Vector3(1 - u - v, v, u);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_25 = ___coords1;
		float L_26 = V_4;
		float L_27 = V_5;
		float L_28 = V_5;
		float L_29 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		memset((&L_30), 0, sizeof(L_30));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_30), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_26)), (float)L_27)), L_28, L_29, /*hidden argument*/NULL);
		*(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_25 = L_30;
		// return true;
		return (bool)1;
	}

IL_0096:
	{
		// return false;
		return (bool)0;
	}
}
// System.Void Obi.ObiTriangleSkinMap/MasterFace::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MasterFace__ctor_mB4AB2008F0F78D8A27CBCD816423C06862A6510E (MasterFace_t812F843F553D984ECC4151EC4483239626EE1B46 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895 (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale2, const RuntimeMethod* method)
{
	{
		// this.position = position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		__this->set_position_0(L_0);
		// this.rotation = rotation;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_1 = ___rotation1;
		__this->set_rotation_1(L_1);
		// this.scale = scale;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___scale2;
		__this->set_scale_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895_AdjustorThunk (RuntimeObject * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___scale2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * _thisAdjusted = reinterpret_cast<SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 *>(__this + _offset);
	SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895(_thisAdjusted, ___position0, ___rotation1, ___scale2, method);
}
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80 (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, const RuntimeMethod* method)
{
	{
		// position = transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___transform0;
		NullCheck(L_0);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		__this->set_position_0(L_1);
		// rotation = transform.rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = ___transform0;
		NullCheck(L_2);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3;
		L_3 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_2, /*hidden argument*/NULL);
		__this->set_rotation_1(L_3);
		// scale = transform.localScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = ___transform0;
		NullCheck(L_4);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_4, /*hidden argument*/NULL);
		__this->set_scale_2(L_5);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80_AdjustorThunk (RuntimeObject * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * _thisAdjusted = reinterpret_cast<SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 *>(__this + _offset);
	SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80(_thisAdjusted, ___transform0, method);
}
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::Apply(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0 (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, const RuntimeMethod* method)
{
	{
		// transform.position = position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___transform0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_position_0();
		NullCheck(L_0);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_1, /*hidden argument*/NULL);
		// transform.rotation = rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = ___transform0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3 = __this->get_rotation_1();
		NullCheck(L_2);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_2, L_3, /*hidden argument*/NULL);
		// transform.localScale = scale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = ___transform0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = __this->get_scale_2();
		NullCheck(L_4);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0_AdjustorThunk (RuntimeObject * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * _thisAdjusted = reinterpret_cast<SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 *>(__this + _offset);
	SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0(_thisAdjusted, ___transform0, method);
}
// UnityEngine.Matrix4x4 Obi.ObiTriangleSkinMap/SkinTransform::GetMatrix4X4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, const RuntimeMethod* method)
{
	{
		// return Matrix4x4.TRS(position, rotation, scale);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_position_0();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_1 = __this->get_rotation_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = __this->get_scale_2();
		Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  L_3;
		L_3 = Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * _thisAdjusted = reinterpret_cast<SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 *>(__this + _offset);
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  _returnValue;
	_returnValue = SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE(_thisAdjusted, method);
	return _returnValue;
}
// System.Void Obi.ObiTriangleSkinMap/SkinTransform::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E (SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * __this, const RuntimeMethod* method)
{
	{
		// position = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_position_0(L_0);
		// rotation = Quaternion.identity;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_1;
		L_1 = Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702(/*hidden argument*/NULL);
		__this->set_rotation_1(L_1);
		// scale = Vector3.one;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB(/*hidden argument*/NULL);
		__this->set_scale_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 * _thisAdjusted = reinterpret_cast<SkinTransform_t62F6686A2FADC109CEA970BFBEB1817BEA65B7C4 *>(__this + _offset);
	SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Obi.ObiTriangleSkinMap/SlaveVertex Obi.ObiTriangleSkinMap/SlaveVertex::get_empty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * SlaveVertex_get_empty_m6B4C7246C11E79FAFFF65C981540626F82F4C5E5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return new SlaveVertex(-1, -1, BarycentricPoint.zero, BarycentricPoint.zero, BarycentricPoint.zero);
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_0;
		L_0 = BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97(/*hidden argument*/NULL);
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_1;
		L_1 = BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97(/*hidden argument*/NULL);
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_2;
		L_2 = BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97(/*hidden argument*/NULL);
		SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * L_3 = (SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB *)il2cpp_codegen_object_new(SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB_il2cpp_TypeInfo_var);
		SlaveVertex__ctor_mB7C3D7EBC3A235D3D58769095378AE46286BC7A7(L_3, (-1), (-1), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Obi.ObiTriangleSkinMap/SlaveVertex::get_isEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SlaveVertex_get_isEmpty_m1B2229CA5E3375CCEDB4898EEBDB72F0EAF2432F (SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * __this, const RuntimeMethod* method)
{
	{
		// get { return slaveIndex < 0 || masterTriangleIndex < 0; }
		int32_t L_0 = __this->get_slaveIndex_0();
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = __this->get_masterTriangleIndex_1();
		return (bool)((((int32_t)L_1) < ((int32_t)0))? 1 : 0);
	}

IL_0013:
	{
		return (bool)1;
	}
}
// System.Void Obi.ObiTriangleSkinMap/SlaveVertex::.ctor(System.Int32,System.Int32,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SlaveVertex__ctor_mB7C3D7EBC3A235D3D58769095378AE46286BC7A7 (SlaveVertex_tD5D14AD418FE897268BB2BCC4FFD47F2FB9DAAFB * __this, int32_t ___slaveIndex0, int32_t ___masterTriangleIndex1, BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___position2, BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___normal3, BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  ___tangent4, const RuntimeMethod* method)
{
	{
		// public SlaveVertex(int slaveIndex, int masterTriangleIndex, BarycentricPoint position, BarycentricPoint normal, BarycentricPoint tangent)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// this.slaveIndex = slaveIndex;
		int32_t L_0 = ___slaveIndex0;
		__this->set_slaveIndex_0(L_0);
		// this.masterTriangleIndex = masterTriangleIndex;
		int32_t L_1 = ___masterTriangleIndex1;
		__this->set_masterTriangleIndex_1(L_1);
		// this.position = position;
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_2 = ___position2;
		__this->set_position_2(L_2);
		// this.normal = normal;
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_3 = ___normal3;
		__this->set_normal_3(L_3);
		// this.tangent = tangent;
		BarycentricPoint_tF1F2855D93BDE7B465DD6EC08782ED32334589D0  L_4 = ___tangent4;
		__this->set_tangent_4(L_4);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		int32_t L_1;
		L_1 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		__this->set_U3CU3El__initialThreadId_2(L_1);
		return;
	}
}
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.ObiUtils/<BilateralInterleaved>d__40::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBilateralInterleavedU3Ed__40_MoveNext_m77C2C019B232853A6CC9102657015E05AC815D61 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_005f;
			}
			case 2:
			{
				goto IL_0082;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// for (int i = 0; i < count; ++i)
		__this->set_U3CiU3E5__2_5(0);
		goto IL_0099;
	}

IL_002b:
	{
		// if (i % 2 != 0)
		int32_t L_2 = __this->get_U3CiU3E5__2_5();
		if (!((int32_t)((int32_t)L_2%(int32_t)2)))
		{
			goto IL_0068;
		}
	}
	{
		// yield return count - (count % 2) - i;
		int32_t L_3 = __this->get_count_3();
		int32_t L_4 = __this->get_count_3();
		int32_t L_5 = __this->get_U3CiU3E5__2_5();
		int32_t L_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)((int32_t)((int32_t)L_4%(int32_t)2)))), (int32_t)L_5));
		RuntimeObject * L_7 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_6);
		__this->set_U3CU3E2__current_1(L_7);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005f:
	{
		__this->set_U3CU3E1__state_0((-1));
		goto IL_0089;
	}

IL_0068:
	{
		// else yield return i;
		int32_t L_8 = __this->get_U3CiU3E5__2_5();
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_9);
		__this->set_U3CU3E2__current_1(L_10);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0082:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0089:
	{
		// for (int i = 0; i < count; ++i)
		int32_t L_11 = __this->get_U3CiU3E5__2_5();
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
		int32_t L_12 = V_1;
		__this->set_U3CiU3E5__2_5(L_12);
	}

IL_0099:
	{
		// for (int i = 0; i < count; ++i)
		int32_t L_13 = __this->get_U3CiU3E5__2_5();
		int32_t L_14 = __this->get_count_3();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_002b;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE_RuntimeMethod_var)));
	}
}
// System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * V_0 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_1 = __this->get_U3CU3El__initialThreadId_2();
		int32_t L_2;
		L_2 = Environment_get_CurrentManagedThreadId_m09DBD4166BFD399056B2F81C77A3A182339BF92D(/*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0022;
		}
	}
	{
		__this->set_U3CU3E1__state_0(0);
		V_0 = __this;
		goto IL_0029;
	}

IL_0022:
	{
		U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * L_3 = (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB *)il2cpp_codegen_object_new(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_il2cpp_TypeInfo_var);
		U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10(L_3, 0, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0029:
	{
		U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * L_4 = V_0;
		int32_t L_5 = __this->get_U3CU3E3__count_4();
		NullCheck(L_4);
		L_4->set_count_3(L_5);
		U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * L_6 = V_0;
		return L_6;
	}
}
// System.Collections.IEnumerator Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6 (U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0;
		L_0 = U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566 (ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * __this, bool ___enabled0, int32_t ___order1, int32_t ___iterations2, const RuntimeMethod* method)
{
	{
		// this.enabled = enabled;
		bool L_0 = ___enabled0;
		__this->set_enabled_3(L_0);
		// this.iterations = iterations;
		int32_t L_1 = ___iterations2;
		__this->set_iterations_1(L_1);
		// this.evaluationOrder = order;
		int32_t L_2 = ___order1;
		__this->set_evaluationOrder_0(L_2);
		// this.SORFactor = 1;
		__this->set_SORFactor_2((1.0f));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk (RuntimeObject * __this, bool ___enabled0, int32_t ___order1, int32_t ___iterations2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 * _thisAdjusted = reinterpret_cast<ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22 *>(__this + _offset);
	ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566(_thisAdjusted, ___enabled0, ___order1, ___iterations2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_pinvoke(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke& marshaled)
{
	marshaled.___start_0 = unmarshaled.get_start_0();
	marshaled.___end_1 = unmarshaled.get_end_1();
	marshaled.___info_2 = unmarshaled.get_info_2();
	marshaled.___pad_3 = unmarshaled.get_pad_3();
	il2cpp_codegen_marshal_string_fixed(unmarshaled.get_name_4(), (char*)&marshaled.___name_4, 64);
}
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_pinvoke_back(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke& marshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled)
{
	double unmarshaled_start_temp_0 = 0.0;
	unmarshaled_start_temp_0 = marshaled.___start_0;
	unmarshaled.set_start_0(unmarshaled_start_temp_0);
	double unmarshaled_end_temp_1 = 0.0;
	unmarshaled_end_temp_1 = marshaled.___end_1;
	unmarshaled.set_end_1(unmarshaled_end_temp_1);
	uint32_t unmarshaled_info_temp_2 = 0;
	unmarshaled_info_temp_2 = marshaled.___info_2;
	unmarshaled.set_info_2(unmarshaled_info_temp_2);
	int32_t unmarshaled_pad_temp_3 = 0;
	unmarshaled_pad_temp_3 = marshaled.___pad_3;
	unmarshaled.set_pad_3(unmarshaled_pad_temp_3);
	unmarshaled.set_name_4(il2cpp_codegen_marshal_string_result(marshaled.___name_4));
}
// Conversion method for clean up from marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_pinvoke_cleanup(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_com(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com& marshaled)
{
	marshaled.___start_0 = unmarshaled.get_start_0();
	marshaled.___end_1 = unmarshaled.get_end_1();
	marshaled.___info_2 = unmarshaled.get_info_2();
	marshaled.___pad_3 = unmarshaled.get_pad_3();
	il2cpp_codegen_marshal_string_fixed(unmarshaled.get_name_4(), (char*)&marshaled.___name_4, 64);
}
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_com_back(const ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com& marshaled, ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A& unmarshaled)
{
	double unmarshaled_start_temp_0 = 0.0;
	unmarshaled_start_temp_0 = marshaled.___start_0;
	unmarshaled.set_start_0(unmarshaled_start_temp_0);
	double unmarshaled_end_temp_1 = 0.0;
	unmarshaled_end_temp_1 = marshaled.___end_1;
	unmarshaled.set_end_1(unmarshaled_end_temp_1);
	uint32_t unmarshaled_info_temp_2 = 0;
	unmarshaled_info_temp_2 = marshaled.___info_2;
	unmarshaled.set_info_2(unmarshaled_info_temp_2);
	int32_t unmarshaled_pad_temp_3 = 0;
	unmarshaled_pad_temp_3 = marshaled.___pad_3;
	unmarshaled.set_pad_3(unmarshaled_pad_temp_3);
	unmarshaled.set_name_4(il2cpp_codegen_marshal_string_result(marshaled.___name_4));
}
// Conversion method for clean up from marshalling of: Oni/ProfileInfo
IL2CPP_EXTERN_C void ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshal_com_cleanup(ProfileInfo_tA9F49050C9E8A3448CF4952B0522D20406D5973A_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3 (SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * __this, int32_t ___interpolation0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___gravity1, const RuntimeMethod* method)
{
	{
		// this.mode = Mode.Mode3D;
		__this->set_mode_0(0);
		// this.gravity = gravity;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0 = ___gravity1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_0, /*hidden argument*/NULL);
		__this->set_gravity_2(L_1);
		// this.interpolation = interpolation;
		int32_t L_2 = ___interpolation0;
		__this->set_interpolation_1(L_2);
		// damping = 0;
		__this->set_damping_3((0.0f));
		// shockPropagation = 0;
		__this->set_shockPropagation_9((0.0f));
		// surfaceCollisionIterations = 8;
		__this->set_surfaceCollisionIterations_10(8);
		// surfaceCollisionTolerance = 0.005f;
		__this->set_surfaceCollisionTolerance_11((0.00499999989f));
		// maxAnisotropy = 3;
		__this->set_maxAnisotropy_4((3.0f));
		// maxDepenetration = 10;
		__this->set_maxDepenetration_7((10.0f));
		// sleepThreshold = 0.0005f;
		__this->set_sleepThreshold_5((0.000500000024f));
		// collisionMargin = 0.02f;
		__this->set_collisionMargin_6((0.0199999996f));
		// continuousCollisionDetection = 1;
		__this->set_continuousCollisionDetection_8((1.0f));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk (RuntimeObject * __this, int32_t ___interpolation0, Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___gravity1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 * _thisAdjusted = reinterpret_cast<SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6 *>(__this + _offset);
	SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3(_thisAdjusted, ___interpolation0, ___gravity1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Obi.ParticleGrid/CalculateCellCoords
IL2CPP_EXTERN_C void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshal_pinvoke(const CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B& unmarshaled, CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_pinvoke& marshaled)
{
	Exception_t* ___simplexBounds_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'simplexBounds' of type 'CalculateCellCoords'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___simplexBounds_0Exception, NULL);
}
IL2CPP_EXTERN_C void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshal_pinvoke_back(const CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_pinvoke& marshaled, CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B& unmarshaled)
{
	Exception_t* ___simplexBounds_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'simplexBounds' of type 'CalculateCellCoords'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___simplexBounds_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/CalculateCellCoords
IL2CPP_EXTERN_C void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshal_pinvoke_cleanup(CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Obi.ParticleGrid/CalculateCellCoords
IL2CPP_EXTERN_C void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshal_com(const CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B& unmarshaled, CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_com& marshaled)
{
	Exception_t* ___simplexBounds_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'simplexBounds' of type 'CalculateCellCoords'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___simplexBounds_0Exception, NULL);
}
IL2CPP_EXTERN_C void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshal_com_back(const CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_com& marshaled, CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B& unmarshaled)
{
	Exception_t* ___simplexBounds_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'simplexBounds' of type 'CalculateCellCoords'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___simplexBounds_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/CalculateCellCoords
IL2CPP_EXTERN_C void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshal_com_cleanup(CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_marshaled_com& marshaled)
{
}
// System.Void Obi.ParticleGrid/CalculateCellCoords::Execute(System.Int32)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955 (CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  V_2;
	memset((&V_2), 0, sizeof(V_2));
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  V_3;
	memset((&V_3), 0, sizeof(V_3));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		// int level = NativeMultilevelGrid<int>.GridLevelForSize(simplexBounds[i].AverageAxisLength());
		NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 * L_0 = __this->get_address_of_simplexBounds_0();
		int32_t L_1 = ___i0;
		BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  L_2;
		L_2 = IL2CPP_NATIVEARRAY_GET_ITEM(BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 , ((NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 *)L_0)->___m_Buffer_0, L_1);
		V_3 = L_2;
		float L_3;
		L_3 = BurstAabb_AverageAxisLength_mC3D10DA8BEC0F0BA5B418EDB620BDCD4A2951C8E((BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 *)(&V_3), /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_inline(L_3, /*hidden argument*/NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_RuntimeMethod_var);
		V_0 = L_4;
		// float cellSize = NativeMultilevelGrid<int>.CellSizeOfLevel(level);
		int32_t L_5 = V_0;
		float L_6;
		L_6 = NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_inline(L_5, /*hidden argument*/NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_RuntimeMethod_var);
		V_1 = L_6;
		// int4 newCellCoord = new int4(GridHash.Quantize(simplexBounds[i].center.xyz, cellSize), level);
		NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 * L_7 = __this->get_address_of_simplexBounds_0();
		int32_t L_8 = ___i0;
		BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  L_9;
		L_9 = IL2CPP_NATIVEARRAY_GET_ITEM(BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 , ((NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 *)L_7)->___m_Buffer_0, L_8);
		V_3 = L_9;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10;
		L_10 = BurstAabb_get_center_mEF7385D666BE5EE77B04D4C1B507CC277498DF83((BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 *)(&V_3), /*hidden argument*/NULL);
		V_4 = L_10;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_11;
		L_11 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_4), /*hidden argument*/NULL);
		float L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_il2cpp_TypeInfo_var);
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_13;
		L_13 = GridHash_Quantize_m67808299EA16142EAEEEAF0E4D4B9B4CADA9E320(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_14 = V_0;
		int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)(&V_2), L_13, L_14, /*hidden argument*/NULL);
		// if (is2D) newCellCoord[2] = 0;
		bool L_15 = __this->get_is2D_2();
		if (!L_15)
		{
			goto IL_005d;
		}
	}
	{
		// if (is2D) newCellCoord[2] = 0;
		int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)(&V_2), 2, 0, /*hidden argument*/NULL);
	}

IL_005d:
	{
		// cellCoords[i] = newCellCoord;
		NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C * L_16 = __this->get_address_of_cellCoords_1();
		int32_t L_17 = ___i0;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_18 = V_2;
		IL2CPP_NATIVEARRAY_SET_ITEM(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 , ((NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C *)L_16)->___m_Buffer_0, L_17, (L_18));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___i0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B * _thisAdjusted = reinterpret_cast<CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B *>(__this + _offset);
	CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955(_thisAdjusted, ___i0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Obi.ParticleGrid/GenerateParticleParticleContactsJob
IL2CPP_EXTERN_C void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshal_pinvoke(const GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5& unmarshaled, GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_pinvoke& marshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'GenerateParticleParticleContactsJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
IL2CPP_EXTERN_C void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshal_pinvoke_back(const GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_pinvoke& marshaled, GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5& unmarshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'GenerateParticleParticleContactsJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/GenerateParticleParticleContactsJob
IL2CPP_EXTERN_C void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshal_pinvoke_cleanup(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Obi.ParticleGrid/GenerateParticleParticleContactsJob
IL2CPP_EXTERN_C void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshal_com(const GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5& unmarshaled, GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_com& marshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'GenerateParticleParticleContactsJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
IL2CPP_EXTERN_C void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshal_com_back(const GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_com& marshaled, GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5& unmarshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'GenerateParticleParticleContactsJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/GenerateParticleParticleContactsJob
IL2CPP_EXTERN_C void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshal_com_cleanup(GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_marshaled_com& marshaled)
{
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::Execute(System.Int32)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___i0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D  V_0;
	memset((&V_0), 0, sizeof(V_0));
	BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// BurstSimplex simplexShape = new BurstSimplex()
		// {
		//     positions = restPositions,
		//     radii = radii,
		//     simplices = simplices,
		// };
		il2cpp_codegen_initobj((&V_1), sizeof(BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D ));
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_0 = __this->get_restPositions_4();
		(&V_1)->set_positions_0(L_0);
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_1 = __this->get_radii_8();
		(&V_1)->set_radii_1(L_1);
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  L_2 = __this->get_simplices_13();
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  L_3;
		L_3 = NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A(L_2, /*hidden argument*/NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_RuntimeMethod_var);
		(&V_1)->set_simplices_2(L_3);
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D  L_4 = V_1;
		V_0 = L_4;
		// IntraCellSearch(i, ref simplexShape);
		int32_t L_5 = ___i0;
		GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_5, (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)(&V_0), /*hidden argument*/NULL);
		// IntraLevelSearch(i, ref simplexShape);
		int32_t L_6 = ___i0;
		GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_6, (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)(&V_0), /*hidden argument*/NULL);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___i0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * _thisAdjusted = reinterpret_cast<GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *>(__this + _offset);
	GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2(_thisAdjusted, ___i0, method);
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::IntraCellSearch(System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// int cellLength = grid.usedCells[cellIndex].Length;
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_0 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_1 = L_0->get_address_of_usedCells_2();
		int32_t L_2 = ___cellIndex0;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_3;
		L_3 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_1, L_2, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_1 = L_3;
		int32_t L_4;
		L_4 = Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_1), /*hidden argument*/Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		V_0 = L_4;
		// for (int p = 0; p < cellLength; ++p)
		V_2 = 0;
		goto IL_006b;
	}

IL_001e:
	{
		// for (int n = p + 1; n < cellLength; ++n)
		int32_t L_5 = V_2;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
		goto IL_0063;
	}

IL_0024:
	{
		// InteractionTest(grid.usedCells[cellIndex][p], grid.usedCells[cellIndex][n], ref simplexShape);
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_6 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_7 = L_6->get_address_of_usedCells_2();
		int32_t L_8 = ___cellIndex0;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_9;
		L_9 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_7, L_8, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_1 = L_9;
		int32_t L_10 = V_2;
		int32_t L_11;
		L_11 = Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_1), L_10, /*hidden argument*/Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_12 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_13 = L_12->get_address_of_usedCells_2();
		int32_t L_14 = ___cellIndex0;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_15;
		L_15 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_13, L_14, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_1 = L_15;
		int32_t L_16 = V_3;
		int32_t L_17;
		L_17 = Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_1), L_16, /*hidden argument*/Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_18 = ___simplexShape1;
		GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_11, L_17, (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_18, /*hidden argument*/NULL);
		// for (int n = p + 1; n < cellLength; ++n)
		int32_t L_19 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0063:
	{
		// for (int n = p + 1; n < cellLength; ++n)
		int32_t L_20 = V_3;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0024;
		}
	}
	{
		// for (int p = 0; p < cellLength; ++p)
		int32_t L_22 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_006b:
	{
		// for (int p = 0; p < cellLength; ++p)
		int32_t L_23 = V_2;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_001e;
		}
	}
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * _thisAdjusted = reinterpret_cast<GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *>(__this + _offset);
	GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB(_thisAdjusted, ___cellIndex0, ___simplexShape1, method);
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::InterCellSearch(System.Int32,System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, int32_t ___neighborCellIndex1, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// int cellLength = grid.usedCells[cellIndex].Length;
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_0 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_1 = L_0->get_address_of_usedCells_2();
		int32_t L_2 = ___cellIndex0;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_3;
		L_3 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_1, L_2, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_2 = L_3;
		int32_t L_4;
		L_4 = Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_2), /*hidden argument*/Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		V_0 = L_4;
		// int neighborCellLength = grid.usedCells[neighborCellIndex].Length;
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_5 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_6 = L_5->get_address_of_usedCells_2();
		int32_t L_7 = ___neighborCellIndex1;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_8;
		L_8 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_6, L_7, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_2 = L_8;
		int32_t L_9;
		L_9 = Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_2), /*hidden argument*/Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		V_1 = L_9;
		// for (int p = 0; p < cellLength; ++p)
		V_3 = 0;
		goto IL_0088;
	}

IL_0038:
	{
		// for (int n = 0; n < neighborCellLength; ++n)
		V_4 = 0;
		goto IL_007f;
	}

IL_003d:
	{
		// InteractionTest(grid.usedCells[cellIndex][p], grid.usedCells[neighborCellIndex][n], ref simplexShape);
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_10 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_11 = L_10->get_address_of_usedCells_2();
		int32_t L_12 = ___cellIndex0;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_13;
		L_13 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_11, L_12, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_2 = L_13;
		int32_t L_14 = V_3;
		int32_t L_15;
		L_15 = Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_2), L_14, /*hidden argument*/Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_16 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_17 = L_16->get_address_of_usedCells_2();
		int32_t L_18 = ___neighborCellIndex1;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_19;
		L_19 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_17, L_18, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_2 = L_19;
		int32_t L_20 = V_4;
		int32_t L_21;
		L_21 = Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_2), L_20, /*hidden argument*/Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_22 = ___simplexShape2;
		GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_15, L_21, (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_22, /*hidden argument*/NULL);
		// for (int n = 0; n < neighborCellLength; ++n)
		int32_t L_23 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_007f:
	{
		// for (int n = 0; n < neighborCellLength; ++n)
		int32_t L_24 = V_4;
		int32_t L_25 = V_1;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_003d;
		}
	}
	{
		// for (int p = 0; p < cellLength; ++p)
		int32_t L_26 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
	}

IL_0088:
	{
		// for (int p = 0; p < cellLength; ++p)
		int32_t L_27 = V_3;
		int32_t L_28 = V_0;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_0038;
		}
	}
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, int32_t ___neighborCellIndex1, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * _thisAdjusted = reinterpret_cast<GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *>(__this + _offset);
	GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194(_thisAdjusted, ___cellIndex0, ___neighborCellIndex1, ___simplexShape2, method);
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::IntraLevelSearch(System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  V_4;
	memset((&V_4), 0, sizeof(V_4));
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  V_7;
	memset((&V_7), 0, sizeof(V_7));
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  V_11;
	memset((&V_11), 0, sizeof(V_11));
	int32_t V_12 = 0;
	{
		// int4 cellCoords = grid.usedCells[cellIndex].Coords;
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_0 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_1 = L_0->get_address_of_usedCells_2();
		int32_t L_2 = ___cellIndex0;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_3;
		L_3 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_1, L_2, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_2 = L_3;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4;
		L_4 = Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_inline((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_2), /*hidden argument*/Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_RuntimeMethod_var);
		V_0 = L_4;
		// for (int i = 0; i < 13; ++i)
		V_3 = 0;
		goto IL_0061;
	}

IL_001e:
	{
		// int4 neighborCellCoords = new int4(cellCoords.xyz + GridHash.cellOffsets3D[i], cellCoords.w);
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_5;
		L_5 = int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_inline((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)(&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_il2cpp_TypeInfo_var);
		int3U5BU5D_tFFAD09108E69A22E46D6BD643AD034FE69D707B2* L_6 = ((GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_StaticFields*)il2cpp_codegen_static_fields_for(GridHash_tEF0449B9131CFB7C906A8BA240D090EAFA76C3B7_il2cpp_TypeInfo_var))->get_cellOffsets3D_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_10;
		L_10 = int3_op_Addition_m08C6B9DB7119923A27914BAFD7DCDEE50CC988A4_inline(L_5, L_9, /*hidden argument*/NULL);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_11 = V_0;
		int32_t L_12 = L_11.get_w_3();
		int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)(&V_4), L_10, L_12, /*hidden argument*/NULL);
		// if (grid.TryGetCellIndex(neighborCellCoords, out neighborCellIndex))
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_13 = __this->get_address_of_grid_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_14 = V_4;
		bool L_15;
		L_15 = NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E((NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *)L_13, L_14, (int32_t*)(&V_5), /*hidden argument*/NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_RuntimeMethod_var);
		if (!L_15)
		{
			goto IL_005d;
		}
	}
	{
		// InterCellSearch(cellIndex, neighborCellIndex, ref simplexShape);
		int32_t L_16 = ___cellIndex0;
		int32_t L_17 = V_5;
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_18 = ___simplexShape1;
		GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_16, L_17, (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_18, /*hidden argument*/NULL);
	}

IL_005d:
	{
		// for (int i = 0; i < 13; ++i)
		int32_t L_19 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0061:
	{
		// for (int i = 0; i < 13; ++i)
		int32_t L_20 = V_3;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)13))))
		{
			goto IL_001e;
		}
	}
	{
		// int levelIndex = gridLevels.IndexOf<int,int>(cellCoords.w);
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  L_21 = __this->get_gridLevels_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_22 = V_0;
		int32_t L_23 = L_22.get_w_3();
		int32_t L_24;
		L_24 = NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610(L_21, L_23, /*hidden argument*/NativeArrayExtensions_IndexOf_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_mB3E02A8C0D75D2DE5EFE763A97D4B731368AD610_RuntimeMethod_var);
		V_1 = L_24;
		// if (levelIndex >= 0)
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) < ((int32_t)0)))
		{
			goto IL_0112;
		}
	}
	{
		// levelIndex++;
		int32_t L_26 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
		goto IL_0101;
	}

IL_0085:
	{
		// int level = gridLevels[levelIndex];
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_27 = __this->get_address_of_gridLevels_1();
		int32_t L_28 = V_1;
		int32_t L_29;
		L_29 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_27)->___m_Buffer_0, L_28);
		V_6 = L_29;
		// int4 parentCellCoords = NativeMultilevelGrid<int>.GetParentCellCoords(cellCoords, level);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_30 = V_0;
		int32_t L_31 = V_6;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_32;
		L_32 = NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA(L_30, L_31, /*hidden argument*/NativeMultilevelGrid_1_GetParentCellCoords_m64E4EE6ACA595CCCC4EBB2F6528EA497AD2751EA_RuntimeMethod_var);
		V_7 = L_32;
		// for (int x = -1; x <= 1; ++x)
		V_8 = (-1);
		goto IL_00f8;
	}

IL_00a2:
	{
		// for (int y = -1; y <= 1; ++y)
		V_9 = (-1);
		goto IL_00ed;
	}

IL_00a7:
	{
		// for (int z = -1; z <= 1; ++z)
		V_10 = (-1);
		goto IL_00e2;
	}

IL_00ac:
	{
		// int4 neighborCellCoords = parentCellCoords + new int4(x, y, z, 0);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_33 = V_7;
		int32_t L_34 = V_8;
		int32_t L_35 = V_9;
		int32_t L_36 = V_10;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_37;
		memset((&L_37), 0, sizeof(L_37));
		int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline((&L_37), L_34, L_35, L_36, 0, /*hidden argument*/NULL);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_38;
		L_38 = int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3_inline(L_33, L_37, /*hidden argument*/NULL);
		V_11 = L_38;
		// if (grid.TryGetCellIndex(neighborCellCoords, out neighborCellIndex))
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_39 = __this->get_address_of_grid_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_40 = V_11;
		bool L_41;
		L_41 = NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E((NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *)L_39, L_40, (int32_t*)(&V_12), /*hidden argument*/NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_RuntimeMethod_var);
		if (!L_41)
		{
			goto IL_00dc;
		}
	}
	{
		// InterCellSearch(cellIndex, neighborCellIndex, ref simplexShape);
		int32_t L_42 = ___cellIndex0;
		int32_t L_43 = V_12;
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_44 = ___simplexShape1;
		GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_42, L_43, (BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_44, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		// for (int z = -1; z <= 1; ++z)
		int32_t L_45 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_45, (int32_t)1));
	}

IL_00e2:
	{
		// for (int z = -1; z <= 1; ++z)
		int32_t L_46 = V_10;
		if ((((int32_t)L_46) <= ((int32_t)1)))
		{
			goto IL_00ac;
		}
	}
	{
		// for (int y = -1; y <= 1; ++y)
		int32_t L_47 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_00ed:
	{
		// for (int y = -1; y <= 1; ++y)
		int32_t L_48 = V_9;
		if ((((int32_t)L_48) <= ((int32_t)1)))
		{
			goto IL_00a7;
		}
	}
	{
		// for (int x = -1; x <= 1; ++x)
		int32_t L_49 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_00f8:
	{
		// for (int x = -1; x <= 1; ++x)
		int32_t L_50 = V_8;
		if ((((int32_t)L_50) <= ((int32_t)1)))
		{
			goto IL_00a2;
		}
	}
	{
		// for (; levelIndex < gridLevels.Length; ++levelIndex)
		int32_t L_51 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_0101:
	{
		// for (; levelIndex < gridLevels.Length; ++levelIndex)
		int32_t L_52 = V_1;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_53 = __this->get_address_of_gridLevels_1();
		int32_t L_54;
		L_54 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_53)->___m_Length_1);
		if ((((int32_t)L_52) < ((int32_t)L_54)))
		{
			goto IL_0085;
		}
	}

IL_0112:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___cellIndex0, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * _thisAdjusted = reinterpret_cast<GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *>(__this + _offset);
	GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418(_thisAdjusted, ___cellIndex0, ___simplexShape1, method);
}
// System.Int32 Obi.ParticleGrid/GenerateParticleParticleContactsJob::GetSimplexGroup(System.Int32,System.Int32,Obi.ObiUtils/ParticleFlags&,System.Int32&,System.Int32&,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR int32_t GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___simplexStart0, int32_t ___simplexSize1, int32_t* ___flags2, int32_t* ___category3, int32_t* ___mask4, bool* ___restPositionsEnabled5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// flags = 0;
		int32_t* L_0 = ___flags2;
		*((int32_t*)L_0) = (int32_t)0;
		// int group = 0;
		V_0 = 0;
		// category = 0;
		int32_t* L_1 = ___category3;
		*((int32_t*)L_1) = (int32_t)0;
		// mask = 0;
		int32_t* L_2 = ___mask4;
		*((int32_t*)L_2) = (int32_t)0;
		// for (int j = 0; j < simplexSize; ++j)
		V_1 = 0;
		goto IL_00a9;
	}

IL_0014:
	{
		// int particleIndex = simplices[simplexStart + j];
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_3 = __this->get_address_of_simplices_13();
		int32_t L_4 = ___simplexStart0;
		int32_t L_5 = V_1;
		int32_t L_6;
		L_6 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_3, ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		V_2 = L_6;
		// flags |= ObiUtils.GetFlagsFromPhase(phases[particleIndex]);
		int32_t* L_7 = ___flags2;
		int32_t* L_8 = ___flags2;
		int32_t L_9 = *((int32_t*)L_8);
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_10 = __this->get_address_of_phases_11();
		int32_t L_11 = V_2;
		int32_t L_12;
		L_12 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_10)->___m_Buffer_0, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		int32_t L_13;
		L_13 = ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57_inline(L_12, /*hidden argument*/NULL);
		*((int32_t*)L_7) = (int32_t)((int32_t)((int32_t)L_9|(int32_t)L_13));
		// category |= filters[particleIndex] & ObiUtils.FilterCategoryBitmask;
		int32_t* L_14 = ___category3;
		int32_t* L_15 = ___category3;
		int32_t L_16 = *((int32_t*)L_15);
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_17 = __this->get_address_of_filters_12();
		int32_t L_18 = V_2;
		int32_t L_19;
		L_19 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_17)->___m_Buffer_0, L_18);
		*((int32_t*)L_14) = (int32_t)((int32_t)((int32_t)L_16|(int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)65535)))));
		// mask |= (filters[particleIndex] & ObiUtils.FilterMaskBitmask) >> 16;
		int32_t* L_20 = ___mask4;
		int32_t* L_21 = ___mask4;
		int32_t L_22 = *((int32_t*)L_21);
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_23 = __this->get_address_of_filters_12();
		int32_t L_24 = V_2;
		int32_t L_25;
		L_25 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_23)->___m_Buffer_0, L_24);
		*((int32_t*)L_20) = (int32_t)((int32_t)((int32_t)L_22|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_25&(int32_t)((int32_t)-65536)))>>(int32_t)((int32_t)16)))));
		// group = math.max(group, ObiUtils.GetGroupFromPhase(phases[particleIndex]));
		int32_t L_26 = V_0;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_27 = __this->get_address_of_phases_11();
		int32_t L_28 = V_2;
		int32_t L_29;
		L_29 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_27)->___m_Buffer_0, L_28);
		int32_t L_30;
		L_30 = ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054_inline(L_29, /*hidden argument*/NULL);
		int32_t L_31;
		L_31 = math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF_inline(L_26, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		// restPositionsEnabled |= restPositions[particleIndex].w > 0.5f;
		bool* L_32 = ___restPositionsEnabled5;
		bool* L_33 = ___restPositionsEnabled5;
		int32_t L_34 = *((uint8_t*)L_33);
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_35 = __this->get_address_of_restPositions_4();
		int32_t L_36 = V_2;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_37;
		L_37 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_35)->___m_Buffer_0, L_36);
		float L_38 = L_37.get_w_3();
		*((int8_t*)L_32) = (int8_t)((int32_t)((int32_t)L_34|(int32_t)((((float)L_38) > ((float)(0.5f)))? 1 : 0)));
		// for (int j = 0; j < simplexSize; ++j)
		int32_t L_39 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
	}

IL_00a9:
	{
		// for (int j = 0; j < simplexSize; ++j)
		int32_t L_40 = V_1;
		int32_t L_41 = ___simplexSize1;
		if ((((int32_t)L_40) < ((int32_t)L_41)))
		{
			goto IL_0014;
		}
	}
	{
		// return group;
		int32_t L_42 = V_0;
		return L_42;
	}
}
IL2CPP_EXTERN_C  int32_t GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___simplexStart0, int32_t ___simplexSize1, int32_t* ___flags2, int32_t* ___category3, int32_t* ___mask4, bool* ___restPositionsEnabled5, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * _thisAdjusted = reinterpret_cast<GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937(_thisAdjusted, ___simplexStart0, ___simplexSize1, ___flags2, ___category3, ___mask4, ___restPositionsEnabled5, method);
	return _returnValue;
}
// System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::InteractionTest(System.Int32,System.Int32,Obi.BurstSimplex&)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7 (GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___A0, int32_t ___B1, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_Swap_TisParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_mDA8D45AAE52ED72F3C11941CBD419D8C7AD4463D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  V_13;
	memset((&V_13), 0, sizeof(V_13));
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  V_14;
	memset((&V_14), 0, sizeof(V_14));
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_19;
	memset((&V_19), 0, sizeof(V_19));
	float V_20 = 0.0f;
	FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D  V_21;
	memset((&V_21), 0, sizeof(V_21));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_22;
	memset((&V_22), 0, sizeof(V_22));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_23;
	memset((&V_23), 0, sizeof(V_23));
	float V_24 = 0.0f;
	float V_25 = 0.0f;
	SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  V_26;
	memset((&V_26), 0, sizeof(V_26));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_27;
	memset((&V_27), 0, sizeof(V_27));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_28;
	memset((&V_28), 0, sizeof(V_28));
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_29;
	memset((&V_29), 0, sizeof(V_29));
	float V_30 = 0.0f;
	SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  V_31;
	memset((&V_31), 0, sizeof(V_31));
	int32_t V_32 = 0;
	int32_t V_33 = 0;
	int32_t V_34 = 0;
	int32_t V_35 = 0;
	int32_t V_36 = 0;
	int32_t V_37 = 0;
	BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C  V_38;
	memset((&V_38), 0, sizeof(V_38));
	{
		// if (!simplexBounds[A].IntersectsAabb(simplexBounds[B]))
		NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 * L_0 = __this->get_address_of_simplexBounds_15();
		int32_t L_1 = ___A0;
		BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  L_2;
		L_2 = IL2CPP_NATIVEARRAY_GET_ITEM(BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 , ((NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 *)L_0)->___m_Buffer_0, L_1);
		V_13 = L_2;
		NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 * L_3 = __this->get_address_of_simplexBounds_15();
		int32_t L_4 = ___B1;
		BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87  L_5;
		L_5 = IL2CPP_NATIVEARRAY_GET_ITEM(BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 , ((NativeArray_1_t5B4D2A0EF9FD07248762BA54073B16AE493458F8 *)L_3)->___m_Buffer_0, L_4);
		V_14 = L_5;
		bool L_6;
		L_6 = BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6((BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 *)(&V_13), (BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87 *)(&V_14), (bool)0, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		// return;
		return;
	}

IL_0029:
	{
		// int simplexStartA = simplexCounts.GetSimplexStartAndSize(A, out int simplexSizeA);
		SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA * L_7 = __this->get_address_of_simplexCounts_14();
		int32_t L_8 = ___A0;
		int32_t L_9;
		L_9 = SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95((SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA *)L_7, L_8, (int32_t*)(&V_1), /*hidden argument*/NULL);
		V_0 = L_9;
		// int simplexStartB = simplexCounts.GetSimplexStartAndSize(B, out int simplexSizeB);
		SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA * L_10 = __this->get_address_of_simplexCounts_14();
		int32_t L_11 = ___B1;
		int32_t L_12;
		L_12 = SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95((SimplexCounts_tFB0EA16560E4982D17631534EF16ACE61CFA1EFA *)L_10, L_11, (int32_t*)(&V_3), /*hidden argument*/NULL);
		V_2 = L_12;
		// for (int a = 0; a < simplexSizeA; ++a)
		V_15 = 0;
		goto IL_0083;
	}

IL_004c:
	{
		// for (int b = 0; b < simplexSizeB; ++b)
		V_16 = 0;
		goto IL_0078;
	}

IL_0051:
	{
		// if (simplices[simplexStartA + a] == simplices[simplexStartB + b])
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_13 = __this->get_address_of_simplices_13();
		int32_t L_14 = V_0;
		int32_t L_15 = V_15;
		int32_t L_16;
		L_16 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_13, ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)L_15)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_17 = __this->get_address_of_simplices_13();
		int32_t L_18 = V_2;
		int32_t L_19 = V_16;
		int32_t L_20;
		L_20 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_17, ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)L_19)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		if ((!(((uint32_t)L_16) == ((uint32_t)L_20))))
		{
			goto IL_0072;
		}
	}
	{
		// return;
		return;
	}

IL_0072:
	{
		// for (int b = 0; b < simplexSizeB; ++b)
		int32_t L_21 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0078:
	{
		// for (int b = 0; b < simplexSizeB; ++b)
		int32_t L_22 = V_16;
		int32_t L_23 = V_3;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0051;
		}
	}
	{
		// for (int a = 0; a < simplexSizeA; ++a)
		int32_t L_24 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)1));
	}

IL_0083:
	{
		// for (int a = 0; a < simplexSizeA; ++a)
		int32_t L_25 = V_15;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_004c;
		}
	}
	{
		// bool restPositionsEnabled = false;
		V_4 = (bool)0;
		// int groupA = GetSimplexGroup(simplexStartA, simplexSizeA, out ObiUtils.ParticleFlags flagsA, out int categoryA, out int maskA, ref restPositionsEnabled);
		int32_t L_27 = V_0;
		int32_t L_28 = V_1;
		int32_t L_29;
		L_29 = GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_27, L_28, (int32_t*)(&V_6), (int32_t*)(&V_7), (int32_t*)(&V_8), (bool*)(&V_4), /*hidden argument*/NULL);
		V_5 = L_29;
		// int groupB = GetSimplexGroup(simplexStartB, simplexSizeB, out ObiUtils.ParticleFlags flagsB, out int categoryB, out int maskB, ref restPositionsEnabled);
		int32_t L_30 = V_2;
		int32_t L_31 = V_3;
		int32_t L_32;
		L_32 = GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937((GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *)__this, L_30, L_31, (int32_t*)(&V_10), (int32_t*)(&V_11), (int32_t*)(&V_12), (bool*)(&V_4), /*hidden argument*/NULL);
		V_9 = L_32;
		// if (groupA == groupB)
		int32_t L_33 = V_5;
		int32_t L_34 = V_9;
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_00c3;
		}
	}
	{
		// if ((flagsA & flagsB & ObiUtils.ParticleFlags.SelfCollide) == 0)
		int32_t L_35 = V_6;
		int32_t L_36 = V_10;
		if (((int32_t)((int32_t)((int32_t)((int32_t)L_35&(int32_t)L_36))&(int32_t)((int32_t)16777216))))
		{
			goto IL_00d2;
		}
	}
	{
		// return;
		return;
	}

IL_00c3:
	{
		// else if ((maskA & categoryB) == 0 || (maskB & categoryA) == 0)
		int32_t L_37 = V_8;
		int32_t L_38 = V_11;
		if (!((int32_t)((int32_t)L_37&(int32_t)L_38)))
		{
			goto IL_00d1;
		}
	}
	{
		int32_t L_39 = V_12;
		int32_t L_40 = V_7;
		if (((int32_t)((int32_t)L_39&(int32_t)L_40)))
		{
			goto IL_00d2;
		}
	}

IL_00d1:
	{
		// return;
		return;
	}

IL_00d2:
	{
		// if ((flagsA & ObiUtils.ParticleFlags.Fluid) != 0 && (flagsB & ObiUtils.ParticleFlags.Fluid) != 0)
		int32_t L_41 = V_6;
		if (!((int32_t)((int32_t)L_41&(int32_t)((int32_t)33554432))))
		{
			goto IL_01bd;
		}
	}
	{
		int32_t L_42 = V_10;
		if (!((int32_t)((int32_t)L_42&(int32_t)((int32_t)33554432))))
		{
			goto IL_01bd;
		}
	}
	{
		// int particleA = simplices[simplexStartA];
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_43 = __this->get_address_of_simplices_13();
		int32_t L_44 = V_0;
		int32_t L_45;
		L_45 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_43, L_44, /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		V_17 = L_45;
		// int particleB = simplices[simplexStartB];
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_46 = __this->get_address_of_simplices_13();
		int32_t L_47 = V_2;
		int32_t L_48;
		L_48 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_46, L_47, /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		V_18 = L_48;
		// float4 predictedPositionA = positions[particleA] + velocities[particleA] * dt;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_49 = __this->get_address_of_positions_2();
		int32_t L_50 = V_17;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_51;
		L_51 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_49)->___m_Buffer_0, L_50);
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_52 = __this->get_address_of_velocities_6();
		int32_t L_53 = V_17;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_54;
		L_54 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_52)->___m_Buffer_0, L_53);
		float L_55 = __this->get_dt_20();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_56;
		L_56 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_54, L_55, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_57;
		L_57 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_51, L_56, /*hidden argument*/NULL);
		// float4 predictedPositionB = positions[particleB] + velocities[particleB] * dt;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_58 = __this->get_address_of_positions_2();
		int32_t L_59 = V_18;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_60;
		L_60 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_58)->___m_Buffer_0, L_59);
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_61 = __this->get_address_of_velocities_6();
		int32_t L_62 = V_18;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_63;
		L_63 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_61)->___m_Buffer_0, L_62);
		float L_64 = __this->get_dt_20();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_65;
		L_65 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_63, L_64, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_66;
		L_66 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_60, L_65, /*hidden argument*/NULL);
		V_19 = L_66;
		// float d2 = math.lengthsq(predictedPositionA - predictedPositionB);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_67 = V_19;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_68;
		L_68 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_57, L_67, /*hidden argument*/NULL);
		float L_69;
		L_69 = math_lengthsq_mD422A214358E935793F5ED10991D70F040848F2D_inline(L_68, /*hidden argument*/NULL);
		// float fluidDistance = math.max(fluidRadii[particleA], fluidRadii[particleB]);
		NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 * L_70 = __this->get_address_of_fluidRadii_10();
		int32_t L_71 = V_17;
		float L_72;
		L_72 = IL2CPP_NATIVEARRAY_GET_ITEM(float, ((NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 *)L_70)->___m_Buffer_0, L_71);
		NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 * L_73 = __this->get_address_of_fluidRadii_10();
		int32_t L_74 = V_18;
		float L_75;
		L_75 = IL2CPP_NATIVEARRAY_GET_ITEM(float, ((NativeArray_1_t5F920DC5A531D604D161A0FAD3479B5BFE0D9232 *)L_73)->___m_Buffer_0, L_74);
		float L_76;
		L_76 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline(L_72, L_75, /*hidden argument*/NULL);
		V_20 = L_76;
		// if (d2 <= fluidDistance * fluidDistance)
		float L_77 = V_20;
		float L_78 = V_20;
		if ((!(((float)L_69) <= ((float)((float)il2cpp_codegen_multiply((float)L_77, (float)L_78))))))
		{
			goto IL_053c;
		}
	}
	{
		// fluidInteractionsQueue.Enqueue(new FluidInteraction { particleA = particleA, particleB = particleB });
		ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 * L_79 = __this->get_address_of_fluidInteractionsQueue_19();
		il2cpp_codegen_initobj((&V_21), sizeof(FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D ));
		int32_t L_80 = V_17;
		(&V_21)->set_particleA_3(L_80);
		int32_t L_81 = V_18;
		(&V_21)->set_particleB_4(L_81);
		FluidInteraction_tE40B119C020BBC8326BCCFB5F44B117B6698F85D  L_82 = V_21;
		ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01((ParallelWriter_t3E7031D9BAC94378A25CAEBF9379C151B287C878 *)L_79, L_82, /*hidden argument*/ParallelWriter_Enqueue_m3F63FAF2D36D0A08A1120E4217DFBB0BD3A1DA01_RuntimeMethod_var);
		// }
		return;
	}

IL_01bd:
	{
		// if ((flagsA & ObiUtils.ParticleFlags.OneSided) != 0 && categoryA < categoryB)
		int32_t L_83 = V_6;
		if (!((int32_t)((int32_t)L_83&(int32_t)((int32_t)67108864))))
		{
			goto IL_01fa;
		}
	}
	{
		int32_t L_84 = V_7;
		int32_t L_85 = V_11;
		if ((((int32_t)L_84) >= ((int32_t)L_85)))
		{
			goto IL_01fa;
		}
	}
	{
		// ObiUtils.Swap(ref A, ref B);
		IL2CPP_RUNTIME_CLASS_INIT(ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_il2cpp_TypeInfo_var);
		ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_inline((int32_t*)(&___A0), (int32_t*)(&___B1), /*hidden argument*/ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_RuntimeMethod_var);
		// ObiUtils.Swap(ref simplexStartA, ref simplexStartB);
		ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_inline((int32_t*)(&V_0), (int32_t*)(&V_2), /*hidden argument*/ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_RuntimeMethod_var);
		// ObiUtils.Swap(ref simplexSizeA, ref simplexSizeB);
		ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_inline((int32_t*)(&V_1), (int32_t*)(&V_3), /*hidden argument*/ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_RuntimeMethod_var);
		// ObiUtils.Swap(ref flagsA, ref flagsB);
		ObiUtils_Swap_TisParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_mDA8D45AAE52ED72F3C11941CBD419D8C7AD4463D_inline((int32_t*)(&V_6), (int32_t*)(&V_10), /*hidden argument*/ObiUtils_Swap_TisParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_mDA8D45AAE52ED72F3C11941CBD419D8C7AD4463D_RuntimeMethod_var);
		// ObiUtils.Swap(ref groupA, ref groupB);
		ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_inline((int32_t*)(&V_5), (int32_t*)(&V_9), /*hidden argument*/ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_RuntimeMethod_var);
	}

IL_01fa:
	{
		// float4 simplexBary = BurstMath.BarycenterForSimplexOfSize(simplexSizeA);
		int32_t L_86 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_il2cpp_TypeInfo_var);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_87;
		L_87 = BurstMath_BarycenterForSimplexOfSize_m26CB914EE114D2DE19C4F75C97009A695F6C076A_inline(L_86, /*hidden argument*/NULL);
		V_22 = L_87;
		// simplexShape.simplexStart = simplexStartB;
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_88 = ___simplexShape2;
		int32_t L_89 = V_2;
		L_88->set_simplexStart_3(L_89);
		// simplexShape.simplexSize = simplexSizeB;
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_90 = ___simplexShape2;
		int32_t L_91 = V_3;
		L_90->set_simplexSize_4(L_91);
		// simplexShape.positions = restPositions;
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_92 = ___simplexShape2;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_93 = __this->get_restPositions_4();
		L_92->set_positions_0(L_93);
		// simplexShape.CacheData();
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_94 = ___simplexShape2;
		BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA((BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_94, /*hidden argument*/NULL);
		// float simplexRadiusA = 0, simplexRadiusB = 0;
		V_24 = (0.0f);
		// float simplexRadiusA = 0, simplexRadiusB = 0;
		V_25 = (0.0f);
		// if (groupA == groupB && restPositionsEnabled)
		int32_t L_95 = V_5;
		int32_t L_96 = V_9;
		bool L_97 = V_4;
		if (!((int32_t)((int32_t)((((int32_t)L_95) == ((int32_t)L_96))? 1 : 0)&(int32_t)L_97)))
		{
			goto IL_0312;
		}
	}
	{
		// var restPoint = BurstLocalOptimization.Optimize<BurstSimplex>(ref simplexShape, restPositions, restOrientations, radii,
		//                             simplices, simplexStartA, simplexSizeA, ref simplexBary, out simplexPoint, 4, 0);
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_98 = ___simplexShape2;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_99 = __this->get_restPositions_4();
		NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  L_100 = __this->get_restOrientations_5();
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_101 = __this->get_radii_8();
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  L_102 = __this->get_simplices_13();
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  L_103;
		L_103 = NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A(L_102, /*hidden argument*/NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_RuntimeMethod_var);
		int32_t L_104 = V_0;
		int32_t L_105 = V_1;
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_106;
		L_106 = BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63((BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_98, L_99, L_100, L_101, L_103, L_104, L_105, (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_22), (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_23), 4, (0.0f), /*hidden argument*/BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63_RuntimeMethod_var);
		V_31 = L_106;
		// for (int j = 0; j < simplexSizeA; ++j)
		V_32 = 0;
		goto IL_02a8;
	}

IL_0274:
	{
		// simplexRadiusA += radii[simplices[simplexStartA + j]].x * simplexBary[j];
		float L_107 = V_24;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_108 = __this->get_address_of_radii_8();
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_109 = __this->get_address_of_simplices_13();
		int32_t L_110 = V_0;
		int32_t L_111 = V_32;
		int32_t L_112;
		L_112 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_109, ((int32_t)il2cpp_codegen_add((int32_t)L_110, (int32_t)L_111)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_113;
		L_113 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_108)->___m_Buffer_0, L_112);
		float L_114 = L_113.get_x_0();
		int32_t L_115 = V_32;
		float L_116;
		L_116 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_22), L_115, /*hidden argument*/NULL);
		V_24 = ((float)il2cpp_codegen_add((float)L_107, (float)((float)il2cpp_codegen_multiply((float)L_114, (float)L_116))));
		// for (int j = 0; j < simplexSizeA; ++j)
		int32_t L_117 = V_32;
		V_32 = ((int32_t)il2cpp_codegen_add((int32_t)L_117, (int32_t)1));
	}

IL_02a8:
	{
		// for (int j = 0; j < simplexSizeA; ++j)
		int32_t L_118 = V_32;
		int32_t L_119 = V_1;
		if ((((int32_t)L_118) < ((int32_t)L_119)))
		{
			goto IL_0274;
		}
	}
	{
		// for (int j = 0; j < simplexSizeB; ++j)
		V_33 = 0;
		goto IL_02eb;
	}

IL_02b2:
	{
		// simplexRadiusB += radii[simplices[simplexStartB + j]].x * restPoint.bary[j];
		float L_120 = V_25;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_121 = __this->get_address_of_radii_8();
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_122 = __this->get_address_of_simplices_13();
		int32_t L_123 = V_2;
		int32_t L_124 = V_33;
		int32_t L_125;
		L_125 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_122, ((int32_t)il2cpp_codegen_add((int32_t)L_123, (int32_t)L_124)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_126;
		L_126 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_121)->___m_Buffer_0, L_125);
		float L_127 = L_126.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_128 = (&V_31)->get_address_of_bary_0();
		int32_t L_129 = V_33;
		float L_130;
		L_130 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_128, L_129, /*hidden argument*/NULL);
		V_25 = ((float)il2cpp_codegen_add((float)L_120, (float)((float)il2cpp_codegen_multiply((float)L_127, (float)L_130))));
		// for (int j = 0; j < simplexSizeB; ++j)
		int32_t L_131 = V_33;
		V_33 = ((int32_t)il2cpp_codegen_add((int32_t)L_131, (int32_t)1));
	}

IL_02eb:
	{
		// for (int j = 0; j < simplexSizeB; ++j)
		int32_t L_132 = V_33;
		int32_t L_133 = V_3;
		if ((((int32_t)L_132) < ((int32_t)L_133)))
		{
			goto IL_02b2;
		}
	}
	{
		// if (math.dot(simplexPoint - restPoint.point, restPoint.normal) < simplexRadiusA + simplexRadiusB)
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_134 = V_23;
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_135 = V_31;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_136 = L_135.get_point_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_137;
		L_137 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_134, L_136, /*hidden argument*/NULL);
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_138 = V_31;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_139 = L_138.get_normal_2();
		float L_140;
		L_140 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_137, L_139, /*hidden argument*/NULL);
		float L_141 = V_24;
		float L_142 = V_25;
		if ((!(((float)L_140) < ((float)((float)il2cpp_codegen_add((float)L_141, (float)L_142))))))
		{
			goto IL_0312;
		}
	}
	{
		// return;
		return;
	}

IL_0312:
	{
		// simplexBary = BurstMath.BarycenterForSimplexOfSize(simplexSizeA);
		int32_t L_143 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_il2cpp_TypeInfo_var);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_144;
		L_144 = BurstMath_BarycenterForSimplexOfSize_m26CB914EE114D2DE19C4F75C97009A695F6C076A_inline(L_143, /*hidden argument*/NULL);
		V_22 = L_144;
		// simplexShape.positions = positions;
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_145 = ___simplexShape2;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_146 = __this->get_positions_2();
		L_145->set_positions_0(L_146);
		// simplexShape.CacheData();
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_147 = ___simplexShape2;
		BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA((BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_147, /*hidden argument*/NULL);
		// var surfacePoint = BurstLocalOptimization.Optimize<BurstSimplex>(ref simplexShape, positions, orientations, radii,
		//                     simplices, simplexStartA, simplexSizeA, ref simplexBary, out simplexPoint, optimizationIterations, optimizationTolerance);
		BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * L_148 = ___simplexShape2;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_149 = __this->get_positions_2();
		NativeArray_1_t2D2712E590650FBA496328D31C2AAF79010AD739  L_150 = __this->get_orientations_3();
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B  L_151 = __this->get_radii_8();
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80  L_152 = __this->get_simplices_13();
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99  L_153;
		L_153 = NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A(L_152, /*hidden argument*/NativeList_1_op_Implicit_mA20A224B9ECA30A9AD94D6316520A3BE71C60C8A_RuntimeMethod_var);
		int32_t L_154 = V_0;
		int32_t L_155 = V_1;
		int32_t L_156 = __this->get_optimizationIterations_22();
		float L_157 = __this->get_optimizationTolerance_23();
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_158;
		L_158 = BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63((BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D *)L_148, L_149, L_150, L_151, L_153, L_154, L_155, (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_22), (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_23), L_156, L_157, /*hidden argument*/BurstLocalOptimization_Optimize_TisBurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D_mA39BD6394D11E5E89D586C56A79CD28C422D7B63_RuntimeMethod_var);
		V_26 = L_158;
		// simplexRadiusA = 0; simplexRadiusB = 0;
		V_24 = (0.0f);
		// simplexRadiusA = 0; simplexRadiusB = 0;
		V_25 = (0.0f);
		// float4 velocityA = float4.zero, velocityB = float4.zero, normalB = float4.zero;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_159 = ((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields*)il2cpp_codegen_static_fields_for(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var))->get_zero_4();
		V_27 = L_159;
		// float4 velocityA = float4.zero, velocityB = float4.zero, normalB = float4.zero;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_160 = ((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields*)il2cpp_codegen_static_fields_for(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var))->get_zero_4();
		V_28 = L_160;
		// float4 velocityA = float4.zero, velocityB = float4.zero, normalB = float4.zero;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_161 = ((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields*)il2cpp_codegen_static_fields_for(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var))->get_zero_4();
		V_29 = L_161;
		// for (int j = 0; j < simplexSizeA; ++j)
		V_34 = 0;
		goto IL_03e7;
	}

IL_038b:
	{
		// int particleIndex = simplices[simplexStartA + j];
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_162 = __this->get_address_of_simplices_13();
		int32_t L_163 = V_0;
		int32_t L_164 = V_34;
		int32_t L_165;
		L_165 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_162, ((int32_t)il2cpp_codegen_add((int32_t)L_163, (int32_t)L_164)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		V_35 = L_165;
		// simplexRadiusA += radii[particleIndex].x * simplexBary[j];
		float L_166 = V_24;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_167 = __this->get_address_of_radii_8();
		int32_t L_168 = V_35;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_169;
		L_169 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_167)->___m_Buffer_0, L_168);
		float L_170 = L_169.get_x_0();
		int32_t L_171 = V_34;
		float L_172;
		L_172 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_22), L_171, /*hidden argument*/NULL);
		V_24 = ((float)il2cpp_codegen_add((float)L_166, (float)((float)il2cpp_codegen_multiply((float)L_170, (float)L_172))));
		// velocityA += velocities[particleIndex] * simplexBary[j];
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_173 = V_27;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_174 = __this->get_address_of_velocities_6();
		int32_t L_175 = V_35;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_176;
		L_176 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_174)->___m_Buffer_0, L_175);
		int32_t L_177 = V_34;
		float L_178;
		L_178 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_22), L_177, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_179;
		L_179 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_176, L_178, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_180;
		L_180 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_173, L_179, /*hidden argument*/NULL);
		V_27 = L_180;
		// for (int j = 0; j < simplexSizeA; ++j)
		int32_t L_181 = V_34;
		V_34 = ((int32_t)il2cpp_codegen_add((int32_t)L_181, (int32_t)1));
	}

IL_03e7:
	{
		// for (int j = 0; j < simplexSizeA; ++j)
		int32_t L_182 = V_34;
		int32_t L_183 = V_1;
		if ((((int32_t)L_182) < ((int32_t)L_183)))
		{
			goto IL_038b;
		}
	}
	{
		// for (int j = 0; j < simplexSizeB; ++j)
		V_36 = 0;
		goto IL_0483;
	}

IL_03f4:
	{
		// int particleIndex = simplices[simplexStartB + j];
		NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 * L_184 = __this->get_address_of_simplices_13();
		int32_t L_185 = V_2;
		int32_t L_186 = V_36;
		int32_t L_187;
		L_187 = NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD((NativeList_1_t37924D67F0E4D83DE506A261B7A05AE619630C80 *)L_184, ((int32_t)il2cpp_codegen_add((int32_t)L_185, (int32_t)L_186)), /*hidden argument*/NativeList_1_get_Item_mA9983E9C959E1D5B95B39FC7C40CF287E32D47FD_RuntimeMethod_var);
		V_37 = L_187;
		// simplexRadiusB += radii[particleIndex].x * surfacePoint.bary[j];
		float L_188 = V_25;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_189 = __this->get_address_of_radii_8();
		int32_t L_190 = V_37;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_191;
		L_191 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_189)->___m_Buffer_0, L_190);
		float L_192 = L_191.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_193 = (&V_26)->get_address_of_bary_0();
		int32_t L_194 = V_36;
		float L_195;
		L_195 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_193, L_194, /*hidden argument*/NULL);
		V_25 = ((float)il2cpp_codegen_add((float)L_188, (float)((float)il2cpp_codegen_multiply((float)L_192, (float)L_195))));
		// velocityB += velocities[particleIndex] * surfacePoint.bary[j];
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_196 = V_28;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_197 = __this->get_address_of_velocities_6();
		int32_t L_198 = V_37;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_199;
		L_199 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_197)->___m_Buffer_0, L_198);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_200 = (&V_26)->get_address_of_bary_0();
		int32_t L_201 = V_36;
		float L_202;
		L_202 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_200, L_201, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_203;
		L_203 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_199, L_202, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_204;
		L_204 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_196, L_203, /*hidden argument*/NULL);
		V_28 = L_204;
		// normalB += normals[particleIndex] * surfacePoint.bary[j];
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_205 = V_29;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_206 = __this->get_address_of_normals_9();
		int32_t L_207 = V_37;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_208;
		L_208 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_206)->___m_Buffer_0, L_207);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_209 = (&V_26)->get_address_of_bary_0();
		int32_t L_210 = V_36;
		float L_211;
		L_211 = float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_209, L_210, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_212;
		L_212 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_208, L_211, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_213;
		L_213 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_205, L_212, /*hidden argument*/NULL);
		V_29 = L_213;
		// for (int j = 0; j < simplexSizeB; ++j)
		int32_t L_214 = V_36;
		V_36 = ((int32_t)il2cpp_codegen_add((int32_t)L_214, (int32_t)1));
	}

IL_0483:
	{
		// for (int j = 0; j < simplexSizeB; ++j)
		int32_t L_215 = V_36;
		int32_t L_216 = V_3;
		if ((((int32_t)L_215) < ((int32_t)L_216)))
		{
			goto IL_03f4;
		}
	}
	{
		// float dAB = math.dot(simplexPoint - surfacePoint.point, surfacePoint.normal);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_217 = V_23;
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_218 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_219 = L_218.get_point_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_220;
		L_220 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_217, L_219, /*hidden argument*/NULL);
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_221 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_222 = L_221.get_normal_2();
		float L_223;
		L_223 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_220, L_222, /*hidden argument*/NULL);
		V_30 = L_223;
		// float vel = math.dot(velocityA    - velocityB,          surfacePoint.normal);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_224 = V_27;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_225 = V_28;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_226;
		L_226 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_224, L_225, /*hidden argument*/NULL);
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_227 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_228 = L_227.get_normal_2();
		float L_229;
		L_229 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_226, L_228, /*hidden argument*/NULL);
		// if (vel * dt + dAB <= simplexRadiusA + simplexRadiusB + collisionMargin)
		float L_230 = __this->get_dt_20();
		float L_231 = V_30;
		float L_232 = V_24;
		float L_233 = V_25;
		float L_234 = __this->get_collisionMargin_21();
		if ((!(((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_229, (float)L_230)), (float)L_231))) <= ((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)L_232, (float)L_233)), (float)L_234))))))
		{
			goto IL_053c;
		}
	}
	{
		// if ((flagsB & ObiUtils.ParticleFlags.OneSided) != 0 && categoryA < categoryB)
		int32_t L_235 = V_10;
		if (!((int32_t)((int32_t)L_235&(int32_t)((int32_t)67108864))))
		{
			goto IL_04f2;
		}
	}
	{
		int32_t L_236 = V_7;
		int32_t L_237 = V_11;
		if ((((int32_t)L_236) >= ((int32_t)L_237)))
		{
			goto IL_04f2;
		}
	}
	{
		// BurstMath.OneSidedNormal(normalB, ref surfacePoint.normal);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_238 = V_29;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_239 = (&V_26)->get_address_of_normal_2();
		IL2CPP_RUNTIME_CLASS_INIT(BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_il2cpp_TypeInfo_var);
		BurstMath_OneSidedNormal_m8551E1209D5159A5CB34E47F9D47F3FCE7186A68_inline(L_238, (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_239, /*hidden argument*/NULL);
	}

IL_04f2:
	{
		// contactsQueue.Enqueue(new BurstContact()
		// {
		//     bodyA = A,
		//     bodyB = B,
		//     pointA = simplexBary,
		//     pointB = surfacePoint.bary,
		//     normal = surfacePoint.normal
		// });
		ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 * L_240 = __this->get_address_of_contactsQueue_18();
		il2cpp_codegen_initobj((&V_38), sizeof(BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C ));
		int32_t L_241 = ___A0;
		(&V_38)->set_bodyA_11(L_241);
		int32_t L_242 = ___B1;
		(&V_38)->set_bodyB_12(L_242);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_243 = V_22;
		(&V_38)->set_pointA_0(L_243);
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_244 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_245 = L_244.get_bary_0();
		(&V_38)->set_pointB_1(L_245);
		SurfacePoint_t69683877C25B2613F38F0949FDF1C8162958E7ED  L_246 = V_26;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_247 = L_246.get_normal_2();
		(&V_38)->set_normal_2(L_247);
		BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C  L_248 = V_38;
		ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108((ParallelWriter_t5D9DBFB5FFA66DB45FE1BDBCFE076E758012BD05 *)L_240, L_248, /*hidden argument*/ParallelWriter_Enqueue_m96B8855FCE1F9462A80B5BD0F93381B5914CE108_RuntimeMethod_var);
	}

IL_053c:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___A0, int32_t ___B1, BurstSimplex_t47F8E2426AFA887E2A2D846FC6FB4484B4451A0D * ___simplexShape2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 * _thisAdjusted = reinterpret_cast<GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5 *>(__this + _offset);
	GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7(_thisAdjusted, ___A0, ___B1, ___simplexShape2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif


// Conversion methods for marshalling of: Obi.ParticleGrid/InterpolateDiffusePropertiesJob
IL2CPP_EXTERN_C void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshal_pinvoke(const InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826& unmarshaled, InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_pinvoke& marshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'InterpolateDiffusePropertiesJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
IL2CPP_EXTERN_C void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshal_pinvoke_back(const InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_pinvoke& marshaled, InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826& unmarshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'InterpolateDiffusePropertiesJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/InterpolateDiffusePropertiesJob
IL2CPP_EXTERN_C void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshal_pinvoke_cleanup(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: Obi.ParticleGrid/InterpolateDiffusePropertiesJob
IL2CPP_EXTERN_C void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshal_com(const InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826& unmarshaled, InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_com& marshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'InterpolateDiffusePropertiesJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
IL2CPP_EXTERN_C void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshal_com_back(const InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_com& marshaled, InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826& unmarshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'InterpolateDiffusePropertiesJob'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/InterpolateDiffusePropertiesJob
IL2CPP_EXTERN_C void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshal_com_cleanup(InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_marshaled_com& marshaled)
{
}
// System.Void Obi.ParticleGrid/InterpolateDiffusePropertiesJob::Execute(System.Int32)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A (InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826 * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_7;
	memset((&V_7), 0, sizeof(V_7));
	int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  V_8;
	memset((&V_8), 0, sizeof(V_8));
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  V_11;
	memset((&V_11), 0, sizeof(V_11));
	int32_t V_12 = 0;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_13;
	memset((&V_13), 0, sizeof(V_13));
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t G_B3_0 = 0;
	{
		// neighbourCount[p] = 0;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_0 = __this->get_address_of_neighbourCount_7();
		int32_t L_1 = ___p0;
		IL2CPP_NATIVEARRAY_SET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_0)->___m_Buffer_0, L_1, (0));
		// float4 diffuseProperty = float4.zero;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields*)il2cpp_codegen_static_fields_for(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var))->get_zero_4();
		V_0 = L_2;
		// float kernelSum = 0;
		V_1 = (0.0f);
		// int offsetCount = mode2D ? 4 : 8;
		bool L_3 = __this->get_mode2D_10();
		if (L_3)
		{
			goto IL_0024;
		}
	}
	{
		G_B3_0 = 8;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 4;
	}

IL_0025:
	{
		V_2 = G_B3_0;
		// float4 solverDiffusePosition = inertialFrame.frame.InverseTransformPoint(diffusePositions[p]);
		BurstInertialFrame_t686F56AC19369B0A2D12FF676CBC956ADA125551 * L_4 = __this->get_address_of_inertialFrame_9();
		BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6 * L_5 = L_4->get_address_of_frame_0();
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_6 = __this->get_address_of_diffusePositions_4();
		int32_t L_7 = ___p0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8;
		L_8 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_6)->___m_Buffer_0, L_7);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9;
		L_9 = BurstAffineTransform_InverseTransformPoint_m62FF98D4C1F4B903143CBEBB8BE560DD8973E955((BurstAffineTransform_tE9C25DCFE3F0E8278B6ABD9854126D0B7BD3C6B6 *)L_5, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		// for (int k = 0; k < gridLevels.Length; ++k)
		V_4 = 0;
		goto IL_01ec;
	}

IL_004b:
	{
		// int l = gridLevels[k];
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_10 = __this->get_address_of_gridLevels_8();
		int32_t L_11 = V_4;
		int32_t L_12;
		L_12 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_10)->___m_Buffer_0, L_11);
		V_5 = L_12;
		// float radius = NativeMultilevelGrid<int>.CellSizeOfLevel(l);
		int32_t L_13 = V_5;
		float L_14;
		L_14 = NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_inline(L_13, /*hidden argument*/NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_RuntimeMethod_var);
		V_6 = L_14;
		// float4 cellCoords = math.floor(solverDiffusePosition / radius);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_15 = V_3;
		float L_16 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_17;
		L_17 = float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline(L_15, L_16, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_18;
		L_18 = math_floor_m6F1A809C667676D8CDF372A24CC318F90943AAC7_inline(L_17, /*hidden argument*/NULL);
		V_7 = L_18;
		// cellCoords[3] = 0;
		float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_7), 3, (0.0f), /*hidden argument*/NULL);
		// if (mode2D)
		bool L_19 = __this->get_mode2D_10();
		if (!L_19)
		{
			goto IL_0094;
		}
	}
	{
		// cellCoords[2] = 0;
		float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_7), 2, (0.0f), /*hidden argument*/NULL);
	}

IL_0094:
	{
		// float4 posInCell = solverDiffusePosition - (cellCoords * radius + new float4(radius * 0.5f));
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_20 = V_3;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_21 = V_7;
		float L_22 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_23;
		L_23 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_21, L_22, /*hidden argument*/NULL);
		float L_24 = V_6;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_25;
		memset((&L_25), 0, sizeof(L_25));
		float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline((&L_25), ((float)il2cpp_codegen_multiply((float)L_24, (float)(0.5f))), /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_26;
		L_26 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_23, L_25, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_27;
		L_27 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_20, L_26, /*hidden argument*/NULL);
		// int4 quadrant = (int4)math.sign(posInCell);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_28;
		L_28 = math_sign_m63A39DE29A6BCB0E9BA152B458A0DD2EB9EFB23C_inline(L_27, /*hidden argument*/NULL);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_29;
		L_29 = int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E_inline(L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		// quadrant[3] = l;
		int32_t L_30 = V_5;
		int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5((int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 *)(&V_8), 3, L_30, /*hidden argument*/NULL);
		// for (int i = 0; i < offsetCount; ++i)
		V_9 = 0;
		goto IL_01de;
	}

IL_00d3:
	{
		// if (grid.TryGetCellIndex((int4)cellCoords + cellOffsets[i] * quadrant, out cellIndex))
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_31 = __this->get_address_of_grid_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_32 = V_7;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_33;
		L_33 = int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E_inline(L_32, /*hidden argument*/NULL);
		NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C * L_34 = __this->get_address_of_cellOffsets_1();
		int32_t L_35 = V_9;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_36;
		L_36 = IL2CPP_NATIVEARRAY_GET_ITEM(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 , ((NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C *)L_34)->___m_Buffer_0, L_35);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_37 = V_8;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_38;
		L_38 = int4_op_Multiply_m5FC679C570C40C6A3CCF3E600DACD56D48063524_inline(L_36, L_37, /*hidden argument*/NULL);
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_39;
		L_39 = int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3_inline(L_33, L_38, /*hidden argument*/NULL);
		bool L_40;
		L_40 = NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E((NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *)L_31, L_39, (int32_t*)(&V_10), /*hidden argument*/NativeMultilevelGrid_1_TryGetCellIndex_mA15CECAF2CC3B557C310BB466353AA33AA8DC01E_RuntimeMethod_var);
		if (!L_40)
		{
			goto IL_01d8;
		}
	}
	{
		// var cell = grid.usedCells[cellIndex];
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_41 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_42 = L_41->get_address_of_usedCells_2();
		int32_t L_43 = V_10;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_44;
		L_44 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_42, L_43, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_11 = L_44;
		// for (int n = 0; n < cell.Length; ++n)
		V_12 = 0;
		goto IL_01ca;
	}

IL_0121:
	{
		// float4 r = solverDiffusePosition - positions[cell[n]];
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_45 = V_3;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_46 = __this->get_address_of_positions_2();
		int32_t L_47 = V_12;
		int32_t L_48;
		L_48 = Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_11), L_47, /*hidden argument*/Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_49;
		L_49 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_46)->___m_Buffer_0, L_48);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_50;
		L_50 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_45, L_49, /*hidden argument*/NULL);
		V_13 = L_50;
		// r[3] = 0;
		float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_13), 3, (0.0f), /*hidden argument*/NULL);
		// if (mode2D)
		bool L_51 = __this->get_mode2D_10();
		if (!L_51)
		{
			goto IL_015f;
		}
	}
	{
		// r[2] = 0;
		float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_13), 2, (0.0f), /*hidden argument*/NULL);
	}

IL_015f:
	{
		// float d = math.length(r);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_52 = V_13;
		float L_53;
		L_53 = math_length_mD7967FEA18B97C7AC6CFE6B0BE3D45D35D355170_inline(L_52, /*hidden argument*/NULL);
		V_14 = L_53;
		// if (d <= radius)
		float L_54 = V_14;
		float L_55 = V_6;
		if ((!(((float)L_54) <= ((float)L_55))))
		{
			goto IL_01c4;
		}
	}
	{
		// float w = densityKernel.W(d, radius);
		Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235 * L_56 = __this->get_address_of_densityKernel_5();
		float L_57 = V_14;
		float L_58 = V_6;
		float L_59;
		L_59 = Poly6Kernel_W_mA9FD1FFD1D90D74DB6803EC97F570DAB31EAE849((Poly6Kernel_t53B2EA5175B2907F05985EBBEEBD736F8E395235 *)L_56, L_57, L_58, /*hidden argument*/NULL);
		V_15 = L_59;
		// kernelSum += w;
		float L_60 = V_1;
		float L_61 = V_15;
		V_1 = ((float)il2cpp_codegen_add((float)L_60, (float)L_61));
		// diffuseProperty += properties[cell[n]] * w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_62 = V_0;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_63 = __this->get_address_of_properties_3();
		int32_t L_64 = V_12;
		int32_t L_65;
		L_65 = Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_11), L_64, /*hidden argument*/Cell_1_get_Item_m7DC9A9183B361C15FFA23FB884D7B6362EEC9389_RuntimeMethod_var);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_66;
		L_66 = IL2CPP_NATIVEARRAY_GET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_63)->___m_Buffer_0, L_65);
		float L_67 = V_15;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_68;
		L_68 = float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline(L_66, L_67, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_69;
		L_69 = float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline(L_62, L_68, /*hidden argument*/NULL);
		V_0 = L_69;
		// neighbourCount[p]++;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_70 = __this->get_address_of_neighbourCount_7();
		int32_t L_71 = ___p0;
		V_16 = L_71;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_72 = L_70;
		int32_t L_73 = V_16;
		int32_t L_74;
		L_74 = IL2CPP_NATIVEARRAY_GET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_72)->___m_Buffer_0, L_73);
		V_17 = L_74;
		int32_t L_75 = V_16;
		int32_t L_76 = V_17;
		IL2CPP_NATIVEARRAY_SET_ITEM(int32_t, ((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_72)->___m_Buffer_0, L_75, (((int32_t)il2cpp_codegen_add((int32_t)L_76, (int32_t)1))));
	}

IL_01c4:
	{
		// for (int n = 0; n < cell.Length; ++n)
		int32_t L_77 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_77, (int32_t)1));
	}

IL_01ca:
	{
		// for (int n = 0; n < cell.Length; ++n)
		int32_t L_78 = V_12;
		int32_t L_79;
		L_79 = Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_11), /*hidden argument*/Cell_1_get_Length_mF96A94EDDA7E38346C93EEF257EFF4B969888E03_RuntimeMethod_var);
		if ((((int32_t)L_78) < ((int32_t)L_79)))
		{
			goto IL_0121;
		}
	}

IL_01d8:
	{
		// for (int i = 0; i < offsetCount; ++i)
		int32_t L_80 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_80, (int32_t)1));
	}

IL_01de:
	{
		// for (int i = 0; i < offsetCount; ++i)
		int32_t L_81 = V_9;
		int32_t L_82 = V_2;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_00d3;
		}
	}
	{
		// for (int k = 0; k < gridLevels.Length; ++k)
		int32_t L_83 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_83, (int32_t)1));
	}

IL_01ec:
	{
		// for (int k = 0; k < gridLevels.Length; ++k)
		int32_t L_84 = V_4;
		NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 * L_85 = __this->get_address_of_gridLevels_8();
		int32_t L_86;
		L_86 = IL2CPP_NATIVEARRAY_GET_LENGTH(((NativeArray_1_tD60079F06B473C85CF6C2BB4F2D203F38191AE99 *)L_85)->___m_Length_1);
		if ((((int32_t)L_84) < ((int32_t)L_86)))
		{
			goto IL_004b;
		}
	}
	{
		// if (kernelSum > BurstMath.epsilon)
		float L_87 = V_1;
		if ((!(((float)L_87) > ((float)(1.00000001E-07f)))))
		{
			goto IL_0219;
		}
	}
	{
		// diffuseProperties[p] = diffuseProperty / kernelSum;
		NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B * L_88 = __this->get_address_of_diffuseProperties_6();
		int32_t L_89 = ___p0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_90 = V_0;
		float L_91 = V_1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_92;
		L_92 = float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline(L_90, L_91, /*hidden argument*/NULL);
		IL2CPP_NATIVEARRAY_SET_ITEM(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 , ((NativeArray_1_t22D7B5C028D583C334D797C271E572FC5F6F041B *)L_88)->___m_Buffer_0, L_89, (L_92));
	}

IL_0219:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, int32_t ___p0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826 * _thisAdjusted = reinterpret_cast<InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826 *>(__this + _offset);
	InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A(_thisAdjusted, ___p0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Obi.ParticleGrid/UpdateGrid
IL2CPP_EXTERN_C void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshal_pinvoke(const UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA& unmarshaled, UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_pinvoke& marshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'UpdateGrid'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
IL2CPP_EXTERN_C void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshal_pinvoke_back(const UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_pinvoke& marshaled, UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA& unmarshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'UpdateGrid'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/UpdateGrid
IL2CPP_EXTERN_C void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshal_pinvoke_cleanup(UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Obi.ParticleGrid/UpdateGrid
IL2CPP_EXTERN_C void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshal_com(const UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA& unmarshaled, UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_com& marshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'UpdateGrid'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
IL2CPP_EXTERN_C void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshal_com_back(const UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_com& marshaled, UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA& unmarshaled)
{
	Exception_t* ___grid_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'grid' of type 'UpdateGrid'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___grid_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Obi.ParticleGrid/UpdateGrid
IL2CPP_EXTERN_C void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshal_com_cleanup(UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_marshaled_com& marshaled)
{
}
// System.Void Obi.ParticleGrid/UpdateGrid::Execute()
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47 (UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA * IL2CPP_PARAMETER_RESTRICT __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// grid.Clear();
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_0 = __this->get_address_of_grid_0();
		NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036((NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *)L_0, /*hidden argument*/NativeMultilevelGrid_1_Clear_m7A926136730C350F51C4CAD966B427115F9D3036_RuntimeMethod_var);
		// for (int i = 0; i < simplexCount; ++i)
		V_0 = 0;
		goto IL_0057;
	}

IL_000f:
	{
		// int cellIndex = grid.GetOrCreateCell(cellCoords[i]);
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_1 = __this->get_address_of_grid_0();
		NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C * L_2 = __this->get_address_of_cellCoords_1();
		int32_t L_3 = V_0;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4;
		L_4 = IL2CPP_NATIVEARRAY_GET_ITEM(int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 , ((NativeArray_1_t25B4156F35F36B899D64072A41D1468AE140502C *)L_2)->___m_Buffer_0, L_3);
		int32_t L_5;
		L_5 = NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8((NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 *)L_1, L_4, /*hidden argument*/NativeMultilevelGrid_1_GetOrCreateCell_mCF183BD78C2C16F7AF8214FB4095AF18220E69C8_RuntimeMethod_var);
		V_1 = L_5;
		// var newCell = grid.usedCells[cellIndex];
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_6 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_7 = L_6->get_address_of_usedCells_2();
		int32_t L_8 = V_1;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_9;
		L_9 = NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_7, L_8, /*hidden argument*/NativeList_1_get_Item_m3F4F3A18D1DC06F2073D5F580B0A0821F7EF95D3_RuntimeMethod_var);
		V_2 = L_9;
		// newCell.Add(i);
		int32_t L_10 = V_0;
		Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9((Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B *)(&V_2), L_10, /*hidden argument*/Cell_1_Add_m45196144128171B46278BF24261A42AF397D71B9_RuntimeMethod_var);
		// grid.usedCells[cellIndex] = newCell;
		NativeMultilevelGrid_1_t657174F29DF36C4B35776A58D55288D41D5928C7 * L_11 = __this->get_address_of_grid_0();
		NativeList_1_t37C384457963A144915FA46915001309FA309A59 * L_12 = L_11->get_address_of_usedCells_2();
		int32_t L_13 = V_1;
		Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B  L_14 = V_2;
		NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118((NativeList_1_t37C384457963A144915FA46915001309FA309A59 *)L_12, L_13, L_14, /*hidden argument*/NativeList_1_set_Item_mF4553D468280CF6A35E3C36DF478CC80D7DF2118_RuntimeMethod_var);
		// for (int i = 0; i < simplexCount; ++i)
		int32_t L_15 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_0057:
	{
		// for (int i = 0; i < simplexCount; ++i)
		int32_t L_16 = V_0;
		int32_t L_17 = __this->get_simplexCount_2();
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_000f;
		}
	}
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47_AdjustorThunk (RuntimeObject * IL2CPP_PARAMETER_RESTRICT __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA * _thisAdjusted = reinterpret_cast<UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA *>(__this + _offset);
	UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Obi.VoxelDistanceField/<JumpFlood>d__5::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CJumpFloodU3Ed__5_MoveNext_mCF3E269446E8A96E7F313896B714A111CBBD3FA3 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* V_5 = NULL;
	int32_t V_6 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_019e;
			}
			case 2:
			{
				goto IL_023e;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// distanceField = new Vector3Int[voxelizer.resolution.x,
		//                                voxelizer.resolution.y,
		//                                voxelizer.resolution.z];
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_3 = V_1;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_4 = V_1;
		NullCheck(L_4);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_5 = L_4->get_voxelizer_1();
		NullCheck(L_5);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_6 = L_5->get_address_of_resolution_8();
		int32_t L_7;
		L_7 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_6, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_8 = V_1;
		NullCheck(L_8);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_9 = L_8->get_voxelizer_1();
		NullCheck(L_9);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_10 = L_9->get_address_of_resolution_8();
		int32_t L_11;
		L_11 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_10, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_12 = V_1;
		NullCheck(L_12);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_13 = L_12->get_voxelizer_1();
		NullCheck(L_13);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_14 = L_13->get_address_of_resolution_8();
		int32_t L_15;
		L_15 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_14, /*hidden argument*/NULL);
		il2cpp_array_size_t L_17[] = { (il2cpp_array_size_t)L_7, (il2cpp_array_size_t)L_11, (il2cpp_array_size_t)L_15 };
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_16 = (Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9*)GenArrayNew(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var, L_17);
		NullCheck(L_3);
		L_3->set_distanceField_0(L_16);
		// Vector3Int[,,] auxBuffer = new Vector3Int[voxelizer.resolution.x,
		//                                           voxelizer.resolution.y,
		//                                           voxelizer.resolution.z];
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_18 = V_1;
		NullCheck(L_18);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_19 = L_18->get_voxelizer_1();
		NullCheck(L_19);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_20 = L_19->get_address_of_resolution_8();
		int32_t L_21;
		L_21 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_20, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_22 = V_1;
		NullCheck(L_22);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_23 = L_22->get_voxelizer_1();
		NullCheck(L_23);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_24 = L_23->get_address_of_resolution_8();
		int32_t L_25;
		L_25 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_24, /*hidden argument*/NULL);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_26 = V_1;
		NullCheck(L_26);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_27 = L_26->get_voxelizer_1();
		NullCheck(L_27);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_28 = L_27->get_address_of_resolution_8();
		int32_t L_29;
		L_29 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_28, /*hidden argument*/NULL);
		il2cpp_array_size_t L_31[] = { (il2cpp_array_size_t)L_21, (il2cpp_array_size_t)L_25, (il2cpp_array_size_t)L_29 };
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_30 = (Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9*)GenArrayNew(Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9_il2cpp_TypeInfo_var, L_31);
		__this->set_U3CauxBufferU3E5__2_3(L_30);
		// for (int x = 0; x < distanceField.GetLength(0); ++x)
		V_2 = 0;
		goto IL_011c;
	}

IL_00a3:
	{
		// for (int y = 0; y < distanceField.GetLength(1); ++y)
		V_3 = 0;
		goto IL_0109;
	}

IL_00a7:
	{
		// for (int z = 0; z < distanceField.GetLength(2); ++z)
		V_4 = 0;
		goto IL_00f5;
	}

IL_00ac:
	{
		// if (voxelizer[x, y, z] == MeshVoxelizer.Voxel.Boundary)
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_32 = V_1;
		NullCheck(L_32);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_33 = L_32->get_voxelizer_1();
		int32_t L_34 = V_2;
		int32_t L_35 = V_3;
		int32_t L_36 = V_4;
		NullCheck(L_33);
		int32_t L_37;
		L_37 = MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356(L_33, L_34, L_35, L_36, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_37) == ((uint32_t)2))))
		{
			goto IL_00d8;
		}
	}
	{
		// distanceField[x, y, z] = new Vector3Int(x, y, z);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_38 = V_1;
		NullCheck(L_38);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_39 = L_38->get_distanceField_0();
		int32_t L_40 = V_2;
		int32_t L_41 = V_3;
		int32_t L_42 = V_4;
		int32_t L_43 = V_2;
		int32_t L_44 = V_3;
		int32_t L_45 = V_4;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_46), L_43, L_44, L_45, /*hidden argument*/NULL);
		NullCheck(L_39);
		(L_39)->SetAt(L_40, L_41, L_42, L_46);
		goto IL_00ef;
	}

IL_00d8:
	{
		// distanceField[x, y, z] = new Vector3Int(-1, -1, -1);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_47 = V_1;
		NullCheck(L_47);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_48 = L_47->get_distanceField_0();
		int32_t L_49 = V_2;
		int32_t L_50 = V_3;
		int32_t L_51 = V_4;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_52;
		memset((&L_52), 0, sizeof(L_52));
		Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline((&L_52), (-1), (-1), (-1), /*hidden argument*/NULL);
		NullCheck(L_48);
		(L_48)->SetAt(L_49, L_50, L_51, L_52);
	}

IL_00ef:
	{
		// for (int z = 0; z < distanceField.GetLength(2); ++z)
		int32_t L_53 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_53, (int32_t)1));
	}

IL_00f5:
	{
		// for (int z = 0; z < distanceField.GetLength(2); ++z)
		int32_t L_54 = V_4;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_55 = V_1;
		NullCheck(L_55);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_56 = L_55->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_56);
		int32_t L_57;
		L_57 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_56, 2, /*hidden argument*/NULL);
		if ((((int32_t)L_54) < ((int32_t)L_57)))
		{
			goto IL_00ac;
		}
	}
	{
		// for (int y = 0; y < distanceField.GetLength(1); ++y)
		int32_t L_58 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
	}

IL_0109:
	{
		// for (int y = 0; y < distanceField.GetLength(1); ++y)
		int32_t L_59 = V_3;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_60 = V_1;
		NullCheck(L_60);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_61 = L_60->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_61);
		int32_t L_62;
		L_62 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_61, 1, /*hidden argument*/NULL);
		if ((((int32_t)L_59) < ((int32_t)L_62)))
		{
			goto IL_00a7;
		}
	}
	{
		// for (int x = 0; x < distanceField.GetLength(0); ++x)
		int32_t L_63 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_63, (int32_t)1));
	}

IL_011c:
	{
		// for (int x = 0; x < distanceField.GetLength(0); ++x)
		int32_t L_64 = V_2;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_65 = V_1;
		NullCheck(L_65);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_66 = L_65->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_66);
		int32_t L_67;
		L_67 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_66, 0, /*hidden argument*/NULL);
		if ((((int32_t)L_64) < ((int32_t)L_67)))
		{
			goto IL_00a3;
		}
	}
	{
		// int size = Mathf.Max(distanceField.GetLength(0),
		//                      distanceField.GetLength(1),
		//                      distanceField.GetLength(2));
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_68 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)3);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_69 = L_68;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_70 = V_1;
		NullCheck(L_70);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_71 = L_70->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_71);
		int32_t L_72;
		L_72 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_71, 0, /*hidden argument*/NULL);
		NullCheck(L_69);
		(L_69)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_72);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_73 = L_69;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_74 = V_1;
		NullCheck(L_74);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_75 = L_74->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_75);
		int32_t L_76;
		L_76 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_75, 1, /*hidden argument*/NULL);
		NullCheck(L_73);
		(L_73)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)L_76);
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_77 = L_73;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_78 = V_1;
		NullCheck(L_78);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_79 = L_78->get_distanceField_0();
		NullCheck((RuntimeArray *)(RuntimeArray *)L_79);
		int32_t L_80;
		L_80 = Array_GetLength_m8EF840DA7BEB0DFF04D36C3DC651B673C49A02BB((RuntimeArray *)(RuntimeArray *)L_79, 2, /*hidden argument*/NULL);
		NullCheck(L_77);
		(L_77)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)L_80);
		int32_t L_81;
		L_81 = Mathf_Max_mC299EEF1FD6084E41A182E2033790DB50DEF5B39(L_77, /*hidden argument*/NULL);
		__this->set_U3CsizeU3E5__3_4(L_81);
		// int step = (int)(size / 2.0f);
		int32_t L_82 = __this->get_U3CsizeU3E5__3_4();
		__this->set_U3CstepU3E5__4_5(il2cpp_codegen_cast_double_to_int<int32_t>(((float)((float)((float)((float)L_82))/(float)(2.0f)))));
		// yield return new CoroutineJob.ProgressInfo("Generating voxel distance field...",0);
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_83 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_83, _stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94, (0.0f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_83);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_019e:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float numPasses = (int) Mathf.Log(size, 2);
		int32_t L_84 = __this->get_U3CsizeU3E5__3_4();
		float L_85;
		L_85 = Mathf_Log_mF7F3624FA030AB57AD8C1F4CAF084B2DCC99897A(((float)((float)L_84)), (2.0f), /*hidden argument*/NULL);
		__this->set_U3CnumPassesU3E5__5_6(((float)((float)il2cpp_codegen_cast_double_to_int<int32_t>(L_85))));
		// int i = 0;
		__this->set_U3CiU3E5__6_7(0);
		goto IL_0245;
	}

IL_01c7:
	{
		// JumpFloodPass(step, distanceField, auxBuffer);
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_86 = V_1;
		int32_t L_87 = __this->get_U3CstepU3E5__4_5();
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_88 = V_1;
		NullCheck(L_88);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_89 = L_88->get_distanceField_0();
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_90 = __this->get_U3CauxBufferU3E5__2_3();
		NullCheck(L_86);
		VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3(L_86, L_87, L_89, L_90, /*hidden argument*/NULL);
		// step /= 2;
		int32_t L_91 = __this->get_U3CstepU3E5__4_5();
		__this->set_U3CstepU3E5__4_5(((int32_t)((int32_t)L_91/(int32_t)2)));
		// Vector3Int[,,] temp = distanceField;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_92 = V_1;
		NullCheck(L_92);
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_93 = L_92->get_distanceField_0();
		V_5 = L_93;
		// distanceField = auxBuffer;
		VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA * L_94 = V_1;
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_95 = __this->get_U3CauxBufferU3E5__2_3();
		NullCheck(L_94);
		L_94->set_distanceField_0(L_95);
		// auxBuffer = temp;
		Vector3IntU5BU2CU2CU5D_tED0969D7AB77EAD4A62C8482B3D82A34FD92C2B9* L_96 = V_5;
		__this->set_U3CauxBufferU3E5__2_3(L_96);
		// yield return new CoroutineJob.ProgressInfo("Generating voxel distance field...", ++i / numPasses);
		int32_t L_97 = __this->get_U3CiU3E5__6_7();
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_97, (int32_t)1));
		int32_t L_98 = V_6;
		__this->set_U3CiU3E5__6_7(L_98);
		int32_t L_99 = V_6;
		float L_100 = __this->get_U3CnumPassesU3E5__5_6();
		ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 * L_101 = (ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74 *)il2cpp_codegen_object_new(ProgressInfo_tA07D168F3215932885B5C3D7CACC637ED337BF74_il2cpp_TypeInfo_var);
		ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1(L_101, _stringLiteral8BF94FD2D8D349E76B64F4A8FEA9E6A331A7DB94, ((float)((float)((float)((float)L_99))/(float)L_100)), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_101);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_023e:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0245:
	{
		// while (step >= 1)
		int32_t L_102 = __this->get_U3CstepU3E5__4_5();
		if ((((int32_t)L_102) >= ((int32_t)1)))
		{
			goto IL_01c7;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22_RuntimeMethod_var)));
	}
}
// System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18 (U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.VoxelPathFinder/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m2E9966CB04100D59E489C39A64FAC67F8FD97311 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * L_0 = (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 *)il2cpp_codegen_object_new(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Obi.VoxelPathFinder/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888 (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Obi.VoxelPathFinder/<>c::<FindClosestNonEmptyVoxel>b__6_1(UnityEngine.Vector3Int)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec_U3CFindClosestNonEmptyVoxelU3Eb__6_1_mF780311AA35327276D353A5C472EBABD63FB6D10 (U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___c0, const RuntimeMethod* method)
{
	{
		// return 0;
		return (0.0f);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Obi.VoxelPathFinder/<>c__DisplayClass7_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0__ctor_mCCDBC132E254A9EA49862CDDDB93E94F182AF58C (U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__0(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__0_m0CBCD40678980CB309FD296C8DCA140B2368C158 (U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___v0, const RuntimeMethod* method)
{
	{
		// return v.coordinates == end;
		TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  L_0 = ___v0;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_1 = L_0.get_coordinates_0();
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_2 = __this->get_end_0();
		bool L_3;
		L_3 = Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline(L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__1(UnityEngine.Vector3Int)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__1_m93C19DF998EC9A85989D9980D3C89F8EB7724361 (U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___c0, const RuntimeMethod* method)
{
	{
		// return Vector3.Distance(c, end) * voxelizer.voxelSize;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_0 = ___c0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline(L_0, /*hidden argument*/NULL);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_2 = __this->get_end_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline(L_2, /*hidden argument*/NULL);
		float L_4;
		L_4 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_1, L_3, /*hidden argument*/NULL);
		VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF * L_5 = __this->get_U3CU3E4__this_1();
		NullCheck(L_5);
		MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D * L_6 = L_5->get_voxelizer_0();
		NullCheck(L_6);
		float L_7 = L_6->get_voxelSize_7();
		return ((float)il2cpp_codegen_multiply((float)L_4, (float)L_7));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Obi.VoxelPathFinder/TargetVoxel::get_cost()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, const RuntimeMethod* method)
{
	{
		// get { return distance + heuristic; }
		float L_0 = __this->get_distance_1();
		float L_1 = __this->get_heuristic_2();
		return ((float)il2cpp_codegen_add((float)L_0, (float)L_1));
	}
}
IL2CPP_EXTERN_C  float TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	float _returnValue;
	_returnValue = TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F(_thisAdjusted, method);
	return _returnValue;
}
// System.Void Obi.VoxelPathFinder/TargetVoxel::.ctor(UnityEngine.Vector3Int,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates0, float ___distance1, float ___heuristic2, const RuntimeMethod* method)
{
	{
		// this.coordinates = coordinates;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_0 = ___coordinates0;
		__this->set_coordinates_0(L_0);
		// this.distance = distance;
		float L_1 = ___distance1;
		__this->set_distance_1(L_1);
		// this.heuristic = heuristic;
		float L_2 = ___heuristic2;
		__this->set_heuristic_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF_AdjustorThunk (RuntimeObject * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___coordinates0, float ___distance1, float ___heuristic2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF(_thisAdjusted, ___coordinates0, ___distance1, ___heuristic2, method);
}
// System.Boolean Obi.VoxelPathFinder/TargetVoxel::Equals(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	{
		// return this.coordinates.Equals(other.coordinates);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * L_0 = __this->get_address_of_coordinates_0();
		TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  L_1 = ___other0;
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_2 = L_1.get_coordinates_0();
		bool L_3;
		L_3 = Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_inline((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)L_0, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C  bool TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681_AdjustorThunk (RuntimeObject * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	bool _returnValue;
	_returnValue = TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681(_thisAdjusted, ___other0, method);
	return _returnValue;
}
// System.Int32 Obi.VoxelPathFinder/TargetVoxel::CompareTo(Obi.VoxelPathFinder/TargetVoxel)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8 (TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// return this.cost.CompareTo(other.cost);
		float L_0;
		L_0 = TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F((TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *)__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1;
		L_1 = TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F((TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *)(&___other0), /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = Single_CompareTo_m80B5B5A70A2343C3A8673F35635EBED4458109B4((float*)(&V_0), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C  int32_t TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8_AdjustorThunk (RuntimeObject * __this, TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564  ___other0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 * _thisAdjusted = reinterpret_cast<TargetVoxel_t6F4DB0802F42F7A4EB07A40D6027D2027BFB6564 *>(__this + _offset);
	int32_t _returnValue;
	_returnValue = TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8(_thisAdjusted, ___other0, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3_inline (int32_t ___mask0, int32_t ___category1, const RuntimeMethod* method)
{
	{
		// return (mask << 16) | (1 << category);
		int32_t L_0 = ___mask0;
		int32_t L_1 = ___category1;
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)((int32_t)16)))|(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323_inline (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Master; }
		ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD * L_0 = __this->get_m_Master_11();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37_inline (ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Slave; }
		Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * L_0 = __this->get_m_Slave_12();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A_inline (ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD * __this, const RuntimeMethod* method)
{
	{
		// get { return m_ActiveParticleCount; }
		int32_t L_0 = __this->get_m_ActiveParticleCount_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___lhs0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___rhs1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___lhs0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___rhs1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___lhs0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___rhs1;
		float L_11 = L_10.get_z_4();
		V_0 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
		goto IL_002d;
	}

IL_002d:
	{
		float L_12 = V_0;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, const RuntimeMethod* method)
{
	{
		// get { return new float3(x, y, z); }
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_y_1();
		float L_2 = __this->get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_3;
		memset((&L_3), 0, sizeof(L_3));
		float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___xyz0, int32_t ___w1, const RuntimeMethod* method)
{
	{
		// this.x = xyz.x;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___xyz0;
		int32_t L_1 = L_0.get_x_0();
		__this->set_x_0(L_1);
		// this.y = xyz.y;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___xyz0;
		int32_t L_3 = L_2.get_y_1();
		__this->set_y_1(L_3);
		// this.z = xyz.z;
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___xyz0;
		int32_t L_5 = L_4.get_z_2();
		__this->set_z_2(L_5);
		// this.w = w;
		int32_t L_6 = ___w1;
		__this->set_w_3(L_6);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, const RuntimeMethod* method)
{
	{
		// get { return new int3(x, y, z); }
		int32_t L_0 = __this->get_x_0();
		int32_t L_1 = __this->get_y_1();
		int32_t L_2 = __this->get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_3;
		memset((&L_3), 0, sizeof(L_3));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  int3_op_Addition_m08C6B9DB7119923A27914BAFD7DCDEE50CC988A4_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___lhs0, int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int3 operator + (int3 lhs, int3 rhs) { return new int3 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z); }
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921  L_12;
		memset((&L_12), 0, sizeof(L_12));
		int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline((&L_12), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		int32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		int32_t L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___lhs0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int4 operator + (int4 lhs, int4 rhs) { return new int4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_12 = ___lhs0;
		int32_t L_13 = L_12.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_14 = ___rhs1;
		int32_t L_15 = L_14.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_16;
		memset((&L_16), 0, sizeof(L_16));
		int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline((&L_16), ((int32_t)il2cpp_codegen_add((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57_inline (int32_t ___phase0, const RuntimeMethod* method)
{
	{
		// return (ParticleFlags)(phase & ~ParticleGroupBitmask);
		int32_t L_0 = ___phase0;
		return (int32_t)(((int32_t)((int32_t)L_0&(int32_t)((int32_t)-16777216))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054_inline (int32_t ___phase0, const RuntimeMethod* method)
{
	{
		// return phase & ParticleGroupBitmask;
		int32_t L_0 = ___phase0;
		return ((int32_t)((int32_t)L_0&(int32_t)((int32_t)16777215)));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF_inline (int32_t ___x0, int32_t ___y1, const RuntimeMethod* method)
{
	{
		// public static int max(int x, int y) { return x > y ? x : y; }
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		if ((((int32_t)L_0) > ((int32_t)L_1)))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_2 = ___y1;
		return L_2;
	}

IL_0006:
	{
		int32_t L_3 = ___x0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float4 lhs, float rhs) { return new float4 (lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w * rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator + (float4 lhs, float4 rhs) { return new float4 (lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), ((float)il2cpp_codegen_add((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator - (float4 lhs, float4 rhs) { return new float4 (lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z, lhs.w - rhs.w); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___lhs0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___lhs0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___rhs1;
		float L_15 = L_14.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_16;
		memset((&L_16), 0, sizeof(L_16));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_16), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), ((float)il2cpp_codegen_subtract((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_lengthsq_mD422A214358E935793F5ED10991D70F040848F2D_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float lengthsq(float4 x) { return dot(x, x); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline (float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		// public static float max(float x, float y) { return float.IsNaN(y) || x > y ? x : y; }
		float L_0 = ___y1;
		bool L_1;
		L_1 = Single_IsNaN_m458FF076EF1944D4D888A585F7C6C49DA4730599(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		float L_2 = ___x0;
		float L_3 = ___y1;
		if ((((float)L_2) > ((float)L_3)))
		{
			goto IL_000e;
		}
	}
	{
		float L_4 = ___y1;
		return L_4;
	}

IL_000e:
	{
		float L_5 = ___x0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  BurstMath_BarycenterForSimplexOfSize_m26CB914EE114D2DE19C4F75C97009A695F6C076A_inline (int32_t ___simplexSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		// float value = 1f / simplexSize;
		int32_t L_0 = ___simplexSize0;
		V_0 = ((float)((float)(1.0f)/(float)((float)((float)L_0))));
		// float4 center = float4.zero;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_StaticFields*)il2cpp_codegen_static_fields_for(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861_il2cpp_TypeInfo_var))->get_zero_4();
		V_1 = L_1;
		// for (int i = 0; i < simplexSize; ++i)
		V_2 = 0;
		goto IL_0020;
	}

IL_0013:
	{
		// center[i] = value;
		int32_t L_2 = V_2;
		float L_3 = V_0;
		float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&V_1), L_2, L_3, /*hidden argument*/NULL);
		// for (int i = 0; i < simplexSize; ++i)
		int32_t L_4 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0020:
	{
		// for (int i = 0; i < simplexSize; ++i)
		int32_t L_5 = V_2;
		int32_t L_6 = ___simplexSize0;
		if ((((int32_t)L_5) < ((int32_t)L_6)))
		{
			goto IL_0013;
		}
	}
	{
		// return center;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = V_1;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float4 x, float4 y) { return x.x * y.x + x.y * y.y + x.z * y.z + x.w * y.w; }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12 = ___x0;
		float L_13 = L_12.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_14 = ___y1;
		float L_15 = L_14.get_w_3();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)))), (float)((float)il2cpp_codegen_multiply((float)L_13, (float)L_15))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BurstMath_OneSidedNormal_m8551E1209D5159A5CB34E47F9D47F3FCE7186A68_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___forward0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * ___normal1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// float dot = math.dot(normal.xyz, forward.xyz);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_0 = ___normal1;
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_1;
		L_1 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_0, /*hidden argument*/NULL);
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2;
		L_2 = float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_inline((float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)(&___forward0), /*hidden argument*/NULL);
		float L_3;
		L_3 = math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (dot < 0) normal -= 2 * dot * forward;
		float L_4 = V_0;
		if ((!(((float)L_4) < ((float)(0.0f)))))
		{
			goto IL_0039;
		}
	}
	{
		// if (dot < 0) normal -= 2 * dot * forward;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_5 = ___normal1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * L_6 = ___normal1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = (*(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_6);
		float L_8 = V_0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___forward0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10;
		L_10 = float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline(((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_8)), L_9, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_11;
		L_11 = float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370_inline(L_7, L_10, /*hidden argument*/NULL);
		*(float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 *)L_5 = L_11;
	}

IL_0039:
	{
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___lhs0, float ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator / (float4 lhs, float rhs) { return new float4 (lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w / rhs); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___lhs0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___lhs0;
		float L_7 = L_6.get_z_2();
		float L_8 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___lhs0;
		float L_10 = L_9.get_w_3();
		float L_11 = ___rhs1;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), ((float)((float)L_10/(float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_floor_m6F1A809C667676D8CDF372A24CC318F90943AAC7_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 floor(float4 x) { return new float4(floor(x.x), floor(x.y), floor(x.z), floor(x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_1, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		float L_11;
		L_11 = math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline(L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___v0, const RuntimeMethod* method)
{
	{
		// this.x = v;
		float L_0 = ___v0;
		__this->set_x_0(L_0);
		// this.y = v;
		float L_1 = ___v0;
		__this->set_y_1(L_1);
		// this.z = v;
		float L_2 = ___v0;
		__this->set_z_2(L_2);
		// this.w = v;
		float L_3 = ___v0;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  math_sign_m63A39DE29A6BCB0E9BA152B458A0DD2EB9EFB23C_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float4 sign(float4 x) { return new float4(sign(x.x), sign(x.y), sign(x.z), sign(x.w)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float L_2;
		L_2 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_1, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_3 = ___x0;
		float L_4 = L_3.get_y_1();
		float L_5;
		L_5 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_4, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___x0;
		float L_7 = L_6.get_z_2();
		float L_8;
		L_8 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_7, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_9 = ___x0;
		float L_10 = L_9.get_w_3();
		float L_11;
		L_11 = math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline(L_10, /*hidden argument*/NULL);
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// public static explicit operator int4(float4 v) { return new int4(v); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_1;
		memset((&L_1), 0, sizeof(L_1));
		int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline((&L_1), L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  int4_op_Multiply_m5FC679C570C40C6A3CCF3E600DACD56D48063524_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___lhs0, int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static int4 operator * (int4 lhs, int4 rhs) { return new int4 (lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z, lhs.w * rhs.w); }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = ___lhs0;
		int32_t L_1 = L_0.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_2 = ___rhs1;
		int32_t L_3 = L_2.get_x_0();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_4 = ___lhs0;
		int32_t L_5 = L_4.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_6 = ___rhs1;
		int32_t L_7 = L_6.get_y_1();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_8 = ___lhs0;
		int32_t L_9 = L_8.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_10 = ___rhs1;
		int32_t L_11 = L_10.get_z_2();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_12 = ___lhs0;
		int32_t L_13 = L_12.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_14 = ___rhs1;
		int32_t L_15 = L_14.get_w_3();
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_16;
		memset((&L_16), 0, sizeof(L_16));
		int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_inline((&L_16), ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)L_3)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)L_7)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)L_11)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15)), /*hidden argument*/NULL);
		return L_16;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_length_mD7967FEA18B97C7AC6CFE6B0BE3D45D35D355170_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___x0, const RuntimeMethod* method)
{
	{
		// public static float length(float4 x) { return sqrt(dot(x, x)); }
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___x0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___x0;
		float L_2;
		L_2 = math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97_inline(L_0, L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_m_X_0(L_0);
		int32_t L_1 = ___y1;
		__this->set_m_Y_1(L_1);
		int32_t L_2 = ___z2;
		__this->set_m_Z_2(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___lhs0, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___rhs1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		int32_t L_0;
		L_0 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___lhs0), /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___rhs1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_2;
		L_2 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___lhs0), /*hidden argument*/NULL);
		int32_t L_3;
		L_3 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___rhs1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_4;
		L_4 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___lhs0), /*hidden argument*/NULL);
		int32_t L_5;
		L_5 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___rhs1), /*hidden argument*/NULL);
		G_B4_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0034;
	}

IL_0033:
	{
		G_B4_0 = 0;
	}

IL_0034:
	{
		V_0 = (bool)G_B4_0;
		goto IL_0037;
	}

IL_0037:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3Int_op_Implicit_mD812DEDBDE886508E86FB3222BB9DDB4949B4475_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		int32_t L_0;
		L_0 = Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___v0), /*hidden argument*/NULL);
		int32_t L_1;
		L_1 = Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___v0), /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6((Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)(&___v0), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_3), ((float)((float)L_0)), ((float)((float)L_1)), ((float)((float)L_2)), /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_inline (Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA * __this, Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  ___other0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_0 = (*(Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA *)__this);
		Vector3Int_t197C3BA05CF19F1A22D40F8AE64CD4102AFB77EA  L_1 = ___other0;
		bool L_2;
		L_2 = Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7_inline(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m89E44EE6CF0DEDCB577181F08FF3734124601592_gshared_inline (List_1_tD96ABAC4771F3D9475E31A02BF7C8946ED8C372C * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mA25B88A51127CE56A7B03A4B60C327580DB69AAF_gshared_inline (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  List_1_get_Item_m0D057F6AACD221F3F2964B904E12CEFC2695BAB8_gshared_inline (List_1_tBBAAB7C14301C91212A018A51A64F717768FFA30 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC* L_2 = (VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC*)__this->get__items_1();
		int32_t L_3 = ___index0;
		Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((VertexU5BU5D_tC5DEB36F1150A13814559F5C5BB065954928DDDC*)L_2, (int32_t)L_3);
		return (Vertex_t4AD540B2EB0411163843106AD29D6347B702A1CD )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t NativeMultilevelGrid_1_GridLevelForSize_mE1FAA4E82EE97BF5EB147361DDE627D1F85DD464_gshared_inline (float ___size0, const RuntimeMethod* method)
{
	{
		// return (int)math.ceil(math.log(math.max(size,minSize)) * 1.44269504089f);
		float L_0 = ___size0;
		float L_1;
		L_1 = math_max_mD8541933650D81292540BAFF46DE531FA1B333FC_inline((float)L_0, (float)(0.00999999978f), /*hidden argument*/NULL);
		float L_2;
		L_2 = math_log_m32923945396EF294F36A8CA9848ED0771A77F50F_inline((float)L_1, /*hidden argument*/NULL);
		float L_3;
		L_3 = math_ceil_m09F7522539A79883D220617019957CCF0CB4DA8B_inline((float)((float)il2cpp_codegen_multiply((float)L_2, (float)(1.44269502f))), /*hidden argument*/NULL);
		return (int32_t)il2cpp_codegen_cast_double_to_int<int32_t>(L_3);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float NativeMultilevelGrid_1_CellSizeOfLevel_m0D0B09A8D95D08DBD4A1DBB9923EE1B826B51459_gshared_inline (int32_t ___level0, const RuntimeMethod* method)
{
	{
		// return math.exp2(level);
		int32_t L_0 = ___level0;
		float L_1;
		L_1 = math_exp2_m91B7071A3214B73A372F626DFAA243B3C65BD230_inline((float)((float)((float)L_0)), /*hidden argument*/NULL);
		return (float)L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  Cell_1_get_Coords_mEDA78AFF80BE71C9BEBBEB48EEA10A9EA5AA41E0_gshared_inline (Cell_1_t64AA7C77B19CB29B1EB281B530350D6EEF8EE88B * __this, const RuntimeMethod* method)
{
	{
		// get { return coords; }
		int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067  L_0 = (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 )__this->get_coords_0();
		return (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 )L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ObiUtils_Swap_TisInt32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_m07926853D688DFEF2AE9948B76F7F6D54FC9F87D_gshared_inline (int32_t* ___lhs0, int32_t* ___rhs1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// T temp = lhs;
		int32_t* L_0 = ___lhs0;
		int32_t L_1 = (*(int32_t*)L_0);
		V_0 = (int32_t)L_1;
		// lhs = rhs;
		int32_t* L_2 = ___lhs0;
		int32_t* L_3 = ___rhs1;
		int32_t L_4 = (*(int32_t*)L_3);
		*(int32_t*)L_2 = L_4;
		// rhs = temp;
		int32_t* L_5 = ___rhs1;
		int32_t L_6 = V_0;
		*(int32_t*)L_5 = L_6;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void ObiUtils_Swap_TisInt32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C_mAB90511ADDFFC04F5B65A8553B95D50B7D75DFA1_gshared_inline (int32_t* ___lhs0, int32_t* ___rhs1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// T temp = lhs;
		int32_t* L_0 = ___lhs0;
		int32_t L_1 = (*(int32_t*)L_0);
		V_0 = (int32_t)L_1;
		// lhs = rhs;
		int32_t* L_2 = ___lhs0;
		int32_t* L_3 = ___rhs1;
		int32_t L_4 = (*(int32_t*)L_3);
		*(int32_t*)L_2 = L_4;
		// rhs = temp;
		int32_t* L_5 = ___rhs1;
		int32_t L_6 = V_0;
		*(int32_t*)L_5 = L_6;
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_inline (int3_t24C3AF0DAAFE1D27B75D7C9DF9ADCE4253D3D921 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	{
		// this.x = x;
		int32_t L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		int32_t L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		int32_t L_2 = ___z2;
		__this->set_z_2(L_2);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline (float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		// this.x = x;
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		// this.y = y;
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		// this.z = z;
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		// this.w = w;
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9_inline (float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___x0, float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  ___y1, const RuntimeMethod* method)
{
	{
		// public static float dot(float3 x, float3 y) { return x.x * y.x + x.y * y.y + x.z * y.z; }
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_0 = ___x0;
		float L_1 = L_0.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_2 = ___y1;
		float L_3 = L_2.get_x_0();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_4 = ___x0;
		float L_5 = L_4.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_6 = ___y1;
		float L_7 = L_6.get_y_1();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_8 = ___x0;
		float L_9 = L_8.get_z_2();
		float3_t9500D105F273B3D86BD354142E891C48FFF9F71D  L_10 = ___y1;
		float L_11 = L_10.get_z_2();
		return ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)((float)il2cpp_codegen_multiply((float)L_9, (float)L_11))));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70_inline (float ___lhs0, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___rhs1, const RuntimeMethod* method)
{
	{
		// public static float4 operator * (float lhs, float4 rhs) { return new float4 (lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, lhs * rhs.w); }
		float L_0 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_1 = ___rhs1;
		float L_2 = L_1.get_x_0();
		float L_3 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___rhs1;
		float L_5 = L_4.get_y_1();
		float L_6 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_7 = ___rhs1;
		float L_8 = L_7.get_z_2();
		float L_9 = ___lhs0;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_10 = ___rhs1;
		float L_11 = L_10.get_w_3();
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_12;
		memset((&L_12), 0, sizeof(L_12));
		float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_0, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_3, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_6, (float)L_8)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float floor(float x) { return (float)System.Math.Floor((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = floor(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611_inline (float ___x0, const RuntimeMethod* method)
{
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	{
		// public static float sign(float x) { return (x > 0.0f ? 1.0f : 0.0f) - (x < 0.0f ? 1.0f : 0.0f); }
		float L_0 = ___x0;
		if ((((float)L_0) > ((float)(0.0f))))
		{
			goto IL_000f;
		}
	}
	{
		G_B3_0 = (0.0f);
		goto IL_0014;
	}

IL_000f:
	{
		G_B3_0 = (1.0f);
	}

IL_0014:
	{
		float L_1 = ___x0;
		G_B4_0 = G_B3_0;
		if ((((float)L_1) < ((float)(0.0f))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0023;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		goto IL_0028;
	}

IL_0023:
	{
		G_B6_0 = (1.0f);
		G_B6_1 = G_B5_0;
	}

IL_0028:
	{
		return ((float)il2cpp_codegen_subtract((float)G_B6_1, (float)G_B6_0));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_inline (int4_t032A1A118EDA73DA66A2238859D535EC9B9CB067 * __this, float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  ___v0, const RuntimeMethod* method)
{
	{
		// this.x = (int)v.x;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		__this->set_x_0(il2cpp_codegen_cast_double_to_int<int32_t>(L_1));
		// this.y = (int)v.y;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		__this->set_y_1(il2cpp_codegen_cast_double_to_int<int32_t>(L_3));
		// this.z = (int)v.z;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_4 = ___v0;
		float L_5 = L_4.get_z_2();
		__this->set_z_2(il2cpp_codegen_cast_double_to_int<int32_t>(L_5));
		// this.w = (int)v.w;
		float4_tE704FC67CF9AC634EBA989ADFB15A4737CDA2861  L_6 = ___v0;
		float L_7 = L_6.get_w_3();
		__this->set_w_3(il2cpp_codegen_cast_double_to_int<int32_t>(L_7));
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float sqrt(float x) { return (float)System.Math.Sqrt((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = sqrt(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_log_m32923945396EF294F36A8CA9848ED0771A77F50F_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float log(float x) { return (float)System.Math.Log((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = log(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_ceil_m09F7522539A79883D220617019957CCF0CB4DA8B_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float ceil(float x) { return (float)System.Math.Ceiling((float)x); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = ceil(((double)((double)((float)((float)L_0)))));
		return ((float)((float)L_1));
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float math_exp2_m91B7071A3214B73A372F626DFAA243B3C65BD230_inline (float ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static float exp2(float x) { return (float)System.Math.Exp((float)x * 0.69314718f); }
		float L_0 = ___x0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = exp(((double)((double)((float)il2cpp_codegen_multiply((float)((float)((float)L_0)), (float)(0.693147182f))))));
		return ((float)((float)L_1));
	}
}
