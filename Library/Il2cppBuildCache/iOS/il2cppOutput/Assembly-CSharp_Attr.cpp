﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MinAttribute
struct MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2;
// JetBrains.Annotations.PublicAPIAttribute
struct PublicAPIAttribute_tED08342205377B135B81CB38721B80C503A76F1E;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// JetBrains.Annotations.PublicAPIAttribute
struct PublicAPIAttribute_tED08342205377B135B81CB38721B80C503A76F1E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MinAttribute
struct MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.MinAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * __this, float ___min0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void JetBrains.Annotations.PublicAPIAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PublicAPIAttribute__ctor_m4308496F0BE2C84B73141BB2D5BC5C6B019AA0B0 (PublicAPIAttribute_tED08342205377B135B81CB38721B80C503A76F1E * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_0_mA8EA2B84DA44FCE5D20EDCD476540D9FE79D7E66(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_1_mDAD7A8FE7ACACAB80270E91FFF0AFE80BA923822(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_2_m1EA55EE03648F140001BB6C0C74318DB9191BCA6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_3_m3C58176A7C17724C2C8DC3C45206A46D663235F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_4_m73A75D4D2C5FB9CE958AA0531FB9301B18BC6254(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tC756FA782B3CA630FBD1E8213E659FABD8019E36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Davinci_t292F437648307B802DF74143EB44E07158F0D541_CustomAttributesCacheGenerator_Davinci_Downloader_m15650852A45FBDAF97CC882F463B75CE7647E772(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_0_0_0_var), NULL);
	}
}
static void Davinci_t292F437648307B802DF74143EB44E07158F0D541_CustomAttributesCacheGenerator_Davinci_ImageLoader_m556EEE7C8C84B797F2FEE75F6140CC9EB92900DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_0_0_0_var), NULL);
	}
}
static void Davinci_t292F437648307B802DF74143EB44E07158F0D541_CustomAttributesCacheGenerator_Davinci_U3CstartU3Eb__37_0_m8B355C931283442F82EA487DF964E84991D80035(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38__ctor_mC0933E4939833D0C437D034A5542BAB979F0D293(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_IDisposable_Dispose_m7BDA6A678D4C61D5C65745A335E01A40C9377321(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF5289C7B9DB78D33EA79C46839A9D83A623BBEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_Collections_IEnumerator_Reset_mF1D52F18D10CB6C87FB5481732F1A1EFEFC988EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_Collections_IEnumerator_get_Current_mC9A6BA27CA0A438B9DFEAA6658C83E8F659DE96E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41__ctor_mB990C3A44F2C25ED035517217354815A4702FF57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_IDisposable_Dispose_m4E0B05BF9B4A31543CE38260AAB6B24316A762C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB39260EC0C134F7CD19D5DCF10ED803C9ACBCDCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_Reset_mA80C9BF77968EC1F27A49592D8F2FCDC749101CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_get_Current_mAB3334FDDC841DB00E54398A582A59001D2FAE97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CreateObiBP_t01CE8E59EECC8AD466168C6E08FC76D71EF27AB9_CustomAttributesCacheGenerator_CreateObiBP_AttachOBI_mE908C1B923E8EBC3C69393E6469B518FD34926EF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_0_0_0_var), NULL);
	}
}
static void CreateObiBP_t01CE8E59EECC8AD466168C6E08FC76D71EF27AB9_CustomAttributesCacheGenerator_CreateObiBP_CreateOBI_m0DFBBACA70566C2FC62BF6D5C649921563471BAE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_0_0_0_var), NULL);
	}
}
static void U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10__ctor_m430968C5EFD2BA0C9B96735FA7AC90263C382180(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_IDisposable_Dispose_mB2D097C019144FCC309F6414F7076DB3E6772006(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA42B13CC11372A044675C07BCDC32019A08A352E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_Reset_mC3C7E467B258179D51349B0F83385F1AEAECBFDE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_get_Current_m1F939691B3EADD74CDA34716DBA3FC4374AC9B15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14__ctor_m8DD675C517F191749940BE2ED1FE999EDF7E465E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_IDisposable_Dispose_mA7F079A16092731E506A9DC1FCF58913EA7C9EEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8F4E6D01EEB24AB539251D4E4711125E71EC0A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_Reset_m94B2BAABBBE38D5B38A23B14F1849F43283A7C52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_get_Current_mE32BA430181E59F701837DD6862D39C8B836F94D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_RespawnSize_mE0E3232D9F0388D51F1DB659898D20142FCE3996(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_0_0_0_var), NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_RespawnColor_mD3B295CF8C5D23AC4BDC05BCCEC5D78D1528CE3F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_0_0_0_var), NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_AvailableColors_m0D8D4CD97205390CCA950E81B8838C974B4813FD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_0_0_0_var), NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_GetAssetBundleExtra_m59ECC3DE93963BD0696FB9CA50F2CA57EF40B5EB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_0_0_0_var), NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_OnPositionContent_m52D3CE7C460401F9F2CF9E6737906F39E728D157(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_0_0_0_var), NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_0_mDC6AF6032E050DE844017BEE165EAAFBA04F7BD1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_1_mB8992DB84930877FD37617539617E9439AED0E99(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_2_m0C49CFD50E46835A000B10F07D526ACDD648838A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_3_m7649A734B850981A28B2DEF385131CCEA6981494(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_4_mB908BB1BE2E3A1BDF9E0AD81FED28253716603CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_5_m50A712DD6056870287C4A7DB9E09435FDD1993E3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_6_m7AD4F82BDD6442A8837A750FEF0FEB560EFADB92(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass45_0_t70614F4D245FA299C9ADE40EC6675823C96EFC39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45__ctor_m88BFF80AEC54A20B5231575F9D453F32FCD3B8A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_IDisposable_Dispose_m88C9FEB4B8FB6C1BB4F83458A6AE573C44B74FDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39AC1153721E7A5AE59D1A77B9900B3A6C5E5EEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_Reset_m974E70A42014423F08D1565E86B4D39C7E268EFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_get_Current_mA773B08876925350D43ECD146FEE0D954D30146C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass46_0_t28BC718D1539C9E628940DD7AB894D21A59EDD1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46__ctor_mD8855F765A5C43BEEF30F012AEB49BD1F2A748DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_IDisposable_Dispose_m249A9D780D8D886614FDED6E01EB42EBEB4A6487(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD97D6E8AAABA5C750DC8CB00D5479AC89A5BFA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_Reset_m08FE605DE550D0A3942DF553EFA3454649868582(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_get_Current_m5376A4C8A91D27D8E3BE69DBF1A0301865167299(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass48_0_t94E74C3BEC45D13787CBC620F5B23B122E9498A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48__ctor_m9ACE7EA35939C6AF56A3F09CAF876C1ADB3E0CE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_IDisposable_Dispose_m55EA1B9E30947A469F5A1B916B43EA3FD3E0AC00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE25815C4D63837B720D40C07AE702CCB69C1D38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_Reset_m22EE8729D104DD16C47EA3D39D7EFD808EAFD7D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_get_Current_m6B887B061C6F11CF5DCDBB3ED3DDFF453CE6EA09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51__ctor_mE87227D1EC63AF5A16DAFC6449A71A27698EA652(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_IDisposable_Dispose_m67B570BAA9827F4884D1A27EF57D8BCEBC9D7650(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m811274D9475214B976094D0D0AA90CBADAE30578(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_Reset_m1FC9B030B7BF99AA20630D8D4E082DD182443C29(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_get_Current_m2AA89470EDD5DAE93E38D463BE1C4D2785AA4106(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52__ctor_mFEC8E57EA5F72169E05AAAFB7FFFB7D33B910830(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_IDisposable_Dispose_m6AC2F351F789E1DA2154C3D796F41C90A7CED279(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B864B6D262CCF48697FACB821EF52279A3002AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_Reset_m1C71BEAB3372161324E40787AEFDA57E524D6F0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_get_Current_m67A830885BDDC192E16E597A6ED50A3E3C92A416(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AutoCannon_t3DC0941B475B3F8CB72889D61B0C72F147C4E74B_CustomAttributesCacheGenerator_AutoCannon_Shoot_mF63F6DA0A7FB8DA424F38A5853EAA1A6B2255444(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_0_0_0_var), NULL);
	}
}
static void U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11__ctor_m884BF51AF4DF8056D4965C028CF7CFF29DC42E43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_IDisposable_Dispose_mE4F09F48E287A53FF7E525CD9413A83AC867F28A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C060564EA19770828A39D2CF575AE4E6387CF6C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_Collections_IEnumerator_Reset_m95B67AE27CE9C309D876182ADFCB7EB45FAB2253(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_Collections_IEnumerator_get_Current_mD7F7BAED04E09C7275269837A41C3C5ACEE25825(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SackGenerator_t0188F31C70F8073B1BFE4A643903E59830ABCBB1_CustomAttributesCacheGenerator_SackGenerator_GenerateSack_m5749634500E10A9C49CC02D5B461D5CE4EC62F74(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_0_0_0_var), NULL);
	}
}
static void U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5__ctor_m60AC74DB36D70827F636F2922CB635E9B7F977F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_IDisposable_Dispose_mC3E89951FB26F5B4CE2B7E27251604B8F4376658(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m748A15AB6A05CC78B95300CA85662577A2EA5F19(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_Reset_mD87A155F07288E5B5248CE7AA284740859004F3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_get_Current_m61246B7B8E9A24805CF9F6D7ABA51935CC7BA088(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void StretchToColors_t127782874CD1A324DA1D0DC404D6052699B03AEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_0_0_0_var), NULL);
	}
}
static void AddRandomVelocity_tF413E105600EFCFDF6AE3AC9A09E3DCAFD98AF94_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void ColliderHighlighter_tC4C5C8B165DA586BA44D63D3463837713C035FC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void CollisionEventHandler_t749A22AA9632B3D4B759B2936D679FEDEE53FA19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void DebugParticleFrames_t592D11A207E1083BFAEB9A0D5C051FA7D5A5829C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_smoothness(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_linearSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_rotationalSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_distanceFromTarget(CustomAttributesCache* cache)
{
	{
		MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 * tmp = (MinAttribute_tD3485D689354D47D8686DC2891D5888FA94920A2 *)cache->attributes[0];
		MinAttribute__ctor_mE15DA1173D46E992FA92FE742686C8E28ABCDDAE(tmp, 0.0f, NULL);
	}
}
static void FPSDisplay_tB99493D73C9C04D0BE47319995B5EBF3A63E49E4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_0_0_0_var), NULL);
	}
}
static void ObiParticleCounter_t32364B0EC08FC57C6E7AAC7740676580A5102A39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void WorldSpaceGravity_tD062DD355E3F4BFA3B14AA3F76DB929A496E50D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t1BDC549E9F93220182B1A1924BE6B424E44EE3BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_SetPrefs_m930EF2A4A0384D344FBA9D9C5EACCB14125E2FB0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButNextRoutine_m491D1048FB724E16FD51D5C8FA3B6E4864E645F5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_DelItemListWait_m304E9147CFA670BF3F714A6326A4C0D40B9AEFC4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButHomeRoutine_m12A488E1C2E634DBFD20C4DF299FF5B998369AB2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButSignOutRoutine_mAD978974F12CC9E41D44B7FB3E307E70173CD761(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButPrevRoutine_mB7E7EF57C738F6AECF963DB4FADFAA20121C4228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilRecRoutine_m9994A64E295BBD9D52D828B6184F2CAC33032C0E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilTypeRoutine_mDAAE833DE05D0C9210A61E10C69BD790D1951783(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilSrcRoutine_m7A9182A31CA54285EF9BFF3C760E3A76CDED3C90(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilUpdRoutine_m4ED3B5CE0BA022BEC13C4AA0DBA0393E0C0AB507(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_StopDownloading_mFE88CA2D4154EE4AC05F5C98970D9E1EE51B4B99(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ResendRepeat_mF9585698444E6288F3D0BFEDE9E6212E7240CD06(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_tmpScroll_m86AE102F3384CE2C55BA0B5F9D750F846FD95D9F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_BeginGarmentCall_m087BB0472C9CCF6A072EFEFC1C2593AB99A16164(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ImageTexHandler_mAFE4B133389E581FD3A8C6CF2141FB043A1932BA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ImageAddingProcess_m7A5ADBEF5D1CC2AE157522E14FD7D6F104BFE1A6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FbaseQueries_mC730E8CD3FF08272E05325E76D173716C0A20A70(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ToProduct_mC0687748A4043499BEEC4066AB0A9578BEF8AE87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ImportImage_m193885C921D7C5A2ACD4867D1802C6551B6D375D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_tmp_m574D1139763D97D78638D7B17E389CC5E75A734D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_0_0_0_var), NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_0_m2096C692CD9442FC46902A0F16DC4328A918C7D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_1_mC08704029D6F7E6D445DCC23E94F12A359980080(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_3_mB54E039BC3C1F73EEA1FAB29614E7F3ECD241CC9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_4_m3E9597F4595EC78C0B9618C3F09A04B8BA85C040(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_5_m38832F72F9C87F8CC9D1500BA5733AFE8A343B35(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_6_m597E16370DAD0288AD7EED4CC61FB32F1C99F481(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_7_m65B2483950F056B5C1287834E018A66BD1DD4B43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_8_mA016DA8AAEB55699B162DD1DF66FD216946D823F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_9_mE809BBAC55ECD75FFA6A593D4143EFB84ED9C9E5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButNextRoutineU3Eb__104_0_mBF7052D323E37B73886148348762818ACE5C002E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButHomeRoutineU3Eb__106_0_m5C45AB4CC871CC952A74FDD92A52CC19E58F8072(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButSignOutRoutineU3Eb__108_0_mA7DC2D1B6D727E144DEAB10191F7F5E6F68A96A6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButPrevRoutineU3Eb__109_0_m5803B19502851521C955B2930AF40EB36847F124(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilRecRoutineU3Eb__111_0_m0F1EBEF4BE8FA985651D2F375CBD989D0EEA42F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilTypeRoutineU3Eb__112_0_m99DB59CD2666C0D6462C28AFD55D09231AAC0741(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilSrcRoutineU3Eb__113_0_m30BA0187BE2962F46D35967E21A811E8E14F5922(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilUpdRoutineU3Eb__114_0_m5D40CD366F70EA64C5CF7F0FBF39CA0F11B29FCC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStopDownloadingU3Eb__116_0_mA97AEED6FC22F8068EB6F9021BCCD84D39A30E57(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CtmpScrollU3Eb__121_0_m025E168AD6D3C68E5476FB87A5F4B7E902C67EC7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CImageAddingProcessU3Eb__126_0_m440ADB857BC9E9AEAA14DBCF5C2B78C6FA04D3B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CToProductU3Eb__134_0_mE07CC29E524AD36543895E68382EF7AA4D07132A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tF67814ECE2731FD4B792DA3AD54AFFEC51CBF1A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103__ctor_m8FB32A3EA183CB8090C3DBB50B0109656692BBAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_IDisposable_Dispose_mB4BEB73E7A346D24D2DEF57F701A013AAC8B83EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093F4D0EFF81DDC45B7624C209E883E5E07B08E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_Reset_mB60350DCF81DA37ECE9FFA39FB691C9836E3E285(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_get_Current_mCDD4AC07D175D9271B089B5B72A7BAEC106B0F1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104__ctor_m0085365472595061903314EC1E4FFF79AC9C485F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_IDisposable_Dispose_m23377156CD91B1D2CFE09DE547413B94CD1B85E3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CC7DBC65BBF1EE481C3463FCC966E79EBAB87A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_Reset_mDBC944F58A8A27B4EED103ACD698716C6F694C39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_get_Current_m9EF10872F1E79554771AD85FBB5C87D3E006067B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105__ctor_mE56DCC6198C2DA9F89186F244E620BE6A4832C24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_IDisposable_Dispose_mAFF526E03E52DD0CF05DD04626EB5F6DA87EB364(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75D67143542BF51250E417F47999162F3082217D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_Reset_m10DD860E4ED3C27E7B607E6F22AC712A8DED3025(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_get_Current_mCF63A193F61F5E9CAEE380812754088DF047E6DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106__ctor_mF0F347434E4D78C82D94BBCBC18072E688202C2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_IDisposable_Dispose_m9B496850533015E4F608FC096B9B500AC23026F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDEDE7FE05FB39A15955781EDD78073457E2CA83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_Reset_m3BB033F0512E738AEA51BFFF949335B96A414367(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m4FCD177E7D65FE61EA91F37BBA0049937F2B08BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108__ctor_mF79A9854726851E2C5773958F6873EBD682799D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_IDisposable_Dispose_m8C68B05A8AE1C61C64F9712905CC74FECD3E690D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m738D1C6B18D3CAA63D518C2B7C0D5DEE4FD8D948(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_Reset_mB3CE8FBA1B3287E592432EDC4554176C8B3E6690(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_mEE50C74269A8013E6987EB9EE0F4249E263BDE85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109__ctor_m4A20BECF42F8B7F4C026E469599A2F8860953006(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_IDisposable_Dispose_mB8627B57D8EDF2A14E07394DE8C70C5C8C559B13(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFCB870E8A5E90D88232266CDBB7CE407C60B363(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m521533485C83DBEC45F0E4E4609BE8269DEA0434(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_m9799E1C63B54F2CD96262A73C227B1F136A9D3D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111__ctor_m9F1A768D3DD7CED2C75103495721D60FDA5281DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_IDisposable_Dispose_mE9A6542D040200FB09383043C06B14A0C59EFFCD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2C803E973C3986774B9663A022FB34250DB0C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_Reset_m8C8316FC76E78AD0D70ABDF37C157B0874E4B59A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_get_Current_mCA6F124A8453688732770F731E9794591A34EE0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112__ctor_m3541807D0A5921CF9362E45C07E1B78D1F5628D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_IDisposable_Dispose_m7BB15FC367573484AAAC7342C6F8E7C805787EB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD81030DA4DF6EABEF2C3440166062FBFDC1EC41E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_Reset_m01D8D7C4430B1CB6B9D833F137FCB920DA0C41C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_get_Current_m1CED9C1E2ABEF0F14A0E4EFDE9B9E89C79B32D67(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113__ctor_m4425196A106EDB43B1640CE7D4F9F6769C0A1CA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_IDisposable_Dispose_m36F97F0CD2C466182E98033B9D28AAB0BB634CE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4503567FA847BD7C5351F587C42DC420F4C1417E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_Reset_m570D349FC4ACAF60F6A794BA378A3E009A92290D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_get_Current_m5FD53CA1FDE0E65DB09E071A5AC660772BDD5AA8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass114_0_t84C44324ECBB94FC44F18320E630291C65CC1387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114__ctor_m7E9325EBC3194C561289526D7988BFC13917F6BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_IDisposable_Dispose_mF09C79B7A5344B289C5788B9EEA124E6960B4FA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D53B015E411EAB5F586060E9184710FC2B3CEBC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_Reset_m4A15596167AA539664B1C694F372712A981963CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_get_Current_mC7DDE1817B9D41C593B61E33C0F6E76DE1E213FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116__ctor_mB8BC0F77581ED0D22C2DC3707858178F36FA1E1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_IDisposable_Dispose_m9A2B019169627DC744778F6A581A1341FBBBF5BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFAF6C8391D7EF16F8A34DA187B9F19859F769A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_Reset_mC9B5D5C32A29DA3C1F6822956056ADC748174746(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_get_Current_mB8DD0110D702CB79F468A1D31EDE5C4DBA965AEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118__ctor_mFBD5278226634B801677A62FE848BDDBC9D38DA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_IDisposable_Dispose_m5C96DFF10E9036353B869A60C5B1DC6C77103A8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D4886779C7D6108537AFC05D1F84B5960FB557B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_Reset_m005EF473A7FE5497F43D0746E0ECCC6C416EB77D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_get_Current_m23FB6E14BEDD84646381D9C2756B6A1DD077C340(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121__ctor_mC57CFA92A60222C607BBFE9DD9070ECE9FC02308(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_IDisposable_Dispose_m058B5042FFD6D072E890FD42FDC4685BF9D90978(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14121AE1F25F2547E709F627B873F6732220D5AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_Reset_m8553DC27B6409578E06916683E3CEC4170FDAAB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_get_Current_m1CCB279ED7CB5FD1F214F85541F1BE979D762957(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122__ctor_mF1D34C8ED14D2838ACB9179BFF9B8E25060E347E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_IDisposable_Dispose_mC2A7529A5F4F8E37EF2543B0160108DAB4BE5EED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC18C03DECED7724F0A8C879A81A799DE616B8884(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_Reset_m33B50615A9C70037B8F1BFAB67DAB62019C6611C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_get_Current_mD4DB387972B3BB6B64DE01FDBF515D72DDFCD398(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125__ctor_m4DC549290A14DEE1FD6853C90B3D95424FE0BA2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_IDisposable_Dispose_mB1A2DA2D622318EF7C7882E054DA421CE22421EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54E7FF932D9D77128ED4C58634C42ADBF5010C96(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_Reset_mCF9361949B92FA15B21927BD9391E205FBC8A0F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_get_Current_m4FE98BC68F8422625EC9F9C5B37277F30787E340(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126__ctor_m8FA7206E28AE6338E7F743FD336B4F1EA75A6124(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_IDisposable_Dispose_m49B8A7C618AE71A995D3411AC554ACD5F2232700(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC465F56FE788453CE681703A3E164BF809528187(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_Reset_m0377E1B1D0E57425A4B765F434124AA99B952FF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_get_Current_m8444CD45133F500A05CCD3393B8594E45FBB4241(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass128_0_tE46D33ABE827F4FC858BABF35A02713BF31451D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128__ctor_m25DB5088A39499E23E6A2A1196C7B42564F860F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_IDisposable_Dispose_m6D6B952EC69EAE27B82D5C14AE57A6B4B87A01B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7808CAD7B133848AAF612E1D96BF5F2E0E492AF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_Reset_m78FBE24F99396A51AB662A48447758AB67DE39A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_get_Current_mA5F4F972647F1DC794480D6E4908D4F4550E20F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134__ctor_m9900F913A83872687C721DCB5D9B714B700A1079(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_IDisposable_Dispose_m397680E3FE53C7724B8685B74462B56C920316F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m246861321A3C383AABF3B464F205A15A62B12B00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_Collections_IEnumerator_Reset_mBB707E3B20E4A99F1327035DA657A128F51AB0CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_Collections_IEnumerator_get_Current_mA0865F6EACB8D266C253B56FFFF15B13AD085323(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass135_0_tEC403547FD1C9AA3D0A5FEDA76FDA6DAFB5D9F4D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass136_0_tBD96319AA52D96DED074836D043A77488E293AC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136__ctor_m18CB1E44865D2185DC631E553BBE9CB761AD5195(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_IDisposable_Dispose_m9BBAB55890CF9448000562B794994367EAFD12E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43399C0073CD2064ABA95AC6724AA31CCDA971D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_Collections_IEnumerator_Reset_m8B0A961330D62896C46FA5268C1506A17F5F32CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_Collections_IEnumerator_get_Current_mDAD3A362FA8B96F11901C0389A2E0F1AE02C5D26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137__ctor_m64A892AAFE35BB0DB81EBC891907BAD699F14A11(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_IDisposable_Dispose_m038DB0E832F38E7D0A7438FBFD21621EE4D66A9F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FECF7697F97244EE9AC8637AA540E38264CF7FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_Collections_IEnumerator_Reset_m8771E3D830378C9D1BD0A73DB43CA451AA8718CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_Collections_IEnumerator_get_Current_m7FC964576C9960A08657AB667982AC5291A27D51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass19_0_t1E5B86B697DFDA6ADAC794D52CB25736C6378294_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_SetPrefs_mB24E9E1899BEF44D6C33A153F63B240862831A89(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButNextRoutine_mC8332E00005A39E745D65C9ABCE09820CDED8AF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_DelItemListWait_m34607DECD00D5A8203DD0C9B94A136F0D8C07A43(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButHomeRoutine_mBDA4FBC56EFB11901F98BF56D39FD62DF1BF5DB9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButSignOutRoutine_mDB66FA14184594255315C663B57AEDC770551A90(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButPrevRoutine_m179C7214D4B09084473108EF4B89F611FFC104B8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_FilSrcRoutine_m7F3CFD187CE0378C12B5016ED5E472362C740472(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_FilUpdRoutine_mAB5BA98A6452BA0212A652CFFC4BFB301782F528(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_StopDownloading_m3811835D137C6DF0D3E289687A00193434FF42ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ResendRepeat_m33ACCF6A21E700039301FDCB1F057F8DC0E646B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_tmpScroll_m7C77D6633C3C6234DCEA3B50B9ADAB6145D6D743(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_BeginGarmentCall_mFD0B654FDB658DCFA40C758EADA73B06943ADF53(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ImageTexHandler_m6CC6A14F463CAA419779D706DCE0EF37BAA578DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ImageAddingProcess_m15B9C3BB4D6B081000AB2D595049CA71D5B32FFB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_FbaseQueries_m4D8C6B4A237936B4ED4475DC22E360BB5364D55F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ToProduct_mA72463E586F425D9C1E93F6C8FAAEF78CC08D407(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ImportImage_mD2F4F737D7C782E165170EED54985C283FF55D13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_tmp_m7751B13DC3FCE7A89EE6396F031D7585480E7509(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_0_0_0_var), NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_0_mA8D5B273FC5BA5D94E2BF1ED4497CE6A3DAE824D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_1_m3EF20EAD20C93BB7BE66740C163360D43939729B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_2_mC9A25F8B804079C5FFB41CD8B5870F3AAAAD74DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_3_mE3A548D498C7971A9E09A67EB2D3A53490C1A218(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_4_mE059827DCAFF42E96F366E0D5A6ABE6DE6218C4E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_5_mCCE5DFBA80917F86A0D33F8F95A90EE68FBD7B8C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_6_m74B31F18DD846810AA503A5E18DFC3338A1EA903(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_7_mEF9860D29D5CE0D20BEA43B5CD887B4C52EFE442(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_8_m79681213EFBBA835E69B65411256690670A91739(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_9_m555BE782F45EBF179B9BABE54B16492B6CEB8812(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButNextRoutineU3Eb__101_0_mF0329110041A2D13DD902A94C366BF88A5BB1153(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButHomeRoutineU3Eb__103_0_m3DD2F91A3E0847798137288B2BD5200AD3743A73(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButSignOutRoutineU3Eb__105_0_mBAC844154801247AF08098C35F5FF884679820A7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButPrevRoutineU3Eb__106_0_m9C4A5DA6DE7430FFD2CA7B92EF9FF19A2390BCD3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CFilSrcRoutineU3Eb__108_0_mA644BDEF06923340BC7780C8B217A4A256DDB23A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CFilUpdRoutineU3Eb__109_0_m49BB9EC280F925B74A31AA25ECC4574E69B5F686(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStopDownloadingU3Eb__112_0_mB21FEE923B57F6D9C1FFA163A7A72EACE957ECD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CtmpScrollU3Eb__117_0_m40BA1F8A9DBBFF062BFA53257DC02DFE9CD623EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CImageAddingProcessU3Eb__123_0_mFF88D401B07CC1CDC0FD84D028C5E4A0AD0EB31A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CToProductU3Eb__128_0_m12AB57D8327CC94A4ACF2D61557E599DAC507D78(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100__ctor_m76E7D60293C830A2EB67DE51B7781AE61C4BE76B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_IDisposable_Dispose_m7A07429A4ACA08DAF578053504AAAEDD40BE443E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09ECBE0E3957BD7BE662F0A0D8F1423E702C59D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_Reset_mBF33BE7CA5F9C5F1DA7EF54857F51C9E81D9ABEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_get_Current_m7891B9920CFC41570FFE0F5564EFA91AF3E18D7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101__ctor_m770F9523CADF1BEF7FF7C16013118EF432B0C662(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_IDisposable_Dispose_m8A144B4DA6125320A42AF0152CB56D7085711F64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE566BDB6564C589FD9A492BDAF211E8BE551926A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_Reset_m0ECC8CDBA0AB2C92963F60C9C5ACF1D68091BCF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_get_Current_m88FF3464C964E918C4D1B4563158F2E2742BF34B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102__ctor_mE4FD584463368E858E6500444E13560D39A8E7E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_IDisposable_Dispose_m84599E19DEB6523032C1B366CE872B1CAD3F029D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E1971853F5E1897DE572CFE2A1F5CE93DCC2E11(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_Reset_m9AC6364FB08D83A09BA4918A401DADF38A842113(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_get_Current_m9A7B9240C6F6E8EFA321E3470395DD66C24C5F9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103__ctor_m8833F61EF7E0A7572E454A323DF97D20BA2588B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_IDisposable_Dispose_m7EEEB03EB525F93260C6A019B8A907A9FEBB3F8F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C82185097BE3FEAB452F9C1ABD34D4DCBFF679C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_Reset_mE8665766A9A6D613DAE425A1054D00FE793FA5EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_get_Current_mD32884E4815C7AEA078E5202A3399B8999BB570E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105__ctor_m007F2C5053AA977BE6D4BA0F4B26EF8168F62BCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_IDisposable_Dispose_mCDB3459813B11DBED7B42C011506ACB7F3FA7FDD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC0D8BF7401FD148B2B40B4D9BF77090FDEE7406(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_Reset_m1BEDE53990B83198CB62F9F2246E3AB05D7286F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_get_Current_m4768AB7E02D5CD67EEF47DCFE270C258F0454E7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106__ctor_mD5DB2A5A9B55B135C05747812341B7DEF3C10503(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_IDisposable_Dispose_m41E2986903131B9738B1D226DE03735A5E5A8393(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m677A17883231084C84F5D047340EEBA04A75E6CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_Reset_mF8889B67CDD20BE3EC695C1369D51E699AF71F59(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m6A78883AB5236917A0C8EEECDB55B0650B83138F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108__ctor_m4A95C6E500D70B86FD6E61E285BA194978AACC8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_IDisposable_Dispose_m02E3089371909C6F27EE212E7AEFC5AD54F45A63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF2BEEC524BF85436365222AE9DCE459A093759C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_Reset_m1C0E3E34FE3BFF6CE43A3C4711E7498BCB8026C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_m6C97BB788CFF7957C613C778C2B2224FEB864D37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass109_0_t67B405568BFB88833DB85FBDB04F0C19168294BF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tBBD2A55ED7740F67094538B3EDC2B960AA0121B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109__ctor_m996A8F53737D39937922BAD80D230D2CDA6C1D9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_IDisposable_Dispose_m076118330E20CBC1EB2648A81B6B1C1E97160BB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m851CADA05D65739DDA283DC975A580F7DDD0CA2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m4E5E5700E9A4F96411B9DD5C3F3E0C1D318277B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_mAB074BD2F4B1C09BDD6D48EFFC3AF7E73D1655A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112__ctor_mF6C2EC9ACE60A4839D8D46C1C43BC6281C03AEF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_IDisposable_Dispose_m0917D6FA891909094BE58CCE929228AB202B62FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DD6FFCB5D8649CB1E542FD833A680725C891CD6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_Reset_mA01D081F3449DBD67B05E87BB4B63E3333994878(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_get_Current_m6434132FC738B3A11CFAACD960F507329B563756(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114__ctor_m3F5BFEDD90E83C5794F3D831B106A8B58CDC04B8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_IDisposable_Dispose_mC1A4224C9CD3D4C57EA721AF4825AE14184C37B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m719B58A1C0991C1BA3B02879D4A1A20254AAB6C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_Reset_m5FCD37DD1EE430EDECE9ED6DAB1B6A56E04C2392(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_get_Current_mA40A27CD9848559171D6093EFB1937A3DCE29D7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117__ctor_m7C268731D5F8756B86633FC062CCC952B6740FE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_IDisposable_Dispose_m831B53BB37B2E9240405FA9BE9F1076F02087F06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m224B1BD9D4CD4FB193F8A17EC08A02C6E8ECE348(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_Reset_mD408582C33E6F0343EA87EC85DC6FD882F3B218A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_get_Current_m7601D10039050BAF1A5EC3F95CF7F794E28876C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118__ctor_mFF9ECD2897932FB44B8FDED5ED73D79BD14E8E8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_IDisposable_Dispose_m4F58A0CDBC68DEF08EE185191E23BE180CD4AD34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27291F3DF06BA60F244574ABBB6F46214B0DA02F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_Reset_m195F2702399FBC0ADC51B2205A1029553D4E87ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_get_Current_mFCDE05B088B0E75E0D13F80DFA5BACD20AF5FF74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122__ctor_mF7419C5E9365A97FB7C1DA3B0424FDDD07887E25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_IDisposable_Dispose_m9AA69120AE92EE8525CD5258A84ECCB851F6A777(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91B0112E2D4FBDA7C9BB2CE5BB6011D358ED2A80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_Reset_m5BF7D635AFBE4EB47303002CB2C6AF54DB25E364(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_get_Current_mA1A91EE97DA546664D9806E5363E4A5D640B1F87(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123__ctor_mD95084F0EADFB19F6539589548840D6AD193B303(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_IDisposable_Dispose_m575C4A8E4B54ABC855E1869356DBE5701BBCFC6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8059792712242D80BA76F0E9D00EC3DE621E69E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_Reset_m13A4AEA4FB51F19C57232D20FE71D9D0E95F965D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_get_Current_mB06D6DA393684E65C8B457E41A43D251E15063C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass125_0_tE4D82853EB011B0201238AFDEA4ECB5249814BA1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125__ctor_m0021C3BB0BEFE90B6EC45561DF67D8B90CC2F05D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_IDisposable_Dispose_m16D84B535CF2EE6EBC48C2BEE44FACBB6172BD57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m482847F1098B4057D8AE595F730B5DB8CBD9CA56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_Reset_m5A083EF573E4B42EF6EABF3A0F000DB6A9D1970C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_get_Current_mA7E00797E8DF86728C49DC78C62E20956683FAD6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128__ctor_m6A9A821B8BABFB4F8CE697657BB2FA1225B4082E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_IDisposable_Dispose_mAA22CFB62E9C7491744FC997FF1567091C6A754A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2014580B242F8751BBB63797DD261AC125C09DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_Collections_IEnumerator_Reset_m3C019D7B395DA65959C6C26F5CDD692641DC1156(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_Collections_IEnumerator_get_Current_m866CBF81E26395A49826034FB021CE050BDCCF03(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass133_0_t0157BA17404E5E1F7FC1E44B1309E82A3F7FB68D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass134_0_tFEE258E896F3AAA907E6CB179096FD7C98077B00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134__ctor_mE4C6E2BC19C6F1318CB4F0BAB54870E62A778064(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_IDisposable_Dispose_m799A659DD685A41779A1E5664864393915A0AF8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96D8D603CFA9A391F5A246114AEFEEDF6664C9F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_Collections_IEnumerator_Reset_m0005C3A33E2057DF0A8C59A47855A44B385D0865(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_Collections_IEnumerator_get_Current_m2B7726DF26F380FDA790258265D8E5E2831759D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135__ctor_mE8EA412BD2B6E55EA20FCA793A6446F4CF18BAE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_IDisposable_Dispose_m800554F20A5F13ABE7D22BAE6F37B0FF286CAF32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9363254BF1666CFC6D4530829C09BC31CF242E00(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_Collections_IEnumerator_Reset_m4045FFCC29548CDAAD61AF52B90AEB91B3090987(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_Collections_IEnumerator_get_Current_mFE3562A87D8848233B9A20676B6178F80562E0A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_buybut(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_ButHomeRoutine_m3A610393D49D4219306D680960B890A93A91EB14(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_0_0_0_var), NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_ButSignOutRoutine_m512500DFBFFC99B2ECDA6B0E017427098D8440F3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_0_0_0_var), NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_CleanTex_m8F1CD069B973B2F305B451EBC3092A3272A2FE1A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_0_0_0_var), NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_ImageAddingProcess_mE0C64BBDA23A454F59F6386BCD424777079E19AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_0_0_0_var), NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CStartU3Eb__27_0_m320F1D837FDB0993585F54FBEE60486E106C47E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CStartU3Eb__27_1_m58A40C02BF1EDD867CECF3998ADB67E23739EFD5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CButHomeRoutineU3Eb__28_0_m0421464A105FB4632D590F221E4A092B9EFABA41(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CButSignOutRoutineU3Eb__29_0_mECADDAC0D393D555328D4CEB84A4AA54BA90FB2B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CImageAddingProcessU3Eb__32_0_mE5C01CEF59A35D36831ACDE62AB012B49058645C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CImageAddingProcessU3Eb__32_1_m6FBB2D3F4AF2D56D3F48FD456C2FF6E4D23A0150(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CImageAddingProcessU3Eb__32_2_m773DFB3E50C1620401ACB394E2E213DE9E0F4E23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28__ctor_mB4504AB9D5F362706B1B45DF56DE52D86107543F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_IDisposable_Dispose_mF7FA8E483FB630014A420E312019C99A4C91630A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CB4964F1334604A1DE3B425844529B9333DA1F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_Reset_m0A2FE0CD2B6E84F803236ADE6B38B5E67E80858D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_get_Current_mFE4C850F5F097C4BDE565B8F00258284878BBADB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29__ctor_mE815D9176BA4FE8E1196967DE1D6D0E66D3AB8ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_IDisposable_Dispose_mD297951099D5E22898EB1BF799A729585C7122E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB6552AC06A51E5E351C52D5EA3F576AC9DF591A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_Reset_m9AAD08A86CC8BF57733370B1172044DAA919B197(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_get_Current_m3F0632BD67CBA8E8A98ECCAD924769029E3994C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30__ctor_m0519A26FB715C19DB3B1BC92370C8546FFC96ADC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_IDisposable_Dispose_mF5AE38C1BF625A1711988EA9BDB0257714C52C7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4518055E3B70544495786AD3A5BB96DF4AA684C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_Collections_IEnumerator_Reset_m569B1DADFD37C2B6E6EB20CDE72DA244FB9C72D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_Collections_IEnumerator_get_Current_mECC037CFDCC6AC9FF87C1D02AAA62AA5D30465FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32__ctor_m854CE612288CB526EA88059B6A9075B463749D16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_IDisposable_Dispose_mFF956FF062FDF19085C086812D7323C48DD38F74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0F9BCD72DD34F2D3C1E470C814BDEF4ABDD0018(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_Reset_mDD12A1D00339C63FCE4232072A6A0140F2415C2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_get_Current_m4BA238ED68F161B93A9D59108075FB6AFC756CCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t8987DDC1C9D5D49E7AB590FF5FAC74478332B94B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t8987DDC1C9D5D49E7AB590FF5FAC74478332B94B_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eg__ViewLoadU7C2_mDE7ABBD9C4B3FEE0C399BE88F28C36D50796E910(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_0_0_0_var), NULL);
	}
}
static void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed__ctor_mFFC5CDFA51BD0578917E8B7D206E899CCE14443E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_IDisposable_Dispose_mF6EF7AE7E4453DA1F88CFDC0B67151108164418E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F167D0D324A297085FF7718472A0DA97810714E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_Reset_m51DA00B6F13F0F29EA10E6464B2994FC00F3A643(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_get_Current_mD147C9A9E2ADE062EE4942F87F7BB7448FBCA02C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t7916A5849C80430E8C7F116229BB3F4D75C11C78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_inputMailObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_inputPassObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_AuthLogIn_U3CStartU3Eb__5_0_mD050B6932D242E0CC487AA1625BC28B7F8973C25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_AuthLogIn_U3CStartU3Eb__5_1_mA894BEB560F0D5DE6DC808C3EBDC62F0C38DD8F1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthUIManager_t0D070C29FF1EA724087AB61768A2120E5A84A373_CustomAttributesCacheGenerator_loginUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BoneController_tFB1633B689BC777BF4A5BEE01B90A482A0B59FAE_CustomAttributesCacheGenerator_m_SkeletonRoot(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x72\x6F\x6F\x74\x20\x62\x6F\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x6B\x65\x6C\x65\x74\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_inputMailObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_inputPassObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_inputPassConObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_0_m8493AE06DE46BB2D14988EFCCED4A6F704ED723A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_1_m279DBDEDDFFE381FDAD7941FCAE37CB8C80CEE77(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_2_m4C82B7B7FFC9F7DE9D05BBC79E8094FEE9B59881(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_3_m0EDEE6B256DBFDCE07F6557CCB638B68C1C4F081(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_4_mFCD9F25CEA26FAB2B305E94A54D9DF59591EF696(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_5_m1AEDA456989B62CAAD3DC6FD724F8F321182ECB4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_6_mADFF5EB61973959631C2F5FD92E7D65D5D99B8F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DropdownFilOverride_t0623B05A6DF67686746EC7FB32AD7B507664E010_CustomAttributesCacheGenerator__placeHolder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownOverride_t9998C5605A1AC9864677FFDCFC5E280388356966_CustomAttributesCacheGenerator__placeHolder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_UpdateInfo_m1233C2A1B3EAD9AACFA1A17F6ADE9950491F9A2E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_0_0_0_var), NULL);
	}
}
static void DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_U3CStartU3Eb__16_0_mD5E5F262F4764E4BDD121F758249F5B07B318230(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_U3CStartU3Eb__16_1_m4C1733F6C750ACFCA947DDDF8E0C70C3F3197A98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_U3CStartU3Eb__16_2_mAA43DE9BCFE604E3E8ABAF9F8B3D390205896289(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t667C4E47B3AA7484773B7EE72F428C3C39C27D57_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tBA21EFFF3E8D35FE4F548E7F96F2ABC45BC4AB71_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17__ctor_m3FEF25B5AD5BC513EA9D5F77465BD37D6D43EB50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_IDisposable_Dispose_m16C5556EAC8EDC7494F39C587FD33B54F4AAD8B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB4C894555B34B6737C66E1D17ECBE9DB1C7927F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_Reset_m253F46F270B3551A1D33007329863296A8A95CAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_get_Current_mA27DF123CD6808FFCFF01A6410EDF7D56732C434(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DrpDwnReg_t0AEF42AB1E23FBC5836DEBF8F8ED71CC5F300FEE_CustomAttributesCacheGenerator_DrpDwnReg_U3CStartU3Eb__10_0_m37F2FF9FA3B44096345696AB66223D8869443F40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseLoad_t7C29D9D5D2C6F5B73B81D42D369F8ED8936B983C_CustomAttributesCacheGenerator_FirebaseLoad_ARDocsFV_m723E735998B8B4D20AAA96EB69FAC26E117084B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_0_0_0_var), NULL);
	}
}
static void FirebaseLoad_t7C29D9D5D2C6F5B73B81D42D369F8ED8936B983C_CustomAttributesCacheGenerator_FirebaseLoad_ARDoc_mE3C0EA4A11B5C67EF5AF8BAFD0D5BEA999FBB461(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_tA96F2015F23A2772E3826FB634E91C20FDBE7DB4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_1_tDFDC22E4DBC40AA616B5C200E065ABE77752AB1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30__ctor_m75CDDE36D3FFCC64B53247BA6FFF09812B19B611(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_IDisposable_Dispose_mCA30BA2BD4B04206E965F6EB72F13B58B4774741(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5700A999E0CADC5394B66BAAEC398D681B28FDC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_Reset_m133F4D7C73219BD846D577CA416A3FD3E71F788B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_get_Current_mA1F3A732F1D8F4FA9FD6BF4BBC3ED3E58DED5292(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_0_t4A6D23927CE1393EEE85B45946CC891C7F2878F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_1_t4AAF52C02358EB2FEFEA710520B36772D98FDA74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_2_tECEB163B0B3FCE7ECCEE31307B66B2B0E4A8A35A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_3_t4F2EBE23A42C32A3B2A7361F170422FD55DAE5CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass33_4_tC88FAA09A99B8295561BA2AB17BE4F1E3457356C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33__ctor_m960E0A911B1A28A79ACDB501FAC11582CDC61C74(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_IDisposable_Dispose_m1F7771C8269333EDF80C8A0A9BC6900F7026A6B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A477DE8915FCE6C5B41631BBF0A1149DF46AA5E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_Collections_IEnumerator_Reset_m829D659EDD2CDD8D554D68CA96FFBFB260607642(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_Collections_IEnumerator_get_Current_m488B0F68F860C0A9E1F27F591DD7E840B49E9DE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FirebaseLoadProduct_t21E2DE2EF93B9DE0EF26C57AE1EDFE54CC01A0CD_CustomAttributesCacheGenerator_FirebaseLoadProduct_SDoc_m3568BA13939C82452402248225F7027FAC287E6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t6942DABFCC5FC53736DD06B7F4DDD293F24ED189_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6__ctor_m11F634D3BA0FE2F0429A352773685482007C1CA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_IDisposable_Dispose_m45D6F15DC5EFC39587FBFEC0379E79580C78CB1A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FDA9AE366B9131C3E4028C87E0F19AC0E02DFB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_Collections_IEnumerator_Reset_m307C49A52A70B1F44B5E4422D634A0CF460E5285(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_Collections_IEnumerator_get_Current_m19E7A850C40FA8D83DB0085945AB5D14CDF3F34B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_auth(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x72\x65\x62\x61\x73\x65"), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_loginEmail(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x67\x69\x6E\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[1];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 5.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_loginPassword(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_loginOutputText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerEmail(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x67\x69\x73\x74\x65\x72\x20\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 5.0f, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerPassword(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerConfirmPassword(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerOutputText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_sizeDropdown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_genderDropdown(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_termsToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CheckAndFixDependencies_mCDC8E2754EC8ECC9B3E0DC26278D64E9077899C1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_checkAutoLogin_mF4122E6817DA0E8B51DB444A81149C836D75F1CE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_LoginLogic_m57B61E650C749A3D740CE8E03A35F1F099EF9A4F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_RegisterLogic_m30CBFD9C2A3A498E6A7427FC73DA16B03DDB80E6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass20_0_tD9ECD7724A5FEEE7398CC64729BB056CCEE59860_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20__ctor_mE513235E9CEFAD28389F4BD273B0234A2546E9A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_IDisposable_Dispose_m50897C509C5058F4E638DB16A8BCD88C2262F6AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66C8BEDCAFFA24358C0D1DCA597C7DD35CF55F50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_Reset_m0ADA09A0EE9657765B6A84B223956B114B8A8479(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_get_Current_m1FC420C47DEF8F1B4127A3E12EB95BB1D8AD6AB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_t7BA4498DD3043CCD1EDFD9830D0047F82E2435EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22__ctor_m872281A15157F52029A9E8A929C8AC92CBA9C840(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_IDisposable_Dispose_mF912F49AC2DCF9900F49A2BBCF2FA99834A0F8B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99FE1B44D48593D6CA001BBDCD7F9857C6665444(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_Reset_mDBD4A544E27C49C79F8AD57FC51E2F9818F0AB0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_get_Current_mB0390E85E68A1251F204074331BB25608E431ACF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t4C59DE6ABFF6168BC92A9597A7E76B0F13253648_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29__ctor_m237FCABA55986243AE341530B887A7B9CFECF3B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_IDisposable_Dispose_m56262A632D7BBE88A4049CDE4E61400A42DE5411(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B518C7141CFA0652289ECD3B525D0285655B7D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_Reset_m7A22EA515F8C3136ED8677F16282E19F0D98DDD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_get_Current_mCBABEFDD04C2BC6495F7DB8329606E5E19172F38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t3BE164DEBA94F18AB86B37549000AADD41D1FA70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_1_t5F903D9D7E2B2CB4A1DCD53F73952E9E1C0DF4E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tFC10E533E4B7DACED888E6F7E6BA060A31831CBE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30__ctor_mB79F27786D67ED3D68638817B1BD0B2089F1FDBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_IDisposable_Dispose_mB5B9C8F6193294A31E5211AADE5344B5C0F68D14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABB3EA176CD17130B14FB881A35DD2D15C6C4A6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_Reset_m777E21375ABD9F78DD640A7188273C00A51F78AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_get_Current_mBBB8E1D2A536AFC119B513D81CE9ED58929E784D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GarmPop_t975DBE2C427EE5E82338538D38351AAE6AC49B56_CustomAttributesCacheGenerator_GarmPop_DownloadImage_m84173F9995B260824C08FC1C11F8B4C8035E3AC2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_t22627AD4A37DDB4571F5158E8CAED841EBCE7F0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27__ctor_m4A14D0747572B8EE136B45D12D121AFE2162C2C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_IDisposable_Dispose_mF6799C18193605A8BE990E1BEFAF3A3A920D975F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2B5B8ABD582D8F5B844A24F13A3C5B25B52AE1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_Reset_m77E7F96BBC7786A52B73A5EFCA9BEC65D739CC29(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_get_Current_m44C8F7E23295F58BC5D19979F36A056B1A68C76D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_avatarOcc(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x76\x61\x74\x61\x72\x20\x4D\x6F\x64\x65"), NULL);
	}
}
static void HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_m_HumanBodyManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x52\x48\x75\x6D\x61\x6E\x42\x6F\x64\x79\x4D\x61\x6E\x61\x67\x65\x72\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x70\x72\x6F\x64\x75\x63\x65\x20\x62\x6F\x64\x79\x20\x74\x72\x61\x63\x6B\x69\x6E\x67\x20\x65\x76\x65\x6E\x74\x73\x2E"), NULL);
	}
}
static void HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_HumanBodyTracker_InitAr_mBEB9EA30F3D57CBB6B997C95EE4E5860A59065DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_0_0_0_var), NULL);
	}
}
static void HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_HumanBodyTracker_GetAssetBundle_m88CEBCF31113436C0DEEBE6E8FB712F4F4E7871D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_0_0_0_var), NULL);
	}
}
static void HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_HumanBodyTracker_U3CInitArU3Eb__14_1_m4DE5C014E90FB1395DECCE1D36220252B78B7BF4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t1CDFED34CA29A2ABC1D369F6FF77E48C93D5DE14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14__ctor_m0BCE60FC071A5BB60B68A3F58582F4072B03F4A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_IDisposable_Dispose_m2D6CADDAA2F5A47F381E9C8197F4E3DE4998CC30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5ECF5BB94B05D7C55A84197D235473F98DFAD1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_Collections_IEnumerator_Reset_mDAADBFE05CAAB470F57C48D2380BD54640B285A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_Collections_IEnumerator_get_Current_mB4952B1DD52B1A6B63B24723EA9B6C6E5704EE9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15__ctor_m401E6206298334CA59DD8FAD9E64B78A6D1DE8FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_IDisposable_Dispose_m0F4FD2EE83D390DD3506608D143E0521AE2B902C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m281ECDE9CA2D3588CE643FDC7865D477D53C70FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_Reset_mC0ADC36AB218452B7F2409B765C99D0D82E8EB8F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_get_Current_m6419ED791E01A27B653D693CD250F4BAE9699232(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HumanBodyTrackerAvatar_tC4C1205BD20B8913B2130223D1AB28BBB2CB9122_CustomAttributesCacheGenerator_avatarOcc(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x76\x61\x74\x61\x72\x20\x4D\x6F\x64\x65"), NULL);
	}
}
static void HumanBodyTrackerAvatar_tC4C1205BD20B8913B2130223D1AB28BBB2CB9122_CustomAttributesCacheGenerator_m_HumanBodyManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x52\x48\x75\x6D\x61\x6E\x42\x6F\x64\x79\x4D\x61\x6E\x61\x67\x65\x72\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x70\x72\x6F\x64\x75\x63\x65\x20\x62\x6F\x64\x79\x20\x74\x72\x61\x63\x6B\x69\x6E\x67\x20\x65\x76\x65\x6E\x74\x73\x2E"), NULL);
	}
}
static void HumanBodyTrackerAvatar_tC4C1205BD20B8913B2130223D1AB28BBB2CB9122_CustomAttributesCacheGenerator_HumanBodyTrackerAvatar_InitAr_mBB6CB82039FBE01973C0A9FEDD0727EAEE8430DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_0_0_0_var), NULL);
	}
}
static void U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17__ctor_m5D62044BEC329579BDD4C8F4BAE9B64F1A9AF6A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_IDisposable_Dispose_mC04A34D6A9AA9D0ADE679858B6A0CF8EDE9098C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD40A5A4069316DB30BB2F1611247BDBD1F5345F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_Collections_IEnumerator_Reset_m95F44591F726BD047A6BB8920E7B62C2CCCDD776(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_Collections_IEnumerator_get_Current_m6017680888DB02B9B20F061F4B8C9EC90E1562D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_avatarOcc(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x76\x61\x74\x61\x72\x20\x4D\x6F\x64\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_m_HumanBodyManager(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x41\x52\x48\x75\x6D\x61\x6E\x42\x6F\x64\x79\x4D\x61\x6E\x61\x67\x65\x72\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x70\x72\x6F\x64\x75\x63\x65\x20\x62\x6F\x64\x79\x20\x74\x72\x61\x63\x6B\x69\x6E\x67\x20\x65\x76\x65\x6E\x74\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_AvailableColors_mC1C04209D418DE6B950FB40DC296DB41724E6CF4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_0_0_0_var), NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_InitAr_mD74EBB8D82EB0E679796BC4561B5C1AD66CAEA99(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_0_0_0_var), NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_GetAssetBundleInit_mC8408C8F96732956774018F9279CF136A3DB118B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_0_0_0_var), NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_RespawnColor_m1AF0CD10B47DBF05E07FB6CB2DD5E7EFC91C86B7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_0_0_0_var), NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_RespawnSize_m187FE19EF6C2040513748D61E17797E13C3B8A34(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_0_0_0_var), NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_GetAssetBundleExtra_m6BA170E938C7A8E18D07F75EBFFE195A7D0AA8C4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_0_0_0_var), NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_U3CInitArU3Eb__41_0_m77EFB7D5E455F11427FFE4DEB5A5E7DFCE0DB687(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_U3CInitArU3Eb__41_1_mC654DD0D05916ACE0CFBF0C58EFCAC4876750B07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_U3CInitArU3Eb__41_2_m9D21EB7E7EC5693B339E241A00BB7A0FA5427BA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_tD5088565E378D09EBEC4586123404E30C93F3BCA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39__ctor_m0E7ED7000437E952D8E68D1C62B1316AAA31A049(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_IDisposable_Dispose_mDBC0CA875800EF0E74C0C02CEC5C5D38966EACC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m71E0D4E1D33EB11C00D58C59F1CB4EF672574BF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_Reset_mB982CFD7B68F4859E7412FE32A24554DDF8B9E34(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_get_Current_m83710E3C8D34400DB97D8B5169AAE0BDFD131444(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41__ctor_m1D5A3ACACB83EE5605FF4AC082EB241A1460BCDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_IDisposable_Dispose_mCB2CE8CBF5674FD98B569F02FE5AC26555DE020C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC0533830866A8CF98F479EF5B8106A8B635384(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_Collections_IEnumerator_Reset_m67F2B799690EB2F34910D863BD4CB2C83F3E1809(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_Collections_IEnumerator_get_Current_m29DD6B0CAAAF87F7F1322A81E39F4F0788B86053(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42__ctor_m3BD2C502FFA7F018FF2A9E5556E12D28D147BDC5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_IDisposable_Dispose_mA97EB664D7C245AA9DA4CBD3507F5603E8464FDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F3D0239193B5CBCC7AADCA905298F2B85B3870D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_Reset_m9F64FD56FD033B71B1AEDF751D313A81230B2FC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_get_Current_mEE72D133A4AD3A0E74B5164CB3F09F72E28D87AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass43_0_t3659AFDC00FF199C2073A781355297C47D6E68AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43__ctor_mCDD3B2C9874ADE71189C1A2ACBDAB227D2455955(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_IDisposable_Dispose_m7BBE4857CF406984A98BD1C95C6F40D8DEF5A516(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D63008CE9C30952B4DEB9402BB40C2C291BF02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_Reset_m7FBE3DC6DA7C4020EE8EE7D13954C9FCAA874F81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_get_Current_m5EDFFCF83AF6C196734AAAA90F2CF68397620C8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass44_0_tB550A42E24053DD54C0A452BAB13FD6E34295100_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44__ctor_mAC3B30605681C825B91BBA89893EF6CB1A3D96E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_IDisposable_Dispose_mA0ECE09AB7D10863359BF0B7026FC47BEF15FF48(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34077D9F5C1F11EE301947D160A382D1349E45EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_Reset_m3FDD44A4DF8529AECBA79B66DEF8E53C8AC5356E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_get_Current_m7F780518B35ADEDB939AE4E7A59237B3BF7F42BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45__ctor_mF7B098B86805EA416C3462209C68E05D57BC41D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_IDisposable_Dispose_mB00EE4A9B81482B388F6A20A290B0D0752167601(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA81081F88F877593B79FB77044DF848A78B7CBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_Reset_mAA2607639C0DF413DF98F03E05E7B59C31B83CCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_get_Current_m9E60F5E5C1D05D243287FD86E4ADE78EE4ABAE36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_cursorListMan(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_pageMan(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_totGarms(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_adjustmentReq(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_updateReq(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_stillDwn(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_scrollPos(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_lastSceneMan(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_sceneRequestedMan(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_filtOption(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_recChoice(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_noMoreGarmentsLocalMan(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_size(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_gender(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_type(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_typeIndx(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_qty(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_fullPath(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_syncGarm(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_syncAvatar(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_isFav(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_title(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_avtrSize(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_avtrSizeReq(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_mulQueriesProcessing(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SceneChanger_t6A30EA4853DA52DBD1479ADCBE7B3B6952D1E068_CustomAttributesCacheGenerator_SceneChanger_Logout_mCC126448679F9759D5D0EC6DACB7AC41863FCEE0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_0_0_0_var), NULL);
	}
}
static void U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2__ctor_m8FF31D23636AB1BA8FE7C518DD99F955F57483BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_IDisposable_Dispose_m19E8D33544E5D32EBA7F3CD5842047218D244A58(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCABAF6E94019D40B2B6971BD8E7801A981335B65(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_Collections_IEnumerator_Reset_m50F33DB45F147049E257D2706FA09BC1E33CD0DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_Collections_IEnumerator_get_Current_m5195917ABAB3B2D446AF9DAA5BB46F03FD156627(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextureManager_t472D9185292B5891A8DD124ABF21F42C15CC65D2_CustomAttributesCacheGenerator_uwrDel(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TextureManager_t472D9185292B5891A8DD124ABF21F42C15CC65D2_CustomAttributesCacheGenerator_TextureManager_texHandle_m439B211B1DEF03FA31002E8E564A3DF8BF6B0049(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_0_0_0_var), NULL);
	}
}
static void U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4__ctor_m78A498D373C0F1D47DF31B65F65830A922CCAA04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_IDisposable_Dispose_mBC6A134D53DCB893C0332E66F377E631EC743E83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61A1F42F4519A4336BDE57BD8AF01BDB6A3EAD09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_Collections_IEnumerator_Reset_m5A9853940637A064502F5F4897D24B489826CBD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_Collections_IEnumerator_get_Current_m8473DEAF348680FBE1E0506F50C6FBCD80D6AB7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__imgBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x73"), NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__panelLayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__navigationType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x70\x65\x72\x74\x69\x65\x73"), NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__darkenBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__tapBackgroundToClose(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__openOnStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__animationDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__imgBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x73"), NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__panelLayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__navigationType(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x70\x65\x72\x74\x69\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__darkenBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__tapBackgroundToClose(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__openOnStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__animationDuration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x61\x79\x6F\x75\x74\x2F\x41\x75\x74\x6F\x20\x45\x78\x70\x61\x6E\x64\x20\x47\x72\x69\x64\x20\x4C\x61\x79\x6F\x75\x74\x20\x47\x72\x6F\x75\x70"), 152LL, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_StartCorner(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_StartAxis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_CellSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_Constraint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CapsuleCollider_t89745329298279F4827FE29C54CC2F8A28654635_0_0_0_var), NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_MovingTurnSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_StationaryTurnSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_JumpPower(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_GravityMultiplier(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 4.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_RunCycleLegOffset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_MoveSpeedMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_AnimSpeedMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_GroundCheckDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SampleCharacterController_t2FBDB3B4D2A82F3A1E5727C01B527C1A148D2A54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_0_0_0_var), NULL);
	}
}
static void ColorFromPhase_t863921ECDC73F68E07849C70AA5B804057CFBE19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void ColorFromVelocity_t00F017DC071BB795BAD0B1CA4D8F8C98AE868308_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void ColorRandomizer_t8494ABD4FEEA922D2A923B17C563FABB64F875E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void LookAroundCamera_t451EF6BFFB4F4F39444C65E9149782DDB8B08C72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var), NULL);
	}
}
static void CreateObiComponents_tD88C34BDA67096EA93DC45B28FDB7222B6A7D2D8_CustomAttributesCacheGenerator_blueprints(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74\x73\x20\x69\x6E\x20\x74\x68\x65\x20\x6F\x72\x64\x65\x72\x20\x74\x68\x65\x79\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x61\x64\x64\x65\x64"), NULL);
	}
}
static void CreateObiComponents_tD88C34BDA67096EA93DC45B28FDB7222B6A7D2D8_CustomAttributesCacheGenerator_CreateObiComponents_CreateComponents_m819F1F20C2601A5E5C8AAFCAEA2FAD334F569908(CustomAttributesCache* cache)
{
	{
		PublicAPIAttribute_tED08342205377B135B81CB38721B80C503A76F1E * tmp = (PublicAPIAttribute_tED08342205377B135B81CB38721B80C503A76F1E *)cache->attributes[0];
		PublicAPIAttribute__ctor_m4308496F0BE2C84B73141BB2D5BC5C6B019AA0B0(tmp, NULL);
	}
}
static void LoadAssetBundle_t65D8D57880F9C95EB5D1D67062F16F1AE7D2C66B_CustomAttributesCacheGenerator_arSessionOrigin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_OnObiAdded(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_add_OnObiAdded_m5F1F706772F4FEB073C6EC4657E856AC8F46924F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_remove_OnObiAdded_m47896DA4AC0060E8BA14323E36446E7DCC56A7C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_AttachObi_mA2D9B03ED5F9CAEDD8C47C859101CAF89F2CF059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_0_0_0_var), NULL);
	}
}
static void PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_CreateObi_m7B19FEF5D807AC60C14A480B266EC3FA4571E603(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_0_0_0_var), NULL);
	}
}
static void U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12__ctor_m323E149194970DEB7C74D42B50BCD92DDFA10E58(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_IDisposable_Dispose_m606C4A0C39F85E47234D008EA5C15466EE21E6B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9F06E12394352EAA6288915A8E1A9FD2A9020E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_m7C8BFAF6A415731CB8B4DF5702063CF293F51321(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mB54B79F9A99CD527CA0F69AA8F4D51E10E398815(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16__ctor_m62416639B5CB564A96F41F34AFF4479F4EC0F737(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m20CA1D1DC9156B9D0BA1C9B45D02B4276BD0227B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1153171D17D5A85E9D1089961F56E8C35031B60E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDECD8FBF73AC546C48D6663310A224A229D2A20E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m20F4A3067E9EE93EBE98BBB16E3A8C3BED678928(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_OnObiAdded(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_add_OnObiAdded_m21C4DFFB388835AAF49AD553672C78EF8083F487(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_remove_OnObiAdded_mD59D6B40F02F799BD8BACCE38AD4BC2D703FF9D7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_AttachObi_mDEA5E44A4A4BD87B69355AC66EC89C5E98AF6099(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_0_0_0_var), NULL);
	}
}
static void SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_CreateObi_m7C3477A5C3F0BB4E8FB587E629AC7F3D5E072425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_0_0_0_var), NULL);
	}
}
static void U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12__ctor_mB9723999E31B7F5F4B26C52846EE0EF7F89C03D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mEB86C2EEC5E55A40B077EAE0B7CD5291FBBE4FDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m124A441CC082D84EB1C7E8C4D09DD36E4BAA1DC4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mF88ACCD9C8DB2BE87221C306721BE2C8E795FD47(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mFDF31BB57526DB3E034C630C43113FEBAE74DA2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16__ctor_m484B3307000BF1D629371B445312F411B8975D51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m884A0B4D81853BF2D53AA1F505BE7B082C5B492E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BFCD23E4DD867743952EEC697B33C6095E83FCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mD9BB880CE88988B91F124368A497E88A3A05AE21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m6C0A0740DA6B6C547A0784E83EE609B703F35B15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UIController_t220385D83E5A815E75664CFC2EEB30BE1ACB59B1_CustomAttributesCacheGenerator_garment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x47\x61\x72\x6D\x65\x6E\x74\x20\x61\x72\x6D\x61\x74\x75\x72\x65\x20\x69\x6E\x73\x69\x64\x65"), NULL);
	}
}
static void UIController_t220385D83E5A815E75664CFC2EEB30BE1ACB59B1_CustomAttributesCacheGenerator_UIController_StartBodyTracking_mB5FBB5B789AC591BEE67379C313B08C96B65AA0F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_0_0_0_var), NULL);
	}
}
static void U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11__ctor_m7D47E8B6DA6315A081D9E41F1EACCA21631D6B63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_IDisposable_Dispose_m0E93CE83F9A57AE50F030818B6979783C7514008(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94FE82124B37C2D7ECD1A80707193342445FF4E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_Reset_mF038F71D5EF0A73A63C671C81B716DE08772EC01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_get_Current_mCAF09EA58826F4E0C4B7679D5070E4FE354EDF97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_skinRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x66\x6F\x72\x20\x73\x6B\x69\x6E\x20\x72\x61\x64\x69\x75\x73"), NULL);
	}
}
static void WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_backstopSphereRadius(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x66\x6F\x72\x20\x62\x61\x63\x6B\x73\x74\x6F\x70\x20\x73\x70\x68\x65\x72\x65\x20\x72\x61\x64\x69\x75\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_backstopSphereDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x62\x61\x63\x6B\x73\x74\x6F\x70\x20\x73\x70\x68\x65\x72\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_WpiObiNoBlueprint_CreateObi_m81599B62AE6C534CFBE93954E065DB7915AB8D32(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_0_0_0_var), NULL);
	}
}
static void U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4__ctor_m6B7FE0270BDD042C4CF7FEE7E23DA518AFCD26BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_IDisposable_Dispose_mF66738AD01D56578BBD2552D07182E3D03750B72(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5AC2783D38C1D425E7FC54C3BA6D510C3F88713(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_Collections_IEnumerator_Reset_mD360873099FC44F0F1ED6DEFC12B6BB0A78DD40A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_Collections_IEnumerator_get_Current_mBD9F9895E725E1403A8F438CA60098BC62E5B8D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_OnObiAdded(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_add_OnObiAdded_mF7DD3C8BDBB033D6743D82515CFB9EC100D5315F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_remove_OnObiAdded_mA052507D0A9DCAAE4F0CFAC9737E7BD2E5B3EA2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_AttachObi_mB362DE10565BBEF3BC97834455F2389F77372CBC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_0_0_0_var), NULL);
	}
}
static void setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_CreateObi_m3ED0DF41000191CB095295ED8735804C08B040FF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_0_0_0_var), NULL);
	}
}
static void U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12__ctor_m54F90B5E1243762481D29EC8164CB32CA8BFC633(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mFF2C2A49D0534860E17ADBCABCD20753A4FB0C90(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0DF70F72243D4FF5714E4EFDC1FAA0ABE2CE5E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mDCFE47940ECB0E6F4067A64FF563DCC8FF30CD52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_m6E8969EF8A777A33E45811966F96C714B26D25D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16__ctor_m122099FC8C75500A0683EC9FC6C4C74DB6309A07(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_IDisposable_Dispose_mC60A3B6541E7D65C9CEDC54A79114FAC9CED9FAC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA77337728FC469EBCAE52E998D106954DF94E715(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDFE053E3602BE312CEF0388C87F276ED43D09CC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m793AE57000E8558A92A7B3C3869D377791749309(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[809] = 
{
	U3CU3Ec_tC756FA782B3CA630FBD1E8213E659FABD8019E36_CustomAttributesCacheGenerator,
	U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator,
	U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator,
	U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator,
	U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass45_0_t70614F4D245FA299C9ADE40EC6675823C96EFC39_CustomAttributesCacheGenerator,
	U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass46_0_t28BC718D1539C9E628940DD7AB894D21A59EDD1F_CustomAttributesCacheGenerator,
	U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass48_0_t94E74C3BEC45D13787CBC620F5B23B122E9498A4_CustomAttributesCacheGenerator,
	U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator,
	U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator,
	U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator,
	U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator,
	U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator,
	StretchToColors_t127782874CD1A324DA1D0DC404D6052699B03AEC_CustomAttributesCacheGenerator,
	AddRandomVelocity_tF413E105600EFCFDF6AE3AC9A09E3DCAFD98AF94_CustomAttributesCacheGenerator,
	ColliderHighlighter_tC4C5C8B165DA586BA44D63D3463837713C035FC0_CustomAttributesCacheGenerator,
	CollisionEventHandler_t749A22AA9632B3D4B759B2936D679FEDEE53FA19_CustomAttributesCacheGenerator,
	DebugParticleFrames_t592D11A207E1083BFAEB9A0D5C051FA7D5A5829C_CustomAttributesCacheGenerator,
	FPSDisplay_tB99493D73C9C04D0BE47319995B5EBF3A63E49E4_CustomAttributesCacheGenerator,
	ObiParticleCounter_t32364B0EC08FC57C6E7AAC7740676580A5102A39_CustomAttributesCacheGenerator,
	WorldSpaceGravity_tD062DD355E3F4BFA3B14AA3F76DB929A496E50D4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t1BDC549E9F93220182B1A1924BE6B424E44EE3BF_CustomAttributesCacheGenerator,
	U3CU3Ec_tF67814ECE2731FD4B792DA3AD54AFFEC51CBF1A7_CustomAttributesCacheGenerator,
	U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator,
	U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator,
	U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator,
	U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator,
	U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator,
	U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator,
	U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator,
	U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator,
	U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass114_0_t84C44324ECBB94FC44F18320E630291C65CC1387_CustomAttributesCacheGenerator,
	U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator,
	U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator,
	U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator,
	U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator,
	U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator,
	U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator,
	U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass128_0_tE46D33ABE827F4FC858BABF35A02713BF31451D7_CustomAttributesCacheGenerator,
	U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator,
	U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass135_0_tEC403547FD1C9AA3D0A5FEDA76FDA6DAFB5D9F4D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass136_0_tBD96319AA52D96DED074836D043A77488E293AC1_CustomAttributesCacheGenerator,
	U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator,
	U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass19_0_t1E5B86B697DFDA6ADAC794D52CB25736C6378294_CustomAttributesCacheGenerator,
	U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator,
	U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator,
	U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator,
	U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator,
	U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator,
	U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator,
	U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass109_0_t67B405568BFB88833DB85FBDB04F0C19168294BF_CustomAttributesCacheGenerator,
	U3CU3Ec_tBBD2A55ED7740F67094538B3EDC2B960AA0121B8_CustomAttributesCacheGenerator,
	U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator,
	U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator,
	U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator,
	U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator,
	U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator,
	U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator,
	U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass125_0_tE4D82853EB011B0201238AFDEA4ECB5249814BA1_CustomAttributesCacheGenerator,
	U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator,
	U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass133_0_t0157BA17404E5E1F7FC1E44B1309E82A3F7FB68D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass134_0_tFEE258E896F3AAA907E6CB179096FD7C98077B00_CustomAttributesCacheGenerator,
	U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator,
	U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator,
	U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator,
	U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator,
	U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator,
	U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t8987DDC1C9D5D49E7AB590FF5FAC74478332B94B_CustomAttributesCacheGenerator,
	U3CU3Ec_t7916A5849C80430E8C7F116229BB3F4D75C11C78_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t667C4E47B3AA7484773B7EE72F428C3C39C27D57_CustomAttributesCacheGenerator,
	U3CU3Ec_tBA21EFFF3E8D35FE4F548E7F96F2ABC45BC4AB71_CustomAttributesCacheGenerator,
	U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_tA96F2015F23A2772E3826FB634E91C20FDBE7DB4_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_1_tDFDC22E4DBC40AA616B5C200E065ABE77752AB1D_CustomAttributesCacheGenerator,
	U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_0_t4A6D23927CE1393EEE85B45946CC891C7F2878F6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_1_t4AAF52C02358EB2FEFEA710520B36772D98FDA74_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_2_tECEB163B0B3FCE7ECCEE31307B66B2B0E4A8A35A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_3_t4F2EBE23A42C32A3B2A7361F170422FD55DAE5CA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass33_4_tC88FAA09A99B8295561BA2AB17BE4F1E3457356C_CustomAttributesCacheGenerator,
	U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t6942DABFCC5FC53736DD06B7F4DDD293F24ED189_CustomAttributesCacheGenerator,
	U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass20_0_tD9ECD7724A5FEEE7398CC64729BB056CCEE59860_CustomAttributesCacheGenerator,
	U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_t7BA4498DD3043CCD1EDFD9830D0047F82E2435EF_CustomAttributesCacheGenerator,
	U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t4C59DE6ABFF6168BC92A9597A7E76B0F13253648_CustomAttributesCacheGenerator,
	U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t3BE164DEBA94F18AB86B37549000AADD41D1FA70_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_1_t5F903D9D7E2B2CB4A1DCD53F73952E9E1C0DF4E9_CustomAttributesCacheGenerator,
	U3CU3Ec_tFC10E533E4B7DACED888E6F7E6BA060A31831CBE_CustomAttributesCacheGenerator,
	U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_t22627AD4A37DDB4571F5158E8CAED841EBCE7F0B_CustomAttributesCacheGenerator,
	U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator,
	U3CU3Ec_t1CDFED34CA29A2ABC1D369F6FF77E48C93D5DE14_CustomAttributesCacheGenerator,
	U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator,
	U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator,
	U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_tD5088565E378D09EBEC4586123404E30C93F3BCA_CustomAttributesCacheGenerator,
	U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator,
	U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator,
	U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass43_0_t3659AFDC00FF199C2073A781355297C47D6E68AE_CustomAttributesCacheGenerator,
	U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass44_0_tB550A42E24053DD54C0A452BAB13FD6E34295100_CustomAttributesCacheGenerator,
	U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator,
	U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator,
	U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator,
	U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator,
	SampleCharacterController_t2FBDB3B4D2A82F3A1E5727C01B527C1A148D2A54_CustomAttributesCacheGenerator,
	ColorFromPhase_t863921ECDC73F68E07849C70AA5B804057CFBE19_CustomAttributesCacheGenerator,
	ColorFromVelocity_t00F017DC071BB795BAD0B1CA4D8F8C98AE868308_CustomAttributesCacheGenerator,
	ColorRandomizer_t8494ABD4FEEA922D2A923B17C563FABB64F875E6_CustomAttributesCacheGenerator,
	LookAroundCamera_t451EF6BFFB4F4F39444C65E9149782DDB8B08C72_CustomAttributesCacheGenerator,
	U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator,
	U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator,
	U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator,
	U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator,
	U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator,
	U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator,
	U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator,
	U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator,
	ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_smoothness,
	ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_linearSpeed,
	ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_rotationalSpeed,
	ExtrapolationCamera_t0343EDA472C2ED85A75075AC48EFBC2172159562_CustomAttributesCacheGenerator_distanceFromTarget,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_buybut,
	AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_inputMailObj,
	AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_inputPassObj,
	AuthUIManager_t0D070C29FF1EA724087AB61768A2120E5A84A373_CustomAttributesCacheGenerator_loginUI,
	BoneController_tFB1633B689BC777BF4A5BEE01B90A482A0B59FAE_CustomAttributesCacheGenerator_m_SkeletonRoot,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_inputMailObj,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_inputPassObj,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_inputPassConObj,
	DropdownFilOverride_t0623B05A6DF67686746EC7FB32AD7B507664E010_CustomAttributesCacheGenerator__placeHolder,
	DropdownOverride_t9998C5605A1AC9864677FFDCFC5E280388356966_CustomAttributesCacheGenerator__placeHolder,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_auth,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_loginEmail,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_loginPassword,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_loginOutputText,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerEmail,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerPassword,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerConfirmPassword,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_registerOutputText,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_sizeDropdown,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_genderDropdown,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_termsToggle,
	HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_avatarOcc,
	HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_m_HumanBodyManager,
	HumanBodyTrackerAvatar_tC4C1205BD20B8913B2130223D1AB28BBB2CB9122_CustomAttributesCacheGenerator_avatarOcc,
	HumanBodyTrackerAvatar_tC4C1205BD20B8913B2130223D1AB28BBB2CB9122_CustomAttributesCacheGenerator_m_HumanBodyManager,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_avatarOcc,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_m_HumanBodyManager,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_cursorListMan,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_pageMan,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_totGarms,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_adjustmentReq,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_updateReq,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_stillDwn,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_scrollPos,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_lastSceneMan,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_sceneRequestedMan,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_filtOption,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_recChoice,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_noMoreGarmentsLocalMan,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_size,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_gender,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_type,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_typeIndx,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_qty,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_fullPath,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_syncGarm,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_syncAvatar,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_isFav,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_title,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_avtrSize,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_avtrSizeReq,
	MainManager_t7724856FD454D1506DC9E9ECA4798072CB7E210F_CustomAttributesCacheGenerator_mulQueriesProcessing,
	TextureManager_t472D9185292B5891A8DD124ABF21F42C15CC65D2_CustomAttributesCacheGenerator_uwrDel,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__imgBackground,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__panelLayer,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__canvas,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__navigationType,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__darkenBackground,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__tapBackgroundToClose,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__openOnStart,
	FilterPanel_tA2A59164C22D145CD05F08FDCA55DB5735D3497E_CustomAttributesCacheGenerator__animationDuration,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__imgBackground,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__panelLayer,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__canvas,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__navigationType,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__darkenBackground,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__tapBackgroundToClose,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__openOnStart,
	NavDrawerPanel_t35DDC468E6493DA1BD8C422695310E2AF268A750_CustomAttributesCacheGenerator__animationDuration,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_StartCorner,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_StartAxis,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_CellSize,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_Spacing,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_Constraint,
	AutoExpandGridLayoutGroup_t7E99DD7A5B64BC6494FDA8C5A5420BC9E4AABD69_CustomAttributesCacheGenerator_m_ConstraintCount,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_MovingTurnSpeed,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_StationaryTurnSpeed,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_JumpPower,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_GravityMultiplier,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_RunCycleLegOffset,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_MoveSpeedMultiplier,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_AnimSpeedMultiplier,
	ObiCharacter_t1928D9D6B59E0AA96905FE41AE29F72E1370BC39_CustomAttributesCacheGenerator_m_GroundCheckDistance,
	CreateObiComponents_tD88C34BDA67096EA93DC45B28FDB7222B6A7D2D8_CustomAttributesCacheGenerator_blueprints,
	LoadAssetBundle_t65D8D57880F9C95EB5D1D67062F16F1AE7D2C66B_CustomAttributesCacheGenerator_arSessionOrigin,
	PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_OnObiAdded,
	SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_OnObiAdded,
	UIController_t220385D83E5A815E75664CFC2EEB30BE1ACB59B1_CustomAttributesCacheGenerator_garment,
	WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_skinRadius,
	WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_backstopSphereRadius,
	WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_backstopSphereDistance,
	setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_OnObiAdded,
	CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_0_mA8EA2B84DA44FCE5D20EDCD476540D9FE79D7E66,
	CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_1_mDAD7A8FE7ACACAB80270E91FFF0AFE80BA923822,
	CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_2_m1EA55EE03648F140001BB6C0C74318DB9191BCA6,
	CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_3_m3C58176A7C17724C2C8DC3C45206A46D663235F8,
	CallbacksExample_t27673A27816C7B1324D222C431260A1DE4C967A3_CustomAttributesCacheGenerator_CallbacksExample_U3CStartU3Eb__5_4_m73A75D4D2C5FB9CE958AA0531FB9301B18BC6254,
	Davinci_t292F437648307B802DF74143EB44E07158F0D541_CustomAttributesCacheGenerator_Davinci_Downloader_m15650852A45FBDAF97CC882F463B75CE7647E772,
	Davinci_t292F437648307B802DF74143EB44E07158F0D541_CustomAttributesCacheGenerator_Davinci_ImageLoader_m556EEE7C8C84B797F2FEE75F6140CC9EB92900DD,
	Davinci_t292F437648307B802DF74143EB44E07158F0D541_CustomAttributesCacheGenerator_Davinci_U3CstartU3Eb__37_0_m8B355C931283442F82EA487DF964E84991D80035,
	U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38__ctor_mC0933E4939833D0C437D034A5542BAB979F0D293,
	U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_IDisposable_Dispose_m7BDA6A678D4C61D5C65745A335E01A40C9377321,
	U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF5289C7B9DB78D33EA79C46839A9D83A623BBEA,
	U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_Collections_IEnumerator_Reset_mF1D52F18D10CB6C87FB5481732F1A1EFEFC988EF,
	U3CDownloaderU3Ed__38_t4F73CE84F8C2720153C164DBD21DD7AF31154CD8_CustomAttributesCacheGenerator_U3CDownloaderU3Ed__38_System_Collections_IEnumerator_get_Current_mC9A6BA27CA0A438B9DFEAA6658C83E8F659DE96E,
	U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41__ctor_mB990C3A44F2C25ED035517217354815A4702FF57,
	U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_IDisposable_Dispose_m4E0B05BF9B4A31543CE38260AAB6B24316A762C8,
	U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB39260EC0C134F7CD19D5DCF10ED803C9ACBCDCC,
	U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_Reset_mA80C9BF77968EC1F27A49592D8F2FCDC749101CB,
	U3CImageLoaderU3Ed__41_t4F4F739A0AB69ED2B5805223C3E92C93CC9C75F4_CustomAttributesCacheGenerator_U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_get_Current_mAB3334FDDC841DB00E54398A582A59001D2FAE97,
	CreateObiBP_t01CE8E59EECC8AD466168C6E08FC76D71EF27AB9_CustomAttributesCacheGenerator_CreateObiBP_AttachOBI_mE908C1B923E8EBC3C69393E6469B518FD34926EF,
	CreateObiBP_t01CE8E59EECC8AD466168C6E08FC76D71EF27AB9_CustomAttributesCacheGenerator_CreateObiBP_CreateOBI_m0DFBBACA70566C2FC62BF6D5C649921563471BAE,
	U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10__ctor_m430968C5EFD2BA0C9B96735FA7AC90263C382180,
	U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_IDisposable_Dispose_mB2D097C019144FCC309F6414F7076DB3E6772006,
	U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA42B13CC11372A044675C07BCDC32019A08A352E,
	U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_Reset_mC3C7E467B258179D51349B0F83385F1AEAECBFDE,
	U3CAttachOBIU3Ed__10_t2CCDBADC7FE90BCA831BEB11AA1F523BA0ECEBB0_CustomAttributesCacheGenerator_U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_get_Current_m1F939691B3EADD74CDA34716DBA3FC4374AC9B15,
	U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14__ctor_m8DD675C517F191749940BE2ED1FE999EDF7E465E,
	U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_IDisposable_Dispose_mA7F079A16092731E506A9DC1FCF58913EA7C9EEC,
	U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8F4E6D01EEB24AB539251D4E4711125E71EC0A4,
	U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_Reset_m94B2BAABBBE38D5B38A23B14F1849F43283A7C52,
	U3CCreateOBIU3Ed__14_t1210C0D6E0BEB4958FDD55C59FC60F0C34B1DA60_CustomAttributesCacheGenerator_U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_get_Current_mE32BA430181E59F701837DD6862D39C8B836F94D,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_RespawnSize_mE0E3232D9F0388D51F1DB659898D20142FCE3996,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_RespawnColor_mD3B295CF8C5D23AC4BDC05BCCEC5D78D1528CE3F,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_AvailableColors_m0D8D4CD97205390CCA950E81B8838C974B4813FD,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_GetAssetBundleExtra_m59ECC3DE93963BD0696FB9CA50F2CA57EF40B5EB,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_OnPositionContent_m52D3CE7C460401F9F2CF9E6737906F39E728D157,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_0_mDC6AF6032E050DE844017BEE165EAAFBA04F7BD1,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_1_mB8992DB84930877FD37617539617E9439AED0E99,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_2_m0C49CFD50E46835A000B10F07D526ACDD648838A,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_3_m7649A734B850981A28B2DEF385131CCEA6981494,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_4_mB908BB1BE2E3A1BDF9E0AD81FED28253716603CF,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_5_m50A712DD6056870287C4A7DB9E09435FDD1993E3,
	SpawnObject_t890F3DF72658E8EA47C216D600EAB46EEEC6097D_CustomAttributesCacheGenerator_SpawnObject_U3CAwakeU3Eb__44_6_m7AD4F82BDD6442A8837A750FEF0FEB560EFADB92,
	U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45__ctor_m88BFF80AEC54A20B5231575F9D453F32FCD3B8A9,
	U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_IDisposable_Dispose_m88C9FEB4B8FB6C1BB4F83458A6AE573C44B74FDB,
	U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39AC1153721E7A5AE59D1A77B9900B3A6C5E5EEE,
	U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_Reset_m974E70A42014423F08D1565E86B4D39C7E268EFF,
	U3CRespawnSizeU3Ed__45_t2659284DD2EC1BFDD3EA17F6217014429CC9F3A4_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_get_Current_mA773B08876925350D43ECD146FEE0D954D30146C,
	U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46__ctor_mD8855F765A5C43BEEF30F012AEB49BD1F2A748DE,
	U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_IDisposable_Dispose_m249A9D780D8D886614FDED6E01EB42EBEB4A6487,
	U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD97D6E8AAABA5C750DC8CB00D5479AC89A5BFA7,
	U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_Reset_m08FE605DE550D0A3942DF553EFA3454649868582,
	U3CRespawnColorU3Ed__46_t81AF6AF6F343C4E576D44BF40AE9C2DF189079A5_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_get_Current_m5376A4C8A91D27D8E3BE69DBF1A0301865167299,
	U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48__ctor_m9ACE7EA35939C6AF56A3F09CAF876C1ADB3E0CE6,
	U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_IDisposable_Dispose_m55EA1B9E30947A469F5A1B916B43EA3FD3E0AC00,
	U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE25815C4D63837B720D40C07AE702CCB69C1D38,
	U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_Reset_m22EE8729D104DD16C47EA3D39D7EFD808EAFD7D4,
	U3CAvailableColorsU3Ed__48_t948D45D8D46D1A8ABD89EF998043BC44AFD5F306_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_get_Current_m6B887B061C6F11CF5DCDBB3ED3DDFF453CE6EA09,
	U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51__ctor_mE87227D1EC63AF5A16DAFC6449A71A27698EA652,
	U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_IDisposable_Dispose_m67B570BAA9827F4884D1A27EF57D8BCEBC9D7650,
	U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m811274D9475214B976094D0D0AA90CBADAE30578,
	U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_Reset_m1FC9B030B7BF99AA20630D8D4E082DD182443C29,
	U3CGetAssetBundleExtraU3Ed__51_t650EBB2829453B5ED326E30C1D02F5672E4466C8_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_get_Current_m2AA89470EDD5DAE93E38D463BE1C4D2785AA4106,
	U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52__ctor_mFEC8E57EA5F72169E05AAAFB7FFFB7D33B910830,
	U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_IDisposable_Dispose_m6AC2F351F789E1DA2154C3D796F41C90A7CED279,
	U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B864B6D262CCF48697FACB821EF52279A3002AA,
	U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_Reset_m1C71BEAB3372161324E40787AEFDA57E524D6F0F,
	U3COnPositionContentU3Ed__52_t95121F6765572A23C3236608ECF41D64572859D2_CustomAttributesCacheGenerator_U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_get_Current_m67A830885BDDC192E16E597A6ED50A3E3C92A416,
	AutoCannon_t3DC0941B475B3F8CB72889D61B0C72F147C4E74B_CustomAttributesCacheGenerator_AutoCannon_Shoot_mF63F6DA0A7FB8DA424F38A5853EAA1A6B2255444,
	U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11__ctor_m884BF51AF4DF8056D4965C028CF7CFF29DC42E43,
	U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_IDisposable_Dispose_mE4F09F48E287A53FF7E525CD9413A83AC867F28A,
	U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C060564EA19770828A39D2CF575AE4E6387CF6C,
	U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_Collections_IEnumerator_Reset_m95B67AE27CE9C309D876182ADFCB7EB45FAB2253,
	U3CShootU3Ed__11_tFC0F7FE9EFF391333CF743B5F7C8CAC06A3296E6_CustomAttributesCacheGenerator_U3CShootU3Ed__11_System_Collections_IEnumerator_get_Current_mD7F7BAED04E09C7275269837A41C3C5ACEE25825,
	SackGenerator_t0188F31C70F8073B1BFE4A643903E59830ABCBB1_CustomAttributesCacheGenerator_SackGenerator_GenerateSack_m5749634500E10A9C49CC02D5B461D5CE4EC62F74,
	U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5__ctor_m60AC74DB36D70827F636F2922CB635E9B7F977F5,
	U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_IDisposable_Dispose_mC3E89951FB26F5B4CE2B7E27251604B8F4376658,
	U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m748A15AB6A05CC78B95300CA85662577A2EA5F19,
	U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_Reset_mD87A155F07288E5B5248CE7AA284740859004F3F,
	U3CGenerateSackU3Ed__5_t6951ADEB11D7883AACF85BFCE11FEC0767C6FF4B_CustomAttributesCacheGenerator_U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_get_Current_m61246B7B8E9A24805CF9F6D7ABA51935CC7BA088,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_SetPrefs_m930EF2A4A0384D344FBA9D9C5EACCB14125E2FB0,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButNextRoutine_m491D1048FB724E16FD51D5C8FA3B6E4864E645F5,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_DelItemListWait_m304E9147CFA670BF3F714A6326A4C0D40B9AEFC4,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButHomeRoutine_m12A488E1C2E634DBFD20C4DF299FF5B998369AB2,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButSignOutRoutine_mAD978974F12CC9E41D44B7FB3E307E70173CD761,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ButPrevRoutine_mB7E7EF57C738F6AECF963DB4FADFAA20121C4228,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilRecRoutine_m9994A64E295BBD9D52D828B6184F2CAC33032C0E,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilTypeRoutine_mDAAE833DE05D0C9210A61E10C69BD790D1951783,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilSrcRoutine_m7A9182A31CA54285EF9BFF3C760E3A76CDED3C90,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FilUpdRoutine_m4ED3B5CE0BA022BEC13C4AA0DBA0393E0C0AB507,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_StopDownloading_mFE88CA2D4154EE4AC05F5C98970D9E1EE51B4B99,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ResendRepeat_mF9585698444E6288F3D0BFEDE9E6212E7240CD06,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_tmpScroll_m86AE102F3384CE2C55BA0B5F9D750F846FD95D9F,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_BeginGarmentCall_m087BB0472C9CCF6A072EFEFC1C2593AB99A16164,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ImageTexHandler_mAFE4B133389E581FD3A8C6CF2141FB043A1932BA,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ImageAddingProcess_m7A5ADBEF5D1CC2AE157522E14FD7D6F104BFE1A6,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_FbaseQueries_mC730E8CD3FF08272E05325E76D173716C0A20A70,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ToProduct_mC0687748A4043499BEEC4066AB0A9578BEF8AE87,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_ImportImage_m193885C921D7C5A2ACD4867D1802C6551B6D375D,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_tmp_m574D1139763D97D78638D7B17E389CC5E75A734D,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_0_m2096C692CD9442FC46902A0F16DC4328A918C7D6,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_1_mC08704029D6F7E6D445DCC23E94F12A359980080,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_3_mB54E039BC3C1F73EEA1FAB29614E7F3ECD241CC9,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_4_m3E9597F4595EC78C0B9618C3F09A04B8BA85C040,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_5_m38832F72F9C87F8CC9D1500BA5733AFE8A343B35,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_6_m597E16370DAD0288AD7EED4CC61FB32F1C99F481,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_7_m65B2483950F056B5C1287834E018A66BD1DD4B43,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_8_mA016DA8AAEB55699B162DD1DF66FD216946D823F,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStartU3Eb__102_9_mE809BBAC55ECD75FFA6A593D4143EFB84ED9C9E5,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButNextRoutineU3Eb__104_0_mBF7052D323E37B73886148348762818ACE5C002E,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButHomeRoutineU3Eb__106_0_m5C45AB4CC871CC952A74FDD92A52CC19E58F8072,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButSignOutRoutineU3Eb__108_0_mA7DC2D1B6D727E144DEAB10191F7F5E6F68A96A6,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CButPrevRoutineU3Eb__109_0_m5803B19502851521C955B2930AF40EB36847F124,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilRecRoutineU3Eb__111_0_m0F1EBEF4BE8FA985651D2F375CBD989D0EEA42F3,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilTypeRoutineU3Eb__112_0_m99DB59CD2666C0D6462C28AFD55D09231AAC0741,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilSrcRoutineU3Eb__113_0_m30BA0187BE2962F46D35967E21A811E8E14F5922,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CFilUpdRoutineU3Eb__114_0_m5D40CD366F70EA64C5CF7F0FBF39CA0F11B29FCC,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CStopDownloadingU3Eb__116_0_mA97AEED6FC22F8068EB6F9021BCCD84D39A30E57,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CtmpScrollU3Eb__121_0_m025E168AD6D3C68E5476FB87A5F4B7E902C67EC7,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CImageAddingProcessU3Eb__126_0_m440ADB857BC9E9AEAA14DBCF5C2B78C6FA04D3B5,
	AddItemComp_t3344BA6CC0D6EB73ED0360CEFD58C01164A24158_CustomAttributesCacheGenerator_AddItemComp_U3CToProductU3Eb__134_0_mE07CC29E524AD36543895E68382EF7AA4D07132A,
	U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103__ctor_m8FB32A3EA183CB8090C3DBB50B0109656692BBAA,
	U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_IDisposable_Dispose_mB4BEB73E7A346D24D2DEF57F701A013AAC8B83EE,
	U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093F4D0EFF81DDC45B7624C209E883E5E07B08E4,
	U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_Reset_mB60350DCF81DA37ECE9FFA39FB691C9836E3E285,
	U3CSetPrefsU3Ed__103_tB842F0D56A554802709CA63E9C8A5D2215B42357_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_get_Current_mCDD4AC07D175D9271B089B5B72A7BAEC106B0F1F,
	U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104__ctor_m0085365472595061903314EC1E4FFF79AC9C485F,
	U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_IDisposable_Dispose_m23377156CD91B1D2CFE09DE547413B94CD1B85E3,
	U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CC7DBC65BBF1EE481C3463FCC966E79EBAB87A2,
	U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_Reset_mDBC944F58A8A27B4EED103ACD698716C6F694C39,
	U3CButNextRoutineU3Ed__104_t9058F540C8D8CB7AD38AEA6AC0EFDD10B2EA9379_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_get_Current_m9EF10872F1E79554771AD85FBB5C87D3E006067B,
	U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105__ctor_mE56DCC6198C2DA9F89186F244E620BE6A4832C24,
	U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_IDisposable_Dispose_mAFF526E03E52DD0CF05DD04626EB5F6DA87EB364,
	U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75D67143542BF51250E417F47999162F3082217D,
	U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_Reset_m10DD860E4ED3C27E7B607E6F22AC712A8DED3025,
	U3CDelItemListWaitU3Ed__105_t9DC53D275CC72F71A731C558F98DA9BB8E4EA200_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_get_Current_mCF63A193F61F5E9CAEE380812754088DF047E6DE,
	U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106__ctor_mF0F347434E4D78C82D94BBCBC18072E688202C2B,
	U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_IDisposable_Dispose_m9B496850533015E4F608FC096B9B500AC23026F2,
	U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDEDE7FE05FB39A15955781EDD78073457E2CA83,
	U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_Reset_m3BB033F0512E738AEA51BFFF949335B96A414367,
	U3CButHomeRoutineU3Ed__106_tF8759F9E843858C8536149D94E798E653179B0EF_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m4FCD177E7D65FE61EA91F37BBA0049937F2B08BA,
	U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108__ctor_mF79A9854726851E2C5773958F6873EBD682799D5,
	U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_IDisposable_Dispose_m8C68B05A8AE1C61C64F9712905CC74FECD3E690D,
	U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m738D1C6B18D3CAA63D518C2B7C0D5DEE4FD8D948,
	U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_Reset_mB3CE8FBA1B3287E592432EDC4554176C8B3E6690,
	U3CButSignOutRoutineU3Ed__108_tE518DC53418DA49E15394C69F0E60353E6D09114_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_mEE50C74269A8013E6987EB9EE0F4249E263BDE85,
	U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109__ctor_m4A20BECF42F8B7F4C026E469599A2F8860953006,
	U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_IDisposable_Dispose_mB8627B57D8EDF2A14E07394DE8C70C5C8C559B13,
	U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFCB870E8A5E90D88232266CDBB7CE407C60B363,
	U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m521533485C83DBEC45F0E4E4609BE8269DEA0434,
	U3CButPrevRoutineU3Ed__109_tBC37EF4513DF5A58A84E2184234DC873E8DE78C9_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_m9799E1C63B54F2CD96262A73C227B1F136A9D3D5,
	U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111__ctor_m9F1A768D3DD7CED2C75103495721D60FDA5281DA,
	U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_IDisposable_Dispose_mE9A6542D040200FB09383043C06B14A0C59EFFCD,
	U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2C803E973C3986774B9663A022FB34250DB0C2F,
	U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_Reset_m8C8316FC76E78AD0D70ABDF37C157B0874E4B59A,
	U3CFilRecRoutineU3Ed__111_t218C2505A98EB5EC5E499023A29DDE47AB63B606_CustomAttributesCacheGenerator_U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_get_Current_mCA6F124A8453688732770F731E9794591A34EE0F,
	U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112__ctor_m3541807D0A5921CF9362E45C07E1B78D1F5628D5,
	U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_IDisposable_Dispose_m7BB15FC367573484AAAC7342C6F8E7C805787EB7,
	U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD81030DA4DF6EABEF2C3440166062FBFDC1EC41E,
	U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_Reset_m01D8D7C4430B1CB6B9D833F137FCB920DA0C41C5,
	U3CFilTypeRoutineU3Ed__112_t9593DBBD1636B6C0E08AC59B336041316E81CBFC_CustomAttributesCacheGenerator_U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_get_Current_m1CED9C1E2ABEF0F14A0E4EFDE9B9E89C79B32D67,
	U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113__ctor_m4425196A106EDB43B1640CE7D4F9F6769C0A1CA7,
	U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_IDisposable_Dispose_m36F97F0CD2C466182E98033B9D28AAB0BB634CE2,
	U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4503567FA847BD7C5351F587C42DC420F4C1417E,
	U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_Reset_m570D349FC4ACAF60F6A794BA378A3E009A92290D,
	U3CFilSrcRoutineU3Ed__113_t35D1F8A675E5756F210A1D14C55ADA0181203996_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_get_Current_m5FD53CA1FDE0E65DB09E071A5AC660772BDD5AA8,
	U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114__ctor_m7E9325EBC3194C561289526D7988BFC13917F6BC,
	U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_IDisposable_Dispose_mF09C79B7A5344B289C5788B9EEA124E6960B4FA6,
	U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D53B015E411EAB5F586060E9184710FC2B3CEBC,
	U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_Reset_m4A15596167AA539664B1C694F372712A981963CF,
	U3CFilUpdRoutineU3Ed__114_tE01540AD348DDDD495C4F4AA25B54581E4D994BF_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_get_Current_mC7DDE1817B9D41C593B61E33C0F6E76DE1E213FC,
	U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116__ctor_mB8BC0F77581ED0D22C2DC3707858178F36FA1E1C,
	U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_IDisposable_Dispose_m9A2B019169627DC744778F6A581A1341FBBBF5BB,
	U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFAF6C8391D7EF16F8A34DA187B9F19859F769A,
	U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_Reset_mC9B5D5C32A29DA3C1F6822956056ADC748174746,
	U3CStopDownloadingU3Ed__116_t008E84052AF669F65E5762EFBC435E232D8B3E47_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_get_Current_mB8DD0110D702CB79F468A1D31EDE5C4DBA965AEA,
	U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118__ctor_mFBD5278226634B801677A62FE848BDDBC9D38DA9,
	U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_IDisposable_Dispose_m5C96DFF10E9036353B869A60C5B1DC6C77103A8C,
	U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D4886779C7D6108537AFC05D1F84B5960FB557B,
	U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_Reset_m005EF473A7FE5497F43D0746E0ECCC6C416EB77D,
	U3CResendRepeatU3Ed__118_t9448C08E55FA338AE2D567878A8CBC7B77416B31_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_get_Current_m23FB6E14BEDD84646381D9C2756B6A1DD077C340,
	U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121__ctor_mC57CFA92A60222C607BBFE9DD9070ECE9FC02308,
	U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_IDisposable_Dispose_m058B5042FFD6D072E890FD42FDC4685BF9D90978,
	U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14121AE1F25F2547E709F627B873F6732220D5AA,
	U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_Reset_m8553DC27B6409578E06916683E3CEC4170FDAAB0,
	U3CtmpScrollU3Ed__121_t91B41F0A45EFF1E0A55F6521762B70E5D36E8978_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_get_Current_m1CCB279ED7CB5FD1F214F85541F1BE979D762957,
	U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122__ctor_mF1D34C8ED14D2838ACB9179BFF9B8E25060E347E,
	U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_IDisposable_Dispose_mC2A7529A5F4F8E37EF2543B0160108DAB4BE5EED,
	U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC18C03DECED7724F0A8C879A81A799DE616B8884,
	U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_Reset_m33B50615A9C70037B8F1BFAB67DAB62019C6611C,
	U3CBeginGarmentCallU3Ed__122_t30C010DD27B95E35569C3A88AFFE8E7967413B51_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_get_Current_mD4DB387972B3BB6B64DE01FDBF515D72DDFCD398,
	U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125__ctor_m4DC549290A14DEE1FD6853C90B3D95424FE0BA2A,
	U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_IDisposable_Dispose_mB1A2DA2D622318EF7C7882E054DA421CE22421EC,
	U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54E7FF932D9D77128ED4C58634C42ADBF5010C96,
	U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_Reset_mCF9361949B92FA15B21927BD9391E205FBC8A0F6,
	U3CImageTexHandlerU3Ed__125_t00AB47B0E4B732ABA38870A699C858587F362A84_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_get_Current_m4FE98BC68F8422625EC9F9C5B37277F30787E340,
	U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126__ctor_m8FA7206E28AE6338E7F743FD336B4F1EA75A6124,
	U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_IDisposable_Dispose_m49B8A7C618AE71A995D3411AC554ACD5F2232700,
	U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC465F56FE788453CE681703A3E164BF809528187,
	U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_Reset_m0377E1B1D0E57425A4B765F434124AA99B952FF8,
	U3CImageAddingProcessU3Ed__126_tF9094AE9F4FE44B4BBD6AB726CEAE90C3F7A273A_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_get_Current_m8444CD45133F500A05CCD3393B8594E45FBB4241,
	U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128__ctor_m25DB5088A39499E23E6A2A1196C7B42564F860F3,
	U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_IDisposable_Dispose_m6D6B952EC69EAE27B82D5C14AE57A6B4B87A01B5,
	U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7808CAD7B133848AAF612E1D96BF5F2E0E492AF9,
	U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_Reset_m78FBE24F99396A51AB662A48447758AB67DE39A6,
	U3CFbaseQueriesU3Ed__128_t2BFE028A360008197F1DC3C4764839FE96B1C0C9_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_get_Current_mA5F4F972647F1DC794480D6E4908D4F4550E20F7,
	U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134__ctor_m9900F913A83872687C721DCB5D9B714B700A1079,
	U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_IDisposable_Dispose_m397680E3FE53C7724B8685B74462B56C920316F3,
	U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m246861321A3C383AABF3B464F205A15A62B12B00,
	U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_Collections_IEnumerator_Reset_mBB707E3B20E4A99F1327035DA657A128F51AB0CF,
	U3CToProductU3Ed__134_t68E76F46CEA732DEBD8AA4987117C4920F9AC298_CustomAttributesCacheGenerator_U3CToProductU3Ed__134_System_Collections_IEnumerator_get_Current_mA0865F6EACB8D266C253B56FFFF15B13AD085323,
	U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136__ctor_m18CB1E44865D2185DC631E553BBE9CB761AD5195,
	U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_IDisposable_Dispose_m9BBAB55890CF9448000562B794994367EAFD12E8,
	U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43399C0073CD2064ABA95AC6724AA31CCDA971D2,
	U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_Collections_IEnumerator_Reset_m8B0A961330D62896C46FA5268C1506A17F5F32CD,
	U3CImportImageU3Ed__136_t23E5C6AC39BEF99C5479B0C6195304D4099DA33D_CustomAttributesCacheGenerator_U3CImportImageU3Ed__136_System_Collections_IEnumerator_get_Current_mDAD3A362FA8B96F11901C0389A2E0F1AE02C5D26,
	U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137__ctor_m64A892AAFE35BB0DB81EBC891907BAD699F14A11,
	U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_IDisposable_Dispose_m038DB0E832F38E7D0A7438FBFD21621EE4D66A9F,
	U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FECF7697F97244EE9AC8637AA540E38264CF7FF,
	U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_Collections_IEnumerator_Reset_m8771E3D830378C9D1BD0A73DB43CA451AA8718CD,
	U3CtmpU3Ed__137_t025BC1470380D9581E28BE9E18C54EF348F5B9A6_CustomAttributesCacheGenerator_U3CtmpU3Ed__137_System_Collections_IEnumerator_get_Current_m7FC964576C9960A08657AB667982AC5291A27D51,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_SetPrefs_mB24E9E1899BEF44D6C33A153F63B240862831A89,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButNextRoutine_mC8332E00005A39E745D65C9ABCE09820CDED8AF2,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_DelItemListWait_m34607DECD00D5A8203DD0C9B94A136F0D8C07A43,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButHomeRoutine_mBDA4FBC56EFB11901F98BF56D39FD62DF1BF5DB9,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButSignOutRoutine_mDB66FA14184594255315C663B57AEDC770551A90,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ButPrevRoutine_m179C7214D4B09084473108EF4B89F611FFC104B8,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_FilSrcRoutine_m7F3CFD187CE0378C12B5016ED5E472362C740472,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_FilUpdRoutine_mAB5BA98A6452BA0212A652CFFC4BFB301782F528,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_StopDownloading_m3811835D137C6DF0D3E289687A00193434FF42ED,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ResendRepeat_m33ACCF6A21E700039301FDCB1F057F8DC0E646B3,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_tmpScroll_m7C77D6633C3C6234DCEA3B50B9ADAB6145D6D743,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_BeginGarmentCall_mFD0B654FDB658DCFA40C758EADA73B06943ADF53,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ImageTexHandler_m6CC6A14F463CAA419779D706DCE0EF37BAA578DF,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ImageAddingProcess_m15B9C3BB4D6B081000AB2D595049CA71D5B32FFB,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_FbaseQueries_m4D8C6B4A237936B4ED4475DC22E360BB5364D55F,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ToProduct_mA72463E586F425D9C1E93F6C8FAAEF78CC08D407,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_ImportImage_mD2F4F737D7C782E165170EED54985C283FF55D13,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_tmp_m7751B13DC3FCE7A89EE6396F031D7585480E7509,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_0_mA8D5B273FC5BA5D94E2BF1ED4497CE6A3DAE824D,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_1_m3EF20EAD20C93BB7BE66740C163360D43939729B,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_2_mC9A25F8B804079C5FFB41CD8B5870F3AAAAD74DD,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_3_mE3A548D498C7971A9E09A67EB2D3A53490C1A218,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_4_mE059827DCAFF42E96F366E0D5A6ABE6DE6218C4E,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_5_mCCE5DFBA80917F86A0D33F8F95A90EE68FBD7B8C,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_6_m74B31F18DD846810AA503A5E18DFC3338A1EA903,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_7_mEF9860D29D5CE0D20BEA43B5CD887B4C52EFE442,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_8_m79681213EFBBA835E69B65411256690670A91739,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStartU3Eb__99_9_m555BE782F45EBF179B9BABE54B16492B6CEB8812,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButNextRoutineU3Eb__101_0_mF0329110041A2D13DD902A94C366BF88A5BB1153,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButHomeRoutineU3Eb__103_0_m3DD2F91A3E0847798137288B2BD5200AD3743A73,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButSignOutRoutineU3Eb__105_0_mBAC844154801247AF08098C35F5FF884679820A7,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CButPrevRoutineU3Eb__106_0_m9C4A5DA6DE7430FFD2CA7B92EF9FF19A2390BCD3,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CFilSrcRoutineU3Eb__108_0_mA644BDEF06923340BC7780C8B217A4A256DDB23A,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CFilUpdRoutineU3Eb__109_0_m49BB9EC280F925B74A31AA25ECC4574E69B5F686,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CStopDownloadingU3Eb__112_0_mB21FEE923B57F6D9C1FFA163A7A72EACE957ECD2,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CtmpScrollU3Eb__117_0_m40BA1F8A9DBBFF062BFA53257DC02DFE9CD623EB,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CImageAddingProcessU3Eb__123_0_mFF88D401B07CC1CDC0FD84D028C5E4A0AD0EB31A,
	AddItemFavB_tBAD73ACE4D7FE595B58331E8C300D4E6150772A1_CustomAttributesCacheGenerator_AddItemFavB_U3CToProductU3Eb__128_0_m12AB57D8327CC94A4ACF2D61557E599DAC507D78,
	U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100__ctor_m76E7D60293C830A2EB67DE51B7781AE61C4BE76B,
	U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_IDisposable_Dispose_m7A07429A4ACA08DAF578053504AAAEDD40BE443E,
	U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09ECBE0E3957BD7BE662F0A0D8F1423E702C59D,
	U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_Reset_mBF33BE7CA5F9C5F1DA7EF54857F51C9E81D9ABEF,
	U3CSetPrefsU3Ed__100_tA5C74A492A46FA581EA4F439E9ED83ABFA90A83A_CustomAttributesCacheGenerator_U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_get_Current_m7891B9920CFC41570FFE0F5564EFA91AF3E18D7E,
	U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101__ctor_m770F9523CADF1BEF7FF7C16013118EF432B0C662,
	U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_IDisposable_Dispose_m8A144B4DA6125320A42AF0152CB56D7085711F64,
	U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE566BDB6564C589FD9A492BDAF211E8BE551926A,
	U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_Reset_m0ECC8CDBA0AB2C92963F60C9C5ACF1D68091BCF3,
	U3CButNextRoutineU3Ed__101_tE3CF13A02CE11A57FF94F4993BAEBFE41D06E412_CustomAttributesCacheGenerator_U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_get_Current_m88FF3464C964E918C4D1B4563158F2E2742BF34B,
	U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102__ctor_mE4FD584463368E858E6500444E13560D39A8E7E4,
	U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_IDisposable_Dispose_m84599E19DEB6523032C1B366CE872B1CAD3F029D,
	U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E1971853F5E1897DE572CFE2A1F5CE93DCC2E11,
	U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_Reset_m9AC6364FB08D83A09BA4918A401DADF38A842113,
	U3CDelItemListWaitU3Ed__102_t2D71A616015F6F8C479F50C732FCB9ADFAF03E74_CustomAttributesCacheGenerator_U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_get_Current_m9A7B9240C6F6E8EFA321E3470395DD66C24C5F9D,
	U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103__ctor_m8833F61EF7E0A7572E454A323DF97D20BA2588B1,
	U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_IDisposable_Dispose_m7EEEB03EB525F93260C6A019B8A907A9FEBB3F8F,
	U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C82185097BE3FEAB452F9C1ABD34D4DCBFF679C,
	U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_Reset_mE8665766A9A6D613DAE425A1054D00FE793FA5EC,
	U3CButHomeRoutineU3Ed__103_tB9CFE4B50D938E82F6461FD17CB7A76C60412606_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_get_Current_mD32884E4815C7AEA078E5202A3399B8999BB570E,
	U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105__ctor_m007F2C5053AA977BE6D4BA0F4B26EF8168F62BCF,
	U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_IDisposable_Dispose_mCDB3459813B11DBED7B42C011506ACB7F3FA7FDD,
	U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC0D8BF7401FD148B2B40B4D9BF77090FDEE7406,
	U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_Reset_m1BEDE53990B83198CB62F9F2246E3AB05D7286F8,
	U3CButSignOutRoutineU3Ed__105_t205750546FC055AB8119BB4702C1DF6CFA696DFF_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_get_Current_m4768AB7E02D5CD67EEF47DCFE270C258F0454E7B,
	U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106__ctor_mD5DB2A5A9B55B135C05747812341B7DEF3C10503,
	U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_IDisposable_Dispose_m41E2986903131B9738B1D226DE03735A5E5A8393,
	U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m677A17883231084C84F5D047340EEBA04A75E6CD,
	U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_Reset_mF8889B67CDD20BE3EC695C1369D51E699AF71F59,
	U3CButPrevRoutineU3Ed__106_tCA4876A9DC6E84D8EF593A856C3BC4547A5BF461_CustomAttributesCacheGenerator_U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m6A78883AB5236917A0C8EEECDB55B0650B83138F,
	U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108__ctor_m4A95C6E500D70B86FD6E61E285BA194978AACC8C,
	U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_IDisposable_Dispose_m02E3089371909C6F27EE212E7AEFC5AD54F45A63,
	U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF2BEEC524BF85436365222AE9DCE459A093759C,
	U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_Reset_m1C0E3E34FE3BFF6CE43A3C4711E7498BCB8026C4,
	U3CFilSrcRoutineU3Ed__108_t31CB4C01FF6D758ED9D3AC4487BDABE51C540729_CustomAttributesCacheGenerator_U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_m6C97BB788CFF7957C613C778C2B2224FEB864D37,
	U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109__ctor_m996A8F53737D39937922BAD80D230D2CDA6C1D9E,
	U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_IDisposable_Dispose_m076118330E20CBC1EB2648A81B6B1C1E97160BB6,
	U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m851CADA05D65739DDA283DC975A580F7DDD0CA2D,
	U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m4E5E5700E9A4F96411B9DD5C3F3E0C1D318277B1,
	U3CFilUpdRoutineU3Ed__109_t7E1E20BC6F8B66BF44D28BAC7F07E9CFCE1AF48D_CustomAttributesCacheGenerator_U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_mAB074BD2F4B1C09BDD6D48EFFC3AF7E73D1655A5,
	U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112__ctor_mF6C2EC9ACE60A4839D8D46C1C43BC6281C03AEF8,
	U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_IDisposable_Dispose_m0917D6FA891909094BE58CCE929228AB202B62FD,
	U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DD6FFCB5D8649CB1E542FD833A680725C891CD6,
	U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_Reset_mA01D081F3449DBD67B05E87BB4B63E3333994878,
	U3CStopDownloadingU3Ed__112_t0A79D2A085301FAF334455E5AC2554ED859F9D0B_CustomAttributesCacheGenerator_U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_get_Current_m6434132FC738B3A11CFAACD960F507329B563756,
	U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114__ctor_m3F5BFEDD90E83C5794F3D831B106A8B58CDC04B8,
	U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_IDisposable_Dispose_mC1A4224C9CD3D4C57EA721AF4825AE14184C37B9,
	U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m719B58A1C0991C1BA3B02879D4A1A20254AAB6C9,
	U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_Reset_m5FCD37DD1EE430EDECE9ED6DAB1B6A56E04C2392,
	U3CResendRepeatU3Ed__114_t66B40D5535559865D39763974D84AF62D889B874_CustomAttributesCacheGenerator_U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_get_Current_mA40A27CD9848559171D6093EFB1937A3DCE29D7A,
	U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117__ctor_m7C268731D5F8756B86633FC062CCC952B6740FE5,
	U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_IDisposable_Dispose_m831B53BB37B2E9240405FA9BE9F1076F02087F06,
	U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m224B1BD9D4CD4FB193F8A17EC08A02C6E8ECE348,
	U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_Reset_mD408582C33E6F0343EA87EC85DC6FD882F3B218A,
	U3CtmpScrollU3Ed__117_t38A0D5CB47B0D70CA0B28958F54247B244872DEF_CustomAttributesCacheGenerator_U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_get_Current_m7601D10039050BAF1A5EC3F95CF7F794E28876C9,
	U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118__ctor_mFF9ECD2897932FB44B8FDED5ED73D79BD14E8E8D,
	U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_IDisposable_Dispose_m4F58A0CDBC68DEF08EE185191E23BE180CD4AD34,
	U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27291F3DF06BA60F244574ABBB6F46214B0DA02F,
	U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_Reset_m195F2702399FBC0ADC51B2205A1029553D4E87ED,
	U3CBeginGarmentCallU3Ed__118_t5EFDEA7F43450276A23D15AEE4D781AE6FC031C8_CustomAttributesCacheGenerator_U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_get_Current_mFCDE05B088B0E75E0D13F80DFA5BACD20AF5FF74,
	U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122__ctor_mF7419C5E9365A97FB7C1DA3B0424FDDD07887E25,
	U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_IDisposable_Dispose_m9AA69120AE92EE8525CD5258A84ECCB851F6A777,
	U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91B0112E2D4FBDA7C9BB2CE5BB6011D358ED2A80,
	U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_Reset_m5BF7D635AFBE4EB47303002CB2C6AF54DB25E364,
	U3CImageTexHandlerU3Ed__122_tBFA280DF70F12C528E0C44401904093F65416DC1_CustomAttributesCacheGenerator_U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_get_Current_mA1A91EE97DA546664D9806E5363E4A5D640B1F87,
	U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123__ctor_mD95084F0EADFB19F6539589548840D6AD193B303,
	U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_IDisposable_Dispose_m575C4A8E4B54ABC855E1869356DBE5701BBCFC6B,
	U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8059792712242D80BA76F0E9D00EC3DE621E69E1,
	U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_Reset_m13A4AEA4FB51F19C57232D20FE71D9D0E95F965D,
	U3CImageAddingProcessU3Ed__123_t8C3AFADBDD7C4CD090F4B7306983DDDBA542FFE5_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_get_Current_mB06D6DA393684E65C8B457E41A43D251E15063C5,
	U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125__ctor_m0021C3BB0BEFE90B6EC45561DF67D8B90CC2F05D,
	U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_IDisposable_Dispose_m16D84B535CF2EE6EBC48C2BEE44FACBB6172BD57,
	U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m482847F1098B4057D8AE595F730B5DB8CBD9CA56,
	U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_Reset_m5A083EF573E4B42EF6EABF3A0F000DB6A9D1970C,
	U3CFbaseQueriesU3Ed__125_t38DF061D01DF97FAB2645C013F5FE7D71C57CE56_CustomAttributesCacheGenerator_U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_get_Current_mA7E00797E8DF86728C49DC78C62E20956683FAD6,
	U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128__ctor_m6A9A821B8BABFB4F8CE697657BB2FA1225B4082E,
	U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_IDisposable_Dispose_mAA22CFB62E9C7491744FC997FF1567091C6A754A,
	U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2014580B242F8751BBB63797DD261AC125C09DC,
	U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_Collections_IEnumerator_Reset_m3C019D7B395DA65959C6C26F5CDD692641DC1156,
	U3CToProductU3Ed__128_t4E8623D81434C2F09E0745751D82D59ACC41FDA7_CustomAttributesCacheGenerator_U3CToProductU3Ed__128_System_Collections_IEnumerator_get_Current_m866CBF81E26395A49826034FB021CE050BDCCF03,
	U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134__ctor_mE4C6E2BC19C6F1318CB4F0BAB54870E62A778064,
	U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_IDisposable_Dispose_m799A659DD685A41779A1E5664864393915A0AF8C,
	U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96D8D603CFA9A391F5A246114AEFEEDF6664C9F8,
	U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_Collections_IEnumerator_Reset_m0005C3A33E2057DF0A8C59A47855A44B385D0865,
	U3CImportImageU3Ed__134_t5A02F07FB8BF469398294E039015AAC4F50176DC_CustomAttributesCacheGenerator_U3CImportImageU3Ed__134_System_Collections_IEnumerator_get_Current_m2B7726DF26F380FDA790258265D8E5E2831759D8,
	U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135__ctor_mE8EA412BD2B6E55EA20FCA793A6446F4CF18BAE9,
	U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_IDisposable_Dispose_m800554F20A5F13ABE7D22BAE6F37B0FF286CAF32,
	U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9363254BF1666CFC6D4530829C09BC31CF242E00,
	U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_Collections_IEnumerator_Reset_m4045FFCC29548CDAAD61AF52B90AEB91B3090987,
	U3CtmpU3Ed__135_t3476DAEAF70B9AE521FD9B57A1A030B7F4A855E0_CustomAttributesCacheGenerator_U3CtmpU3Ed__135_System_Collections_IEnumerator_get_Current_mFE3562A87D8848233B9A20676B6178F80562E0A9,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_ButHomeRoutine_m3A610393D49D4219306D680960B890A93A91EB14,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_ButSignOutRoutine_m512500DFBFFC99B2ECDA6B0E017427098D8440F3,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_CleanTex_m8F1CD069B973B2F305B451EBC3092A3272A2FE1A,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_ImageAddingProcess_mE0C64BBDA23A454F59F6386BCD424777079E19AE,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CStartU3Eb__27_0_m320F1D837FDB0993585F54FBEE60486E106C47E4,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CStartU3Eb__27_1_m58A40C02BF1EDD867CECF3998ADB67E23739EFD5,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CButHomeRoutineU3Eb__28_0_m0421464A105FB4632D590F221E4A092B9EFABA41,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CButSignOutRoutineU3Eb__29_0_mECADDAC0D393D555328D4CEB84A4AA54BA90FB2B,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CImageAddingProcessU3Eb__32_0_mE5C01CEF59A35D36831ACDE62AB012B49058645C,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CImageAddingProcessU3Eb__32_1_m6FBB2D3F4AF2D56D3F48FD456C2FF6E4D23A0150,
	AddItemSingle_tB344924D930F484662F22F7CE4D99CCE218CFFB8_CustomAttributesCacheGenerator_AddItemSingle_U3CImageAddingProcessU3Eb__32_2_m773DFB3E50C1620401ACB394E2E213DE9E0F4E23,
	U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28__ctor_mB4504AB9D5F362706B1B45DF56DE52D86107543F,
	U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_IDisposable_Dispose_mF7FA8E483FB630014A420E312019C99A4C91630A,
	U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CB4964F1334604A1DE3B425844529B9333DA1F8,
	U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_Reset_m0A2FE0CD2B6E84F803236ADE6B38B5E67E80858D,
	U3CButHomeRoutineU3Ed__28_t496540183C47D49F4B3F8B0950A2A454F2BB703F_CustomAttributesCacheGenerator_U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_get_Current_mFE4C850F5F097C4BDE565B8F00258284878BBADB,
	U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29__ctor_mE815D9176BA4FE8E1196967DE1D6D0E66D3AB8ED,
	U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_IDisposable_Dispose_mD297951099D5E22898EB1BF799A729585C7122E8,
	U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB6552AC06A51E5E351C52D5EA3F576AC9DF591A,
	U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_Reset_m9AAD08A86CC8BF57733370B1172044DAA919B197,
	U3CButSignOutRoutineU3Ed__29_tF0F03A538BD08EB1AD3E55FDBC7B1E6FF9B201DA_CustomAttributesCacheGenerator_U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_get_Current_m3F0632BD67CBA8E8A98ECCAD924769029E3994C8,
	U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30__ctor_m0519A26FB715C19DB3B1BC92370C8546FFC96ADC,
	U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_IDisposable_Dispose_mF5AE38C1BF625A1711988EA9BDB0257714C52C7C,
	U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4518055E3B70544495786AD3A5BB96DF4AA684C,
	U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_Collections_IEnumerator_Reset_m569B1DADFD37C2B6E6EB20CDE72DA244FB9C72D9,
	U3CCleanTexU3Ed__30_t795931A4AEB37CF9A7B99956F52EAA65A1F29D34_CustomAttributesCacheGenerator_U3CCleanTexU3Ed__30_System_Collections_IEnumerator_get_Current_mECC037CFDCC6AC9FF87C1D02AAA62AA5D30465FD,
	U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32__ctor_m854CE612288CB526EA88059B6A9075B463749D16,
	U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_IDisposable_Dispose_mFF956FF062FDF19085C086812D7323C48DD38F74,
	U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0F9BCD72DD34F2D3C1E470C814BDEF4ABDD0018,
	U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_Reset_mDD12A1D00339C63FCE4232072A6A0140F2415C2E,
	U3CImageAddingProcessU3Ed__32_t91D9995E8478CFDC4C694BC52D78417FE64BDED6_CustomAttributesCacheGenerator_U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_get_Current_m4BA238ED68F161B93A9D59108075FB6AFC756CCE,
	U3CU3Ec__DisplayClass33_0_t8987DDC1C9D5D49E7AB590FF5FAC74478332B94B_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eg__ViewLoadU7C2_mDE7ABBD9C4B3FEE0C399BE88F28C36D50796E910,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed__ctor_mFFC5CDFA51BD0578917E8B7D206E899CCE14443E,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_IDisposable_Dispose_mF6EF7AE7E4453DA1F88CFDC0B67151108164418E,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F167D0D324A297085FF7718472A0DA97810714E,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_Reset_m51DA00B6F13F0F29EA10E6464B2994FC00F3A643,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_t5DE35B002213761FF62D37FAE5A77809C96321E9_CustomAttributesCacheGenerator_U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_get_Current_mD147C9A9E2ADE062EE4942F87F7BB7448FBCA02C,
	AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_AuthLogIn_U3CStartU3Eb__5_0_mD050B6932D242E0CC487AA1625BC28B7F8973C25,
	AuthLogIn_t56D68978026E81B344555F2C9994EE8B41086EDD_CustomAttributesCacheGenerator_AuthLogIn_U3CStartU3Eb__5_1_mA894BEB560F0D5DE6DC808C3EBDC62F0C38DD8F1,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_0_m8493AE06DE46BB2D14988EFCCED4A6F704ED723A,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_1_m279DBDEDDFFE381FDAD7941FCAE37CB8C80CEE77,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_2_m4C82B7B7FFC9F7DE9D05BBC79E8094FEE9B59881,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_3_m0EDEE6B256DBFDCE07F6557CCB638B68C1C4F081,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_4_mFCD9F25CEA26FAB2B305E94A54D9DF59591EF696,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_5_m1AEDA456989B62CAAD3DC6FD724F8F321182ECB4,
	CreateAcc_t1EFB451F88F6EAE80C7498C061E2CA4D3373F1A0_CustomAttributesCacheGenerator_CreateAcc_U3CStartU3Eb__14_6_mADFF5EB61973959631C2F5FD92E7D65D5D99B8F3,
	DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_UpdateInfo_m1233C2A1B3EAD9AACFA1A17F6ADE9950491F9A2E,
	DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_U3CStartU3Eb__16_0_mD5E5F262F4764E4BDD121F758249F5B07B318230,
	DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_U3CStartU3Eb__16_1_m4C1733F6C750ACFCA947DDDF8E0C70C3F3197A98,
	DrpDwn_tDCCD9F11E122E751C1CD03EB4FD9CC833D785A92_CustomAttributesCacheGenerator_DrpDwn_U3CStartU3Eb__16_2_mAA43DE9BCFE604E3E8ABAF9F8B3D390205896289,
	U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17__ctor_m3FEF25B5AD5BC513EA9D5F77465BD37D6D43EB50,
	U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_IDisposable_Dispose_m16C5556EAC8EDC7494F39C587FD33B54F4AAD8B7,
	U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB4C894555B34B6737C66E1D17ECBE9DB1C7927F,
	U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_Reset_m253F46F270B3551A1D33007329863296A8A95CAC,
	U3CUpdateInfoU3Ed__17_tC9A3EFAD676E1031F03337C432FEC79DEAFE0901_CustomAttributesCacheGenerator_U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_get_Current_mA27DF123CD6808FFCFF01A6410EDF7D56732C434,
	DrpDwnReg_t0AEF42AB1E23FBC5836DEBF8F8ED71CC5F300FEE_CustomAttributesCacheGenerator_DrpDwnReg_U3CStartU3Eb__10_0_m37F2FF9FA3B44096345696AB66223D8869443F40,
	FirebaseLoad_t7C29D9D5D2C6F5B73B81D42D369F8ED8936B983C_CustomAttributesCacheGenerator_FirebaseLoad_ARDocsFV_m723E735998B8B4D20AAA96EB69FAC26E117084B5,
	FirebaseLoad_t7C29D9D5D2C6F5B73B81D42D369F8ED8936B983C_CustomAttributesCacheGenerator_FirebaseLoad_ARDoc_mE3C0EA4A11B5C67EF5AF8BAFD0D5BEA999FBB461,
	U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30__ctor_m75CDDE36D3FFCC64B53247BA6FFF09812B19B611,
	U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_IDisposable_Dispose_mCA30BA2BD4B04206E965F6EB72F13B58B4774741,
	U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5700A999E0CADC5394B66BAAEC398D681B28FDC0,
	U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_Reset_m133F4D7C73219BD846D577CA416A3FD3E71F788B,
	U3CARDocsFVU3Ed__30_t5CA3BEF3432594B8AC7FAB06AC6E61A5A93C46FA_CustomAttributesCacheGenerator_U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_get_Current_mA1F3A732F1D8F4FA9FD6BF4BBC3ED3E58DED5292,
	U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33__ctor_m960E0A911B1A28A79ACDB501FAC11582CDC61C74,
	U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_IDisposable_Dispose_m1F7771C8269333EDF80C8A0A9BC6900F7026A6B1,
	U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A477DE8915FCE6C5B41631BBF0A1149DF46AA5E,
	U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_Collections_IEnumerator_Reset_m829D659EDD2CDD8D554D68CA96FFBFB260607642,
	U3CARDocU3Ed__33_t44702E1F8D2599B889ABD50C28A8C6A391B727CC_CustomAttributesCacheGenerator_U3CARDocU3Ed__33_System_Collections_IEnumerator_get_Current_m488B0F68F860C0A9E1F27F591DD7E840B49E9DE5,
	FirebaseLoadProduct_t21E2DE2EF93B9DE0EF26C57AE1EDFE54CC01A0CD_CustomAttributesCacheGenerator_FirebaseLoadProduct_SDoc_m3568BA13939C82452402248225F7027FAC287E6D,
	U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6__ctor_m11F634D3BA0FE2F0429A352773685482007C1CA9,
	U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_IDisposable_Dispose_m45D6F15DC5EFC39587FBFEC0379E79580C78CB1A,
	U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FDA9AE366B9131C3E4028C87E0F19AC0E02DFB5,
	U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_Collections_IEnumerator_Reset_m307C49A52A70B1F44B5E4422D634A0CF460E5285,
	U3CSDocU3Ed__6_tE30A9DD0473E5D34A002A326389D089AAA75CDDF_CustomAttributesCacheGenerator_U3CSDocU3Ed__6_System_Collections_IEnumerator_get_Current_m19E7A850C40FA8D83DB0085945AB5D14CDF3F34B,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CheckAndFixDependencies_mCDC8E2754EC8ECC9B3E0DC26278D64E9077899C1,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_checkAutoLogin_mF4122E6817DA0E8B51DB444A81149C836D75F1CE,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_LoginLogic_m57B61E650C749A3D740CE8E03A35F1F099EF9A4F,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_RegisterLogic_m30CBFD9C2A3A498E6A7427FC73DA16B03DDB80E6,
	U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20__ctor_mE513235E9CEFAD28389F4BD273B0234A2546E9A6,
	U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_IDisposable_Dispose_m50897C509C5058F4E638DB16A8BCD88C2262F6AF,
	U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66C8BEDCAFFA24358C0D1DCA597C7DD35CF55F50,
	U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_Reset_m0ADA09A0EE9657765B6A84B223956B114B8A8479,
	U3CCheckAndFixDependenciesU3Ed__20_t7FA4FEE7E4839275D8D36B4BE4AC88194AE04223_CustomAttributesCacheGenerator_U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_get_Current_m1FC420C47DEF8F1B4127A3E12EB95BB1D8AD6AB5,
	U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22__ctor_m872281A15157F52029A9E8A929C8AC92CBA9C840,
	U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_IDisposable_Dispose_mF912F49AC2DCF9900F49A2BBCF2FA99834A0F8B2,
	U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99FE1B44D48593D6CA001BBDCD7F9857C6665444,
	U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_Reset_mDBD4A544E27C49C79F8AD57FC51E2F9818F0AB0B,
	U3CcheckAutoLoginU3Ed__22_t2C5D85E3A46289F83C188EF2054A9201F11E0569_CustomAttributesCacheGenerator_U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_get_Current_mB0390E85E68A1251F204074331BB25608E431ACF,
	U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29__ctor_m237FCABA55986243AE341530B887A7B9CFECF3B2,
	U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_IDisposable_Dispose_m56262A632D7BBE88A4049CDE4E61400A42DE5411,
	U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B518C7141CFA0652289ECD3B525D0285655B7D1,
	U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_Reset_m7A22EA515F8C3136ED8677F16282E19F0D98DDD8,
	U3CLoginLogicU3Ed__29_t4CC3C8E94824BFF846791815E768CBDFA0FB593C_CustomAttributesCacheGenerator_U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_get_Current_mCBABEFDD04C2BC6495F7DB8329606E5E19172F38,
	U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30__ctor_mB79F27786D67ED3D68638817B1BD0B2089F1FDBD,
	U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_IDisposable_Dispose_mB5B9C8F6193294A31E5211AADE5344B5C0F68D14,
	U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABB3EA176CD17130B14FB881A35DD2D15C6C4A6F,
	U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_Reset_m777E21375ABD9F78DD640A7188273C00A51F78AB,
	U3CRegisterLogicU3Ed__30_tFD1A04AB28E250AB9E7B507943843F6A2A2DC272_CustomAttributesCacheGenerator_U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_get_Current_mBBB8E1D2A536AFC119B513D81CE9ED58929E784D,
	GarmPop_t975DBE2C427EE5E82338538D38351AAE6AC49B56_CustomAttributesCacheGenerator_GarmPop_DownloadImage_m84173F9995B260824C08FC1C11F8B4C8035E3AC2,
	U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27__ctor_m4A14D0747572B8EE136B45D12D121AFE2162C2C8,
	U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_IDisposable_Dispose_mF6799C18193605A8BE990E1BEFAF3A3A920D975F,
	U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2B5B8ABD582D8F5B844A24F13A3C5B25B52AE1D,
	U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_Reset_m77E7F96BBC7786A52B73A5EFCA9BEC65D739CC29,
	U3CDownloadImageU3Ed__27_t2C69FFD16BC76B8642C7F911334D7B64B3229090_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_get_Current_m44C8F7E23295F58BC5D19979F36A056B1A68C76D,
	HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_HumanBodyTracker_InitAr_mBEB9EA30F3D57CBB6B997C95EE4E5860A59065DF,
	HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_HumanBodyTracker_GetAssetBundle_m88CEBCF31113436C0DEEBE6E8FB712F4F4E7871D,
	HumanBodyTracker_t36FA73F26004B9C7B08378DD042427AA57C6CABA_CustomAttributesCacheGenerator_HumanBodyTracker_U3CInitArU3Eb__14_1_m4DE5C014E90FB1395DECCE1D36220252B78B7BF4,
	U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14__ctor_m0BCE60FC071A5BB60B68A3F58582F4072B03F4A7,
	U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_IDisposable_Dispose_m2D6CADDAA2F5A47F381E9C8197F4E3DE4998CC30,
	U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5ECF5BB94B05D7C55A84197D235473F98DFAD1F,
	U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_Collections_IEnumerator_Reset_mDAADBFE05CAAB470F57C48D2380BD54640B285A9,
	U3CInitArU3Ed__14_tD857787942AF2E6EE52564E21E72D3893B4E112D_CustomAttributesCacheGenerator_U3CInitArU3Ed__14_System_Collections_IEnumerator_get_Current_mB4952B1DD52B1A6B63B24723EA9B6C6E5704EE9E,
	U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15__ctor_m401E6206298334CA59DD8FAD9E64B78A6D1DE8FF,
	U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_IDisposable_Dispose_m0F4FD2EE83D390DD3506608D143E0521AE2B902C,
	U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m281ECDE9CA2D3588CE643FDC7865D477D53C70FA,
	U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_Reset_mC0ADC36AB218452B7F2409B765C99D0D82E8EB8F,
	U3CGetAssetBundleU3Ed__15_t0DA4DD052C5709DF8204081362E9E16A4BF5F609_CustomAttributesCacheGenerator_U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_get_Current_m6419ED791E01A27B653D693CD250F4BAE9699232,
	HumanBodyTrackerAvatar_tC4C1205BD20B8913B2130223D1AB28BBB2CB9122_CustomAttributesCacheGenerator_HumanBodyTrackerAvatar_InitAr_mBB6CB82039FBE01973C0A9FEDD0727EAEE8430DD,
	U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17__ctor_m5D62044BEC329579BDD4C8F4BAE9B64F1A9AF6A8,
	U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_IDisposable_Dispose_mC04A34D6A9AA9D0ADE679858B6A0CF8EDE9098C2,
	U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD40A5A4069316DB30BB2F1611247BDBD1F5345F3,
	U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_Collections_IEnumerator_Reset_m95F44591F726BD047A6BB8920E7B62C2CCCDD776,
	U3CInitArU3Ed__17_tF2BA9B46FD0C9C5A75225AE9FF72FAE7058C6EC8_CustomAttributesCacheGenerator_U3CInitArU3Ed__17_System_Collections_IEnumerator_get_Current_m6017680888DB02B9B20F061F4B8C9EC90E1562D8,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_AvailableColors_mC1C04209D418DE6B950FB40DC296DB41724E6CF4,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_InitAr_mD74EBB8D82EB0E679796BC4561B5C1AD66CAEA99,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_GetAssetBundleInit_mC8408C8F96732956774018F9279CF136A3DB118B,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_RespawnColor_m1AF0CD10B47DBF05E07FB6CB2DD5E7EFC91C86B7,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_RespawnSize_m187FE19EF6C2040513748D61E17797E13C3B8A34,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_GetAssetBundleExtra_m6BA170E938C7A8E18D07F75EBFFE195A7D0AA8C4,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_U3CInitArU3Eb__41_0_m77EFB7D5E455F11427FFE4DEB5A5E7DFCE0DB687,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_U3CInitArU3Eb__41_1_mC654DD0D05916ACE0CFBF0C58EFCAC4876750B07,
	HumanBodyTrackerGarment_t88BB9AD26CEA8B0EE2592CA5081494F3954311D6_CustomAttributesCacheGenerator_HumanBodyTrackerGarment_U3CInitArU3Eb__41_2_m9D21EB7E7EC5693B339E241A00BB7A0FA5427BA9,
	U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39__ctor_m0E7ED7000437E952D8E68D1C62B1316AAA31A049,
	U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_IDisposable_Dispose_mDBC0CA875800EF0E74C0C02CEC5C5D38966EACC8,
	U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m71E0D4E1D33EB11C00D58C59F1CB4EF672574BF8,
	U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_Reset_mB982CFD7B68F4859E7412FE32A24554DDF8B9E34,
	U3CAvailableColorsU3Ed__39_t13FA2789697ED2A7107D9573DF702CE7F5B8FB14_CustomAttributesCacheGenerator_U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_get_Current_m83710E3C8D34400DB97D8B5169AAE0BDFD131444,
	U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41__ctor_m1D5A3ACACB83EE5605FF4AC082EB241A1460BCDF,
	U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_IDisposable_Dispose_mCB2CE8CBF5674FD98B569F02FE5AC26555DE020C,
	U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC0533830866A8CF98F479EF5B8106A8B635384,
	U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_Collections_IEnumerator_Reset_m67F2B799690EB2F34910D863BD4CB2C83F3E1809,
	U3CInitArU3Ed__41_t3239BB8514372B09ABEBF6463581469EB1B2676C_CustomAttributesCacheGenerator_U3CInitArU3Ed__41_System_Collections_IEnumerator_get_Current_m29DD6B0CAAAF87F7F1322A81E39F4F0788B86053,
	U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42__ctor_m3BD2C502FFA7F018FF2A9E5556E12D28D147BDC5,
	U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_IDisposable_Dispose_mA97EB664D7C245AA9DA4CBD3507F5603E8464FDB,
	U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F3D0239193B5CBCC7AADCA905298F2B85B3870D,
	U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_Reset_m9F64FD56FD033B71B1AEDF751D313A81230B2FC7,
	U3CGetAssetBundleInitU3Ed__42_tA626BD6782BCF911194D6718FDF2E964BB84D3C9_CustomAttributesCacheGenerator_U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_get_Current_mEE72D133A4AD3A0E74B5164CB3F09F72E28D87AA,
	U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43__ctor_mCDD3B2C9874ADE71189C1A2ACBDAB227D2455955,
	U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_IDisposable_Dispose_m7BBE4857CF406984A98BD1C95C6F40D8DEF5A516,
	U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D63008CE9C30952B4DEB9402BB40C2C291BF02,
	U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_Reset_m7FBE3DC6DA7C4020EE8EE7D13954C9FCAA874F81,
	U3CRespawnColorU3Ed__43_tFB939FE84EBBFE001C5F969C2956783FC8C7A00D_CustomAttributesCacheGenerator_U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_get_Current_m5EDFFCF83AF6C196734AAAA90F2CF68397620C8E,
	U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44__ctor_mAC3B30605681C825B91BBA89893EF6CB1A3D96E8,
	U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_IDisposable_Dispose_mA0ECE09AB7D10863359BF0B7026FC47BEF15FF48,
	U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34077D9F5C1F11EE301947D160A382D1349E45EA,
	U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_Reset_m3FDD44A4DF8529AECBA79B66DEF8E53C8AC5356E,
	U3CRespawnSizeU3Ed__44_t26C05BC20926CA25CB8C75683776276033B432C5_CustomAttributesCacheGenerator_U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_get_Current_m7F780518B35ADEDB939AE4E7A59237B3BF7F42BC,
	U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45__ctor_mF7B098B86805EA416C3462209C68E05D57BC41D6,
	U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_IDisposable_Dispose_mB00EE4A9B81482B388F6A20A290B0D0752167601,
	U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA81081F88F877593B79FB77044DF848A78B7CBF,
	U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_Reset_mAA2607639C0DF413DF98F03E05E7B59C31B83CCC,
	U3CGetAssetBundleExtraU3Ed__45_t616035E2B2D900D579D2EDD7BD404430500B0917_CustomAttributesCacheGenerator_U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_get_Current_m9E60F5E5C1D05D243287FD86E4ADE78EE4ABAE36,
	SceneChanger_t6A30EA4853DA52DBD1479ADCBE7B3B6952D1E068_CustomAttributesCacheGenerator_SceneChanger_Logout_mCC126448679F9759D5D0EC6DACB7AC41863FCEE0,
	U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2__ctor_m8FF31D23636AB1BA8FE7C518DD99F955F57483BC,
	U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_IDisposable_Dispose_m19E8D33544E5D32EBA7F3CD5842047218D244A58,
	U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCABAF6E94019D40B2B6971BD8E7801A981335B65,
	U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_Collections_IEnumerator_Reset_m50F33DB45F147049E257D2706FA09BC1E33CD0DF,
	U3CLogoutU3Ed__2_t3A5A90FA8AE4A49CFFF6C717198159D4B9738240_CustomAttributesCacheGenerator_U3CLogoutU3Ed__2_System_Collections_IEnumerator_get_Current_m5195917ABAB3B2D446AF9DAA5BB46F03FD156627,
	TextureManager_t472D9185292B5891A8DD124ABF21F42C15CC65D2_CustomAttributesCacheGenerator_TextureManager_texHandle_m439B211B1DEF03FA31002E8E564A3DF8BF6B0049,
	U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4__ctor_m78A498D373C0F1D47DF31B65F65830A922CCAA04,
	U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_IDisposable_Dispose_mBC6A134D53DCB893C0332E66F377E631EC743E83,
	U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61A1F42F4519A4336BDE57BD8AF01BDB6A3EAD09,
	U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_Collections_IEnumerator_Reset_m5A9853940637A064502F5F4897D24B489826CBD1,
	U3CtexHandleU3Ed__4_t0B3729A60F80CA771D64ED0FBBA8F236FC6739F4_CustomAttributesCacheGenerator_U3CtexHandleU3Ed__4_System_Collections_IEnumerator_get_Current_m8473DEAF348680FBE1E0506F50C6FBCD80D6AB7E,
	CreateObiComponents_tD88C34BDA67096EA93DC45B28FDB7222B6A7D2D8_CustomAttributesCacheGenerator_CreateObiComponents_CreateComponents_m819F1F20C2601A5E5C8AAFCAEA2FAD334F569908,
	PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_add_OnObiAdded_m5F1F706772F4FEB073C6EC4657E856AC8F46924F,
	PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_remove_OnObiAdded_m47896DA4AC0060E8BA14323E36446E7DCC56A7C3,
	PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_AttachObi_mA2D9B03ED5F9CAEDD8C47C859101CAF89F2CF059,
	PartialObi_tAB022A9FFFB15BFFC39F922DFE005A4B8AC8BD79_CustomAttributesCacheGenerator_PartialObi_CreateObi_m7B19FEF5D807AC60C14A480B266EC3FA4571E603,
	U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12__ctor_m323E149194970DEB7C74D42B50BCD92DDFA10E58,
	U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_IDisposable_Dispose_m606C4A0C39F85E47234D008EA5C15466EE21E6B9,
	U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9F06E12394352EAA6288915A8E1A9FD2A9020E9,
	U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_m7C8BFAF6A415731CB8B4DF5702063CF293F51321,
	U3CAttachObiU3Ed__12_tA2B29DA4B9C318C44B2902030AA4A54E9C33E321_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mB54B79F9A99CD527CA0F69AA8F4D51E10E398815,
	U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16__ctor_m62416639B5CB564A96F41F34AFF4479F4EC0F737,
	U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m20CA1D1DC9156B9D0BA1C9B45D02B4276BD0227B,
	U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1153171D17D5A85E9D1089961F56E8C35031B60E,
	U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDECD8FBF73AC546C48D6663310A224A229D2A20E,
	U3CCreateObiU3Ed__16_t0DDFD9F462689E939A0E4F730C6829ED2A574DC5_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m20F4A3067E9EE93EBE98BBB16E3A8C3BED678928,
	SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_add_OnObiAdded_m21C4DFFB388835AAF49AD553672C78EF8083F487,
	SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_remove_OnObiAdded_mD59D6B40F02F799BD8BACCE38AD4BC2D703FF9D7,
	SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_AttachObi_mDEA5E44A4A4BD87B69355AC66EC89C5E98AF6099,
	SetUpObi_t7AA2BDDBBBD4398693E29CBC04EBC453FA6D4A9E_CustomAttributesCacheGenerator_SetUpObi_CreateObi_m7C3477A5C3F0BB4E8FB587E629AC7F3D5E072425,
	U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12__ctor_mB9723999E31B7F5F4B26C52846EE0EF7F89C03D8,
	U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mEB86C2EEC5E55A40B077EAE0B7CD5291FBBE4FDB,
	U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m124A441CC082D84EB1C7E8C4D09DD36E4BAA1DC4,
	U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mF88ACCD9C8DB2BE87221C306721BE2C8E795FD47,
	U3CAttachObiU3Ed__12_tFD9AD6CC56589A6673EE6F83597A7A9C1E162258_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mFDF31BB57526DB3E034C630C43113FEBAE74DA2C,
	U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16__ctor_m484B3307000BF1D629371B445312F411B8975D51,
	U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m884A0B4D81853BF2D53AA1F505BE7B082C5B492E,
	U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BFCD23E4DD867743952EEC697B33C6095E83FCB,
	U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mD9BB880CE88988B91F124368A497E88A3A05AE21,
	U3CCreateObiU3Ed__16_t9F9D16629816980379AE3E899982844FC11827A3_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m6C0A0740DA6B6C547A0784E83EE609B703F35B15,
	UIController_t220385D83E5A815E75664CFC2EEB30BE1ACB59B1_CustomAttributesCacheGenerator_UIController_StartBodyTracking_mB5FBB5B789AC591BEE67379C313B08C96B65AA0F,
	U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11__ctor_m7D47E8B6DA6315A081D9E41F1EACCA21631D6B63,
	U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_IDisposable_Dispose_m0E93CE83F9A57AE50F030818B6979783C7514008,
	U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94FE82124B37C2D7ECD1A80707193342445FF4E9,
	U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_Reset_mF038F71D5EF0A73A63C671C81B716DE08772EC01,
	U3CStartBodyTrackingU3Ed__11_t98FDFB7DC805BC82A8B44668DF575569A484AC6A_CustomAttributesCacheGenerator_U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_get_Current_mCAF09EA58826F4E0C4B7679D5070E4FE354EDF97,
	WpiObiNoBlueprint_tEAA8E513911F217CA97CFB712A64962F7B12EE52_CustomAttributesCacheGenerator_WpiObiNoBlueprint_CreateObi_m81599B62AE6C534CFBE93954E065DB7915AB8D32,
	U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4__ctor_m6B7FE0270BDD042C4CF7FEE7E23DA518AFCD26BD,
	U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_IDisposable_Dispose_mF66738AD01D56578BBD2552D07182E3D03750B72,
	U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5AC2783D38C1D425E7FC54C3BA6D510C3F88713,
	U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_Collections_IEnumerator_Reset_mD360873099FC44F0F1ED6DEFC12B6BB0A78DD40A,
	U3CCreateObiU3Ed__4_t40F9A574E51F8C64A9B7039811F283B2ECC6A1B0_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__4_System_Collections_IEnumerator_get_Current_mBD9F9895E725E1403A8F438CA60098BC62E5B8D7,
	setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_add_OnObiAdded_mF7DD3C8BDBB033D6743D82515CFB9EC100D5315F,
	setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_remove_OnObiAdded_mA052507D0A9DCAAE4F0CFAC9737E7BD2E5B3EA2A,
	setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_AttachObi_mB362DE10565BBEF3BC97834455F2389F77372CBC,
	setupsingle_tF215D76BF3822E3CDA09CC66F727E097564D2401_CustomAttributesCacheGenerator_setupsingle_CreateObi_m3ED0DF41000191CB095295ED8735804C08B040FF,
	U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12__ctor_m54F90B5E1243762481D29EC8164CB32CA8BFC633,
	U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mFF2C2A49D0534860E17ADBCABCD20753A4FB0C90,
	U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0DF70F72243D4FF5714E4EFDC1FAA0ABE2CE5E6,
	U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mDCFE47940ECB0E6F4067A64FF563DCC8FF30CD52,
	U3CAttachObiU3Ed__12_tADB3409796FF6980DE26868360C6F9DA3B1AC9CE_CustomAttributesCacheGenerator_U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_m6E8969EF8A777A33E45811966F96C714B26D25D4,
	U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16__ctor_m122099FC8C75500A0683EC9FC6C4C74DB6309A07,
	U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_IDisposable_Dispose_mC60A3B6541E7D65C9CEDC54A79114FAC9CED9FAC,
	U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA77337728FC469EBCAE52E998D106954DF94E715,
	U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDFE053E3602BE312CEF0388C87F276ED43D09CC3,
	U3CCreateObiU3Ed__16_t3043A4D2498DFE174BA9ACF605E123F23509F8B8_CustomAttributesCacheGenerator_U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m793AE57000E8558A92A7B3C3869D377791749309,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
