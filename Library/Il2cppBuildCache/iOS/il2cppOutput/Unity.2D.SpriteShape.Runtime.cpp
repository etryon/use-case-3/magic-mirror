﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>




IL2CPP_EXTERN_C RuntimeClass* ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBE47432A2C34069359476B411B4F22F7F0900B4E 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.U2D.UTess.ModuleHandle
struct ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA 
{
public:
	union
	{
		struct
		{
		};
		uint8_t ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA__padding[1];
	};

public:
};

struct ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields
{
public:
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxArea
	int32_t ___kMaxArea_0;
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxEdgeCount
	int32_t ___kMaxEdgeCount_1;
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxIndexCount
	int32_t ___kMaxIndexCount_2;
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxVertexCount
	int32_t ___kMaxVertexCount_3;
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxTriangleCount
	int32_t ___kMaxTriangleCount_4;
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxRefineIterations
	int32_t ___kMaxRefineIterations_5;
	// System.Int32 UnityEngine.U2D.UTess.ModuleHandle::kMaxSmoothenIterations
	int32_t ___kMaxSmoothenIterations_6;
	// System.Single UnityEngine.U2D.UTess.ModuleHandle::kIncrementAreaFactor
	float ___kIncrementAreaFactor_7;

public:
	inline static int32_t get_offset_of_kMaxArea_0() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxArea_0)); }
	inline int32_t get_kMaxArea_0() const { return ___kMaxArea_0; }
	inline int32_t* get_address_of_kMaxArea_0() { return &___kMaxArea_0; }
	inline void set_kMaxArea_0(int32_t value)
	{
		___kMaxArea_0 = value;
	}

	inline static int32_t get_offset_of_kMaxEdgeCount_1() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxEdgeCount_1)); }
	inline int32_t get_kMaxEdgeCount_1() const { return ___kMaxEdgeCount_1; }
	inline int32_t* get_address_of_kMaxEdgeCount_1() { return &___kMaxEdgeCount_1; }
	inline void set_kMaxEdgeCount_1(int32_t value)
	{
		___kMaxEdgeCount_1 = value;
	}

	inline static int32_t get_offset_of_kMaxIndexCount_2() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxIndexCount_2)); }
	inline int32_t get_kMaxIndexCount_2() const { return ___kMaxIndexCount_2; }
	inline int32_t* get_address_of_kMaxIndexCount_2() { return &___kMaxIndexCount_2; }
	inline void set_kMaxIndexCount_2(int32_t value)
	{
		___kMaxIndexCount_2 = value;
	}

	inline static int32_t get_offset_of_kMaxVertexCount_3() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxVertexCount_3)); }
	inline int32_t get_kMaxVertexCount_3() const { return ___kMaxVertexCount_3; }
	inline int32_t* get_address_of_kMaxVertexCount_3() { return &___kMaxVertexCount_3; }
	inline void set_kMaxVertexCount_3(int32_t value)
	{
		___kMaxVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_kMaxTriangleCount_4() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxTriangleCount_4)); }
	inline int32_t get_kMaxTriangleCount_4() const { return ___kMaxTriangleCount_4; }
	inline int32_t* get_address_of_kMaxTriangleCount_4() { return &___kMaxTriangleCount_4; }
	inline void set_kMaxTriangleCount_4(int32_t value)
	{
		___kMaxTriangleCount_4 = value;
	}

	inline static int32_t get_offset_of_kMaxRefineIterations_5() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxRefineIterations_5)); }
	inline int32_t get_kMaxRefineIterations_5() const { return ___kMaxRefineIterations_5; }
	inline int32_t* get_address_of_kMaxRefineIterations_5() { return &___kMaxRefineIterations_5; }
	inline void set_kMaxRefineIterations_5(int32_t value)
	{
		___kMaxRefineIterations_5 = value;
	}

	inline static int32_t get_offset_of_kMaxSmoothenIterations_6() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kMaxSmoothenIterations_6)); }
	inline int32_t get_kMaxSmoothenIterations_6() const { return ___kMaxSmoothenIterations_6; }
	inline int32_t* get_address_of_kMaxSmoothenIterations_6() { return &___kMaxSmoothenIterations_6; }
	inline void set_kMaxSmoothenIterations_6(int32_t value)
	{
		___kMaxSmoothenIterations_6 = value;
	}

	inline static int32_t get_offset_of_kIncrementAreaFactor_7() { return static_cast<int32_t>(offsetof(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields, ___kIncrementAreaFactor_7)); }
	inline float get_kIncrementAreaFactor_7() const { return ___kIncrementAreaFactor_7; }
	inline float* get_address_of_kIncrementAreaFactor_7() { return &___kIncrementAreaFactor_7; }
	inline void set_kIncrementAreaFactor_7(float value)
	{
		___kIncrementAreaFactor_7 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.U2D.UTess.ModuleHandle::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModuleHandle__cctor_m5B5F98EDA48782C6642760F26B21E52AC6A819DA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal static readonly  int kMaxArea = 65536;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxArea_0(((int32_t)65536));
		// internal static readonly  int kMaxEdgeCount = 65536;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxEdgeCount_1(((int32_t)65536));
		// internal static readonly  int kMaxIndexCount = 65536;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxIndexCount_2(((int32_t)65536));
		// internal static readonly  int kMaxVertexCount = 65536;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxVertexCount_3(((int32_t)65536));
		// internal static readonly  int kMaxTriangleCount = kMaxIndexCount / 3;
		int32_t L_0 = ((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->get_kMaxIndexCount_2();
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxTriangleCount_4(((int32_t)((int32_t)L_0/(int32_t)3)));
		// internal static readonly  int kMaxRefineIterations = 48;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxRefineIterations_5(((int32_t)48));
		// internal static readonly  int kMaxSmoothenIterations = 256;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kMaxSmoothenIterations_6(((int32_t)256));
		// internal static readonly  float kIncrementAreaFactor = 1.2f;
		((ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_StaticFields*)il2cpp_codegen_static_fields_for(ModuleHandle_t3918279B8957CB453D5F7E2D7FAEFDC7D51E2AAA_il2cpp_TypeInfo_var))->set_kIncrementAreaFactor_7((1.20000005f));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
