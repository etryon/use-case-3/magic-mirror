﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float2)
extern void math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9 (void);
// 0x00000002 Unity.Mathematics.float3 Unity.Mathematics.math::float3(System.Single,System.Single,System.Single)
extern void math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB (void);
// 0x00000003 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3)
extern void math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84 (void);
// 0x00000004 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float3x3)
extern void math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855 (void);
// 0x00000005 Unity.Mathematics.float4 Unity.Mathematics.math::float4(System.Single,System.Single,System.Single,System.Single)
extern void math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6 (void);
// 0x00000006 Unity.Mathematics.float4 Unity.Mathematics.math::float4(Unity.Mathematics.float3,System.Single)
extern void math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F (void);
// 0x00000007 Unity.Mathematics.float4 Unity.Mathematics.math::float4(System.Single)
extern void math_float4_m6D1066B47EE7BC7A47A136D969B5A3CA41902224 (void);
// 0x00000008 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4)
extern void math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5 (void);
// 0x00000009 Unity.Mathematics.float4 Unity.Mathematics.math::shuffle(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent,Unity.Mathematics.math/ShuffleComponent)
extern void math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6 (void);
// 0x0000000A System.Single Unity.Mathematics.math::select_shuffle_component(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.math/ShuffleComponent)
extern void math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC (void);
// 0x0000000B Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC (void);
// 0x0000000C Unity.Mathematics.float4x4 Unity.Mathematics.math::float4x4(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF (void);
// 0x0000000D Unity.Mathematics.float4x4 Unity.Mathematics.math::transpose(Unity.Mathematics.float4x4)
extern void math_transpose_m76E580466C3B5A7D1E0A1534C8E17BD0176F51A0 (void);
// 0x0000000E Unity.Mathematics.float4x4 Unity.Mathematics.math::inverse(Unity.Mathematics.float4x4)
extern void math_inverse_m26EB769712BD20C07185B9ECAC5808E06DDC7BF3 (void);
// 0x0000000F System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.float4x4)
extern void math_hash_m16C61605FBE531F8EB703823255407075EA5A464 (void);
// 0x00000010 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int2)
extern void math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6 (void);
// 0x00000011 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int3)
extern void math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D (void);
// 0x00000012 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.int4)
extern void math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871 (void);
// 0x00000013 System.Int32 Unity.Mathematics.math::asint(System.Single)
extern void math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86 (void);
// 0x00000014 Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.int2)
extern void math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B (void);
// 0x00000015 Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.int3)
extern void math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B (void);
// 0x00000016 Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.int4)
extern void math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D (void);
// 0x00000017 System.UInt32 Unity.Mathematics.math::asuint(System.Single)
extern void math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC (void);
// 0x00000018 Unity.Mathematics.uint2 Unity.Mathematics.math::asuint(Unity.Mathematics.float2)
extern void math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863 (void);
// 0x00000019 Unity.Mathematics.uint3 Unity.Mathematics.math::asuint(Unity.Mathematics.float3)
extern void math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550 (void);
// 0x0000001A Unity.Mathematics.uint4 Unity.Mathematics.math::asuint(Unity.Mathematics.float4)
extern void math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D (void);
// 0x0000001B System.Single Unity.Mathematics.math::asfloat(System.Int32)
extern void math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181 (void);
// 0x0000001C System.Single Unity.Mathematics.math::asfloat(System.UInt32)
extern void math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF (void);
// 0x0000001D Unity.Mathematics.float3 Unity.Mathematics.math::asfloat(Unity.Mathematics.uint3)
extern void math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826 (void);
// 0x0000001E Unity.Mathematics.float4 Unity.Mathematics.math::asfloat(Unity.Mathematics.uint4)
extern void math_asfloat_mD9B4C5BBB6AC641027D94C2045C4C3AF416DFF6D (void);
// 0x0000001F System.Int32 Unity.Mathematics.math::min(System.Int32,System.Int32)
extern void math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A (void);
// 0x00000020 Unity.Mathematics.int3 Unity.Mathematics.math::min(Unity.Mathematics.int3,Unity.Mathematics.int3)
extern void math_min_m661F28A20439E739FAB5DA0FF11854BEAF7EC951 (void);
// 0x00000021 System.Int64 Unity.Mathematics.math::min(System.Int64,System.Int64)
extern void math_min_m65ACBB9005609DE8184015662C758B23A85C794E (void);
// 0x00000022 System.Single Unity.Mathematics.math::min(System.Single,System.Single)
extern void math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89 (void);
// 0x00000023 Unity.Mathematics.float2 Unity.Mathematics.math::min(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void math_min_mDCA05EE5BF99F3753F8AF682B3C5B85C4620C60C (void);
// 0x00000024 Unity.Mathematics.float3 Unity.Mathematics.math::min(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_min_m6BF29A95DEDC36F4E386F4C02122671FCC271BC8 (void);
// 0x00000025 Unity.Mathematics.float4 Unity.Mathematics.math::min(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_min_mC45C7024F6850EB1D382A2B2F6F31CCA4B2750E6 (void);
// 0x00000026 System.Int32 Unity.Mathematics.math::max(System.Int32,System.Int32)
extern void math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF (void);
// 0x00000027 System.Int64 Unity.Mathematics.math::max(System.Int64,System.Int64)
extern void math_max_mAED5DA82BD5495A15A2365FCE7CA4B775F6E1132 (void);
// 0x00000028 System.Single Unity.Mathematics.math::max(System.Single,System.Single)
extern void math_max_mD8541933650D81292540BAFF46DE531FA1B333FC (void);
// 0x00000029 Unity.Mathematics.float2 Unity.Mathematics.math::max(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void math_max_m3082575385F8F5B755FF5D4399B1CD43A17550EA (void);
// 0x0000002A Unity.Mathematics.float3 Unity.Mathematics.math::max(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_max_m7E3D48AE84E0E23DD089176B4C0CD6886303ABCF (void);
// 0x0000002B Unity.Mathematics.float4 Unity.Mathematics.math::max(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_max_m5A2AD47597A69870F6E5A8A2ABEE9A1EAC771393 (void);
// 0x0000002C Unity.Mathematics.float4 Unity.Mathematics.math::lerp(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91 (void);
// 0x0000002D System.Int32 Unity.Mathematics.math::clamp(System.Int32,System.Int32,System.Int32)
extern void math_clamp_mAC2AAD9592C2C59601A5CF30D601D645033E2569 (void);
// 0x0000002E System.Single Unity.Mathematics.math::clamp(System.Single,System.Single,System.Single)
extern void math_clamp_m3FDC8AAE05842733E193852C929DAAC6F5E8C931 (void);
// 0x0000002F Unity.Mathematics.float4 Unity.Mathematics.math::clamp(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_clamp_m4A3A6B869C607E006DBC5EB2A50C0812F40E09A4 (void);
// 0x00000030 System.Single Unity.Mathematics.math::saturate(System.Single)
extern void math_saturate_mA17845866D2189A354872A777FF9EE7D7AD0C7FC (void);
// 0x00000031 System.Single Unity.Mathematics.math::abs(System.Single)
extern void math_abs_m684426E813FD887C103114B2E15FCBAFFF32D3B0 (void);
// 0x00000032 Unity.Mathematics.float4 Unity.Mathematics.math::abs(Unity.Mathematics.float4)
extern void math_abs_m3842FE0B152ABEA4BDD3985EF8C38B1E13BE6273 (void);
// 0x00000033 System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void math_dot_m6238E071DBA53F376473FFEC28706C9B537E6BAE (void);
// 0x00000034 System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9 (void);
// 0x00000035 System.Single Unity.Mathematics.math::dot(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97 (void);
// 0x00000036 System.Single Unity.Mathematics.math::atan2(System.Single,System.Single)
extern void math_atan2_mFFA7942F9F41993E3F752E630DB45D0B4104034C (void);
// 0x00000037 System.Single Unity.Mathematics.math::cos(System.Single)
extern void math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424 (void);
// 0x00000038 System.Single Unity.Mathematics.math::acos(System.Single)
extern void math_acos_mDF4948B8CF18EF8052C38D7914802E7869B79B6C (void);
// 0x00000039 System.Single Unity.Mathematics.math::sin(System.Single)
extern void math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE (void);
// 0x0000003A System.Single Unity.Mathematics.math::floor(System.Single)
extern void math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B (void);
// 0x0000003B Unity.Mathematics.float2 Unity.Mathematics.math::floor(Unity.Mathematics.float2)
extern void math_floor_mCADCD88A3E295AEB16A2356DACBA00FDD8DFA9B9 (void);
// 0x0000003C Unity.Mathematics.float3 Unity.Mathematics.math::floor(Unity.Mathematics.float3)
extern void math_floor_m1595239B5E0B9CA2944C200E1ACADE9CEED6EA2D (void);
// 0x0000003D Unity.Mathematics.float4 Unity.Mathematics.math::floor(Unity.Mathematics.float4)
extern void math_floor_m6F1A809C667676D8CDF372A24CC318F90943AAC7 (void);
// 0x0000003E System.Single Unity.Mathematics.math::ceil(System.Single)
extern void math_ceil_m09F7522539A79883D220617019957CCF0CB4DA8B (void);
// 0x0000003F System.Single Unity.Mathematics.math::rcp(System.Single)
extern void math_rcp_mBBCDD8F05AFE2E307F898E81A06B64BF0E2625D0 (void);
// 0x00000040 Unity.Mathematics.float3 Unity.Mathematics.math::rcp(Unity.Mathematics.float3)
extern void math_rcp_m60294695218F02BCB25674A2FA40A86499DD6F38 (void);
// 0x00000041 Unity.Mathematics.float4 Unity.Mathematics.math::rcp(Unity.Mathematics.float4)
extern void math_rcp_m3D4A844DEF4B4531007D5E1A17BF493690FEBB8C (void);
// 0x00000042 System.Single Unity.Mathematics.math::sign(System.Single)
extern void math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611 (void);
// 0x00000043 Unity.Mathematics.float4 Unity.Mathematics.math::sign(Unity.Mathematics.float4)
extern void math_sign_m63A39DE29A6BCB0E9BA152B458A0DD2EB9EFB23C (void);
// 0x00000044 System.Single Unity.Mathematics.math::pow(System.Single,System.Single)
extern void math_pow_m792865DB1E3BCC792726FFB800ABDE9FF109D3B4 (void);
// 0x00000045 System.Single Unity.Mathematics.math::exp2(System.Single)
extern void math_exp2_m91B7071A3214B73A372F626DFAA243B3C65BD230 (void);
// 0x00000046 System.Single Unity.Mathematics.math::log(System.Single)
extern void math_log_m32923945396EF294F36A8CA9848ED0771A77F50F (void);
// 0x00000047 System.Single Unity.Mathematics.math::sqrt(System.Single)
extern void math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF (void);
// 0x00000048 System.Single Unity.Mathematics.math::rsqrt(System.Single)
extern void math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896 (void);
// 0x00000049 Unity.Mathematics.float3 Unity.Mathematics.math::normalize(Unity.Mathematics.float3)
extern void math_normalize_m06189F2D06A4C6DEB81C6623B802A878E3356975 (void);
// 0x0000004A Unity.Mathematics.float3 Unity.Mathematics.math::normalizesafe(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_normalizesafe_mD616A562864DA7E3FF165AB6E2CE7BE30131E668 (void);
// 0x0000004B Unity.Mathematics.float4 Unity.Mathematics.math::normalizesafe(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_normalizesafe_m0043348AB7956C50EFFD5572CA0D516DCB54A376 (void);
// 0x0000004C System.Single Unity.Mathematics.math::length(Unity.Mathematics.float2)
extern void math_length_m25A33854A8D7F4A4B96C82171BE616376ECA25CE (void);
// 0x0000004D System.Single Unity.Mathematics.math::length(Unity.Mathematics.float3)
extern void math_length_mECD912F8B5F13E8FDFEFC19DDC928AC69C9669D4 (void);
// 0x0000004E System.Single Unity.Mathematics.math::length(Unity.Mathematics.float4)
extern void math_length_mD7967FEA18B97C7AC6CFE6B0BE3D45D35D355170 (void);
// 0x0000004F System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float3)
extern void math_lengthsq_mCA5F01F89CC6AAAE7F0263A8F3C34FC2C7A98561 (void);
// 0x00000050 System.Single Unity.Mathematics.math::lengthsq(Unity.Mathematics.float4)
extern void math_lengthsq_mD422A214358E935793F5ED10991D70F040848F2D (void);
// 0x00000051 Unity.Mathematics.float3 Unity.Mathematics.math::cross(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019 (void);
// 0x00000052 Unity.Mathematics.float3 Unity.Mathematics.math::select(Unity.Mathematics.float3,Unity.Mathematics.float3,System.Boolean)
extern void math_select_mD6522E9D958653C41F4B2E3463B79F7A011E60AB (void);
// 0x00000053 Unity.Mathematics.float4 Unity.Mathematics.math::select(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Boolean)
extern void math_select_m3FF6FBC5429C94D76C025979A6D3DCA2B2B62602 (void);
// 0x00000054 System.Void Unity.Mathematics.math::sincos(System.Single,System.Single&,System.Single&)
extern void math_sincos_m62FF0B31557448DAF3C3ED1EBCA171BD524BD96B (void);
// 0x00000055 System.Int32 Unity.Mathematics.math::lzcnt(System.Int32)
extern void math_lzcnt_mBB573111B33D6C11E506BA3986481FA994D201FF (void);
// 0x00000056 System.Int32 Unity.Mathematics.math::lzcnt(System.UInt32)
extern void math_lzcnt_m69123CF8445E0B67FA30F24BA42F663C105BE7EB (void);
// 0x00000057 System.Int32 Unity.Mathematics.math::tzcnt(System.Int32)
extern void math_tzcnt_mAD942678F41AC6A34E7E83752D38E40F1043FE4E (void);
// 0x00000058 System.Int32 Unity.Mathematics.math::tzcnt(System.UInt32)
extern void math_tzcnt_m3A483A42E55BE961B589C59F99EE7972D0D296E8 (void);
// 0x00000059 System.Int32 Unity.Mathematics.math::ceilpow2(System.Int32)
extern void math_ceilpow2_mA7B90555C48581B479A78BA3EC7EE1DC7E2F657E (void);
// 0x0000005A System.Int64 Unity.Mathematics.math::ceilpow2(System.Int64)
extern void math_ceilpow2_mC273C4BD00A26075F4370BA1B290D42C42C97227 (void);
// 0x0000005B System.Single Unity.Mathematics.math::cmin(Unity.Mathematics.float2)
extern void math_cmin_mDDC6705B504F566B700A58BBC5DBE63EE40EAE53 (void);
// 0x0000005C System.Single Unity.Mathematics.math::cmin(Unity.Mathematics.float3)
extern void math_cmin_mD6518D31DF8A9C48087287DF5B5A82A3DF280E0A (void);
// 0x0000005D System.Single Unity.Mathematics.math::cmax(Unity.Mathematics.float2)
extern void math_cmax_m5B2F559332B7FD6F7B77F932F39C18D277A17888 (void);
// 0x0000005E System.Single Unity.Mathematics.math::cmax(Unity.Mathematics.float3)
extern void math_cmax_m468A5263FCD72A1EC99854B9477FD2FAF4ABBFFF (void);
// 0x0000005F System.Single Unity.Mathematics.math::cmax(Unity.Mathematics.float4)
extern void math_cmax_mA222CC6FC5245F461911FC523F1DCC9B14D36508 (void);
// 0x00000060 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint2)
extern void math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1 (void);
// 0x00000061 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint3)
extern void math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751 (void);
// 0x00000062 System.UInt32 Unity.Mathematics.math::csum(Unity.Mathematics.uint4)
extern void math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5 (void);
// 0x00000063 Unity.Mathematics.float4 Unity.Mathematics.math::movelh(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C (void);
// 0x00000064 Unity.Mathematics.float4 Unity.Mathematics.math::movehl(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553 (void);
// 0x00000065 Unity.Mathematics.float3x3 Unity.Mathematics.math::float3x3(Unity.Mathematics.quaternion)
extern void math_float3x3_m083898B3E74368DC86FB3D19406C3ED60AF63583 (void);
// 0x00000066 Unity.Mathematics.float4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4)
extern void math_mul_mDBF2C1741940E16BDEB49774E0CF13AB4889A255 (void);
// 0x00000067 Unity.Mathematics.float4x4 Unity.Mathematics.math::mul(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
extern void math_mul_m5C88F4BCCEAC68636D82BCA37DFD19731E9C8B3D (void);
// 0x00000068 Unity.Mathematics.quaternion Unity.Mathematics.math::quaternion(Unity.Mathematics.float4)
extern void math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD (void);
// 0x00000069 Unity.Mathematics.quaternion Unity.Mathematics.math::conjugate(Unity.Mathematics.quaternion)
extern void math_conjugate_mF4ABF8CC798742764982DEEC731088BF719B7935 (void);
// 0x0000006A Unity.Mathematics.quaternion Unity.Mathematics.math::inverse(Unity.Mathematics.quaternion)
extern void math_inverse_m8C4E2F1AA7871C40EA7FFAB90D5F2653466E894D (void);
// 0x0000006B System.Single Unity.Mathematics.math::dot(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion)
extern void math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634 (void);
// 0x0000006C Unity.Mathematics.quaternion Unity.Mathematics.math::normalize(Unity.Mathematics.quaternion)
extern void math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D (void);
// 0x0000006D Unity.Mathematics.quaternion Unity.Mathematics.math::mul(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion)
extern void math_mul_m545D5B9EB2838DCDD021F61D385D8DE119422FC3 (void);
// 0x0000006E Unity.Mathematics.float3 Unity.Mathematics.math::mul(Unity.Mathematics.quaternion,Unity.Mathematics.float3)
extern void math_mul_m003A157203A82B7842A6894B3127F84D61BE3B9B (void);
// 0x0000006F Unity.Mathematics.float3 Unity.Mathematics.math::rotate(Unity.Mathematics.quaternion,Unity.Mathematics.float3)
extern void math_rotate_mD7EBD96F0C1DAD111B6A3CD7942C006539777D47 (void);
// 0x00000070 Unity.Mathematics.quaternion Unity.Mathematics.math::nlerp(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,System.Single)
extern void math_nlerp_mB7BD6C0F6F0AD1411F206E3BFDCC4161F1DF5D54 (void);
// 0x00000071 Unity.Mathematics.quaternion Unity.Mathematics.math::slerp(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,System.Single)
extern void math_slerp_m4819ACD4A8A9FA276AE84D9EAAA2ACAD2ED54A28 (void);
// 0x00000072 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.quaternion)
extern void math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C (void);
// 0x00000073 Unity.Mathematics.uint2 Unity.Mathematics.math::uint2(System.UInt32,System.UInt32)
extern void math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2 (void);
// 0x00000074 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint2)
extern void math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90 (void);
// 0x00000075 Unity.Mathematics.uint3 Unity.Mathematics.math::uint3(System.UInt32,System.UInt32,System.UInt32)
extern void math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729 (void);
// 0x00000076 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint3)
extern void math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6 (void);
// 0x00000077 Unity.Mathematics.uint4 Unity.Mathematics.math::uint4(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777 (void);
// 0x00000078 System.UInt32 Unity.Mathematics.math::hash(Unity.Mathematics.uint4)
extern void math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811 (void);
// 0x00000079 System.Void Unity.Mathematics.float2::.ctor(System.Single,System.Single)
extern void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449 (void);
// 0x0000007A System.Void Unity.Mathematics.float2::.ctor(System.Int32)
extern void float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C (void);
// 0x0000007B Unity.Mathematics.float2 Unity.Mathematics.float2::op_Implicit(System.Int32)
extern void float2_op_Implicit_mB01382E876FA2AFC63701F493121DEA95D2677E1 (void);
// 0x0000007C Unity.Mathematics.float2 Unity.Mathematics.float2::op_Multiply(Unity.Mathematics.float2,System.Single)
extern void float2_op_Multiply_m7262BDAFAD6CB1D6D980929C82826C82C86F5039 (void);
// 0x0000007D Unity.Mathematics.float2 Unity.Mathematics.float2::op_Addition(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void float2_op_Addition_m1FCEA54B4EB9E2E5BBF9E2C5A6466589B486CF81 (void);
// 0x0000007E Unity.Mathematics.float2 Unity.Mathematics.float2::op_Subtraction(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void float2_op_Subtraction_mD7E1752FB29F54A98A4A52140E8BCFD659471E19 (void);
// 0x0000007F Unity.Mathematics.float2 Unity.Mathematics.float2::op_Division(Unity.Mathematics.float2,System.Single)
extern void float2_op_Division_m122E66E69678386034C53B7D9B89A551628473FD (void);
// 0x00000080 Unity.Mathematics.float2 Unity.Mathematics.float2::get_xy()
extern void float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB (void);
// 0x00000081 System.Single Unity.Mathematics.float2::get_Item(System.Int32)
extern void float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665 (void);
// 0x00000082 System.Void Unity.Mathematics.float2::set_Item(System.Int32,System.Single)
extern void float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092 (void);
// 0x00000083 System.Boolean Unity.Mathematics.float2::Equals(Unity.Mathematics.float2)
extern void float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB (void);
// 0x00000084 System.Boolean Unity.Mathematics.float2::Equals(System.Object)
extern void float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D (void);
// 0x00000085 System.Int32 Unity.Mathematics.float2::GetHashCode()
extern void float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9 (void);
// 0x00000086 System.String Unity.Mathematics.float2::ToString()
extern void float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28 (void);
// 0x00000087 System.String Unity.Mathematics.float2::ToString(System.String,System.IFormatProvider)
extern void float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78 (void);
// 0x00000088 System.Void Unity.Mathematics.float3::.ctor(System.Single,System.Single,System.Single)
extern void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A (void);
// 0x00000089 System.Void Unity.Mathematics.float3::.ctor(System.Single)
extern void float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C (void);
// 0x0000008A Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1 (void);
// 0x0000008B Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(Unity.Mathematics.float3,System.Single)
extern void float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13 (void);
// 0x0000008C Unity.Mathematics.float3 Unity.Mathematics.float3::op_Multiply(System.Single,Unity.Mathematics.float3)
extern void float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE (void);
// 0x0000008D Unity.Mathematics.float3 Unity.Mathematics.float3::op_Addition(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978 (void);
// 0x0000008E Unity.Mathematics.float3 Unity.Mathematics.float3::op_Subtraction(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2 (void);
// 0x0000008F Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3_op_Division_m84488830E369A20FBB0BE95A05FD94DE1F4DDBB2 (void);
// 0x00000090 Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(Unity.Mathematics.float3,System.Single)
extern void float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A (void);
// 0x00000091 Unity.Mathematics.float3 Unity.Mathematics.float3::op_Division(System.Single,Unity.Mathematics.float3)
extern void float3_op_Division_mE2739F8C6E0ADC06D8DCD923F9828B75EACB3ECF (void);
// 0x00000092 Unity.Mathematics.float3 Unity.Mathematics.float3::get_xyz()
extern void float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F (void);
// 0x00000093 Unity.Mathematics.float3 Unity.Mathematics.float3::get_yzx()
extern void float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024 (void);
// 0x00000094 Unity.Mathematics.float2 Unity.Mathematics.float3::get_xy()
extern void float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43 (void);
// 0x00000095 Unity.Mathematics.float2 Unity.Mathematics.float3::get_yz()
extern void float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8 (void);
// 0x00000096 System.Single Unity.Mathematics.float3::get_Item(System.Int32)
extern void float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF (void);
// 0x00000097 System.Void Unity.Mathematics.float3::set_Item(System.Int32,System.Single)
extern void float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A (void);
// 0x00000098 System.Boolean Unity.Mathematics.float3::Equals(Unity.Mathematics.float3)
extern void float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF (void);
// 0x00000099 System.Boolean Unity.Mathematics.float3::Equals(System.Object)
extern void float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B (void);
// 0x0000009A System.Int32 Unity.Mathematics.float3::GetHashCode()
extern void float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3 (void);
// 0x0000009B System.String Unity.Mathematics.float3::ToString()
extern void float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF (void);
// 0x0000009C System.String Unity.Mathematics.float3::ToString(System.String,System.IFormatProvider)
extern void float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F (void);
// 0x0000009D UnityEngine.Vector3 Unity.Mathematics.float3::op_Implicit(Unity.Mathematics.float3)
extern void float3_op_Implicit_m10DFE908DCDD18D73FA11E4C9CDB23E16FA3ABE8 (void);
// 0x0000009E Unity.Mathematics.float3 Unity.Mathematics.float3::op_Implicit(UnityEngine.Vector3)
extern void float3_op_Implicit_mC2BC870EF4246C5C3A2FC27EE9ABEDAFF49DC1EF (void);
// 0x0000009F System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.float3,Unity.Mathematics.float3,Unity.Mathematics.float3)
extern void float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E (void);
// 0x000000A0 System.Void Unity.Mathematics.float3x3::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026 (void);
// 0x000000A1 Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Multiply(Unity.Mathematics.float3x3,System.Single)
extern void float3x3_op_Multiply_m84830685A93039297DC893DA1B6A28D675822961 (void);
// 0x000000A2 Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Addition(Unity.Mathematics.float3x3,Unity.Mathematics.float3x3)
extern void float3x3_op_Addition_mD075893A8D24978DC605DA7A415F7CE4BEE8F0F3 (void);
// 0x000000A3 Unity.Mathematics.float3x3 Unity.Mathematics.float3x3::op_Division(Unity.Mathematics.float3x3,System.Single)
extern void float3x3_op_Division_m92B0F2EBB2E77BF84C675C513DC3F84445F2CB69 (void);
// 0x000000A4 System.Boolean Unity.Mathematics.float3x3::Equals(Unity.Mathematics.float3x3)
extern void float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9 (void);
// 0x000000A5 System.Boolean Unity.Mathematics.float3x3::Equals(System.Object)
extern void float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2 (void);
// 0x000000A6 System.Int32 Unity.Mathematics.float3x3::GetHashCode()
extern void float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC (void);
// 0x000000A7 System.String Unity.Mathematics.float3x3::ToString()
extern void float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA (void);
// 0x000000A8 System.String Unity.Mathematics.float3x3::ToString(System.String,System.IFormatProvider)
extern void float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24 (void);
// 0x000000A9 System.Void Unity.Mathematics.float3x3::.ctor(Unity.Mathematics.quaternion)
extern void float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4 (void);
// 0x000000AA System.Void Unity.Mathematics.float3x3::.cctor()
extern void float3x3__cctor_mF2DDB6D13785F3BF4A1D002DD387896890A8ACC4 (void);
// 0x000000AB System.Void Unity.Mathematics.float4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241 (void);
// 0x000000AC System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float2,System.Single,System.Single)
extern void float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7 (void);
// 0x000000AD System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float2,Unity.Mathematics.float2)
extern void float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B (void);
// 0x000000AE System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float3,System.Single)
extern void float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65 (void);
// 0x000000AF System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.float4)
extern void float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758 (void);
// 0x000000B0 System.Void Unity.Mathematics.float4::.ctor(System.Single)
extern void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E (void);
// 0x000000B1 System.Void Unity.Mathematics.float4::.ctor(Unity.Mathematics.int4)
extern void float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54 (void);
// 0x000000B2 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(System.Single)
extern void float4_op_Implicit_m18F0BAF7440BF58605AA7A75CB911431ED4347DF (void);
// 0x000000B3 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(Unity.Mathematics.int4)
extern void float4_op_Implicit_m4C5BAD1D894F2CFBC62675DBE2813CC48BB57F4C (void);
// 0x000000B4 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8 (void);
// 0x000000B5 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(Unity.Mathematics.float4,System.Single)
extern void float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7 (void);
// 0x000000B6 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Multiply(System.Single,Unity.Mathematics.float4)
extern void float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70 (void);
// 0x000000B7 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0 (void);
// 0x000000B8 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Addition(Unity.Mathematics.float4,System.Single)
extern void float4_op_Addition_m50BF16260478F8BEBB56698FC677066CB2792697 (void);
// 0x000000B9 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370 (void);
// 0x000000BA Unity.Mathematics.float4 Unity.Mathematics.float4::op_Subtraction(Unity.Mathematics.float4,System.Single)
extern void float4_op_Subtraction_mC5AB272630A81EDCFC642F4953FA24F84F03FBFB (void);
// 0x000000BB Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4_op_Division_m489385B46636C7B5CCE01DEECA0A603A2EE807AA (void);
// 0x000000BC Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(Unity.Mathematics.float4,System.Single)
extern void float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8 (void);
// 0x000000BD Unity.Mathematics.float4 Unity.Mathematics.float4::op_Division(System.Single,Unity.Mathematics.float4)
extern void float4_op_Division_m847947B86BE29A43C35E74AB168A525866AA4B15 (void);
// 0x000000BE Unity.Mathematics.float4 Unity.Mathematics.float4::op_UnaryNegation(Unity.Mathematics.float4)
extern void float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA (void);
// 0x000000BF Unity.Mathematics.float4 Unity.Mathematics.float4::get_xyzx()
extern void float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE (void);
// 0x000000C0 Unity.Mathematics.float4 Unity.Mathematics.float4::get_yzxy()
extern void float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB (void);
// 0x000000C1 Unity.Mathematics.float4 Unity.Mathematics.float4::get_yzxz()
extern void float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3 (void);
// 0x000000C2 Unity.Mathematics.float4 Unity.Mathematics.float4::get_zxyy()
extern void float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9 (void);
// 0x000000C3 Unity.Mathematics.float4 Unity.Mathematics.float4::get_zxyz()
extern void float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B (void);
// 0x000000C4 Unity.Mathematics.float4 Unity.Mathematics.float4::get_wwwx()
extern void float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E (void);
// 0x000000C5 Unity.Mathematics.float4 Unity.Mathematics.float4::get_wwww()
extern void float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC (void);
// 0x000000C6 Unity.Mathematics.float3 Unity.Mathematics.float4::get_xyz()
extern void float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC (void);
// 0x000000C7 System.Void Unity.Mathematics.float4::set_xyz(Unity.Mathematics.float3)
extern void float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91 (void);
// 0x000000C8 Unity.Mathematics.float3 Unity.Mathematics.float4::get_yxw()
extern void float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF (void);
// 0x000000C9 Unity.Mathematics.float3 Unity.Mathematics.float4::get_zwx()
extern void float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87 (void);
// 0x000000CA Unity.Mathematics.float3 Unity.Mathematics.float4::get_wzy()
extern void float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6 (void);
// 0x000000CB Unity.Mathematics.float2 Unity.Mathematics.float4::get_xy()
extern void float4_get_xy_mC5A971D69E3278438139480A25313449133571E0 (void);
// 0x000000CC Unity.Mathematics.float2 Unity.Mathematics.float4::get_zw()
extern void float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5 (void);
// 0x000000CD System.Single Unity.Mathematics.float4::get_Item(System.Int32)
extern void float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F (void);
// 0x000000CE System.Void Unity.Mathematics.float4::set_Item(System.Int32,System.Single)
extern void float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D (void);
// 0x000000CF System.Boolean Unity.Mathematics.float4::Equals(Unity.Mathematics.float4)
extern void float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508 (void);
// 0x000000D0 System.Boolean Unity.Mathematics.float4::Equals(System.Object)
extern void float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B (void);
// 0x000000D1 System.Int32 Unity.Mathematics.float4::GetHashCode()
extern void float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230 (void);
// 0x000000D2 System.String Unity.Mathematics.float4::ToString()
extern void float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850 (void);
// 0x000000D3 System.String Unity.Mathematics.float4::ToString(System.String,System.IFormatProvider)
extern void float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B (void);
// 0x000000D4 Unity.Mathematics.float4 Unity.Mathematics.float4::op_Implicit(UnityEngine.Vector4)
extern void float4_op_Implicit_mADB6904960D508B7B30752A99C7013AED76065ED (void);
// 0x000000D5 UnityEngine.Vector4 Unity.Mathematics.float4::op_Implicit(Unity.Mathematics.float4)
extern void float4_op_Implicit_mD1C84DA295349F9E668ACB153BA03F21D2B976CE (void);
// 0x000000D6 System.Void Unity.Mathematics.float4x4::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929 (void);
// 0x000000D7 System.Void Unity.Mathematics.float4x4::.ctor(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988 (void);
// 0x000000D8 Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Multiply(Unity.Mathematics.float4x4,System.Single)
extern void float4x4_op_Multiply_mEEE7C88A7EF269370E12BA82BDC101D83893FF96 (void);
// 0x000000D9 Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Multiply(System.Single,Unity.Mathematics.float4x4)
extern void float4x4_op_Multiply_mC1B6C82737F3D82CF4A5783D7B504E76CB1CF553 (void);
// 0x000000DA Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Addition(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
extern void float4x4_op_Addition_m8ADC44933E779260371FB510153637822E9BDE84 (void);
// 0x000000DB Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Subtraction(Unity.Mathematics.float4x4,Unity.Mathematics.float4x4)
extern void float4x4_op_Subtraction_mFB25888DC0C2F88D032EB8B09D219CDA84BB1D71 (void);
// 0x000000DC Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_Division(Unity.Mathematics.float4x4,System.Single)
extern void float4x4_op_Division_mD54BB05F0E1C9D03F8D3F9B58EAD537168D92E37 (void);
// 0x000000DD Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::op_UnaryNegation(Unity.Mathematics.float4x4)
extern void float4x4_op_UnaryNegation_mD32379B42356BEC1149EA39D841F96A12E8B8786 (void);
// 0x000000DE Unity.Mathematics.float4& Unity.Mathematics.float4x4::get_Item(System.Int32)
extern void float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A (void);
// 0x000000DF System.Boolean Unity.Mathematics.float4x4::Equals(Unity.Mathematics.float4x4)
extern void float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832 (void);
// 0x000000E0 System.Boolean Unity.Mathematics.float4x4::Equals(System.Object)
extern void float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A (void);
// 0x000000E1 System.Int32 Unity.Mathematics.float4x4::GetHashCode()
extern void float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD (void);
// 0x000000E2 System.String Unity.Mathematics.float4x4::ToString()
extern void float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2 (void);
// 0x000000E3 System.String Unity.Mathematics.float4x4::ToString(System.String,System.IFormatProvider)
extern void float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2 (void);
// 0x000000E4 Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::Scale(System.Single,System.Single,System.Single)
extern void float4x4_Scale_mEE04A811A295C99F280056A51ABC83356CB0A4A3 (void);
// 0x000000E5 Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::Scale(Unity.Mathematics.float3)
extern void float4x4_Scale_mD6E98ACC10BB3F6550EC578201C07A89B72CDC74 (void);
// 0x000000E6 Unity.Mathematics.float4x4 Unity.Mathematics.float4x4::TRS(Unity.Mathematics.float3,Unity.Mathematics.quaternion,Unity.Mathematics.float3)
extern void float4x4_TRS_m2A09A0CF1B7D774D0674577FEA3FBBAD5B25AC92 (void);
// 0x000000E7 System.Void Unity.Mathematics.float4x4::.cctor()
extern void float4x4__cctor_mBA99BE0167C79FA3EFFB97DBBD42249AB9894684 (void);
// 0x000000E8 System.Void Unity.Mathematics.int2::.ctor(System.Int32,System.Int32)
extern void int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1 (void);
// 0x000000E9 System.Void Unity.Mathematics.int2::.ctor(Unity.Mathematics.float2)
extern void int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974 (void);
// 0x000000EA System.Int32 Unity.Mathematics.int2::get_Item(System.Int32)
extern void int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E (void);
// 0x000000EB System.Boolean Unity.Mathematics.int2::Equals(Unity.Mathematics.int2)
extern void int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7 (void);
// 0x000000EC System.Boolean Unity.Mathematics.int2::Equals(System.Object)
extern void int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66 (void);
// 0x000000ED System.Int32 Unity.Mathematics.int2::GetHashCode()
extern void int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A (void);
// 0x000000EE System.String Unity.Mathematics.int2::ToString()
extern void int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9 (void);
// 0x000000EF System.String Unity.Mathematics.int2::ToString(System.String,System.IFormatProvider)
extern void int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691 (void);
// 0x000000F0 System.Void Unity.Mathematics.int3::.ctor(System.Int32,System.Int32,System.Int32)
extern void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32 (void);
// 0x000000F1 System.Void Unity.Mathematics.int3::.ctor(System.Int32)
extern void int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF (void);
// 0x000000F2 System.Void Unity.Mathematics.int3::.ctor(Unity.Mathematics.float3)
extern void int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618 (void);
// 0x000000F3 Unity.Mathematics.int3 Unity.Mathematics.int3::op_Addition(Unity.Mathematics.int3,Unity.Mathematics.int3)
extern void int3_op_Addition_m08C6B9DB7119923A27914BAFD7DCDEE50CC988A4 (void);
// 0x000000F4 Unity.Mathematics.int3 Unity.Mathematics.int3::op_Subtraction(Unity.Mathematics.int3,Unity.Mathematics.int3)
extern void int3_op_Subtraction_m0AE9F07D1B4603D7CBD3AF567CAAE42CDF0501FC (void);
// 0x000000F5 System.Int32 Unity.Mathematics.int3::get_Item(System.Int32)
extern void int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029 (void);
// 0x000000F6 System.Boolean Unity.Mathematics.int3::Equals(Unity.Mathematics.int3)
extern void int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9 (void);
// 0x000000F7 System.Boolean Unity.Mathematics.int3::Equals(System.Object)
extern void int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748 (void);
// 0x000000F8 System.Int32 Unity.Mathematics.int3::GetHashCode()
extern void int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C (void);
// 0x000000F9 System.String Unity.Mathematics.int3::ToString()
extern void int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667 (void);
// 0x000000FA System.String Unity.Mathematics.int3::ToString(System.String,System.IFormatProvider)
extern void int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D (void);
// 0x000000FB System.Void Unity.Mathematics.int4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A (void);
// 0x000000FC System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.int3,System.Int32)
extern void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724 (void);
// 0x000000FD System.Void Unity.Mathematics.int4::.ctor(Unity.Mathematics.float4)
extern void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C (void);
// 0x000000FE Unity.Mathematics.int4 Unity.Mathematics.int4::op_Explicit(Unity.Mathematics.float4)
extern void int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E (void);
// 0x000000FF Unity.Mathematics.int4 Unity.Mathematics.int4::op_Multiply(Unity.Mathematics.int4,Unity.Mathematics.int4)
extern void int4_op_Multiply_m5FC679C570C40C6A3CCF3E600DACD56D48063524 (void);
// 0x00000100 Unity.Mathematics.int4 Unity.Mathematics.int4::op_Addition(Unity.Mathematics.int4,Unity.Mathematics.int4)
extern void int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3 (void);
// 0x00000101 Unity.Mathematics.int3 Unity.Mathematics.int4::get_xyz()
extern void int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD (void);
// 0x00000102 System.Int32 Unity.Mathematics.int4::get_Item(System.Int32)
extern void int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC (void);
// 0x00000103 System.Void Unity.Mathematics.int4::set_Item(System.Int32,System.Int32)
extern void int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5 (void);
// 0x00000104 System.Boolean Unity.Mathematics.int4::Equals(Unity.Mathematics.int4)
extern void int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978 (void);
// 0x00000105 System.Boolean Unity.Mathematics.int4::Equals(System.Object)
extern void int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96 (void);
// 0x00000106 System.Int32 Unity.Mathematics.int4::GetHashCode()
extern void int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B (void);
// 0x00000107 System.String Unity.Mathematics.int4::ToString()
extern void int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786 (void);
// 0x00000108 System.String Unity.Mathematics.int4::ToString(System.String,System.IFormatProvider)
extern void int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC (void);
// 0x00000109 Unity.Mathematics.quaternion Unity.Mathematics.quaternion::op_Implicit(UnityEngine.Quaternion)
extern void quaternion_op_Implicit_m4D19584694391AD6CBB3A0FC09D23DCEAE29E9A0 (void);
// 0x0000010A System.Void Unity.Mathematics.quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433 (void);
// 0x0000010B System.Void Unity.Mathematics.quaternion::.ctor(Unity.Mathematics.float4)
extern void quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D (void);
// 0x0000010C Unity.Mathematics.quaternion Unity.Mathematics.quaternion::op_Implicit(Unity.Mathematics.float4)
extern void quaternion_op_Implicit_m20E9FBF48FCB66C9F65E1174F94CBD513449A4A7 (void);
// 0x0000010D Unity.Mathematics.quaternion Unity.Mathematics.quaternion::AxisAngle(Unity.Mathematics.float3,System.Single)
extern void quaternion_AxisAngle_m983F92B5F96B2B35C8B50B91285557721362E48F (void);
// 0x0000010E System.Boolean Unity.Mathematics.quaternion::Equals(Unity.Mathematics.quaternion)
extern void quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775 (void);
// 0x0000010F System.Boolean Unity.Mathematics.quaternion::Equals(System.Object)
extern void quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011 (void);
// 0x00000110 System.Int32 Unity.Mathematics.quaternion::GetHashCode()
extern void quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929 (void);
// 0x00000111 System.String Unity.Mathematics.quaternion::ToString()
extern void quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498 (void);
// 0x00000112 System.String Unity.Mathematics.quaternion::ToString(System.String,System.IFormatProvider)
extern void quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057 (void);
// 0x00000113 System.Void Unity.Mathematics.quaternion::.cctor()
extern void quaternion__cctor_mA10CF9925C7F128021CBD135EEC961A6646F3527 (void);
// 0x00000114 System.Void Unity.Mathematics.uint2::.ctor(System.UInt32,System.UInt32)
extern void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727 (void);
// 0x00000115 Unity.Mathematics.uint2 Unity.Mathematics.uint2::op_Multiply(Unity.Mathematics.uint2,Unity.Mathematics.uint2)
extern void uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D (void);
// 0x00000116 System.Boolean Unity.Mathematics.uint2::Equals(Unity.Mathematics.uint2)
extern void uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF (void);
// 0x00000117 System.Boolean Unity.Mathematics.uint2::Equals(System.Object)
extern void uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C (void);
// 0x00000118 System.Int32 Unity.Mathematics.uint2::GetHashCode()
extern void uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED (void);
// 0x00000119 System.String Unity.Mathematics.uint2::ToString()
extern void uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8 (void);
// 0x0000011A System.String Unity.Mathematics.uint2::ToString(System.String,System.IFormatProvider)
extern void uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1 (void);
// 0x0000011B System.Void Unity.Mathematics.uint3::.ctor(System.UInt32,System.UInt32,System.UInt32)
extern void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E (void);
// 0x0000011C Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Multiply(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
extern void uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A (void);
// 0x0000011D Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_Addition(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
extern void uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3 (void);
// 0x0000011E Unity.Mathematics.uint3 Unity.Mathematics.uint3::op_ExclusiveOr(Unity.Mathematics.uint3,Unity.Mathematics.uint3)
extern void uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816 (void);
// 0x0000011F System.Boolean Unity.Mathematics.uint3::Equals(Unity.Mathematics.uint3)
extern void uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91 (void);
// 0x00000120 System.Boolean Unity.Mathematics.uint3::Equals(System.Object)
extern void uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE (void);
// 0x00000121 System.Int32 Unity.Mathematics.uint3::GetHashCode()
extern void uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76 (void);
// 0x00000122 System.String Unity.Mathematics.uint3::ToString()
extern void uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277 (void);
// 0x00000123 System.String Unity.Mathematics.uint3::ToString(System.String,System.IFormatProvider)
extern void uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167 (void);
// 0x00000124 System.Void Unity.Mathematics.uint4::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
extern void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852 (void);
// 0x00000125 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Multiply(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
extern void uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6 (void);
// 0x00000126 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_Addition(Unity.Mathematics.uint4,Unity.Mathematics.uint4)
extern void uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF (void);
// 0x00000127 Unity.Mathematics.uint4 Unity.Mathematics.uint4::op_BitwiseAnd(Unity.Mathematics.uint4,System.UInt32)
extern void uint4_op_BitwiseAnd_m8BDB3A45AD663DDFBC3E18A0EA84B3C902767C36 (void);
// 0x00000128 System.Boolean Unity.Mathematics.uint4::Equals(Unity.Mathematics.uint4)
extern void uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5 (void);
// 0x00000129 System.Boolean Unity.Mathematics.uint4::Equals(System.Object)
extern void uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C (void);
// 0x0000012A System.Int32 Unity.Mathematics.uint4::GetHashCode()
extern void uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04 (void);
// 0x0000012B System.String Unity.Mathematics.uint4::ToString()
extern void uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21 (void);
// 0x0000012C System.String Unity.Mathematics.uint4::ToString(System.String,System.IFormatProvider)
extern void uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622 (void);
static Il2CppMethodPointer s_methodPointers[300] = 
{
	math_hash_m02C3B1E016522A7C4B32A4FCD30DED6807391ED9,
	math_float3_m894589B682C227F824338292623B5C7B5BC1EEDB,
	math_hash_mE7D1E0FE1B3FB42A30188A62D737426389918F84,
	math_hash_m17F6D2D904AD2436624B3646618ACCCA8527F855,
	math_float4_mAC34C42566D3E20AC2549885C6BC4FEC38ED9DD6,
	math_float4_m6C80DFA29DAA1B56B5B52BC291181255CE3B556F,
	math_float4_m6D1066B47EE7BC7A47A136D969B5A3CA41902224,
	math_hash_mE2E294D89343D0C0D08ABE6E8D97BFDBD5AD60A5,
	math_shuffle_m31F8B693F53A1BCA32C880DC8E01B42B26314AE6,
	math_select_shuffle_component_m53D41A669176422EEA1A2071743EDEA42A1162DC,
	math_float4x4_m09B0514DCACA00D584AD198A2C37CC4AE70096DC,
	math_float4x4_mF52F65C990BE5D24056AFFED84A87B9B358572BF,
	math_transpose_m76E580466C3B5A7D1E0A1534C8E17BD0176F51A0,
	math_inverse_m26EB769712BD20C07185B9ECAC5808E06DDC7BF3,
	math_hash_m16C61605FBE531F8EB703823255407075EA5A464,
	math_hash_m39794F85EE67A7E11DFBED0AE3910D019E290EF6,
	math_hash_m0854130F9B19EB36E238AE66BEC317C9C16BB05D,
	math_hash_mF6001182C01D5B6AF14C65F4F5E06E13A17D6871,
	math_asint_mAB46275EA58D5FC95717224B4214F3C9987A8C86,
	math_asuint_m9523E8E14D1E3ACBAA1D65C6143AB38EF8EB671B,
	math_asuint_m0B6AD24480AE16B1914466CEEF2DD1887AB8D79B,
	math_asuint_m1C568C2DA90918A60EBEB42D75760D9B75709E5D,
	math_asuint_mE13F6DE596F0D55A1A5BC805D1801FB3822042AC,
	math_asuint_m351AD372B79DB7BEFB5AC7667CDF3409544E3863,
	math_asuint_m7BB84B6113F67FF1E002B1E0414D8A4754070550,
	math_asuint_m6BECD59CBEEAB7DBA7EB79A50D54424B974B760D,
	math_asfloat_mFC74F5F38E8967C1320B94E1AF95AB1D55D46181,
	math_asfloat_m6ABF86DC7492891584574087116E4904661A73DF,
	math_asfloat_mE07CE8DE7DA371941697F3D0D043A26DEB053826,
	math_asfloat_mD9B4C5BBB6AC641027D94C2045C4C3AF416DFF6D,
	math_min_mD5F9F74A53F030155B9E68672EF5B4415FB0AB4A,
	math_min_m661F28A20439E739FAB5DA0FF11854BEAF7EC951,
	math_min_m65ACBB9005609DE8184015662C758B23A85C794E,
	math_min_m61425DAA42F41CE3CD08A5DEDD02206F7A704F89,
	math_min_mDCA05EE5BF99F3753F8AF682B3C5B85C4620C60C,
	math_min_m6BF29A95DEDC36F4E386F4C02122671FCC271BC8,
	math_min_mC45C7024F6850EB1D382A2B2F6F31CCA4B2750E6,
	math_max_mC3AC72A0590480D0AEFE3E45D34C9DD72057FEDF,
	math_max_mAED5DA82BD5495A15A2365FCE7CA4B775F6E1132,
	math_max_mD8541933650D81292540BAFF46DE531FA1B333FC,
	math_max_m3082575385F8F5B755FF5D4399B1CD43A17550EA,
	math_max_m7E3D48AE84E0E23DD089176B4C0CD6886303ABCF,
	math_max_m5A2AD47597A69870F6E5A8A2ABEE9A1EAC771393,
	math_lerp_m6F14A6C30E7D69AAF60AD89BCEFDC4E1B286ED91,
	math_clamp_mAC2AAD9592C2C59601A5CF30D601D645033E2569,
	math_clamp_m3FDC8AAE05842733E193852C929DAAC6F5E8C931,
	math_clamp_m4A3A6B869C607E006DBC5EB2A50C0812F40E09A4,
	math_saturate_mA17845866D2189A354872A777FF9EE7D7AD0C7FC,
	math_abs_m684426E813FD887C103114B2E15FCBAFFF32D3B0,
	math_abs_m3842FE0B152ABEA4BDD3985EF8C38B1E13BE6273,
	math_dot_m6238E071DBA53F376473FFEC28706C9B537E6BAE,
	math_dot_mF7AC1B7E5E29630523124BDBFA34A2672CB852A9,
	math_dot_mE2D9B276C8BDE4C98B8E900115C368F23EB0EB97,
	math_atan2_mFFA7942F9F41993E3F752E630DB45D0B4104034C,
	math_cos_m210EFB3DFB9561CDEEF523635CF60E1406678424,
	math_acos_mDF4948B8CF18EF8052C38D7914802E7869B79B6C,
	math_sin_mFE9768BE04BEBBD19B08FB6694319C9FECCC95FE,
	math_floor_m292DE338C9AC58EEEF598B246CB3406533709F3B,
	math_floor_mCADCD88A3E295AEB16A2356DACBA00FDD8DFA9B9,
	math_floor_m1595239B5E0B9CA2944C200E1ACADE9CEED6EA2D,
	math_floor_m6F1A809C667676D8CDF372A24CC318F90943AAC7,
	math_ceil_m09F7522539A79883D220617019957CCF0CB4DA8B,
	math_rcp_mBBCDD8F05AFE2E307F898E81A06B64BF0E2625D0,
	math_rcp_m60294695218F02BCB25674A2FA40A86499DD6F38,
	math_rcp_m3D4A844DEF4B4531007D5E1A17BF493690FEBB8C,
	math_sign_m7031A67FC34735AB07B240EB86432BDDCEA5C611,
	math_sign_m63A39DE29A6BCB0E9BA152B458A0DD2EB9EFB23C,
	math_pow_m792865DB1E3BCC792726FFB800ABDE9FF109D3B4,
	math_exp2_m91B7071A3214B73A372F626DFAA243B3C65BD230,
	math_log_m32923945396EF294F36A8CA9848ED0771A77F50F,
	math_sqrt_mCFFE475634B2765D4E1A8DE9A9B331742E0637CF,
	math_rsqrt_m4BCAD96F434850A62C649607F810ED996C19D896,
	math_normalize_m06189F2D06A4C6DEB81C6623B802A878E3356975,
	math_normalizesafe_mD616A562864DA7E3FF165AB6E2CE7BE30131E668,
	math_normalizesafe_m0043348AB7956C50EFFD5572CA0D516DCB54A376,
	math_length_m25A33854A8D7F4A4B96C82171BE616376ECA25CE,
	math_length_mECD912F8B5F13E8FDFEFC19DDC928AC69C9669D4,
	math_length_mD7967FEA18B97C7AC6CFE6B0BE3D45D35D355170,
	math_lengthsq_mCA5F01F89CC6AAAE7F0263A8F3C34FC2C7A98561,
	math_lengthsq_mD422A214358E935793F5ED10991D70F040848F2D,
	math_cross_mB8F4E8BD47C40A250C155CA18704F0ECDE9F7019,
	math_select_mD6522E9D958653C41F4B2E3463B79F7A011E60AB,
	math_select_m3FF6FBC5429C94D76C025979A6D3DCA2B2B62602,
	math_sincos_m62FF0B31557448DAF3C3ED1EBCA171BD524BD96B,
	math_lzcnt_mBB573111B33D6C11E506BA3986481FA994D201FF,
	math_lzcnt_m69123CF8445E0B67FA30F24BA42F663C105BE7EB,
	math_tzcnt_mAD942678F41AC6A34E7E83752D38E40F1043FE4E,
	math_tzcnt_m3A483A42E55BE961B589C59F99EE7972D0D296E8,
	math_ceilpow2_mA7B90555C48581B479A78BA3EC7EE1DC7E2F657E,
	math_ceilpow2_mC273C4BD00A26075F4370BA1B290D42C42C97227,
	math_cmin_mDDC6705B504F566B700A58BBC5DBE63EE40EAE53,
	math_cmin_mD6518D31DF8A9C48087287DF5B5A82A3DF280E0A,
	math_cmax_m5B2F559332B7FD6F7B77F932F39C18D277A17888,
	math_cmax_m468A5263FCD72A1EC99854B9477FD2FAF4ABBFFF,
	math_cmax_mA222CC6FC5245F461911FC523F1DCC9B14D36508,
	math_csum_m8D93664E8D149EFD78EC8DEBC03CD3D53AF12DC1,
	math_csum_m65FAB8CE28E024565B82659E352D7C3A6CA0C751,
	math_csum_m6790689A2E1340EB594A26E4942D6C9537AB22F5,
	math_movelh_mD108B399435E872C1D47C71A9FEF0C424A135E0C,
	math_movehl_mF12E4BC00C4D2CC19E2B4209A2BEF9EAE302C553,
	math_float3x3_m083898B3E74368DC86FB3D19406C3ED60AF63583,
	math_mul_mDBF2C1741940E16BDEB49774E0CF13AB4889A255,
	math_mul_m5C88F4BCCEAC68636D82BCA37DFD19731E9C8B3D,
	math_quaternion_m98D3F384B62E7B953EBDCF6D74C5C7E95C2284DD,
	math_conjugate_mF4ABF8CC798742764982DEEC731088BF719B7935,
	math_inverse_m8C4E2F1AA7871C40EA7FFAB90D5F2653466E894D,
	math_dot_m7775437DA754568CDBD60C8CDDFCA27F7D197634,
	math_normalize_m2652F56AA1212DD896B0FBF9BB69AD02B1A17C6D,
	math_mul_m545D5B9EB2838DCDD021F61D385D8DE119422FC3,
	math_mul_m003A157203A82B7842A6894B3127F84D61BE3B9B,
	math_rotate_mD7EBD96F0C1DAD111B6A3CD7942C006539777D47,
	math_nlerp_mB7BD6C0F6F0AD1411F206E3BFDCC4161F1DF5D54,
	math_slerp_m4819ACD4A8A9FA276AE84D9EAAA2ACAD2ED54A28,
	math_hash_mD498CD0DD6F1862ED24987E8CBF9CFDF29C1782C,
	math_uint2_m01CB3A33D39ACAB2FC3FB5E6AB23FED049C2D5B2,
	math_hash_mA46BEA1D3896F53E98145896C8F7136062532F90,
	math_uint3_m16B75AEC39C187586C9AAAAA5EFF23DB7237C729,
	math_hash_m93543A4E24AB7FC17AA2810F0AEDF9ADABE333E6,
	math_uint4_mB8BA57231B5CA2C79825112A5B8895CCD7241777,
	math_hash_m5E517FEDA0EC60364F96435FEFECAC5E5B7A1811,
	float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449,
	float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C,
	float2_op_Implicit_mB01382E876FA2AFC63701F493121DEA95D2677E1,
	float2_op_Multiply_m7262BDAFAD6CB1D6D980929C82826C82C86F5039,
	float2_op_Addition_m1FCEA54B4EB9E2E5BBF9E2C5A6466589B486CF81,
	float2_op_Subtraction_mD7E1752FB29F54A98A4A52140E8BCFD659471E19,
	float2_op_Division_m122E66E69678386034C53B7D9B89A551628473FD,
	float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB,
	float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665,
	float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092,
	float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB,
	float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D,
	float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9,
	float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28,
	float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78,
	float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A,
	float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C,
	float3_op_Multiply_m6E5382EBCA40ADBD10C11AB231C4A8629279CCF1,
	float3_op_Multiply_m0F61C5933324CAE6D497DED1EBB85E4DA759AB13,
	float3_op_Multiply_m4A0B40DB4725F815AD14147DA2D00F1A7B1E88BE,
	float3_op_Addition_m7247CCD21BDA2187158DE59FD2C609D6BC966978,
	float3_op_Subtraction_m3093AF71655C11E35349271AB39F99183B75B7E2,
	float3_op_Division_m84488830E369A20FBB0BE95A05FD94DE1F4DDBB2,
	float3_op_Division_mD5CB333470F4CE8648AA61B52F79CC8FABAEAB4A,
	float3_op_Division_mE2739F8C6E0ADC06D8DCD923F9828B75EACB3ECF,
	float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F,
	float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024,
	float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43,
	float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8,
	float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF,
	float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A,
	float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF,
	float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B,
	float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3,
	float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF,
	float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F,
	float3_op_Implicit_m10DFE908DCDD18D73FA11E4C9CDB23E16FA3ABE8,
	float3_op_Implicit_mC2BC870EF4246C5C3A2FC27EE9ABEDAFF49DC1EF,
	float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E,
	float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026,
	float3x3_op_Multiply_m84830685A93039297DC893DA1B6A28D675822961,
	float3x3_op_Addition_mD075893A8D24978DC605DA7A415F7CE4BEE8F0F3,
	float3x3_op_Division_m92B0F2EBB2E77BF84C675C513DC3F84445F2CB69,
	float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9,
	float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2,
	float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC,
	float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA,
	float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24,
	float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4,
	float3x3__cctor_mF2DDB6D13785F3BF4A1D002DD387896890A8ACC4,
	float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241,
	float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7,
	float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B,
	float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65,
	float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758,
	float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E,
	float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54,
	float4_op_Implicit_m18F0BAF7440BF58605AA7A75CB911431ED4347DF,
	float4_op_Implicit_m4C5BAD1D894F2CFBC62675DBE2813CC48BB57F4C,
	float4_op_Multiply_m6F464E9AFD43E52565002DA4AC98567B107266D8,
	float4_op_Multiply_mE43C8199A7C88F47C58C11F2B1D2E4634CE57FB7,
	float4_op_Multiply_m39AF6CD3EB92CBC6977B29E9F28AEA4AF6F7BC70,
	float4_op_Addition_m65F1F7CB4942EB32EAC170BF384223B5CD733FF0,
	float4_op_Addition_m50BF16260478F8BEBB56698FC677066CB2792697,
	float4_op_Subtraction_m1A66982C4188B84BABA32FA6F9E7849F73BBF370,
	float4_op_Subtraction_mC5AB272630A81EDCFC642F4953FA24F84F03FBFB,
	float4_op_Division_m489385B46636C7B5CCE01DEECA0A603A2EE807AA,
	float4_op_Division_m053698B6254EF416945FD2161A92DFBB524A74F8,
	float4_op_Division_m847947B86BE29A43C35E74AB168A525866AA4B15,
	float4_op_UnaryNegation_m4888DF914E2005D9A25A835D0BBF68609E009CDA,
	float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE,
	float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB,
	float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3,
	float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9,
	float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B,
	float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E,
	float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC,
	float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC,
	float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91,
	float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF,
	float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87,
	float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6,
	float4_get_xy_mC5A971D69E3278438139480A25313449133571E0,
	float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5,
	float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F,
	float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D,
	float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508,
	float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B,
	float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230,
	float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850,
	float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B,
	float4_op_Implicit_mADB6904960D508B7B30752A99C7013AED76065ED,
	float4_op_Implicit_mD1C84DA295349F9E668ACB153BA03F21D2B976CE,
	float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929,
	float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988,
	float4x4_op_Multiply_mEEE7C88A7EF269370E12BA82BDC101D83893FF96,
	float4x4_op_Multiply_mC1B6C82737F3D82CF4A5783D7B504E76CB1CF553,
	float4x4_op_Addition_m8ADC44933E779260371FB510153637822E9BDE84,
	float4x4_op_Subtraction_mFB25888DC0C2F88D032EB8B09D219CDA84BB1D71,
	float4x4_op_Division_mD54BB05F0E1C9D03F8D3F9B58EAD537168D92E37,
	float4x4_op_UnaryNegation_mD32379B42356BEC1149EA39D841F96A12E8B8786,
	float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A,
	float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832,
	float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A,
	float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD,
	float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2,
	float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2,
	float4x4_Scale_mEE04A811A295C99F280056A51ABC83356CB0A4A3,
	float4x4_Scale_mD6E98ACC10BB3F6550EC578201C07A89B72CDC74,
	float4x4_TRS_m2A09A0CF1B7D774D0674577FEA3FBBAD5B25AC92,
	float4x4__cctor_mBA99BE0167C79FA3EFFB97DBBD42249AB9894684,
	int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1,
	int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974,
	int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E,
	int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7,
	int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66,
	int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A,
	int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9,
	int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691,
	int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32,
	int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF,
	int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618,
	int3_op_Addition_m08C6B9DB7119923A27914BAFD7DCDEE50CC988A4,
	int3_op_Subtraction_m0AE9F07D1B4603D7CBD3AF567CAAE42CDF0501FC,
	int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029,
	int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9,
	int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748,
	int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C,
	int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667,
	int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D,
	int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A,
	int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724,
	int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C,
	int4_op_Explicit_m4524AFD23931BE3522824DC94F61924FCE72DB1E,
	int4_op_Multiply_m5FC679C570C40C6A3CCF3E600DACD56D48063524,
	int4_op_Addition_m6AFB8A4BB9BA20FD4C4E308A0AD2657E0BF22EE3,
	int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD,
	int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC,
	int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5,
	int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978,
	int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96,
	int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B,
	int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786,
	int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC,
	quaternion_op_Implicit_m4D19584694391AD6CBB3A0FC09D23DCEAE29E9A0,
	quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433,
	quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D,
	quaternion_op_Implicit_m20E9FBF48FCB66C9F65E1174F94CBD513449A4A7,
	quaternion_AxisAngle_m983F92B5F96B2B35C8B50B91285557721362E48F,
	quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775,
	quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011,
	quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929,
	quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498,
	quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057,
	quaternion__cctor_mA10CF9925C7F128021CBD135EEC961A6646F3527,
	uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727,
	uint2_op_Multiply_mAFCBB08307277608981FD1631CAF3E7C009C569D,
	uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF,
	uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C,
	uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED,
	uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8,
	uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1,
	uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E,
	uint3_op_Multiply_m398023D1304A0332CD32AA2E89B7E5CBAB38AE8A,
	uint3_op_Addition_m20E123E82363987B7995B4532CD2FB72A33915D3,
	uint3_op_ExclusiveOr_mE588D29DA27A3537CF4F1DC1D9951DD66E4CE816,
	uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91,
	uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE,
	uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76,
	uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277,
	uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167,
	uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852,
	uint4_op_Multiply_m6625351DAB9491A1C68523349071700C2BE353F6,
	uint4_op_Addition_m754DF7CDC5C23AE88623DD83AF43F9DB1EBF78BF,
	uint4_op_BitwiseAnd_m8BDB3A45AD663DDFBC3E18A0EA84B3C902767C36,
	uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5,
	uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C,
	uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04,
	uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21,
	uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622,
};
extern void float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_AdjustorThunk (void);
extern void float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_AdjustorThunk (void);
extern void float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB_AdjustorThunk (void);
extern void float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665_AdjustorThunk (void);
extern void float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092_AdjustorThunk (void);
extern void float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_AdjustorThunk (void);
extern void float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D_AdjustorThunk (void);
extern void float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_AdjustorThunk (void);
extern void float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_AdjustorThunk (void);
extern void float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_AdjustorThunk (void);
extern void float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_AdjustorThunk (void);
extern void float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C_AdjustorThunk (void);
extern void float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F_AdjustorThunk (void);
extern void float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_AdjustorThunk (void);
extern void float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43_AdjustorThunk (void);
extern void float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8_AdjustorThunk (void);
extern void float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF_AdjustorThunk (void);
extern void float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A_AdjustorThunk (void);
extern void float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_AdjustorThunk (void);
extern void float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B_AdjustorThunk (void);
extern void float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_AdjustorThunk (void);
extern void float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_AdjustorThunk (void);
extern void float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_AdjustorThunk (void);
extern void float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_AdjustorThunk (void);
extern void float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_AdjustorThunk (void);
extern void float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_AdjustorThunk (void);
extern void float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2_AdjustorThunk (void);
extern void float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC_AdjustorThunk (void);
extern void float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA_AdjustorThunk (void);
extern void float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24_AdjustorThunk (void);
extern void float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4_AdjustorThunk (void);
extern void float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_AdjustorThunk (void);
extern void float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7_AdjustorThunk (void);
extern void float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B_AdjustorThunk (void);
extern void float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_AdjustorThunk (void);
extern void float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758_AdjustorThunk (void);
extern void float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_AdjustorThunk (void);
extern void float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_AdjustorThunk (void);
extern void float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_AdjustorThunk (void);
extern void float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_AdjustorThunk (void);
extern void float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_AdjustorThunk (void);
extern void float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_AdjustorThunk (void);
extern void float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_AdjustorThunk (void);
extern void float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_AdjustorThunk (void);
extern void float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_AdjustorThunk (void);
extern void float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_AdjustorThunk (void);
extern void float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91_AdjustorThunk (void);
extern void float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_AdjustorThunk (void);
extern void float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_AdjustorThunk (void);
extern void float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_AdjustorThunk (void);
extern void float4_get_xy_mC5A971D69E3278438139480A25313449133571E0_AdjustorThunk (void);
extern void float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5_AdjustorThunk (void);
extern void float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F_AdjustorThunk (void);
extern void float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D_AdjustorThunk (void);
extern void float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_AdjustorThunk (void);
extern void float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B_AdjustorThunk (void);
extern void float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_AdjustorThunk (void);
extern void float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_AdjustorThunk (void);
extern void float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_AdjustorThunk (void);
extern void float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_AdjustorThunk (void);
extern void float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_AdjustorThunk (void);
extern void float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A_AdjustorThunk (void);
extern void float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_AdjustorThunk (void);
extern void float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A_AdjustorThunk (void);
extern void float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD_AdjustorThunk (void);
extern void float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2_AdjustorThunk (void);
extern void float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2_AdjustorThunk (void);
extern void int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1_AdjustorThunk (void);
extern void int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974_AdjustorThunk (void);
extern void int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E_AdjustorThunk (void);
extern void int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_AdjustorThunk (void);
extern void int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66_AdjustorThunk (void);
extern void int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A_AdjustorThunk (void);
extern void int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9_AdjustorThunk (void);
extern void int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691_AdjustorThunk (void);
extern void int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_AdjustorThunk (void);
extern void int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF_AdjustorThunk (void);
extern void int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618_AdjustorThunk (void);
extern void int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029_AdjustorThunk (void);
extern void int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_AdjustorThunk (void);
extern void int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748_AdjustorThunk (void);
extern void int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C_AdjustorThunk (void);
extern void int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667_AdjustorThunk (void);
extern void int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D_AdjustorThunk (void);
extern void int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_AdjustorThunk (void);
extern void int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_AdjustorThunk (void);
extern void int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_AdjustorThunk (void);
extern void int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_AdjustorThunk (void);
extern void int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC_AdjustorThunk (void);
extern void int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5_AdjustorThunk (void);
extern void int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_AdjustorThunk (void);
extern void int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96_AdjustorThunk (void);
extern void int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B_AdjustorThunk (void);
extern void int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786_AdjustorThunk (void);
extern void int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC_AdjustorThunk (void);
extern void quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_AdjustorThunk (void);
extern void quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_AdjustorThunk (void);
extern void quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_AdjustorThunk (void);
extern void quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011_AdjustorThunk (void);
extern void quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929_AdjustorThunk (void);
extern void quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498_AdjustorThunk (void);
extern void quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057_AdjustorThunk (void);
extern void uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_AdjustorThunk (void);
extern void uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_AdjustorThunk (void);
extern void uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C_AdjustorThunk (void);
extern void uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_AdjustorThunk (void);
extern void uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_AdjustorThunk (void);
extern void uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_AdjustorThunk (void);
extern void uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_AdjustorThunk (void);
extern void uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_AdjustorThunk (void);
extern void uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE_AdjustorThunk (void);
extern void uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_AdjustorThunk (void);
extern void uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_AdjustorThunk (void);
extern void uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_AdjustorThunk (void);
extern void uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_AdjustorThunk (void);
extern void uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_AdjustorThunk (void);
extern void uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C_AdjustorThunk (void);
extern void uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_AdjustorThunk (void);
extern void uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_AdjustorThunk (void);
extern void uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[120] = 
{
	{ 0x06000079, float2__ctor_mDEC8A4039E029926E0424F0FB614C7F679AE7449_AdjustorThunk },
	{ 0x0600007A, float2__ctor_mEBE7123BFDA33634F685419877BDC927C4C2D56C_AdjustorThunk },
	{ 0x06000080, float2_get_xy_mC1845752E0FD8E1D413D170DB0BCF604F8E4C2DB_AdjustorThunk },
	{ 0x06000081, float2_get_Item_m3F8D13AF9B232D6340975F1D04F26E62A2120665_AdjustorThunk },
	{ 0x06000082, float2_set_Item_mB443DBBB1EA64C5D95E3E9E4AB06065307426092_AdjustorThunk },
	{ 0x06000083, float2_Equals_mD7E0010E86764F768D155F084C5049B1A37451AB_AdjustorThunk },
	{ 0x06000084, float2_Equals_m2B929D2B1750063ED9C8F71F517E707629A2865D_AdjustorThunk },
	{ 0x06000085, float2_GetHashCode_m14F0A1D75CEB912B8D368074F8EC66E768800FE9_AdjustorThunk },
	{ 0x06000086, float2_ToString_mDD9456E5C3F28889E650CED5533DCD06219F3A28_AdjustorThunk },
	{ 0x06000087, float2_ToString_mCCF46926AC2D48D3344DC9373B33F0E005071C78_AdjustorThunk },
	{ 0x06000088, float3__ctor_mA1B0F2B6874F0DEDFC715C334892EB2FAB31B40A_AdjustorThunk },
	{ 0x06000089, float3__ctor_m9E3A136F0CAD409A42B39B54E95C86ECE24FC35C_AdjustorThunk },
	{ 0x06000092, float3_get_xyz_mFA8A57E53CE26A944536ADBD9CB3DB5BD122546F_AdjustorThunk },
	{ 0x06000093, float3_get_yzx_m38F00E0047938E610A1D5B7F3A6289321927E024_AdjustorThunk },
	{ 0x06000094, float3_get_xy_m55576372D92476BE28D41F21F873589ECAAF2D43_AdjustorThunk },
	{ 0x06000095, float3_get_yz_mA62D2771CF241B0F763E30DDA94343BE2A6B49C8_AdjustorThunk },
	{ 0x06000096, float3_get_Item_mA440BF3C3475DC2596FF3FD9043B6AB780DABCAF_AdjustorThunk },
	{ 0x06000097, float3_set_Item_m68A1B35F9CB55309EACA7BA64A4EDE2FD435B45A_AdjustorThunk },
	{ 0x06000098, float3_Equals_m21D80AD2B9F37B1210C6F36BE86F76AA527BF2CF_AdjustorThunk },
	{ 0x06000099, float3_Equals_m3C3B40E46D39C1FC39D250269E33EFC955235B2B_AdjustorThunk },
	{ 0x0600009A, float3_GetHashCode_m127685CE1D48644B6DC250A3CA9B989A54BA1AF3_AdjustorThunk },
	{ 0x0600009B, float3_ToString_mB8035CAC163C19E11A35ADFE448E5B362A56D8AF_AdjustorThunk },
	{ 0x0600009C, float3_ToString_mAA3FEBFB0473D5C7F22CB3B6B56E52BEE6054F5F_AdjustorThunk },
	{ 0x0600009F, float3x3__ctor_m387EDCCF0BA938738A552018FAF3884F9BEAEA2E_AdjustorThunk },
	{ 0x060000A0, float3x3__ctor_m5B9B5B41FA9CF99FEABE62561D521AEB10C1A026_AdjustorThunk },
	{ 0x060000A4, float3x3_Equals_m1C645D93DC0EEEF5D4093A9E580671C0DB5950C9_AdjustorThunk },
	{ 0x060000A5, float3x3_Equals_mF46E47DBB734D45F1D9CDE010EA463B044F99CF2_AdjustorThunk },
	{ 0x060000A6, float3x3_GetHashCode_mE9CEC4A58B2307772643D638BE0A6D9701FEF0FC_AdjustorThunk },
	{ 0x060000A7, float3x3_ToString_m8B5B4FF2BF288F52487FC42C6ACE91B4C296A0BA_AdjustorThunk },
	{ 0x060000A8, float3x3_ToString_m017C3A3447160D0608B1F70A08271F40BCD70F24_AdjustorThunk },
	{ 0x060000A9, float3x3__ctor_mABA6CE7801B3ED930728FAD79F40CDEC050DAAD4_AdjustorThunk },
	{ 0x060000AB, float4__ctor_mD7DD9759C791823116719CAE8EE693E9C173E241_AdjustorThunk },
	{ 0x060000AC, float4__ctor_m7A7D1FDA2A10363C8CE92AE7D2E9923BEF3A0BE7_AdjustorThunk },
	{ 0x060000AD, float4__ctor_m1C57E2A41AF306A0318A9E0CA5B5BCAF4192370B_AdjustorThunk },
	{ 0x060000AE, float4__ctor_mBBBCA39AAFF077165E0BB4240EC12425F92F6D65_AdjustorThunk },
	{ 0x060000AF, float4__ctor_m2D58BF2058B16A0A0DF798D1A24AE432D5C85758_AdjustorThunk },
	{ 0x060000B0, float4__ctor_mED7A8FA2B6D8DF819ACB58EF0E6521E446FA0B6E_AdjustorThunk },
	{ 0x060000B1, float4__ctor_m4BF0A0752D5C3948115EC7556C7FE2A3B8422A54_AdjustorThunk },
	{ 0x060000BF, float4_get_xyzx_m989180C1D28F90F893570322765085DC30548BFE_AdjustorThunk },
	{ 0x060000C0, float4_get_yzxy_m6A4E1974253696A11C375173D1EB4BFB00C363DB_AdjustorThunk },
	{ 0x060000C1, float4_get_yzxz_m33E80FF35BBC3A0072855AB29E70AE3A3CFBB9F3_AdjustorThunk },
	{ 0x060000C2, float4_get_zxyy_m075C03F61628AE4FD18D2111E6FF56DF784DCAC9_AdjustorThunk },
	{ 0x060000C3, float4_get_zxyz_m5981863F98C1D86F322D795603ADB4599A561F7B_AdjustorThunk },
	{ 0x060000C4, float4_get_wwwx_mE83114BE364D60F434305FFBAE8EE453252F0C7E_AdjustorThunk },
	{ 0x060000C5, float4_get_wwww_m130A5CBE171942FF5CC1D4C87F1C4DB62508F0BC_AdjustorThunk },
	{ 0x060000C6, float4_get_xyz_m4737722995A5B3DC8DD7C8064525E79FC8327AAC_AdjustorThunk },
	{ 0x060000C7, float4_set_xyz_m466072DC7ED4CC4D22B577BF142AFA8E5BA00C91_AdjustorThunk },
	{ 0x060000C8, float4_get_yxw_m865297C6484EF88B49C0C1C850AA7D2A5B4166CF_AdjustorThunk },
	{ 0x060000C9, float4_get_zwx_mE900A07D61FF468E0B30A16271DBA68C7031FD87_AdjustorThunk },
	{ 0x060000CA, float4_get_wzy_mE92E1FE78BF0B129A274DFD044AF6F53D4AECAC6_AdjustorThunk },
	{ 0x060000CB, float4_get_xy_mC5A971D69E3278438139480A25313449133571E0_AdjustorThunk },
	{ 0x060000CC, float4_get_zw_mD9EC8B740E81546EFA785527A600A8A10345F7C5_AdjustorThunk },
	{ 0x060000CD, float4_get_Item_m700A43C22C96BF7C10FC1588E4C785A4A0D1280F_AdjustorThunk },
	{ 0x060000CE, float4_set_Item_mBF30C3518F0E504C576D67E1D31AF827FCD9ED3D_AdjustorThunk },
	{ 0x060000CF, float4_Equals_mC17658EE14E0595E33E170A1CF5325AD7240B508_AdjustorThunk },
	{ 0x060000D0, float4_Equals_m6247D066192E1714DDC2CC001D0D20D10D60228B_AdjustorThunk },
	{ 0x060000D1, float4_GetHashCode_mDAE54F7570659BE4963BEA7F347A137B7EFC5230_AdjustorThunk },
	{ 0x060000D2, float4_ToString_mF8E4242AD739E6BC3CFDAF96C26236D385DF7850_AdjustorThunk },
	{ 0x060000D3, float4_ToString_m643BEBFFE6AE7B5E0366C777BEE1178924D5374B_AdjustorThunk },
	{ 0x060000D6, float4x4__ctor_m0FDA901A772807CC3D37BE20A487617CE7738929_AdjustorThunk },
	{ 0x060000D7, float4x4__ctor_mF042192A4C60E1AD2A05A71DFAC452B16ACBF988_AdjustorThunk },
	{ 0x060000DE, float4x4_get_Item_m812D6956F0F442948E34DD096414A3F3A756D71A_AdjustorThunk },
	{ 0x060000DF, float4x4_Equals_mD4459B6C21ED6DDD45A6BE6D5C50C15B404FE832_AdjustorThunk },
	{ 0x060000E0, float4x4_Equals_m67A2A5FB7B6E8ADA62208E1296EB3ACC0D5FD50A_AdjustorThunk },
	{ 0x060000E1, float4x4_GetHashCode_m67E61D84AA6FFBD9989B24F167D13B9904B8EEDD_AdjustorThunk },
	{ 0x060000E2, float4x4_ToString_m2843568259ACB923AA177A478AD93FD4887556C2_AdjustorThunk },
	{ 0x060000E3, float4x4_ToString_mBFCBD17C8C9D3C38DA74DC1E3FE1B9B758CA19B2_AdjustorThunk },
	{ 0x060000E8, int2__ctor_mCE4BEB0059BFFC4639438295127895BBB4DE92C1_AdjustorThunk },
	{ 0x060000E9, int2__ctor_m7DC30C9277029820A7E0C6142E309C2942DDC974_AdjustorThunk },
	{ 0x060000EA, int2_get_Item_m70D47E8F21D0DD9D0FCB4D9C06A525FB18DEF08E_AdjustorThunk },
	{ 0x060000EB, int2_Equals_mD6876BF79810802E9BA823CFF8E1D1C87AEF79A7_AdjustorThunk },
	{ 0x060000EC, int2_Equals_mB003B2E3025C65815AC3282C0C9776FE58066B66_AdjustorThunk },
	{ 0x060000ED, int2_GetHashCode_m78F2C3698B49C74926D9B88327C97F93E4205B9A_AdjustorThunk },
	{ 0x060000EE, int2_ToString_m2B5C06E9DADFCA468DA0E7F4731E140BEF55B5E9_AdjustorThunk },
	{ 0x060000EF, int2_ToString_m7E99CF0A6825A1DE9E1C11F37446955214E31691_AdjustorThunk },
	{ 0x060000F0, int3__ctor_m74D3CA49981FF42F85189F58938C9A53E76A6D32_AdjustorThunk },
	{ 0x060000F1, int3__ctor_m29323CA6D2D245761F1137016B93EDF31B4187CF_AdjustorThunk },
	{ 0x060000F2, int3__ctor_m7EC813A9BB39C6BF2046283DB6B51BDA5F4EA618_AdjustorThunk },
	{ 0x060000F5, int3_get_Item_m81FAF129BACD4DDA87C0446E826C1C4195546029_AdjustorThunk },
	{ 0x060000F6, int3_Equals_m6D63A8D64A118D3B3A08FFCCF6D2FC9D9E03D2C9_AdjustorThunk },
	{ 0x060000F7, int3_Equals_mCBF9B7647C30EEDBCA8182F86D46359FB9E3D748_AdjustorThunk },
	{ 0x060000F8, int3_GetHashCode_m31960EAF60715F1C01FB579DCAF5379F36F3283C_AdjustorThunk },
	{ 0x060000F9, int3_ToString_m174725CCEE10A8880C17E44DB21966CF484EE667_AdjustorThunk },
	{ 0x060000FA, int3_ToString_m785ABCBC3B373E9C024B47CF95BFEF8C03A1DB4D_AdjustorThunk },
	{ 0x060000FB, int4__ctor_m26329A7A32A46737674F27BA3A9BF837AF84A76A_AdjustorThunk },
	{ 0x060000FC, int4__ctor_m087421DC383BFF516CD84111D4EDEC2C260D0724_AdjustorThunk },
	{ 0x060000FD, int4__ctor_m1412DD3681CE34F91D8144123595CE27E583808C_AdjustorThunk },
	{ 0x06000101, int4_get_xyz_m6EA8952C3146F20CAA13B3AA73ED1BD08F671DCD_AdjustorThunk },
	{ 0x06000102, int4_get_Item_m6557CF3F68024490881251DEC6CA7C03177F54CC_AdjustorThunk },
	{ 0x06000103, int4_set_Item_mF68EB672EB3ACAFF179823A030A4B29B49BAFDA5_AdjustorThunk },
	{ 0x06000104, int4_Equals_mBC43D293382DF25F317665789C0F85AC0B787978_AdjustorThunk },
	{ 0x06000105, int4_Equals_m26127338CC556E69F60DFB4DDC606AB6D1E1AC96_AdjustorThunk },
	{ 0x06000106, int4_GetHashCode_m02430B4D5BB56C32AB71A082B7A029F585D1DA4B_AdjustorThunk },
	{ 0x06000107, int4_ToString_m80DED0523A2789C5F8121BC126D4A763B4BB6786_AdjustorThunk },
	{ 0x06000108, int4_ToString_m28ABEE07F5EF759E782841D9E095D70AF4C258DC_AdjustorThunk },
	{ 0x0600010A, quaternion__ctor_m8206385CD33D6464E0F60D5527897A05B6357433_AdjustorThunk },
	{ 0x0600010B, quaternion__ctor_mBEFF4B0481D9F5C24B68E3C3E0108EA23AF4ED7D_AdjustorThunk },
	{ 0x0600010E, quaternion_Equals_m74189FB2CB72067DD1997F8B6ED5C507ACAE6775_AdjustorThunk },
	{ 0x0600010F, quaternion_Equals_mBE1D9112B03E995394456618507CB2C7EE3D4011_AdjustorThunk },
	{ 0x06000110, quaternion_GetHashCode_m2296BBBFA8CFBEDE61E65D1CE0DAAB22D1C71929_AdjustorThunk },
	{ 0x06000111, quaternion_ToString_m619F76404EBF0A8D789C97883621ADA1F2116498_AdjustorThunk },
	{ 0x06000112, quaternion_ToString_m9B9ADE4BCEC08DD4E2A3B1566BE125124BF9B057_AdjustorThunk },
	{ 0x06000114, uint2__ctor_m15E587DF7FC128857586EA962472021625CB0727_AdjustorThunk },
	{ 0x06000116, uint2_Equals_m4D6B7E58271563553964C88B0C6C11B11E9DDEFF_AdjustorThunk },
	{ 0x06000117, uint2_Equals_mDC5AF9975359EB05CA526D9D132038CC07BA610C_AdjustorThunk },
	{ 0x06000118, uint2_GetHashCode_m1AA42656371544BEFEF58990F0F5D3375771BDED_AdjustorThunk },
	{ 0x06000119, uint2_ToString_m11C8B65BDF3EE9F3E076886108F29C7085311CD8_AdjustorThunk },
	{ 0x0600011A, uint2_ToString_m82A6C25A6E380499B47B6D45037EC327449303A1_AdjustorThunk },
	{ 0x0600011B, uint3__ctor_m6000FC36B0AD34E3897EBB27D734326F63D5198E_AdjustorThunk },
	{ 0x0600011F, uint3_Equals_m13DA44E1022044FEBBA2066ECD1A8AC1CB53DF91_AdjustorThunk },
	{ 0x06000120, uint3_Equals_mAAD3238F59F9CA833513E94E33A329834CB205FE_AdjustorThunk },
	{ 0x06000121, uint3_GetHashCode_mD983DD667EC5C064E237BFA773C185AB36599B76_AdjustorThunk },
	{ 0x06000122, uint3_ToString_mEADE36DE83569B87E7DF9B37EB4A04357B83B277_AdjustorThunk },
	{ 0x06000123, uint3_ToString_mF1C800E07DAF3C5478742C64CD0C4C76C7E63167_AdjustorThunk },
	{ 0x06000124, uint4__ctor_mE2EF35B487246E995E6B5226C41B2ABF7D695852_AdjustorThunk },
	{ 0x06000128, uint4_Equals_mA01BD6C642C7AA15938773343109B65A7017A8E5_AdjustorThunk },
	{ 0x06000129, uint4_Equals_mA36D75D93A16B63C63FF5556877325DE7209CA0C_AdjustorThunk },
	{ 0x0600012A, uint4_GetHashCode_m0ACFBA8EEEBE72824B814A9147C62BA2DAD67E04_AdjustorThunk },
	{ 0x0600012B, uint4_ToString_m88626950AE1516CF95F414CFC9D3328308CFFD21_AdjustorThunk },
	{ 0x0600012C, uint4_ToString_m89A2419938888294462214E4F1B77BFB2A6D6622_AdjustorThunk },
};
static const int32_t s_InvokerIndices[300] = 
{
	6026,
	5198,
	6027,
	6028,
	4714,
	5835,
	6234,
	6029,
	3993,
	4971,
	4717,
	3845,
	6242,
	6242,
	6030,
	6031,
	6032,
	6033,
	6024,
	6249,
	6251,
	6253,
	6024,
	6248,
	6250,
	6252,
	6163,
	6163,
	6231,
	6239,
	5297,
	5847,
	5318,
	5527,
	5822,
	5826,
	5837,
	5297,
	5318,
	5527,
	5822,
	5826,
	5837,
	5203,
	4833,
	4968,
	5204,
	6168,
	6168,
	6236,
	5531,
	5532,
	5533,
	5527,
	6168,
	6168,
	6168,
	6168,
	6227,
	6229,
	6236,
	6168,
	6168,
	6229,
	6236,
	6168,
	6236,
	5527,
	6168,
	6168,
	6168,
	6168,
	6229,
	5826,
	5837,
	6171,
	6172,
	6173,
	6172,
	6173,
	5826,
	5199,
	5202,
	5192,
	6015,
	6015,
	6015,
	6015,
	6015,
	6045,
	6171,
	6172,
	6171,
	6172,
	6173,
	6035,
	6036,
	6037,
	5837,
	5837,
	6232,
	5838,
	5844,
	6246,
	6247,
	6247,
	5534,
	6247,
	5853,
	5828,
	5828,
	5210,
	5210,
	6034,
	5854,
	6035,
	5211,
	6036,
	4718,
	6037,
	1650,
	3021,
	6226,
	5821,
	5822,
	5822,
	5821,
	3801,
	2708,
	1517,
	2659,
	2570,
	3705,
	3723,
	1103,
	922,
	3062,
	5826,
	5825,
	5824,
	5826,
	5826,
	5826,
	5825,
	5824,
	3802,
	3802,
	3801,
	3801,
	2708,
	1517,
	2660,
	2570,
	3705,
	3723,
	1103,
	6194,
	6228,
	946,
	39,
	5830,
	5831,
	5830,
	2661,
	2570,
	3705,
	3723,
	1103,
	3124,
	6299,
	610,
	944,
	1669,
	1670,
	3119,
	3062,
	3123,
	6234,
	6238,
	5837,
	5836,
	5834,
	5837,
	5836,
	5837,
	5836,
	5837,
	5836,
	5834,
	6236,
	3804,
	3804,
	3804,
	3804,
	3804,
	3804,
	3804,
	3802,
	3117,
	3802,
	3802,
	3802,
	3801,
	3801,
	2708,
	1517,
	2662,
	2570,
	3705,
	3723,
	1103,
	6235,
	6200,
	619,
	4,
	5843,
	5840,
	5844,
	5844,
	5843,
	6242,
	1837,
	2663,
	2570,
	3705,
	3723,
	1103,
	5206,
	6240,
	5207,
	6299,
	1488,
	3116,
	2060,
	2664,
	2570,
	3705,
	3723,
	1103,
	846,
	3021,
	3117,
	5847,
	5847,
	2060,
	2665,
	2570,
	3705,
	3723,
	1103,
	545,
	1673,
	3119,
	6244,
	5850,
	5850,
	3807,
	2060,
	1488,
	2666,
	2570,
	3705,
	3723,
	1103,
	6245,
	610,
	3119,
	6246,
	5852,
	2667,
	2570,
	3705,
	3723,
	1103,
	6299,
	1488,
	5855,
	2668,
	2570,
	3705,
	3723,
	1103,
	846,
	5856,
	5856,
	5856,
	2669,
	2570,
	3705,
	3723,
	1103,
	545,
	5859,
	5859,
	5858,
	2670,
	2570,
	3705,
	3723,
	1103,
};
extern const CustomAttributesCacheGenerator g_Unity_Mathematics_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_Mathematics_CodeGenModule;
const Il2CppCodeGenModule g_Unity_Mathematics_CodeGenModule = 
{
	"Unity.Mathematics.dll",
	300,
	s_methodPointers,
	120,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_Mathematics_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
