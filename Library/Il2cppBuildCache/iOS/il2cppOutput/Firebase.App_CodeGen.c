﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* FutureString_SWIG_CompletionDispatcher_mF70841BDB2D4C1CD323DD8BDB5ACE5BF66D505D2_RuntimeMethod_var;
extern const RuntimeMethod* FutureVoid_SWIG_CompletionDispatcher_m60ECAC83E9887F17ECCAFE1C1DE0340903FB5281_RuntimeMethod_var;
extern const RuntimeMethod* LogUtil_LogMessageFromCallback_m7B4B1B33C46B05A14DE066D91BB2110DF44B8107_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_mD6D4D05834648B9CE669A89E0AD6CD0D32A0EDEB_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_m1D154F67ADBC4A696102A0BAAAE4FF18BC2D8B1C_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_mCB01A40C26F2595EE0928F65A06D942BEF9F881D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m355EE981BB1BAA35BB8E5C5EC2E90625C1C95166_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m99B7D12BD99B0EAE887BC2C4B43366133758A5BA_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mD01159299A641E72ED233ACA5F9049D89B916FEC_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_mE89EF518A4630B7ACD06C6C2E31E6CA3FCB01774_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m917EAD3BCF29EFE9E6450F3420BE2917F60CC2F2_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_mF21809DB109F445315F916048C030A4F86843301_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_m3C9E61FD1588B6CF1950AC28B4CC3599A7E4E16D_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_m8C550DB92EB91F9325BBAF8DDF2A868F082EABCD_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mB184DCD107C95ED1930E66177083BB532FC6E037_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_mE247FE196CCC5A806A4941FC95F6270BAE75653F_RuntimeMethod_var;
extern const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_m0BFC9561278749A88CC8847749084DE5FBCC5E15_RuntimeMethod_var;
extern const RuntimeMethod* SWIGStringHelper_CreateString_m411964D0112A7FBA74A5AA7693C8AC07D24F13BC_RuntimeMethod_var;



// 0x00000001 System.Void Firebase.AppUtilPINVOKE::.cctor()
extern void AppUtilPINVOKE__cctor_mB82716D517E855CA4815CA1B872B0599F1785B19 (void);
// 0x00000002 System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureBase(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_delete_FutureBase_mC0371BBEAC15767EEC2E7FEAFB5261B2D7273129 (void);
// 0x00000003 System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_status(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FutureBase_status_m69559C80656F21136A45099E3C62CBC74C2A1BCD (void);
// 0x00000004 System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_error(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FutureBase_error_mAA54B287095DA9B85322B3CE60E9CAF44D0B9048 (void);
// 0x00000005 System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureBase_error_message(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FutureBase_error_message_m070740D938581C3EA594AD8BA0C3B60016F8D442 (void);
// 0x00000006 System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.FutureString/SWIG_CompletionDelegate,System.Int32)
extern void AppUtilPINVOKE_Firebase_App_FutureString_SWIG_OnCompletion_m1368D28A5452CB27304785D935403D41BFEA542F (void);
// 0x00000007 System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_Firebase_App_FutureString_SWIG_FreeCompletionData_mBF0481C3CFC80DD426C1DCDE5B060F2B04C0D198 (void);
// 0x00000008 System.String Firebase.AppUtilPINVOKE::Firebase_App_FutureString_GetResult(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FutureString_GetResult_m2D16D3BC7FDB19D59DB380CF95AB93452B8D7FAF (void);
// 0x00000009 System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureString(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_delete_FutureString_mBF9996BDDE0EEBECC6AABEAA9C060F856FC69562 (void);
// 0x0000000A System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureVoid_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.FutureVoid/SWIG_CompletionDelegate,System.Int32)
extern void AppUtilPINVOKE_Firebase_App_FutureVoid_SWIG_OnCompletion_m81C4CC3161231C32B1BC7D97ADF7E5BA121416E2 (void);
// 0x0000000B System.Void Firebase.AppUtilPINVOKE::Firebase_App_FutureVoid_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
extern void AppUtilPINVOKE_Firebase_App_FutureVoid_SWIG_FreeCompletionData_mC56638B87DA13B354294E909DC9595BCC4872AFE (void);
// 0x0000000C System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_FutureVoid(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_delete_FutureVoid_mF078672BD07988BD3F2BE972B5A23F688BDD9125 (void);
// 0x0000000D System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_GetDatabaseUrlInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_GetDatabaseUrlInternal_m077E3CF5460258D80A6228EC0E2DE06891889337 (void);
// 0x0000000E System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_AppId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_AppId_get_m0CA0B75BCD22C0B03722ED12289993242ECE447F (void);
// 0x0000000F System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_ApiKey_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ApiKey_get_m976600637E68A5A6304904142D08D67FC0F6A971 (void);
// 0x00000010 System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_MessageSenderId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_MessageSenderId_get_m54741C1B5E0272982A3AF6A9E937A2262054FF8E (void);
// 0x00000011 System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_StorageBucket_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_StorageBucket_get_m446532889980F04EFFDB613515B43DBF907797D8 (void);
// 0x00000012 System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_ProjectId_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ProjectId_get_m0AA2B920BC2C6439CFA52D1043106B3439A9D611 (void);
// 0x00000013 System.String Firebase.AppUtilPINVOKE::Firebase_App_AppOptionsInternal_PackageName_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_AppOptionsInternal_PackageName_get_m7EA8F7B861D1A95AB389D9CE25EF692FC6A7A482 (void);
// 0x00000014 System.Void Firebase.AppUtilPINVOKE::Firebase_App_delete_AppOptionsInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_delete_AppOptionsInternal_mFE72A08CA8C69B69E301D0A1D2247E2D36559994 (void);
// 0x00000015 System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_options(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_options_m2860C63F3B2A12C2358DB5B674B07CAE5EDE4E5D (void);
// 0x00000016 System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_NameInternal_get(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_NameInternal_get_m3FCAAB68F8E03CE3A3079B717D5C596D4561310F (void);
// 0x00000017 System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_CreateInternal__SWIG_0()
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_0_m880301BF18F95C1602A9C5D802DDAE626D45CAD2 (void);
// 0x00000018 System.Void Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_ReleaseReferenceInternal_m97C1DE32BCE04F1AC59A117C6ED30E138E424D26 (void);
// 0x00000019 System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_GetLogLevelInternal()
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_GetLogLevelInternal_m64D61C2F951379C639BE99996C5B8DDA0CF7ABCE (void);
// 0x0000001A System.Void Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_RegisterLibraryInternal(System.String,System.String)
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_RegisterLibraryInternal_mE23204CA5E247273D5F52CBB42F8D708C27C1DC9 (void);
// 0x0000001B System.Void Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_AppSetDefaultConfigPath(System.String)
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_AppSetDefaultConfigPath_mD2E944BB42CC3E9F6F443F3D6DC65CF9E4F639F3 (void);
// 0x0000001C System.String Firebase.AppUtilPINVOKE::Firebase_App_FirebaseApp_DefaultName_get()
extern void AppUtilPINVOKE_Firebase_App_FirebaseApp_DefaultName_get_m13F1175243A022F0686B765261DF2FFC68B1822F (void);
// 0x0000001D System.Void Firebase.AppUtilPINVOKE::Firebase_App_PollCallbacks()
extern void AppUtilPINVOKE_Firebase_App_PollCallbacks_mF25D3634B38B19C7409BCC9A50B3482979F741EA (void);
// 0x0000001E System.Void Firebase.AppUtilPINVOKE::Firebase_App_AppEnableLogCallback(System.Boolean)
extern void AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m10954DADA1EE183C4F6B9CECC784C5395CE441FD (void);
// 0x0000001F System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m5F66E2EA7BCF9B05EE87FEEDDAD08E5F8C75622C (void);
// 0x00000020 System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtilPINVOKE_Firebase_App_SetEnabledAppCallbackByName_m38695B5F0435658A754FFCF6D32C0EB805414864 (void);
// 0x00000021 System.Boolean Firebase.AppUtilPINVOKE::Firebase_App_GetEnabledAppCallbackByName(System.String)
extern void AppUtilPINVOKE_Firebase_App_GetEnabledAppCallbackByName_m39176480A7F163BA61D22646908BB95D4CD6096E (void);
// 0x00000022 System.Void Firebase.AppUtilPINVOKE::Firebase_App_SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtilPINVOKE_Firebase_App_SetLogFunction_m293E0D5D4A2685DBF99526F3D6D59FD39DC418F4 (void);
// 0x00000023 System.Int32 Firebase.AppUtilPINVOKE::Firebase_App_CheckAndroidDependencies()
extern void AppUtilPINVOKE_Firebase_App_CheckAndroidDependencies_m5C0A10F6B6AC96084E7DE73FFB37D250D7092625 (void);
// 0x00000024 System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FixAndroidDependencies()
extern void AppUtilPINVOKE_Firebase_App_FixAndroidDependencies_m80CBA444E7744FA45BC12DA7F391A8068D6C614A (void);
// 0x00000025 System.Void Firebase.AppUtilPINVOKE::Firebase_App_InitializePlayServicesInternal()
extern void AppUtilPINVOKE_Firebase_App_InitializePlayServicesInternal_m0DD10568B070F46C4145D9239C052B1EBEF813D0 (void);
// 0x00000026 System.Void Firebase.AppUtilPINVOKE::Firebase_App_TerminatePlayServicesInternal()
extern void AppUtilPINVOKE_Firebase_App_TerminatePlayServicesInternal_m8958E8F39507FE7C80C2120160DAAC726D6A430B (void);
// 0x00000027 System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureString_SWIGUpcast(System.IntPtr)
extern void AppUtilPINVOKE_Firebase_App_FutureString_SWIGUpcast_mD5C01A2C8CE3419B9108F03822C9C1BEB51E700E (void);
// 0x00000028 System.IntPtr Firebase.AppUtilPINVOKE::Firebase_App_FutureVoid_SWIGUpcast(System.IntPtr)
extern void AppUtilPINVOKE_Firebase_App_FutureVoid_SWIGUpcast_mB8CCA2100141567D297560E8DB1FB6C641FC76EC (void);
// 0x00000029 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.cctor()
extern void SWIGExceptionHelper__cctor_m88D96246E0C2DD75CFC4054F09FA9044A5B6FA90 (void);
// 0x0000002A System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::.ctor()
extern void SWIGExceptionHelper__ctor_m06C48C4611CDA458CA1AF651ED06BF7FF7EDF536 (void);
// 0x0000002B System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m73FDAC85B33251A5207C88493A40F2F24D634676 (void);
// 0x0000002C System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AppUtil(Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
extern void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m8F9C20ECB599940EF0CFCFF5598BD3C3222E2B37 (void);
// 0x0000002D System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
extern void SWIGExceptionHelper_SetPendingApplicationException_mD6D4D05834648B9CE669A89E0AD6CD0D32A0EDEB (void);
// 0x0000002E System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
extern void SWIGExceptionHelper_SetPendingArithmeticException_m99B7D12BD99B0EAE887BC2C4B43366133758A5BA (void);
// 0x0000002F System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
extern void SWIGExceptionHelper_SetPendingDivideByZeroException_mD01159299A641E72ED233ACA5F9049D89B916FEC (void);
// 0x00000030 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
extern void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m917EAD3BCF29EFE9E6450F3420BE2917F60CC2F2 (void);
// 0x00000031 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidCastException_mF21809DB109F445315F916048C030A4F86843301 (void);
// 0x00000032 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
extern void SWIGExceptionHelper_SetPendingInvalidOperationException_m3C9E61FD1588B6CF1950AC28B4CC3599A7E4E16D (void);
// 0x00000033 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
extern void SWIGExceptionHelper_SetPendingIOException_mE89EF518A4630B7ACD06C6C2E31E6CA3FCB01774 (void);
// 0x00000034 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
extern void SWIGExceptionHelper_SetPendingNullReferenceException_m8C550DB92EB91F9325BBAF8DDF2A868F082EABCD (void);
// 0x00000035 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
extern void SWIGExceptionHelper_SetPendingOutOfMemoryException_mB184DCD107C95ED1930E66177083BB532FC6E037 (void);
// 0x00000036 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
extern void SWIGExceptionHelper_SetPendingOverflowException_mE247FE196CCC5A806A4941FC95F6270BAE75653F (void);
// 0x00000037 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
extern void SWIGExceptionHelper_SetPendingSystemException_m0BFC9561278749A88CC8847749084DE5FBCC5E15 (void);
// 0x00000038 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentException_m1D154F67ADBC4A696102A0BAAAE4FF18BC2D8B1C (void);
// 0x00000039 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentNullException_mCB01A40C26F2595EE0928F65A06D942BEF9F881D (void);
// 0x0000003A System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
extern void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m355EE981BB1BAA35BB8E5C5EC2E90625C1C95166 (void);
// 0x0000003B System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionDelegate__ctor_m4E04BD56501AA698F333F3189D232E0DD8BE66A0 (void);
// 0x0000003C System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::Invoke(System.String)
extern void ExceptionDelegate_Invoke_mE907915DC5B6A911DE7F253DF0E0D82F63B23A06 (void);
// 0x0000003D System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ExceptionDelegate_BeginInvoke_m72D31AEE58624296E481B8F6C28EDF28C445F92B (void);
// 0x0000003E System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionDelegate_EndInvoke_mCD778A944D0755D6227785C17547B6F3FCCB9D59 (void);
// 0x0000003F System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
extern void ExceptionArgumentDelegate__ctor_m9B64B0E9472C1DDAA639843324FD57FBCCE07E08 (void);
// 0x00000040 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::Invoke(System.String,System.String)
extern void ExceptionArgumentDelegate_Invoke_mD10622418D792C1CDA2D02B0117C251187C52D74 (void);
// 0x00000041 System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ExceptionArgumentDelegate_BeginInvoke_m0410594AB6ABF10A9740F06B324A5A6C059E39B9 (void);
// 0x00000042 System.Void Firebase.AppUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
extern void ExceptionArgumentDelegate_EndInvoke_mE24172C5085232AD4E4A4EAC36FBF77A79A93C31 (void);
// 0x00000043 System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
extern void SWIGPendingException_get_Pending_m57F7C179B5EFB37003896A5F25F4FBED7DA3D2AD (void);
// 0x00000044 System.Void Firebase.AppUtilPINVOKE/SWIGPendingException::Set(System.Exception)
extern void SWIGPendingException_Set_m3016052808B54728D457EB1D4E8E7306D806098A (void);
// 0x00000045 System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
extern void SWIGPendingException_Retrieve_m62D9AC53AD2901040C0DF7F7800858C07617B6CD (void);
// 0x00000046 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.cctor()
extern void SWIGStringHelper__cctor_m8EE8BC5E0ABB17F0F9D0A2F615EE4987FD86F3B0 (void);
// 0x00000047 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::.ctor()
extern void SWIGStringHelper__ctor_m9F305BAB06F185B49FD5AC05A407928C69D672F6 (void);
// 0x00000048 System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AppUtil(Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
extern void SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m72D311A8F2D513C5602B6F4E7936C2910DBECA15 (void);
// 0x00000049 System.String Firebase.AppUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
extern void SWIGStringHelper_CreateString_m411964D0112A7FBA74A5AA7693C8AC07D24F13BC (void);
// 0x0000004A System.Void Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIGStringDelegate__ctor_mED39AF7AB0675F58D7C5E732BB50C419BF321299 (void);
// 0x0000004B System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::Invoke(System.String)
extern void SWIGStringDelegate_Invoke_mE2D5B14F87E5528B7095C2B08CFD4B10A4926BDF (void);
// 0x0000004C System.IAsyncResult Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void SWIGStringDelegate_BeginInvoke_m071087F52EF4FF4FB5914D2D986CD4607DFDE8E7 (void);
// 0x0000004D System.String Firebase.AppUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::EndInvoke(System.IAsyncResult)
extern void SWIGStringDelegate_EndInvoke_m4A38AD7D0C3965603592F826BC582D78A7FB7AA0 (void);
// 0x0000004E System.Void Firebase.AppUtil::PollCallbacks()
extern void AppUtil_PollCallbacks_m75E222C3BCE3563C9C27265D3AE011E3E342E527 (void);
// 0x0000004F System.Void Firebase.AppUtil::AppEnableLogCallback(System.Boolean)
extern void AppUtil_AppEnableLogCallback_m01A441841A004A6048FCFC012F083BFAA3581C66 (void);
// 0x00000050 System.Void Firebase.AppUtil::SetEnabledAllAppCallbacks(System.Boolean)
extern void AppUtil_SetEnabledAllAppCallbacks_m40DACCE1222844931485D67E3885D9ACD1C31FF2 (void);
// 0x00000051 System.Void Firebase.AppUtil::SetEnabledAppCallbackByName(System.String,System.Boolean)
extern void AppUtil_SetEnabledAppCallbackByName_m59AF9169D18540D471ECB1A999A5F7B67D0B63BC (void);
// 0x00000052 System.Boolean Firebase.AppUtil::GetEnabledAppCallbackByName(System.String)
extern void AppUtil_GetEnabledAppCallbackByName_m4E31F50E6B3C90FF204A4BC57293B0A1C5F81513 (void);
// 0x00000053 System.Void Firebase.AppUtil::SetLogFunction(Firebase.LogUtil/LogMessageDelegate)
extern void AppUtil_SetLogFunction_m9B6FD50FE9307EC41EB21DD82665887C9F6514BB (void);
// 0x00000054 Firebase.GooglePlayServicesAvailability Firebase.AppUtil::CheckAndroidDependencies()
extern void AppUtil_CheckAndroidDependencies_mF6F1264E5DA034CDB3B4A20715419FB36BB09FED (void);
// 0x00000055 System.Threading.Tasks.Task Firebase.AppUtil::FixAndroidDependenciesAsync()
extern void AppUtil_FixAndroidDependenciesAsync_m4E0A5F5046D85FBCEEB21963516269A404D9ACA7 (void);
// 0x00000056 System.Void Firebase.AppUtil::InitializePlayServicesInternal()
extern void AppUtil_InitializePlayServicesInternal_mFDAD6568DBE745E692B38FC49EA137C6FBCB2850 (void);
// 0x00000057 System.Void Firebase.AppUtil::TerminatePlayServicesInternal()
extern void AppUtil_TerminatePlayServicesInternal_mEEDE73F4D1E3F490FFE1329DBD9255A944830B57 (void);
// 0x00000058 System.Void Firebase.FirebaseApp::.ctor(System.IntPtr,System.Boolean)
extern void FirebaseApp__ctor_mB94B4439B6474A9B3420A388F4C847153BB7DA2B (void);
// 0x00000059 System.Void Firebase.FirebaseApp::.cctor()
extern void FirebaseApp__cctor_m65C207A2EB72FE3DF40D35D1018C27881D66EB62 (void);
// 0x0000005A System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
extern void FirebaseApp_getCPtr_mB9BA3F66814A2FC1CCF1355B112F2D1DD4E72E86 (void);
// 0x0000005B System.Void Firebase.FirebaseApp::Finalize()
extern void FirebaseApp_Finalize_mBA9B05FC454D571B021370352E3DAA24A927C964 (void);
// 0x0000005C System.Void Firebase.FirebaseApp::Dispose()
extern void FirebaseApp_Dispose_mD97452CFB97FC00105EB0369582537ED1C457A93 (void);
// 0x0000005D System.Void Firebase.FirebaseApp::TranslateDllNotFoundException(System.Action)
extern void FirebaseApp_TranslateDllNotFoundException_m7AA751F31022C03AA209E73B99646C4D0A17A90D (void);
// 0x0000005E Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
extern void FirebaseApp_get_DefaultInstance_mCC924BAC33B68B03C851ACE856930B839F8267D7 (void);
// 0x0000005F Firebase.FirebaseApp Firebase.FirebaseApp::GetInstance(System.String)
extern void FirebaseApp_GetInstance_mED18364B5B6A567DEDDBFE38F5CE174FF8171196 (void);
// 0x00000060 Firebase.FirebaseApp Firebase.FirebaseApp::Create()
extern void FirebaseApp_Create_m9A391D6EC41EE45EAA3CCAE368F182EAD69791ED (void);
// 0x00000061 System.String Firebase.FirebaseApp::get_Name()
extern void FirebaseApp_get_Name_m5945BBCED21D565E2D871D4CC00D03BB6EDB60B8 (void);
// 0x00000062 Firebase.LogLevel Firebase.FirebaseApp::get_LogLevel()
extern void FirebaseApp_get_LogLevel_mF5210CA8F87660D4B3747792C00C98579142CAAF (void);
// 0x00000063 System.Void Firebase.FirebaseApp::add_AppDisposed(System.EventHandler)
extern void FirebaseApp_add_AppDisposed_m72EFA96351AC82CC8DF3A42ABEFB313B0D9E7D79 (void);
// 0x00000064 System.Void Firebase.FirebaseApp::remove_AppDisposed(System.EventHandler)
extern void FirebaseApp_remove_AppDisposed_m1E390E9068C2247AFF44122B61195EC3627F7812 (void);
// 0x00000065 System.Void Firebase.FirebaseApp::AddReference()
extern void FirebaseApp_AddReference_m1B148D579E5DBB9E6608813E714769757F33142B (void);
// 0x00000066 System.Void Firebase.FirebaseApp::RemoveReference()
extern void FirebaseApp_RemoveReference_mF1E15EE01A41D7674FAFDAB777CF637071F907E0 (void);
// 0x00000067 System.Void Firebase.FirebaseApp::ThrowIfNull()
extern void FirebaseApp_ThrowIfNull_m408BCFBB4ED87F4FF412F374897F083E87897315 (void);
// 0x00000068 System.Void Firebase.FirebaseApp::InitializeAppUtilCallbacks()
extern void FirebaseApp_InitializeAppUtilCallbacks_m598537A6D9F6327DAA4E5398BB645C5B7CA81064 (void);
// 0x00000069 System.Void Firebase.FirebaseApp::OnAllAppsDestroyed()
extern void FirebaseApp_OnAllAppsDestroyed_m6D2ADF3AFBC105388CB7D63FA431CF5BAF9FA94D (void);
// 0x0000006A System.Uri Firebase.FirebaseApp::UrlStringToUri(System.String)
extern void FirebaseApp_UrlStringToUri_mA6A34CF111210B3561C2450F87640A23BC9BCA51 (void);
// 0x0000006B System.Object Firebase.FirebaseApp::WeakReferenceGetTarget(System.WeakReference)
extern void FirebaseApp_WeakReferenceGetTarget_m4F195CE3072FD67139BDC76BAB407034828B2C3C (void);
// 0x0000006C System.Boolean Firebase.FirebaseApp::InitializeCrashlyticsIfPresent()
extern void FirebaseApp_InitializeCrashlyticsIfPresent_m28C2D06F823206C421BF3D241335A8FA4D26A91B (void);
// 0x0000006D Firebase.FirebaseApp Firebase.FirebaseApp::CreateAndTrack(Firebase.FirebaseApp/CreateDelegate,Firebase.FirebaseApp)
extern void FirebaseApp_CreateAndTrack_m7FC684DE29AF92053981EB1323B38DBC4D34AC26 (void);
// 0x0000006E System.Void Firebase.FirebaseApp::SetCheckDependenciesThread(System.Int32)
extern void FirebaseApp_SetCheckDependenciesThread_m4DFA2531E30E77FFA43EBC45E23CD306E8BA8764 (void);
// 0x0000006F System.Void Firebase.FirebaseApp::ThrowIfCheckDependenciesRunning()
extern void FirebaseApp_ThrowIfCheckDependenciesRunning_m6452309AE10D2E53CCA8457A4D8A181B164D05EA (void);
// 0x00000070 System.Boolean Firebase.FirebaseApp::IsCheckDependenciesRunning()
extern void FirebaseApp_IsCheckDependenciesRunning_m35488E6B310C9CF1BC00E4464028166723C3F078 (void);
// 0x00000071 System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::CheckDependenciesAsync()
extern void FirebaseApp_CheckDependenciesAsync_mB7A747897A064C84E4D84585DFEAF74A38D38F43 (void);
// 0x00000072 System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::CheckAndFixDependenciesAsync()
extern void FirebaseApp_CheckAndFixDependenciesAsync_m268BF2FC002D4D4CC247EF2636CDF7EBFF043009 (void);
// 0x00000073 Firebase.DependencyStatus Firebase.FirebaseApp::CheckDependencies()
extern void FirebaseApp_CheckDependencies_mCD719D29867B855EBA5F74102286BB9A44FCB946 (void);
// 0x00000074 Firebase.DependencyStatus Firebase.FirebaseApp::CheckDependenciesInternal()
extern void FirebaseApp_CheckDependenciesInternal_m3C1FA1603F994655D1C49443FFF8053420F98509 (void);
// 0x00000075 System.Threading.Tasks.Task Firebase.FirebaseApp::FixDependenciesAsync()
extern void FirebaseApp_FixDependenciesAsync_mDBBC901E36336AE90E590E9DACD32FE1CCDB79E1 (void);
// 0x00000076 System.Void Firebase.FirebaseApp::ResetDefaultAppCPtr()
extern void FirebaseApp_ResetDefaultAppCPtr_m23794116100108FA137A11654DDF8C74A5C5F453 (void);
// 0x00000077 Firebase.AppOptions Firebase.FirebaseApp::get_Options()
extern void FirebaseApp_get_Options_m98E649DAB99D126DC16D6E78682F933B02049180 (void);
// 0x00000078 Firebase.AppOptionsInternal Firebase.FirebaseApp::options()
extern void FirebaseApp_options_m246546AF14ED87EA3EDB8095EA99C5AA2396E8B4 (void);
// 0x00000079 System.String Firebase.FirebaseApp::get_NameInternal()
extern void FirebaseApp_get_NameInternal_m89112573EBE5801863EA50B17FBAE20CBE855C75 (void);
// 0x0000007A Firebase.FirebaseApp Firebase.FirebaseApp::CreateInternal()
extern void FirebaseApp_CreateInternal_m7C9DA4F992AD0B435BE57A04D5BE3537AB2A0233 (void);
// 0x0000007B System.Void Firebase.FirebaseApp::ReleaseReferenceInternal(Firebase.FirebaseApp)
extern void FirebaseApp_ReleaseReferenceInternal_m251EC6B33F516335E844BC8CBC7531205D41C934 (void);
// 0x0000007C System.Void Firebase.FirebaseApp::RegisterLibraryInternal(System.String,System.String)
extern void FirebaseApp_RegisterLibraryInternal_mEE0AF7BC02554570144A1305095A22FC07602164 (void);
// 0x0000007D System.Void Firebase.FirebaseApp::AppSetDefaultConfigPath(System.String)
extern void FirebaseApp_AppSetDefaultConfigPath_m55D859EEC14CA1FFEF3DC3422F5CE8C1328E097F (void);
// 0x0000007E System.String Firebase.FirebaseApp::get_DefaultName()
extern void FirebaseApp_get_DefaultName_mFAACACB5590298942D3A75F2A6BC53BD6531963C (void);
// 0x0000007F Firebase.FirebaseApp Firebase.FirebaseApp::<Create>m__0()
extern void FirebaseApp_U3CCreateU3Em__0_m0C29C0EE9E16A55E80148E3233D6491114F11CA6 (void);
// 0x00000080 System.Boolean Firebase.FirebaseApp::<CreateAndTrack>m__1()
extern void FirebaseApp_U3CCreateAndTrackU3Em__1_m9C34C3C32726662CBE52EDB06D70C5E197AF62A5 (void);
// 0x00000081 Firebase.DependencyStatus Firebase.FirebaseApp::<CheckDependenciesAsync>m__2()
extern void FirebaseApp_U3CCheckDependenciesAsyncU3Em__2_m7418E08620989B76203932B94ED9C94E3C928ACF (void);
// 0x00000082 System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::<CheckAndFixDependenciesAsync>m__3(System.Threading.Tasks.Task`1<Firebase.DependencyStatus>)
extern void FirebaseApp_U3CCheckAndFixDependenciesAsyncU3Em__3_mC58ECF5CAA7305CB261496EACAC79A62C810C25B (void);
// 0x00000083 System.Threading.Tasks.Task`1<Firebase.DependencyStatus> Firebase.FirebaseApp::<CheckAndFixDependenciesAsync>m__4(System.Threading.Tasks.Task)
extern void FirebaseApp_U3CCheckAndFixDependenciesAsyncU3Em__4_m2FAFE0753DFF5C2D5E1CF45BDB2870220F7E4484 (void);
// 0x00000084 System.Void Firebase.FirebaseApp/EnableModuleParams::.ctor(System.String,System.String,System.Boolean)
extern void EnableModuleParams__ctor_mC9619ED6B8BE82D305359BB53BDE6274A8A2D2C2 (void);
// 0x00000085 System.String Firebase.FirebaseApp/EnableModuleParams::get_CppModuleName()
extern void EnableModuleParams_get_CppModuleName_mE57521DAC3F8972C81AFBC72DB70FE79A9F946B3 (void);
// 0x00000086 System.Void Firebase.FirebaseApp/EnableModuleParams::set_CppModuleName(System.String)
extern void EnableModuleParams_set_CppModuleName_m780B77AD33765B83D0675C02876BB379B9EFCCEB (void);
// 0x00000087 System.String Firebase.FirebaseApp/EnableModuleParams::get_CSharpClassName()
extern void EnableModuleParams_get_CSharpClassName_m5C21BC47A020FE24984E1A282267CE62CF09080B (void);
// 0x00000088 System.Void Firebase.FirebaseApp/EnableModuleParams::set_CSharpClassName(System.String)
extern void EnableModuleParams_set_CSharpClassName_mB1413BCF93E8A3B658798ED556E586C47981F018 (void);
// 0x00000089 System.Boolean Firebase.FirebaseApp/EnableModuleParams::get_AlwaysEnable()
extern void EnableModuleParams_get_AlwaysEnable_m76B3B18100019E68E79EA0A0B320B1EAE0AB8260 (void);
// 0x0000008A System.Void Firebase.FirebaseApp/EnableModuleParams::set_AlwaysEnable(System.Boolean)
extern void EnableModuleParams_set_AlwaysEnable_m38C379905DD5810F629E35AD34DD0F677990ACFE (void);
// 0x0000008B System.Void Firebase.FirebaseApp/CreateDelegate::.ctor(System.Object,System.IntPtr)
extern void CreateDelegate__ctor_m9B61AF9F4EFF9CCA9FC10B8BDB5E8AD7130E4DE1 (void);
// 0x0000008C Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::Invoke()
extern void CreateDelegate_Invoke_m9FC551133A4F9301FB4F107B90F7C98A66F95BE9 (void);
// 0x0000008D System.IAsyncResult Firebase.FirebaseApp/CreateDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void CreateDelegate_BeginInvoke_mE55A9209FBD926992A08A1F0C0DCAC9083CC66A5 (void);
// 0x0000008E Firebase.FirebaseApp Firebase.FirebaseApp/CreateDelegate::EndInvoke(System.IAsyncResult)
extern void CreateDelegate_EndInvoke_mA8FE3700C59A150FAA37B17BDAA7A44319C1116E (void);
// 0x0000008F System.Void Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey2::.ctor()
extern void U3CCheckDependenciesU3Ec__AnonStorey2__ctor_m0251932201CC1594FA83952DAA5A7DDE7DF0CDF6 (void);
// 0x00000090 System.Void Firebase.FirebaseApp/<CheckDependencies>c__AnonStorey2::<>m__0()
extern void U3CCheckDependenciesU3Ec__AnonStorey2_U3CU3Em__0_mECC3E61E5A6E1E53D59A57324902DABE019526D2 (void);
// 0x00000091 System.Void Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey3::.ctor()
extern void U3CFixDependenciesAsyncU3Ec__AnonStorey3__ctor_m973162F7E6495FC5DE2DCABF7315FE26018F5682 (void);
// 0x00000092 System.Void Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey3::<>m__0()
extern void U3CFixDependenciesAsyncU3Ec__AnonStorey3_U3CU3Em__0_mA13297C364FBCCC7D7AA0EE96EA755838C9D0FFD (void);
// 0x00000093 System.Void Firebase.FirebaseApp/<FixDependenciesAsync>c__AnonStorey3::<>m__1(System.Threading.Tasks.Task)
extern void U3CFixDependenciesAsyncU3Ec__AnonStorey3_U3CU3Em__1_mD41D7E8967C69FA3765509BAD33BF548FB64092D (void);
// 0x00000094 System.Void Firebase.AppOptionsInternal::.ctor(System.IntPtr,System.Boolean)
extern void AppOptionsInternal__ctor_mBA3D4999FA58E5D0E10F920846372ACD71CDEDD0 (void);
// 0x00000095 System.Void Firebase.AppOptionsInternal::Finalize()
extern void AppOptionsInternal_Finalize_m98E79714FD18F1FBD487CE442C7CAFCECFA572A5 (void);
// 0x00000096 System.Void Firebase.AppOptionsInternal::Dispose()
extern void AppOptionsInternal_Dispose_m923FE7958D4317ED8825DBAD4254B533825AEAA1 (void);
// 0x00000097 System.Uri Firebase.AppOptionsInternal::get_DatabaseUrl()
extern void AppOptionsInternal_get_DatabaseUrl_mA1508C095A0862C3DC8C1A6C728106653B0EDE60 (void);
// 0x00000098 System.String Firebase.AppOptionsInternal::GetDatabaseUrlInternal()
extern void AppOptionsInternal_GetDatabaseUrlInternal_m000B201A09C953887FC4428FCFDC097E22451C6B (void);
// 0x00000099 System.String Firebase.AppOptionsInternal::get_AppId()
extern void AppOptionsInternal_get_AppId_mD0FF298D8468A9664BF58EBE95CDCA487CAE20AA (void);
// 0x0000009A System.String Firebase.AppOptionsInternal::get_ApiKey()
extern void AppOptionsInternal_get_ApiKey_m7E4EEEE1100FAE72251593632619092B7E675090 (void);
// 0x0000009B System.String Firebase.AppOptionsInternal::get_MessageSenderId()
extern void AppOptionsInternal_get_MessageSenderId_mC448D7030D9EC1FAA418F608C81ADC4ADB999946 (void);
// 0x0000009C System.String Firebase.AppOptionsInternal::get_StorageBucket()
extern void AppOptionsInternal_get_StorageBucket_mB90A0A959007D89009EDB6A11CF27F1E0A4357C3 (void);
// 0x0000009D System.String Firebase.AppOptionsInternal::get_ProjectId()
extern void AppOptionsInternal_get_ProjectId_m3FFDDF11A39C948719985A512855D7C9740143CB (void);
// 0x0000009E System.String Firebase.AppOptionsInternal::get_PackageName()
extern void AppOptionsInternal_get_PackageName_m091E48C6CA71A5662C6DFC640F32FA7AA810D248 (void);
// 0x0000009F System.Void Firebase.FutureVoid::.ctor(System.IntPtr,System.Boolean)
extern void FutureVoid__ctor_m1360132BEDC4A7668F93C0D1EB79CE3E28E94597 (void);
// 0x000000A0 System.Void Firebase.FutureVoid::Finalize()
extern void FutureVoid_Finalize_mB465E6923BAB39AFFCBCBB52DDF12FB02166E0B5 (void);
// 0x000000A1 System.Void Firebase.FutureVoid::Dispose()
extern void FutureVoid_Dispose_mFF268F835BFCFFFBF90B730ABBB16B4E438D9A7E (void);
// 0x000000A2 System.Threading.Tasks.Task Firebase.FutureVoid::GetTask(Firebase.FutureVoid)
extern void FutureVoid_GetTask_m99313F8ED4D21B3440FA37F24EDE6206F6B0E3BF (void);
// 0x000000A3 System.Void Firebase.FutureVoid::ThrowIfDisposed()
extern void FutureVoid_ThrowIfDisposed_mA7C1D4055AA1AC53502961F16F6449C4647792AF (void);
// 0x000000A4 System.Void Firebase.FutureVoid::SetOnCompletionCallback(Firebase.FutureVoid/Action)
extern void FutureVoid_SetOnCompletionCallback_mA6429D52596940DA40C03180D7020560EAB0C14B (void);
// 0x000000A5 System.Void Firebase.FutureVoid::SetCompletionData(System.IntPtr)
extern void FutureVoid_SetCompletionData_mF298BA528990E45DD85839CA0E91130876DAF491 (void);
// 0x000000A6 System.Void Firebase.FutureVoid::SWIG_CompletionDispatcher(System.Int32)
extern void FutureVoid_SWIG_CompletionDispatcher_m60ECAC83E9887F17ECCAFE1C1DE0340903FB5281 (void);
// 0x000000A7 System.IntPtr Firebase.FutureVoid::SWIG_OnCompletion(Firebase.FutureVoid/SWIG_CompletionDelegate,System.Int32)
extern void FutureVoid_SWIG_OnCompletion_m48F9542CC9CFAF8D061DB014E2791A8C4D6EEBF3 (void);
// 0x000000A8 System.Void Firebase.FutureVoid::SWIG_FreeCompletionData(System.IntPtr)
extern void FutureVoid_SWIG_FreeCompletionData_mFEA68F96EA671BB05C45C11DDC981A965E7C4ACF (void);
// 0x000000A9 System.Void Firebase.FutureVoid::.cctor()
extern void FutureVoid__cctor_mDCA344A3A0F77964E3A10A61E864E8DA62070ADD (void);
// 0x000000AA System.Void Firebase.FutureVoid/Action::.ctor(System.Object,System.IntPtr)
extern void Action__ctor_mBB9FA88AEDA150A5C03EF7CC69344846B1A4FD22 (void);
// 0x000000AB System.Void Firebase.FutureVoid/Action::Invoke()
extern void Action_Invoke_m8F270518E4B4FBDC595EA69BEC37C94DE19EBB3E (void);
// 0x000000AC System.IAsyncResult Firebase.FutureVoid/Action::BeginInvoke(System.AsyncCallback,System.Object)
extern void Action_BeginInvoke_m880F277B9978E98E5E80FB4B6F6B92085AA9DC10 (void);
// 0x000000AD System.Void Firebase.FutureVoid/Action::EndInvoke(System.IAsyncResult)
extern void Action_EndInvoke_m25D93B13687F5F2BF6E3C41FA7E3BC79CDA289AB (void);
// 0x000000AE System.Void Firebase.FutureVoid/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIG_CompletionDelegate__ctor_m93C9C83EF65339E2A9134FA2E79AA1895FE881A2 (void);
// 0x000000AF System.Void Firebase.FutureVoid/SWIG_CompletionDelegate::Invoke(System.Int32)
extern void SWIG_CompletionDelegate_Invoke_m331EF656E6CAEB2B61DFD5CF6A68D4045F486CE5 (void);
// 0x000000B0 System.IAsyncResult Firebase.FutureVoid/SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void SWIG_CompletionDelegate_BeginInvoke_mADCF9246A7BD1637ABAA9876C8C16EBFD318C78C (void);
// 0x000000B1 System.Void Firebase.FutureVoid/SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern void SWIG_CompletionDelegate_EndInvoke_m3D04ABBCBB71B86479A9252A5B3D39C7A15AF54C (void);
// 0x000000B2 System.Void Firebase.FutureVoid/<GetTask>c__AnonStorey0::.ctor()
extern void U3CGetTaskU3Ec__AnonStorey0__ctor_m7F3052D39B10F90854FA21CA6AFA941CC6CC1307 (void);
// 0x000000B3 System.Void Firebase.FutureVoid/<GetTask>c__AnonStorey0::<>m__0()
extern void U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_mBDC99E2B07B94C3D13918A68BD7E4B85AA737008 (void);
// 0x000000B4 System.Void Firebase.FutureString::.ctor(System.IntPtr,System.Boolean)
extern void FutureString__ctor_mEE94FE29EDA6856652FFBFE07AF06BD33B5BBC82 (void);
// 0x000000B5 System.Void Firebase.FutureString::Finalize()
extern void FutureString_Finalize_mB6305C6EB07C7F50E313ACD220D24DD26F2D0E59 (void);
// 0x000000B6 System.Void Firebase.FutureString::Dispose()
extern void FutureString_Dispose_m47F695224312122CF024A272296BEF5328B1A3FA (void);
// 0x000000B7 System.Threading.Tasks.Task`1<System.String> Firebase.FutureString::GetTask(Firebase.FutureString)
extern void FutureString_GetTask_m7CF681D446F480F4930D6B4A406CF9CE12B4A761 (void);
// 0x000000B8 System.Void Firebase.FutureString::ThrowIfDisposed()
extern void FutureString_ThrowIfDisposed_m1F2D5089BB1C888631DF053DFD0F38F4345FB5F3 (void);
// 0x000000B9 System.Void Firebase.FutureString::SetOnCompletionCallback(Firebase.FutureString/Action)
extern void FutureString_SetOnCompletionCallback_m878C505E25F5712C619B311473DC5DD3CCDB3D68 (void);
// 0x000000BA System.Void Firebase.FutureString::SetCompletionData(System.IntPtr)
extern void FutureString_SetCompletionData_mBC4FCA2EF3886CD720A27FFDAEC7F4D4A03F1CF9 (void);
// 0x000000BB System.Void Firebase.FutureString::SWIG_CompletionDispatcher(System.Int32)
extern void FutureString_SWIG_CompletionDispatcher_mF70841BDB2D4C1CD323DD8BDB5ACE5BF66D505D2 (void);
// 0x000000BC System.IntPtr Firebase.FutureString::SWIG_OnCompletion(Firebase.FutureString/SWIG_CompletionDelegate,System.Int32)
extern void FutureString_SWIG_OnCompletion_m73D0FB17B8063EEBF36900B519B00EBD1DFF22E6 (void);
// 0x000000BD System.Void Firebase.FutureString::SWIG_FreeCompletionData(System.IntPtr)
extern void FutureString_SWIG_FreeCompletionData_mA0E9ABB8F418A39F2135AB385995207964B74C66 (void);
// 0x000000BE System.String Firebase.FutureString::GetResult()
extern void FutureString_GetResult_m44B49349D344B988B4BD2F8782BE5B81EDE12B20 (void);
// 0x000000BF System.Void Firebase.FutureString::.cctor()
extern void FutureString__cctor_mFEA2D128868417773638967DA7ED28A09ACC36C6 (void);
// 0x000000C0 System.Void Firebase.FutureString/Action::.ctor(System.Object,System.IntPtr)
extern void Action__ctor_m5EB9223B37DA74BFAA497D2BFC061034B38B45F2 (void);
// 0x000000C1 System.Void Firebase.FutureString/Action::Invoke()
extern void Action_Invoke_m8AEC3442D3825511735DB7CCF3F68124CCD3976F (void);
// 0x000000C2 System.IAsyncResult Firebase.FutureString/Action::BeginInvoke(System.AsyncCallback,System.Object)
extern void Action_BeginInvoke_m5F1427DF1B48FD24F83E4A0536A3E5476AF22E3D (void);
// 0x000000C3 System.Void Firebase.FutureString/Action::EndInvoke(System.IAsyncResult)
extern void Action_EndInvoke_m6953546D1860D0FFCEBCAD104E8E7870E16DEC30 (void);
// 0x000000C4 System.Void Firebase.FutureString/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
extern void SWIG_CompletionDelegate__ctor_mA94A734983B630AD0AD8815F10FC080BCCC45DE8 (void);
// 0x000000C5 System.Void Firebase.FutureString/SWIG_CompletionDelegate::Invoke(System.Int32)
extern void SWIG_CompletionDelegate_Invoke_m8D12A0B28F8226371F614F927ABFEE5199049153 (void);
// 0x000000C6 System.IAsyncResult Firebase.FutureString/SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern void SWIG_CompletionDelegate_BeginInvoke_m14AB81912E26ECFE7E5F7CBDD7CD7BDDF3332949 (void);
// 0x000000C7 System.Void Firebase.FutureString/SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
extern void SWIG_CompletionDelegate_EndInvoke_m6615391FBE0AA5F5D8C25138C98D36EEEFAF5327 (void);
// 0x000000C8 System.Void Firebase.FutureString/<GetTask>c__AnonStorey0::.ctor()
extern void U3CGetTaskU3Ec__AnonStorey0__ctor_mC4E736186B15B13755932456B84F22137E2144DC (void);
// 0x000000C9 System.Void Firebase.FutureString/<GetTask>c__AnonStorey0::<>m__0()
extern void U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_mA5779D80D53EEFDC6F36BF5283B81F326398DCB2 (void);
// 0x000000CA System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
extern void FutureBase__ctor_m69C88EC69B422C5752B2E249303D92F649B8C8AC (void);
// 0x000000CB System.Void Firebase.FutureBase::Finalize()
extern void FutureBase_Finalize_m02E7843DEC68FBDDCA2B009E905FE4657C2B04AC (void);
// 0x000000CC System.Void Firebase.FutureBase::Dispose()
extern void FutureBase_Dispose_m2C0FDC1F8EF2499A1E52D6CFEA94348388784BDB (void);
// 0x000000CD Firebase.FutureStatus Firebase.FutureBase::status()
extern void FutureBase_status_m478C1E6AF62FB15C218A7C422CF5DC8CA1486CAA (void);
// 0x000000CE System.Int32 Firebase.FutureBase::error()
extern void FutureBase_error_mBA8200B272D3DB91D1EE78ECE0A10AAB84771C03 (void);
// 0x000000CF System.String Firebase.FutureBase::error_message()
extern void FutureBase_error_message_m6E9B30EF5EC5EE999B91077E60E3B96978DE4774 (void);
// 0x000000D0 System.Void Firebase.Internal.TaskCompletionSourceCompat`1::SetExceptionInternal(System.Threading.Tasks.TaskCompletionSource`1<T>,System.AggregateException)
// 0x000000D1 System.Void Firebase.Internal.TaskCompletionSourceCompat`1::SetException(System.Threading.Tasks.TaskCompletionSource`1<T>,System.AggregateException)
// 0x000000D2 System.Void Firebase.Platform.FirebaseAppUtils::.ctor()
extern void FirebaseAppUtils__ctor_m69CA1CBCD58CB3128DF35E9560A3D1D38005845F (void);
// 0x000000D3 Firebase.Platform.FirebaseAppUtils Firebase.Platform.FirebaseAppUtils::get_Instance()
extern void FirebaseAppUtils_get_Instance_m79B79F13BD6329A947B640DB886DC398439A90BF (void);
// 0x000000D4 System.Void Firebase.Platform.FirebaseAppUtils::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtils_TranslateDllNotFoundException_m6E4E3109BD0827EE0E675EE9CD50E9C6EE343356 (void);
// 0x000000D5 System.Void Firebase.Platform.FirebaseAppUtils::PollCallbacks()
extern void FirebaseAppUtils_PollCallbacks_m0A88A069EC5477A1CC9BFCEDC55DED431EE64E91 (void);
// 0x000000D6 Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtils::GetLogLevel()
extern void FirebaseAppUtils_GetLogLevel_m9E036F13631BCE81463AA50592FBCD54E9CB09D7 (void);
// 0x000000D7 System.Void Firebase.Platform.FirebaseAppUtils::.cctor()
extern void FirebaseAppUtils__cctor_mD68087540805827D81757968316C3971FF48B537 (void);
// 0x000000D8 System.String Firebase.VersionInfo::get_SdkVersion()
extern void VersionInfo_get_SdkVersion_m98A33172434D6EA468C4FF32AC941045D88DCF8B (void);
// 0x000000D9 System.Void Firebase.MonoPInvokeCallbackAttribute::.ctor(System.Type)
extern void MonoPInvokeCallbackAttribute__ctor_mD5BA102663DCE67244B79EF374E5641E765AB5CB (void);
// 0x000000DA System.Void Firebase.LogUtil::.cctor()
extern void LogUtil__cctor_m3A18E1D17E9D3E61E2A8B33C51742249F708BB0A (void);
// 0x000000DB System.Void Firebase.LogUtil::.ctor()
extern void LogUtil__ctor_mE6F41CDC7EFF92D76E3D07B5F8350BF7D5A4983D (void);
// 0x000000DC System.Void Firebase.LogUtil::InitializeLogging()
extern void LogUtil_InitializeLogging_m05046FDF1759F0A851931F7FCF743FAA24BDB434 (void);
// 0x000000DD Firebase.Platform.PlatformLogLevel Firebase.LogUtil::ConvertLogLevel(Firebase.LogLevel)
extern void LogUtil_ConvertLogLevel_mD765D2120AA5F1681CFFCB822C189152704565FC (void);
// 0x000000DE System.Void Firebase.LogUtil::LogMessage(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessage_mC36C5CCA27AFA6A1773D0A993DAC4A0C609F6C8B (void);
// 0x000000DF System.Void Firebase.LogUtil::LogMessageFromCallback(Firebase.LogLevel,System.String)
extern void LogUtil_LogMessageFromCallback_m7B4B1B33C46B05A14DE066D91BB2110DF44B8107 (void);
// 0x000000E0 System.Void Firebase.LogUtil::Finalize()
extern void LogUtil_Finalize_mD288B9870DD8D7AF744044513FCA62F9AB42A85C (void);
// 0x000000E1 System.Void Firebase.LogUtil::Dispose()
extern void LogUtil_Dispose_mB12D003420083CAA79A613F44A635DA5418C989D (void);
// 0x000000E2 System.Void Firebase.LogUtil::Dispose(System.Boolean)
extern void LogUtil_Dispose_m3E431D1105B6EBDC3183FA2B55AA7608607BBC6D (void);
// 0x000000E3 System.Void Firebase.LogUtil::<LogUtil>m__0(System.Object,System.EventArgs)
extern void LogUtil_U3CLogUtilU3Em__0_mE0E18AA95FE82B679A079D3C7208ED52CC513A15 (void);
// 0x000000E4 System.Void Firebase.LogUtil/LogMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void LogMessageDelegate__ctor_mEBA3FFB53CCE522DBB1B5571A5623A649E6643F0 (void);
// 0x000000E5 System.Void Firebase.LogUtil/LogMessageDelegate::Invoke(Firebase.LogLevel,System.String)
extern void LogMessageDelegate_Invoke_mB54C38843065556AF65D1E42C9DDC9AFAFA5C5E8 (void);
// 0x000000E6 System.IAsyncResult Firebase.LogUtil/LogMessageDelegate::BeginInvoke(Firebase.LogLevel,System.String,System.AsyncCallback,System.Object)
extern void LogMessageDelegate_BeginInvoke_m9A9B00026484A1266F2E8E1101699C83D2755654 (void);
// 0x000000E7 System.Void Firebase.LogUtil/LogMessageDelegate::EndInvoke(System.IAsyncResult)
extern void LogMessageDelegate_EndInvoke_m13C568B3D481DF381F37AAEF2FA598025FDB8C95 (void);
// 0x000000E8 System.Void Firebase.InitializationException::.ctor(Firebase.InitResult)
extern void InitializationException__ctor_mCE6B67DCF469CF7CC9CC8A5C6C2E6CAC73D6366F (void);
// 0x000000E9 System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String)
extern void InitializationException__ctor_m4F5649529A9F0863B359E63E74F8B5331F138A14 (void);
// 0x000000EA System.Void Firebase.InitializationException::.ctor(Firebase.InitResult,System.String,System.Exception)
extern void InitializationException__ctor_m56641135A502F6D70F771A69157A0D674EB99DD3 (void);
// 0x000000EB Firebase.InitResult Firebase.InitializationException::get_InitResult()
extern void InitializationException_get_InitResult_m597B4C9A381618A9F73238F131D9AF3433880CF7 (void);
// 0x000000EC System.Void Firebase.InitializationException::set_InitResult(Firebase.InitResult)
extern void InitializationException_set_InitResult_mD6E68B41830F64CB54B3BF7FFE915263D8AD8E34 (void);
// 0x000000ED System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
extern void FirebaseException__ctor_mCB919E722DF4F366C4E0D9278716CBED5DD8907C (void);
// 0x000000EE System.Int32 Firebase.FirebaseException::get_ErrorCode()
extern void FirebaseException_get_ErrorCode_mA7E9CF1AB755AEEE49032E4AC4A7A253C3735F60 (void);
// 0x000000EF System.Void Firebase.FirebaseException::set_ErrorCode(System.Int32)
extern void FirebaseException_set_ErrorCode_mE216C4C0EBAACC7ADA04ED328DEC1474680F9B5A (void);
// 0x000000F0 System.String Firebase.ErrorMessages::get_DependencyNotFoundErrorMessage()
extern void ErrorMessages_get_DependencyNotFoundErrorMessage_m93703910D97FC22ECA3B89EEF8681C6F29989106 (void);
// 0x000000F1 System.String Firebase.ErrorMessages::get_DllNotFoundExceptionErrorMessage()
extern void ErrorMessages_get_DllNotFoundExceptionErrorMessage_m493D1E37E274C2D633EB65991F5B26102E3F6595 (void);
// 0x000000F2 System.Void Firebase.ErrorMessages::.cctor()
extern void ErrorMessages__cctor_mC64C25A9DFBF5FA4B8784803560444FA59FDD7DC (void);
// 0x000000F3 System.Void Firebase.AppOptions::.ctor(Firebase.AppOptionsInternal)
extern void AppOptions__ctor_m4B51654E54F4784FA7A21269288E4DB9A11DDEC4 (void);
// 0x000000F4 System.Void Firebase.AppOptions::Dispose()
extern void AppOptions_Dispose_m50CC70BB62373D28C47303E1F3AE451E483F2D99 (void);
// 0x000000F5 System.Void Firebase.AppOptions::set_DatabaseUrl(System.Uri)
extern void AppOptions_set_DatabaseUrl_mB06D163A37683E7B36BA8268035ABEB005A87FB9 (void);
// 0x000000F6 System.Void Firebase.AppOptions::set_AppId(System.String)
extern void AppOptions_set_AppId_m4B5BA1B974CB6388A1ECDE5170945EA7D0202501 (void);
// 0x000000F7 System.Void Firebase.AppOptions::set_ApiKey(System.String)
extern void AppOptions_set_ApiKey_m42DFAA546F61385DBDAD31B93E12A3A23B7FC468 (void);
// 0x000000F8 System.Void Firebase.AppOptions::set_MessageSenderId(System.String)
extern void AppOptions_set_MessageSenderId_m778712C8ED6C08E1E0E3FCBC23BE1CFAD0336835 (void);
// 0x000000F9 System.String Firebase.AppOptions::get_StorageBucket()
extern void AppOptions_get_StorageBucket_m63489CFD73EA950BCF40D828F23658740C8B35FB (void);
// 0x000000FA System.Void Firebase.AppOptions::set_StorageBucket(System.String)
extern void AppOptions_set_StorageBucket_mE44829D9949FFF08FDD4ED72F8D16259A9809373 (void);
// 0x000000FB System.Void Firebase.AppOptions::set_ProjectId(System.String)
extern void AppOptions_set_ProjectId_m56FE1094D175376A26E44F102A482B772CAADDA4 (void);
// 0x000000FC System.Void Firebase.AppOptions::set_PackageName(System.String)
extern void AppOptions_set_PackageName_mE97F6696842F899D803C0B0B02A557F6C47B01C6 (void);
static Il2CppMethodPointer s_methodPointers[252] = 
{
	AppUtilPINVOKE__cctor_mB82716D517E855CA4815CA1B872B0599F1785B19,
	AppUtilPINVOKE_Firebase_App_delete_FutureBase_mC0371BBEAC15767EEC2E7FEAFB5261B2D7273129,
	AppUtilPINVOKE_Firebase_App_FutureBase_status_m69559C80656F21136A45099E3C62CBC74C2A1BCD,
	AppUtilPINVOKE_Firebase_App_FutureBase_error_mAA54B287095DA9B85322B3CE60E9CAF44D0B9048,
	AppUtilPINVOKE_Firebase_App_FutureBase_error_message_m070740D938581C3EA594AD8BA0C3B60016F8D442,
	AppUtilPINVOKE_Firebase_App_FutureString_SWIG_OnCompletion_m1368D28A5452CB27304785D935403D41BFEA542F,
	AppUtilPINVOKE_Firebase_App_FutureString_SWIG_FreeCompletionData_mBF0481C3CFC80DD426C1DCDE5B060F2B04C0D198,
	AppUtilPINVOKE_Firebase_App_FutureString_GetResult_m2D16D3BC7FDB19D59DB380CF95AB93452B8D7FAF,
	AppUtilPINVOKE_Firebase_App_delete_FutureString_mBF9996BDDE0EEBECC6AABEAA9C060F856FC69562,
	AppUtilPINVOKE_Firebase_App_FutureVoid_SWIG_OnCompletion_m81C4CC3161231C32B1BC7D97ADF7E5BA121416E2,
	AppUtilPINVOKE_Firebase_App_FutureVoid_SWIG_FreeCompletionData_mC56638B87DA13B354294E909DC9595BCC4872AFE,
	AppUtilPINVOKE_Firebase_App_delete_FutureVoid_mF078672BD07988BD3F2BE972B5A23F688BDD9125,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_GetDatabaseUrlInternal_m077E3CF5460258D80A6228EC0E2DE06891889337,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_AppId_get_m0CA0B75BCD22C0B03722ED12289993242ECE447F,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ApiKey_get_m976600637E68A5A6304904142D08D67FC0F6A971,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_MessageSenderId_get_m54741C1B5E0272982A3AF6A9E937A2262054FF8E,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_StorageBucket_get_m446532889980F04EFFDB613515B43DBF907797D8,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_ProjectId_get_m0AA2B920BC2C6439CFA52D1043106B3439A9D611,
	AppUtilPINVOKE_Firebase_App_AppOptionsInternal_PackageName_get_m7EA8F7B861D1A95AB389D9CE25EF692FC6A7A482,
	AppUtilPINVOKE_Firebase_App_delete_AppOptionsInternal_mFE72A08CA8C69B69E301D0A1D2247E2D36559994,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_options_m2860C63F3B2A12C2358DB5B674B07CAE5EDE4E5D,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_NameInternal_get_m3FCAAB68F8E03CE3A3079B717D5C596D4561310F,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_CreateInternal__SWIG_0_m880301BF18F95C1602A9C5D802DDAE626D45CAD2,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_ReleaseReferenceInternal_m97C1DE32BCE04F1AC59A117C6ED30E138E424D26,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_GetLogLevelInternal_m64D61C2F951379C639BE99996C5B8DDA0CF7ABCE,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_RegisterLibraryInternal_mE23204CA5E247273D5F52CBB42F8D708C27C1DC9,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_AppSetDefaultConfigPath_mD2E944BB42CC3E9F6F443F3D6DC65CF9E4F639F3,
	AppUtilPINVOKE_Firebase_App_FirebaseApp_DefaultName_get_m13F1175243A022F0686B765261DF2FFC68B1822F,
	AppUtilPINVOKE_Firebase_App_PollCallbacks_mF25D3634B38B19C7409BCC9A50B3482979F741EA,
	AppUtilPINVOKE_Firebase_App_AppEnableLogCallback_m10954DADA1EE183C4F6B9CECC784C5395CE441FD,
	AppUtilPINVOKE_Firebase_App_SetEnabledAllAppCallbacks_m5F66E2EA7BCF9B05EE87FEEDDAD08E5F8C75622C,
	AppUtilPINVOKE_Firebase_App_SetEnabledAppCallbackByName_m38695B5F0435658A754FFCF6D32C0EB805414864,
	AppUtilPINVOKE_Firebase_App_GetEnabledAppCallbackByName_m39176480A7F163BA61D22646908BB95D4CD6096E,
	AppUtilPINVOKE_Firebase_App_SetLogFunction_m293E0D5D4A2685DBF99526F3D6D59FD39DC418F4,
	AppUtilPINVOKE_Firebase_App_CheckAndroidDependencies_m5C0A10F6B6AC96084E7DE73FFB37D250D7092625,
	AppUtilPINVOKE_Firebase_App_FixAndroidDependencies_m80CBA444E7744FA45BC12DA7F391A8068D6C614A,
	AppUtilPINVOKE_Firebase_App_InitializePlayServicesInternal_m0DD10568B070F46C4145D9239C052B1EBEF813D0,
	AppUtilPINVOKE_Firebase_App_TerminatePlayServicesInternal_m8958E8F39507FE7C80C2120160DAAC726D6A430B,
	AppUtilPINVOKE_Firebase_App_FutureString_SWIGUpcast_mD5C01A2C8CE3419B9108F03822C9C1BEB51E700E,
	AppUtilPINVOKE_Firebase_App_FutureVoid_SWIGUpcast_mB8CCA2100141567D297560E8DB1FB6C641FC76EC,
	SWIGExceptionHelper__cctor_m88D96246E0C2DD75CFC4054F09FA9044A5B6FA90,
	SWIGExceptionHelper__ctor_m06C48C4611CDA458CA1AF651ED06BF7FF7EDF536,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AppUtil_m73FDAC85B33251A5207C88493A40F2F24D634676,
	SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AppUtil_m8F9C20ECB599940EF0CFCFF5598BD3C3222E2B37,
	SWIGExceptionHelper_SetPendingApplicationException_mD6D4D05834648B9CE669A89E0AD6CD0D32A0EDEB,
	SWIGExceptionHelper_SetPendingArithmeticException_m99B7D12BD99B0EAE887BC2C4B43366133758A5BA,
	SWIGExceptionHelper_SetPendingDivideByZeroException_mD01159299A641E72ED233ACA5F9049D89B916FEC,
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m917EAD3BCF29EFE9E6450F3420BE2917F60CC2F2,
	SWIGExceptionHelper_SetPendingInvalidCastException_mF21809DB109F445315F916048C030A4F86843301,
	SWIGExceptionHelper_SetPendingInvalidOperationException_m3C9E61FD1588B6CF1950AC28B4CC3599A7E4E16D,
	SWIGExceptionHelper_SetPendingIOException_mE89EF518A4630B7ACD06C6C2E31E6CA3FCB01774,
	SWIGExceptionHelper_SetPendingNullReferenceException_m8C550DB92EB91F9325BBAF8DDF2A868F082EABCD,
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mB184DCD107C95ED1930E66177083BB532FC6E037,
	SWIGExceptionHelper_SetPendingOverflowException_mE247FE196CCC5A806A4941FC95F6270BAE75653F,
	SWIGExceptionHelper_SetPendingSystemException_m0BFC9561278749A88CC8847749084DE5FBCC5E15,
	SWIGExceptionHelper_SetPendingArgumentException_m1D154F67ADBC4A696102A0BAAAE4FF18BC2D8B1C,
	SWIGExceptionHelper_SetPendingArgumentNullException_mCB01A40C26F2595EE0928F65A06D942BEF9F881D,
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m355EE981BB1BAA35BB8E5C5EC2E90625C1C95166,
	ExceptionDelegate__ctor_m4E04BD56501AA698F333F3189D232E0DD8BE66A0,
	ExceptionDelegate_Invoke_mE907915DC5B6A911DE7F253DF0E0D82F63B23A06,
	ExceptionDelegate_BeginInvoke_m72D31AEE58624296E481B8F6C28EDF28C445F92B,
	ExceptionDelegate_EndInvoke_mCD778A944D0755D6227785C17547B6F3FCCB9D59,
	ExceptionArgumentDelegate__ctor_m9B64B0E9472C1DDAA639843324FD57FBCCE07E08,
	ExceptionArgumentDelegate_Invoke_mD10622418D792C1CDA2D02B0117C251187C52D74,
	ExceptionArgumentDelegate_BeginInvoke_m0410594AB6ABF10A9740F06B324A5A6C059E39B9,
	ExceptionArgumentDelegate_EndInvoke_mE24172C5085232AD4E4A4EAC36FBF77A79A93C31,
	SWIGPendingException_get_Pending_m57F7C179B5EFB37003896A5F25F4FBED7DA3D2AD,
	SWIGPendingException_Set_m3016052808B54728D457EB1D4E8E7306D806098A,
	SWIGPendingException_Retrieve_m62D9AC53AD2901040C0DF7F7800858C07617B6CD,
	SWIGStringHelper__cctor_m8EE8BC5E0ABB17F0F9D0A2F615EE4987FD86F3B0,
	SWIGStringHelper__ctor_m9F305BAB06F185B49FD5AC05A407928C69D672F6,
	SWIGStringHelper_SWIGRegisterStringCallback_AppUtil_m72D311A8F2D513C5602B6F4E7936C2910DBECA15,
	SWIGStringHelper_CreateString_m411964D0112A7FBA74A5AA7693C8AC07D24F13BC,
	SWIGStringDelegate__ctor_mED39AF7AB0675F58D7C5E732BB50C419BF321299,
	SWIGStringDelegate_Invoke_mE2D5B14F87E5528B7095C2B08CFD4B10A4926BDF,
	SWIGStringDelegate_BeginInvoke_m071087F52EF4FF4FB5914D2D986CD4607DFDE8E7,
	SWIGStringDelegate_EndInvoke_m4A38AD7D0C3965603592F826BC582D78A7FB7AA0,
	AppUtil_PollCallbacks_m75E222C3BCE3563C9C27265D3AE011E3E342E527,
	AppUtil_AppEnableLogCallback_m01A441841A004A6048FCFC012F083BFAA3581C66,
	AppUtil_SetEnabledAllAppCallbacks_m40DACCE1222844931485D67E3885D9ACD1C31FF2,
	AppUtil_SetEnabledAppCallbackByName_m59AF9169D18540D471ECB1A999A5F7B67D0B63BC,
	AppUtil_GetEnabledAppCallbackByName_m4E31F50E6B3C90FF204A4BC57293B0A1C5F81513,
	AppUtil_SetLogFunction_m9B6FD50FE9307EC41EB21DD82665887C9F6514BB,
	AppUtil_CheckAndroidDependencies_mF6F1264E5DA034CDB3B4A20715419FB36BB09FED,
	AppUtil_FixAndroidDependenciesAsync_m4E0A5F5046D85FBCEEB21963516269A404D9ACA7,
	AppUtil_InitializePlayServicesInternal_mFDAD6568DBE745E692B38FC49EA137C6FBCB2850,
	AppUtil_TerminatePlayServicesInternal_mEEDE73F4D1E3F490FFE1329DBD9255A944830B57,
	FirebaseApp__ctor_mB94B4439B6474A9B3420A388F4C847153BB7DA2B,
	FirebaseApp__cctor_m65C207A2EB72FE3DF40D35D1018C27881D66EB62,
	FirebaseApp_getCPtr_mB9BA3F66814A2FC1CCF1355B112F2D1DD4E72E86,
	FirebaseApp_Finalize_mBA9B05FC454D571B021370352E3DAA24A927C964,
	FirebaseApp_Dispose_mD97452CFB97FC00105EB0369582537ED1C457A93,
	FirebaseApp_TranslateDllNotFoundException_m7AA751F31022C03AA209E73B99646C4D0A17A90D,
	FirebaseApp_get_DefaultInstance_mCC924BAC33B68B03C851ACE856930B839F8267D7,
	FirebaseApp_GetInstance_mED18364B5B6A567DEDDBFE38F5CE174FF8171196,
	FirebaseApp_Create_m9A391D6EC41EE45EAA3CCAE368F182EAD69791ED,
	FirebaseApp_get_Name_m5945BBCED21D565E2D871D4CC00D03BB6EDB60B8,
	FirebaseApp_get_LogLevel_mF5210CA8F87660D4B3747792C00C98579142CAAF,
	FirebaseApp_add_AppDisposed_m72EFA96351AC82CC8DF3A42ABEFB313B0D9E7D79,
	FirebaseApp_remove_AppDisposed_m1E390E9068C2247AFF44122B61195EC3627F7812,
	FirebaseApp_AddReference_m1B148D579E5DBB9E6608813E714769757F33142B,
	FirebaseApp_RemoveReference_mF1E15EE01A41D7674FAFDAB777CF637071F907E0,
	FirebaseApp_ThrowIfNull_m408BCFBB4ED87F4FF412F374897F083E87897315,
	FirebaseApp_InitializeAppUtilCallbacks_m598537A6D9F6327DAA4E5398BB645C5B7CA81064,
	FirebaseApp_OnAllAppsDestroyed_m6D2ADF3AFBC105388CB7D63FA431CF5BAF9FA94D,
	FirebaseApp_UrlStringToUri_mA6A34CF111210B3561C2450F87640A23BC9BCA51,
	FirebaseApp_WeakReferenceGetTarget_m4F195CE3072FD67139BDC76BAB407034828B2C3C,
	FirebaseApp_InitializeCrashlyticsIfPresent_m28C2D06F823206C421BF3D241335A8FA4D26A91B,
	FirebaseApp_CreateAndTrack_m7FC684DE29AF92053981EB1323B38DBC4D34AC26,
	FirebaseApp_SetCheckDependenciesThread_m4DFA2531E30E77FFA43EBC45E23CD306E8BA8764,
	FirebaseApp_ThrowIfCheckDependenciesRunning_m6452309AE10D2E53CCA8457A4D8A181B164D05EA,
	FirebaseApp_IsCheckDependenciesRunning_m35488E6B310C9CF1BC00E4464028166723C3F078,
	FirebaseApp_CheckDependenciesAsync_mB7A747897A064C84E4D84585DFEAF74A38D38F43,
	FirebaseApp_CheckAndFixDependenciesAsync_m268BF2FC002D4D4CC247EF2636CDF7EBFF043009,
	FirebaseApp_CheckDependencies_mCD719D29867B855EBA5F74102286BB9A44FCB946,
	FirebaseApp_CheckDependenciesInternal_m3C1FA1603F994655D1C49443FFF8053420F98509,
	FirebaseApp_FixDependenciesAsync_mDBBC901E36336AE90E590E9DACD32FE1CCDB79E1,
	FirebaseApp_ResetDefaultAppCPtr_m23794116100108FA137A11654DDF8C74A5C5F453,
	FirebaseApp_get_Options_m98E649DAB99D126DC16D6E78682F933B02049180,
	FirebaseApp_options_m246546AF14ED87EA3EDB8095EA99C5AA2396E8B4,
	FirebaseApp_get_NameInternal_m89112573EBE5801863EA50B17FBAE20CBE855C75,
	FirebaseApp_CreateInternal_m7C9DA4F992AD0B435BE57A04D5BE3537AB2A0233,
	FirebaseApp_ReleaseReferenceInternal_m251EC6B33F516335E844BC8CBC7531205D41C934,
	FirebaseApp_RegisterLibraryInternal_mEE0AF7BC02554570144A1305095A22FC07602164,
	FirebaseApp_AppSetDefaultConfigPath_m55D859EEC14CA1FFEF3DC3422F5CE8C1328E097F,
	FirebaseApp_get_DefaultName_mFAACACB5590298942D3A75F2A6BC53BD6531963C,
	FirebaseApp_U3CCreateU3Em__0_m0C29C0EE9E16A55E80148E3233D6491114F11CA6,
	FirebaseApp_U3CCreateAndTrackU3Em__1_m9C34C3C32726662CBE52EDB06D70C5E197AF62A5,
	FirebaseApp_U3CCheckDependenciesAsyncU3Em__2_m7418E08620989B76203932B94ED9C94E3C928ACF,
	FirebaseApp_U3CCheckAndFixDependenciesAsyncU3Em__3_mC58ECF5CAA7305CB261496EACAC79A62C810C25B,
	FirebaseApp_U3CCheckAndFixDependenciesAsyncU3Em__4_m2FAFE0753DFF5C2D5E1CF45BDB2870220F7E4484,
	EnableModuleParams__ctor_mC9619ED6B8BE82D305359BB53BDE6274A8A2D2C2,
	EnableModuleParams_get_CppModuleName_mE57521DAC3F8972C81AFBC72DB70FE79A9F946B3,
	EnableModuleParams_set_CppModuleName_m780B77AD33765B83D0675C02876BB379B9EFCCEB,
	EnableModuleParams_get_CSharpClassName_m5C21BC47A020FE24984E1A282267CE62CF09080B,
	EnableModuleParams_set_CSharpClassName_mB1413BCF93E8A3B658798ED556E586C47981F018,
	EnableModuleParams_get_AlwaysEnable_m76B3B18100019E68E79EA0A0B320B1EAE0AB8260,
	EnableModuleParams_set_AlwaysEnable_m38C379905DD5810F629E35AD34DD0F677990ACFE,
	CreateDelegate__ctor_m9B61AF9F4EFF9CCA9FC10B8BDB5E8AD7130E4DE1,
	CreateDelegate_Invoke_m9FC551133A4F9301FB4F107B90F7C98A66F95BE9,
	CreateDelegate_BeginInvoke_mE55A9209FBD926992A08A1F0C0DCAC9083CC66A5,
	CreateDelegate_EndInvoke_mA8FE3700C59A150FAA37B17BDAA7A44319C1116E,
	U3CCheckDependenciesU3Ec__AnonStorey2__ctor_m0251932201CC1594FA83952DAA5A7DDE7DF0CDF6,
	U3CCheckDependenciesU3Ec__AnonStorey2_U3CU3Em__0_mECC3E61E5A6E1E53D59A57324902DABE019526D2,
	U3CFixDependenciesAsyncU3Ec__AnonStorey3__ctor_m973162F7E6495FC5DE2DCABF7315FE26018F5682,
	U3CFixDependenciesAsyncU3Ec__AnonStorey3_U3CU3Em__0_mA13297C364FBCCC7D7AA0EE96EA755838C9D0FFD,
	U3CFixDependenciesAsyncU3Ec__AnonStorey3_U3CU3Em__1_mD41D7E8967C69FA3765509BAD33BF548FB64092D,
	AppOptionsInternal__ctor_mBA3D4999FA58E5D0E10F920846372ACD71CDEDD0,
	AppOptionsInternal_Finalize_m98E79714FD18F1FBD487CE442C7CAFCECFA572A5,
	AppOptionsInternal_Dispose_m923FE7958D4317ED8825DBAD4254B533825AEAA1,
	AppOptionsInternal_get_DatabaseUrl_mA1508C095A0862C3DC8C1A6C728106653B0EDE60,
	AppOptionsInternal_GetDatabaseUrlInternal_m000B201A09C953887FC4428FCFDC097E22451C6B,
	AppOptionsInternal_get_AppId_mD0FF298D8468A9664BF58EBE95CDCA487CAE20AA,
	AppOptionsInternal_get_ApiKey_m7E4EEEE1100FAE72251593632619092B7E675090,
	AppOptionsInternal_get_MessageSenderId_mC448D7030D9EC1FAA418F608C81ADC4ADB999946,
	AppOptionsInternal_get_StorageBucket_mB90A0A959007D89009EDB6A11CF27F1E0A4357C3,
	AppOptionsInternal_get_ProjectId_m3FFDDF11A39C948719985A512855D7C9740143CB,
	AppOptionsInternal_get_PackageName_m091E48C6CA71A5662C6DFC640F32FA7AA810D248,
	FutureVoid__ctor_m1360132BEDC4A7668F93C0D1EB79CE3E28E94597,
	FutureVoid_Finalize_mB465E6923BAB39AFFCBCBB52DDF12FB02166E0B5,
	FutureVoid_Dispose_mFF268F835BFCFFFBF90B730ABBB16B4E438D9A7E,
	FutureVoid_GetTask_m99313F8ED4D21B3440FA37F24EDE6206F6B0E3BF,
	FutureVoid_ThrowIfDisposed_mA7C1D4055AA1AC53502961F16F6449C4647792AF,
	FutureVoid_SetOnCompletionCallback_mA6429D52596940DA40C03180D7020560EAB0C14B,
	FutureVoid_SetCompletionData_mF298BA528990E45DD85839CA0E91130876DAF491,
	FutureVoid_SWIG_CompletionDispatcher_m60ECAC83E9887F17ECCAFE1C1DE0340903FB5281,
	FutureVoid_SWIG_OnCompletion_m48F9542CC9CFAF8D061DB014E2791A8C4D6EEBF3,
	FutureVoid_SWIG_FreeCompletionData_mFEA68F96EA671BB05C45C11DDC981A965E7C4ACF,
	FutureVoid__cctor_mDCA344A3A0F77964E3A10A61E864E8DA62070ADD,
	Action__ctor_mBB9FA88AEDA150A5C03EF7CC69344846B1A4FD22,
	Action_Invoke_m8F270518E4B4FBDC595EA69BEC37C94DE19EBB3E,
	Action_BeginInvoke_m880F277B9978E98E5E80FB4B6F6B92085AA9DC10,
	Action_EndInvoke_m25D93B13687F5F2BF6E3C41FA7E3BC79CDA289AB,
	SWIG_CompletionDelegate__ctor_m93C9C83EF65339E2A9134FA2E79AA1895FE881A2,
	SWIG_CompletionDelegate_Invoke_m331EF656E6CAEB2B61DFD5CF6A68D4045F486CE5,
	SWIG_CompletionDelegate_BeginInvoke_mADCF9246A7BD1637ABAA9876C8C16EBFD318C78C,
	SWIG_CompletionDelegate_EndInvoke_m3D04ABBCBB71B86479A9252A5B3D39C7A15AF54C,
	U3CGetTaskU3Ec__AnonStorey0__ctor_m7F3052D39B10F90854FA21CA6AFA941CC6CC1307,
	U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_mBDC99E2B07B94C3D13918A68BD7E4B85AA737008,
	FutureString__ctor_mEE94FE29EDA6856652FFBFE07AF06BD33B5BBC82,
	FutureString_Finalize_mB6305C6EB07C7F50E313ACD220D24DD26F2D0E59,
	FutureString_Dispose_m47F695224312122CF024A272296BEF5328B1A3FA,
	FutureString_GetTask_m7CF681D446F480F4930D6B4A406CF9CE12B4A761,
	FutureString_ThrowIfDisposed_m1F2D5089BB1C888631DF053DFD0F38F4345FB5F3,
	FutureString_SetOnCompletionCallback_m878C505E25F5712C619B311473DC5DD3CCDB3D68,
	FutureString_SetCompletionData_mBC4FCA2EF3886CD720A27FFDAEC7F4D4A03F1CF9,
	FutureString_SWIG_CompletionDispatcher_mF70841BDB2D4C1CD323DD8BDB5ACE5BF66D505D2,
	FutureString_SWIG_OnCompletion_m73D0FB17B8063EEBF36900B519B00EBD1DFF22E6,
	FutureString_SWIG_FreeCompletionData_mA0E9ABB8F418A39F2135AB385995207964B74C66,
	FutureString_GetResult_m44B49349D344B988B4BD2F8782BE5B81EDE12B20,
	FutureString__cctor_mFEA2D128868417773638967DA7ED28A09ACC36C6,
	Action__ctor_m5EB9223B37DA74BFAA497D2BFC061034B38B45F2,
	Action_Invoke_m8AEC3442D3825511735DB7CCF3F68124CCD3976F,
	Action_BeginInvoke_m5F1427DF1B48FD24F83E4A0536A3E5476AF22E3D,
	Action_EndInvoke_m6953546D1860D0FFCEBCAD104E8E7870E16DEC30,
	SWIG_CompletionDelegate__ctor_mA94A734983B630AD0AD8815F10FC080BCCC45DE8,
	SWIG_CompletionDelegate_Invoke_m8D12A0B28F8226371F614F927ABFEE5199049153,
	SWIG_CompletionDelegate_BeginInvoke_m14AB81912E26ECFE7E5F7CBDD7CD7BDDF3332949,
	SWIG_CompletionDelegate_EndInvoke_m6615391FBE0AA5F5D8C25138C98D36EEEFAF5327,
	U3CGetTaskU3Ec__AnonStorey0__ctor_mC4E736186B15B13755932456B84F22137E2144DC,
	U3CGetTaskU3Ec__AnonStorey0_U3CU3Em__0_mA5779D80D53EEFDC6F36BF5283B81F326398DCB2,
	FutureBase__ctor_m69C88EC69B422C5752B2E249303D92F649B8C8AC,
	FutureBase_Finalize_m02E7843DEC68FBDDCA2B009E905FE4657C2B04AC,
	FutureBase_Dispose_m2C0FDC1F8EF2499A1E52D6CFEA94348388784BDB,
	FutureBase_status_m478C1E6AF62FB15C218A7C422CF5DC8CA1486CAA,
	FutureBase_error_mBA8200B272D3DB91D1EE78ECE0A10AAB84771C03,
	FutureBase_error_message_m6E9B30EF5EC5EE999B91077E60E3B96978DE4774,
	NULL,
	NULL,
	FirebaseAppUtils__ctor_m69CA1CBCD58CB3128DF35E9560A3D1D38005845F,
	FirebaseAppUtils_get_Instance_m79B79F13BD6329A947B640DB886DC398439A90BF,
	FirebaseAppUtils_TranslateDllNotFoundException_m6E4E3109BD0827EE0E675EE9CD50E9C6EE343356,
	FirebaseAppUtils_PollCallbacks_m0A88A069EC5477A1CC9BFCEDC55DED431EE64E91,
	FirebaseAppUtils_GetLogLevel_m9E036F13631BCE81463AA50592FBCD54E9CB09D7,
	FirebaseAppUtils__cctor_mD68087540805827D81757968316C3971FF48B537,
	VersionInfo_get_SdkVersion_m98A33172434D6EA468C4FF32AC941045D88DCF8B,
	MonoPInvokeCallbackAttribute__ctor_mD5BA102663DCE67244B79EF374E5641E765AB5CB,
	LogUtil__cctor_m3A18E1D17E9D3E61E2A8B33C51742249F708BB0A,
	LogUtil__ctor_mE6F41CDC7EFF92D76E3D07B5F8350BF7D5A4983D,
	LogUtil_InitializeLogging_m05046FDF1759F0A851931F7FCF743FAA24BDB434,
	LogUtil_ConvertLogLevel_mD765D2120AA5F1681CFFCB822C189152704565FC,
	LogUtil_LogMessage_mC36C5CCA27AFA6A1773D0A993DAC4A0C609F6C8B,
	LogUtil_LogMessageFromCallback_m7B4B1B33C46B05A14DE066D91BB2110DF44B8107,
	LogUtil_Finalize_mD288B9870DD8D7AF744044513FCA62F9AB42A85C,
	LogUtil_Dispose_mB12D003420083CAA79A613F44A635DA5418C989D,
	LogUtil_Dispose_m3E431D1105B6EBDC3183FA2B55AA7608607BBC6D,
	LogUtil_U3CLogUtilU3Em__0_mE0E18AA95FE82B679A079D3C7208ED52CC513A15,
	LogMessageDelegate__ctor_mEBA3FFB53CCE522DBB1B5571A5623A649E6643F0,
	LogMessageDelegate_Invoke_mB54C38843065556AF65D1E42C9DDC9AFAFA5C5E8,
	LogMessageDelegate_BeginInvoke_m9A9B00026484A1266F2E8E1101699C83D2755654,
	LogMessageDelegate_EndInvoke_m13C568B3D481DF381F37AAEF2FA598025FDB8C95,
	InitializationException__ctor_mCE6B67DCF469CF7CC9CC8A5C6C2E6CAC73D6366F,
	InitializationException__ctor_m4F5649529A9F0863B359E63E74F8B5331F138A14,
	InitializationException__ctor_m56641135A502F6D70F771A69157A0D674EB99DD3,
	InitializationException_get_InitResult_m597B4C9A381618A9F73238F131D9AF3433880CF7,
	InitializationException_set_InitResult_mD6E68B41830F64CB54B3BF7FFE915263D8AD8E34,
	FirebaseException__ctor_mCB919E722DF4F366C4E0D9278716CBED5DD8907C,
	FirebaseException_get_ErrorCode_mA7E9CF1AB755AEEE49032E4AC4A7A253C3735F60,
	FirebaseException_set_ErrorCode_mE216C4C0EBAACC7ADA04ED328DEC1474680F9B5A,
	ErrorMessages_get_DependencyNotFoundErrorMessage_m93703910D97FC22ECA3B89EEF8681C6F29989106,
	ErrorMessages_get_DllNotFoundExceptionErrorMessage_m493D1E37E274C2D633EB65991F5B26102E3F6595,
	ErrorMessages__cctor_mC64C25A9DFBF5FA4B8784803560444FA59FDD7DC,
	AppOptions__ctor_m4B51654E54F4784FA7A21269288E4DB9A11DDEC4,
	AppOptions_Dispose_m50CC70BB62373D28C47303E1F3AE451E483F2D99,
	AppOptions_set_DatabaseUrl_mB06D163A37683E7B36BA8268035ABEB005A87FB9,
	AppOptions_set_AppId_m4B5BA1B974CB6388A1ECDE5170945EA7D0202501,
	AppOptions_set_ApiKey_m42DFAA546F61385DBDAD31B93E12A3A23B7FC468,
	AppOptions_set_MessageSenderId_m778712C8ED6C08E1E0E3FCBC23BE1CFAD0336835,
	AppOptions_get_StorageBucket_m63489CFD73EA950BCF40D828F23658740C8B35FB,
	AppOptions_set_StorageBucket_mE44829D9949FFF08FDD4ED72F8D16259A9809373,
	AppOptions_set_ProjectId_m56FE1094D175376A26E44F102A482B772CAADDA4,
	AppOptions_set_PackageName_mE97F6696842F899D803C0B0B02A557F6C47B01C6,
};
static const int32_t s_InvokerIndices[252] = 
{
	6299,
	6207,
	6013,
	6013,
	6106,
	4867,
	5680,
	6106,
	6207,
	4867,
	5680,
	6207,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6106,
	6207,
	6056,
	6106,
	6273,
	6207,
	6271,
	5784,
	6212,
	6278,
	6299,
	6214,
	6214,
	5788,
	6145,
	6212,
	6271,
	6273,
	6299,
	6299,
	6059,
	6059,
	6299,
	3780,
	3859,
	5183,
	6212,
	6212,
	6212,
	6212,
	6212,
	6212,
	6212,
	6212,
	6212,
	6212,
	6212,
	5784,
	5784,
	5784,
	1614,
	3038,
	707,
	3038,
	1614,
	1617,
	430,
	3038,
	6287,
	6212,
	6278,
	6299,
	3780,
	6212,
	6113,
	1614,
	2196,
	707,
	2196,
	6299,
	6214,
	6214,
	5788,
	6145,
	6212,
	6271,
	6278,
	6299,
	6299,
	1599,
	6299,
	5999,
	3780,
	3780,
	6212,
	6278,
	6113,
	6278,
	3723,
	6271,
	3038,
	3038,
	3780,
	3780,
	3780,
	6299,
	6299,
	6113,
	6113,
	6287,
	5395,
	6208,
	6299,
	6287,
	6278,
	6278,
	6271,
	6271,
	6278,
	6299,
	3723,
	3723,
	3723,
	6278,
	6212,
	5784,
	6212,
	6278,
	6278,
	6287,
	6271,
	6113,
	6113,
	889,
	3723,
	3038,
	3723,
	3038,
	3749,
	3060,
	1614,
	3723,
	1103,
	2196,
	3780,
	3780,
	3780,
	3780,
	6212,
	1599,
	3780,
	3780,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	1599,
	3780,
	3780,
	6113,
	3780,
	3038,
	3023,
	6208,
	1078,
	3023,
	6299,
	1614,
	3780,
	1103,
	3038,
	1614,
	3021,
	693,
	3038,
	3780,
	3780,
	1599,
	3780,
	3780,
	6113,
	3780,
	3038,
	3023,
	6208,
	1078,
	3023,
	3723,
	6299,
	1614,
	3780,
	1103,
	3038,
	1614,
	3021,
	693,
	3038,
	3780,
	3780,
	1599,
	3780,
	3780,
	3705,
	3705,
	3723,
	-1,
	-1,
	3780,
	6278,
	3038,
	3780,
	3705,
	6299,
	6278,
	3038,
	6299,
	3780,
	6299,
	6015,
	5686,
	5686,
	3780,
	3780,
	3060,
	1617,
	1614,
	1502,
	412,
	3038,
	3021,
	1502,
	855,
	3705,
	3021,
	1502,
	3705,
	3021,
	6278,
	6278,
	6299,
	3038,
	3780,
	3038,
	3038,
	3038,
	3038,
	3723,
	3038,
	3038,
	3038,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[18] = 
{
	{ 0x0600002D, 27,  (void**)&SWIGExceptionHelper_SetPendingApplicationException_mD6D4D05834648B9CE669A89E0AD6CD0D32A0EDEB_RuntimeMethod_var, 0 },
	{ 0x0600002E, 31,  (void**)&SWIGExceptionHelper_SetPendingArithmeticException_m99B7D12BD99B0EAE887BC2C4B43366133758A5BA_RuntimeMethod_var, 0 },
	{ 0x0600002F, 32,  (void**)&SWIGExceptionHelper_SetPendingDivideByZeroException_mD01159299A641E72ED233ACA5F9049D89B916FEC_RuntimeMethod_var, 0 },
	{ 0x06000030, 34,  (void**)&SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m917EAD3BCF29EFE9E6450F3420BE2917F60CC2F2_RuntimeMethod_var, 0 },
	{ 0x06000031, 35,  (void**)&SWIGExceptionHelper_SetPendingInvalidCastException_mF21809DB109F445315F916048C030A4F86843301_RuntimeMethod_var, 0 },
	{ 0x06000032, 36,  (void**)&SWIGExceptionHelper_SetPendingInvalidOperationException_m3C9E61FD1588B6CF1950AC28B4CC3599A7E4E16D_RuntimeMethod_var, 0 },
	{ 0x06000033, 33,  (void**)&SWIGExceptionHelper_SetPendingIOException_mE89EF518A4630B7ACD06C6C2E31E6CA3FCB01774_RuntimeMethod_var, 0 },
	{ 0x06000034, 37,  (void**)&SWIGExceptionHelper_SetPendingNullReferenceException_m8C550DB92EB91F9325BBAF8DDF2A868F082EABCD_RuntimeMethod_var, 0 },
	{ 0x06000035, 38,  (void**)&SWIGExceptionHelper_SetPendingOutOfMemoryException_mB184DCD107C95ED1930E66177083BB532FC6E037_RuntimeMethod_var, 0 },
	{ 0x06000036, 39,  (void**)&SWIGExceptionHelper_SetPendingOverflowException_mE247FE196CCC5A806A4941FC95F6270BAE75653F_RuntimeMethod_var, 0 },
	{ 0x06000037, 40,  (void**)&SWIGExceptionHelper_SetPendingSystemException_m0BFC9561278749A88CC8847749084DE5FBCC5E15_RuntimeMethod_var, 0 },
	{ 0x06000038, 28,  (void**)&SWIGExceptionHelper_SetPendingArgumentException_m1D154F67ADBC4A696102A0BAAAE4FF18BC2D8B1C_RuntimeMethod_var, 0 },
	{ 0x06000039, 29,  (void**)&SWIGExceptionHelper_SetPendingArgumentNullException_mCB01A40C26F2595EE0928F65A06D942BEF9F881D_RuntimeMethod_var, 0 },
	{ 0x0600003A, 30,  (void**)&SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m355EE981BB1BAA35BB8E5C5EC2E90625C1C95166_RuntimeMethod_var, 0 },
	{ 0x06000049, 41,  (void**)&SWIGStringHelper_CreateString_m411964D0112A7FBA74A5AA7693C8AC07D24F13BC_RuntimeMethod_var, 0 },
	{ 0x060000A6, 12,  (void**)&FutureVoid_SWIG_CompletionDispatcher_m60ECAC83E9887F17ECCAFE1C1DE0340903FB5281_RuntimeMethod_var, 0 },
	{ 0x060000BB, 11,  (void**)&FutureString_SWIG_CompletionDispatcher_mF70841BDB2D4C1CD323DD8BDB5ACE5BF66D505D2_RuntimeMethod_var, 0 },
	{ 0x060000DF, 17,  (void**)&LogUtil_LogMessageFromCallback_m7B4B1B33C46B05A14DE066D91BB2110DF44B8107_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0200001D, { 0, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)3, 21638 },
	{ (Il2CppRGCTXDataType)3, 21629 },
	{ (Il2CppRGCTXDataType)2, 3523 },
	{ (Il2CppRGCTXDataType)3, 21639 },
};
extern const CustomAttributesCacheGenerator g_Firebase_App_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Firebase_App_CodeGenModule;
const Il2CppCodeGenModule g_Firebase_App_CodeGenModule = 
{
	"Firebase.App.dll",
	252,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	18,
	s_reversePInvokeIndices,
	1,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_Firebase_App_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
