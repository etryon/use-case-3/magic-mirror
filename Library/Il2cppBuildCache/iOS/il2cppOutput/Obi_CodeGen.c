﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m2C7592EF4A769E56015DDE4C3669E0B9B7042AEF (void);
// 0x00000004 System.Int32 IConstraint::GetParticleCount()
// 0x00000005 System.Int32 IConstraint::GetParticle(System.Int32)
// 0x00000006 System.Int32 GridHash::Hash(Unity.Mathematics.float3,System.Single)
extern void GridHash_Hash_m4DB19DB7B792B51169CF490DE8087E17100D38A5 (void);
// 0x00000007 Unity.Mathematics.int3 GridHash::Quantize(Unity.Mathematics.float3,System.Single)
extern void GridHash_Quantize_m67808299EA16142EAEEEAF0E4D4B9B4CADA9E320 (void);
// 0x00000008 System.Int32 GridHash::Hash(Unity.Mathematics.float2,System.Single)
extern void GridHash_Hash_mE1AA8FB3CE469C0D1D7D83404CAA0C97BEF6E24A (void);
// 0x00000009 Unity.Mathematics.int2 GridHash::Quantize(Unity.Mathematics.float2,System.Single)
extern void GridHash_Quantize_m865B39F8CE4C3A0091ACD6ADCB0656BE54442284 (void);
// 0x0000000A System.Int32 GridHash::Hash(Unity.Mathematics.int3)
extern void GridHash_Hash_m755039C35AA0B62A86AD1D19A0C15CE7ACAA064D (void);
// 0x0000000B System.Int32 GridHash::Hash(Unity.Mathematics.int2)
extern void GridHash_Hash_m835A98B5B854051EEB7B912A554F24CB46B0D259 (void);
// 0x0000000C System.UInt64 GridHash::Hash(System.UInt64,System.UInt64)
extern void GridHash_Hash_m8F60FDB29079F3C1B3FB38496CCF27681AE19159 (void);
// 0x0000000D System.Void GridHash::.cctor()
extern void GridHash__cctor_mCF343F9A0800C2F30089E283179AAA2538BE52F0 (void);
// 0x0000000E System.Boolean ObiContactGrabber::get_grabbed()
extern void ObiContactGrabber_get_grabbed_mB390126778E0A789B441B372D5F525DB318BC301 (void);
// 0x0000000F System.Void ObiContactGrabber::Awake()
extern void ObiContactGrabber_Awake_m81FAB1A7D0894DFF410B881A4BD34C377CCDF33D (void);
// 0x00000010 System.Void ObiContactGrabber::OnEnable()
extern void ObiContactGrabber_OnEnable_m0CDF84CEFA0997E0C363D99406A72B7F6617015D (void);
// 0x00000011 System.Void ObiContactGrabber::OnDisable()
extern void ObiContactGrabber_OnDisable_m70E63109356461451716F2656B1F703ABDF30B67 (void);
// 0x00000012 System.Void ObiContactGrabber::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiContactGrabber_Solver_OnCollision_m02CB781C94F8F518ADC1B61E01487BBDC62BA026 (void);
// 0x00000013 System.Void ObiContactGrabber::UpdateParticleProperties()
extern void ObiContactGrabber_UpdateParticleProperties_m874B590742CF4C376FDD13678A387918D098B615 (void);
// 0x00000014 System.Boolean ObiContactGrabber::GrabParticle(Obi.ObiSolver,System.Int32)
extern void ObiContactGrabber_GrabParticle_mA112EAB2920DB3B64AA31FCF9455C6F4702858B5 (void);
// 0x00000015 System.Void ObiContactGrabber::Grab()
extern void ObiContactGrabber_Grab_mF88B7A50127D874AC0295C6D7D98FFEBE6D8F618 (void);
// 0x00000016 System.Void ObiContactGrabber::Release()
extern void ObiContactGrabber_Release_m82B5D7B992BC9423026E8CE0BD6DE0884D72BA68 (void);
// 0x00000017 System.Void ObiContactGrabber::FixedUpdate()
extern void ObiContactGrabber_FixedUpdate_mCAD4BA24875E897F95967F4540B06AC26800BF01 (void);
// 0x00000018 System.Void ObiContactGrabber::.ctor()
extern void ObiContactGrabber__ctor_m9C1D048C7D8633509DB48156260CB765A7E6E364 (void);
// 0x00000019 System.Void ObiContactGrabber/GrabbedParticle::.ctor(Obi.ObiSolver,System.Int32,System.Single)
extern void GrabbedParticle__ctor_m29866A846BC99D62A7E53A7F55FD6234B7DB7567 (void);
// 0x0000001A System.Boolean ObiContactGrabber/GrabbedParticle::Equals(ObiContactGrabber/GrabbedParticle,ObiContactGrabber/GrabbedParticle)
extern void GrabbedParticle_Equals_mFF2582C3BFDDE99366B35AC5DF73C20ECE8E430D (void);
// 0x0000001B System.Int32 ObiContactGrabber/GrabbedParticle::GetHashCode(ObiContactGrabber/GrabbedParticle)
extern void GrabbedParticle_GetHashCode_m37D33E44CE7086BE8CD4AF64B4270BE0FADDACC9 (void);
// 0x0000001C System.Runtime.InteropServices.GCHandle Oni::PinMemory(System.Object)
extern void Oni_PinMemory_m03E1EA6D2AD2B2501D052E52FDE072606EDEB6EF (void);
// 0x0000001D System.Void Oni::UnpinMemory(System.Runtime.InteropServices.GCHandle)
extern void Oni_UnpinMemory_m029F0BA710E2DBE187A2300ED9F5CFA9DE37B62F (void);
// 0x0000001E System.Void Oni::UpdateColliderGrid(System.Single)
extern void Oni_UpdateColliderGrid_m75034617DEA373FD3AAC6D3F57A9530AA426895D (void);
// 0x0000001F System.Void Oni::SetColliders(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetColliders_m901960CF23BF96A6DCE6751F09B4425431091A65 (void);
// 0x00000020 System.Void Oni::SetRigidbodies(System.IntPtr)
extern void Oni_SetRigidbodies_mF84FEC7A0C4A850D6CD1F9CF39A29C2D6AFEAECF (void);
// 0x00000021 System.Void Oni::SetCollisionMaterials(System.IntPtr)
extern void Oni_SetCollisionMaterials_mAAE3428F89A91AF30BB753E728556AD1F592F3B1 (void);
// 0x00000022 System.Void Oni::SetTriangleMeshData(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetTriangleMeshData_mFFF4366C8A8B13EDA5301EDE1CED9A2CD92AE43D (void);
// 0x00000023 System.Void Oni::SetEdgeMeshData(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetEdgeMeshData_m7D6EBD72BA2169E3C697F341B75BC716161524D0 (void);
// 0x00000024 System.Void Oni::SetDistanceFieldData(System.IntPtr,System.IntPtr)
extern void Oni_SetDistanceFieldData_mED7CC8724B49A2AF36F817A57152CD95AA6E0200 (void);
// 0x00000025 System.Void Oni::SetHeightFieldData(System.IntPtr,System.IntPtr)
extern void Oni_SetHeightFieldData_m3890F2905A2990080378C1C2F28A9AD2DA077BEE (void);
// 0x00000026 System.IntPtr Oni::CreateSolver(System.Int32)
extern void Oni_CreateSolver_m95484CA4C8E4048EB871BB224918540A317C885A (void);
// 0x00000027 System.Void Oni::DestroySolver(System.IntPtr)
extern void Oni_DestroySolver_m0ADEE02261B34DC1C348911FCDC53E4471B37D0B (void);
// 0x00000028 System.Void Oni::SetCapacity(System.IntPtr,System.Int32)
extern void Oni_SetCapacity_m4804EBFCAF115506C8DD5332214C6A2E0CB4810B (void);
// 0x00000029 System.Void Oni::InitializeFrame(System.IntPtr,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Quaternion&)
extern void Oni_InitializeFrame_mB306333E4A867678C088DACC72B039EF58E80D31 (void);
// 0x0000002A System.Void Oni::UpdateFrame(System.IntPtr,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Quaternion&,System.Single)
extern void Oni_UpdateFrame_m680BE80814872FABDBFDC7ABBE5FF1E740C5773D (void);
// 0x0000002B System.Void Oni::ApplyFrame(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Oni_ApplyFrame_mD136F376FE68D93651459FB1B946CE18BFAA8D10 (void);
// 0x0000002C System.Void Oni::RecalculateInertiaTensors(System.IntPtr)
extern void Oni_RecalculateInertiaTensors_m5D778943EF3E20DCF00A85175E39DD691A8AFA16 (void);
// 0x0000002D System.Void Oni::ResetForces(System.IntPtr)
extern void Oni_ResetForces_m38A179189C49CCAC6142D9F38F234BA8D03C5F64 (void);
// 0x0000002E System.Void Oni::SetRigidbodyLinearDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetRigidbodyLinearDeltas_m3D5B23B5969BC13392B4E7E019D03730E61E3639 (void);
// 0x0000002F System.Void Oni::SetRigidbodyAngularDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetRigidbodyAngularDeltas_m4A436C37D1F6F9FE39F1184279883463F586CBF0 (void);
// 0x00000030 System.Void Oni::GetBounds(System.IntPtr,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Oni_GetBounds_m363A96EE8BAB3FB760C636B8C5F361B8EE1E53B5 (void);
// 0x00000031 System.Int32 Oni::GetParticleGridSize(System.IntPtr)
extern void Oni_GetParticleGridSize_m2BD9A7939A0F7733323CA9C9EDE32C53FC7B6ABD (void);
// 0x00000032 System.Void Oni::GetParticleGrid(System.IntPtr,Oni/GridCell[])
extern void Oni_GetParticleGrid_m21D4AA3548CF77E25C5E456096E16162C1BAD4C0 (void);
// 0x00000033 System.Int32 Oni::SpatialQuery(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SpatialQuery_m526F90093505F1A4FB3E31A2AC6EC6B8C1F850A5 (void);
// 0x00000034 System.Void Oni::GetQueryResults(System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_GetQueryResults_mB69A3B8645A8218700E75C49956E15AC27B125B7 (void);
// 0x00000035 System.Void Oni::SetSolverParameters(System.IntPtr,Oni/SolverParameters&)
extern void Oni_SetSolverParameters_mA78FF4C834E23D50A56D989521B278AA6B11BD48 (void);
// 0x00000036 System.Void Oni::GetSolverParameters(System.IntPtr,Oni/SolverParameters&)
extern void Oni_GetSolverParameters_mD299B385DF9770D77C9E0D4F4781EFADE20C30E4 (void);
// 0x00000037 System.Int32 Oni::SetActiveParticles(System.IntPtr,System.Int32[],System.Int32)
extern void Oni_SetActiveParticles_mA8361CFE331459188A7701673465FCEC0770F84B (void);
// 0x00000038 System.IntPtr Oni::CollisionDetection(System.IntPtr,System.Single)
extern void Oni_CollisionDetection_mD890F2AED814E8B7F3766DDD603916EDA4B1B861 (void);
// 0x00000039 System.IntPtr Oni::Step(System.IntPtr,System.Single,System.Single,System.Int32)
extern void Oni_Step_m73AE7B946E5053FC14C8061FCD6508D4824213B7 (void);
// 0x0000003A System.Void Oni::ApplyPositionInterpolation(System.IntPtr,System.IntPtr,System.IntPtr,System.Single,System.Single)
extern void Oni_ApplyPositionInterpolation_mA7A631314B43D3A62FAF81D209F1CCB00A3A901D (void);
// 0x0000003B System.Void Oni::UpdateSkeletalAnimation(System.IntPtr)
extern void Oni_UpdateSkeletalAnimation_m0F0F49630F0ABF1B6C742D13FEDB8A69B3C71A31 (void);
// 0x0000003C System.Int32 Oni::GetConstraintCount(System.IntPtr,System.Int32)
extern void Oni_GetConstraintCount_m046D7B8D04B0EDDC24A336FA1AD6F93B181B143A (void);
// 0x0000003D System.Void Oni::SetRenderableParticlePositions(System.IntPtr,System.IntPtr)
extern void Oni_SetRenderableParticlePositions_mF10457D7F9724C687D3AB6E83DEEBB4C9937F138 (void);
// 0x0000003E System.Void Oni::SetParticlePhases(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePhases_m330FC442852B31A851FB05486D9D5D4A9E85080C (void);
// 0x0000003F System.Void Oni::SetParticleFilters(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleFilters_m4689F2DCACACC85971902111BF779817074E976C (void);
// 0x00000040 System.Void Oni::SetParticleCollisionMaterials(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleCollisionMaterials_m64562F4539FF5805E44B181ED119B61FB636DB91 (void);
// 0x00000041 System.Void Oni::SetParticlePositions(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositions_mBF4B08453E99AACC5D3C934F3B7F1EDB2AA3934A (void);
// 0x00000042 System.Void Oni::SetParticlePreviousPositions(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePreviousPositions_mF7643A2DD9F3D10409DF62601E0D8EE2D0DBD7E4 (void);
// 0x00000043 System.Void Oni::SetParticleOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientations_m92BF1084EB29939C992A0666D1E75C6AAB14801D (void);
// 0x00000044 System.Void Oni::SetParticlePreviousOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePreviousOrientations_m08E1313630723606D179B6FB44025E49B838D2BB (void);
// 0x00000045 System.Void Oni::SetRenderableParticleOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetRenderableParticleOrientations_mE90A2228D4CDC6641661C730F1164F65609788B8 (void);
// 0x00000046 System.Void Oni::SetParticleInverseMasses(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseMasses_mA5F3998095FBD44B558C75B634E18628520D2385 (void);
// 0x00000047 System.Void Oni::SetParticleInverseRotationalMasses(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseRotationalMasses_mBB6281669729C39BEF623DDDEDCC12EC93C31FBB (void);
// 0x00000048 System.Void Oni::SetParticlePrincipalRadii(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePrincipalRadii_mCE141D972ABA98C2CCC7B66419BC0009E227F8CE (void);
// 0x00000049 System.Void Oni::SetParticleVelocities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVelocities_m806F66F28AB0D4E632921B7E0CF3464C777B00C7 (void);
// 0x0000004A System.Void Oni::SetParticleAngularVelocities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAngularVelocities_mD376C89032B153B1C2EB36FAF673C90600871052 (void);
// 0x0000004B System.Void Oni::SetParticleExternalForces(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleExternalForces_mABE204855B05F30D980984785EFC178E8178657B (void);
// 0x0000004C System.Void Oni::SetParticleExternalTorques(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleExternalTorques_m52F453DFC994BB2CB946C0ECFCA90755E8CE81BF (void);
// 0x0000004D System.Void Oni::SetParticleWinds(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleWinds_m4D8B34B1F691B0D06B66B5AC4A9CE7A47C438D7A (void);
// 0x0000004E System.Void Oni::SetParticlePositionDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositionDeltas_m44290488FA81D82A7B5E34C6C419183E4AC4C7F3 (void);
// 0x0000004F System.Void Oni::SetParticleOrientationDeltas(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientationDeltas_m80BA163C8E315CDE4D535070E044140582F4FB75 (void);
// 0x00000050 System.Void Oni::SetParticlePositionConstraintCounts(System.IntPtr,System.IntPtr)
extern void Oni_SetParticlePositionConstraintCounts_mC6C3DF0767ABFE55B7D56664BD2B85CACC2F2AF3 (void);
// 0x00000051 System.Void Oni::SetParticleOrientationConstraintCounts(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleOrientationConstraintCounts_mE8DE30A217492D6C2961351F94471600E84DDF33 (void);
// 0x00000052 System.Void Oni::SetParticleNormals(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleNormals_mC7DB3812E3D8DA4944D5783D612327C14DAA1407 (void);
// 0x00000053 System.Void Oni::SetParticleInverseInertiaTensors(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleInverseInertiaTensors_m107AC92B3D36DA0EC84A2BC81EAAFDE1A87645F3 (void);
// 0x00000054 System.Void Oni::SetParticleSmoothingRadii(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleSmoothingRadii_m23C24AAADB64BB87F5AB6C0F29146E045BD15F32 (void);
// 0x00000055 System.Void Oni::SetParticleBuoyancy(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleBuoyancy_m1D5C0C42C6E1F7F9F0009907C3C47777493BFFFD (void);
// 0x00000056 System.Void Oni::SetParticleRestDensities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleRestDensities_mC2C5BF07F3F31A9A9BD30F1E0FD65FA90DAB8668 (void);
// 0x00000057 System.Void Oni::SetParticleViscosities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleViscosities_m40D16C05777FCCBA236F8812E90FCE61732F8F65 (void);
// 0x00000058 System.Void Oni::SetParticleSurfaceTension(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleSurfaceTension_mEB650D756ACFD8AFCE0B0CA1FA516B8A04166803 (void);
// 0x00000059 System.Void Oni::SetParticleVorticityConfinement(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVorticityConfinement_m490207BCEB92C2090FA6F812903E9DC720C542CD (void);
// 0x0000005A System.Void Oni::SetParticleAtmosphericDragPressure(System.IntPtr,System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAtmosphericDragPressure_m0DE4F75155DE8255D542463CED62BBA10DE95991 (void);
// 0x0000005B System.Void Oni::SetParticleDiffusion(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleDiffusion_m2E7A56B1429ABF2C68BCBEAF9A8B903BCFBBEB97 (void);
// 0x0000005C System.Void Oni::SetParticleVorticities(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleVorticities_mBFAFD13B80D055251ECE3BB09331FEE93EC096CE (void);
// 0x0000005D System.Void Oni::SetParticleFluidData(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleFluidData_m5475F465341FBD9D3646CF21F640D31DBBDA52BC (void);
// 0x0000005E System.Void Oni::SetParticleUserData(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleUserData_m1C95E2860A342CC2408369BE71453D688A4940E2 (void);
// 0x0000005F System.Void Oni::SetParticleAnisotropies(System.IntPtr,System.IntPtr)
extern void Oni_SetParticleAnisotropies_m4F40683E247F34D06C6FAC46E7BB2D100699D943 (void);
// 0x00000060 System.Void Oni::SetSimplices(System.IntPtr,System.Int32[],System.Int32,System.Int32,System.Int32)
extern void Oni_SetSimplices_mFA1EA60EF9F96B8AD384993D7AC15B73A2B08D95 (void);
// 0x00000061 System.Int32 Oni::GetDeformableTriangleCount(System.IntPtr)
extern void Oni_GetDeformableTriangleCount_m3D720CBB04BADD1E4AAE5D0F630E7E8A1AB8D398 (void);
// 0x00000062 System.Void Oni::SetDeformableTriangles(System.IntPtr,System.Int32[],System.Int32,System.Int32)
extern void Oni_SetDeformableTriangles_m93D02F1E05CEF46D51296532191387392C8885C6 (void);
// 0x00000063 System.Int32 Oni::RemoveDeformableTriangles(System.IntPtr,System.Int32,System.Int32)
extern void Oni_RemoveDeformableTriangles_mA0A4A28EA77A35743C37CF80A333559E7C8CC8AF (void);
// 0x00000064 System.Void Oni::SetConstraintGroupParameters(System.IntPtr,System.Int32,Oni/ConstraintParameters&)
extern void Oni_SetConstraintGroupParameters_m7402C64CA0198FD7D07E4F2BBFC81F8C9DB926B3 (void);
// 0x00000065 System.Void Oni::GetConstraintGroupParameters(System.IntPtr,System.Int32,Oni/ConstraintParameters&)
extern void Oni_GetConstraintGroupParameters_m841DD84B6D78A0B694506D815E4756313934B29F (void);
// 0x00000066 System.Void Oni::SetRestPositions(System.IntPtr,System.IntPtr)
extern void Oni_SetRestPositions_m819342E902C330762AEB5BE9D4C68F31DA6CA8A1 (void);
// 0x00000067 System.Void Oni::SetRestOrientations(System.IntPtr,System.IntPtr)
extern void Oni_SetRestOrientations_m87B3C65B2FA89627A6526A5C22B674280DC3C577 (void);
// 0x00000068 System.IntPtr Oni::CreateBatch(System.Int32)
extern void Oni_CreateBatch_m289C846274F339F1F3E4A026FE857DAAAC56ABE8 (void);
// 0x00000069 System.Void Oni::DestroyBatch(System.IntPtr)
extern void Oni_DestroyBatch_m0C065F052A4E0B16A426180BF8EC84E74845007E (void);
// 0x0000006A System.IntPtr Oni::AddBatch(System.IntPtr,System.IntPtr)
extern void Oni_AddBatch_m22AFEB35DB5CD69377763213EEED12183545A8DE (void);
// 0x0000006B System.Void Oni::RemoveBatch(System.IntPtr,System.IntPtr)
extern void Oni_RemoveBatch_m62A5EE513FF64B56579A035F1463C0308DD01A48 (void);
// 0x0000006C System.Boolean Oni::EnableBatch(System.IntPtr,System.Boolean)
extern void Oni_EnableBatch_mE5AAEE57C0386681A3016600946C205D90CCBD0B (void);
// 0x0000006D System.Int32 Oni::GetBatchConstraintForces(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern void Oni_GetBatchConstraintForces_m2F4FB4C6732CFC54C859D7DB5C4A3AC75E4400FE (void);
// 0x0000006E System.Void Oni::SetBatchConstraintCount(System.IntPtr,System.Int32)
extern void Oni_SetBatchConstraintCount_m31FD93985340AF90A5D98742034D010888144B41 (void);
// 0x0000006F System.Int32 Oni::GetBatchConstraintCount(System.IntPtr)
extern void Oni_GetBatchConstraintCount_mCDA7F7C7B72B04711AAD3435FFE3AB6FDF657966 (void);
// 0x00000070 System.Void Oni::SetDistanceConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetDistanceConstraints_m6BAF3B1FE95B48E8E681121385077A369140BA88 (void);
// 0x00000071 System.Void Oni::SetBendingConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetBendingConstraints_m4DD1607554632126E50CA62B31DE223000DEE579 (void);
// 0x00000072 System.Void Oni::SetSkinConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetSkinConstraints_m980F260DC4107F86B16555CB40B9B63A196F558C (void);
// 0x00000073 System.Void Oni::SetAerodynamicConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetAerodynamicConstraints_m97BED4EFAF9B73A677757503EF1AF7EA59CB712C (void);
// 0x00000074 System.Void Oni::SetVolumeConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetVolumeConstraints_m7914A0DFEEF418BE4C6C81DE6A27EEC21701F511 (void);
// 0x00000075 System.Void Oni::SetShapeMatchingConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetShapeMatchingConstraints_mE2D86779FF689D2FBEB8F39FF1683CEAA9D7547E (void);
// 0x00000076 System.Void Oni::CalculateRestShapeMatching(System.IntPtr,System.IntPtr)
extern void Oni_CalculateRestShapeMatching_mD268677C88F2E8ABB2C0E8149F0683FFF9AF3BDD (void);
// 0x00000077 System.Void Oni::SetStretchShearConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetStretchShearConstraints_m49F7345551857151AE2572A3F59A90FCA236CDA8 (void);
// 0x00000078 System.Void Oni::SetBendTwistConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetBendTwistConstraints_mE4976251409BFB7882CC39AD1F21D4A97B7F4D03 (void);
// 0x00000079 System.Void Oni::SetTetherConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetTetherConstraints_m10810C34086C7751FB8A5600EFBBC9C70887449E (void);
// 0x0000007A System.Void Oni::SetPinConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetPinConstraints_mC5929BC4A3B8861A32C47072223BD7C88CAC8843 (void);
// 0x0000007B System.Void Oni::SetStitchConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetStitchConstraints_m8576B5213542C38E501BD55E1C0936DF69506253 (void);
// 0x0000007C System.Void Oni::SetChainConstraints(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_SetChainConstraints_m8F08E1B952B69C7A6D3F1957CA4511BE3D95D25D (void);
// 0x0000007D System.Void Oni::GetCollisionContacts(System.IntPtr,Oni/Contact[],System.Int32)
extern void Oni_GetCollisionContacts_mD80B55291B76FB42F97EE2C75EB2E5089540AE63 (void);
// 0x0000007E System.Void Oni::GetParticleCollisionContacts(System.IntPtr,Oni/Contact[],System.Int32)
extern void Oni_GetParticleCollisionContacts_mCDAC4DB2F8B1A798CEC15191044C4FE761AF69D1 (void);
// 0x0000007F System.Int32 Oni::InterpolateDiffuseParticles(System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void Oni_InterpolateDiffuseParticles_m4A3F61BFA0B073204F17E45D12E58B292B3C5D44 (void);
// 0x00000080 System.Int32 Oni::MakePhase(System.Int32,Obi.ObiUtils/ParticleFlags)
extern void Oni_MakePhase_m9FFA8D86C974D669797A0DF8FF1E6A6270554D71 (void);
// 0x00000081 System.Int32 Oni::GetGroupFromPhase(System.Int32)
extern void Oni_GetGroupFromPhase_m1E9E33EEBF211AD24F224C4DDF044F55DEC4E6CF (void);
// 0x00000082 System.Int32 Oni::GetFlagsFromPhase(System.Int32)
extern void Oni_GetFlagsFromPhase_m4EC4AEB3B15B30111080A14FB54D7939A4924F0B (void);
// 0x00000083 System.Single Oni::BendingConstraintRest(System.Single[])
extern void Oni_BendingConstraintRest_mBF9E6E4CB4E998E291B956B4304FFB31A1ED6058 (void);
// 0x00000084 System.Void Oni::CompleteAll()
extern void Oni_CompleteAll_m24DA6FE9992164ABDA19A2E0E5BB58280FD02582 (void);
// 0x00000085 System.Void Oni::Complete(System.IntPtr)
extern void Oni_Complete_m4909573C5EF85056474F9EEB3A8C91F6F62050B5 (void);
// 0x00000086 System.IntPtr Oni::CreateEmpty()
extern void Oni_CreateEmpty_mEEB6A72C1C211426318B7A3164BD1697F1C0859C (void);
// 0x00000087 System.Void Oni::Schedule(System.IntPtr)
extern void Oni_Schedule_mF1BBBF5D226699D18E8EB55A8F31A08BE1DE528B (void);
// 0x00000088 System.Void Oni::AddChild(System.IntPtr,System.IntPtr)
extern void Oni_AddChild_m2AA3FE95D394A0AB211C7FAF46DA6DDA34645B49 (void);
// 0x00000089 System.Int32 Oni::GetMaxSystemConcurrency()
extern void Oni_GetMaxSystemConcurrency_mA467F4FF67759D65BBACC64B265BBD53DBEE3177 (void);
// 0x0000008A System.Void Oni::ClearProfiler()
extern void Oni_ClearProfiler_m3CE1E415966CFAA8CBF71A57B412B980CE3B5E11 (void);
// 0x0000008B System.Void Oni::EnableProfiler(System.Boolean)
extern void Oni_EnableProfiler_m50B45E5B063E18108D99A6851C2C441A8FEDCFD4 (void);
// 0x0000008C System.Void Oni::BeginSample(System.String,System.Byte)
extern void Oni_BeginSample_m584963073106DEA213EA5679DA42F89AB29B0931 (void);
// 0x0000008D System.Void Oni::EndSample()
extern void Oni_EndSample_mE3D8141E3F15F6DEEB30C16BDA9BC9E82368F8EE (void);
// 0x0000008E System.Int32 Oni::GetProfilingInfoCount()
extern void Oni_GetProfilingInfoCount_m89525656BF980E088FBF35150F29DC2BCAF974EB (void);
// 0x0000008F System.Void Oni::GetProfilingInfo(Oni/ProfileInfo[],System.Int32)
extern void Oni_GetProfilingInfo_m5450B203396A0BD0042264E198B358D3FA41BA23 (void);
// 0x00000090 System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
extern void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3 (void);
// 0x00000091 System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
extern void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566 (void);
// 0x00000092 Obi.ObiActorBlueprint Obi.ObiCloth::get_sourceBlueprint()
extern void ObiCloth_get_sourceBlueprint_m7A60AB633A6899056FE31A30FDF9126DEE4A32CF (void);
// 0x00000093 Obi.ObiClothBlueprintBase Obi.ObiCloth::get_clothBlueprintBase()
extern void ObiCloth_get_clothBlueprintBase_mCB3ED81DB059E3BA6A2A05A04671D346B2E80E1E (void);
// 0x00000094 System.Boolean Obi.ObiCloth::get_volumeConstraintsEnabled()
extern void ObiCloth_get_volumeConstraintsEnabled_mF12200D8E63D163F8D3D585D7AC479860DCE7DC6 (void);
// 0x00000095 System.Void Obi.ObiCloth::set_volumeConstraintsEnabled(System.Boolean)
extern void ObiCloth_set_volumeConstraintsEnabled_mC125B982F69E1B9542006432130B902596A76E6C (void);
// 0x00000096 System.Single Obi.ObiCloth::get_compressionCompliance()
extern void ObiCloth_get_compressionCompliance_m9186FB60A86F801C2423F01E5A464258B045CE7F (void);
// 0x00000097 System.Void Obi.ObiCloth::set_compressionCompliance(System.Single)
extern void ObiCloth_set_compressionCompliance_mF125B6A8FD286D33892DF1E7AD4112E4D0A8D066 (void);
// 0x00000098 System.Single Obi.ObiCloth::get_pressure()
extern void ObiCloth_get_pressure_mFF6475950C18617020944507D0E53E8DEF0D832B (void);
// 0x00000099 System.Void Obi.ObiCloth::set_pressure(System.Single)
extern void ObiCloth_set_pressure_m16C6A0415FA0D7705031E593A5DF2E25DEB4399B (void);
// 0x0000009A System.Boolean Obi.ObiCloth::get_tetherConstraintsEnabled()
extern void ObiCloth_get_tetherConstraintsEnabled_m659B71F2615E9745A37683F80EBC511DBE98A0CF (void);
// 0x0000009B System.Void Obi.ObiCloth::set_tetherConstraintsEnabled(System.Boolean)
extern void ObiCloth_set_tetherConstraintsEnabled_m578A93337CA1C606418FFD36B1F8D42A63BAF131 (void);
// 0x0000009C System.Single Obi.ObiCloth::get_tetherCompliance()
extern void ObiCloth_get_tetherCompliance_mC27665C4D2558899A84241992C1487432FDE4808 (void);
// 0x0000009D System.Void Obi.ObiCloth::set_tetherCompliance(System.Single)
extern void ObiCloth_set_tetherCompliance_m79AB074264748F775FACB6EBC910420341528253 (void);
// 0x0000009E System.Single Obi.ObiCloth::get_tetherScale()
extern void ObiCloth_get_tetherScale_m0422EEC36F6C734B99A97E8C41E4F8666AE9095E (void);
// 0x0000009F System.Void Obi.ObiCloth::set_tetherScale(System.Single)
extern void ObiCloth_set_tetherScale_m4C63B9250EDF777173EFCEDB5DA08286D16F892A (void);
// 0x000000A0 Obi.ObiClothBlueprint Obi.ObiCloth::get_clothBlueprint()
extern void ObiCloth_get_clothBlueprint_m95CC97BDC2C2AA18A02730FDE298D79D2399438C (void);
// 0x000000A1 System.Void Obi.ObiCloth::set_clothBlueprint(Obi.ObiClothBlueprint)
extern void ObiCloth_set_clothBlueprint_m9C13230540CA0035412B8F415261C483560BBA16 (void);
// 0x000000A2 System.Void Obi.ObiCloth::OnValidate()
extern void ObiCloth_OnValidate_m09DF476881D69E1B90AFAC8B1C95DE21D8C1240E (void);
// 0x000000A3 System.Void Obi.ObiCloth::SetupRuntimeConstraints()
extern void ObiCloth_SetupRuntimeConstraints_m9E9D082292F0E747E5C7C87FD814663C27677278 (void);
// 0x000000A4 System.Void Obi.ObiCloth::.ctor()
extern void ObiCloth__ctor_m8F253D91A3CB7CCA5D0993BD5C1A94ECA76DA10C (void);
// 0x000000A5 Obi.ObiClothBlueprintBase Obi.ObiClothBase::get_clothBlueprintBase()
// 0x000000A6 System.Boolean Obi.ObiClothBase::get_oneSided()
extern void ObiClothBase_get_oneSided_mCF71829A68D6C17A6DDABD9FE5FD4953CB096A9E (void);
// 0x000000A7 System.Void Obi.ObiClothBase::set_oneSided(System.Boolean)
extern void ObiClothBase_set_oneSided_mEAB625E767A38419887FDA8C17F4E7E0132BB335 (void);
// 0x000000A8 System.Boolean Obi.ObiClothBase::get_selfCollisions()
extern void ObiClothBase_get_selfCollisions_mA8188702FAC8AB35A10EA440CAACE6402650981F (void);
// 0x000000A9 System.Void Obi.ObiClothBase::set_selfCollisions(System.Boolean)
extern void ObiClothBase_set_selfCollisions_m565AD30B9E80A9C10ADB69715A77992DA236F4E3 (void);
// 0x000000AA System.Boolean Obi.ObiClothBase::get_distanceConstraintsEnabled()
extern void ObiClothBase_get_distanceConstraintsEnabled_mA71C6AB3A7A15023B0F25DA31D8F068CB7056B0B (void);
// 0x000000AB System.Void Obi.ObiClothBase::set_distanceConstraintsEnabled(System.Boolean)
extern void ObiClothBase_set_distanceConstraintsEnabled_m0AD906019BCEA7BB359643832B5C6FEBC90F41B3 (void);
// 0x000000AC System.Single Obi.ObiClothBase::get_stretchingScale()
extern void ObiClothBase_get_stretchingScale_mEEE3C91265561E074058EA7EF4E4FE91A273454D (void);
// 0x000000AD System.Void Obi.ObiClothBase::set_stretchingScale(System.Single)
extern void ObiClothBase_set_stretchingScale_m9FDB17864055E9ABCEC250BEFC87FD5AA5D9C7FE (void);
// 0x000000AE System.Single Obi.ObiClothBase::get_stretchCompliance()
extern void ObiClothBase_get_stretchCompliance_m986E8274AEBE7D18FD43D9EC2FF810B00E7614AE (void);
// 0x000000AF System.Void Obi.ObiClothBase::set_stretchCompliance(System.Single)
extern void ObiClothBase_set_stretchCompliance_mEF00294CCF1A3D9F708EEDEC951F1B39DD5FF03A (void);
// 0x000000B0 System.Single Obi.ObiClothBase::get_maxCompression()
extern void ObiClothBase_get_maxCompression_m7EED40DF37E302E1CD3DC5890E3A5EE75241068E (void);
// 0x000000B1 System.Void Obi.ObiClothBase::set_maxCompression(System.Single)
extern void ObiClothBase_set_maxCompression_m549EDB7CFA059EDDBAD3F6FDF049BED83C0FEBF4 (void);
// 0x000000B2 System.Boolean Obi.ObiClothBase::get_bendConstraintsEnabled()
extern void ObiClothBase_get_bendConstraintsEnabled_m488BC08F2F2DA4970B170EBA3853BF4236833F6B (void);
// 0x000000B3 System.Void Obi.ObiClothBase::set_bendConstraintsEnabled(System.Boolean)
extern void ObiClothBase_set_bendConstraintsEnabled_m5B6532793182F92B6B11E3895FF060E8863132A9 (void);
// 0x000000B4 System.Single Obi.ObiClothBase::get_bendCompliance()
extern void ObiClothBase_get_bendCompliance_m646783573274ACC26EC85C56EEEFEAC2C987D259 (void);
// 0x000000B5 System.Void Obi.ObiClothBase::set_bendCompliance(System.Single)
extern void ObiClothBase_set_bendCompliance_mF16739C7F7B77E19CADB5806DD2A61DADA849C84 (void);
// 0x000000B6 System.Single Obi.ObiClothBase::get_maxBending()
extern void ObiClothBase_get_maxBending_mD311AD66F421152BAC8EA4262D644DBCF1300394 (void);
// 0x000000B7 System.Void Obi.ObiClothBase::set_maxBending(System.Single)
extern void ObiClothBase_set_maxBending_m20B4EFE0A496F15EC228E3064F8BC865359F521C (void);
// 0x000000B8 System.Single Obi.ObiClothBase::get_plasticYield()
extern void ObiClothBase_get_plasticYield_mC14422183FE80C52498410EFB928D2955279F54E (void);
// 0x000000B9 System.Void Obi.ObiClothBase::set_plasticYield(System.Single)
extern void ObiClothBase_set_plasticYield_m79DD5850D6059FE3D7A793E2CF11C89FD6786B9F (void);
// 0x000000BA System.Single Obi.ObiClothBase::get_plasticCreep()
extern void ObiClothBase_get_plasticCreep_m8926FF8313698F646B2FC20E1C6E5D1A1E036F63 (void);
// 0x000000BB System.Void Obi.ObiClothBase::set_plasticCreep(System.Single)
extern void ObiClothBase_set_plasticCreep_mF2A78749BD78479F98B34EB517A7A100A28F43D1 (void);
// 0x000000BC System.Boolean Obi.ObiClothBase::get_aerodynamicsEnabled()
extern void ObiClothBase_get_aerodynamicsEnabled_m8AD844A743A70B3C29168739D93422C132494B39 (void);
// 0x000000BD System.Void Obi.ObiClothBase::set_aerodynamicsEnabled(System.Boolean)
extern void ObiClothBase_set_aerodynamicsEnabled_m50A7F807E8441451D866444A85910E863488B64F (void);
// 0x000000BE System.Single Obi.ObiClothBase::get_drag()
extern void ObiClothBase_get_drag_m14DE68E55D8E00EF647709EB52C88A8A5A7B11F7 (void);
// 0x000000BF System.Void Obi.ObiClothBase::set_drag(System.Single)
extern void ObiClothBase_set_drag_mEF0A2D9B6B8BC041F853B1CF19FFC94CFF448C61 (void);
// 0x000000C0 System.Single Obi.ObiClothBase::get_lift()
extern void ObiClothBase_get_lift_mE6CCF75F4E2D2987AA6E74569AB7282C72F12C16 (void);
// 0x000000C1 System.Void Obi.ObiClothBase::set_lift(System.Single)
extern void ObiClothBase_set_lift_m29FD4958A66AE91B0CA98791F0B5C21759370D90 (void);
// 0x000000C2 System.Boolean Obi.ObiClothBase::get_usesCustomExternalForces()
extern void ObiClothBase_get_usesCustomExternalForces_m40DE22CFE82E2CC2A588254D8E3D8D80AF259BF7 (void);
// 0x000000C3 System.Void Obi.ObiClothBase::LoadBlueprint(Obi.ObiSolver)
extern void ObiClothBase_LoadBlueprint_m1997F75430C7EDA2E6F53C95C0236A5EEC4FCE82 (void);
// 0x000000C4 System.Void Obi.ObiClothBase::UnloadBlueprint(Obi.ObiSolver)
extern void ObiClothBase_UnloadBlueprint_mA3989EAD9E5BC68E4714ED61C583554B937E5E2B (void);
// 0x000000C5 System.Void Obi.ObiClothBase::UpdateDeformableTriangles()
extern void ObiClothBase_UpdateDeformableTriangles_mD4772368FDE132FEB030FE07538DE949C0E71595 (void);
// 0x000000C6 System.Void Obi.ObiClothBase::.ctor()
extern void ObiClothBase__ctor_mDA1B5958E1609E7D236766AD66D45B40630F0500 (void);
// 0x000000C7 Obi.ObiActorBlueprint Obi.ObiSkinnedCloth::get_sourceBlueprint()
extern void ObiSkinnedCloth_get_sourceBlueprint_m13328271C367E75F468A5CB821E46B516518FED7 (void);
// 0x000000C8 Obi.ObiClothBlueprintBase Obi.ObiSkinnedCloth::get_clothBlueprintBase()
extern void ObiSkinnedCloth_get_clothBlueprintBase_mC361EC10AE593A55928FBC327BBA63C42BEA5D9D (void);
// 0x000000C9 System.Boolean Obi.ObiSkinnedCloth::get_tetherConstraintsEnabled()
extern void ObiSkinnedCloth_get_tetherConstraintsEnabled_m7D5D97F93312AD609B63C443A42C57076675D6D4 (void);
// 0x000000CA System.Void Obi.ObiSkinnedCloth::set_tetherConstraintsEnabled(System.Boolean)
extern void ObiSkinnedCloth_set_tetherConstraintsEnabled_m5323B4430C10747FE22EA963D06B7230604156FA (void);
// 0x000000CB System.Single Obi.ObiSkinnedCloth::get_tetherCompliance()
extern void ObiSkinnedCloth_get_tetherCompliance_m7920AFD1C3D359FAE653E7445E5A9ADC3E7AEBD4 (void);
// 0x000000CC System.Void Obi.ObiSkinnedCloth::set_tetherCompliance(System.Single)
extern void ObiSkinnedCloth_set_tetherCompliance_m17CE38B39DB2E5E70D5FDBDA5639742A1F9D525D (void);
// 0x000000CD System.Single Obi.ObiSkinnedCloth::get_tetherScale()
extern void ObiSkinnedCloth_get_tetherScale_m7CE8A2EC5FD8E5BB3E2B2AA313658130C865334B (void);
// 0x000000CE System.Void Obi.ObiSkinnedCloth::set_tetherScale(System.Single)
extern void ObiSkinnedCloth_set_tetherScale_m5A8979CF63152BB15C971717E8C1AC2C57982820 (void);
// 0x000000CF System.Boolean Obi.ObiSkinnedCloth::get_skinConstraintsEnabled()
extern void ObiSkinnedCloth_get_skinConstraintsEnabled_mD7B825F96326630C3CE232B495F7FF5007DA95C5 (void);
// 0x000000D0 System.Void Obi.ObiSkinnedCloth::set_skinConstraintsEnabled(System.Boolean)
extern void ObiSkinnedCloth_set_skinConstraintsEnabled_m90CDD0C2015E0221D7DF97938A16AB9783ABA7B0 (void);
// 0x000000D1 Obi.ObiSkinnedClothBlueprint Obi.ObiSkinnedCloth::get_skinnedClothBlueprint()
extern void ObiSkinnedCloth_get_skinnedClothBlueprint_mEDC031943522A53F8C3F23987722ED415ECE3366 (void);
// 0x000000D2 System.Void Obi.ObiSkinnedCloth::set_skinnedClothBlueprint(Obi.ObiSkinnedClothBlueprint)
extern void ObiSkinnedCloth_set_skinnedClothBlueprint_m2E4A135DF3669904458562CD66A118BD02CDF2C8 (void);
// 0x000000D3 System.Void Obi.ObiSkinnedCloth::LoadBlueprint(Obi.ObiSolver)
extern void ObiSkinnedCloth_LoadBlueprint_mDD2DCF50FE65F33D6E620579C8920ADDF28FC7A8 (void);
// 0x000000D4 System.Void Obi.ObiSkinnedCloth::OnValidate()
extern void ObiSkinnedCloth_OnValidate_m2FB46885B796AC72D9EADB3F09A744BE95065202 (void);
// 0x000000D5 System.Void Obi.ObiSkinnedCloth::SetupRuntimeConstraints()
extern void ObiSkinnedCloth_SetupRuntimeConstraints_m0226D8DA4DDFB1A3FED8F30A9AB98DCB7E5F19E0 (void);
// 0x000000D6 System.Void Obi.ObiSkinnedCloth::OnEnable()
extern void ObiSkinnedCloth_OnEnable_mE6C374E90EE387B5C68EFAE8104D0CB1B17610E6 (void);
// 0x000000D7 System.Void Obi.ObiSkinnedCloth::OnDisable()
extern void ObiSkinnedCloth_OnDisable_mBE078E76699C913C05A75F52A521B0320570757B (void);
// 0x000000D8 UnityEngine.Vector3 Obi.ObiSkinnedCloth::GetSkinRadiiBackstop(Obi.ObiSkinConstraintsBatch,System.Int32)
extern void ObiSkinnedCloth_GetSkinRadiiBackstop_m46D90477638A5D51D42926241416EA31A816F86B (void);
// 0x000000D9 System.Single Obi.ObiSkinnedCloth::GetSkinCompliance(Obi.ObiSkinConstraintsBatch,System.Int32)
extern void ObiSkinnedCloth_GetSkinCompliance_mA3B6AF93037C08FCC5386B7482B28921B7449C29 (void);
// 0x000000DA System.Void Obi.ObiSkinnedCloth::CreateBakedMeshIfNeeded()
extern void ObiSkinnedCloth_CreateBakedMeshIfNeeded_m32D7C98608AC99DC501E5B407628D29F60F40DCB (void);
// 0x000000DB System.Void Obi.ObiSkinnedCloth::SetTargetSkin()
extern void ObiSkinnedCloth_SetTargetSkin_m69DDCC57FC91662996DB98FB1AB2FBBCB9CD6327 (void);
// 0x000000DC System.Void Obi.ObiSkinnedCloth::BeginStep(System.Single)
extern void ObiSkinnedCloth_BeginStep_m809CA572F7AAE52160D52456C9A7366EFBEE95EE (void);
// 0x000000DD System.Void Obi.ObiSkinnedCloth::.ctor()
extern void ObiSkinnedCloth__ctor_mCF9EB2A6435C578912D11ED86DA9FA4CFA3CC746 (void);
// 0x000000DE System.Void Obi.ObiSkinnedCloth::.cctor()
extern void ObiSkinnedCloth__cctor_m4D88F6E59065C9E8AA4D18FFADDE7998603008F9 (void);
// 0x000000DF Obi.ObiActorBlueprint Obi.ObiTearableCloth::get_sourceBlueprint()
extern void ObiTearableCloth_get_sourceBlueprint_m55F81480B527CF8C758ABD4D2F7F929F2695B26C (void);
// 0x000000E0 Obi.ObiClothBlueprintBase Obi.ObiTearableCloth::get_clothBlueprintBase()
extern void ObiTearableCloth_get_clothBlueprintBase_m006E40329FC8205C0365BB51A98DAB915418AB1B (void);
// 0x000000E1 Obi.ObiTearableClothBlueprint Obi.ObiTearableCloth::get_clothBlueprint()
extern void ObiTearableCloth_get_clothBlueprint_m8220EDF63D73810E436E64556C9DAC603592A319 (void);
// 0x000000E2 System.Void Obi.ObiTearableCloth::set_clothBlueprint(Obi.ObiTearableClothBlueprint)
extern void ObiTearableCloth_set_clothBlueprint_mC2FDC91099716CDCC7874B65540F166FBA1043AE (void);
// 0x000000E3 System.Void Obi.ObiTearableCloth::add_OnClothTorn(Obi.ObiTearableCloth/ClothTornCallback)
extern void ObiTearableCloth_add_OnClothTorn_m6224CDA7378B8DB12B0333F7FED7B7227E6E3694 (void);
// 0x000000E4 System.Void Obi.ObiTearableCloth::remove_OnClothTorn(Obi.ObiTearableCloth/ClothTornCallback)
extern void ObiTearableCloth_remove_OnClothTorn_mB5CE17825525987AFCBD938933DC1248DD4D39E9 (void);
// 0x000000E5 System.Void Obi.ObiTearableCloth::LoadBlueprint(Obi.ObiSolver)
extern void ObiTearableCloth_LoadBlueprint_m1EF48A55B465C2152873E8016AF3246D09A9B6B0 (void);
// 0x000000E6 System.Void Obi.ObiTearableCloth::UnloadBlueprint(Obi.ObiSolver)
extern void ObiTearableCloth_UnloadBlueprint_m2C5B272467836FBE613772B64769919BAEAE405E (void);
// 0x000000E7 System.Void Obi.ObiTearableCloth::SetupRuntimeConstraints()
extern void ObiTearableCloth_SetupRuntimeConstraints_m352FA8CB0E8A4C38B93748260EF2809F6D999F5B (void);
// 0x000000E8 System.Void Obi.ObiTearableCloth::OnValidate()
extern void ObiTearableCloth_OnValidate_mC48629E37DE0E229B250AD3D02867D30D1D594C8 (void);
// 0x000000E9 System.Void Obi.ObiTearableCloth::Substep(System.Single)
extern void ObiTearableCloth_Substep_m84B4C10A833B11DF53CE4A21472EC6618EC06ECE (void);
// 0x000000EA System.Void Obi.ObiTearableCloth::ApplyTearing(System.Single)
extern void ObiTearableCloth_ApplyTearing_m58E1240278F594B2879FA2FAC25111E15BD6B92C (void);
// 0x000000EB System.Boolean Obi.ObiTearableCloth::Tear(Obi.StructuralConstraint)
extern void ObiTearableCloth_Tear_mBB5C8D532C81E303B4CE3248420B41CE8A3BFFC1 (void);
// 0x000000EC System.Boolean Obi.ObiTearableCloth::TopologySplitAttempt(System.Int32&,System.Int32&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>,System.Collections.Generic.HashSet`1<System.Int32>)
extern void ObiTearableCloth_TopologySplitAttempt_m012C38617352C8DC07B3FC6D3458493AAFC5041C (void);
// 0x000000ED System.Void Obi.ObiTearableCloth::SplitParticle(System.Int32)
extern void ObiTearableCloth_SplitParticle_mC3907A3EC8864E637067EA7D77A7CA30802044E9 (void);
// 0x000000EE System.Void Obi.ObiTearableCloth::WeakenCutPoint(System.Int32,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiTearableCloth_WeakenCutPoint_m05580FE624C9F5DC2FE3DB60191E8EA6EE4D0A53 (void);
// 0x000000EF System.Void Obi.ObiTearableCloth::ClassifyFaces(Obi.HalfEdgeMesh/Vertex,UnityEngine.Plane,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiTearableCloth_ClassifyFaces_m6A1EB9D5D3BAEE86E0017E48163380A6F1446A87 (void);
// 0x000000F0 System.Boolean Obi.ObiTearableCloth::SplitTopologyAtVertex(System.Int32,UnityEngine.Plane,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>,System.Collections.Generic.HashSet`1<System.Int32>)
extern void ObiTearableCloth_SplitTopologyAtVertex_m70A2A43A0F3EC4030C822C5AECCB618B37EC3D13 (void);
// 0x000000F1 System.Void Obi.ObiTearableCloth::UpdateTornDistanceConstraints(System.Collections.Generic.HashSet`1<System.Int32>)
extern void ObiTearableCloth_UpdateTornDistanceConstraints_m0650A836BF04788C4A67D0017569FD2D66D7DBD8 (void);
// 0x000000F2 System.Void Obi.ObiTearableCloth::UpdateTornBendConstraints(System.Int32)
extern void ObiTearableCloth_UpdateTornBendConstraints_m6366F12EFB3806E932D03202CB07ECF729DF9611 (void);
// 0x000000F3 System.Void Obi.ObiTearableCloth::UpdateDeformableTriangles()
extern void ObiTearableCloth_UpdateDeformableTriangles_m719EBB29E6926ED8D1AD437B74C746D34DD6C6BE (void);
// 0x000000F4 System.Void Obi.ObiTearableCloth::OnDrawGizmosSelected()
extern void ObiTearableCloth_OnDrawGizmosSelected_m167AACF10A6B65D3ED9A2F132EFFD93F68BF0011 (void);
// 0x000000F5 System.Void Obi.ObiTearableCloth::Update()
extern void ObiTearableCloth_Update_m336DD8214A6E58666ED7A3B971FA9D5251D24155 (void);
// 0x000000F6 System.Void Obi.ObiTearableCloth::.ctor()
extern void ObiTearableCloth__ctor_m2F7DADF87737E8DEDBCAE4BD55383CE966AD925B (void);
// 0x000000F7 System.Void Obi.ObiTearableCloth::.cctor()
extern void ObiTearableCloth__cctor_m68C6A57547A2A8B14B87BA2954B8E1F8FB0D5EC4 (void);
// 0x000000F8 System.Void Obi.ObiTearableCloth/ClothTornCallback::.ctor(System.Object,System.IntPtr)
extern void ClothTornCallback__ctor_mFE1A20090B1772B8603F4E182EDD9A74B3317BB9 (void);
// 0x000000F9 System.Void Obi.ObiTearableCloth/ClothTornCallback::Invoke(Obi.ObiTearableCloth,Obi.ObiTearableCloth/ObiClothTornEventArgs)
extern void ClothTornCallback_Invoke_m8722453F21056D8FB4D156A024E92CC9C4870543 (void);
// 0x000000FA System.IAsyncResult Obi.ObiTearableCloth/ClothTornCallback::BeginInvoke(Obi.ObiTearableCloth,Obi.ObiTearableCloth/ObiClothTornEventArgs,System.AsyncCallback,System.Object)
extern void ClothTornCallback_BeginInvoke_m909A628C8D63E1E980C7F3A619C86E6D532F5572 (void);
// 0x000000FB System.Void Obi.ObiTearableCloth/ClothTornCallback::EndInvoke(System.IAsyncResult)
extern void ClothTornCallback_EndInvoke_m4EE457920329435479957813272A88931CC2914F (void);
// 0x000000FC System.Void Obi.ObiTearableCloth/ObiClothTornEventArgs::.ctor(Obi.StructuralConstraint,System.Int32,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiClothTornEventArgs__ctor_m6807C72DB2AA9763890282409B9C3B199663AFDB (void);
// 0x000000FD System.Void Obi.ObiTearableCloth/<>c::.cctor()
extern void U3CU3Ec__cctor_m4D75FDE740AC5CF001A128CC9C5C2C017FB3946B (void);
// 0x000000FE System.Void Obi.ObiTearableCloth/<>c::.ctor()
extern void U3CU3Ec__ctor_m18EF89102B8FF84031744A82A5EE4C1007BC7E31 (void);
// 0x000000FF System.Int32 Obi.ObiTearableCloth/<>c::<ApplyTearing>b__24_0(Obi.StructuralConstraint,Obi.StructuralConstraint)
extern void U3CU3Ec_U3CApplyTearingU3Eb__24_0_m9CC26D4E8A6074AA2D35CC247E51C1B3FF0D97CB (void);
// 0x00000100 System.Boolean Obi.ObiClothBlueprint::get_usesTethers()
extern void ObiClothBlueprint_get_usesTethers_mEF8E423B48ED084C6C645ECC5A44886C1ADF69AA (void);
// 0x00000101 System.Collections.IEnumerator Obi.ObiClothBlueprint::Initialize()
extern void ObiClothBlueprint_Initialize_mB541B50B4B6C0D58EDE43368C99D89F1690B4AF7 (void);
// 0x00000102 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateDistanceConstraints()
extern void ObiClothBlueprint_CreateDistanceConstraints_m1570C2E0E2E30FF955FAED78EB8DF6C12ED36B65 (void);
// 0x00000103 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateAerodynamicConstraints()
extern void ObiClothBlueprint_CreateAerodynamicConstraints_mB62C6FAE788BF62F4C9B9D2D437136E90995A256 (void);
// 0x00000104 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateBendingConstraints()
extern void ObiClothBlueprint_CreateBendingConstraints_m94A0CAEBCCD4E334449EA330C0D77D22333B0B31 (void);
// 0x00000105 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateVolumeConstraints()
extern void ObiClothBlueprint_CreateVolumeConstraints_m61301FCFCDA8C8B2F6B27808ED4DBEB2F4EE5540 (void);
// 0x00000106 System.Void Obi.ObiClothBlueprint::ClearTethers()
extern void ObiClothBlueprint_ClearTethers_m39BE3E2A63684E6D4A9801AA297EA4259978FA6A (void);
// 0x00000107 System.Collections.Generic.List`1<System.Collections.Generic.HashSet`1<System.Int32>> Obi.ObiClothBlueprint::GenerateIslands(System.Collections.Generic.IEnumerable`1<System.Int32>,System.Func`2<System.Int32,System.Boolean>)
extern void ObiClothBlueprint_GenerateIslands_m464C75ADB17A516E2F605ABDC87C34F65D984247 (void);
// 0x00000108 System.Void Obi.ObiClothBlueprint::GenerateTethers(System.Boolean[])
extern void ObiClothBlueprint_GenerateTethers_m09AA686B87C84120527DA07B296D7AC75337E698 (void);
// 0x00000109 System.Void Obi.ObiClothBlueprint::GenerateTethersForIsland(System.Collections.Generic.HashSet`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>,System.Boolean[],System.Int32)
extern void ObiClothBlueprint_GenerateTethersForIsland_m2D9E0618A7595F13C233B6F082A7FB39774E5E4F (void);
// 0x0000010A System.Void Obi.ObiClothBlueprint::.ctor()
extern void ObiClothBlueprint__ctor_m7BC51165B1F52A611E3C44A8C72C8C56DB7FC8FD (void);
// 0x0000010B System.Void Obi.ObiClothBlueprint/<Initialize>d__2::.ctor(System.Int32)
extern void U3CInitializeU3Ed__2__ctor_mD89C5AE9C9C80916FB8FFD79CFC095D19AE5D57D (void);
// 0x0000010C System.Void Obi.ObiClothBlueprint/<Initialize>d__2::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__2_System_IDisposable_Dispose_m7004C46EC7D295E0968AF94ABE4F7E8BF273593F (void);
// 0x0000010D System.Boolean Obi.ObiClothBlueprint/<Initialize>d__2::MoveNext()
extern void U3CInitializeU3Ed__2_MoveNext_m8709F8E8082C3720C72A9D9297E2E124E10F925E (void);
// 0x0000010E System.Object Obi.ObiClothBlueprint/<Initialize>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E02322F5A0D23311828213F94F9CAE0E0750934 (void);
// 0x0000010F System.Void Obi.ObiClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mFB62B5B536F9DCCFFA18FAD799255FBB349E0CB2 (void);
// 0x00000110 System.Object Obi.ObiClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m74CF369892ADB61833B9A3B086E1C2EC78434A01 (void);
// 0x00000111 System.Void Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::.ctor(System.Int32)
extern void U3CCreateDistanceConstraintsU3Ed__3__ctor_m09BD3C57322A5EF62A54F6731CDF444FA9BB2E09 (void);
// 0x00000112 System.Void Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.IDisposable.Dispose()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_mDAD5189CF5BE3E2386462303C5D8363F9695B6E9 (void);
// 0x00000113 System.Boolean Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::MoveNext()
extern void U3CCreateDistanceConstraintsU3Ed__3_MoveNext_m4284389740A92E0CF3454BE0990E2093E68CAFB7 (void);
// 0x00000114 System.Object Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D1E0E87E7FD71C4EC798103826D6140BFD871F7 (void);
// 0x00000115 System.Void Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mC8A761BFD80CE17A1F65517143C99E6A20387CDB (void);
// 0x00000116 System.Object Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8896FC2746D379EEBF8E8501E3C010284F085BC2 (void);
// 0x00000117 System.Void Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::.ctor(System.Int32)
extern void U3CCreateAerodynamicConstraintsU3Ed__4__ctor_m4FC51372D57AB411957AE946A83F3EFDC329A358 (void);
// 0x00000118 System.Void Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.IDisposable.Dispose()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_IDisposable_Dispose_m75085E8EEE584CA48781239A3A035272ED0D28E4 (void);
// 0x00000119 System.Boolean Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::MoveNext()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_MoveNext_m1D5C0373C79C2575C46F7DCDEB6EA324288F2F0A (void);
// 0x0000011A System.Object Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADC37B2982CCFCDA50AB1C551B944F7022C3FF0F (void);
// 0x0000011B System.Void Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_mB2CC4104E9C7AB68C42D91F0D25AAD0316D92347 (void);
// 0x0000011C System.Object Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m993A90ABEEA80806AA028F49D6215A00BD8CAB0C (void);
// 0x0000011D System.Void Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::.ctor(System.Int32)
extern void U3CCreateBendingConstraintsU3Ed__5__ctor_m216136A55AF138945D5561DC181CE943371035F6 (void);
// 0x0000011E System.Void Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.IDisposable.Dispose()
extern void U3CCreateBendingConstraintsU3Ed__5_System_IDisposable_Dispose_m6821B7A23670BE0F2DE199C41EE831168CA242AD (void);
// 0x0000011F System.Boolean Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::MoveNext()
extern void U3CCreateBendingConstraintsU3Ed__5_MoveNext_m7329FBDB0B5B5BCC7DE22B993DF8A03791187568 (void);
// 0x00000120 System.Object Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateBendingConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE75E1D32AFBD9B07503268FC3E0BA64851456C6 (void);
// 0x00000121 System.Void Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.Collections.IEnumerator.Reset()
extern void U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m731DAB5B414FCB6F5253FAE15495E32006F1400E (void);
// 0x00000122 System.Object Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_m5767B962F68DA952AABEA5D0B178933A58E445E5 (void);
// 0x00000123 System.Void Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::.ctor(System.Int32)
extern void U3CCreateVolumeConstraintsU3Ed__6__ctor_m88D1946A7A914EC0CC61A5D2BAB66310AB4C807C (void);
// 0x00000124 System.Void Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.IDisposable.Dispose()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_IDisposable_Dispose_m051782DF73D0AA384FAEB7DC1642C3598EC916AE (void);
// 0x00000125 System.Boolean Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::MoveNext()
extern void U3CCreateVolumeConstraintsU3Ed__6_MoveNext_m7CE8307D10CFABCD59A9D95C245F2EF758EB9FFF (void);
// 0x00000126 System.Object Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m243C532028DFB65774C84E5FDFA0528EC46DE0B1 (void);
// 0x00000127 System.Void Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_mED3CB28B77CD40AE79F199DE53151239982A3BBD (void);
// 0x00000128 System.Object Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_m49682C0DC4ADBD851E1ED3B1631283CBC211B9E4 (void);
// 0x00000129 System.Void Obi.ObiClothBlueprint/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mB4D1F41E7404154EE84C80A3FE7A1C124DDA0F98 (void);
// 0x0000012A System.Boolean Obi.ObiClothBlueprint/<>c__DisplayClass10_0::<GenerateTethersForIsland>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CGenerateTethersForIslandU3Eb__0_m64F724014094E65DF04665BAF91C1C927C3DDB6E (void);
// 0x0000012B System.Void Obi.ObiClothBlueprint/<>c::.cctor()
extern void U3CU3Ec__cctor_m10D357B87EFBAE919239FF9DEF0C29DC529976AD (void);
// 0x0000012C System.Void Obi.ObiClothBlueprint/<>c::.ctor()
extern void U3CU3Ec__ctor_mCFA8AD25CE5EF86DA9F38676268211EF9967F0D9 (void);
// 0x0000012D System.Int32 Obi.ObiClothBlueprint/<>c::<GenerateTethersForIsland>b__10_1(System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>)
extern void U3CU3Ec_U3CGenerateTethersForIslandU3Eb__10_1_m874449CF067336C582EDBBAD64435C2AA7E0E70B (void);
// 0x0000012E Obi.HalfEdgeMesh Obi.ObiClothBlueprintBase::get_Topology()
extern void ObiClothBlueprintBase_get_Topology_mE37FD318FEB32A3D2B3B5CE3CBFCCE98D6F3C15A (void);
// 0x0000012F System.Void Obi.ObiClothBlueprintBase::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiClothBlueprintBase_SwapWithFirstInactiveParticle_mC690773C23D9B72FA5D4C4C3838219FEF0E797DD (void);
// 0x00000130 System.Collections.IEnumerator Obi.ObiClothBlueprintBase::GenerateDeformableTriangles()
extern void ObiClothBlueprintBase_GenerateDeformableTriangles_m412F7EDE785459A5F256F70B1BCC07A6DE86E97A (void);
// 0x00000131 System.Collections.IEnumerator Obi.ObiClothBlueprintBase::CreateSimplices()
extern void ObiClothBlueprintBase_CreateSimplices_m792C9C38FCE60CB1F28D70F757E38C50C8B381DD (void);
// 0x00000132 System.Void Obi.ObiClothBlueprintBase::.ctor()
extern void ObiClothBlueprintBase__ctor_m80CB63BEF2CCBA303A83241741195400E4568E38 (void);
// 0x00000133 System.Void Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::.ctor(System.Int32)
extern void U3CGenerateDeformableTrianglesU3Ed__8__ctor_m0B4BEE607BAA1A2C0EBFEC240D52F91801C0B17C (void);
// 0x00000134 System.Void Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.IDisposable.Dispose()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_IDisposable_Dispose_m9FACA3940826A6F604CE1ED095A420F3B70BEF31 (void);
// 0x00000135 System.Boolean Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::MoveNext()
extern void U3CGenerateDeformableTrianglesU3Ed__8_MoveNext_mAF7BF73A00CF8BE7EA65AA18680FFDCC98DD3971 (void);
// 0x00000136 System.Object Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94D6F01A98AAF5D8ACB13EB4E5C66B583F950A17 (void);
// 0x00000137 System.Void Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.Collections.IEnumerator.Reset()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_Reset_mFF432431540D581589CE78CB502104BA515F44A5 (void);
// 0x00000138 System.Object Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_get_Current_mCAC139C7F6132507DA4DD623B3F44AB50ABED2F6 (void);
// 0x00000139 System.Void Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::.ctor(System.Int32)
extern void U3CCreateSimplicesU3Ed__9__ctor_m6BDB71B25348530DD662CD094363B8BD7290B3F8 (void);
// 0x0000013A System.Void Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.IDisposable.Dispose()
extern void U3CCreateSimplicesU3Ed__9_System_IDisposable_Dispose_m9D9E3AF192D6A21176BC771CD6B070A775A15627 (void);
// 0x0000013B System.Boolean Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::MoveNext()
extern void U3CCreateSimplicesU3Ed__9_MoveNext_m8E143004A4C17F2DC72AD5CE7F77847338308DD5 (void);
// 0x0000013C System.Object Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateSimplicesU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF2A1B6D8EB60FC887ACEA5CA1A98501EFC8DBA8 (void);
// 0x0000013D System.Void Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.Collections.IEnumerator.Reset()
extern void U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_Reset_m168698F93B24A0D1D8BF0D33260667A9AE613315 (void);
// 0x0000013E System.Object Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_get_Current_mA5534B33C95835ED9EE2DD141CC15F742A981072 (void);
// 0x0000013F System.Boolean Obi.ObiSkinnedClothBlueprint::get_usesTethers()
extern void ObiSkinnedClothBlueprint_get_usesTethers_mBD787F53836BFDEAA10AD88B6EBBB3B8370EBECB (void);
// 0x00000140 System.Collections.IEnumerator Obi.ObiSkinnedClothBlueprint::Initialize()
extern void ObiSkinnedClothBlueprint_Initialize_mEEEBD3D9C13B8A98A3761D7E712512A3CCC377B5 (void);
// 0x00000141 System.Collections.IEnumerator Obi.ObiSkinnedClothBlueprint::CreateSkinConstraints()
extern void ObiSkinnedClothBlueprint_CreateSkinConstraints_m58C14E2205A69B9015B3F01ADF03CB072338C3A7 (void);
// 0x00000142 System.Void Obi.ObiSkinnedClothBlueprint::.ctor()
extern void ObiSkinnedClothBlueprint__ctor_m2E1C425AA22A7BCBE3322AEA18CADF7A4B159F4F (void);
// 0x00000143 System.Void Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::.ctor(System.Int32)
extern void U3CInitializeU3Ed__2__ctor_mBE818931D3B01A168B9747C7573E4B406C0EB303 (void);
// 0x00000144 System.Void Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__2_System_IDisposable_Dispose_m0BDE2AC7C8E038318E8B83BAC143BD3363720DEC (void);
// 0x00000145 System.Boolean Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::MoveNext()
extern void U3CInitializeU3Ed__2_MoveNext_mB04B335715B998CC2A7BB06BD1AFF7D17FAB8FEC (void);
// 0x00000146 System.Object Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB92A210007613B2909D214DF7B445C938F4C6EF (void);
// 0x00000147 System.Void Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mC9AF6B8BE46FC43D8A1334E15A189A796056CBE6 (void);
// 0x00000148 System.Object Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m22FAD500DB712F880583DA421A4A69DE13357712 (void);
// 0x00000149 System.Void Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::.ctor(System.Int32)
extern void U3CCreateSkinConstraintsU3Ed__3__ctor_mD52C4DA6057FEE9E99D0D32779C65C6EF756FA28 (void);
// 0x0000014A System.Void Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.IDisposable.Dispose()
extern void U3CCreateSkinConstraintsU3Ed__3_System_IDisposable_Dispose_m2188832D0686304E2FA1C1E3CBC9244B11DB4EA2 (void);
// 0x0000014B System.Boolean Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::MoveNext()
extern void U3CCreateSkinConstraintsU3Ed__3_MoveNext_mD485952F9778BAFF68B8A405A65CA0874C48730C (void);
// 0x0000014C System.Object Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateSkinConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE5966D180DA31A5B8CC609EA3CC94FD482431528 (void);
// 0x0000014D System.Void Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m3423E9E200FEB13FEF7BEC122845FB66403E2A30 (void);
// 0x0000014E System.Object Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8C97E9A1F24B2A98644A9E03FD1DE0A49AB21A05 (void);
// 0x0000014F System.Int32 Obi.ObiTearableClothBlueprint::get_PooledParticles()
extern void ObiTearableClothBlueprint_get_PooledParticles_m0184AD47C028F8E14C23E2922244E1640215DDC3 (void);
// 0x00000150 System.Boolean Obi.ObiTearableClothBlueprint::get_usesTethers()
extern void ObiTearableClothBlueprint_get_usesTethers_m025E36BFB3E22AEA5B754ECCBDAC2EBC472FD2BF (void);
// 0x00000151 System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::Initialize()
extern void ObiTearableClothBlueprint_Initialize_mEE76304F3987B091FC3F24395B52A9776C85B7B6 (void);
// 0x00000152 System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::CreateDistanceConstraints()
extern void ObiTearableClothBlueprint_CreateDistanceConstraints_mFD063AE5F8C647760AC90B516949FF31D0238EB5 (void);
// 0x00000153 System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::CreateInitialDistanceConstraints(System.Collections.Generic.List`1<System.Int32>)
extern void ObiTearableClothBlueprint_CreateInitialDistanceConstraints_mA1AD7123377815A2D7DCADC9CEFE1C7E450B718D (void);
// 0x00000154 System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::CreatePooledDistanceConstraints(System.Collections.Generic.List`1<System.Int32>)
extern void ObiTearableClothBlueprint_CreatePooledDistanceConstraints_mC50382B6EF8301BB863CB65BBF73FC20FD8B83BC (void);
// 0x00000155 System.Void Obi.ObiTearableClothBlueprint::.ctor()
extern void ObiTearableClothBlueprint__ctor_mAFEF2B7C7182E5D4B8C2E44D862699556AD80D15 (void);
// 0x00000156 System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::.ctor(System.Int32)
extern void U3CInitializeU3Ed__8__ctor_mE1FD39996C2F57CF4C0B2C10C7CB001423DE2381 (void);
// 0x00000157 System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__8_System_IDisposable_Dispose_m6A03B2E04499DB25B9E769956B700DB301ECA55A (void);
// 0x00000158 System.Boolean Obi.ObiTearableClothBlueprint/<Initialize>d__8::MoveNext()
extern void U3CInitializeU3Ed__8_MoveNext_m2028F8C3F5AB82D80D35ABCDAEF7DBF1767E1171 (void);
// 0x00000159 System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D1131F06EECDDE2B78AD00FB36522B66AD3538C (void);
// 0x0000015A System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0 (void);
// 0x0000015B System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m6ADA7C8F48F23CC6603E2ECDDF2999F8B85CF87F (void);
// 0x0000015C System.Void Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::.ctor(System.Int32)
extern void U3CCreateDistanceConstraintsU3Ed__9__ctor_m225975D257A624A724A08C3E1FAF8C2DF4654668 (void);
// 0x0000015D System.Void Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.IDisposable.Dispose()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_IDisposable_Dispose_mF9F663E01209D2ADCFC88D6EB3697151496279D4 (void);
// 0x0000015E System.Boolean Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::MoveNext()
extern void U3CCreateDistanceConstraintsU3Ed__9_MoveNext_mB7DADFE9E579B8C1A858C9EEC79439CDD69EABCC (void);
// 0x0000015F System.Object Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC86A1722C558C6E0F887CE3CE9B2CDFA9D75BE (void);
// 0x00000160 System.Void Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.Collections.IEnumerator.Reset()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_Reset_mDA4DBAC64C3B419B5FD761D86BEB3A05984873E7 (void);
// 0x00000161 System.Object Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_get_Current_mA37DF615EFA87C9F148D307AD7FC73526A9207CE (void);
// 0x00000162 System.Void Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::.ctor(System.Int32)
extern void U3CCreateInitialDistanceConstraintsU3Ed__10__ctor_mB3B7E5310F4928BB37C9512E9F40ACFC3F140C9F (void);
// 0x00000163 System.Void Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.IDisposable.Dispose()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_IDisposable_Dispose_m39D16A16F80402BC68F47E0A9F83F7BC9CDFE417 (void);
// 0x00000164 System.Boolean Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::MoveNext()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_MoveNext_mD78D4A1D7991FB01E3150A015B30B3C8E19ECF0F (void);
// 0x00000165 System.Object Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF3452EB8DE1B9490F900218D00937A623C33312 (void);
// 0x00000166 System.Void Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.Collections.IEnumerator.Reset()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_Reset_mC4356FC9C856276AFA630708CF08D9DB17A3E189 (void);
// 0x00000167 System.Object Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D3BB3B7750F054BA802A619B79F0A538D1D6FE (void);
// 0x00000168 System.Void Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::.ctor(System.Int32)
extern void U3CCreatePooledDistanceConstraintsU3Ed__11__ctor_m3C790EFEB6A41148BFA287BBF9EE4C00327BEB64 (void);
// 0x00000169 System.Void Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.IDisposable.Dispose()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_IDisposable_Dispose_m5B3AC37A9F1F1BAED5319C961D4121B7B17D08A0 (void);
// 0x0000016A System.Boolean Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::MoveNext()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_MoveNext_m39BB550821EBBBF28571E7BA29467CC2F9204F11 (void);
// 0x0000016B System.Object Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7BCE5ABEDC2654A1A0453DE030EF2ED8CDC3FC (void);
// 0x0000016C System.Void Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.Collections.IEnumerator.Reset()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_Reset_m824F230E26816CB43250CD6BA6C491C34058CD38 (void);
// 0x0000016D System.Object Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_get_Current_m684F6B83E2834AA135F20041DDCF51BFE4ABB3A0 (void);
// 0x0000016E System.Boolean Obi.HalfEdgeMesh::get_ContainsData()
extern void HalfEdgeMesh_get_ContainsData_mFDC9D71E52A1481FA1D537067E1DC7A061354B36 (void);
// 0x0000016F System.Boolean Obi.HalfEdgeMesh::get_closed()
extern void HalfEdgeMesh_get_closed_m2661313240CDAE0D2AFBD3015313CC467F901519 (void);
// 0x00000170 System.Single Obi.HalfEdgeMesh::get_area()
extern void HalfEdgeMesh_get_area_m30E2F4CBCA561EA3811FAC94E781D84C0141FD3A (void);
// 0x00000171 System.Single Obi.HalfEdgeMesh::get_volume()
extern void HalfEdgeMesh_get_volume_m7E0D5685C7E215D5AB749185F4EFB3B909C1A20B (void);
// 0x00000172 System.Void Obi.HalfEdgeMesh::.ctor()
extern void HalfEdgeMesh__ctor_m641C71B020EB960E23164DEABD3019B25C6F5404 (void);
// 0x00000173 System.Void Obi.HalfEdgeMesh::.ctor(Obi.HalfEdgeMesh)
extern void HalfEdgeMesh__ctor_m58E71C3E236533676CBC924DB3E03E914BBFB8BF (void);
// 0x00000174 System.Void Obi.HalfEdgeMesh::Generate()
extern void HalfEdgeMesh_Generate_mD60A36F035F35EBE9F282926F0C8E06962265CC7 (void);
// 0x00000175 System.Void Obi.HalfEdgeMesh::CalculateRestNormals()
extern void HalfEdgeMesh_CalculateRestNormals_mBD6C6FB703AFD8CDC27D5C247EDC36210DC72F25 (void);
// 0x00000176 System.Void Obi.HalfEdgeMesh::CalculateRestOrientations()
extern void HalfEdgeMesh_CalculateRestOrientations_mFAE7DA9079678E610B968168F3BCFC49794D32C4 (void);
// 0x00000177 System.Void Obi.HalfEdgeMesh::SwapVertices(System.Int32,System.Int32)
extern void HalfEdgeMesh_SwapVertices_m7F3C9ED658FE5675EDEA0FE43227BCACB6034413 (void);
// 0x00000178 System.Int32 Obi.HalfEdgeMesh::GetHalfEdgeStartVertex(Obi.HalfEdgeMesh/HalfEdge)
extern void HalfEdgeMesh_GetHalfEdgeStartVertex_m4BB747F2E34E9DAA7686C4EBE135F0ACF5CC96EC (void);
// 0x00000179 System.Single Obi.HalfEdgeMesh::GetFaceArea(Obi.HalfEdgeMesh/Face)
extern void HalfEdgeMesh_GetFaceArea_m27C2ADF35DAB7C31E951DE1EED24C9CA6472FB6E (void);
// 0x0000017A System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Vertex> Obi.HalfEdgeMesh::GetNeighbourVerticesEnumerator(Obi.HalfEdgeMesh/Vertex)
extern void HalfEdgeMesh_GetNeighbourVerticesEnumerator_m5D851186C1DD0DDF3C79F394DC9946C6664AE4E2 (void);
// 0x0000017B System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh::GetNeighbourEdgesEnumerator(Obi.HalfEdgeMesh/Vertex)
extern void HalfEdgeMesh_GetNeighbourEdgesEnumerator_m46006FB9A047B16085C6EF70CC20209053622818 (void);
// 0x0000017C System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Face> Obi.HalfEdgeMesh::GetNeighbourFacesEnumerator(Obi.HalfEdgeMesh/Vertex)
extern void HalfEdgeMesh_GetNeighbourFacesEnumerator_m055B1E4B59B1C20B1C6F542EE88DA13DBE7DF26D (void);
// 0x0000017D System.Collections.Generic.List`1<System.Int32> Obi.HalfEdgeMesh::GetEdgeList()
extern void HalfEdgeMesh_GetEdgeList_m96FD781747444BC98F4E83CC20DFF87FBEDBB0D2 (void);
// 0x0000017E System.Boolean Obi.HalfEdgeMesh::IsSplit(System.Int32)
extern void HalfEdgeMesh_IsSplit_m4ADC42896318F8A8193FE1776008C804CE230C3F (void);
// 0x0000017F System.Void Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::.ctor(System.Int32)
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31__ctor_mF8ED85D8453E3677145D2F9202A2F2CBFC6929E9 (void);
// 0x00000180 System.Void Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.IDisposable.Dispose()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_IDisposable_Dispose_mF5E674694F95EB02F8B41CC0A6A0AF7D3C1276DE (void);
// 0x00000181 System.Boolean Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::MoveNext()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_MoveNext_mAFAABF824BEB8AE1F0EB2DBA7CEFB03ABF6140B1 (void);
// 0x00000182 Obi.HalfEdgeMesh/Vertex Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.Generic.IEnumerator<Obi.HalfEdgeMesh.Vertex>.get_Current()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_VertexU3E_get_Current_mB8974C8C5F9B458C49BBBD99546E03A15E277212 (void);
// 0x00000183 System.Void Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.IEnumerator.Reset()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_Reset_mF9C32E78CBDC8991CB3AA3E8517D2434CB4060C6 (void);
// 0x00000184 System.Object Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_get_Current_mE029EC97DE5EBDB2302D6C9C4E042A8D402111CB (void);
// 0x00000185 System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/Vertex> Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.Generic.IEnumerable<Obi.HalfEdgeMesh.Vertex>.GetEnumerator()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_VertexU3E_GetEnumerator_mAF607573C51602451E445A9210B356A40C744B6E (void);
// 0x00000186 System.Collections.IEnumerator Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerable_GetEnumerator_m3F71BE162D53C1A40174A26D4BEB7864B8D1213B (void);
// 0x00000187 System.Void Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::.ctor(System.Int32)
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32__ctor_mD46F103C7C9B4F9B8A0AD31F7AD7D11C248D9E45 (void);
// 0x00000188 System.Void Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.IDisposable.Dispose()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_IDisposable_Dispose_m2DCA4922446B3F5F983E88E30DE79278DEDE5613 (void);
// 0x00000189 System.Boolean Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::MoveNext()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_MoveNext_m97A4063AAC540677687D09D05E0E4922FA65094D (void);
// 0x0000018A Obi.HalfEdgeMesh/HalfEdge Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.Generic.IEnumerator<Obi.HalfEdgeMesh.HalfEdge>.get_Current()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_HalfEdgeU3E_get_Current_mCD5275CC511E91FCA17A93FC067DD0201E6C87E7 (void);
// 0x0000018B System.Void Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.IEnumerator.Reset()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_Reset_mE09272AC7F4A5074F95EC162B00DA8152F551EFA (void);
// 0x0000018C System.Object Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_get_Current_mD8A977C5E95E52F283E5BB7BCAB03DB08B4363BA (void);
// 0x0000018D System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.Generic.IEnumerable<Obi.HalfEdgeMesh.HalfEdge>.GetEnumerator()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_HalfEdgeU3E_GetEnumerator_mC4B54D7954EA17442D6D38B91B6E903FB1C54C3B (void);
// 0x0000018E System.Collections.IEnumerator Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerable_GetEnumerator_m2F65C8046D5AC6277F7723BF04D7AEF6A27B507E (void);
// 0x0000018F System.Void Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::.ctor(System.Int32)
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33__ctor_m4C8DC2E13D54FFEEB86EF6AE20DEEA7A1E918A6D (void);
// 0x00000190 System.Void Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.IDisposable.Dispose()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_IDisposable_Dispose_m972ADD603D342F7AD8F1317F6B20179617A042DF (void);
// 0x00000191 System.Boolean Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::MoveNext()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_MoveNext_mB03F1EEE1294832A224C3F46D64F77F89F662CD0 (void);
// 0x00000192 Obi.HalfEdgeMesh/Face Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.Generic.IEnumerator<Obi.HalfEdgeMesh.Face>.get_Current()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_FaceU3E_get_Current_m11DEA6ABA872F8CBB0E39CAA35823EE6B5E5C15A (void);
// 0x00000193 System.Void Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.IEnumerator.Reset()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_Reset_m9E664CABCDA715691724536236FAEBBB657DD313 (void);
// 0x00000194 System.Object Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_get_Current_mBBBADC2F55A8568A51101245F7598E952B837304 (void);
// 0x00000195 System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/Face> Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.Generic.IEnumerable<Obi.HalfEdgeMesh.Face>.GetEnumerator()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_FaceU3E_GetEnumerator_mF45B315F345EC2F18D84B7400ECD0FC83B7BE26F (void);
// 0x00000196 System.Collections.IEnumerator Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m66EB03D0F227B3A19E153D78CC869BA345527948 (void);
// 0x00000197 System.Void Obi.ObiTriangleSkinMap::set_master(Obi.ObiClothBlueprintBase)
extern void ObiTriangleSkinMap_set_master_m39BC02C26C0865DBE331455792D67D6BD8AD56C7 (void);
// 0x00000198 Obi.ObiClothBlueprintBase Obi.ObiTriangleSkinMap::get_master()
extern void ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323 (void);
// 0x00000199 System.Void Obi.ObiTriangleSkinMap::set_slave(UnityEngine.Mesh)
extern void ObiTriangleSkinMap_set_slave_mA52CD5B38D9EC50DA0E98BDDA5F35B4E971B11D7 (void);
// 0x0000019A UnityEngine.Mesh Obi.ObiTriangleSkinMap::get_slave()
extern void ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37 (void);
// 0x0000019B System.Void Obi.ObiTriangleSkinMap::OnEnable()
extern void ObiTriangleSkinMap_OnEnable_m1A2A5D0FA393ACCB977142E487098C16DAEA712D (void);
// 0x0000019C System.Void Obi.ObiTriangleSkinMap::Clear()
extern void ObiTriangleSkinMap_Clear_mAE6BED8941D24054DD5ACAF5C13431FF25515143 (void);
// 0x0000019D System.Void Obi.ObiTriangleSkinMap::ValidateMasterChannels(System.Boolean)
extern void ObiTriangleSkinMap_ValidateMasterChannels_mA6CDFE2BEDBC318272453C575BF1BC406929442C (void);
// 0x0000019E System.Void Obi.ObiTriangleSkinMap::ValidateSlaveChannels(System.Boolean)
extern void ObiTriangleSkinMap_ValidateSlaveChannels_m1971F2714B57EE50C970D39CD5D6BA903E8B79A6 (void);
// 0x0000019F System.Void Obi.ObiTriangleSkinMap::CopyChannel(System.UInt32[],System.Int32,System.Int32)
extern void ObiTriangleSkinMap_CopyChannel_m1B53803DFBEC32A0B39F6C61774D965DB2B01D47 (void);
// 0x000001A0 System.Void Obi.ObiTriangleSkinMap::FillChannel(System.UInt32[],System.Int32)
extern void ObiTriangleSkinMap_FillChannel_mA0F0F131BF0D10D9DDC2FB2E7AA2CAAF79576D6F (void);
// 0x000001A1 System.Void Obi.ObiTriangleSkinMap::ClearChannel(System.UInt32[],System.Int32)
extern void ObiTriangleSkinMap_ClearChannel_m526C2803E32B79B5E868B1BC99F9B48C42711F36 (void);
// 0x000001A2 System.Boolean Obi.ObiTriangleSkinMap::BindToFace(System.Int32,Obi.ObiTriangleSkinMap/MasterFace,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,Obi.ObiTriangleSkinMap/SlaveVertex&)
extern void ObiTriangleSkinMap_BindToFace_mC6D3795B2BEA297D2DE57744229DBEEA82E3DEF3 (void);
// 0x000001A3 System.Single Obi.ObiTriangleSkinMap::GetBarycentricError(UnityEngine.Vector3)
extern void ObiTriangleSkinMap_GetBarycentricError_mC39FA869436CE0D85F611B3C0DE58228E72A4319 (void);
// 0x000001A4 System.Single Obi.ObiTriangleSkinMap::GetFaceMappingError(Obi.ObiTriangleSkinMap/MasterFace,Obi.ObiTriangleSkinMap/SlaveVertex,UnityEngine.Vector3)
extern void ObiTriangleSkinMap_GetFaceMappingError_mEE274B631EC04C834C233252FFA299316F01F697 (void);
// 0x000001A5 System.Boolean Obi.ObiTriangleSkinMap::FindSkinBarycentricCoords(Obi.ObiTriangleSkinMap/MasterFace,UnityEngine.Vector3,System.Int32,System.Single,Obi.ObiTriangleSkinMap/BarycentricPoint&)
extern void ObiTriangleSkinMap_FindSkinBarycentricCoords_m50355A18A36BEB74CD8B45EB49D85A06B18B5920 (void);
// 0x000001A6 System.Collections.IEnumerator Obi.ObiTriangleSkinMap::Bind()
extern void ObiTriangleSkinMap_Bind_m2DE9C31E8338896A2BFD8CCD2C47D0561FA663F9 (void);
// 0x000001A7 System.Void Obi.ObiTriangleSkinMap::.ctor()
extern void ObiTriangleSkinMap__ctor_mD9FC75F05FC3C960044BBA2051378DF793185411 (void);
// 0x000001A8 System.Void Obi.ObiTriangleSkinMap/MasterFace::CacheBarycentricData()
extern void MasterFace_CacheBarycentricData_m05DB432E4FAE84E0A5D863267B948CCDDE4FB120 (void);
// 0x000001A9 System.Boolean Obi.ObiTriangleSkinMap/MasterFace::BarycentricCoords(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void MasterFace_BarycentricCoords_m9D062C8071B60CDBB21459F906FCB9EF23E5EC46 (void);
// 0x000001AA System.Void Obi.ObiTriangleSkinMap/MasterFace::.ctor()
extern void MasterFace__ctor_mB4AB2008F0F78D8A27CBCD816423C06862A6510E (void);
// 0x000001AB System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895 (void);
// 0x000001AC System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Transform)
extern void SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80 (void);
// 0x000001AD System.Void Obi.ObiTriangleSkinMap/SkinTransform::Apply(UnityEngine.Transform)
extern void SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0 (void);
// 0x000001AE UnityEngine.Matrix4x4 Obi.ObiTriangleSkinMap/SkinTransform::GetMatrix4X4()
extern void SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE (void);
// 0x000001AF System.Void Obi.ObiTriangleSkinMap/SkinTransform::Reset()
extern void SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E (void);
// 0x000001B0 Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/BarycentricPoint::get_zero()
extern void BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97 (void);
// 0x000001B1 System.Void Obi.ObiTriangleSkinMap/BarycentricPoint::.ctor(UnityEngine.Vector3,System.Single)
extern void BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F (void);
// 0x000001B2 Obi.ObiTriangleSkinMap/SlaveVertex Obi.ObiTriangleSkinMap/SlaveVertex::get_empty()
extern void SlaveVertex_get_empty_m6B4C7246C11E79FAFFF65C981540626F82F4C5E5 (void);
// 0x000001B3 System.Boolean Obi.ObiTriangleSkinMap/SlaveVertex::get_isEmpty()
extern void SlaveVertex_get_isEmpty_m1B2229CA5E3375CCEDB4898EEBDB72F0EAF2432F (void);
// 0x000001B4 System.Void Obi.ObiTriangleSkinMap/SlaveVertex::.ctor(System.Int32,System.Int32,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint)
extern void SlaveVertex__ctor_mB7C3D7EBC3A235D3D58769095378AE46286BC7A7 (void);
// 0x000001B5 System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::.ctor(System.Int32)
extern void U3CBindU3Ed__31__ctor_mE5C6B10FCF9C0277DFC6D366EB0FD7BE84FB77BE (void);
// 0x000001B6 System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::System.IDisposable.Dispose()
extern void U3CBindU3Ed__31_System_IDisposable_Dispose_m0470FFE716CE5F6D58F2070F1980E5DEF012C319 (void);
// 0x000001B7 System.Boolean Obi.ObiTriangleSkinMap/<Bind>d__31::MoveNext()
extern void U3CBindU3Ed__31_MoveNext_m8C37AFBF1649B64A19A3A721B1324FAB72700C9F (void);
// 0x000001B8 System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m799C177A7037BA730C8BC7776C400B5065B64A16 (void);
// 0x000001B9 System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.IEnumerator.Reset()
extern void U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50 (void);
// 0x000001BA System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_m937FC6C298B8CA31EA931CA6B7205A747ABFC996 (void);
// 0x000001BB System.Void Obi.ObiClothProxy::set_master(Obi.ObiClothRendererBase)
extern void ObiClothProxy_set_master_mA5D20E853B2DAD3E6DF07C2A457477EB4CCB08C1 (void);
// 0x000001BC Obi.ObiClothRendererBase Obi.ObiClothProxy::get_master()
extern void ObiClothProxy_get_master_m682D27ED96F155E4640E7FB680820C35850D7F8D (void);
// 0x000001BD System.Void Obi.ObiClothProxy::OnEnable()
extern void ObiClothProxy_OnEnable_m30F59AE3858AB8FE5CDA82FD83EB93952C43FB08 (void);
// 0x000001BE System.Void Obi.ObiClothProxy::OnDisable()
extern void ObiClothProxy_OnDisable_m97C875EB28CCDEC05EF380AA8816B4AEDF05EE3B (void);
// 0x000001BF System.Void Obi.ObiClothProxy::GetSlaveMeshIfNeeded()
extern void ObiClothProxy_GetSlaveMeshIfNeeded_mC2E1F607B22ACFD8775999EBA93DB56BF994A793 (void);
// 0x000001C0 System.Void Obi.ObiClothProxy::UpdateSkinning(Obi.ObiActor)
extern void ObiClothProxy_UpdateSkinning_m89A644C901FE3F5E6A9DE6D977D18846C11E15A5 (void);
// 0x000001C1 System.Void Obi.ObiClothProxy::.ctor()
extern void ObiClothProxy__ctor_m6A724D2010C7C1126914C3998F85787C7F26C73B (void);
// 0x000001C2 System.Void Obi.ObiClothRenderer::.ctor()
extern void ObiClothRenderer__ctor_mB8C905CC003D1E9ADCB98277CEEE8885244B3A69 (void);
// 0x000001C3 System.Void Obi.ObiClothRendererBase::add_OnRendererUpdated(Obi.ObiActor/ActorCallback)
extern void ObiClothRendererBase_add_OnRendererUpdated_mD0F78B5164967A7079E170FB1758688B3B7B9A53 (void);
// 0x000001C4 System.Void Obi.ObiClothRendererBase::remove_OnRendererUpdated(Obi.ObiActor/ActorCallback)
extern void ObiClothRendererBase_remove_OnRendererUpdated_m31F9D492473F2A469ECDDE8815A8A4ED54AB2FC2 (void);
// 0x000001C5 Obi.HalfEdgeMesh Obi.ObiClothRendererBase::get_topology()
extern void ObiClothRendererBase_get_topology_mC1EA7DC745031C7E2659C8A85C450234FEF7A887 (void);
// 0x000001C6 UnityEngine.Matrix4x4 Obi.ObiClothRendererBase::get_renderMatrix()
extern void ObiClothRendererBase_get_renderMatrix_m59D1188D4E7843373753816E380BE810C87C72A4 (void);
// 0x000001C7 System.Void Obi.ObiClothRendererBase::OnEnable()
extern void ObiClothRendererBase_OnEnable_m22AA0397E99355C620ADF810AE18F4CE79BAB36A (void);
// 0x000001C8 System.Void Obi.ObiClothRendererBase::OnDisable()
extern void ObiClothRendererBase_OnDisable_m46AFAB09595CD3D1D53C4E62BC90D6B1D592D799 (void);
// 0x000001C9 System.Void Obi.ObiClothRendererBase::GetClothMeshData()
extern void ObiClothRendererBase_GetClothMeshData_m88074B0F7E2C1AB37B926B5D5CA19C0B2CC5CD02 (void);
// 0x000001CA System.Void Obi.ObiClothRendererBase::SetClothMeshData()
extern void ObiClothRendererBase_SetClothMeshData_m55D26DF37258885C4FAC39CDA70CC4C9BF834BA1 (void);
// 0x000001CB System.Void Obi.ObiClothRendererBase::OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererBase_OnBlueprintLoaded_mA41298CF8E3348C30A4935C1F78FB405CDB0C6E5 (void);
// 0x000001CC System.Void Obi.ObiClothRendererBase::OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererBase_OnBlueprintUnloaded_m080F6520843923778C773CFE98CEE64D27BA3E6E (void);
// 0x000001CD System.Void Obi.ObiClothRendererBase::SetupUpdate()
extern void ObiClothRendererBase_SetupUpdate_mF41CB3C0F730521B3CB97E185B322B53A1A48221 (void);
// 0x000001CE System.Void Obi.ObiClothRendererBase::UpdateActiveVertex(Obi.ObiSolver,System.Int32,System.Int32)
extern void ObiClothRendererBase_UpdateActiveVertex_mE26734D59A1227D5C281409282047F59FC80A0F3 (void);
// 0x000001CF System.Void Obi.ObiClothRendererBase::UpdateInactiveVertex(Obi.ObiSolver,System.Int32,System.Int32)
extern void ObiClothRendererBase_UpdateInactiveVertex_mC796782B675D43C4DFE91E69BB990B105791AD58 (void);
// 0x000001D0 System.Void Obi.ObiClothRendererBase::UpdateRenderer(Obi.ObiActor)
extern void ObiClothRendererBase_UpdateRenderer_m52FE3EDD4EC4660F1A264944E52A2BB1C559A3FC (void);
// 0x000001D1 System.Void Obi.ObiClothRendererBase::.ctor()
extern void ObiClothRendererBase__ctor_m97CACE1219B97C09CD47EAD9DBBD8EF0B980458B (void);
// 0x000001D2 System.Void Obi.ObiClothRendererBase::.cctor()
extern void ObiClothRendererBase__cctor_mD95E153B9F2B07840872EE13E7B3C6B2F44EC0D3 (void);
// 0x000001D3 System.Void Obi.ObiClothRendererMeshFilter::OnEnable()
extern void ObiClothRendererMeshFilter_OnEnable_m14C0C404EB0B054FC3860DDE0BEA2B21D19B6BDE (void);
// 0x000001D4 System.Void Obi.ObiClothRendererMeshFilter::OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererMeshFilter_OnBlueprintLoaded_m7DC0E650090A6BD7DF5CE19BBD09442A13B14894 (void);
// 0x000001D5 System.Void Obi.ObiClothRendererMeshFilter::OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererMeshFilter_OnBlueprintUnloaded_m324AD1AC22239216957C1FD2A96CD5CDA1063B15 (void);
// 0x000001D6 System.Void Obi.ObiClothRendererMeshFilter::.ctor()
extern void ObiClothRendererMeshFilter__ctor_m281C31755A0C1BFD98FFA51A69BC5C072F9BD5ED (void);
// 0x000001D7 UnityEngine.Matrix4x4 Obi.ObiSkinnedClothRenderer::get_renderMatrix()
extern void ObiSkinnedClothRenderer_get_renderMatrix_m362E5F3D96F17C448B1C2397AECA82DE33DBEAD0 (void);
// 0x000001D8 System.Void Obi.ObiSkinnedClothRenderer::OnEnable()
extern void ObiSkinnedClothRenderer_OnEnable_m30D6ADEFBB33BCB831251C795FB6586AB326294E (void);
// 0x000001D9 System.Void Obi.ObiSkinnedClothRenderer::OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiSkinnedClothRenderer_OnBlueprintLoaded_mC4F8EEA70CBE24C043D9D3D7903CC89510BBB1BE (void);
// 0x000001DA System.Void Obi.ObiSkinnedClothRenderer::SetupUpdate()
extern void ObiSkinnedClothRenderer_SetupUpdate_mF06DCF83FCB097EE7DDF39D959D51B808913A9EB (void);
// 0x000001DB System.Void Obi.ObiSkinnedClothRenderer::UpdateInactiveVertex(Obi.ObiSolver,System.Int32,System.Int32)
extern void ObiSkinnedClothRenderer_UpdateInactiveVertex_m709D8133409DDA6637C10CBD201B275655DC8EF4 (void);
// 0x000001DC System.Void Obi.ObiSkinnedClothRenderer::UpdateRenderer(Obi.ObiActor)
extern void ObiSkinnedClothRenderer_UpdateRenderer_mA3E40089F829AF3B5F2CB9C0C280F8D6014C75A2 (void);
// 0x000001DD System.Void Obi.ObiSkinnedClothRenderer::.ctor()
extern void ObiSkinnedClothRenderer__ctor_m7BF0C3CC512D893A909772BA480CA0CA7FB9436F (void);
// 0x000001DE Obi.HalfEdgeMesh Obi.ObiTearableClothRenderer::get_topology()
extern void ObiTearableClothRenderer_get_topology_mD2F198C1CCF5966CD5DA3CC2FE6C48521A7519AE (void);
// 0x000001DF System.Void Obi.ObiTearableClothRenderer::OnEnable()
extern void ObiTearableClothRenderer_OnEnable_mA0B0F805992B6983E009DFB1CA0B17B1F25A0668 (void);
// 0x000001E0 System.Void Obi.ObiTearableClothRenderer::OnDisable()
extern void ObiTearableClothRenderer_OnDisable_m361038B61B8468A4EABC1977849C77CA88AEF16B (void);
// 0x000001E1 System.Void Obi.ObiTearableClothRenderer::GetClothMeshData()
extern void ObiTearableClothRenderer_GetClothMeshData_m5FD8A359120B88E96E2763B9629DBBCBAA7496C0 (void);
// 0x000001E2 System.Void Obi.ObiTearableClothRenderer::SetClothMeshData()
extern void ObiTearableClothRenderer_SetClothMeshData_m85FE724DF5D42CD8DD7521331F365315001285F7 (void);
// 0x000001E3 System.Void Obi.ObiTearableClothRenderer::UpdateMesh(System.Object,Obi.ObiTearableCloth/ObiClothTornEventArgs)
extern void ObiTearableClothRenderer_UpdateMesh_mD6A0A8E00E54BEFE5AB2965C25746CBA2128BA9A (void);
// 0x000001E4 System.Collections.Generic.HashSet`1<System.Int32> Obi.ObiTearableClothRenderer::GetTornMeshVertices(System.Int32,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiTearableClothRenderer_GetTornMeshVertices_mE3ECE20DD4227DD7A668F294467CBEFCDA7AC5FC (void);
// 0x000001E5 System.Void Obi.ObiTearableClothRenderer::UpdateTornMeshVertices(System.Collections.Generic.HashSet`1<System.Int32>,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiTearableClothRenderer_UpdateTornMeshVertices_mB56215EAAECDB24A3ABAA9EB45E18D455E57410D (void);
// 0x000001E6 System.Void Obi.ObiTearableClothRenderer::.ctor()
extern void ObiTearableClothRenderer__ctor_mBB2CB662624757C06735294FB3333BA63E47C6B5 (void);
// 0x000001E7 System.Int32 Obi.IObiParticleCollection::get_particleCount()
// 0x000001E8 System.Int32 Obi.IObiParticleCollection::get_activeParticleCount()
// 0x000001E9 System.Boolean Obi.IObiParticleCollection::get_usesOrientedParticles()
// 0x000001EA System.Int32 Obi.IObiParticleCollection::GetParticleRuntimeIndex(System.Int32)
// 0x000001EB UnityEngine.Vector3 Obi.IObiParticleCollection::GetParticlePosition(System.Int32)
// 0x000001EC UnityEngine.Quaternion Obi.IObiParticleCollection::GetParticleOrientation(System.Int32)
// 0x000001ED System.Void Obi.IObiParticleCollection::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
// 0x000001EE System.Single Obi.IObiParticleCollection::GetParticleMaxRadius(System.Int32)
// 0x000001EF UnityEngine.Color Obi.IObiParticleCollection::GetParticleColor(System.Int32)
// 0x000001F0 System.Void Obi.ObiActor::add_OnBlueprintLoaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E (void);
// 0x000001F1 System.Void Obi.ObiActor::remove_OnBlueprintLoaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8 (void);
// 0x000001F2 System.Void Obi.ObiActor::add_OnBlueprintUnloaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601 (void);
// 0x000001F3 System.Void Obi.ObiActor::remove_OnBlueprintUnloaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB (void);
// 0x000001F4 System.Void Obi.ObiActor::add_OnPrepareFrame(Obi.ObiActor/ActorCallback)
extern void ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E (void);
// 0x000001F5 System.Void Obi.ObiActor::remove_OnPrepareFrame(Obi.ObiActor/ActorCallback)
extern void ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB (void);
// 0x000001F6 System.Void Obi.ObiActor::add_OnPrepareStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E (void);
// 0x000001F7 System.Void Obi.ObiActor::remove_OnPrepareStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC (void);
// 0x000001F8 System.Void Obi.ObiActor::add_OnBeginStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10 (void);
// 0x000001F9 System.Void Obi.ObiActor::remove_OnBeginStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13 (void);
// 0x000001FA System.Void Obi.ObiActor::add_OnSubstep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F (void);
// 0x000001FB System.Void Obi.ObiActor::remove_OnSubstep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592 (void);
// 0x000001FC System.Void Obi.ObiActor::add_OnEndStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208 (void);
// 0x000001FD System.Void Obi.ObiActor::remove_OnEndStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9 (void);
// 0x000001FE System.Void Obi.ObiActor::add_OnInterpolate(Obi.ObiActor/ActorCallback)
extern void ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65 (void);
// 0x000001FF System.Void Obi.ObiActor::remove_OnInterpolate(Obi.ObiActor/ActorCallback)
extern void ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D (void);
// 0x00000200 Obi.ObiSolver Obi.ObiActor::get_solver()
extern void ObiActor_get_solver_mDE668FD9EA16553E2252B51FC4AA0EE5F2476940 (void);
// 0x00000201 System.Boolean Obi.ObiActor::get_isLoaded()
extern void ObiActor_get_isLoaded_mE37E960D0EE5EF278241FE730E4E6D56FBBE217E (void);
// 0x00000202 Obi.ObiCollisionMaterial Obi.ObiActor::get_collisionMaterial()
extern void ObiActor_get_collisionMaterial_mFEBA9610A92672D379939CCF56E5A07800EF7AB7 (void);
// 0x00000203 System.Void Obi.ObiActor::set_collisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiActor_set_collisionMaterial_m04C98A6B84C31070613652C4D12C3EFC0D98A0C4 (void);
// 0x00000204 System.Boolean Obi.ObiActor::get_surfaceCollisions()
extern void ObiActor_get_surfaceCollisions_m280DE5273E14E28B1037C4CEA893F3BFFBEB2CB8 (void);
// 0x00000205 System.Void Obi.ObiActor::set_surfaceCollisions(System.Boolean)
extern void ObiActor_set_surfaceCollisions_m40EE79424632CAE1EBA8FB1C3087D54FD9FC5046 (void);
// 0x00000206 System.Int32 Obi.ObiActor::get_particleCount()
extern void ObiActor_get_particleCount_m8A511B9B8EE21FE09B45EA484ADE54CE1A469C24 (void);
// 0x00000207 System.Int32 Obi.ObiActor::get_activeParticleCount()
extern void ObiActor_get_activeParticleCount_m330540C2CFF296103F759A43A156960A14EF40E3 (void);
// 0x00000208 System.Boolean Obi.ObiActor::get_usesOrientedParticles()
extern void ObiActor_get_usesOrientedParticles_m1F5AC23A705419FFA18109ACE2CD45861FFCADDE (void);
// 0x00000209 System.Boolean Obi.ObiActor::get_usesAnisotropicParticles()
extern void ObiActor_get_usesAnisotropicParticles_m54157FE27C8DBE561125BC87EA21BA034F246B69 (void);
// 0x0000020A System.Boolean Obi.ObiActor::get_usesCustomExternalForces()
extern void ObiActor_get_usesCustomExternalForces_m63A0FBED9893C8176AA7DB6B5567978FB1D71DCA (void);
// 0x0000020B UnityEngine.Matrix4x4 Obi.ObiActor::get_actorLocalToSolverMatrix()
extern void ObiActor_get_actorLocalToSolverMatrix_mCF3080C74BC381BF19F21BB6204A19DBDDFB82DE (void);
// 0x0000020C UnityEngine.Matrix4x4 Obi.ObiActor::get_actorSolverToLocalMatrix()
extern void ObiActor_get_actorSolverToLocalMatrix_m467C1F4C003B591FBE2C93BA9BDB33C0371962CE (void);
// 0x0000020D Obi.ObiActorBlueprint Obi.ObiActor::get_sourceBlueprint()
// 0x0000020E Obi.ObiActorBlueprint Obi.ObiActor::get_sharedBlueprint()
extern void ObiActor_get_sharedBlueprint_mE4DB059D49A26A1EF8D5AFD8CECEDB63CDACD863 (void);
// 0x0000020F Obi.ObiActorBlueprint Obi.ObiActor::get_blueprint()
extern void ObiActor_get_blueprint_m47A228582F43BE1A2AF98A4CFBF052C0DCF852F5 (void);
// 0x00000210 System.Void Obi.ObiActor::Awake()
extern void ObiActor_Awake_m241F3014CB9150A6234C8BBFE5F7F1506CC4A4A8 (void);
// 0x00000211 System.Void Obi.ObiActor::OnDestroy()
extern void ObiActor_OnDestroy_m2F3D4A01290E0E5EB2F3753AB9913AC96249A655 (void);
// 0x00000212 System.Void Obi.ObiActor::OnEnable()
extern void ObiActor_OnEnable_m971216E2924B472ED6E1EF5CA46C20711919C967 (void);
// 0x00000213 System.Void Obi.ObiActor::OnDisable()
extern void ObiActor_OnDisable_mCD8A0D978C2AD361EA37D62139DD083606463FD7 (void);
// 0x00000214 System.Void Obi.ObiActor::OnValidate()
extern void ObiActor_OnValidate_m52B606D1428A6597252E13B0A7664A6A2A9AC196 (void);
// 0x00000215 System.Void Obi.ObiActor::OnTransformParentChanged()
extern void ObiActor_OnTransformParentChanged_mE56EB6C5787A9B68CC2DCBF59A69FC5DBAC340B7 (void);
// 0x00000216 System.Void Obi.ObiActor::AddToSolver()
extern void ObiActor_AddToSolver_m4612A91281D3B2EEA44B2654DF515E757FA4D3FF (void);
// 0x00000217 System.Void Obi.ObiActor::RemoveFromSolver()
extern void ObiActor_RemoveFromSolver_m6E84D3DEBFE722DD4EC517EB2622FC80350D4FEA (void);
// 0x00000218 System.Void Obi.ObiActor::SetSolver(Obi.ObiSolver)
extern void ObiActor_SetSolver_m6163A3076A049EAC233DCD2BB28B05C282DA55B6 (void);
// 0x00000219 System.Void Obi.ObiActor::OnBlueprintRegenerate(Obi.ObiActorBlueprint)
extern void ObiActor_OnBlueprintRegenerate_mC7B6851CA42C438CCA449E6F6B675C88F4515C71 (void);
// 0x0000021A System.Void Obi.ObiActor::UpdateCollisionMaterials()
extern void ObiActor_UpdateCollisionMaterials_m5E96C2AE2B9F7811085F85D0D25A5621C46D6817 (void);
// 0x0000021B System.Boolean Obi.ObiActor::CopyParticle(System.Int32,System.Int32)
extern void ObiActor_CopyParticle_m8AA520BF621655A1D7FD60BE63769B097E1996D6 (void);
// 0x0000021C System.Void Obi.ObiActor::TeleportParticle(System.Int32,UnityEngine.Vector3)
extern void ObiActor_TeleportParticle_mD6150ED98C32F82A0ADB734DDE58A37BA0B78E39 (void);
// 0x0000021D System.Void Obi.ObiActor::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObiActor_Teleport_mE63A190E1F4170CF5E1BE19522E3B6061AE96A04 (void);
// 0x0000021E System.Void Obi.ObiActor::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActor_SwapWithFirstInactiveParticle_mC6593AA19F1C4AF00040F46D4EC723008B97B52B (void);
// 0x0000021F System.Boolean Obi.ObiActor::ActivateParticle(System.Int32)
extern void ObiActor_ActivateParticle_m45FF9EA8B2788D3C9EAE0EDE77DA4FA2133BF9BA (void);
// 0x00000220 System.Boolean Obi.ObiActor::DeactivateParticle(System.Int32)
extern void ObiActor_DeactivateParticle_m04DDF95A73A8AE09950848D4A8DBB2A57EC42C59 (void);
// 0x00000221 System.Boolean Obi.ObiActor::IsParticleActive(System.Int32)
extern void ObiActor_IsParticleActive_mFD14C2BCE7E86131404B2EEDA413555B7288C0E3 (void);
// 0x00000222 System.Void Obi.ObiActor::SetSelfCollisions(System.Boolean)
extern void ObiActor_SetSelfCollisions_m45F0B0F5D1A4306D161C7EEC3C1435D9D52E3686 (void);
// 0x00000223 System.Void Obi.ObiActor::SetOneSided(System.Boolean)
extern void ObiActor_SetOneSided_m99DA6DD46C17B2357C1815E76EA6FB70B6525D33 (void);
// 0x00000224 System.Void Obi.ObiActor::SetSimplicesDirty()
extern void ObiActor_SetSimplicesDirty_m1FA63CB84FD2F2219BB54CB69CC5E8473FE2167A (void);
// 0x00000225 System.Void Obi.ObiActor::SetConstraintsDirty(Oni/ConstraintType)
extern void ObiActor_SetConstraintsDirty_mF1D5DEE2405AE6F94AE264FC9CD0A63C31189D0F (void);
// 0x00000226 Obi.IObiConstraints Obi.ObiActor::GetConstraintsByType(Oni/ConstraintType)
extern void ObiActor_GetConstraintsByType_mA485A63E65B2E5A863562BA515B15F6DE11138E7 (void);
// 0x00000227 System.Void Obi.ObiActor::UpdateParticleProperties()
extern void ObiActor_UpdateParticleProperties_m9B2C616C0275DB62285F9E0CB0F1FF615D9FFAFF (void);
// 0x00000228 System.Int32 Obi.ObiActor::GetParticleRuntimeIndex(System.Int32)
extern void ObiActor_GetParticleRuntimeIndex_m4AECF54626524359EE8CA930022DDFA864946B0D (void);
// 0x00000229 UnityEngine.Vector3 Obi.ObiActor::GetParticlePosition(System.Int32)
extern void ObiActor_GetParticlePosition_m02493A97C489BA4E448F1B3BE3AAB59DB1E9039A (void);
// 0x0000022A UnityEngine.Quaternion Obi.ObiActor::GetParticleOrientation(System.Int32)
extern void ObiActor_GetParticleOrientation_mC35CD07B08A81AC2159E02DF583118B7ACF8E9C3 (void);
// 0x0000022B System.Void Obi.ObiActor::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActor_GetParticleAnisotropy_mC8BAD8C2ACCEB1DF3284AEF13CC5E12ABEFF727A (void);
// 0x0000022C System.Single Obi.ObiActor::GetParticleMaxRadius(System.Int32)
extern void ObiActor_GetParticleMaxRadius_m834AF87A1D3204613AC0A6658EDBAF54871C13BD (void);
// 0x0000022D UnityEngine.Color Obi.ObiActor::GetParticleColor(System.Int32)
extern void ObiActor_GetParticleColor_m08A638A989F579EFADEA293B32C74635EECD25EA (void);
// 0x0000022E System.Void Obi.ObiActor::SetFilterCategory(System.Int32)
extern void ObiActor_SetFilterCategory_mAE60934D51EBD397225139EE05F2E80F08118D7D (void);
// 0x0000022F System.Void Obi.ObiActor::SetFilterMask(System.Int32)
extern void ObiActor_SetFilterMask_m7FED73FD99F34632090C9174E5EB278DC5D76F08 (void);
// 0x00000230 System.Void Obi.ObiActor::SetMass(System.Single)
extern void ObiActor_SetMass_m8D79A26F4FBEF1A895C6F482808A23A02B8BFAC4 (void);
// 0x00000231 System.Single Obi.ObiActor::GetMass(UnityEngine.Vector3&)
extern void ObiActor_GetMass_m2E9973F1EE3CBB300948F36489B7A7637B768F98 (void);
// 0x00000232 System.Void Obi.ObiActor::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddForce_mE87CF6C0A21C03ED45209758E7BFE0E1AB2B1A00 (void);
// 0x00000233 System.Void Obi.ObiActor::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddTorque_m43A494B40EA87E0EC7A6274A0931F9CF27B5D77D (void);
// 0x00000234 System.Void Obi.ObiActor::LoadBlueprintParticles(Obi.ObiActorBlueprint,System.Int32)
extern void ObiActor_LoadBlueprintParticles_m11D7669681BB9F35863E2BE6B9B50F9C7F8CFE88 (void);
// 0x00000235 System.Void Obi.ObiActor::UnloadBlueprintParticles()
extern void ObiActor_UnloadBlueprintParticles_mAFE44CA41C0A763A6142420F2145AAEDCFD296A3 (void);
// 0x00000236 System.Void Obi.ObiActor::ResetParticles()
extern void ObiActor_ResetParticles_m081DAAF528B4EF4F9E79AA17ECBEFC2C0BCB03D2 (void);
// 0x00000237 System.Void Obi.ObiActor::SaveStateToBlueprint(Obi.ObiActorBlueprint)
extern void ObiActor_SaveStateToBlueprint_m86BB211764276B407A42BF4C603D75AEBD260F84 (void);
// 0x00000238 System.Void Obi.ObiActor::StoreState()
extern void ObiActor_StoreState_mDF522A37AED6A366CD98C42E6D12EB524E5A6B67 (void);
// 0x00000239 System.Void Obi.ObiActor::ClearState()
extern void ObiActor_ClearState_m2708E40E0CC1259F0A4A23A134D7C08A34D64D9E (void);
// 0x0000023A System.Void Obi.ObiActor::LoadBlueprint(Obi.ObiSolver)
extern void ObiActor_LoadBlueprint_mD8204B2EFE860EADB85ABAAEA380D0EB7DE52966 (void);
// 0x0000023B System.Void Obi.ObiActor::UnloadBlueprint(Obi.ObiSolver)
extern void ObiActor_UnloadBlueprint_m0380FCD5E41E56876B933D34F94EE112FE49D14E (void);
// 0x0000023C System.Void Obi.ObiActor::PrepareFrame()
extern void ObiActor_PrepareFrame_mB2B1C5E2FB1A0570C0B7527B5654C17ADE677A53 (void);
// 0x0000023D System.Void Obi.ObiActor::PrepareStep(System.Single)
extern void ObiActor_PrepareStep_mB06CED03B998A63A4AC43A5318D99D1AE8ABA400 (void);
// 0x0000023E System.Void Obi.ObiActor::BeginStep(System.Single)
extern void ObiActor_BeginStep_m2CB8338B9754937B0F151D9F06AF97263B2D313F (void);
// 0x0000023F System.Void Obi.ObiActor::Substep(System.Single)
extern void ObiActor_Substep_m784F85C0BD4464D6E4B1BCE24E44047956C78486 (void);
// 0x00000240 System.Void Obi.ObiActor::EndStep(System.Single)
extern void ObiActor_EndStep_m5076BADB8970EC68040EA131738946988F140513 (void);
// 0x00000241 System.Void Obi.ObiActor::Interpolate()
extern void ObiActor_Interpolate_m589CB41AB063846C5C621944F1B14EACB90C3683 (void);
// 0x00000242 System.Void Obi.ObiActor::OnSolverVisibilityChanged(System.Boolean)
extern void ObiActor_OnSolverVisibilityChanged_mA9B03F298B37CD3BB66DD5527F9E3CDB43A728EB (void);
// 0x00000243 System.Void Obi.ObiActor::.ctor()
extern void ObiActor__ctor_mE992775E0773F687C886A5C462E2EAB7FA5D740E (void);
// 0x00000244 Obi.ObiSolver Obi.ObiActor/ObiActorSolverArgs::get_solver()
extern void ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF (void);
// 0x00000245 System.Void Obi.ObiActor/ObiActorSolverArgs::.ctor(Obi.ObiSolver)
extern void ObiActorSolverArgs__ctor_m645D2D61FF1A0780D4B70C2494C8C90607C7FA1B (void);
// 0x00000246 System.Void Obi.ObiActor/ActorCallback::.ctor(System.Object,System.IntPtr)
extern void ActorCallback__ctor_mA6CF95A70DAB4FFF21823741E58A598FD0A3CC61 (void);
// 0x00000247 System.Void Obi.ObiActor/ActorCallback::Invoke(Obi.ObiActor)
extern void ActorCallback_Invoke_m44385DCB93F7AC483E18B7477BDEB1C44993D508 (void);
// 0x00000248 System.IAsyncResult Obi.ObiActor/ActorCallback::BeginInvoke(Obi.ObiActor,System.AsyncCallback,System.Object)
extern void ActorCallback_BeginInvoke_m511CEA8353354A1869B3548D98C3EA166C757E30 (void);
// 0x00000249 System.Void Obi.ObiActor/ActorCallback::EndInvoke(System.IAsyncResult)
extern void ActorCallback_EndInvoke_m44778EF05F7C946EE7ACD0315B4F14F101980024 (void);
// 0x0000024A System.Void Obi.ObiActor/ActorStepCallback::.ctor(System.Object,System.IntPtr)
extern void ActorStepCallback__ctor_m48119AF13D841B3D5476543A1C79108F94D53E00 (void);
// 0x0000024B System.Void Obi.ObiActor/ActorStepCallback::Invoke(Obi.ObiActor,System.Single)
extern void ActorStepCallback_Invoke_m25120AB25E0465B7AF5976D51946EED8935E2A38 (void);
// 0x0000024C System.IAsyncResult Obi.ObiActor/ActorStepCallback::BeginInvoke(Obi.ObiActor,System.Single,System.AsyncCallback,System.Object)
extern void ActorStepCallback_BeginInvoke_mE4197C4E139BFCC95434E2C3E20BE882EB5F2989 (void);
// 0x0000024D System.Void Obi.ObiActor/ActorStepCallback::EndInvoke(System.IAsyncResult)
extern void ActorStepCallback_EndInvoke_m8AA8AAD3C0E68BB1AF5F7E6A61EA2269F3A57AFB (void);
// 0x0000024E System.Void Obi.ObiActor/ActorBlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void ActorBlueprintCallback__ctor_mADC829707A4F631EADEE895A25D2AAEA59A4D64D (void);
// 0x0000024F System.Void Obi.ObiActor/ActorBlueprintCallback::Invoke(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ActorBlueprintCallback_Invoke_m7BFF8526B4E3B31969016C044E63EA9AF3FFE7AD (void);
// 0x00000250 System.IAsyncResult Obi.ObiActor/ActorBlueprintCallback::BeginInvoke(Obi.ObiActor,Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void ActorBlueprintCallback_BeginInvoke_m32D286D241BEC1F0C4FD9CF6C04C5E7BF1823485 (void);
// 0x00000251 System.Void Obi.ObiActor/ActorBlueprintCallback::EndInvoke(System.IAsyncResult)
extern void ActorBlueprintCallback_EndInvoke_m7B651D23DA455A93DCC87B95CCE21A5789433013 (void);
// 0x00000252 Obi.ISolverImpl Obi.BurstBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void BurstBackend_CreateSolver_m365F45F03BBF4E610276C6F35CF0E9571A34ECA9 (void);
// 0x00000253 System.Void Obi.BurstBackend::DestroySolver(Obi.ISolverImpl)
extern void BurstBackend_DestroySolver_mE387C5978ADCE197E5A514ABFCFC8B208DDA8918 (void);
// 0x00000254 System.Void Obi.BurstBackend::.ctor()
extern void BurstBackend__ctor_mEF1F6C9604213C51BE39D80F166780E5F6308E51 (void);
// 0x00000255 Unity.Mathematics.float4 Obi.BurstIntegration::IntegrateLinear(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstIntegration_IntegrateLinear_m921E2CDA30530CC03E67061FCB3E58B4CD580F19 (void);
// 0x00000256 Unity.Mathematics.float4 Obi.BurstIntegration::DifferentiateLinear(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstIntegration_DifferentiateLinear_m18FFA3260D236DE1A749C1982054E4358A92F668 (void);
// 0x00000257 Unity.Mathematics.quaternion Obi.BurstIntegration::AngularVelocityToSpinQuaternion(Unity.Mathematics.quaternion,Unity.Mathematics.float4,System.Single)
extern void BurstIntegration_AngularVelocityToSpinQuaternion_mECA9B9F387A94F998143099B85244F25A7C71C3A (void);
// 0x00000258 Unity.Mathematics.quaternion Obi.BurstIntegration::IntegrateAngular(Unity.Mathematics.quaternion,Unity.Mathematics.float4,System.Single)
extern void BurstIntegration_IntegrateAngular_m1AC7507CA63312B4AED6B0D3D41026A58E286DA9 (void);
// 0x00000259 Unity.Mathematics.float4 Obi.BurstIntegration::DifferentiateAngular(Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,System.Single)
extern void BurstIntegration_DifferentiateAngular_m8E1D447491B00E55C4AA5337E06EDD9E04FA391F (void);
// 0x0000025A Obi.BurstJobHandle Obi.BurstJobHandle::SetHandle(Unity.Jobs.JobHandle)
extern void BurstJobHandle_SetHandle_m57F25376A67FD363BF9E2FA481C34F33BE928479 (void);
// 0x0000025B System.Void Obi.BurstJobHandle::Complete()
extern void BurstJobHandle_Complete_m2443FB0658D5667B94E3E26494792DFA42E7C9C1 (void);
// 0x0000025C System.Void Obi.BurstJobHandle::Release()
extern void BurstJobHandle_Release_m70B6B8DEE092DA24E3526CE9F558FF44B064D231 (void);
// 0x0000025D System.Void Obi.BurstJobHandle::.ctor()
extern void BurstJobHandle__ctor_m66E9504D512A2946F639E96C611289F618F1F820 (void);
// 0x0000025E Unity.Mathematics.float3x3 Obi.BurstMath::multrnsp(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void BurstMath_multrnsp_m6DCBD243896AD27CA2286015B3D35ED6B678FE12 (void);
// 0x0000025F Unity.Mathematics.float4x4 Obi.BurstMath::multrnsp4(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void BurstMath_multrnsp4_mFDFD027BEF04401275FFFBFDA8BEEBADA78C2755 (void);
// 0x00000260 Unity.Mathematics.float4 Obi.BurstMath::project(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void BurstMath_project_m1A54A528F3FC9B0309892E868CF525734419BB49 (void);
// 0x00000261 Unity.Mathematics.float4x4 Obi.BurstMath::TransformInertiaTensor(Unity.Mathematics.float4,Unity.Mathematics.quaternion)
extern void BurstMath_TransformInertiaTensor_m54DB5D9D60F182D81A1A94D4AE87C537BB5B716B (void);
// 0x00000262 System.Single Obi.BurstMath::RotationalInvMass(Unity.Mathematics.float4x4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void BurstMath_RotationalInvMass_m77B1237CBA82520C99CFCEF39BB74697B2E2CE6A (void);
// 0x00000263 Unity.Mathematics.float4 Obi.BurstMath::GetParticleVelocityAtPoint(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstMath_GetParticleVelocityAtPoint_m533DAD00C56A4ADEA7DC7F2155446CF97BFFCE9E (void);
// 0x00000264 Unity.Mathematics.float4 Obi.BurstMath::GetParticleVelocityAtPoint(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,Unity.Mathematics.float4,System.Single)
extern void BurstMath_GetParticleVelocityAtPoint_mEE6D03ADB42973E2DADB2CF67BB6AEEAE643F584 (void);
// 0x00000265 Unity.Mathematics.float4 Obi.BurstMath::GetRigidbodyVelocityAtPoint(System.Int32,Unity.Mathematics.float4,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Obi.BurstAffineTransform)
extern void BurstMath_GetRigidbodyVelocityAtPoint_m6D7F526FF3A3868816B466207E631CBEF3A723BC (void);
// 0x00000266 Unity.Mathematics.float4 Obi.BurstMath::GetRigidbodyVelocityAtPoint(System.Int32,Unity.Mathematics.float4,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Obi.BurstAffineTransform)
extern void BurstMath_GetRigidbodyVelocityAtPoint_m0F22193AA325748DBB42F5633911298C651B0938 (void);
// 0x00000267 System.Void Obi.BurstMath::ApplyImpulse(System.Int32,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Obi.BurstAffineTransform)
extern void BurstMath_ApplyImpulse_m203334B30F3ACFBAAD67BF9A6D76FB1216D06DB5 (void);
// 0x00000268 System.Void Obi.BurstMath::ApplyDeltaQuaternion(System.Int32,Unity.Mathematics.quaternion,Unity.Mathematics.quaternion,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Obi.BurstAffineTransform,System.Single)
extern void BurstMath_ApplyDeltaQuaternion_m5697C6202036D32860B3C613DE6A089EC28C850A (void);
// 0x00000269 System.Void Obi.BurstMath::OneSidedNormal(Unity.Mathematics.float4,Unity.Mathematics.float4&)
extern void BurstMath_OneSidedNormal_m8551E1209D5159A5CB34E47F9D47F3FCE7186A68 (void);
// 0x0000026A System.Single Obi.BurstMath::EllipsoidRadius(Unity.Mathematics.float4,Unity.Mathematics.quaternion,Unity.Mathematics.float3)
extern void BurstMath_EllipsoidRadius_mB707721A79E5ED040C1DFF300DF0028F72CDF65D (void);
// 0x0000026B Unity.Mathematics.quaternion Obi.BurstMath::ExtractRotation(Unity.Mathematics.float4x4,Unity.Mathematics.quaternion,System.Int32)
extern void BurstMath_ExtractRotation_m94C5F740BF10C6707B30C7C228E5AF9905A2BBEF (void);
// 0x0000026C System.Void Obi.BurstMath::SwingTwist(Unity.Mathematics.quaternion,Unity.Mathematics.float3,Unity.Mathematics.quaternion&,Unity.Mathematics.quaternion&)
extern void BurstMath_SwingTwist_mA729CD3BDD82BF4422C6DED532B204F590DF55DF (void);
// 0x0000026D Unity.Mathematics.float4x4 Obi.BurstMath::toMatrix(Unity.Mathematics.quaternion)
extern void BurstMath_toMatrix_mAA0372FB0B34939F42DA7D8C213E694BC0C7DF4D (void);
// 0x0000026E Unity.Mathematics.float4x4 Obi.BurstMath::asDiagonal(Unity.Mathematics.float4)
extern void BurstMath_asDiagonal_mED83A376E4FB3DD5D01304739CF1209A86E846AE (void);
// 0x0000026F Unity.Mathematics.float4 Obi.BurstMath::diagonal(Unity.Mathematics.float4x4)
extern void BurstMath_diagonal_m2495565B886EDB95594F3A6D3D3D04413E407984 (void);
// 0x00000270 System.Single Obi.BurstMath::frobeniusNorm(Unity.Mathematics.float4x4)
extern void BurstMath_frobeniusNorm_mD8B2D136717389B775B17E51B995F2EA8818F106 (void);
// 0x00000271 System.Void Obi.BurstMath::EigenSolve(Unity.Mathematics.float3x3,Unity.Mathematics.float3&,Unity.Mathematics.float3x3&)
extern void BurstMath_EigenSolve_m80C6B3EEE5D40D6D991C2D75C06994D64A47870E (void);
// 0x00000272 Unity.Mathematics.float3 Obi.BurstMath::unitOrthogonal(Unity.Mathematics.float3)
extern void BurstMath_unitOrthogonal_m0CF287F50224232FE30E0E6C09BAB1F314F35F53 (void);
// 0x00000273 Unity.Mathematics.float3 Obi.BurstMath::EigenVector(Unity.Mathematics.float3x3,System.Single)
extern void BurstMath_EigenVector_mFD07DD0849851A35E3352CFE18907AC042F21951 (void);
// 0x00000274 Unity.Mathematics.float3 Obi.BurstMath::EigenValues(Unity.Mathematics.float3x3)
extern void BurstMath_EigenValues_mF9B2E812F51CC5AD5A9D58B57A7F89F68C67C13F (void);
// 0x00000275 Unity.Mathematics.float4 Obi.BurstMath::NearestPointOnTri(Obi.BurstMath/CachedTri&,Unity.Mathematics.float4,Unity.Mathematics.float4&)
extern void BurstMath_NearestPointOnTri_m15F131F6314B94E56B30D27DC3FFFBE574379AF6 (void);
// 0x00000276 Unity.Mathematics.float4 Obi.BurstMath::NearestPointOnEdge(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single&,System.Boolean)
extern void BurstMath_NearestPointOnEdge_mFBAE1492DA93CE465DDCCD5235C17B338EB00A6F (void);
// 0x00000277 Unity.Mathematics.float4 Obi.BurstMath::NearestPointsTwoEdges(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single&,System.Single&)
extern void BurstMath_NearestPointsTwoEdges_m1B56CCBBE464C3541D9D3F96F377E0F8E0654F25 (void);
// 0x00000278 Unity.Mathematics.float4 Obi.BurstMath::BaryCoords(Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.float4&)
extern void BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73 (void);
// 0x00000279 Unity.Mathematics.float4 Obi.BurstMath::BaryCoords2(Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.float4&)
extern void BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A (void);
// 0x0000027A Unity.Mathematics.float4 Obi.BurstMath::BaryIntrpl(Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.float4&)
extern void BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9 (void);
// 0x0000027B Unity.Mathematics.float4 Obi.BurstMath::BaryIntrpl(Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.float4&)
extern void BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740 (void);
// 0x0000027C System.Single Obi.BurstMath::BaryIntrpl(System.Single,System.Single,System.Single,Unity.Mathematics.float4)
extern void BurstMath_BaryIntrpl_m38A47800E85DBDC519BB0BEE48E77F26C7ADACC8 (void);
// 0x0000027D System.Single Obi.BurstMath::BaryIntrpl(System.Single,System.Single,Unity.Mathematics.float4)
extern void BurstMath_BaryIntrpl_mAAA9279D994329F44077A24887823FE8F110C10E (void);
// 0x0000027E System.Single Obi.BurstMath::BaryScale(Unity.Mathematics.float4)
extern void BurstMath_BaryScale_m97EA0DB01CB35CD7B8E25408543E3DBD2E60FAF5 (void);
// 0x0000027F Unity.Mathematics.float4 Obi.BurstMath::BarycenterForSimplexOfSize(System.Int32)
extern void BurstMath_BarycenterForSimplexOfSize_m26CB914EE114D2DE19C4F75C97009A695F6C076A (void);
// 0x00000280 System.Void Obi.BurstMath::RemoveRangeBurst(Unity.Collections.NativeList`1<T>,System.Int32,System.Int32)
// 0x00000281 System.Void Obi.BurstMath::.cctor()
extern void BurstMath__cctor_m99432CC5A4D9AB0AD2645ACA2D65A07749F55B8C (void);
// 0x00000282 System.Void Obi.BurstMath/CachedTri::Cache(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void CachedTri_Cache_mAE403285E691BBA51817154E6CF25A99BF9D24F1 (void);
// 0x00000283 System.Void Obi.BurstBox::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstBox_Evaluate_m9A7975B75E4F6E739D90F73F2CB2209B369FDBFA (void);
// 0x00000284 System.Void Obi.BurstBox::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstBox_Contacts_m9D3DED1FA7E7454E9EB7A52A7ED94B8A08C99CE3 (void);
// 0x00000285 System.Void Obi.BurstBox::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstBox_Obi_IBurstCollider_Contacts_m97856F2BFA4EBA5278F111B41C7F5CA5A8C58573 (void);
// 0x00000286 System.Void Obi.BurstCapsule::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstCapsule_Evaluate_mB2A54FDA05AAE32DF69B20A013BDCF441F379D46 (void);
// 0x00000287 System.Void Obi.BurstCapsule::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstCapsule_Contacts_mE461E9B2E609E1A411ED668A5C6380B29FCCE4D6 (void);
// 0x00000288 System.Void Obi.BurstCapsule::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstCapsule_Obi_IBurstCollider_Contacts_mADF596860548ABE29238ADEA0F82324B6D1A6A28 (void);
// 0x00000289 System.Int32 Obi.BurstColliderWorld::get_referenceCount()
extern void BurstColliderWorld_get_referenceCount_m5410ABFAEEF5B2F079D12F85BA4BC00EBBA25A18 (void);
// 0x0000028A System.Void Obi.BurstColliderWorld::Awake()
extern void BurstColliderWorld_Awake_mF0444BFAAC7B1AEB3C8D6AF3C16FCA8079C41D7F (void);
// 0x0000028B System.Void Obi.BurstColliderWorld::OnDestroy()
extern void BurstColliderWorld_OnDestroy_m37CCCE53E7CC76AEA2F9C46BE1E23DEC6E1EE507 (void);
// 0x0000028C System.Void Obi.BurstColliderWorld::IncreaseReferenceCount()
extern void BurstColliderWorld_IncreaseReferenceCount_mE35A203B38FF99F444DC3DDC2E5260EB324E6D66 (void);
// 0x0000028D System.Void Obi.BurstColliderWorld::DecreaseReferenceCount()
extern void BurstColliderWorld_DecreaseReferenceCount_m641A956FDB4496B991213DE60E9843A212993A25 (void);
// 0x0000028E System.Void Obi.BurstColliderWorld::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
extern void BurstColliderWorld_SetColliders_mE6D28C17E1BD553ACAF043209701869C53E0B16B (void);
// 0x0000028F System.Void Obi.BurstColliderWorld::SetRigidbodies(Obi.ObiNativeRigidbodyList)
extern void BurstColliderWorld_SetRigidbodies_m6179D6A032B77BAF16651CF801C99D069927A4ED (void);
// 0x00000290 System.Void Obi.BurstColliderWorld::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
extern void BurstColliderWorld_SetCollisionMaterials_m56B61F9B00642E85C44EDFE7E45A8FD0566B6D48 (void);
// 0x00000291 System.Void Obi.BurstColliderWorld::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
extern void BurstColliderWorld_SetTriangleMeshData_m802E995C73CDDDF449C5FFA86CA2E9058430E847 (void);
// 0x00000292 System.Void Obi.BurstColliderWorld::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
extern void BurstColliderWorld_SetEdgeMeshData_m9A88483ACF3E325F8F71E7AFB8B5F3E60BBF2BA7 (void);
// 0x00000293 System.Void Obi.BurstColliderWorld::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
extern void BurstColliderWorld_SetDistanceFieldData_m78614076EB71DEC2C03A944DD308F95BB1179F8F (void);
// 0x00000294 System.Void Obi.BurstColliderWorld::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
extern void BurstColliderWorld_SetHeightFieldData_m507B16C528E0397A6A794CE2C82A8B3AAEF2AC13 (void);
// 0x00000295 System.Void Obi.BurstColliderWorld::UpdateWorld(System.Single)
extern void BurstColliderWorld_UpdateWorld_mFCE3E12510C2C0D767280588509E55A17E360AC1 (void);
// 0x00000296 Unity.Jobs.JobHandle Obi.BurstColliderWorld::GenerateContacts(Obi.BurstSolverImpl,System.Single,Unity.Jobs.JobHandle)
extern void BurstColliderWorld_GenerateContacts_mB8B09872216AF5D0CA158E500AB1B291ACA23BD7 (void);
// 0x00000297 System.Void Obi.BurstColliderWorld::.ctor()
extern void BurstColliderWorld__ctor_mB4D7132EC2AB0DC806D426C3D38035DB919504FA (void);
// 0x00000298 System.Void Obi.BurstColliderWorld/IdentifyMovingColliders::Execute(System.Int32)
extern void IdentifyMovingColliders_Execute_m6536871CD5E23F8233E90C455F8B29C3839BA490 (void);
// 0x00000299 System.Void Obi.BurstColliderWorld/UpdateMovingColliders::Execute()
extern void UpdateMovingColliders_Execute_m05B0B145094896C8C5D0A421ECFAB255D1C7D4D6 (void);
// 0x0000029A System.Void Obi.BurstColliderWorld/GenerateContactsJob::Execute(System.Int32)
extern void GenerateContactsJob_Execute_m1A30B956B328522C42AAB9316CFE714A83873DE5 (void);
// 0x0000029B System.Void Obi.BurstColliderWorld/GenerateContactsJob::GenerateContacts(Obi.BurstColliderShape&,Obi.BurstAffineTransform&,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,Obi.BurstAabb&)
extern void GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E (void);
// 0x0000029C Unity.Mathematics.float4 Obi.BurstDFNode::SampleWithGradient(Unity.Mathematics.float4)
extern void BurstDFNode_SampleWithGradient_m383A3E8C19EBD56E7660F1D957022D53CC0D5173 (void);
// 0x0000029D Unity.Mathematics.float4 Obi.BurstDFNode::GetNormalizedPos(Unity.Mathematics.float4)
extern void BurstDFNode_GetNormalizedPos_mD01E0D79BAC83035CC86DC3706B9B7AA98B3CF9D (void);
// 0x0000029E System.Int32 Obi.BurstDFNode::GetOctant(Unity.Mathematics.float4)
extern void BurstDFNode_GetOctant_m76BF4D7E298B1E7238848063468AFE583C403408 (void);
// 0x0000029F System.Void Obi.BurstDistanceField::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstDistanceField_Evaluate_mC93945AD7F3206A1EC39A627C982644CF24673B9 (void);
// 0x000002A0 System.Void Obi.BurstDistanceField::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstDistanceField_Contacts_mC98735809C55B8C503AC05F45BEC80420352057E (void);
// 0x000002A1 Unity.Mathematics.float4 Obi.BurstDistanceField::DFTraverse(Unity.Mathematics.float4,System.Int32,Obi.DistanceFieldHeader&,Unity.Collections.NativeArray`1<Obi.BurstDFNode>&)
extern void BurstDistanceField_DFTraverse_m21F88015BB4FB3D50DD96EC893D5A10CD6827850 (void);
// 0x000002A2 System.Void Obi.BurstDistanceField::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstDistanceField_Obi_IBurstCollider_Contacts_m212E14857D066BE00574BE85A69F636A0B5A14BF (void);
// 0x000002A3 System.Void Obi.BurstEdgeMesh::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstEdgeMesh_Evaluate_mB1DE344E5F95F0D35C81FCEAAFD439CCA1A0281B (void);
// 0x000002A4 System.Void Obi.BurstEdgeMesh::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstEdgeMesh_Contacts_mDEAD5DA052578CD2794227155F0CFDEFB76DD9BF (void);
// 0x000002A5 System.Void Obi.BurstEdgeMesh::BIHTraverse(System.Int32,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstEdgeMesh_BIHTraverse_m8E806DD0BD9B370CC1862099121BC5B3F83B9CA9 (void);
// 0x000002A6 System.Void Obi.BurstEdgeMesh::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstEdgeMesh_Obi_IBurstCollider_Contacts_mE8D91763243332F3BBE8D636493C232F3085C1C1 (void);
// 0x000002A7 System.Void Obi.BurstHeightField::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstHeightField_Evaluate_m37E0774FD60539E9B60C6265C5289EABA40FEE16 (void);
// 0x000002A8 System.Void Obi.BurstHeightField::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstHeightField_Contacts_m8D519E8FEE86E9D62A2F24AFE65643D75C82E321 (void);
// 0x000002A9 System.Void Obi.BurstHeightField::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstHeightField_Obi_IBurstCollider_Contacts_mAF9CC53C9637FA7BEFAACAB317DEECC4325EFEA8 (void);
// 0x000002AA System.Void Obi.BurstLocalOptimization::GetInterpolatedSimplexData(System.Int32,System.Int32,Unity.Collections.NativeArray`1<System.Int32>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Mathematics.float4,Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.quaternion&)
extern void BurstLocalOptimization_GetInterpolatedSimplexData_m711A70635DA122946C2ED3E19F344B5C34B307D9 (void);
// 0x000002AB Obi.BurstLocalOptimization/SurfacePoint Obi.BurstLocalOptimization::Optimize(T&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32,System.Int32,Unity.Mathematics.float4&,Unity.Mathematics.float4&,System.Int32,System.Single)
// 0x000002AC System.Void Obi.BurstLocalOptimization::FrankWolfe(T&,System.Int32,System.Int32,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.quaternion&,Unity.Mathematics.float4&,Obi.BurstLocalOptimization/SurfacePoint&,System.Int32,System.Single)
// 0x000002AD System.Void Obi.BurstLocalOptimization::GoldenSearch(T&,System.Int32,System.Int32,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Unity.Mathematics.float4&,Unity.Mathematics.float4&,Unity.Mathematics.quaternion&,Unity.Mathematics.float4&,Obi.BurstLocalOptimization/SurfacePoint&,System.Int32,System.Single)
// 0x000002AE System.Void Obi.BurstLocalOptimization/IDistanceFunction::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
// 0x000002AF System.Void Obi.BurstSimplex::CacheData()
extern void BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA (void);
// 0x000002B0 System.Void Obi.BurstSimplex::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstSimplex_Evaluate_m96C3BF51F11E624BC054D9BA276F177A9DBBBE73 (void);
// 0x000002B1 System.Void Obi.BurstSphere::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstSphere_Evaluate_mD03D68FDA6CCD8B6481B8CF75FB5C1528BA034B8 (void);
// 0x000002B2 System.Void Obi.BurstSphere::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstSphere_Contacts_mDA37B8F9954ECD2EFD7BF8F9A51A43CA73A869AE (void);
// 0x000002B3 System.Void Obi.BurstSphere::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstSphere_Obi_IBurstCollider_Contacts_m16229652DCB867F59A4F4360F064FDA00691D32B (void);
// 0x000002B4 System.Void Obi.BurstTriangleMesh::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstTriangleMesh_Evaluate_mB03B52D7D8BA26D3C383CB9334A57EDD03B03DE5 (void);
// 0x000002B5 System.Void Obi.BurstTriangleMesh::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstTriangleMesh_Contacts_m03A033B4760F0729757BF20D322FC8D112802A00 (void);
// 0x000002B6 System.Void Obi.BurstTriangleMesh::BIHTraverse(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb&,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstTriangleMesh_BIHTraverse_m53B6E0B2D8D09B465000034E751D2BB96E194907 (void);
// 0x000002B7 System.Void Obi.BurstTriangleMesh::Obi.IBurstCollider.Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
extern void BurstTriangleMesh_Obi_IBurstCollider_Contacts_m0414B63AB8F72DB613D9240E6B5AA847FB4B296D (void);
// 0x000002B8 System.Void Obi.IBurstCollider::Contacts(System.Int32,System.Int32,Unity.Collections.NativeArray`1<Obi.BurstRigidbody>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,Obi.BurstAabb& modreq(System.Runtime.InteropServices.InAttribute),System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstContact>,System.Int32,System.Single)
// 0x000002B9 System.Void Obi.BurstAerodynamicConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstAerodynamicConstraints__ctor_m171DB4C41AB61E57D6CFBCBF8F1EFBEECA50060E (void);
// 0x000002BA Obi.IConstraintsBatchImpl Obi.BurstAerodynamicConstraints::CreateConstraintsBatch()
extern void BurstAerodynamicConstraints_CreateConstraintsBatch_m5953E0EE649D7A6BCDFB1001111E82776F5AD4FC (void);
// 0x000002BB System.Void Obi.BurstAerodynamicConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstAerodynamicConstraints_RemoveBatch_mF45FB4F617887EA56C19CDD0A5599C3E5AF81309 (void);
// 0x000002BC System.Void Obi.BurstAerodynamicConstraintsBatch::.ctor(Obi.BurstAerodynamicConstraints)
extern void BurstAerodynamicConstraintsBatch__ctor_mAB73CE6EA7FA450AB8D8F9746F2DAF4D94301CE6 (void);
// 0x000002BD System.Void Obi.BurstAerodynamicConstraintsBatch::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
extern void BurstAerodynamicConstraintsBatch_SetAerodynamicConstraints_mD81E86E3899A743FBFF0D573642F9A31C36AD2DE (void);
// 0x000002BE Unity.Jobs.JobHandle Obi.BurstAerodynamicConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstAerodynamicConstraintsBatch_Initialize_m52DE3A5FFF09C7C19AFF49C578CD68B3BBA5A21A (void);
// 0x000002BF Unity.Jobs.JobHandle Obi.BurstAerodynamicConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstAerodynamicConstraintsBatch_Evaluate_m8313BC819CC1002CD147C384E8B1FD6186E1B362 (void);
// 0x000002C0 Unity.Jobs.JobHandle Obi.BurstAerodynamicConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstAerodynamicConstraintsBatch_Apply_m9B000CDF164C57958AB1FE3D032F672500B32F4A (void);
// 0x000002C1 System.Void Obi.BurstAerodynamicConstraintsBatch/AerodynamicConstraintsBatchJob::Execute(System.Int32)
extern void AerodynamicConstraintsBatchJob_Execute_mA12D60F5737F1B49A118E40DEBB6D5BE5D9FDCE1 (void);
// 0x000002C2 System.Void Obi.BurstBendConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstBendConstraints__ctor_mBB3175AC8830813B19E7C7406E322F622AC44B95 (void);
// 0x000002C3 Obi.IConstraintsBatchImpl Obi.BurstBendConstraints::CreateConstraintsBatch()
extern void BurstBendConstraints_CreateConstraintsBatch_m9482B32B4FFC637B8FACDB69E2E5187E1D7A2AD7 (void);
// 0x000002C4 System.Void Obi.BurstBendConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstBendConstraints_RemoveBatch_m524A898FC77AD6343137E8318D873E45CF4DDC95 (void);
// 0x000002C5 System.Void Obi.BurstBendConstraintsBatch::.ctor(Obi.BurstBendConstraints)
extern void BurstBendConstraintsBatch__ctor_m4A37A5130C84D585906D468AB0E8629E00088E10 (void);
// 0x000002C6 System.Void Obi.BurstBendConstraintsBatch::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void BurstBendConstraintsBatch_SetBendConstraints_m0D61DFB4075472908BF4B7AD7F7A5B91E9DDF4DB (void);
// 0x000002C7 Unity.Jobs.JobHandle Obi.BurstBendConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstBendConstraintsBatch_Evaluate_m8EA7F55220FE67E51678D8D753326CAD0A468980 (void);
// 0x000002C8 Unity.Jobs.JobHandle Obi.BurstBendConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstBendConstraintsBatch_Apply_m04AB64084DD5979B3C016A69AA5E324D8FA6A2E8 (void);
// 0x000002C9 System.Void Obi.BurstBendConstraintsBatch/BendConstraintsBatchJob::Execute(System.Int32)
extern void BendConstraintsBatchJob_Execute_mF6E997FD25554FC9F4071D28B24E8804DEF39870 (void);
// 0x000002CA System.Void Obi.BurstBendConstraintsBatch/ApplyBendConstraintsBatchJob::Execute(System.Int32)
extern void ApplyBendConstraintsBatchJob_Execute_m99F086827B78551FE09B2FBC6881B2894BCB7682 (void);
// 0x000002CB System.Void Obi.BurstBendTwistConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstBendTwistConstraints__ctor_mFF24BA182B06546A107E4746E9A16E003676E284 (void);
// 0x000002CC Obi.IConstraintsBatchImpl Obi.BurstBendTwistConstraints::CreateConstraintsBatch()
extern void BurstBendTwistConstraints_CreateConstraintsBatch_m0F238B5239F33DBBFF455F99E7ECA3C68042AB6B (void);
// 0x000002CD System.Void Obi.BurstBendTwistConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstBendTwistConstraints_RemoveBatch_mF7931B74B2FA5847F36D90151AA4771ACCF8F88C (void);
// 0x000002CE System.Void Obi.BurstBendTwistConstraintsBatch::.ctor(Obi.BurstBendTwistConstraints)
extern void BurstBendTwistConstraintsBatch__ctor_mAD28426736368584FBBF5F0B3F1A050017051A6D (void);
// 0x000002CF System.Void Obi.BurstBendTwistConstraintsBatch::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void BurstBendTwistConstraintsBatch_SetBendTwistConstraints_m83ECC4A3EB27AF0B0DD7286BBBBE9D3F5B9F595A (void);
// 0x000002D0 Unity.Jobs.JobHandle Obi.BurstBendTwistConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstBendTwistConstraintsBatch_Evaluate_m5A18EA6B83F56965C44CC599B6F11196EA9FC14F (void);
// 0x000002D1 Unity.Jobs.JobHandle Obi.BurstBendTwistConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstBendTwistConstraintsBatch_Apply_mB66593567CAA16CD1E737D5F960CB3E6DAEC3AF5 (void);
// 0x000002D2 System.Void Obi.BurstBendTwistConstraintsBatch/BendTwistConstraintsBatchJob::Execute(System.Int32)
extern void BendTwistConstraintsBatchJob_Execute_m72C33183ADAB152169BB181429D90663B0DB648A (void);
// 0x000002D3 System.Void Obi.BurstBendTwistConstraintsBatch/ApplyBendTwistConstraintsBatchJob::Execute(System.Int32)
extern void ApplyBendTwistConstraintsBatchJob_Execute_m74A218C4F257CB77BB42C70892690A866BC97861 (void);
// 0x000002D4 Oni/ConstraintType Obi.BurstConstraintsBatchImpl::get_constraintType()
extern void BurstConstraintsBatchImpl_get_constraintType_m2590C7D4F84F01AE788FF492011B71322F061925 (void);
// 0x000002D5 System.Void Obi.BurstConstraintsBatchImpl::set_enabled(System.Boolean)
extern void BurstConstraintsBatchImpl_set_enabled_mD1431710ABDE3B26BCC8B23B0D5C435C272B96D5 (void);
// 0x000002D6 System.Boolean Obi.BurstConstraintsBatchImpl::get_enabled()
extern void BurstConstraintsBatchImpl_get_enabled_m8D8BD7F684A9A2F366378F484FFB173C5A9A2388 (void);
// 0x000002D7 Obi.IConstraints Obi.BurstConstraintsBatchImpl::get_constraints()
extern void BurstConstraintsBatchImpl_get_constraints_m216AF95B2EC1563DA5C512071259B3D3EB52602B (void);
// 0x000002D8 Obi.ObiSolver Obi.BurstConstraintsBatchImpl::get_solverAbstraction()
extern void BurstConstraintsBatchImpl_get_solverAbstraction_m71CCA0338078FDB7879A9C7EE6EDBE36213746C2 (void);
// 0x000002D9 Obi.BurstSolverImpl Obi.BurstConstraintsBatchImpl::get_solverImplementation()
extern void BurstConstraintsBatchImpl_get_solverImplementation_mC0CFD60D058F313752ECD6AFFBDA5B019C7B92BA (void);
// 0x000002DA Unity.Jobs.JobHandle Obi.BurstConstraintsBatchImpl::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstConstraintsBatchImpl_Initialize_m3902E95C54BF2D85419EE6D4FF20324FD0A4113F (void);
// 0x000002DB Unity.Jobs.JobHandle Obi.BurstConstraintsBatchImpl::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
// 0x000002DC Unity.Jobs.JobHandle Obi.BurstConstraintsBatchImpl::Apply(Unity.Jobs.JobHandle,System.Single)
// 0x000002DD System.Void Obi.BurstConstraintsBatchImpl::Destroy()
extern void BurstConstraintsBatchImpl_Destroy_mA32FB47CE96F9AF44D3491F65C2ABC3282BFBC3C (void);
// 0x000002DE System.Void Obi.BurstConstraintsBatchImpl::SetConstraintCount(System.Int32)
extern void BurstConstraintsBatchImpl_SetConstraintCount_m56168F91898DD35463CE2F34E28420EDA26B08AC (void);
// 0x000002DF System.Int32 Obi.BurstConstraintsBatchImpl::GetConstraintCount()
extern void BurstConstraintsBatchImpl_GetConstraintCount_mECC340A005EB3FACC0D768D75B2AF91F3C537BBD (void);
// 0x000002E0 System.Void Obi.BurstConstraintsBatchImpl::ApplyPositionDelta(System.Int32,System.Single,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>&,Unity.Collections.NativeArray`1<System.Int32>&)
extern void BurstConstraintsBatchImpl_ApplyPositionDelta_mB768C6B64D66437DF5CD923CE2954F39C13EAF80 (void);
// 0x000002E1 System.Void Obi.BurstConstraintsBatchImpl::ApplyOrientationDelta(System.Int32,System.Single,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>&,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>&,Unity.Collections.NativeArray`1<System.Int32>&)
extern void BurstConstraintsBatchImpl_ApplyOrientationDelta_m5D183E7D73228BC30AF7F29AEA97093451DB3229 (void);
// 0x000002E2 System.Void Obi.BurstConstraintsBatchImpl::.ctor()
extern void BurstConstraintsBatchImpl__ctor_m4CC32FC741A8F1B78E1AD38A2CE9A173390D06F7 (void);
// 0x000002E3 Unity.Jobs.JobHandle Obi.IBurstConstraintsImpl::Initialize(Unity.Jobs.JobHandle,System.Single)
// 0x000002E4 Unity.Jobs.JobHandle Obi.IBurstConstraintsImpl::Project(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
// 0x000002E5 System.Void Obi.IBurstConstraintsImpl::Dispose()
// 0x000002E6 Obi.IConstraintsBatchImpl Obi.IBurstConstraintsImpl::CreateConstraintsBatch()
// 0x000002E7 System.Void Obi.IBurstConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x000002E8 Oni/ConstraintType Obi.BurstConstraintsImpl`1::get_constraintType()
// 0x000002E9 Obi.ISolverImpl Obi.BurstConstraintsImpl`1::get_solver()
// 0x000002EA System.Void Obi.BurstConstraintsImpl`1::.ctor(Obi.BurstSolverImpl,Oni/ConstraintType)
// 0x000002EB System.Void Obi.BurstConstraintsImpl`1::Dispose()
// 0x000002EC Obi.IConstraintsBatchImpl Obi.BurstConstraintsImpl`1::CreateConstraintsBatch()
// 0x000002ED System.Void Obi.BurstConstraintsImpl`1::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x000002EE System.Int32 Obi.BurstConstraintsImpl`1::GetConstraintCount()
// 0x000002EF Unity.Jobs.JobHandle Obi.BurstConstraintsImpl`1::Initialize(Unity.Jobs.JobHandle,System.Single)
// 0x000002F0 Unity.Jobs.JobHandle Obi.BurstConstraintsImpl`1::Project(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
// 0x000002F1 Unity.Jobs.JobHandle Obi.BurstConstraintsImpl`1::EvaluateSequential(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
// 0x000002F2 Unity.Jobs.JobHandle Obi.BurstConstraintsImpl`1::EvaluateParallel(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
// 0x000002F3 System.Void Obi.BurstChainConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstChainConstraints__ctor_m6BD031F1AF74A05ED788A97BD2E29DCC5405F252 (void);
// 0x000002F4 Obi.IConstraintsBatchImpl Obi.BurstChainConstraints::CreateConstraintsBatch()
extern void BurstChainConstraints_CreateConstraintsBatch_m6FA686222F58ADF6A8FB9DA868EF1ECB274EE794 (void);
// 0x000002F5 System.Void Obi.BurstChainConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstChainConstraints_RemoveBatch_m08C05C7C473E97B0EBD28FB927A087C22B30942C (void);
// 0x000002F6 System.Void Obi.BurstChainConstraintsBatch::.ctor(Obi.BurstChainConstraints)
extern void BurstChainConstraintsBatch__ctor_m3ED7DC4EFF029DCA737750DEDD59C96B8C5BEE93 (void);
// 0x000002F7 System.Void Obi.BurstChainConstraintsBatch::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
extern void BurstChainConstraintsBatch_SetChainConstraints_m7700742D24FE5E03135B25C8D0B077BD478BD329 (void);
// 0x000002F8 Unity.Jobs.JobHandle Obi.BurstChainConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstChainConstraintsBatch_Evaluate_m4DF6B73D65C12C2A211E71F0B35B6218057EBBAD (void);
// 0x000002F9 Unity.Jobs.JobHandle Obi.BurstChainConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstChainConstraintsBatch_Apply_m159D14CB6EB95D9D87E6E02D0EFA91451FEE15FF (void);
// 0x000002FA System.Void Obi.BurstChainConstraintsBatch/ChainConstraintsBatchJob::Execute(System.Int32)
extern void ChainConstraintsBatchJob_Execute_m576FF3805857AEBF06581865F975D5D982550907 (void);
// 0x000002FB System.Void Obi.BurstChainConstraintsBatch/ApplyChainConstraintsBatchJob::Execute(System.Int32)
extern void ApplyChainConstraintsBatchJob_Execute_mA038CCA6A34C6A73AECD256274108E245F5FE91A (void);
// 0x000002FC System.Void Obi.ApplyCollisionConstraintsBatchJob::Execute()
extern void ApplyCollisionConstraintsBatchJob_Execute_m875BE8298CCA64DB1EC6FA478647C40913DD330D (void);
// 0x000002FD System.Void Obi.BurstColliderCollisionConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstColliderCollisionConstraints__ctor_m4A7EE0A944FE1C78D90F82014E5023ECAE2E6F31 (void);
// 0x000002FE Obi.IConstraintsBatchImpl Obi.BurstColliderCollisionConstraints::CreateConstraintsBatch()
extern void BurstColliderCollisionConstraints_CreateConstraintsBatch_m885FF083ECF3133071C7360CC0EE6C27ED2465A3 (void);
// 0x000002FF System.Void Obi.BurstColliderCollisionConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstColliderCollisionConstraints_RemoveBatch_m316998BCB1F7E4271675FD5DBBBD5BA7C9B4D6DE (void);
// 0x00000300 System.Int32 Obi.BurstColliderCollisionConstraints::GetConstraintCount()
extern void BurstColliderCollisionConstraints_GetConstraintCount_m5BC945E5958AA1BB663354D0BCA87839CC7B8CAA (void);
// 0x00000301 System.Void Obi.BurstColliderCollisionConstraintsBatch::.ctor(Obi.BurstColliderCollisionConstraints)
extern void BurstColliderCollisionConstraintsBatch__ctor_m72900719130F8A4CEB14F342487A809771B2E85F (void);
// 0x00000302 Unity.Jobs.JobHandle Obi.BurstColliderCollisionConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstColliderCollisionConstraintsBatch_Initialize_m7066076D2BE9129C48624E1F828AB929F5725813 (void);
// 0x00000303 Unity.Jobs.JobHandle Obi.BurstColliderCollisionConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstColliderCollisionConstraintsBatch_Evaluate_mA980331AF0B4F032C3F5C6753D2DCCAB369C4580 (void);
// 0x00000304 Unity.Jobs.JobHandle Obi.BurstColliderCollisionConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstColliderCollisionConstraintsBatch_Apply_mB6FFEB95B0DBDE98C8E3C632A9A9A1A57A6B85C5 (void);
// 0x00000305 System.Void Obi.BurstColliderCollisionConstraintsBatch/UpdateContactsJob::Execute(System.Int32)
extern void UpdateContactsJob_Execute_mBDE78DEE6DE6D94DC3B6684199C0292D4C915E0D (void);
// 0x00000306 System.Void Obi.BurstColliderCollisionConstraintsBatch/CollisionConstraintsBatchJob::Execute()
extern void CollisionConstraintsBatchJob_Execute_m7E251F652F57C72F5A552957040597FA6DCD6688 (void);
// 0x00000307 Obi.BurstCollisionMaterial Obi.BurstColliderCollisionConstraintsBatch/CollisionConstraintsBatchJob::CombineCollisionMaterials(System.Int32,System.Int32)
extern void CollisionConstraintsBatchJob_CombineCollisionMaterials_mF1338B4C889E6A783168F52825A68777DBAE0C99 (void);
// 0x00000308 System.Void Obi.BurstColliderFrictionConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstColliderFrictionConstraints__ctor_m1577CBB923541040E3A27C93F0805CB4A88DE8AF (void);
// 0x00000309 Obi.IConstraintsBatchImpl Obi.BurstColliderFrictionConstraints::CreateConstraintsBatch()
extern void BurstColliderFrictionConstraints_CreateConstraintsBatch_m1473DB1005C33E2C3DE67DF92361EB8A74DEF544 (void);
// 0x0000030A System.Void Obi.BurstColliderFrictionConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstColliderFrictionConstraints_RemoveBatch_mAB3660C122E2A7BF08E0BAF080A3FAF5850391F1 (void);
// 0x0000030B System.Int32 Obi.BurstColliderFrictionConstraints::GetConstraintCount()
extern void BurstColliderFrictionConstraints_GetConstraintCount_m5194E2B5E8B1CBC581875DA5D490BDE5B15170CE (void);
// 0x0000030C System.Void Obi.BurstColliderFrictionConstraintsBatch::.ctor(Obi.BurstColliderFrictionConstraints)
extern void BurstColliderFrictionConstraintsBatch__ctor_mE6913E769AC6B04E509535FA82C20B3F2FC774E0 (void);
// 0x0000030D Unity.Jobs.JobHandle Obi.BurstColliderFrictionConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstColliderFrictionConstraintsBatch_Initialize_m7DD945D2223E2EECCCA54488772BF58A10E3BB0D (void);
// 0x0000030E Unity.Jobs.JobHandle Obi.BurstColliderFrictionConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstColliderFrictionConstraintsBatch_Evaluate_mD324C6CB95633E67336AFE9EDAD78658DA1BFB84 (void);
// 0x0000030F Unity.Jobs.JobHandle Obi.BurstColliderFrictionConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstColliderFrictionConstraintsBatch_Apply_m11BFC339A0FEC51B4D71D12CEE63F057511F38CF (void);
// 0x00000310 System.Void Obi.BurstColliderFrictionConstraintsBatch/FrictionConstraintsBatchJob::Execute()
extern void FrictionConstraintsBatchJob_Execute_mA6E69C218FC0969B77330BD6B4D5C15A6F49E056 (void);
// 0x00000311 Obi.BurstCollisionMaterial Obi.BurstColliderFrictionConstraintsBatch/FrictionConstraintsBatchJob::CombineCollisionMaterials(System.Int32,System.Int32)
extern void FrictionConstraintsBatchJob_CombineCollisionMaterials_mE4F955E2ED7F5DD7834726D2014A7555573FDB3C (void);
// 0x00000312 System.Void Obi.BurstDensityConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstDensityConstraints__ctor_mD9F106B23B9C178D8A545615DA87111141AAB4D9 (void);
// 0x00000313 Obi.IConstraintsBatchImpl Obi.BurstDensityConstraints::CreateConstraintsBatch()
extern void BurstDensityConstraints_CreateConstraintsBatch_m52015A08886A596B1A39E3C5F2BA1989A89CF3BF (void);
// 0x00000314 System.Void Obi.BurstDensityConstraints::Dispose()
extern void BurstDensityConstraints_Dispose_mF70DE3A0F0AC9B3A0674E49030A9E4C437D398AC (void);
// 0x00000315 System.Void Obi.BurstDensityConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstDensityConstraints_RemoveBatch_mA9C71BF83BC9090BFBDEDCDF4A7FBB00CF79EB1F (void);
// 0x00000316 Unity.Jobs.JobHandle Obi.BurstDensityConstraints::EvaluateSequential(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstDensityConstraints_EvaluateSequential_m95B6E2581F0A959D02937D9B1C54BB9B4759B617 (void);
// 0x00000317 Unity.Jobs.JobHandle Obi.BurstDensityConstraints::EvaluateParallel(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstDensityConstraints_EvaluateParallel_mE21B9F82D9A68EC930698C9E620CFEBEA2405C8D (void);
// 0x00000318 Unity.Jobs.JobHandle Obi.BurstDensityConstraints::ApplyVelocityCorrections(Unity.Jobs.JobHandle,System.Single)
extern void BurstDensityConstraints_ApplyVelocityCorrections_m6C4774683C02BC70687BB40104626F422032DAB2 (void);
// 0x00000319 Unity.Jobs.JobHandle Obi.BurstDensityConstraints::CalculateAnisotropyLaplacianSmoothing(Unity.Jobs.JobHandle)
extern void BurstDensityConstraints_CalculateAnisotropyLaplacianSmoothing_m75B282B819AFEE8C68D7C882C5BDE8398CBA2C90 (void);
// 0x0000031A Unity.Jobs.JobHandle Obi.BurstDensityConstraints::UpdateInteractions(Unity.Jobs.JobHandle)
extern void BurstDensityConstraints_UpdateInteractions_mE27800B95C636B6903FC35B9398934F80505744A (void);
// 0x0000031B Unity.Jobs.JobHandle Obi.BurstDensityConstraints::CalculateLambdas(Unity.Jobs.JobHandle,System.Single)
extern void BurstDensityConstraints_CalculateLambdas_m9EAAC7EE53D7E59D292F18AC46DEE931D474F291 (void);
// 0x0000031C Unity.Jobs.JobHandle Obi.BurstDensityConstraints::ApplyVorticityAndAtmosphere(Unity.Jobs.JobHandle,System.Single)
extern void BurstDensityConstraints_ApplyVorticityAndAtmosphere_mB08E5CC7602CE03DC15C33792AC0320047269AB3 (void);
// 0x0000031D Unity.Jobs.JobHandle Obi.BurstDensityConstraints::IdentityAnisotropy(Unity.Jobs.JobHandle)
extern void BurstDensityConstraints_IdentityAnisotropy_mF4B1F55394D53099A14A4F0919ED5ECBF7E631AA (void);
// 0x0000031E Unity.Jobs.JobHandle Obi.BurstDensityConstraints::AverageSmoothPositions(Unity.Jobs.JobHandle)
extern void BurstDensityConstraints_AverageSmoothPositions_m3CCD94810EA90442C6D242384973857DDBA53163 (void);
// 0x0000031F Unity.Jobs.JobHandle Obi.BurstDensityConstraints::AverageAnisotropy(Unity.Jobs.JobHandle)
extern void BurstDensityConstraints_AverageAnisotropy_m13C3ECFDF3234BC1A14104920A85AD970209CE2B (void);
// 0x00000320 System.Void Obi.BurstDensityConstraints/ClearFluidDataJob::Execute(System.Int32)
extern void ClearFluidDataJob_Execute_mD48A32AF77242D2E6CDFF920317494A459561F18 (void);
// 0x00000321 System.Void Obi.BurstDensityConstraints/UpdateInteractionsJob::Execute(System.Int32)
extern void UpdateInteractionsJob_Execute_m1EEE83F71148CE066BB48348ECCA642F2A1E80CE (void);
// 0x00000322 System.Void Obi.BurstDensityConstraints/CalculateLambdasJob::Execute(System.Int32)
extern void CalculateLambdasJob_Execute_m2B425DF0EEAD9932568604F718B2921296DA4A79 (void);
// 0x00000323 System.Void Obi.BurstDensityConstraints/ApplyVorticityConfinementAndAtmosphere::Execute(System.Int32)
extern void ApplyVorticityConfinementAndAtmosphere_Execute_mB2A90B74BE8FF621E0A2E25863B37740AF5D95D9 (void);
// 0x00000324 System.Void Obi.BurstDensityConstraints/IdentityAnisotropyJob::Execute(System.Int32)
extern void IdentityAnisotropyJob_Execute_m8BCDAC71C9652E2C66490C406047751573B95B40 (void);
// 0x00000325 System.Void Obi.BurstDensityConstraints/AverageSmoothPositionsJob::Execute(System.Int32)
extern void AverageSmoothPositionsJob_Execute_m8530DC393B3E014A32A67CF6BE5657D78714B493 (void);
// 0x00000326 System.Void Obi.BurstDensityConstraints/AverageAnisotropyJob::Execute(System.Int32)
extern void AverageAnisotropyJob_Execute_m8E8F860CE0895F6EEE07E14E2C09507F15CAF28C (void);
// 0x00000327 System.Void Obi.BurstDensityConstraintsBatch::.ctor(Obi.BurstDensityConstraints)
extern void BurstDensityConstraintsBatch__ctor_mB6C3F53C5168D7331932D08C931EC7B602712A0A (void);
// 0x00000328 Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstDensityConstraintsBatch_Initialize_m7884A93CD87D7006DA805934BC141B511F535AD9 (void);
// 0x00000329 Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstDensityConstraintsBatch_Evaluate_m951FDA2E0C0B4109E7675F5E0F61C84CDA2E151A (void);
// 0x0000032A Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstDensityConstraintsBatch_Apply_mAB46573B2FB97CB81902232085C3703950F4E8D1 (void);
// 0x0000032B Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::CalculateViscosityAndNormals(Unity.Jobs.JobHandle,System.Single)
extern void BurstDensityConstraintsBatch_CalculateViscosityAndNormals_mB6B1E6A8977BE8DC76932C65C3E06124A7140EBB (void);
// 0x0000032C Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::CalculateVorticity(Unity.Jobs.JobHandle)
extern void BurstDensityConstraintsBatch_CalculateVorticity_m5BAEBAE4AC97FD2EC47D95A216003E30D72D1FE9 (void);
// 0x0000032D Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::AccumulateSmoothPositions(Unity.Jobs.JobHandle)
extern void BurstDensityConstraintsBatch_AccumulateSmoothPositions_mAEB7E673A4E53C02447AAAA70199A9D2487A9020 (void);
// 0x0000032E Unity.Jobs.JobHandle Obi.BurstDensityConstraintsBatch::AccumulateAnisotropy(Unity.Jobs.JobHandle)
extern void BurstDensityConstraintsBatch_AccumulateAnisotropy_m7C86504F22194F961A7113A8DDF60366DA670727 (void);
// 0x0000032F System.Void Obi.BurstDensityConstraintsBatch/UpdateDensitiesJob::Execute(System.Int32)
extern void UpdateDensitiesJob_Execute_m59352095C34B63F24D23B524548542B60EF8B6F7 (void);
// 0x00000330 System.Void Obi.BurstDensityConstraintsBatch/ApplyDensityConstraintsJob::Execute(System.Int32)
extern void ApplyDensityConstraintsJob_Execute_m9CE8FB7550A4AFC0C47CD7569ABB38E69B978F16 (void);
// 0x00000331 System.Void Obi.BurstDensityConstraintsBatch/NormalsViscosityAndVorticityJob::Execute(System.Int32)
extern void NormalsViscosityAndVorticityJob_Execute_mBABF5C9E0E64304EEA30D40729CEAB8C02E78C86 (void);
// 0x00000332 System.Void Obi.BurstDensityConstraintsBatch/CalculateVorticityEta::Execute(System.Int32)
extern void CalculateVorticityEta_Execute_mBD1C201BA56247D5A29070646ECD1C54D8FA3B31 (void);
// 0x00000333 System.Void Obi.BurstDensityConstraintsBatch/AccumulateSmoothPositionsJob::Execute(System.Int32)
extern void AccumulateSmoothPositionsJob_Execute_m198C016973ED826593021549CB78790D8D97590C (void);
// 0x00000334 System.Void Obi.BurstDensityConstraintsBatch/AccumulateAnisotropyJob::Execute(System.Int32)
extern void AccumulateAnisotropyJob_Execute_mDC2644AC6278164D69AF0BF1AB94E439BA2B1396 (void);
// 0x00000335 System.Void Obi.Poly6Kernel::.ctor(System.Boolean)
extern void Poly6Kernel__ctor_m458F1673FE9C7B44144243984BF8D3351EF76641 (void);
// 0x00000336 System.Single Obi.Poly6Kernel::W(System.Single,System.Single)
extern void Poly6Kernel_W_mA9FD1FFD1D90D74DB6803EC97F570DAB31EAE849 (void);
// 0x00000337 System.Void Obi.SpikyKernel::.ctor(System.Boolean)
extern void SpikyKernel__ctor_mA630C18D4CA7AEE79027ADBEB1BFCBF26A7452BE (void);
// 0x00000338 System.Single Obi.SpikyKernel::W(System.Single,System.Single)
extern void SpikyKernel_W_mA7FD6FEFAF094558177A73FB38603EFC70E8D697 (void);
// 0x00000339 System.Void Obi.BurstDistanceConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstDistanceConstraints__ctor_m069D55C3879EBD80F0DA9EBE04BF2E4665C92F4C (void);
// 0x0000033A Obi.IConstraintsBatchImpl Obi.BurstDistanceConstraints::CreateConstraintsBatch()
extern void BurstDistanceConstraints_CreateConstraintsBatch_mAC76A2B7CAE8D6D745BC72BCFCF66DFFB59E2918 (void);
// 0x0000033B System.Void Obi.BurstDistanceConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstDistanceConstraints_RemoveBatch_m16908B57FCA878AFB863730B2456FBB4BD1CA3DE (void);
// 0x0000033C System.Void Obi.BurstDistanceConstraintsBatch::.ctor(Obi.BurstDistanceConstraints)
extern void BurstDistanceConstraintsBatch__ctor_mF0B06C747DC7B62A981276C4451DCB5C1E9C7AAE (void);
// 0x0000033D System.Void Obi.BurstDistanceConstraintsBatch::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void BurstDistanceConstraintsBatch_SetDistanceConstraints_m89D1EFCA556E894E49CAEBC9506145FA5F1062DE (void);
// 0x0000033E Unity.Jobs.JobHandle Obi.BurstDistanceConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstDistanceConstraintsBatch_Evaluate_m5F116504938B3902C4729AF320BBCF699E91685B (void);
// 0x0000033F Unity.Jobs.JobHandle Obi.BurstDistanceConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstDistanceConstraintsBatch_Apply_mB228CA05BF444CFAC93F865BFE0818079108F43A (void);
// 0x00000340 System.Void Obi.BurstDistanceConstraintsBatch/DistanceConstraintsBatchJob::Execute(System.Int32)
extern void DistanceConstraintsBatchJob_Execute_m8974C56ADBED13F088ADEC56AF17CBF723258A16 (void);
// 0x00000341 System.Void Obi.BurstDistanceConstraintsBatch/ApplyDistanceConstraintsBatchJob::Execute(System.Int32)
extern void ApplyDistanceConstraintsBatchJob_Execute_m3D79DE4E8EDE7541DC1B61399471933671AFAFDF (void);
// 0x00000342 System.Void Obi.ApplyBatchedCollisionConstraintsBatchJob::Execute(System.Int32)
extern void ApplyBatchedCollisionConstraintsBatchJob_Execute_m6B349C47BB948DA01570848A8C29F1D3BEF4C3D5 (void);
// 0x00000343 System.Void Obi.BurstParticleCollisionConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstParticleCollisionConstraints__ctor_m0E59AC63D9FD308E957F8C5A346C4B0FC225F1EE (void);
// 0x00000344 Obi.IConstraintsBatchImpl Obi.BurstParticleCollisionConstraints::CreateConstraintsBatch()
extern void BurstParticleCollisionConstraints_CreateConstraintsBatch_m7B3464699C43C7C53DADF07C2AE43F8F65EA5D8D (void);
// 0x00000345 System.Void Obi.BurstParticleCollisionConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstParticleCollisionConstraints_RemoveBatch_m64170A2A7F0002FF237F21EF5B42DD1F8CB12833 (void);
// 0x00000346 System.Int32 Obi.BurstParticleCollisionConstraints::GetConstraintCount()
extern void BurstParticleCollisionConstraints_GetConstraintCount_mDD050203A0C51B1F67186B24DF38F5764EF90467 (void);
// 0x00000347 System.Void Obi.BurstParticleCollisionConstraintsBatch::.ctor(Obi.BurstParticleCollisionConstraints)
extern void BurstParticleCollisionConstraintsBatch__ctor_mB4FE8DE0B3E4C9E89E6CB5A1DAD86AD86E1F8D15 (void);
// 0x00000348 System.Void Obi.BurstParticleCollisionConstraintsBatch::.ctor(Obi.BatchData)
extern void BurstParticleCollisionConstraintsBatch__ctor_m62534C1226C33861E70E8151B60BC1FE4A3CA1E6 (void);
// 0x00000349 Unity.Jobs.JobHandle Obi.BurstParticleCollisionConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstParticleCollisionConstraintsBatch_Initialize_mA8DA2295D258754046BA9D17FB19FEDFC945986F (void);
// 0x0000034A Unity.Jobs.JobHandle Obi.BurstParticleCollisionConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstParticleCollisionConstraintsBatch_Evaluate_m5156FEC0808BA2A778CC11EDE1E15A05BB71EEE2 (void);
// 0x0000034B Unity.Jobs.JobHandle Obi.BurstParticleCollisionConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstParticleCollisionConstraintsBatch_Apply_m51688D061B17E5E33A86D643F16BB3F17A186C92 (void);
// 0x0000034C System.Void Obi.BurstParticleCollisionConstraintsBatch/UpdateParticleContactsJob::Execute(System.Int32)
extern void UpdateParticleContactsJob_Execute_mA60AE07BFCE3A0F563D19A316C153C7F9887ED7E (void);
// 0x0000034D System.Void Obi.BurstParticleCollisionConstraintsBatch/ParticleCollisionConstraintsBatchJob::Execute(System.Int32)
extern void ParticleCollisionConstraintsBatchJob_Execute_m8485003051D23F0D75146E26663B7CA0AF2386F1 (void);
// 0x0000034E Obi.BurstCollisionMaterial Obi.BurstParticleCollisionConstraintsBatch/ParticleCollisionConstraintsBatchJob::CombineCollisionMaterials(System.Int32,System.Int32)
extern void ParticleCollisionConstraintsBatchJob_CombineCollisionMaterials_mEC293E92648E63BD95F591444FDFF4B18F33D703 (void);
// 0x0000034F System.Void Obi.BurstParticleFrictionConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstParticleFrictionConstraints__ctor_mF28E936F7126378219D48275D4453F81FB9ED434 (void);
// 0x00000350 Obi.IConstraintsBatchImpl Obi.BurstParticleFrictionConstraints::CreateConstraintsBatch()
extern void BurstParticleFrictionConstraints_CreateConstraintsBatch_m8346A4E3AEFEAAA93A6F7EE4395C8E578CD5A44D (void);
// 0x00000351 System.Void Obi.BurstParticleFrictionConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstParticleFrictionConstraints_RemoveBatch_m79E022580926338212A7B5F8369F4308F675A1A8 (void);
// 0x00000352 System.Int32 Obi.BurstParticleFrictionConstraints::GetConstraintCount()
extern void BurstParticleFrictionConstraints_GetConstraintCount_mFD9B9FFCBB8FC509CABDC56CDAC151DDD2894AC4 (void);
// 0x00000353 System.Void Obi.BurstParticleFrictionConstraintsBatch::.ctor(Obi.BurstParticleFrictionConstraints)
extern void BurstParticleFrictionConstraintsBatch__ctor_m5DBFED861162777EA753172BE5C49734B6AEF2A4 (void);
// 0x00000354 System.Void Obi.BurstParticleFrictionConstraintsBatch::.ctor(Obi.BatchData)
extern void BurstParticleFrictionConstraintsBatch__ctor_mA8F4839E9244CFC1FC091AB7FE7E4C8CF9A13A4A (void);
// 0x00000355 Unity.Jobs.JobHandle Obi.BurstParticleFrictionConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstParticleFrictionConstraintsBatch_Initialize_m3AAE395541CD850FD9709DA038F1E595B91182B8 (void);
// 0x00000356 Unity.Jobs.JobHandle Obi.BurstParticleFrictionConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstParticleFrictionConstraintsBatch_Evaluate_m3A757A216EB2A595E353BEE0424AD014B13AD2B1 (void);
// 0x00000357 Unity.Jobs.JobHandle Obi.BurstParticleFrictionConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstParticleFrictionConstraintsBatch_Apply_mB336EF2223B6F7525003DA96ED14AA3BFC2DD30B (void);
// 0x00000358 System.Void Obi.BurstParticleFrictionConstraintsBatch/ParticleFrictionConstraintsBatchJob::Execute(System.Int32)
extern void ParticleFrictionConstraintsBatchJob_Execute_m79FE4542796B3C2B9146A83500647A9D0680830C (void);
// 0x00000359 Obi.BurstCollisionMaterial Obi.BurstParticleFrictionConstraintsBatch/ParticleFrictionConstraintsBatchJob::CombineCollisionMaterials(System.Int32,System.Int32)
extern void ParticleFrictionConstraintsBatchJob_CombineCollisionMaterials_mF0CD7C3874B720CC924931A83AB1EE44FBF0797D (void);
// 0x0000035A System.Void Obi.BurstPinConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstPinConstraints__ctor_m16AF01D598A8930881C5164CD7F8BEFF85046751 (void);
// 0x0000035B Obi.IConstraintsBatchImpl Obi.BurstPinConstraints::CreateConstraintsBatch()
extern void BurstPinConstraints_CreateConstraintsBatch_mCF848FC65CC60655B498F6B6C85FC7FBF79D265A (void);
// 0x0000035C System.Void Obi.BurstPinConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstPinConstraints_RemoveBatch_mC49FA4F97486F185171751D0A036BE8B737201CC (void);
// 0x0000035D System.Void Obi.BurstPinConstraintsBatch::.ctor(Obi.BurstPinConstraints)
extern void BurstPinConstraintsBatch__ctor_m1A637B9849DA051CE9C6E3AB44F0B5609217B115 (void);
// 0x0000035E System.Void Obi.BurstPinConstraintsBatch::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void BurstPinConstraintsBatch_SetPinConstraints_mC82D58B41EAB2D7DB4D6E479003BCD81D5EE898F (void);
// 0x0000035F Unity.Jobs.JobHandle Obi.BurstPinConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstPinConstraintsBatch_Evaluate_m4EEDF2C7D717FF5D55781B855290A83D12CE3BE5 (void);
// 0x00000360 Unity.Jobs.JobHandle Obi.BurstPinConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstPinConstraintsBatch_Apply_m6B9F30FD5304D83F40C52E4AD11F2BF5DEB35E74 (void);
// 0x00000361 System.Void Obi.BurstPinConstraintsBatch/PinConstraintsBatchJob::Execute()
extern void PinConstraintsBatchJob_Execute_mEF5E6798264EF1F83CC82A6A98B54DECCD4D1E36 (void);
// 0x00000362 System.Void Obi.BurstPinConstraintsBatch/ApplyPinConstraintsBatchJob::Execute()
extern void ApplyPinConstraintsBatchJob_Execute_mB647701B0C09CE1D750AF92DA09F9F8E78FD52DB (void);
// 0x00000363 System.Void Obi.BurstShapeMatchingConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstShapeMatchingConstraints__ctor_m0B050ABA0169470B1A7FDC3AE039E9486251D29A (void);
// 0x00000364 Obi.IConstraintsBatchImpl Obi.BurstShapeMatchingConstraints::CreateConstraintsBatch()
extern void BurstShapeMatchingConstraints_CreateConstraintsBatch_m419791AEEE7452103FF7878019023EA0F6739BD5 (void);
// 0x00000365 System.Void Obi.BurstShapeMatchingConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstShapeMatchingConstraints_RemoveBatch_m5BBE41B473B96570BA54EE7F63A9E3C7ECF640BD (void);
// 0x00000366 System.Void Obi.BurstShapeMatchingConstraintsBatch::.ctor(Obi.BurstShapeMatchingConstraints)
extern void BurstShapeMatchingConstraintsBatch__ctor_mD3A6AA7CDA7637D9B8D9AA4276876A9FC2126522 (void);
// 0x00000367 System.Void Obi.BurstShapeMatchingConstraintsBatch::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
extern void BurstShapeMatchingConstraintsBatch_SetShapeMatchingConstraints_mD7B426D930EA25257837F4352448D97907AD8CC0 (void);
// 0x00000368 System.Void Obi.BurstShapeMatchingConstraintsBatch::Destroy()
extern void BurstShapeMatchingConstraintsBatch_Destroy_mA07771DB7398466BCD0C10936A03823C43253C16 (void);
// 0x00000369 Unity.Jobs.JobHandle Obi.BurstShapeMatchingConstraintsBatch::Initialize(Unity.Jobs.JobHandle,System.Single)
extern void BurstShapeMatchingConstraintsBatch_Initialize_m12CBDA7C9CCE459EA9149D3DDA2F3C63BF63F394 (void);
// 0x0000036A Unity.Jobs.JobHandle Obi.BurstShapeMatchingConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstShapeMatchingConstraintsBatch_Evaluate_m2054022121F711252557EF1F696843DD1DF63590 (void);
// 0x0000036B Unity.Jobs.JobHandle Obi.BurstShapeMatchingConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstShapeMatchingConstraintsBatch_Apply_m579EEF8C0773D60CAD1333EAE243B9276473EA13 (void);
// 0x0000036C System.Void Obi.BurstShapeMatchingConstraintsBatch::CalculateRestShapeMatching()
extern void BurstShapeMatchingConstraintsBatch_CalculateRestShapeMatching_m134076AF07B375506EBC37128FE4D1CEA936AEF4 (void);
// 0x0000036D System.Void Obi.BurstShapeMatchingConstraintsBatch::RecalculateRestData(System.Int32,Unity.Collections.NativeArray`1<System.Int32>&,Unity.Collections.NativeArray`1<System.Int32>&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4x4>&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4x4>&,Unity.Collections.NativeArray`1<System.Int32>&,Unity.Collections.NativeArray`1<System.Single>&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>&,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>&,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>&)
extern void BurstShapeMatchingConstraintsBatch_RecalculateRestData_mFE475921ECBA487E5BB3C157D059C0718FF34084 (void);
// 0x0000036E System.Void Obi.BurstShapeMatchingConstraintsBatch/ShapeMatchingCalculateRestJob::Execute(System.Int32)
extern void ShapeMatchingCalculateRestJob_Execute_mF557395226A5479543E3F443C1787EE2F5D4AB24 (void);
// 0x0000036F System.Void Obi.BurstShapeMatchingConstraintsBatch/ShapeMatchingConstraintsBatchJob::Execute(System.Int32)
extern void ShapeMatchingConstraintsBatchJob_Execute_mE253B06332A40DCA4C6453A876DB428FA345B047 (void);
// 0x00000370 System.Void Obi.BurstShapeMatchingConstraintsBatch/ApplyShapeMatchingConstraintsBatchJob::Execute(System.Int32)
extern void ApplyShapeMatchingConstraintsBatchJob_Execute_m1ED0E064C42CCD032FDE804F098C1250B8DDE687 (void);
// 0x00000371 System.Void Obi.BurstSkinConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstSkinConstraints__ctor_m7203B844BA1CA2592BE81D0D05B692CC2AC7E191 (void);
// 0x00000372 Obi.IConstraintsBatchImpl Obi.BurstSkinConstraints::CreateConstraintsBatch()
extern void BurstSkinConstraints_CreateConstraintsBatch_m1FC6B0D73F7FC806E76F0545926DE742008E8FBA (void);
// 0x00000373 System.Void Obi.BurstSkinConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstSkinConstraints_RemoveBatch_m34D9EA06E8213228121EEEA8CC782C87C7372F51 (void);
// 0x00000374 System.Void Obi.BurstSkinConstraintsBatch::.ctor(Obi.BurstSkinConstraints)
extern void BurstSkinConstraintsBatch__ctor_mC2146DD3CDAFA928092F86F972B6EA5FC642123C (void);
// 0x00000375 System.Void Obi.BurstSkinConstraintsBatch::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void BurstSkinConstraintsBatch_SetSkinConstraints_m45A1CE4D67EE3CC055D11F20D330BCD340F0AF19 (void);
// 0x00000376 Unity.Jobs.JobHandle Obi.BurstSkinConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstSkinConstraintsBatch_Evaluate_mBD987ABEA52B5113BB442929413ECB04674E99A2 (void);
// 0x00000377 Unity.Jobs.JobHandle Obi.BurstSkinConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstSkinConstraintsBatch_Apply_m267C40110D1FB9E8EDEB5099443F59D59F5B893C (void);
// 0x00000378 System.Void Obi.BurstSkinConstraintsBatch/SkinConstraintsBatchJob::Execute(System.Int32)
extern void SkinConstraintsBatchJob_Execute_mE9464013B468FA413A33380F7080A6E8984B03A5 (void);
// 0x00000379 System.Void Obi.BurstSkinConstraintsBatch/ApplySkinConstraintsBatchJob::Execute(System.Int32)
extern void ApplySkinConstraintsBatchJob_Execute_m65A2A62E22D2996164E8F0E34B60EAB4C923CDCD (void);
// 0x0000037A System.Void Obi.BurstStitchConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstStitchConstraints__ctor_mB747CF967BA6E4EE50397F14D46B8B7DA14A804B (void);
// 0x0000037B Obi.IConstraintsBatchImpl Obi.BurstStitchConstraints::CreateConstraintsBatch()
extern void BurstStitchConstraints_CreateConstraintsBatch_m8C527A61912F3EAACCBED5D7984AB355ED672600 (void);
// 0x0000037C System.Void Obi.BurstStitchConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstStitchConstraints_RemoveBatch_m618BBA385C7B77C3D7C98F23B672452465100330 (void);
// 0x0000037D System.Void Obi.BurstStitchConstraintsBatch::.ctor(Obi.BurstStitchConstraints)
extern void BurstStitchConstraintsBatch__ctor_m503248B040EE8C6F74F7A5880AF7D5063BE0AD74 (void);
// 0x0000037E System.Void Obi.BurstStitchConstraintsBatch::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void BurstStitchConstraintsBatch_SetStitchConstraints_m9324CC010E744DF4A39DB9F37F2FCE97B678F793 (void);
// 0x0000037F Unity.Jobs.JobHandle Obi.BurstStitchConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstStitchConstraintsBatch_Evaluate_mF3C332AFB74C39800785FCB3EF9AD0037B611860 (void);
// 0x00000380 Unity.Jobs.JobHandle Obi.BurstStitchConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstStitchConstraintsBatch_Apply_m6F3AE07C4231D6F0D0A5716D84F84744381009B6 (void);
// 0x00000381 System.Void Obi.BurstStitchConstraintsBatch/StitchConstraintsBatchJob::Execute()
extern void StitchConstraintsBatchJob_Execute_mA5D5BEA6FB1DA22EEAB3C0CD7CEB52145DBE1BCD (void);
// 0x00000382 System.Void Obi.BurstStitchConstraintsBatch/ApplyStitchConstraintsBatchJob::Execute()
extern void ApplyStitchConstraintsBatchJob_Execute_m50E228A24C8D90CE705CA819CBCB9F3D1FF7E7B6 (void);
// 0x00000383 System.Void Obi.BurstStretchShearConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstStretchShearConstraints__ctor_m2C91556F549F1C111EC012472476B9156DE41A12 (void);
// 0x00000384 Obi.IConstraintsBatchImpl Obi.BurstStretchShearConstraints::CreateConstraintsBatch()
extern void BurstStretchShearConstraints_CreateConstraintsBatch_m8A10C037510E47B5F17C1C454263A95B893FCDC1 (void);
// 0x00000385 System.Void Obi.BurstStretchShearConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstStretchShearConstraints_RemoveBatch_m09C8EFF7AF589EED5ECB673986EFC4A53DC98D0A (void);
// 0x00000386 System.Void Obi.BurstStretchShearConstraintsBatch::.ctor(Obi.BurstStretchShearConstraints)
extern void BurstStretchShearConstraintsBatch__ctor_m0B7DCD678CEB70494E6B9639BB1DC964B410C0F1 (void);
// 0x00000387 System.Void Obi.BurstStretchShearConstraintsBatch::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
extern void BurstStretchShearConstraintsBatch_SetStretchShearConstraints_mBCE29CFCF8BC3A52C5168F861D0D928975DB54CA (void);
// 0x00000388 Unity.Jobs.JobHandle Obi.BurstStretchShearConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstStretchShearConstraintsBatch_Evaluate_m7ABCD9857963B8964AD4313EBA15893DB12DB078 (void);
// 0x00000389 Unity.Jobs.JobHandle Obi.BurstStretchShearConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstStretchShearConstraintsBatch_Apply_mBB933D7AD6511C7F8BA82292C137240A5F76F2A3 (void);
// 0x0000038A System.Void Obi.BurstStretchShearConstraintsBatch/StretchShearConstraintsBatchJob::Execute(System.Int32)
extern void StretchShearConstraintsBatchJob_Execute_m38FE03FB561A839F2E3C895AC52308E3374C50CC (void);
// 0x0000038B System.Void Obi.BurstStretchShearConstraintsBatch/ApplyStretchShearConstraintsBatchJob::Execute(System.Int32)
extern void ApplyStretchShearConstraintsBatchJob_Execute_mB1D83D142361E62A2D687C29EE346B26AD985A70 (void);
// 0x0000038C System.Void Obi.BurstTetherConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstTetherConstraints__ctor_m34FD8D32D12C33A6401F8DB8932DFD387471F22E (void);
// 0x0000038D Obi.IConstraintsBatchImpl Obi.BurstTetherConstraints::CreateConstraintsBatch()
extern void BurstTetherConstraints_CreateConstraintsBatch_m0116EEF8DC8CE5CA2562DC4E6A091755A9E9D138 (void);
// 0x0000038E System.Void Obi.BurstTetherConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstTetherConstraints_RemoveBatch_m650ED58729D5166338B77B9CE30850ABCD3E9155 (void);
// 0x0000038F System.Void Obi.BurstTetherConstraintsBatch::.ctor(Obi.BurstTetherConstraints)
extern void BurstTetherConstraintsBatch__ctor_m70E75DB508A77183C1CD484EBF9E63A8676D07E0 (void);
// 0x00000390 System.Void Obi.BurstTetherConstraintsBatch::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void BurstTetherConstraintsBatch_SetTetherConstraints_m988FCF9B205603EE496CA5181F037F902331AAC5 (void);
// 0x00000391 Unity.Jobs.JobHandle Obi.BurstTetherConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstTetherConstraintsBatch_Evaluate_m06654C031806619BF5FF50F07D02B7BA549630A0 (void);
// 0x00000392 Unity.Jobs.JobHandle Obi.BurstTetherConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstTetherConstraintsBatch_Apply_m99D86EAFFB29B296AA8F9E7553E4A584C3F4D76C (void);
// 0x00000393 System.Void Obi.BurstTetherConstraintsBatch/TetherConstraintsBatchJob::Execute(System.Int32)
extern void TetherConstraintsBatchJob_Execute_mEF1FE91E81FCED26D0A12BF953AFB35780ED52C8 (void);
// 0x00000394 System.Void Obi.BurstTetherConstraintsBatch/ApplyTetherConstraintsBatchJob::Execute(System.Int32)
extern void ApplyTetherConstraintsBatchJob_Execute_mBF97BD7B8E8E46587578F03ADCD029486BD104CD (void);
// 0x00000395 System.Void Obi.BurstVolumeConstraints::.ctor(Obi.BurstSolverImpl)
extern void BurstVolumeConstraints__ctor_m572903003FCEC3CE2A23EFCE28FA3BED68810DB6 (void);
// 0x00000396 Obi.IConstraintsBatchImpl Obi.BurstVolumeConstraints::CreateConstraintsBatch()
extern void BurstVolumeConstraints_CreateConstraintsBatch_m6C62BE9A7681C03DA2A70D091F1374679B77578B (void);
// 0x00000397 System.Void Obi.BurstVolumeConstraints::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void BurstVolumeConstraints_RemoveBatch_m2F1D3097AD2249B40FF0C0BBA64F2FA026E4ECFD (void);
// 0x00000398 System.Void Obi.BurstVolumeConstraintsBatch::.ctor(Obi.BurstVolumeConstraints)
extern void BurstVolumeConstraintsBatch__ctor_m2A07696AD34BBDF0104A55B12AD247C0AF0BC8C2 (void);
// 0x00000399 System.Void Obi.BurstVolumeConstraintsBatch::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void BurstVolumeConstraintsBatch_SetVolumeConstraints_m683F87D414D09123A02BD6C122EF1D40B6DBB43A (void);
// 0x0000039A Unity.Jobs.JobHandle Obi.BurstVolumeConstraintsBatch::Evaluate(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstVolumeConstraintsBatch_Evaluate_mB2CF2EFCFED1E8A493A0FCE4FDEE99472BA9CC0D (void);
// 0x0000039B Unity.Jobs.JobHandle Obi.BurstVolumeConstraintsBatch::Apply(Unity.Jobs.JobHandle,System.Single)
extern void BurstVolumeConstraintsBatch_Apply_m539BB6E964A7D666939558EDCA2442AA17B780E0 (void);
// 0x0000039C System.Void Obi.BurstVolumeConstraintsBatch/VolumeConstraintsBatchJob::Execute(System.Int32)
extern void VolumeConstraintsBatchJob_Execute_mB6BC30FDA2A00B126A2939FE5841FABE48971AB6 (void);
// 0x0000039D System.Void Obi.BurstVolumeConstraintsBatch/ApplyVolumeConstraintsBatchJob::Execute(System.Int32)
extern void ApplyVolumeConstraintsBatchJob_Execute_m8A7F092F18089C81D544CD0E08BCA2EE41AB7C25 (void);
// 0x0000039E Unity.Mathematics.float4 Obi.BurstAabb::get_size()
extern void BurstAabb_get_size_m5E23B4420CB21B196C931459BECA271D6567E28C (void);
// 0x0000039F Unity.Mathematics.float4 Obi.BurstAabb::get_center()
extern void BurstAabb_get_center_mEF7385D666BE5EE77B04D4C1B507CC277498DF83 (void);
// 0x000003A0 System.Void Obi.BurstAabb::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4)
extern void BurstAabb__ctor_m5EA38EB6DA406EE85C1CDCC3B4053EFDA9AF999C (void);
// 0x000003A1 System.Void Obi.BurstAabb::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstAabb__ctor_m25051D2B2D8D634E1497B4FBA7DA5855CA630293 (void);
// 0x000003A2 System.Void Obi.BurstAabb::.ctor(Unity.Mathematics.float2,Unity.Mathematics.float2,System.Single)
extern void BurstAabb__ctor_mBFC57B41EDB0B6AD54F1CEED8ABDFCE2CF73FD6D (void);
// 0x000003A3 System.Void Obi.BurstAabb::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstAabb__ctor_m20D651928C007C6CDCF1FA28E061578888324215 (void);
// 0x000003A4 System.Single Obi.BurstAabb::AverageAxisLength()
extern void BurstAabb_AverageAxisLength_mC3D10DA8BEC0F0BA5B418EDB620BDCD4A2951C8E (void);
// 0x000003A5 System.Single Obi.BurstAabb::MaxAxisLength()
extern void BurstAabb_MaxAxisLength_m00B824BC8D68124AD6362187459834CD10F2C9AD (void);
// 0x000003A6 System.Void Obi.BurstAabb::EncapsulateParticle(Unity.Mathematics.float4,System.Single)
extern void BurstAabb_EncapsulateParticle_m2BF518A13C65F7BC9B5EB0175BA00D3A07FA3350 (void);
// 0x000003A7 System.Void Obi.BurstAabb::EncapsulateParticle(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstAabb_EncapsulateParticle_m9154ADC7C45964966B05F06F5B92412630067FA7 (void);
// 0x000003A8 System.Void Obi.BurstAabb::EncapsulateBounds(Obi.BurstAabb&)
extern void BurstAabb_EncapsulateBounds_m9AFA483BA1B7BEBF77C1B85D57549FB36375E1D7 (void);
// 0x000003A9 System.Void Obi.BurstAabb::Expand(Unity.Mathematics.float4)
extern void BurstAabb_Expand_mD6FD7B154B8700A8F46316D8ACFFD928F92DAD49 (void);
// 0x000003AA System.Void Obi.BurstAabb::Sweep(Unity.Mathematics.float4)
extern void BurstAabb_Sweep_mCB51F0C277EA7EF3C7F033CF125C1A0B04E7541E (void);
// 0x000003AB System.Void Obi.BurstAabb::Transform(Obi.BurstAffineTransform&)
extern void BurstAabb_Transform_m972887B0B52D4B08B4806F6BB8196E17B64C0B90 (void);
// 0x000003AC System.Void Obi.BurstAabb::Transform(Unity.Mathematics.float4x4&)
extern void BurstAabb_Transform_m20348C56E245E750A6399FA6F7C2D767EE11B91C (void);
// 0x000003AD Obi.BurstAabb Obi.BurstAabb::Transformed(Obi.BurstAffineTransform&)
extern void BurstAabb_Transformed_m09422ADB914E6917056FB53FEDDC172E7AEE88C1 (void);
// 0x000003AE Obi.BurstAabb Obi.BurstAabb::Transformed(Unity.Mathematics.float4x4&)
extern void BurstAabb_Transformed_mE44D84A994DBAAD9EED74F026F8220ADCD9A249E (void);
// 0x000003AF System.Boolean Obi.BurstAabb::IntersectsAabb(Obi.BurstAabb&,System.Boolean)
extern void BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6 (void);
// 0x000003B0 System.Boolean Obi.BurstAabb::IntersectsRay(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Boolean)
extern void BurstAabb_IntersectsRay_m94E7181E1827E8BE7F727FBCFB48EB11CC40CFAD (void);
// 0x000003B1 System.Void Obi.BurstAffineTransform::.ctor(Unity.Mathematics.float4,Unity.Mathematics.quaternion,Unity.Mathematics.float4)
extern void BurstAffineTransform__ctor_m05A35A300C65D6551377AD2C948ACA1FC2F8667C (void);
// 0x000003B2 Obi.BurstAffineTransform Obi.BurstAffineTransform::op_Multiply(Obi.BurstAffineTransform,Obi.BurstAffineTransform)
extern void BurstAffineTransform_op_Multiply_mB96F7410C6F8B9421CC886A7C77CF07B41CABEA6 (void);
// 0x000003B3 Obi.BurstAffineTransform Obi.BurstAffineTransform::Inverse()
extern void BurstAffineTransform_Inverse_mDD81DD6911040D1CA2FE9F0D1F315F7812DF27B5 (void);
// 0x000003B4 Obi.BurstAffineTransform Obi.BurstAffineTransform::Interpolate(Obi.BurstAffineTransform,System.Single,System.Single,System.Single)
extern void BurstAffineTransform_Interpolate_mECFFF4666C1393B072387A61B32573B56BCFE676 (void);
// 0x000003B5 Unity.Mathematics.float4 Obi.BurstAffineTransform::TransformPoint(Unity.Mathematics.float4)
extern void BurstAffineTransform_TransformPoint_m391756FAA0719FE03FA3ABFD002CF41953DFE610 (void);
// 0x000003B6 Unity.Mathematics.float4 Obi.BurstAffineTransform::InverseTransformPoint(Unity.Mathematics.float4)
extern void BurstAffineTransform_InverseTransformPoint_m62FF98D4C1F4B903143CBEBB8BE560DD8973E955 (void);
// 0x000003B7 Unity.Mathematics.float4 Obi.BurstAffineTransform::TransformPointUnscaled(Unity.Mathematics.float4)
extern void BurstAffineTransform_TransformPointUnscaled_m88D0781E4CA7ACAB74CC46049AF03EEE34AF17D8 (void);
// 0x000003B8 Unity.Mathematics.float4 Obi.BurstAffineTransform::InverseTransformPointUnscaled(Unity.Mathematics.float4)
extern void BurstAffineTransform_InverseTransformPointUnscaled_mB57EB7ACC6E6630D7AB86C3B3C4B330CDA781675 (void);
// 0x000003B9 Unity.Mathematics.float4 Obi.BurstAffineTransform::TransformDirection(Unity.Mathematics.float4)
extern void BurstAffineTransform_TransformDirection_m6194FE90F0734E4FAC3D9EC2F18B1418E054B284 (void);
// 0x000003BA Unity.Mathematics.float4 Obi.BurstAffineTransform::InverseTransformDirection(Unity.Mathematics.float4)
extern void BurstAffineTransform_InverseTransformDirection_mD556AD3447707CC7AF2C21B42B1009B169B07E13 (void);
// 0x000003BB Unity.Mathematics.float4 Obi.BurstAffineTransform::TransformVector(Unity.Mathematics.float4)
extern void BurstAffineTransform_TransformVector_m93FE5B596EE6E92173AEF3E44B265907B0371AF0 (void);
// 0x000003BC Unity.Mathematics.float4 Obi.BurstAffineTransform::InverseTransformVector(Unity.Mathematics.float4)
extern void BurstAffineTransform_InverseTransformVector_m5F477834E781A85ADC934DE24BC0FEB89A6BDDF9 (void);
// 0x000003BD System.Void Obi.BurstCellSpan::.ctor(Obi.CellSpan)
extern void BurstCellSpan__ctor_m06DA83A6F5C75A3D3B7B8E2F97C86D245EB3DD9D (void);
// 0x000003BE System.Void Obi.BurstCellSpan::.ctor(Unity.Mathematics.int4,Unity.Mathematics.int4)
extern void BurstCellSpan__ctor_m285AB617964F7D42BF536E07ED69BEA63327EE37 (void);
// 0x000003BF System.Int32 Obi.BurstCellSpan::get_level()
extern void BurstCellSpan_get_level_mEA8299C260E4E7D5A362075EAFDBAEC20CDCF21B (void);
// 0x000003C0 System.Boolean Obi.BurstCellSpan::Equals(Obi.BurstCellSpan)
extern void BurstCellSpan_Equals_mE730FAAEE94E797BCE3F326B494C301956A7014D (void);
// 0x000003C1 System.Boolean Obi.BurstCellSpan::Equals(System.Object)
extern void BurstCellSpan_Equals_mACAF86A3EDB8CCB7033E573669BBE26088CCF696 (void);
// 0x000003C2 System.Int32 Obi.BurstCellSpan::GetHashCode()
extern void BurstCellSpan_GetHashCode_mACB4CF48DD8B4D70211F28DACDEE8DC6A7F421EA (void);
// 0x000003C3 System.Boolean Obi.BurstCellSpan::op_Equality(Obi.BurstCellSpan,Obi.BurstCellSpan)
extern void BurstCellSpan_op_Equality_mEB4B1016C43F6753ABE279B48DF7349FC3AE6E02 (void);
// 0x000003C4 System.Boolean Obi.BurstCellSpan::op_Inequality(Obi.BurstCellSpan,Obi.BurstCellSpan)
extern void BurstCellSpan_op_Inequality_m571B820F6562829C7EC1A00B80BE1E158013612A (void);
// 0x000003C5 Obi.BurstCollisionMaterial Obi.BurstCollisionMaterial::CombineWith(Obi.BurstCollisionMaterial,Obi.BurstCollisionMaterial)
extern void BurstCollisionMaterial_CombineWith_mC472F029D139FD8BC35E71EB37CA9CEF821B2AFD (void);
// 0x000003C6 System.Void Obi.BurstInertialFrame::.ctor(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion)
extern void BurstInertialFrame__ctor_mA98B43A3120665CC585634E855B81B740024C066 (void);
// 0x000003C7 System.Void Obi.BurstInertialFrame::.ctor(Obi.BurstAffineTransform)
extern void BurstInertialFrame__ctor_m23E7F3F42FD0210FF6C53F7D04954946BC11A943 (void);
// 0x000003C8 System.Void Obi.BurstInertialFrame::Update(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,System.Single)
extern void BurstInertialFrame_Update_mFDDBF9FACE375ABB5982E879EDC404B54CFF6E34 (void);
// 0x000003C9 System.Void Obi.BatchLUT::.ctor(System.Int32)
extern void BatchLUT__ctor_m93F109809773B154AF095575B0C00C35E224346C (void);
// 0x000003CA System.Void Obi.BatchLUT::Dispose()
extern void BatchLUT_Dispose_mCFABB1B7385B31C2C7DD8AD9123205A3732D13BE (void);
// 0x000003CB System.Void Obi.BatchData::.ctor(System.Int32,System.Int32)
extern void BatchData__ctor_mD6B3E7F16DF9B5364DDCC3E92B5CDF971E4A69BD (void);
// 0x000003CC System.Void Obi.BatchData::GetConstraintRange(System.Int32,System.Int32&,System.Int32&)
extern void BatchData_GetConstraintRange_m685408EAF447F1B6A4AA9C17803E061A58752449 (void);
// 0x000003CD System.Void Obi.ConstraintBatcher::.ctor(System.Int32)
extern void ConstraintBatcher__ctor_mEEC00AF4C13C2B7D3E750958E9F78A639DCFBC76 (void);
// 0x000003CE System.Void Obi.ConstraintBatcher::Dispose()
extern void ConstraintBatcher_Dispose_m832285B8BE78DFA00F4E2253150C447F9E546889 (void);
// 0x000003CF Unity.Jobs.JobHandle Obi.ConstraintBatcher::BatchConstraints(T&,System.Int32,Unity.Collections.NativeArray`1<Obi.BatchData>&,Unity.Collections.NativeArray`1<System.Int32>&,Unity.Jobs.JobHandle)
// 0x000003D0 System.Boolean Obi.ConstraintBatcher/WorkItem::Add(System.Int32)
extern void WorkItem_Add_m8A0EF0EB1E6104C1CD4E12708761C75BCE6281DD (void);
// 0x000003D1 System.Void Obi.ConstraintBatcher/BatchContactsJob`1::Execute()
// 0x000003D2 Unity.Jobs.JobHandle Obi.ConstraintSorter::SortConstraints(System.Int32,Unity.Collections.NativeArray`1<T>,Unity.Collections.NativeArray`1<T>&,Unity.Jobs.JobHandle)
// 0x000003D3 System.Int32 Obi.ConstraintSorter/ConstraintComparer`1::Compare(K,K)
// 0x000003D4 System.Void Obi.ConstraintSorter/CountSortPerFirstParticleJob`1::Execute()
// 0x000003D5 System.Void Obi.ConstraintSorter/SortSubArraysJob`1::Execute(System.Int32)
// 0x000003D6 System.Void Obi.ConstraintSorter/SortSubArraysJob`1::DefaultSortOfSubArrays(Unity.Collections.NativeArray`1<K>,System.Int32,System.Int32,Obi.ConstraintSorter/ConstraintComparer`1<K>)
// 0x000003D7 System.Int32 Obi.ContactProvider::GetConstraintCount()
extern void ContactProvider_GetConstraintCount_mACC69AA75EE8103A1AAED3B3924CB26DF9798FEF (void);
// 0x000003D8 System.Int32 Obi.ContactProvider::GetParticleCount(System.Int32)
extern void ContactProvider_GetParticleCount_m3C3C405609067B4AF80D1D4269CDB486BA02D9FC (void);
// 0x000003D9 System.Int32 Obi.ContactProvider::GetParticle(System.Int32,System.Int32)
extern void ContactProvider_GetParticle_m0B5FB5F90C47141EF186E6ECE7169AD3F0B5E54E (void);
// 0x000003DA System.Void Obi.ContactProvider::WriteSortedConstraint(System.Int32,System.Int32)
extern void ContactProvider_WriteSortedConstraint_m5568D339D5163D4BAF3BF92562D993B463670F5E (void);
// 0x000003DB System.Int32 Obi.FluidInteractionProvider::GetConstraintCount()
extern void FluidInteractionProvider_GetConstraintCount_m010E43F859A7E41424DDDD5B8C03B75C3B05F3FA (void);
// 0x000003DC System.Int32 Obi.FluidInteractionProvider::GetParticleCount(System.Int32)
extern void FluidInteractionProvider_GetParticleCount_m7E6514985CBFF6300D92399124419655353DB050 (void);
// 0x000003DD System.Int32 Obi.FluidInteractionProvider::GetParticle(System.Int32,System.Int32)
extern void FluidInteractionProvider_GetParticle_mBB30B78F2DEA50E3E3BDEABAE3B2CB39DBA52DCF (void);
// 0x000003DE System.Void Obi.FluidInteractionProvider::WriteSortedConstraint(System.Int32,System.Int32)
extern void FluidInteractionProvider_WriteSortedConstraint_m2CAE0F1AC443F672E9503FC238EEC9A73C84DFE3 (void);
// 0x000003DF System.Int32 Obi.IConstraintProvider::GetConstraintCount()
// 0x000003E0 System.Int32 Obi.IConstraintProvider::GetParticleCount(System.Int32)
// 0x000003E1 System.Int32 Obi.IConstraintProvider::GetParticle(System.Int32,System.Int32)
// 0x000003E2 System.Void Obi.IConstraintProvider::WriteSortedConstraint(System.Int32,System.Int32)
// 0x000003E3 System.Int32 Obi.FluidInteraction::GetParticleCount()
extern void FluidInteraction_GetParticleCount_m986AE991B21B5250E354ADCE40955BB274A954C0 (void);
// 0x000003E4 System.Int32 Obi.FluidInteraction::GetParticle(System.Int32)
extern void FluidInteraction_GetParticle_mE71B2BF68C60B06FFB723114CCB4874EFBAD83E9 (void);
// 0x000003E5 System.Void Obi.NativeMultilevelGrid`1::.ctor(System.Int32,Unity.Collections.Allocator)
// 0x000003E6 System.Int32 Obi.NativeMultilevelGrid`1::get_CellCount()
// 0x000003E7 System.Void Obi.NativeMultilevelGrid`1::Clear()
// 0x000003E8 System.Void Obi.NativeMultilevelGrid`1::Dispose()
// 0x000003E9 System.Int32 Obi.NativeMultilevelGrid`1::GetOrCreateCell(Unity.Mathematics.int4)
// 0x000003EA System.Boolean Obi.NativeMultilevelGrid`1::TryGetCellIndex(Unity.Mathematics.int4,System.Int32&)
// 0x000003EB System.Void Obi.NativeMultilevelGrid`1::RemoveEmpty()
// 0x000003EC System.Int32 Obi.NativeMultilevelGrid`1::GridLevelForSize(System.Single)
// 0x000003ED System.Single Obi.NativeMultilevelGrid`1::CellSizeOfLevel(System.Int32)
// 0x000003EE Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1::GetParentCellCoords(Unity.Mathematics.int4,System.Int32)
// 0x000003EF System.Void Obi.NativeMultilevelGrid`1::RemoveFromCells(Obi.BurstCellSpan,T)
// 0x000003F0 System.Void Obi.NativeMultilevelGrid`1::AddToCells(Obi.BurstCellSpan,T)
// 0x000003F1 System.Void Obi.NativeMultilevelGrid`1::GetCellCoordsForBoundsAtLevel(Unity.Collections.NativeList`1<Unity.Mathematics.int4>,Obi.BurstAabb,System.Int32,System.Int32)
// 0x000003F2 System.Void Obi.NativeMultilevelGrid`1::IncreaseLevelPopulation(System.Int32)
// 0x000003F3 System.Void Obi.NativeMultilevelGrid`1::DecreaseLevelPopulation(System.Int32)
// 0x000003F4 System.Void Obi.NativeMultilevelGrid`1/Cell`1::.ctor(Unity.Mathematics.int4)
// 0x000003F5 Unity.Mathematics.int4 Obi.NativeMultilevelGrid`1/Cell`1::get_Coords()
// 0x000003F6 System.Int32 Obi.NativeMultilevelGrid`1/Cell`1::get_Length()
// 0x000003F7 System.Void* Obi.NativeMultilevelGrid`1/Cell`1::get_ContentsPointer()
// 0x000003F8 K Obi.NativeMultilevelGrid`1/Cell`1::get_Item(System.Int32)
// 0x000003F9 System.Void Obi.NativeMultilevelGrid`1/Cell`1::Add(K)
// 0x000003FA System.Boolean Obi.NativeMultilevelGrid`1/Cell`1::Remove(K)
// 0x000003FB System.Void Obi.NativeMultilevelGrid`1/Cell`1::Destroy()
// 0x000003FC System.Void Obi.ParticleGrid::.ctor()
extern void ParticleGrid__ctor_mA92DC689B3AF2090AB5ADE796C79C50B5CB6A90E (void);
// 0x000003FD System.Void Obi.ParticleGrid::Update(Obi.BurstSolverImpl,System.Single,Unity.Jobs.JobHandle)
extern void ParticleGrid_Update_m4A130AAE52E137C3271369A2D3E5151AEC2831E9 (void);
// 0x000003FE Unity.Jobs.JobHandle Obi.ParticleGrid::GenerateContacts(Obi.BurstSolverImpl,System.Single)
extern void ParticleGrid_GenerateContacts_m68B980DE9BB7EE1557C792974E48F09E790F3BF5 (void);
// 0x000003FF Unity.Jobs.JobHandle Obi.ParticleGrid::InterpolateDiffuseProperties(Obi.BurstSolverImpl,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32)
extern void ParticleGrid_InterpolateDiffuseProperties_m22B1DE675771F78C98B6972C3963A814405BA539 (void);
// 0x00000400 Unity.Jobs.JobHandle Obi.ParticleGrid::SpatialQuery(Obi.BurstSolverImpl,Unity.Collections.NativeArray`1<Obi.BurstQueryShape>,Unity.Collections.NativeArray`1<Obi.BurstAffineTransform>,Unity.Collections.NativeQueue`1<Obi.BurstQueryResult>)
extern void ParticleGrid_SpatialQuery_m8342F98C377CD26D3670BFFA545A612B5CF35F38 (void);
// 0x00000401 System.Void Obi.ParticleGrid::GetCells(Obi.ObiNativeAabbList)
extern void ParticleGrid_GetCells_m5BB4AF028FA1E1E1F94AA6782F6074BE41A9CE2E (void);
// 0x00000402 System.Void Obi.ParticleGrid::Dispose()
extern void ParticleGrid_Dispose_mB2A4C6486565763B3AD1D3A8863302F2D6D88645 (void);
// 0x00000403 System.Void Obi.ParticleGrid/CalculateCellCoords::Execute(System.Int32)
extern void CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955 (void);
// 0x00000404 System.Void Obi.ParticleGrid/UpdateGrid::Execute()
extern void UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47 (void);
// 0x00000405 System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::Execute(System.Int32)
extern void GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2 (void);
// 0x00000406 System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::IntraCellSearch(System.Int32,Obi.BurstSimplex&)
extern void GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB (void);
// 0x00000407 System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::InterCellSearch(System.Int32,System.Int32,Obi.BurstSimplex&)
extern void GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194 (void);
// 0x00000408 System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::IntraLevelSearch(System.Int32,Obi.BurstSimplex&)
extern void GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418 (void);
// 0x00000409 System.Int32 Obi.ParticleGrid/GenerateParticleParticleContactsJob::GetSimplexGroup(System.Int32,System.Int32,Obi.ObiUtils/ParticleFlags&,System.Int32&,System.Int32&,System.Boolean&)
extern void GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937 (void);
// 0x0000040A System.Void Obi.ParticleGrid/GenerateParticleParticleContactsJob::InteractionTest(System.Int32,System.Int32,Obi.BurstSimplex&)
extern void GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7 (void);
// 0x0000040B System.Void Obi.ParticleGrid/InterpolateDiffusePropertiesJob::Execute(System.Int32)
extern void InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A (void);
// 0x0000040C System.Int32 Obi.BurstContact::GetParticleCount()
extern void BurstContact_GetParticleCount_m07601B081D4B2D33AA274A224230DDF7FA41DA12 (void);
// 0x0000040D System.Int32 Obi.BurstContact::GetParticle(System.Int32)
extern void BurstContact_GetParticle_mA6D14AB9D6B0C1C0E2D489D9AEAC4F1DACF44976 (void);
// 0x0000040E System.String Obi.BurstContact::ToString()
extern void BurstContact_ToString_m082A762EDA6C99C0D046A08E1456DC09B503654B (void);
// 0x0000040F System.Int32 Obi.BurstContact::CompareTo(Obi.BurstContact)
extern void BurstContact_CompareTo_m4AAE9C314CA58F9ECD6DAC6E1FE8C4046C00A550 (void);
// 0x00000410 System.Single Obi.BurstContact::get_TotalNormalInvMass()
extern void BurstContact_get_TotalNormalInvMass_m8DB8BB9F92C654E952F6EFD3E30695C049FADAE4 (void);
// 0x00000411 System.Single Obi.BurstContact::get_TotalTangentInvMass()
extern void BurstContact_get_TotalTangentInvMass_m23A1474CCF4F8B7EFD804072C2B8C1081570C36A (void);
// 0x00000412 System.Single Obi.BurstContact::get_TotalBitangentInvMass()
extern void BurstContact_get_TotalBitangentInvMass_m6584B11DFF4F6F649C7F2048CA928C7321528FEB (void);
// 0x00000413 System.Void Obi.BurstContact::CalculateBasis(Unity.Mathematics.float4)
extern void BurstContact_CalculateBasis_m7756F0C85D337D0FCC1301EF91A9DDA3AC1F11FF (void);
// 0x00000414 System.Void Obi.BurstContact::CalculateContactMassesA(System.Single,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Unity.Mathematics.float4,System.Boolean)
extern void BurstContact_CalculateContactMassesA_m6442A92150D7BACA30A83170C952918D5941B9B7 (void);
// 0x00000415 System.Void Obi.BurstContact::CalculateContactMassesB(System.Single,Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Unity.Mathematics.float4,System.Boolean)
extern void BurstContact_CalculateContactMassesB_m525CB01618D4402EFE433CDD47AC61735B04A24D (void);
// 0x00000416 System.Void Obi.BurstContact::CalculateContactMassesB(Obi.BurstRigidbody&,Obi.BurstAffineTransform&)
extern void BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524 (void);
// 0x00000417 System.Single Obi.BurstContact::SolveAdhesion(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single,System.Single,System.Single)
extern void BurstContact_SolveAdhesion_m1CFCDDFF32CBB02EA4050AA3184E9AB1BF33B39B (void);
// 0x00000418 System.Single Obi.BurstContact::SolvePenetration(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single)
extern void BurstContact_SolvePenetration_mA388AC5B79F15B50ED2678A37DFA62CF8D7D12D3 (void);
// 0x00000419 Unity.Mathematics.float2 Obi.BurstContact::SolveFriction(Unity.Mathematics.float4,System.Single,System.Single,System.Single)
extern void BurstContact_SolveFriction_m50255C39E593D5D9ABAAE76C19E618577F5E24F4 (void);
// 0x0000041A System.Single Obi.BurstContact::SolveRollingFriction(Unity.Mathematics.float4,Unity.Mathematics.float4,System.Single,System.Single,System.Single,Unity.Mathematics.float4&)
extern void BurstContact_SolveRollingFriction_mE67B076235FF4D42A16E7624EDF3B4ED4E32463A (void);
// 0x0000041B System.Void Obi.BurstBoxQuery::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstBoxQuery_Evaluate_mB454C8F8076A2A41E291FAE3D4FD31B2DC810F62 (void);
// 0x0000041C System.Void Obi.BurstBoxQuery::Query(System.Int32,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstQueryResult>,System.Int32,System.Single)
extern void BurstBoxQuery_Query_m78C0813809BC44E8027D68204D187FD1DA7242DB (void);
// 0x0000041D System.Void Obi.BurstRay::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstRay_Evaluate_mE001829B68C458A87581958A3B9C6D7DBC85EC68 (void);
// 0x0000041E System.Void Obi.BurstRay::Query(System.Int32,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstQueryResult>,System.Int32,System.Single)
extern void BurstRay_Query_m0B4F7997C30EF099D2F72584B7EC3DB15EFBBEA9 (void);
// 0x0000041F System.Void Obi.BurstSphereQuery::Evaluate(Unity.Mathematics.float4,Unity.Mathematics.float4,Unity.Mathematics.quaternion,Obi.BurstLocalOptimization/SurfacePoint&)
extern void BurstSphereQuery_Evaluate_mA4AC455ADF4F22279A7667EFC7731B78EF1ECB9C (void);
// 0x00000420 System.Void Obi.BurstSphereQuery::Query(System.Int32,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<Unity.Mathematics.quaternion>,Unity.Collections.NativeArray`1<Unity.Mathematics.float4>,Unity.Collections.NativeArray`1<System.Int32>,System.Int32,System.Int32,System.Int32,Unity.Collections.NativeQueue`1/ParallelWriter<Obi.BurstQueryResult>,System.Int32,System.Single)
extern void BurstSphereQuery_Query_mC1E1903D249FD6440ACFB80FB3D2EBDB75F81875 (void);
// 0x00000421 System.Void Obi.SpatialQueryJob::Execute(System.Int32)
extern void SpatialQueryJob_Execute_mF6926D6C57FF21B92A42A0CF712D5DE2808238CF (void);
// 0x00000422 Obi.BurstAabb Obi.SpatialQueryJob::CalculateShapeAABB(Obi.BurstQueryShape&)
extern void SpatialQueryJob_CalculateShapeAABB_mD4BB4AAFB5B7C57689D4C530F27046BAEEBBF9D2 (void);
// 0x00000423 System.Void Obi.SpatialQueryJob::Query(Obi.BurstQueryShape&,Obi.BurstAffineTransform&,System.Int32,System.Int32,System.Int32,System.Int32)
extern void SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8 (void);
// 0x00000424 System.Void Obi.CalculateQueryDistances::Execute(System.Int32)
extern void CalculateQueryDistances_Execute_m8DAEA0919E33220185485804208ED37F68B12355 (void);
// 0x00000425 System.Void Obi.ApplyInertialForcesJob::Execute(System.Int32)
extern void ApplyInertialForcesJob_Execute_m6D16C9CD6820B5E2E5B69F722A4CEB47D3109B74 (void);
// 0x00000426 System.Void Obi.ParticleToBoundsJob::Execute(System.Int32)
extern void ParticleToBoundsJob_Execute_mF06E5D2A133701BF1984DAD08A9CAA0A53FF1F0C (void);
// 0x00000427 System.Void Obi.BoundsReductionJob::Execute(System.Int32)
extern void BoundsReductionJob_Execute_m560D9C4E0E5F1D2C870425DF2282FB1278ADB1C6 (void);
// 0x00000428 System.Void Obi.BuildSimplexAabbs::Execute(System.Int32)
extern void BuildSimplexAabbs_Execute_mEBBE064B8F9A20CC5507809D8B1A4D30C3A4EA7B (void);
// 0x00000429 Obi.ObiSolver Obi.BurstSolverImpl::get_abstraction()
extern void BurstSolverImpl_get_abstraction_m71B8AF402C0DA85EED28CC6FF0137DAC7FD3BC12 (void);
// 0x0000042A System.Int32 Obi.BurstSolverImpl::get_particleCount()
extern void BurstSolverImpl_get_particleCount_m7AE56747FF242C220B343A722C00A755CF171453 (void);
// 0x0000042B System.Int32 Obi.BurstSolverImpl::get_activeParticleCount()
extern void BurstSolverImpl_get_activeParticleCount_m72D861358815D53ED2376649B20BA6D0039CC857 (void);
// 0x0000042C Obi.BurstInertialFrame Obi.BurstSolverImpl::get_inertialFrame()
extern void BurstSolverImpl_get_inertialFrame_mD1E672E98D01E2AAE88930F5993E6493A0D26B1A (void);
// 0x0000042D Obi.BurstAffineTransform Obi.BurstSolverImpl::get_solverToWorld()
extern void BurstSolverImpl_get_solverToWorld_mDA781F49C5AAADD01FB3E43DF3AFDF923943D4C4 (void);
// 0x0000042E Obi.BurstAffineTransform Obi.BurstSolverImpl::get_worldToSolver()
extern void BurstSolverImpl_get_worldToSolver_m9C7E959103476BEE3E8508768EA6219B1CA116EB (void);
// 0x0000042F System.Void Obi.BurstSolverImpl::.ctor(Obi.ObiSolver)
extern void BurstSolverImpl__ctor_mBE8B17A47282B2B675D48FD685A329F8EBBC15F3 (void);
// 0x00000430 System.Void Obi.BurstSolverImpl::Destroy()
extern void BurstSolverImpl_Destroy_mD83C292B1BB19506F7AA063174E7A5BFCFAC34C8 (void);
// 0x00000431 System.Void Obi.BurstSolverImpl::ReleaseJobHandles()
extern void BurstSolverImpl_ReleaseJobHandles_m83684E4DD509A99D072B49C5696B1F71D25DA659 (void);
// 0x00000432 System.Void Obi.BurstSolverImpl::ScheduleBatchedJobsIfNeeded()
extern void BurstSolverImpl_ScheduleBatchedJobsIfNeeded_m572239F65137E8364015720187D55DD3A7112F05 (void);
// 0x00000433 System.Void Obi.BurstSolverImpl::GetOrCreateColliderWorld()
extern void BurstSolverImpl_GetOrCreateColliderWorld_m18279617CF056BA1CBAC23488A9D5A2256F6C792 (void);
// 0x00000434 System.Void Obi.BurstSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void BurstSolverImpl_InitializeFrame_mAD36C218F6792712B52B215262A12F6B588DDC7F (void);
// 0x00000435 System.Void Obi.BurstSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void BurstSolverImpl_UpdateFrame_m6357FD16BAD403BF8F93C863FC9E695D19F52B86 (void);
// 0x00000436 System.Void Obi.BurstSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void BurstSolverImpl_ApplyFrame_m0B2202471F03C50E65C88590302A3096D9264DC0 (void);
// 0x00000437 System.Int32 Obi.BurstSolverImpl::GetDeformableTriangleCount()
extern void BurstSolverImpl_GetDeformableTriangleCount_m6100E41F13329539ED29A671B488ED9B9F621F97 (void);
// 0x00000438 System.Void Obi.BurstSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void BurstSolverImpl_SetDeformableTriangles_mF51EF61CF41D64C93E2FA42BDB4F7EF8E31737CA (void);
// 0x00000439 System.Int32 Obi.BurstSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void BurstSolverImpl_RemoveDeformableTriangles_m2AD1A93F0CAB50206B6EAE64367918E161BD1AE6 (void);
// 0x0000043A System.Void Obi.BurstSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void BurstSolverImpl_SetSimplices_mF7242E46022196A5B3C894483C6047C91E5B42C2 (void);
// 0x0000043B System.Void Obi.BurstSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void BurstSolverImpl_SetActiveParticles_mEBA625A10B170D8D1E9BF455C1E60E4111FC1E0E (void);
// 0x0000043C System.Int32 Obi.BurstSolverImpl::ClampArrayAccess(System.Int32,System.Int32,System.Int32)
extern void BurstSolverImpl_ClampArrayAccess_m5AC5E1904640B1FECE5466DC705AE69905A2A692 (void);
// 0x0000043D Unity.Jobs.JobHandle Obi.BurstSolverImpl::RecalculateInertiaTensors(Unity.Jobs.JobHandle)
extern void BurstSolverImpl_RecalculateInertiaTensors_m3072558AE7AE81271459440FAA651C5EF7224DE0 (void);
// 0x0000043E System.Void Obi.BurstSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void BurstSolverImpl_GetBounds_m6B2DAA13B1AE81002985C2B41A586057AAF221BA (void);
// 0x0000043F System.Void Obi.BurstSolverImpl::ResetForces()
extern void BurstSolverImpl_ResetForces_m3DA78D31EAF1FF66296946CB9DE2397DDB649B29 (void);
// 0x00000440 System.Int32 Obi.BurstSolverImpl::GetConstraintCount(Oni/ConstraintType)
extern void BurstSolverImpl_GetConstraintCount_mF317E95D1AE7B8AD23F8C109C47FC8AAFD72B37D (void);
// 0x00000441 System.Void Obi.BurstSolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
extern void BurstSolverImpl_GetCollisionContacts_m819998819D1BEFE35B28D69A8A50AF2E2AE4C852 (void);
// 0x00000442 System.Void Obi.BurstSolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
extern void BurstSolverImpl_GetParticleCollisionContacts_mBC98CFEE63E49BBB7D1616101E8273A5F0149632 (void);
// 0x00000443 System.Void Obi.BurstSolverImpl::SetParameters(Oni/SolverParameters)
extern void BurstSolverImpl_SetParameters_mB64780E8C06460A178D23ECDC0EA518FCFF104E9 (void);
// 0x00000444 System.Void Obi.BurstSolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
extern void BurstSolverImpl_SetConstraintGroupParameters_m7CF6BDF3BB22607D2FF38C33116979BCACC1AC59 (void);
// 0x00000445 System.Void Obi.BurstSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void BurstSolverImpl_ParticleCountChanged_m6A87552C2D94DD0D11E529012D3AC9C28742B746 (void);
// 0x00000446 System.Void Obi.BurstSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void BurstSolverImpl_SetRigidbodyArrays_mC3F8D877C7555BD269C6F09B2A80419C273EEE8B (void);
// 0x00000447 Obi.IConstraintsBatchImpl Obi.BurstSolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
extern void BurstSolverImpl_CreateConstraintsBatch_m92521EF7923BAD7D60C29676B3D86A2AB6092482 (void);
// 0x00000448 System.Void Obi.BurstSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void BurstSolverImpl_DestroyConstraintsBatch_m046232143A4ABA1047A88EC2EC11B7970F30168B (void);
// 0x00000449 Obi.IObiJobHandle Obi.BurstSolverImpl::CollisionDetection(System.Single)
extern void BurstSolverImpl_CollisionDetection_m845F7FDE95A99E26B4078E764F5D3C794D6FD624 (void);
// 0x0000044A Unity.Jobs.JobHandle Obi.BurstSolverImpl::FindFluidParticles()
extern void BurstSolverImpl_FindFluidParticles_m0FA802C04A57D9118A9488DBB2D040A1D0DB78F5 (void);
// 0x0000044B Unity.Jobs.JobHandle Obi.BurstSolverImpl::UpdateSimplexBounds(Unity.Jobs.JobHandle,System.Single)
extern void BurstSolverImpl_UpdateSimplexBounds_m412233B3DB56438B9771BC882C2946FAA9AF3EC5 (void);
// 0x0000044C Unity.Jobs.JobHandle Obi.BurstSolverImpl::GenerateContacts(Unity.Jobs.JobHandle,System.Single)
extern void BurstSolverImpl_GenerateContacts_m62D3E6316F7D5C4B7C6244BE0CEEA6EC65DAA35E (void);
// 0x0000044D Obi.IObiJobHandle Obi.BurstSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void BurstSolverImpl_Substep_mD745D7E31C329A826DA018630CE5C69EE3C3F9C9 (void);
// 0x0000044E Unity.Jobs.JobHandle Obi.BurstSolverImpl::ApplyVelocityCorrections(Unity.Jobs.JobHandle,System.Single)
extern void BurstSolverImpl_ApplyVelocityCorrections_mF209AD82E3FB34F43C25C66A14AE533787D2D31D (void);
// 0x0000044F Unity.Jobs.JobHandle Obi.BurstSolverImpl::ApplyConstraints(Unity.Jobs.JobHandle,System.Single,System.Single,System.Int32)
extern void BurstSolverImpl_ApplyConstraints_m4CC5581B725747E6960B7B0C87A7A7E0B08A2B60 (void);
// 0x00000450 System.Void Obi.BurstSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void BurstSolverImpl_ApplyInterpolation_mDD491C3B1397FAF54EED5EAAF90A45E9E75234B0 (void);
// 0x00000451 System.Void Obi.BurstSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void BurstSolverImpl_InterpolateDiffuseProperties_m6BA4E4470E4D368EBBDE37428865798935659789 (void);
// 0x00000452 System.Void Obi.BurstSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void BurstSolverImpl_SpatialQuery_mF06E870F8951DF84B8B6B48711ECA386F6E1C66B (void);
// 0x00000453 System.Int32 Obi.BurstSolverImpl::GetParticleGridSize()
extern void BurstSolverImpl_GetParticleGridSize_m66C2C2B27C2EC8B3ECFB0A6C8BD6BD968F244132 (void);
// 0x00000454 System.Void Obi.BurstSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void BurstSolverImpl_GetParticleGrid_m6AFAB13B76899AF4A38587AF036F3ADE3CCB2610 (void);
// 0x00000455 System.Void Obi.DequeueIntoArrayJob`1::Execute()
// 0x00000456 System.Void Obi.FindFluidParticlesJob::Execute()
extern void FindFluidParticlesJob_Execute_m477B14F5ADFE05D34CC12D3351EC114D4C03408E (void);
// 0x00000457 System.Void Obi.InterpolationJob::Execute(System.Int32)
extern void InterpolationJob_Execute_mF037213986C8166D161BEBF6CA5836F2F42FFAF6 (void);
// 0x00000458 System.Void Obi.PredictPositionsJob::Execute(System.Int32)
extern void PredictPositionsJob_Execute_m42A393C96C2581FB6A24356ECC0E1807B43E5B8E (void);
// 0x00000459 System.Void Obi.UpdateInertiaTensorsJob::Execute(System.Int32)
extern void UpdateInertiaTensorsJob_Execute_mB727A2BFE75FE34ACF7D46675FA0D766EF6C4D7B (void);
// 0x0000045A System.Void Obi.UpdateNormalsJob::Execute()
extern void UpdateNormalsJob_Execute_m96B1F05E2523C94475A069C1D707F97A21B1EA81 (void);
// 0x0000045B System.Void Obi.UpdatePositionsJob::Execute(System.Int32)
extern void UpdatePositionsJob_Execute_m8DF43769C21749D1195FD8A483D9C8440A26614D (void);
// 0x0000045C System.Void Obi.UpdatePrincipalAxisJob::Execute(System.Int32)
extern void UpdatePrincipalAxisJob_Execute_mE9809E4137D315000D2A3E570BCC7573B050ABDC (void);
// 0x0000045D System.Void Obi.UpdateVelocitiesJob::Execute(System.Int32)
extern void UpdateVelocitiesJob_Execute_mBDA6FF10947E19ACC17D971540182892E043B621 (void);
// 0x0000045E System.Int32 Obi.IColliderWorldImpl::get_referenceCount()
// 0x0000045F System.Void Obi.IColliderWorldImpl::UpdateWorld(System.Single)
// 0x00000460 System.Void Obi.IColliderWorldImpl::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
// 0x00000461 System.Void Obi.IColliderWorldImpl::SetRigidbodies(Obi.ObiNativeRigidbodyList)
// 0x00000462 System.Void Obi.IColliderWorldImpl::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
// 0x00000463 System.Void Obi.IColliderWorldImpl::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
// 0x00000464 System.Void Obi.IColliderWorldImpl::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
// 0x00000465 System.Void Obi.IColliderWorldImpl::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
// 0x00000466 System.Void Obi.IColliderWorldImpl::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
// 0x00000467 System.Void Obi.IAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
// 0x00000468 System.Void Obi.IBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000469 System.Void Obi.IBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000046A System.Void Obi.IChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
// 0x0000046B Oni/ConstraintType Obi.IConstraintsBatchImpl::get_constraintType()
// 0x0000046C Obi.IConstraints Obi.IConstraintsBatchImpl::get_constraints()
// 0x0000046D System.Void Obi.IConstraintsBatchImpl::set_enabled(System.Boolean)
// 0x0000046E System.Boolean Obi.IConstraintsBatchImpl::get_enabled()
// 0x0000046F System.Void Obi.IConstraintsBatchImpl::Destroy()
// 0x00000470 System.Void Obi.IConstraintsBatchImpl::SetConstraintCount(System.Int32)
// 0x00000471 System.Int32 Obi.IConstraintsBatchImpl::GetConstraintCount()
// 0x00000472 Oni/ConstraintType Obi.IConstraints::get_constraintType()
// 0x00000473 Obi.ISolverImpl Obi.IConstraints::get_solver()
// 0x00000474 System.Int32 Obi.IConstraints::GetConstraintCount()
// 0x00000475 System.Void Obi.IDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000476 System.Void Obi.IPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x00000477 System.Void Obi.IShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
// 0x00000478 System.Void Obi.IShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
// 0x00000479 System.Void Obi.ISkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000047A System.Void Obi.IStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000047B System.Void Obi.IStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000047C System.Void Obi.ITetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x0000047D System.Void Obi.IVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x0000047E Obi.ISolverImpl Obi.IObiBackend::CreateSolver(Obi.ObiSolver,System.Int32)
// 0x0000047F System.Void Obi.IObiBackend::DestroySolver(Obi.ISolverImpl)
// 0x00000480 System.Void Obi.IObiJobHandle::Complete()
// 0x00000481 System.Void Obi.IObiJobHandle::Release()
// 0x00000482 System.Void Obi.JobHandlePool`1::.ctor(System.Int32)
// 0x00000483 T Obi.JobHandlePool`1::Borrow()
// 0x00000484 System.Void Obi.JobHandlePool`1::ReleaseAll()
// 0x00000485 System.Void Obi.ISolverImpl::Destroy()
// 0x00000486 System.Void Obi.ISolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
// 0x00000487 System.Void Obi.ISolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
// 0x00000488 System.Void Obi.ISolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
// 0x00000489 System.Void Obi.ISolverImpl::ParticleCountChanged(Obi.ObiSolver)
// 0x0000048A System.Void Obi.ISolverImpl::SetActiveParticles(System.Int32[],System.Int32)
// 0x0000048B System.Void Obi.ISolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
// 0x0000048C System.Void Obi.ISolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
// 0x0000048D Obi.IConstraintsBatchImpl Obi.ISolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
// 0x0000048E System.Void Obi.ISolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
// 0x0000048F System.Int32 Obi.ISolverImpl::GetConstraintCount(Oni/ConstraintType)
// 0x00000490 System.Void Obi.ISolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
// 0x00000491 System.Void Obi.ISolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
// 0x00000492 System.Void Obi.ISolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
// 0x00000493 Obi.IObiJobHandle Obi.ISolverImpl::CollisionDetection(System.Single)
// 0x00000494 Obi.IObiJobHandle Obi.ISolverImpl::Substep(System.Single,System.Single,System.Int32)
// 0x00000495 System.Void Obi.ISolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
// 0x00000496 System.Int32 Obi.ISolverImpl::GetDeformableTriangleCount()
// 0x00000497 System.Void Obi.ISolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
// 0x00000498 System.Int32 Obi.ISolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
// 0x00000499 System.Void Obi.ISolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
// 0x0000049A System.Void Obi.ISolverImpl::SetParameters(Oni/SolverParameters)
// 0x0000049B System.Void Obi.ISolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
// 0x0000049C System.Void Obi.ISolverImpl::ResetForces()
// 0x0000049D System.Int32 Obi.ISolverImpl::GetParticleGridSize()
// 0x0000049E System.Void Obi.ISolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
// 0x0000049F System.Void Obi.ISolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
// 0x000004A0 System.Void Obi.ISolverImpl::ReleaseJobHandles()
// 0x000004A1 Obi.ISolverImpl Obi.NullBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void NullBackend_CreateSolver_m149A74012C4CF9DEC9EC68F1C16EE99F3D6E88AF (void);
// 0x000004A2 System.Void Obi.NullBackend::DestroySolver(Obi.ISolverImpl)
extern void NullBackend_DestroySolver_m5E51947F4BEC3D57534FD796732535ED2B83D3F6 (void);
// 0x000004A3 System.Void Obi.NullBackend::.ctor()
extern void NullBackend__ctor_m7C4DE499807A5119E6914F1A7B6666E7BBFE44A9 (void);
// 0x000004A4 System.Void Obi.NullSolverImpl::Destroy()
extern void NullSolverImpl_Destroy_mCEF74F887CB2BF0E13C8176A052B1539F51BD6AB (void);
// 0x000004A5 System.Void Obi.NullSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void NullSolverImpl_InitializeFrame_m1A9943BBB1200D17394A300365060CC1C180C333 (void);
// 0x000004A6 System.Void Obi.NullSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void NullSolverImpl_UpdateFrame_mE9842C5421EB8BCEF09BEF66B7ECE4C6BBF68453 (void);
// 0x000004A7 System.Void Obi.NullSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void NullSolverImpl_ApplyFrame_m6E1688FD679EF4B17A6C885CD116D47885BA9A6D (void);
// 0x000004A8 System.Int32 Obi.NullSolverImpl::GetDeformableTriangleCount()
extern void NullSolverImpl_GetDeformableTriangleCount_m5B5658F9B5D43BC6CF7131229FE97D16E931E5AB (void);
// 0x000004A9 System.Void Obi.NullSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void NullSolverImpl_SetDeformableTriangles_m3555C196B17530814FC47EB0D65B0B19428AE73E (void);
// 0x000004AA System.Int32 Obi.NullSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void NullSolverImpl_RemoveDeformableTriangles_m327A23C82D523DB2799DC060C88746468C9CB012 (void);
// 0x000004AB System.Void Obi.NullSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void NullSolverImpl_SetSimplices_m31A164287A45DD5E59CD0C787A24C85DAB4C3D40 (void);
// 0x000004AC System.Void Obi.NullSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void NullSolverImpl_ParticleCountChanged_m8557EBD352BB202123699487E00B843F3CA0DC0B (void);
// 0x000004AD System.Void Obi.NullSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void NullSolverImpl_SetRigidbodyArrays_mEC64908F48D6E3EE3F03975F21201DAD33E36848 (void);
// 0x000004AE System.Void Obi.NullSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void NullSolverImpl_SetActiveParticles_m270CC166EEFCBB7494E52DFD1F7385B091AFB05B (void);
// 0x000004AF System.Void Obi.NullSolverImpl::ResetForces()
extern void NullSolverImpl_ResetForces_m02F4803EE6AB50D5563B60A00E5535B12F8E3B11 (void);
// 0x000004B0 System.Void Obi.NullSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void NullSolverImpl_GetBounds_m4D24F01134C67A078F93680525FB8DC5804A3DBB (void);
// 0x000004B1 System.Void Obi.NullSolverImpl::SetParameters(Oni/SolverParameters)
extern void NullSolverImpl_SetParameters_mDCC55383F8800DB49278298ABF8A176E6F5B3CAA (void);
// 0x000004B2 System.Int32 Obi.NullSolverImpl::GetConstraintCount(Oni/ConstraintType)
extern void NullSolverImpl_GetConstraintCount_mA5540FE96215F61ADEFE829931F4D927E70019E0 (void);
// 0x000004B3 System.Void Obi.NullSolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
extern void NullSolverImpl_GetCollisionContacts_mD2C3B675F93A95F33D2472E11AD4F48C503B2B54 (void);
// 0x000004B4 System.Void Obi.NullSolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
extern void NullSolverImpl_GetParticleCollisionContacts_m55885C928BC24A887BD63F783CD979F085E07381 (void);
// 0x000004B5 System.Void Obi.NullSolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
extern void NullSolverImpl_SetConstraintGroupParameters_m3A8213C3F85B7A2187B6030CC6EDC4DE5CB505BE (void);
// 0x000004B6 Obi.IConstraintsBatchImpl Obi.NullSolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
extern void NullSolverImpl_CreateConstraintsBatch_mB98D773275819E868529278DC289D6E084CC913D (void);
// 0x000004B7 System.Void Obi.NullSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void NullSolverImpl_DestroyConstraintsBatch_m42D46BA79047F3A0DBD2B305549E97FA654EF2B7 (void);
// 0x000004B8 Obi.IObiJobHandle Obi.NullSolverImpl::CollisionDetection(System.Single)
extern void NullSolverImpl_CollisionDetection_m3BE61763707C061EB9912C7E9DCD44C3F135C0FB (void);
// 0x000004B9 Obi.IObiJobHandle Obi.NullSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void NullSolverImpl_Substep_m46BCCC5D64A69DD461D4EF42A3D227A2F53A6A08 (void);
// 0x000004BA System.Void Obi.NullSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void NullSolverImpl_ApplyInterpolation_mB63837B2AE0542F306D50ABF7A05868F2A9A75BC (void);
// 0x000004BB System.Void Obi.NullSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void NullSolverImpl_InterpolateDiffuseProperties_m5D4052E47F4C0CE896A55E4932A7AB35F0001133 (void);
// 0x000004BC System.Int32 Obi.NullSolverImpl::GetParticleGridSize()
extern void NullSolverImpl_GetParticleGridSize_mD74EA2D59BC1B4472754D76EFF3B4FBC969B2A66 (void);
// 0x000004BD System.Void Obi.NullSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void NullSolverImpl_GetParticleGrid_mCBCC36302C00CB01775017E21F63754021E04BEC (void);
// 0x000004BE System.Void Obi.NullSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void NullSolverImpl_SpatialQuery_m252FFC078850F330BB8276D6996CD2303F90D6F1 (void);
// 0x000004BF System.Void Obi.NullSolverImpl::ReleaseJobHandles()
extern void NullSolverImpl_ReleaseJobHandles_m021F504138409DDC3785A8FEB37B25B3DA81BE24 (void);
// 0x000004C0 System.Void Obi.NullSolverImpl::.ctor()
extern void NullSolverImpl__ctor_m835D73EC52194B7861F10BCEC2C8F3D30E98D6EF (void);
// 0x000004C1 System.Void Obi.OniAerodynamicConstraintsBatchImpl::.ctor(Obi.OniAerodynamicConstraintsImpl)
extern void OniAerodynamicConstraintsBatchImpl__ctor_m08CF6D0353900E2F5161D01B30D6D4FD181EAF67 (void);
// 0x000004C2 System.Void Obi.OniAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
extern void OniAerodynamicConstraintsBatchImpl_SetAerodynamicConstraints_m78BC6505E02E608B6E7E64C31C785C0653DCCDD5 (void);
// 0x000004C3 System.Void Obi.OniAerodynamicConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniAerodynamicConstraintsImpl__ctor_m525B8236B34A2BEB8A129BC2DFCAD6433E10673D (void);
// 0x000004C4 Obi.IConstraintsBatchImpl Obi.OniAerodynamicConstraintsImpl::CreateConstraintsBatch()
extern void OniAerodynamicConstraintsImpl_CreateConstraintsBatch_m3C15118FE42A98BE00C420EFF5CC108133E8C9D1 (void);
// 0x000004C5 System.Void Obi.OniAerodynamicConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniAerodynamicConstraintsImpl_RemoveBatch_m7312BFD8301F75F44EF449DAB1CA208CDF256820 (void);
// 0x000004C6 System.Void Obi.OniBendConstraintsBatchImpl::.ctor(Obi.OniBendConstraintsImpl)
extern void OniBendConstraintsBatchImpl__ctor_mD888A9E984F69E1F75358F7C87F224E5BBFEF9C1 (void);
// 0x000004C7 System.Void Obi.OniBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniBendConstraintsBatchImpl_SetBendConstraints_mACD9168310968682EC7F9FAF4FAF01D439561251 (void);
// 0x000004C8 System.Void Obi.OniBendConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniBendConstraintsImpl__ctor_mDF7E828956758CC442DFCE530D6ACB3DA28C7520 (void);
// 0x000004C9 Obi.IConstraintsBatchImpl Obi.OniBendConstraintsImpl::CreateConstraintsBatch()
extern void OniBendConstraintsImpl_CreateConstraintsBatch_mE943BA310CF0508FA4B457AE9C4CEDE436C0256C (void);
// 0x000004CA System.Void Obi.OniBendConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniBendConstraintsImpl_RemoveBatch_mE65A138E34D866312EA8D6811377B0D2A5A8F759 (void);
// 0x000004CB System.Void Obi.OniBendTwistConstraintsBatchImpl::.ctor(Obi.OniBendTwistConstraintsImpl)
extern void OniBendTwistConstraintsBatchImpl__ctor_m8F131CD09F6F7F09BBDA55AF3E953E5EA2D1F52F (void);
// 0x000004CC System.Void Obi.OniBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniBendTwistConstraintsBatchImpl_SetBendTwistConstraints_m93208190B4C9F21B66804826F3F388365464C7E6 (void);
// 0x000004CD System.Void Obi.OniBendTwistConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniBendTwistConstraintsImpl__ctor_m2D810DA4120C0AB84298B4A542CA5B932E774DCB (void);
// 0x000004CE Obi.IConstraintsBatchImpl Obi.OniBendTwistConstraintsImpl::CreateConstraintsBatch()
extern void OniBendTwistConstraintsImpl_CreateConstraintsBatch_m5F1FC4850DB914A408732085F9AAEB1FE7D9DEF9 (void);
// 0x000004CF System.Void Obi.OniBendTwistConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniBendTwistConstraintsImpl_RemoveBatch_m571856C1E93A0B2507FD9309A2984CAD3143BB55 (void);
// 0x000004D0 System.Void Obi.OniChainConstraintsBatchImpl::.ctor(Obi.OniChainConstraintsImpl)
extern void OniChainConstraintsBatchImpl__ctor_m11A76775D8DA7D09B7DACBF1D1168D4FEF4F1694 (void);
// 0x000004D1 System.Void Obi.OniChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
extern void OniChainConstraintsBatchImpl_SetChainConstraints_mA704113F26DE40220400DE5B85B1475C2B3D0A82 (void);
// 0x000004D2 System.Void Obi.OniChainConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniChainConstraintsImpl__ctor_mAE3D234A2597D468A4621529AECFC4CD2D1BB328 (void);
// 0x000004D3 Obi.IConstraintsBatchImpl Obi.OniChainConstraintsImpl::CreateConstraintsBatch()
extern void OniChainConstraintsImpl_CreateConstraintsBatch_m84FCCD1FC3FF8E6DE2FF68D3B259A0AF6E595B9D (void);
// 0x000004D4 System.Void Obi.OniChainConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniChainConstraintsImpl_RemoveBatch_mD1FF35D0BD359EB12FC8A74DE8A560E487A9057B (void);
// 0x000004D5 System.Void Obi.OniDistanceConstraintsBatchImpl::.ctor(Obi.OniDistanceConstraintsImpl)
extern void OniDistanceConstraintsBatchImpl__ctor_m9005CD0907A9E6BACBBDA3777B6717B8611AFA18 (void);
// 0x000004D6 System.Void Obi.OniDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniDistanceConstraintsBatchImpl_SetDistanceConstraints_m2305BC0CB077A2F81D031356D983305E8344B34B (void);
// 0x000004D7 System.Void Obi.OniDistanceConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniDistanceConstraintsImpl__ctor_m46EF17FABD8A1BFB1EDE24EDE0F6EB61473907F8 (void);
// 0x000004D8 Obi.IConstraintsBatchImpl Obi.OniDistanceConstraintsImpl::CreateConstraintsBatch()
extern void OniDistanceConstraintsImpl_CreateConstraintsBatch_m68ACFFA63F7B3ACF12200ACCDD9796BC1E00453D (void);
// 0x000004D9 System.Void Obi.OniDistanceConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniDistanceConstraintsImpl_RemoveBatch_m4F114348DEFF33909A2AFCF15D10D98187F7912C (void);
// 0x000004DA System.IntPtr Obi.OniConstraintsBatchImpl::get_oniBatch()
extern void OniConstraintsBatchImpl_get_oniBatch_mD3068ADFBFA72D2C5AEC1E4493AF44A61BAAB3D6 (void);
// 0x000004DB Oni/ConstraintType Obi.OniConstraintsBatchImpl::get_constraintType()
extern void OniConstraintsBatchImpl_get_constraintType_mA4D89C531D6E50262AAB808ED481F01FCBC4554F (void);
// 0x000004DC Obi.IConstraints Obi.OniConstraintsBatchImpl::get_constraints()
extern void OniConstraintsBatchImpl_get_constraints_mC86F66AEBA1CCFE2EE1484C2CA0844257A7A0CCD (void);
// 0x000004DD System.Void Obi.OniConstraintsBatchImpl::set_enabled(System.Boolean)
extern void OniConstraintsBatchImpl_set_enabled_mE3FE79E17C8786437000CF9DF7ED7CC24C58DD1F (void);
// 0x000004DE System.Boolean Obi.OniConstraintsBatchImpl::get_enabled()
extern void OniConstraintsBatchImpl_get_enabled_m74C3931C9C466399F511D1258F6DF49FAF007D8F (void);
// 0x000004DF System.Void Obi.OniConstraintsBatchImpl::.ctor(Obi.IConstraints,Oni/ConstraintType)
extern void OniConstraintsBatchImpl__ctor_m7E856D436221DA306E7EFF8409F799DC8F926C38 (void);
// 0x000004E0 System.Void Obi.OniConstraintsBatchImpl::Destroy()
extern void OniConstraintsBatchImpl_Destroy_mB934F5194DA3A35DAC45581D183BE8C1BB056A1A (void);
// 0x000004E1 System.Void Obi.OniConstraintsBatchImpl::SetConstraintCount(System.Int32)
extern void OniConstraintsBatchImpl_SetConstraintCount_m9A9C7B081DBCAFE727F2682F742B32A0A5CD1FC7 (void);
// 0x000004E2 System.Int32 Obi.OniConstraintsBatchImpl::GetConstraintCount()
extern void OniConstraintsBatchImpl_GetConstraintCount_mBC3CD543643EBE713BEDBCEC5266DE52F0E6E53E (void);
// 0x000004E3 Obi.IConstraintsBatchImpl Obi.IOniConstraintsImpl::CreateConstraintsBatch()
// 0x000004E4 System.Void Obi.IOniConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x000004E5 Obi.ISolverImpl Obi.OniConstraintsImpl::get_solver()
extern void OniConstraintsImpl_get_solver_m44585FB563C805AF280120F7FDE3B5737F4C0411 (void);
// 0x000004E6 Oni/ConstraintType Obi.OniConstraintsImpl::get_constraintType()
extern void OniConstraintsImpl_get_constraintType_mF0F59F28976DF29B32C94C4156BD7752105087CC (void);
// 0x000004E7 System.Void Obi.OniConstraintsImpl::.ctor(Obi.OniSolverImpl,Oni/ConstraintType)
extern void OniConstraintsImpl__ctor_m9A0E8C6A195C96B8CB7EF27D99B7A0988672BF61 (void);
// 0x000004E8 Obi.IConstraintsBatchImpl Obi.OniConstraintsImpl::CreateConstraintsBatch()
// 0x000004E9 System.Void Obi.OniConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
// 0x000004EA System.Int32 Obi.OniConstraintsImpl::GetConstraintCount()
extern void OniConstraintsImpl_GetConstraintCount_mDE65EE94DCFAB8905A9AEF5543E7119928F2900A (void);
// 0x000004EB System.Void Obi.OniPinConstraintsBatchImpl::.ctor(Obi.OniPinConstraintsImpl)
extern void OniPinConstraintsBatchImpl__ctor_mA95003D5E91633FA72AA1F912FFCC947CBFC25CF (void);
// 0x000004EC System.Void Obi.OniPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniPinConstraintsBatchImpl_SetPinConstraints_mB28FD52B804E30D4B06AADE5A8764C9F3F4DC223 (void);
// 0x000004ED System.Void Obi.OniPinConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniPinConstraintsImpl__ctor_m2F0E0AF0C30CE5AA9BBA5B0F2C170582A8442ABE (void);
// 0x000004EE Obi.IConstraintsBatchImpl Obi.OniPinConstraintsImpl::CreateConstraintsBatch()
extern void OniPinConstraintsImpl_CreateConstraintsBatch_mF0305992D09B680091BD1EEF5519A801F19F69D0 (void);
// 0x000004EF System.Void Obi.OniPinConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniPinConstraintsImpl_RemoveBatch_m1EA7BE417BEF16951FAD0CA1106DB5BC257F38E1 (void);
// 0x000004F0 System.Void Obi.OniShapeMatchingConstraintsBatchImpl::.ctor(Obi.OniShapeMatchingConstraintsImpl)
extern void OniShapeMatchingConstraintsBatchImpl__ctor_mA35A5ADBE7591DFAFCD6775D89CD7C7BBEB7E25D (void);
// 0x000004F1 System.Void Obi.OniShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
extern void OniShapeMatchingConstraintsBatchImpl_SetShapeMatchingConstraints_mDDBCB5CFAB68DDF0E19581524119A7981D214588 (void);
// 0x000004F2 System.Void Obi.OniShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
extern void OniShapeMatchingConstraintsBatchImpl_CalculateRestShapeMatching_m6E5073835BB3D1CED559A45D987CC3FCD9D4C21D (void);
// 0x000004F3 System.Void Obi.OniShapeMatchingConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniShapeMatchingConstraintsImpl__ctor_m945AA881838FE9644266E98CA531247FE004C1D7 (void);
// 0x000004F4 Obi.IConstraintsBatchImpl Obi.OniShapeMatchingConstraintsImpl::CreateConstraintsBatch()
extern void OniShapeMatchingConstraintsImpl_CreateConstraintsBatch_mB055C2DDEB21A68DC4E3A56E2A989231D1109BBD (void);
// 0x000004F5 System.Void Obi.OniShapeMatchingConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniShapeMatchingConstraintsImpl_RemoveBatch_m0C9FC7FFB2680541A78037980C49C907F3598756 (void);
// 0x000004F6 System.Void Obi.OniSkinConstraintsBatchImpl::.ctor(Obi.OniSkinConstraintsImpl)
extern void OniSkinConstraintsBatchImpl__ctor_m5DDA5D3F347C6EC9FF501C52A4374AD88A10AABD (void);
// 0x000004F7 System.Void Obi.OniSkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniSkinConstraintsBatchImpl_SetSkinConstraints_m8065C2605A9D036F95C2F1B234883144F9C84CD3 (void);
// 0x000004F8 System.Void Obi.OniSkinConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniSkinConstraintsImpl__ctor_m9A29E8FD8F9F99978DFF2EB7149829624A7939A5 (void);
// 0x000004F9 Obi.IConstraintsBatchImpl Obi.OniSkinConstraintsImpl::CreateConstraintsBatch()
extern void OniSkinConstraintsImpl_CreateConstraintsBatch_m9FB2EAD640C26270C35A54351F354F8AA1D7E043 (void);
// 0x000004FA System.Void Obi.OniSkinConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniSkinConstraintsImpl_RemoveBatch_mC0102FC9F40941F1DDEBCDA28632CBDD43660523 (void);
// 0x000004FB System.Void Obi.OniStitchConstraintsBatchImpl::.ctor(Obi.OniStitchConstraintsImpl)
extern void OniStitchConstraintsBatchImpl__ctor_mC565125A40106134B8A526F21E536F819AACDB34 (void);
// 0x000004FC System.Void Obi.OniStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniStitchConstraintsBatchImpl_SetStitchConstraints_m850BB5F12FBB10D113073FFD360406906FE84F55 (void);
// 0x000004FD System.Void Obi.OniStitchConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniStitchConstraintsImpl__ctor_m5A0F9B6CD43ECC7D7AE65AD107B559178559308C (void);
// 0x000004FE Obi.IConstraintsBatchImpl Obi.OniStitchConstraintsImpl::CreateConstraintsBatch()
extern void OniStitchConstraintsImpl_CreateConstraintsBatch_m94119E21DE281CD292B556C5077176B4D9C9284F (void);
// 0x000004FF System.Void Obi.OniStitchConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniStitchConstraintsImpl_RemoveBatch_m7B1ED49DA5FB278734D0C9F8597E820B3E284FAC (void);
// 0x00000500 System.Void Obi.OniStretchShearConstraintsBatchImpl::.ctor(Obi.OniStretchShearConstraintsImpl)
extern void OniStretchShearConstraintsBatchImpl__ctor_m06A2F02D467608368981ECD3800852E4B1229908 (void);
// 0x00000501 System.Void Obi.OniStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
extern void OniStretchShearConstraintsBatchImpl_SetStretchShearConstraints_mACC9CB76D4FDD53A4FDD96FA2031EBD5F893B6AA (void);
// 0x00000502 System.Void Obi.OniStretchShearConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniStretchShearConstraintsImpl__ctor_m743767DB7C25C334E9584FACAF87392343A39ACC (void);
// 0x00000503 Obi.IConstraintsBatchImpl Obi.OniStretchShearConstraintsImpl::CreateConstraintsBatch()
extern void OniStretchShearConstraintsImpl_CreateConstraintsBatch_m78706F1FEE7FEAA5EF471FAB390E0E0D71861127 (void);
// 0x00000504 System.Void Obi.OniStretchShearConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniStretchShearConstraintsImpl_RemoveBatch_mF8F67B57D02D49DEC0DC0197146B243A44090457 (void);
// 0x00000505 System.Void Obi.OniTetherConstraintsBatchImpl::.ctor(Obi.OniTetherConstraintsImpl)
extern void OniTetherConstraintsBatchImpl__ctor_mA18D6CEBB82288A3B578C8C4C52F4E0119DAB0B4 (void);
// 0x00000506 System.Void Obi.OniTetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
extern void OniTetherConstraintsBatchImpl_SetTetherConstraints_mCA9A516027A94559796B9C29CD716F10CB4BEFD7 (void);
// 0x00000507 System.Void Obi.OniTetherConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniTetherConstraintsImpl__ctor_m79D07270D5F4E7E62395AA8BE2B0F5B2C5B9AD25 (void);
// 0x00000508 Obi.IConstraintsBatchImpl Obi.OniTetherConstraintsImpl::CreateConstraintsBatch()
extern void OniTetherConstraintsImpl_CreateConstraintsBatch_mD49AD0DA234A2CBAF481401273DC88CF16D87AD7 (void);
// 0x00000509 System.Void Obi.OniTetherConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniTetherConstraintsImpl_RemoveBatch_m4B4909C5425342565704DFD35EDD47C5A1E2FB20 (void);
// 0x0000050A System.Void Obi.OniVolumeConstraintsBatchImpl::.ctor(Obi.OniVolumeConstraintsImpl)
extern void OniVolumeConstraintsBatchImpl__ctor_m17E79E7DCFE92922A8861BE569C3FF460D175CAE (void);
// 0x0000050B System.Void Obi.OniVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
extern void OniVolumeConstraintsBatchImpl_SetVolumeConstraints_m9C580C7D330A95E2032129920D22CC13D70E8AFC (void);
// 0x0000050C System.Void Obi.OniVolumeConstraintsImpl::.ctor(Obi.OniSolverImpl)
extern void OniVolumeConstraintsImpl__ctor_m5147F9E64F13F4D205515CE6F1906887F787BA74 (void);
// 0x0000050D Obi.IConstraintsBatchImpl Obi.OniVolumeConstraintsImpl::CreateConstraintsBatch()
extern void OniVolumeConstraintsImpl_CreateConstraintsBatch_mF7765774D05626A213C729351AE71CF664A0A8B2 (void);
// 0x0000050E System.Void Obi.OniVolumeConstraintsImpl::RemoveBatch(Obi.IConstraintsBatchImpl)
extern void OniVolumeConstraintsImpl_RemoveBatch_m726F19C3F55138E1BB32C8D3C97804E44013547C (void);
// 0x0000050F Obi.ISolverImpl Obi.OniBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void OniBackend_CreateSolver_m5E3C44A4EA9BCDD470E7E10E684F0AF897C8AF5F (void);
// 0x00000510 System.Void Obi.OniBackend::DestroySolver(Obi.ISolverImpl)
extern void OniBackend_DestroySolver_mC6AFB023EC9F9532FF2B840DDF955E89C71A0D01 (void);
// 0x00000511 System.Void Obi.OniBackend::GetOrCreateColliderWorld()
extern void OniBackend_GetOrCreateColliderWorld_m92C5F279126E62C80B84C7ACAC6FEB209751EF1D (void);
// 0x00000512 System.Void Obi.OniBackend::.ctor()
extern void OniBackend__ctor_m45B8DE94A53B0740C6704A51A4BC4FDEB7C7D4F7 (void);
// 0x00000513 System.Int32 Obi.OniColliderWorld::get_referenceCount()
extern void OniColliderWorld_get_referenceCount_m2E1D878672ADF99B997AA9EF3050BD41B27DC9FD (void);
// 0x00000514 System.Void Obi.OniColliderWorld::Awake()
extern void OniColliderWorld_Awake_mBB4F8B9A43CB88C337CD9DB344E0B7169D47E923 (void);
// 0x00000515 System.Void Obi.OniColliderWorld::OnDestroy()
extern void OniColliderWorld_OnDestroy_mAF83AA5D931A38B0E55E2226B94527B592FD9D83 (void);
// 0x00000516 System.Void Obi.OniColliderWorld::IncreaseReferenceCount()
extern void OniColliderWorld_IncreaseReferenceCount_m78FCC3BFF315F84942D3994F5092A54A8D7F678E (void);
// 0x00000517 System.Void Obi.OniColliderWorld::DecreaseReferenceCount()
extern void OniColliderWorld_DecreaseReferenceCount_m35AE9297895C720AC03E0D0F320DDAD1366BF64C (void);
// 0x00000518 System.Void Obi.OniColliderWorld::UpdateWorld(System.Single)
extern void OniColliderWorld_UpdateWorld_m07BB5250B08FE6F9B66FBDC04C2B54A98C4B4251 (void);
// 0x00000519 System.Void Obi.OniColliderWorld::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
extern void OniColliderWorld_SetColliders_mBC49AA3ED5E83E70CE733DCEC0683FF672872263 (void);
// 0x0000051A System.Void Obi.OniColliderWorld::SetRigidbodies(Obi.ObiNativeRigidbodyList)
extern void OniColliderWorld_SetRigidbodies_m28F9E1B58302DB5A756C243EF52E788E708DA85F (void);
// 0x0000051B System.Void Obi.OniColliderWorld::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
extern void OniColliderWorld_SetCollisionMaterials_mEB26C8AC30F1B5CE27D8F19A77ABB7B252115E0D (void);
// 0x0000051C System.Void Obi.OniColliderWorld::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
extern void OniColliderWorld_SetTriangleMeshData_m4965F3D38925BFCACB96B6B3479DD4F232ED7104 (void);
// 0x0000051D System.Void Obi.OniColliderWorld::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
extern void OniColliderWorld_SetEdgeMeshData_mD9F54F0603E44D518BC9243FF1979EC24E25FD44 (void);
// 0x0000051E System.Void Obi.OniColliderWorld::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
extern void OniColliderWorld_SetDistanceFieldData_m2C1ADF6E94752BC41CE632A804CBAE1AC19A9EDF (void);
// 0x0000051F System.Void Obi.OniColliderWorld::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
extern void OniColliderWorld_SetHeightFieldData_m4C593F78531A7AE191FA9F8D4A8BBFC23E84F68A (void);
// 0x00000520 System.Void Obi.OniColliderWorld::.ctor()
extern void OniColliderWorld__ctor_m4DE663B94B5C376427CEB42288BA88428E549A52 (void);
// 0x00000521 Obi.OniJobHandle Obi.OniJobHandle::SetPointer(System.IntPtr)
extern void OniJobHandle_SetPointer_mA85FEFE91D585455A4BA43722BD3AB87A838603C (void);
// 0x00000522 System.Void Obi.OniJobHandle::Complete()
extern void OniJobHandle_Complete_mB9E3D94D50CF207A0F961C9F4ADEB30B2FA8A6DE (void);
// 0x00000523 System.Void Obi.OniJobHandle::Release()
extern void OniJobHandle_Release_m7B54D7F8A274FFC942C4860D5E28DFF47284129B (void);
// 0x00000524 System.Void Obi.OniJobHandle::.ctor()
extern void OniJobHandle__ctor_m514FBCF3D1AAF7DA9034107984D0DED6C5510C73 (void);
// 0x00000525 System.IntPtr Obi.OniSolverImpl::get_oniSolver()
extern void OniSolverImpl_get_oniSolver_mA021288BE5461FA7F14399158965ECDD9ECA1706 (void);
// 0x00000526 System.Void Obi.OniSolverImpl::.ctor(System.IntPtr)
extern void OniSolverImpl__ctor_m2FE0413D6AF7939DBE480B789D43360A39DDFAD4 (void);
// 0x00000527 System.Void Obi.OniSolverImpl::Destroy()
extern void OniSolverImpl_Destroy_m317E88AD3A919043461CE59F399DF3670A904961 (void);
// 0x00000528 System.Void Obi.OniSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void OniSolverImpl_InitializeFrame_mC8E921CBD6A70A2F9F46310EB5556586B6749DC7 (void);
// 0x00000529 System.Void Obi.OniSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void OniSolverImpl_UpdateFrame_m52DA894215FDD134640AD29208E780EEBD65180A (void);
// 0x0000052A System.Void Obi.OniSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void OniSolverImpl_ApplyFrame_m67E28FA4B4FBE5C072E359F3574F562C59820DB4 (void);
// 0x0000052B System.Int32 Obi.OniSolverImpl::GetDeformableTriangleCount()
extern void OniSolverImpl_GetDeformableTriangleCount_m230CCC63F4275668FF4EF1B34F56859D5C70E68F (void);
// 0x0000052C System.Void Obi.OniSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void OniSolverImpl_SetDeformableTriangles_mE67CB6225F084DE826F48AA4D27E43FA43BE9827 (void);
// 0x0000052D System.Int32 Obi.OniSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void OniSolverImpl_RemoveDeformableTriangles_m3B80FB7C6FA7DB0890B5BC37670151643F54C650 (void);
// 0x0000052E System.Void Obi.OniSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void OniSolverImpl_SetSimplices_mD571A7AFB950F771AE96A6A30743D2E7CE5BAE5B (void);
// 0x0000052F System.Void Obi.OniSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void OniSolverImpl_ParticleCountChanged_mC72028DD674A3E54C4E08E758A3CE3D26E3E7577 (void);
// 0x00000530 System.Void Obi.OniSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void OniSolverImpl_SetRigidbodyArrays_mFA7655D4B61E7E1C65C4E42C8B77EA55DFC70AEE (void);
// 0x00000531 System.Void Obi.OniSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void OniSolverImpl_SetActiveParticles_m850250C4A4DE24AB05AB59AADCD43D6F232077E8 (void);
// 0x00000532 System.Void Obi.OniSolverImpl::ResetForces()
extern void OniSolverImpl_ResetForces_m5E8F36B6FCA63BD1E720DBFD5B8764863E3CE626 (void);
// 0x00000533 System.Void Obi.OniSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void OniSolverImpl_GetBounds_m478CF8A2FAB372FE347E9899D262869163ACCB57 (void);
// 0x00000534 System.Void Obi.OniSolverImpl::SetParameters(Oni/SolverParameters)
extern void OniSolverImpl_SetParameters_mF506A029DB22FF674610C76D892D387BAD7178A5 (void);
// 0x00000535 System.Int32 Obi.OniSolverImpl::GetConstraintCount(Oni/ConstraintType)
extern void OniSolverImpl_GetConstraintCount_m7C95C3E195679C7076DEBAB096213874408B03EF (void);
// 0x00000536 System.Void Obi.OniSolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
extern void OniSolverImpl_GetCollisionContacts_m3179A5E075C13134C753E47D58F150C004D15906 (void);
// 0x00000537 System.Void Obi.OniSolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
extern void OniSolverImpl_GetParticleCollisionContacts_m4B99957B57A3E6BAF05C9E3D64DF28B5E0748259 (void);
// 0x00000538 System.Void Obi.OniSolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
extern void OniSolverImpl_SetConstraintGroupParameters_mB58C6320D75A44B682B4AEA82214F8ACA62E53DA (void);
// 0x00000539 Obi.IConstraintsBatchImpl Obi.OniSolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
extern void OniSolverImpl_CreateConstraintsBatch_m388EEDF658EF8F63CD78DC8DC33F0DC642688911 (void);
// 0x0000053A System.Void Obi.OniSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void OniSolverImpl_DestroyConstraintsBatch_m7769244161551FB17B31600D453AC2F68F156860 (void);
// 0x0000053B Obi.IObiJobHandle Obi.OniSolverImpl::CollisionDetection(System.Single)
extern void OniSolverImpl_CollisionDetection_m56F7F7DC6C224E1DEE5CF8A1437ED6FE02BAB5E2 (void);
// 0x0000053C Obi.IObiJobHandle Obi.OniSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void OniSolverImpl_Substep_mCA3F4A59045D0A506692A48C78ACE6BB4E884D93 (void);
// 0x0000053D System.Void Obi.OniSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void OniSolverImpl_ApplyInterpolation_mAD4820F8A797FF35107C1C920D5B50D4415C67AD (void);
// 0x0000053E System.Void Obi.OniSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void OniSolverImpl_InterpolateDiffuseProperties_m0678F52DF465E6E9B34DECE91C6C3D48F96E8821 (void);
// 0x0000053F System.Int32 Obi.OniSolverImpl::GetParticleGridSize()
extern void OniSolverImpl_GetParticleGridSize_mC858C1BFF95D86B1B0841E40378F882A014E8E78 (void);
// 0x00000540 System.Void Obi.OniSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void OniSolverImpl_GetParticleGrid_m3A454F1EA5EDCF6A590F99A29EEAC84F0D65B790 (void);
// 0x00000541 System.Void Obi.OniSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void OniSolverImpl_SpatialQuery_m6BD4BCC1AACE17D8D66365F9FE7F16012A1E8F7F (void);
// 0x00000542 System.Void Obi.OniSolverImpl::ReleaseJobHandles()
extern void OniSolverImpl_ReleaseJobHandles_mC05A2D743B7E73A54FBBD5BBB93CA5C8FFC3548F (void);
// 0x00000543 System.Single Obi.IStructuralConstraintBatch::GetRestLength(System.Int32)
// 0x00000544 System.Void Obi.IStructuralConstraintBatch::SetRestLength(System.Int32,System.Single)
// 0x00000545 Obi.ParticlePair Obi.IStructuralConstraintBatch::GetParticleIndices(System.Int32)
// 0x00000546 Oni/ConstraintType Obi.ObiAerodynamicConstraintsBatch::get_constraintType()
extern void ObiAerodynamicConstraintsBatch_get_constraintType_mBC6B3C472BCF899A37C3C9F15B61F0B22DB160F4 (void);
// 0x00000547 Obi.IConstraintsBatchImpl Obi.ObiAerodynamicConstraintsBatch::get_implementation()
extern void ObiAerodynamicConstraintsBatch_get_implementation_mD6185053ABF60610B561CE15B51770F6E2556E08 (void);
// 0x00000548 System.Void Obi.ObiAerodynamicConstraintsBatch::.ctor(Obi.ObiAerodynamicConstraintsData)
extern void ObiAerodynamicConstraintsBatch__ctor_m9DE858EA294B273AEED18201A289AEC50F28F02E (void);
// 0x00000549 System.Void Obi.ObiAerodynamicConstraintsBatch::AddConstraint(System.Int32,System.Single,System.Single,System.Single)
extern void ObiAerodynamicConstraintsBatch_AddConstraint_mB9A927126FB700DC1E84DDA91D9132310EFEE124 (void);
// 0x0000054A System.Void Obi.ObiAerodynamicConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m430311A6B436224B845F2F196525AD41A67EB605 (void);
// 0x0000054B System.Void Obi.ObiAerodynamicConstraintsBatch::Clear()
extern void ObiAerodynamicConstraintsBatch_Clear_m5ACDB0CA34591C9F454153521B67AC2478467EA2 (void);
// 0x0000054C System.Void Obi.ObiAerodynamicConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiAerodynamicConstraintsBatch_SwapConstraints_m3F24E8A02E9F7E06130C7E27A8438623CBC36B99 (void);
// 0x0000054D System.Void Obi.ObiAerodynamicConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiAerodynamicConstraintsBatch_Merge_m4CEBEA6CE4426002B0326CFCAE2C25E3F4954CD0 (void);
// 0x0000054E System.Void Obi.ObiAerodynamicConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_AddToSolver_mF2BA5925F8D160264B0EC1995C4EF9856E4E48FB (void);
// 0x0000054F System.Void Obi.ObiAerodynamicConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_RemoveFromSolver_m3288141E2907B24EF4337F9C513C6C69D68D8684 (void);
// 0x00000550 Oni/ConstraintType Obi.ObiBendConstraintsBatch::get_constraintType()
extern void ObiBendConstraintsBatch_get_constraintType_mA302627388787213E045E481476B7C9EB7F43D92 (void);
// 0x00000551 Obi.IConstraintsBatchImpl Obi.ObiBendConstraintsBatch::get_implementation()
extern void ObiBendConstraintsBatch_get_implementation_mE6CC10221A772592842A23E0EE91871FAA8ED5FA (void);
// 0x00000552 System.Void Obi.ObiBendConstraintsBatch::.ctor(Obi.ObiBendConstraintsData)
extern void ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9 (void);
// 0x00000553 System.Void Obi.ObiBendConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendConstraintsBatch_Merge_mC7EBF91A7220B4C03792E048ABB718337C6F03AA (void);
// 0x00000554 System.Void Obi.ObiBendConstraintsBatch::AddConstraint(UnityEngine.Vector3Int,System.Single)
extern void ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C (void);
// 0x00000555 System.Void Obi.ObiBendConstraintsBatch::Clear()
extern void ObiBendConstraintsBatch_Clear_mA0DC32AE2AE98E09AF547DE49FB25D8CDFF0DB97 (void);
// 0x00000556 System.Void Obi.ObiBendConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendConstraintsBatch_GetParticlesInvolved_mA9C31748143F96BC5E341BE711B68E9F78204C4A (void);
// 0x00000557 System.Void Obi.ObiBendConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendConstraintsBatch_SwapConstraints_mD42CD848B36CEB0540B9F211A1C7592348949487 (void);
// 0x00000558 System.Void Obi.ObiBendConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_AddToSolver_m65C74A506765E2DFFD6575EE0E9D8AF1F723EBC1 (void);
// 0x00000559 System.Void Obi.ObiBendConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_RemoveFromSolver_m771E799A2D261FFC3AB2686EB70676ABEBCE21F7 (void);
// 0x0000055A Oni/ConstraintType Obi.ObiBendTwistConstraintsBatch::get_constraintType()
extern void ObiBendTwistConstraintsBatch_get_constraintType_mAC26F20B9CE45F7ACE966652B0ACAB8337A9703D (void);
// 0x0000055B Obi.IConstraintsBatchImpl Obi.ObiBendTwistConstraintsBatch::get_implementation()
extern void ObiBendTwistConstraintsBatch_get_implementation_m293D659D51624728D74EB2EC1BEF2EBA93A301D2 (void);
// 0x0000055C System.Void Obi.ObiBendTwistConstraintsBatch::.ctor(Obi.ObiBendTwistConstraintsData)
extern void ObiBendTwistConstraintsBatch__ctor_mA84FE1A64BC385B93B82ACDB515147D1BFD9D8E9 (void);
// 0x0000055D System.Void Obi.ObiBendTwistConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,UnityEngine.Quaternion)
extern void ObiBendTwistConstraintsBatch_AddConstraint_m04DEFCAA48B91E8849ECC4FB1F151C17740B3E78 (void);
// 0x0000055E System.Void Obi.ObiBendTwistConstraintsBatch::Clear()
extern void ObiBendTwistConstraintsBatch_Clear_mEA7D8BB6BCCA8C72D2CB42CF1D9C197F3C3FB905 (void);
// 0x0000055F System.Void Obi.ObiBendTwistConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendTwistConstraintsBatch_GetParticlesInvolved_m7F039D1FBFA40A6E0EFFB3F66FAA2A5D4D621393 (void);
// 0x00000560 System.Void Obi.ObiBendTwistConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendTwistConstraintsBatch_SwapConstraints_m19754F194196E8509DE60D4C6A4E01CDE61951CC (void);
// 0x00000561 System.Void Obi.ObiBendTwistConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendTwistConstraintsBatch_Merge_m65D2EB86911176AE0DBCFC755950330B0FF4D9C4 (void);
// 0x00000562 System.Void Obi.ObiBendTwistConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_AddToSolver_m00517D42056B5BD7E11AF70F1C617FD33E313B35 (void);
// 0x00000563 System.Void Obi.ObiBendTwistConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_RemoveFromSolver_m89781A48762FBA02AFF8BE9423981220F26A147B (void);
// 0x00000564 Oni/ConstraintType Obi.ObiChainConstraintsBatch::get_constraintType()
extern void ObiChainConstraintsBatch_get_constraintType_mBAD7C6C6A0862F3A03015BCA2BA2B969706672C7 (void);
// 0x00000565 Obi.IConstraintsBatchImpl Obi.ObiChainConstraintsBatch::get_implementation()
extern void ObiChainConstraintsBatch_get_implementation_m2883B97D7E6175B2C1FE458CD3E667789C1FEC92 (void);
// 0x00000566 System.Void Obi.ObiChainConstraintsBatch::.ctor(Obi.ObiChainConstraintsData)
extern void ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC (void);
// 0x00000567 System.Void Obi.ObiChainConstraintsBatch::AddConstraint(System.Int32[],System.Single,System.Single,System.Single)
extern void ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB (void);
// 0x00000568 System.Void Obi.ObiChainConstraintsBatch::Clear()
extern void ObiChainConstraintsBatch_Clear_mC5BA1FD8A165468C43063493EF8BFB93A3D2CE21 (void);
// 0x00000569 System.Void Obi.ObiChainConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiChainConstraintsBatch_GetParticlesInvolved_m1B0852EE90B8C0089C7793F4915B9D5C5E739187 (void);
// 0x0000056A System.Void Obi.ObiChainConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiChainConstraintsBatch_SwapConstraints_m56826B5809EDF1D888AC022EC80813AB5E8A8029 (void);
// 0x0000056B System.Void Obi.ObiChainConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiChainConstraintsBatch_Merge_m7C8E59CC49A14B6A3F20B923CB9983A18709369E (void);
// 0x0000056C System.Void Obi.ObiChainConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_AddToSolver_m8E12FD5F4ACAA95E5FFA4E57A15C6D01796354AA (void);
// 0x0000056D System.Void Obi.ObiChainConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_RemoveFromSolver_m97FD3AAF49F7296DC655C1BE2B0C866D40330AAB (void);
// 0x0000056E System.Int32 Obi.IObiConstraintsBatch::get_constraintCount()
// 0x0000056F System.Int32 Obi.IObiConstraintsBatch::get_activeConstraintCount()
// 0x00000570 System.Void Obi.IObiConstraintsBatch::set_activeConstraintCount(System.Int32)
// 0x00000571 System.Int32 Obi.IObiConstraintsBatch::get_initialActiveConstraintCount()
// 0x00000572 System.Void Obi.IObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
// 0x00000573 Oni/ConstraintType Obi.IObiConstraintsBatch::get_constraintType()
// 0x00000574 Obi.IConstraintsBatchImpl Obi.IObiConstraintsBatch::get_implementation()
// 0x00000575 System.Void Obi.IObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x00000576 System.Void Obi.IObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x00000577 System.Void Obi.IObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
// 0x00000578 System.Boolean Obi.IObiConstraintsBatch::DeactivateConstraint(System.Int32)
// 0x00000579 System.Boolean Obi.IObiConstraintsBatch::ActivateConstraint(System.Int32)
// 0x0000057A System.Void Obi.IObiConstraintsBatch::DeactivateAllConstraints()
// 0x0000057B System.Void Obi.IObiConstraintsBatch::Clear()
// 0x0000057C System.Void Obi.IObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x0000057D System.Void Obi.IObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
// 0x0000057E System.Int32 Obi.ObiConstraintsBatch::get_constraintCount()
extern void ObiConstraintsBatch_get_constraintCount_mA8FC55D7B67C6DE94D5B7383C39D4330CEC3C4A8 (void);
// 0x0000057F System.Int32 Obi.ObiConstraintsBatch::get_activeConstraintCount()
extern void ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02 (void);
// 0x00000580 System.Void Obi.ObiConstraintsBatch::set_activeConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A (void);
// 0x00000581 System.Int32 Obi.ObiConstraintsBatch::get_initialActiveConstraintCount()
extern void ObiConstraintsBatch_get_initialActiveConstraintCount_m5F0841BDCCB934AB148E2A870F3EAA2F8552189B (void);
// 0x00000582 System.Void Obi.ObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_initialActiveConstraintCount_mF8081C7DCFDC4CF1B2A306517632F6E9C026912C (void);
// 0x00000583 Oni/ConstraintType Obi.ObiConstraintsBatch::get_constraintType()
// 0x00000584 Obi.IConstraintsBatchImpl Obi.ObiConstraintsBatch::get_implementation()
// 0x00000585 System.Void Obi.ObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiConstraintsBatch_Merge_m0157340350A52C6574BBB2D68D612AA6852ADEC3 (void);
// 0x00000586 System.Void Obi.ObiConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
// 0x00000587 System.Void Obi.ObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000588 System.Void Obi.ObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x00000589 System.Void Obi.ObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x0000058A System.Void Obi.ObiConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiConstraintsBatch_CopyConstraint_m204FC07814B69BA73E78A0E743D059F565B85874 (void);
// 0x0000058B System.Void Obi.ObiConstraintsBatch::InnerSwapConstraints(System.Int32,System.Int32)
extern void ObiConstraintsBatch_InnerSwapConstraints_m72121F6E08F53AA93A94891FB98F3B6FF2D23A91 (void);
// 0x0000058C System.Void Obi.ObiConstraintsBatch::RegisterConstraint()
extern void ObiConstraintsBatch_RegisterConstraint_m8EABAFCE01074053D942CE05D9CF5CEC9381F5FB (void);
// 0x0000058D System.Void Obi.ObiConstraintsBatch::Clear()
extern void ObiConstraintsBatch_Clear_mC782D142E91B8F81F511A011339846EC1310C2D5 (void);
// 0x0000058E System.Int32 Obi.ObiConstraintsBatch::GetConstraintIndex(System.Int32)
extern void ObiConstraintsBatch_GetConstraintIndex_mA7193601AF174D3A9323DCBC41B86CF2F9EDF24B (void);
// 0x0000058F System.Boolean Obi.ObiConstraintsBatch::IsConstraintActive(System.Int32)
extern void ObiConstraintsBatch_IsConstraintActive_m86CFCB36A5A1AEBBE1FA877DFEE386BB244A7C65 (void);
// 0x00000590 System.Boolean Obi.ObiConstraintsBatch::ActivateConstraint(System.Int32)
extern void ObiConstraintsBatch_ActivateConstraint_m715C1DA819EA321555CC3E50E102E4130B0DF12E (void);
// 0x00000591 System.Boolean Obi.ObiConstraintsBatch::DeactivateConstraint(System.Int32)
extern void ObiConstraintsBatch_DeactivateConstraint_m75EE8E2E70006545791D2BA14E1E4513F9193025 (void);
// 0x00000592 System.Void Obi.ObiConstraintsBatch::DeactivateAllConstraints()
extern void ObiConstraintsBatch_DeactivateAllConstraints_mC7259C1E725CF3BFB8EC6ECFD548E6C70C2F63C5 (void);
// 0x00000593 System.Void Obi.ObiConstraintsBatch::RemoveConstraint(System.Int32)
extern void ObiConstraintsBatch_RemoveConstraint_m5AAEA90963297F57C4BFF0C6858AA1E13AE6EC46 (void);
// 0x00000594 System.Void Obi.ObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
extern void ObiConstraintsBatch_ParticlesSwapped_mFA21282517AF5D3D422315E8748273AC39BC7ED1 (void);
// 0x00000595 System.Void Obi.ObiConstraintsBatch::.ctor()
extern void ObiConstraintsBatch__ctor_m2B0D34B60AB411AA8D929BA04F3BF98231678A81 (void);
// 0x00000596 Oni/ConstraintType Obi.ObiDistanceConstraintsBatch::get_constraintType()
extern void ObiDistanceConstraintsBatch_get_constraintType_m738D4B915ECC57433D1FBE3736930F510797A6D5 (void);
// 0x00000597 Obi.IConstraintsBatchImpl Obi.ObiDistanceConstraintsBatch::get_implementation()
extern void ObiDistanceConstraintsBatch_get_implementation_mB2318A9886BF7C3D0513E65A18526B4F28740515 (void);
// 0x00000598 System.Void Obi.ObiDistanceConstraintsBatch::.ctor(System.Int32)
extern void ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3 (void);
// 0x00000599 System.Void Obi.ObiDistanceConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single)
extern void ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021 (void);
// 0x0000059A System.Void Obi.ObiDistanceConstraintsBatch::Clear()
extern void ObiDistanceConstraintsBatch_Clear_mEE9F3DDD52ECC58C3A9B4391667F88D3FF4B48C4 (void);
// 0x0000059B System.Single Obi.ObiDistanceConstraintsBatch::GetRestLength(System.Int32)
extern void ObiDistanceConstraintsBatch_GetRestLength_m5E1C408F549700872BADA871F7301C1A368E0F12 (void);
// 0x0000059C System.Void Obi.ObiDistanceConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiDistanceConstraintsBatch_SetRestLength_m1CA836FD76DCC1AC15BECCF86DD92E487F71397E (void);
// 0x0000059D Obi.ParticlePair Obi.ObiDistanceConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiDistanceConstraintsBatch_GetParticleIndices_mB6664FFE2333714B74F7BFEE6D915DC49C3812C6 (void);
// 0x0000059E System.Void Obi.ObiDistanceConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiDistanceConstraintsBatch_GetParticlesInvolved_mC73EBE0B6DB236EC4BE938CB43ACE50F2AE65EFE (void);
// 0x0000059F System.Void Obi.ObiDistanceConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiDistanceConstraintsBatch_CopyConstraint_m0EBF810160015C8A19A39367C329D343DBD1BF44 (void);
// 0x000005A0 System.Void Obi.ObiDistanceConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiDistanceConstraintsBatch_SwapConstraints_mFC45320687BA195BF90408368D14ED2B804DC370 (void);
// 0x000005A1 System.Void Obi.ObiDistanceConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiDistanceConstraintsBatch_Merge_m4D30F883821CC7CF022584BEC90B548D14C6DC15 (void);
// 0x000005A2 System.Void Obi.ObiDistanceConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_AddToSolver_m3308FD903CF4BC955B8D2F2E0B387D2C8A79B7B8 (void);
// 0x000005A3 System.Void Obi.ObiDistanceConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_RemoveFromSolver_m6BC9C6E426F431C44B10DCA8CBFFEF4F67E0936E (void);
// 0x000005A4 Oni/ConstraintType Obi.ObiPinConstraintsBatch::get_constraintType()
extern void ObiPinConstraintsBatch_get_constraintType_m41C00D52B2D27084965D72E1F35D4E075444E482 (void);
// 0x000005A5 Obi.IConstraintsBatchImpl Obi.ObiPinConstraintsBatch::get_implementation()
extern void ObiPinConstraintsBatch_get_implementation_m4578AAC572F0C5466E210AC1BC23930F8C09C0E6 (void);
// 0x000005A6 System.Void Obi.ObiPinConstraintsBatch::.ctor(Obi.ObiPinConstraintsData)
extern void ObiPinConstraintsBatch__ctor_mEE99790C9B386AE46D24F129CDDCF2C1C5B5F5FC (void);
// 0x000005A7 System.Void Obi.ObiPinConstraintsBatch::AddConstraint(System.Int32,Obi.ObiColliderBase,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Single,System.Single)
extern void ObiPinConstraintsBatch_AddConstraint_mF22DD07678C73491928469A7741BD8B076071DCC (void);
// 0x000005A8 System.Void Obi.ObiPinConstraintsBatch::Clear()
extern void ObiPinConstraintsBatch_Clear_mB0A07E7DF21B6C3CF9C5E054F58510FFDAA522AE (void);
// 0x000005A9 System.Void Obi.ObiPinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiPinConstraintsBatch_GetParticlesInvolved_m91C044A47CCA58E91BD3E72C7DE0ECE74BC37DA8 (void);
// 0x000005AA System.Void Obi.ObiPinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiPinConstraintsBatch_SwapConstraints_mAE0389E323855EC02CB78CE15E73076B72CB1D85 (void);
// 0x000005AB System.Void Obi.ObiPinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiPinConstraintsBatch_Merge_m2E0E93A262BDA66314A1CF7125B02B0E3D3AA3D7 (void);
// 0x000005AC System.Void Obi.ObiPinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_AddToSolver_mDAB05B2150F51AA80C2911CEF1BCE5E104D37E86 (void);
// 0x000005AD System.Void Obi.ObiPinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_RemoveFromSolver_mC766AF925EE815328507994A26FC0DA6399A473C (void);
// 0x000005AE Oni/ConstraintType Obi.ObiShapeMatchingConstraintsBatch::get_constraintType()
extern void ObiShapeMatchingConstraintsBatch_get_constraintType_m1D1B013631E72B441D2F440FABC9A47260BC1EFC (void);
// 0x000005AF Obi.IConstraintsBatchImpl Obi.ObiShapeMatchingConstraintsBatch::get_implementation()
extern void ObiShapeMatchingConstraintsBatch_get_implementation_mDE72A361D520AEF2417D5FA3D99265B24AE622BE (void);
// 0x000005B0 System.Void Obi.ObiShapeMatchingConstraintsBatch::.ctor(Obi.ObiShapeMatchingConstraintsData)
extern void ObiShapeMatchingConstraintsBatch__ctor_m07C5E1D77AD00968700D64371442119BD0EFD860 (void);
// 0x000005B1 System.Void Obi.ObiShapeMatchingConstraintsBatch::AddConstraint(System.Int32[],System.Boolean)
extern void ObiShapeMatchingConstraintsBatch_AddConstraint_m36DB2368774B955435601E74A559F9194797B73E (void);
// 0x000005B2 System.Void Obi.ObiShapeMatchingConstraintsBatch::Clear()
extern void ObiShapeMatchingConstraintsBatch_Clear_m7613DFE3A4A449971A4C9C0E7F78D8ED7D99B7CF (void);
// 0x000005B3 System.Void Obi.ObiShapeMatchingConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m460EACD5CA047B22B48866E7980990188EF04B88 (void);
// 0x000005B4 System.Void Obi.ObiShapeMatchingConstraintsBatch::RemoveParticleFromConstraint(System.Int32,System.Int32)
extern void ObiShapeMatchingConstraintsBatch_RemoveParticleFromConstraint_mD284790EE70116086A1EF3AF9565322E629039FF (void);
// 0x000005B5 System.Void Obi.ObiShapeMatchingConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiShapeMatchingConstraintsBatch_SwapConstraints_m203CA70E418F8DF2BF506ED333695247E286BA2B (void);
// 0x000005B6 System.Void Obi.ObiShapeMatchingConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiShapeMatchingConstraintsBatch_Merge_m5A9DEA535310B3863D91C0FBE5A52C051E0357A7 (void);
// 0x000005B7 System.Void Obi.ObiShapeMatchingConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_AddToSolver_mA64BACE29C81AC0D914642A2326C5A768BCAD690 (void);
// 0x000005B8 System.Void Obi.ObiShapeMatchingConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_RemoveFromSolver_m64F2CB468C81ED844CEE845FC280A5497A18AD33 (void);
// 0x000005B9 System.Void Obi.ObiShapeMatchingConstraintsBatch::RecalculateRestShapeMatching()
extern void ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_m49C8363C8157E30089548C4BDF8BDA633D26DDB7 (void);
// 0x000005BA Oni/ConstraintType Obi.ObiSkinConstraintsBatch::get_constraintType()
extern void ObiSkinConstraintsBatch_get_constraintType_mB8336F19B6DBE7777042FA4814EA12115D899223 (void);
// 0x000005BB Obi.IConstraintsBatchImpl Obi.ObiSkinConstraintsBatch::get_implementation()
extern void ObiSkinConstraintsBatch_get_implementation_m8EFE8C0407125BE90F4A544FD0BBD6801F606924 (void);
// 0x000005BC System.Void Obi.ObiSkinConstraintsBatch::.ctor(Obi.ObiSkinConstraintsData)
extern void ObiSkinConstraintsBatch__ctor_m1C9F633F90BE267DDA66B580DED05A7F19BD61C5 (void);
// 0x000005BD System.Void Obi.ObiSkinConstraintsBatch::AddConstraint(System.Int32,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Single,System.Single,System.Single)
extern void ObiSkinConstraintsBatch_AddConstraint_m5D3D3C2490F09B0C7B72F763AA56E3AE9D485134 (void);
// 0x000005BE System.Void Obi.ObiSkinConstraintsBatch::Clear()
extern void ObiSkinConstraintsBatch_Clear_m9760EF8B82970CC9CDE9A840BD9EAF78BD982393 (void);
// 0x000005BF System.Void Obi.ObiSkinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiSkinConstraintsBatch_GetParticlesInvolved_m77E2085BC3B66F68FE4B9453F0D76A9566718597 (void);
// 0x000005C0 System.Void Obi.ObiSkinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiSkinConstraintsBatch_SwapConstraints_m90010A1D7C64381F5575F0F393B7030C2201DF8F (void);
// 0x000005C1 System.Void Obi.ObiSkinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiSkinConstraintsBatch_Merge_m5AC677F66016C8CC6DCC768E4A60ECEE633075A6 (void);
// 0x000005C2 System.Void Obi.ObiSkinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_AddToSolver_mF9FF0C39D2BA6C21C642F2F0880004B3D719DC2C (void);
// 0x000005C3 System.Void Obi.ObiSkinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_RemoveFromSolver_m146DF16BA35D6BBBAE5781504D744C59D61D80FE (void);
// 0x000005C4 Oni/ConstraintType Obi.ObiStretchShearConstraintsBatch::get_constraintType()
extern void ObiStretchShearConstraintsBatch_get_constraintType_mC6DEA77B39D02D597E2CA898182A9BD560B545C6 (void);
// 0x000005C5 Obi.IConstraintsBatchImpl Obi.ObiStretchShearConstraintsBatch::get_implementation()
extern void ObiStretchShearConstraintsBatch_get_implementation_m0DAC376106BC35C967BE578A40049E3C35E27024 (void);
// 0x000005C6 System.Void Obi.ObiStretchShearConstraintsBatch::.ctor(Obi.ObiStretchShearConstraintsData)
extern void ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7 (void);
// 0x000005C7 System.Void Obi.ObiStretchShearConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Int32,System.Single,UnityEngine.Quaternion)
extern void ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5 (void);
// 0x000005C8 System.Void Obi.ObiStretchShearConstraintsBatch::Clear()
extern void ObiStretchShearConstraintsBatch_Clear_m827C6A5985D286459089DC4635044609BC6B098C (void);
// 0x000005C9 System.Single Obi.ObiStretchShearConstraintsBatch::GetRestLength(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetRestLength_mC0368700D80E14C1F758085E1051265DB7E6F140 (void);
// 0x000005CA System.Void Obi.ObiStretchShearConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiStretchShearConstraintsBatch_SetRestLength_m4401CCB79E8593DE77C2F66191AE967C6DC7E5DC (void);
// 0x000005CB Obi.ParticlePair Obi.ObiStretchShearConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetParticleIndices_mC265643A55371F01BD13A0C3FC34A11D9FFE838C (void);
// 0x000005CC System.Void Obi.ObiStretchShearConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiStretchShearConstraintsBatch_GetParticlesInvolved_m304EEFB3092A0112EAE84B4CECF3F6A948D9833E (void);
// 0x000005CD System.Void Obi.ObiStretchShearConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiStretchShearConstraintsBatch_SwapConstraints_m2C8677D96612ADAE5BFCC4825A9BA4E42685609E (void);
// 0x000005CE System.Void Obi.ObiStretchShearConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiStretchShearConstraintsBatch_Merge_mC487E3A37D51D78F57A1880A27B8168E45F05BB8 (void);
// 0x000005CF System.Void Obi.ObiStretchShearConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_AddToSolver_mA25B51FDA1EBF1E1BE6AD594D45C153216AA8C94 (void);
// 0x000005D0 System.Void Obi.ObiStretchShearConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_RemoveFromSolver_m45797BE9F2A7CC2B0BD983E810A313C126621382 (void);
// 0x000005D1 Oni/ConstraintType Obi.ObiTetherConstraintsBatch::get_constraintType()
extern void ObiTetherConstraintsBatch_get_constraintType_m8483A4DC0E6B622F67EC74C1BB113C00981EFA6C (void);
// 0x000005D2 Obi.IConstraintsBatchImpl Obi.ObiTetherConstraintsBatch::get_implementation()
extern void ObiTetherConstraintsBatch_get_implementation_mC6C6A9923482CF3A380B6D250E1A63C0EC69E02F (void);
// 0x000005D3 System.Void Obi.ObiTetherConstraintsBatch::.ctor(Obi.ObiTetherConstraintsData)
extern void ObiTetherConstraintsBatch__ctor_m8866FC2DA610911877A83CE2904D78CACB19FC51 (void);
// 0x000005D4 System.Void Obi.ObiTetherConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single,System.Single)
extern void ObiTetherConstraintsBatch_AddConstraint_m1BF702B37FD24AD678D201379C614BFA7C24824A (void);
// 0x000005D5 System.Void Obi.ObiTetherConstraintsBatch::Clear()
extern void ObiTetherConstraintsBatch_Clear_m2F60F212808B2D2566D9486751BA43D287D7A616 (void);
// 0x000005D6 System.Void Obi.ObiTetherConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiTetherConstraintsBatch_GetParticlesInvolved_m77FB1AD6E9F76D24F63C6CF2FCA60F8627D497BA (void);
// 0x000005D7 System.Void Obi.ObiTetherConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiTetherConstraintsBatch_SwapConstraints_mE3AD347DF0A9C8FA3D0467F87FFEC0B13FD61E72 (void);
// 0x000005D8 System.Void Obi.ObiTetherConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiTetherConstraintsBatch_Merge_m1AD265CC0EE520D80F27DEFBE161FF9E44B80A76 (void);
// 0x000005D9 System.Void Obi.ObiTetherConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_AddToSolver_mD57866191E80F88A3D272C0E0AC76364326A80E3 (void);
// 0x000005DA System.Void Obi.ObiTetherConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_RemoveFromSolver_mEE3D5BD7C2CF365225947E773A815095121EF2C6 (void);
// 0x000005DB System.Void Obi.ObiTetherConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiTetherConstraintsBatch_SetParameters_m1577626B4E1C0111CF067DABC451BC0503615466 (void);
// 0x000005DC Oni/ConstraintType Obi.ObiVolumeConstraintsBatch::get_constraintType()
extern void ObiVolumeConstraintsBatch_get_constraintType_m381ED3FD4921DB235D1AE2E0D93D354C840CE86A (void);
// 0x000005DD Obi.IConstraintsBatchImpl Obi.ObiVolumeConstraintsBatch::get_implementation()
extern void ObiVolumeConstraintsBatch_get_implementation_mA945BB5C6B7A7EE83AC6C01C6B5DD21207FB0325 (void);
// 0x000005DE System.Void Obi.ObiVolumeConstraintsBatch::.ctor(Obi.ObiVolumeConstraintsData)
extern void ObiVolumeConstraintsBatch__ctor_mA44A358BC97BD1830050A02DFFDF5108A6FD9715 (void);
// 0x000005DF System.Void Obi.ObiVolumeConstraintsBatch::AddConstraint(System.Int32[],System.Single)
extern void ObiVolumeConstraintsBatch_AddConstraint_m411C62C1E1117BF0B3B6954C3641F5B418583424 (void);
// 0x000005E0 System.Void Obi.ObiVolumeConstraintsBatch::Clear()
extern void ObiVolumeConstraintsBatch_Clear_m53B029F54892E0D5B9D4D2BCB647A4176FE793A0 (void);
// 0x000005E1 System.Void Obi.ObiVolumeConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiVolumeConstraintsBatch_GetParticlesInvolved_mC38E81ADD236D1D8841141D16BA6989DA3A489F2 (void);
// 0x000005E2 System.Void Obi.ObiVolumeConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiVolumeConstraintsBatch_SwapConstraints_m42B669FC7CBDFA494210C6486682BD7A3CFFDDB4 (void);
// 0x000005E3 System.Void Obi.ObiVolumeConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiVolumeConstraintsBatch_Merge_m1DDAB49D1408ADCD12D27C72737072480252DA7B (void);
// 0x000005E4 System.Void Obi.ObiVolumeConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_AddToSolver_m1070A8D58A5B37CC00B5290D1448490F3DD9CE29 (void);
// 0x000005E5 System.Void Obi.ObiVolumeConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_RemoveFromSolver_m01AB99DBC04273CEB49EFAAFE595F93D5F05B726 (void);
// 0x000005E6 System.Void Obi.ObiVolumeConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiVolumeConstraintsBatch_SetParameters_m8B574D29F6430355A0AF20B229AA493E0119AEF5 (void);
// 0x000005E7 System.Boolean Obi.IAerodynamicConstraintsUser::get_aerodynamicsEnabled()
// 0x000005E8 System.Void Obi.IAerodynamicConstraintsUser::set_aerodynamicsEnabled(System.Boolean)
// 0x000005E9 System.Single Obi.IAerodynamicConstraintsUser::get_drag()
// 0x000005EA System.Void Obi.IAerodynamicConstraintsUser::set_drag(System.Single)
// 0x000005EB System.Single Obi.IAerodynamicConstraintsUser::get_lift()
// 0x000005EC System.Void Obi.IAerodynamicConstraintsUser::set_lift(System.Single)
// 0x000005ED Obi.ObiAerodynamicConstraintsBatch Obi.ObiAerodynamicConstraintsData::CreateBatch(Obi.ObiAerodynamicConstraintsBatch)
extern void ObiAerodynamicConstraintsData_CreateBatch_mD91F60B676E69FE28DD53AA57D5E64E621F7E489 (void);
// 0x000005EE System.Void Obi.ObiAerodynamicConstraintsData::.ctor()
extern void ObiAerodynamicConstraintsData__ctor_mD5BD0D829EC39F1CACD35CF16E040852B4B4784B (void);
// 0x000005EF System.Boolean Obi.IBendConstraintsUser::get_bendConstraintsEnabled()
// 0x000005F0 System.Void Obi.IBendConstraintsUser::set_bendConstraintsEnabled(System.Boolean)
// 0x000005F1 System.Single Obi.IBendConstraintsUser::get_bendCompliance()
// 0x000005F2 System.Void Obi.IBendConstraintsUser::set_bendCompliance(System.Single)
// 0x000005F3 System.Single Obi.IBendConstraintsUser::get_maxBending()
// 0x000005F4 System.Void Obi.IBendConstraintsUser::set_maxBending(System.Single)
// 0x000005F5 System.Single Obi.IBendConstraintsUser::get_plasticYield()
// 0x000005F6 System.Void Obi.IBendConstraintsUser::set_plasticYield(System.Single)
// 0x000005F7 System.Single Obi.IBendConstraintsUser::get_plasticCreep()
// 0x000005F8 System.Void Obi.IBendConstraintsUser::set_plasticCreep(System.Single)
// 0x000005F9 Obi.ObiBendConstraintsBatch Obi.ObiBendConstraintsData::CreateBatch(Obi.ObiBendConstraintsBatch)
extern void ObiBendConstraintsData_CreateBatch_mD0A372B63F6C4F88AC48D4E4E9304944D3FEA0F2 (void);
// 0x000005FA System.Void Obi.ObiBendConstraintsData::.ctor()
extern void ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A (void);
// 0x000005FB System.Boolean Obi.IBendTwistConstraintsUser::get_bendTwistConstraintsEnabled()
// 0x000005FC System.Void Obi.IBendTwistConstraintsUser::set_bendTwistConstraintsEnabled(System.Boolean)
// 0x000005FD UnityEngine.Vector3 Obi.IBendTwistConstraintsUser::GetBendTwistCompliance(Obi.ObiBendTwistConstraintsBatch,System.Int32)
// 0x000005FE UnityEngine.Vector2 Obi.IBendTwistConstraintsUser::GetBendTwistPlasticity(Obi.ObiBendTwistConstraintsBatch,System.Int32)
// 0x000005FF Obi.ObiBendTwistConstraintsBatch Obi.ObiBendTwistConstraintsData::CreateBatch(Obi.ObiBendTwistConstraintsBatch)
extern void ObiBendTwistConstraintsData_CreateBatch_m9A87A7382C1A63A339F0C8B17CF14D1CB03B0D46 (void);
// 0x00000600 System.Void Obi.ObiBendTwistConstraintsData::.ctor()
extern void ObiBendTwistConstraintsData__ctor_mC6E27E26B877C88510C9E27FB9ED17BF94E98B9A (void);
// 0x00000601 System.Boolean Obi.IChainConstraintsUser::get_chainConstraintsEnabled()
// 0x00000602 System.Void Obi.IChainConstraintsUser::set_chainConstraintsEnabled(System.Boolean)
// 0x00000603 System.Single Obi.IChainConstraintsUser::get_tightness()
// 0x00000604 System.Void Obi.IChainConstraintsUser::set_tightness(System.Single)
// 0x00000605 Obi.ObiChainConstraintsBatch Obi.ObiChainConstraintsData::CreateBatch(Obi.ObiChainConstraintsBatch)
extern void ObiChainConstraintsData_CreateBatch_mC2FB00A47639537C6959AE16661DAAF1AD4C3F7B (void);
// 0x00000606 System.Void Obi.ObiChainConstraintsData::.ctor()
extern void ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD (void);
// 0x00000607 System.Boolean Obi.IDistanceConstraintsUser::get_distanceConstraintsEnabled()
// 0x00000608 System.Void Obi.IDistanceConstraintsUser::set_distanceConstraintsEnabled(System.Boolean)
// 0x00000609 System.Single Obi.IDistanceConstraintsUser::get_stretchingScale()
// 0x0000060A System.Void Obi.IDistanceConstraintsUser::set_stretchingScale(System.Single)
// 0x0000060B System.Single Obi.IDistanceConstraintsUser::get_stretchCompliance()
// 0x0000060C System.Void Obi.IDistanceConstraintsUser::set_stretchCompliance(System.Single)
// 0x0000060D System.Single Obi.IDistanceConstraintsUser::get_maxCompression()
// 0x0000060E System.Void Obi.IDistanceConstraintsUser::set_maxCompression(System.Single)
// 0x0000060F Obi.ObiDistanceConstraintsBatch Obi.ObiDistanceConstraintsData::CreateBatch(Obi.ObiDistanceConstraintsBatch)
extern void ObiDistanceConstraintsData_CreateBatch_mA63574AD8629D5C6907EB15B3FFA9F0D1616D026 (void);
// 0x00000610 System.Void Obi.ObiDistanceConstraintsData::.ctor()
extern void ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E (void);
// 0x00000611 Obi.ObiPinConstraintsBatch Obi.ObiPinConstraintsData::CreateBatch(Obi.ObiPinConstraintsBatch)
extern void ObiPinConstraintsData_CreateBatch_mC8E21D95D7C5E27097C6B75B20DC74CBDD2D345B (void);
// 0x00000612 System.Void Obi.ObiPinConstraintsData::.ctor()
extern void ObiPinConstraintsData__ctor_mA86A434A33F8CF37FA8E7E2410BF8223116F03A0 (void);
// 0x00000613 System.Boolean Obi.IShapeMatchingConstraintsUser::get_shapeMatchingConstraintsEnabled()
// 0x00000614 System.Void Obi.IShapeMatchingConstraintsUser::set_shapeMatchingConstraintsEnabled(System.Boolean)
// 0x00000615 System.Single Obi.IShapeMatchingConstraintsUser::get_deformationResistance()
// 0x00000616 System.Void Obi.IShapeMatchingConstraintsUser::set_deformationResistance(System.Single)
// 0x00000617 System.Single Obi.IShapeMatchingConstraintsUser::get_maxDeformation()
// 0x00000618 System.Void Obi.IShapeMatchingConstraintsUser::set_maxDeformation(System.Single)
// 0x00000619 System.Single Obi.IShapeMatchingConstraintsUser::get_plasticYield()
// 0x0000061A System.Void Obi.IShapeMatchingConstraintsUser::set_plasticYield(System.Single)
// 0x0000061B System.Single Obi.IShapeMatchingConstraintsUser::get_plasticCreep()
// 0x0000061C System.Void Obi.IShapeMatchingConstraintsUser::set_plasticCreep(System.Single)
// 0x0000061D System.Single Obi.IShapeMatchingConstraintsUser::get_plasticRecovery()
// 0x0000061E System.Void Obi.IShapeMatchingConstraintsUser::set_plasticRecovery(System.Single)
// 0x0000061F Obi.ObiShapeMatchingConstraintsBatch Obi.ObiShapeMatchingConstraintsData::CreateBatch(Obi.ObiShapeMatchingConstraintsBatch)
extern void ObiShapeMatchingConstraintsData_CreateBatch_mDD0753126D9E04AB8FD0D7F4CB4D6C9ACA8BF359 (void);
// 0x00000620 System.Void Obi.ObiShapeMatchingConstraintsData::.ctor()
extern void ObiShapeMatchingConstraintsData__ctor_m39A87174E47FB428D6589800F2F18FE1F79D340A (void);
// 0x00000621 System.Boolean Obi.ISkinConstraintsUser::get_skinConstraintsEnabled()
// 0x00000622 System.Void Obi.ISkinConstraintsUser::set_skinConstraintsEnabled(System.Boolean)
// 0x00000623 UnityEngine.Vector3 Obi.ISkinConstraintsUser::GetSkinRadiiBackstop(Obi.ObiSkinConstraintsBatch,System.Int32)
// 0x00000624 System.Single Obi.ISkinConstraintsUser::GetSkinCompliance(Obi.ObiSkinConstraintsBatch,System.Int32)
// 0x00000625 Obi.ObiSkinConstraintsBatch Obi.ObiSkinConstraintsData::CreateBatch(Obi.ObiSkinConstraintsBatch)
extern void ObiSkinConstraintsData_CreateBatch_m0420AD7727A873F46A9E43F8BF1F94895AB14369 (void);
// 0x00000626 System.Void Obi.ObiSkinConstraintsData::.ctor()
extern void ObiSkinConstraintsData__ctor_mCB9A63E7B637DEAEB1063E52913985321ABBCAA3 (void);
// 0x00000627 System.Boolean Obi.IStretchShearConstraintsUser::get_stretchShearConstraintsEnabled()
// 0x00000628 System.Void Obi.IStretchShearConstraintsUser::set_stretchShearConstraintsEnabled(System.Boolean)
// 0x00000629 UnityEngine.Vector3 Obi.IStretchShearConstraintsUser::GetStretchShearCompliance(Obi.ObiStretchShearConstraintsBatch,System.Int32)
// 0x0000062A Obi.ObiStretchShearConstraintsBatch Obi.ObiStretchShearConstraintsData::CreateBatch(Obi.ObiStretchShearConstraintsBatch)
extern void ObiStretchShearConstraintsData_CreateBatch_mFBE3B0B12D6BD8E226076AB93E16906F92B60C68 (void);
// 0x0000062B System.Void Obi.ObiStretchShearConstraintsData::.ctor()
extern void ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC (void);
// 0x0000062C System.Boolean Obi.ITetherConstraintsUser::get_tetherConstraintsEnabled()
// 0x0000062D System.Void Obi.ITetherConstraintsUser::set_tetherConstraintsEnabled(System.Boolean)
// 0x0000062E System.Single Obi.ITetherConstraintsUser::get_tetherCompliance()
// 0x0000062F System.Void Obi.ITetherConstraintsUser::set_tetherCompliance(System.Single)
// 0x00000630 System.Single Obi.ITetherConstraintsUser::get_tetherScale()
// 0x00000631 System.Void Obi.ITetherConstraintsUser::set_tetherScale(System.Single)
// 0x00000632 Obi.ObiTetherConstraintsBatch Obi.ObiTetherConstraintsData::CreateBatch(Obi.ObiTetherConstraintsBatch)
extern void ObiTetherConstraintsData_CreateBatch_m28C477BD204B78B77C867473CB959C5340B918D7 (void);
// 0x00000633 System.Void Obi.ObiTetherConstraintsData::.ctor()
extern void ObiTetherConstraintsData__ctor_m7488642C0CCCDA9C252605F19F887C961D3BB5C4 (void);
// 0x00000634 System.Boolean Obi.IVolumeConstraintsUser::get_volumeConstraintsEnabled()
// 0x00000635 System.Void Obi.IVolumeConstraintsUser::set_volumeConstraintsEnabled(System.Boolean)
// 0x00000636 System.Single Obi.IVolumeConstraintsUser::get_compressionCompliance()
// 0x00000637 System.Void Obi.IVolumeConstraintsUser::set_compressionCompliance(System.Single)
// 0x00000638 System.Single Obi.IVolumeConstraintsUser::get_pressure()
// 0x00000639 System.Void Obi.IVolumeConstraintsUser::set_pressure(System.Single)
// 0x0000063A Obi.ObiVolumeConstraintsBatch Obi.ObiVolumeConstraintsData::CreateBatch(Obi.ObiVolumeConstraintsBatch)
extern void ObiVolumeConstraintsData_CreateBatch_m1DDE0FAC895B8FC54D095EB773D7AEBDE107A061 (void);
// 0x0000063B System.Void Obi.ObiVolumeConstraintsData::.ctor()
extern void ObiVolumeConstraintsData__ctor_m09F646B51D450E9B87B194F9E29B8B5FE9C2404B (void);
// 0x0000063C System.Nullable`1<Oni/ConstraintType> Obi.IObiConstraints::GetConstraintType()
// 0x0000063D Obi.IObiConstraintsBatch Obi.IObiConstraints::GetBatch(System.Int32)
// 0x0000063E System.Int32 Obi.IObiConstraints::GetBatchCount()
// 0x0000063F System.Void Obi.IObiConstraints::Clear()
// 0x00000640 System.Boolean Obi.IObiConstraints::AddToSolver(Obi.ObiSolver)
// 0x00000641 System.Boolean Obi.IObiConstraints::RemoveFromSolver()
// 0x00000642 System.Int32 Obi.IObiConstraints::GetConstraintCount()
// 0x00000643 System.Int32 Obi.IObiConstraints::GetActiveConstraintCount()
// 0x00000644 System.Void Obi.IObiConstraints::DeactivateAllConstraints()
// 0x00000645 System.Void Obi.IObiConstraints::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x00000646 System.Void Obi.ObiConstraints`1::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x00000647 Obi.IObiConstraintsBatch Obi.ObiConstraints`1::GetBatch(System.Int32)
// 0x00000648 System.Int32 Obi.ObiConstraints`1::GetBatchCount()
// 0x00000649 System.Int32 Obi.ObiConstraints`1::GetConstraintCount()
// 0x0000064A System.Int32 Obi.ObiConstraints`1::GetActiveConstraintCount()
// 0x0000064B System.Void Obi.ObiConstraints`1::DeactivateAllConstraints()
// 0x0000064C T Obi.ObiConstraints`1::GetFirstBatch()
// 0x0000064D System.Nullable`1<Oni/ConstraintType> Obi.ObiConstraints`1::GetConstraintType()
// 0x0000064E System.Void Obi.ObiConstraints`1::Clear()
// 0x0000064F T Obi.ObiConstraints`1::CreateBatch(T)
// 0x00000650 System.Void Obi.ObiConstraints`1::AddBatch(T)
// 0x00000651 System.Boolean Obi.ObiConstraints`1::RemoveBatch(T)
// 0x00000652 System.Boolean Obi.ObiConstraints`1::AddToSolver(Obi.ObiSolver)
// 0x00000653 System.Boolean Obi.ObiConstraints`1::RemoveFromSolver()
// 0x00000654 System.Void Obi.ObiConstraints`1::.ctor()
// 0x00000655 System.Single Obi.StructuralConstraint::get_restLength()
extern void StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57 (void);
// 0x00000656 System.Void Obi.StructuralConstraint::set_restLength(System.Single)
extern void StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF (void);
// 0x00000657 System.Void Obi.StructuralConstraint::.ctor(Obi.IStructuralConstraintBatch,System.Int32,System.Single)
extern void StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322 (void);
// 0x00000658 System.Int32[] Obi.GraphColoring::Colorize(System.Int32[],System.Int32[])
extern void GraphColoring_Colorize_mC1E72C7E56EE6F1F72682546D5CB94934287958C (void);
// 0x00000659 System.Void Obi.ObiActorBlueprint::add_OnBlueprintGenerate(Obi.ObiActorBlueprint/BlueprintCallback)
extern void ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E (void);
// 0x0000065A System.Void Obi.ObiActorBlueprint::remove_OnBlueprintGenerate(Obi.ObiActorBlueprint/BlueprintCallback)
extern void ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E (void);
// 0x0000065B System.Int32 Obi.ObiActorBlueprint::get_particleCount()
extern void ObiActorBlueprint_get_particleCount_mCC6F47C83D7F2BE2BBF1667511B5C96BAA5A94B2 (void);
// 0x0000065C System.Int32 Obi.ObiActorBlueprint::get_activeParticleCount()
extern void ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A (void);
// 0x0000065D System.Boolean Obi.ObiActorBlueprint::get_usesOrientedParticles()
extern void ObiActorBlueprint_get_usesOrientedParticles_m8E951A7BDC8EEA54633C2AA8BE8E41F01A418233 (void);
// 0x0000065E System.Boolean Obi.ObiActorBlueprint::get_usesTethers()
extern void ObiActorBlueprint_get_usesTethers_m7A16422C66E95F3984D2A3A8F950305E9D5C2026 (void);
// 0x0000065F System.Boolean Obi.ObiActorBlueprint::IsParticleActive(System.Int32)
extern void ObiActorBlueprint_IsParticleActive_mD079A02C9E95DB8F8900FA0C1FE9A057C1F1D05A (void);
// 0x00000660 System.Void Obi.ObiActorBlueprint::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActorBlueprint_SwapWithFirstInactiveParticle_mCBD4A74BAC682B6DB6228F3B53FCC5D90B069126 (void);
// 0x00000661 System.Boolean Obi.ObiActorBlueprint::ActivateParticle(System.Int32)
extern void ObiActorBlueprint_ActivateParticle_mA772B1D5DA1364AA8A889F069D9F903E565A3F02 (void);
// 0x00000662 System.Boolean Obi.ObiActorBlueprint::DeactivateParticle(System.Int32)
extern void ObiActorBlueprint_DeactivateParticle_m2EFB2F10CE118C9041593C482C6FC80F24A9C8A7 (void);
// 0x00000663 System.Boolean Obi.ObiActorBlueprint::get_empty()
extern void ObiActorBlueprint_get_empty_m5407EABADC0A7CFE5EBBA320717C3B4609F2FDD1 (void);
// 0x00000664 System.Void Obi.ObiActorBlueprint::RecalculateBounds()
extern void ObiActorBlueprint_RecalculateBounds_mC32E8C37E376B8342D46E8894A36161EDF37E48A (void);
// 0x00000665 UnityEngine.Bounds Obi.ObiActorBlueprint::get_bounds()
extern void ObiActorBlueprint_get_bounds_m8F16C4CEAFB92614DEC83B9F41D565B4FE542C15 (void);
// 0x00000666 System.Collections.Generic.IEnumerable`1<Obi.IObiConstraints> Obi.ObiActorBlueprint::GetConstraints()
extern void ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597 (void);
// 0x00000667 Obi.IObiConstraints Obi.ObiActorBlueprint::GetConstraintsByType(Oni/ConstraintType)
extern void ObiActorBlueprint_GetConstraintsByType_mD32BF0C8BE5453BF0F534BCC92F2BD30477E7761 (void);
// 0x00000668 System.Int32 Obi.ObiActorBlueprint::GetParticleRuntimeIndex(System.Int32)
extern void ObiActorBlueprint_GetParticleRuntimeIndex_m6495778E4FF39D59DA86401684ABED0361E6803B (void);
// 0x00000669 UnityEngine.Vector3 Obi.ObiActorBlueprint::GetParticlePosition(System.Int32)
extern void ObiActorBlueprint_GetParticlePosition_m4B9F2D62EEF90A610C7DC8E1B07D4525ED90A081 (void);
// 0x0000066A UnityEngine.Quaternion Obi.ObiActorBlueprint::GetParticleOrientation(System.Int32)
extern void ObiActorBlueprint_GetParticleOrientation_mE80B9B4333A1EE1670B7DAD73FD55F8C8F6F178D (void);
// 0x0000066B System.Void Obi.ObiActorBlueprint::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActorBlueprint_GetParticleAnisotropy_mBFB7DD70BF52D6DEC5E08DEFC5A8F07BBFCBE6B9 (void);
// 0x0000066C System.Single Obi.ObiActorBlueprint::GetParticleMaxRadius(System.Int32)
extern void ObiActorBlueprint_GetParticleMaxRadius_m1106A97F11068907833E40301081A52749BC79F4 (void);
// 0x0000066D UnityEngine.Color Obi.ObiActorBlueprint::GetParticleColor(System.Int32)
extern void ObiActorBlueprint_GetParticleColor_mA1CA0E89A089B4CEBAFFD4EB83CBFC5EB5A6EB0D (void);
// 0x0000066E System.Void Obi.ObiActorBlueprint::GenerateImmediate()
extern void ObiActorBlueprint_GenerateImmediate_mD3FF977E24F4965AB776BF1E1DF0272F3D6CCA33 (void);
// 0x0000066F System.Collections.IEnumerator Obi.ObiActorBlueprint::Generate()
extern void ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A (void);
// 0x00000670 System.Void Obi.ObiActorBlueprint::Clear()
extern void ObiActorBlueprint_Clear_mD6317D8274462CCEF04D76D0957286FF73D692EB (void);
// 0x00000671 Obi.ObiParticleGroup Obi.ObiActorBlueprint::InsertNewParticleGroup(System.String,System.Int32,System.Boolean)
extern void ObiActorBlueprint_InsertNewParticleGroup_m5C512B2A9ED1227695249B3DDC9C19A9D421AAAD (void);
// 0x00000672 Obi.ObiParticleGroup Obi.ObiActorBlueprint::AppendNewParticleGroup(System.String,System.Boolean)
extern void ObiActorBlueprint_AppendNewParticleGroup_m33A05025BCF9CD281364DDA0E3563AE5091E6D79 (void);
// 0x00000673 System.Boolean Obi.ObiActorBlueprint::RemoveParticleGroupAt(System.Int32,System.Boolean)
extern void ObiActorBlueprint_RemoveParticleGroupAt_m583F9722B398FB85BFFB3AA17AB4E53424FD4E18 (void);
// 0x00000674 System.Boolean Obi.ObiActorBlueprint::SetParticleGroupName(System.Int32,System.String,System.Boolean)
extern void ObiActorBlueprint_SetParticleGroupName_mEC41E6FFFC12BB73B6BA214F81CF70CE0A034A34 (void);
// 0x00000675 System.Void Obi.ObiActorBlueprint::ClearParticleGroups(System.Boolean)
extern void ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18 (void);
// 0x00000676 System.Boolean Obi.ObiActorBlueprint::IsParticleSharedInConstraint(System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_IsParticleSharedInConstraint_m9F71E2E0E9B16D9CCB47B7D270391F65232D64A1 (void);
// 0x00000677 System.Boolean Obi.ObiActorBlueprint::DoesParticleShareConstraints(Obi.IObiConstraints,System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_DoesParticleShareConstraints_mCCD929D6E8D050B9B2A377A3965B6F8E64538883 (void);
// 0x00000678 System.Void Obi.ObiActorBlueprint::DeactivateConstraintsWithInactiveParticles(Obi.IObiConstraints,System.Collections.Generic.List`1<System.Int32>)
extern void ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_m2F8B411DA9FE9FCDC8116158247E0E83EEA513E1 (void);
// 0x00000679 System.Void Obi.ObiActorBlueprint::ParticlesSwappedInGroups(System.Int32,System.Int32)
extern void ObiActorBlueprint_ParticlesSwappedInGroups_m9104E8E53B4D76CFFD62EF8277E5313A73867EE5 (void);
// 0x0000067A System.Void Obi.ObiActorBlueprint::RemoveSelectedParticles(System.Boolean[]&,System.Boolean)
extern void ObiActorBlueprint_RemoveSelectedParticles_m1FD86679B5F6C5DEDED32F79658292F13566C63F (void);
// 0x0000067B System.Void Obi.ObiActorBlueprint::RestoreRemovedParticles()
extern void ObiActorBlueprint_RestoreRemovedParticles_m6968118C8E330062E76B6F79155E07B86C091400 (void);
// 0x0000067C System.Void Obi.ObiActorBlueprint::GenerateTethers(System.Boolean[])
extern void ObiActorBlueprint_GenerateTethers_mE0CECBEF8A33A7E44D7EFD24A87697C45209B668 (void);
// 0x0000067D System.Void Obi.ObiActorBlueprint::ClearTethers()
extern void ObiActorBlueprint_ClearTethers_mA11CB91D97A72D5311C67FCC335ACA3900020EEB (void);
// 0x0000067E System.Collections.IEnumerator Obi.ObiActorBlueprint::Initialize()
// 0x0000067F System.Void Obi.ObiActorBlueprint::.ctor()
extern void ObiActorBlueprint__ctor_m9D95F18391AC084CE03CF008E1D47864DF75A636 (void);
// 0x00000680 System.Void Obi.ObiActorBlueprint/BlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void BlueprintCallback__ctor_m75F65AE60505CE5651A0F020168360371AD0D0EE (void);
// 0x00000681 System.Void Obi.ObiActorBlueprint/BlueprintCallback::Invoke(Obi.ObiActorBlueprint)
extern void BlueprintCallback_Invoke_m0F0A317733BA923A00DCF82C10E2AF745667377A (void);
// 0x00000682 System.IAsyncResult Obi.ObiActorBlueprint/BlueprintCallback::BeginInvoke(Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void BlueprintCallback_BeginInvoke_mD8A830F18B9F7B76E81DB436AF1462092202B3F3 (void);
// 0x00000683 System.Void Obi.ObiActorBlueprint/BlueprintCallback::EndInvoke(System.IAsyncResult)
extern void BlueprintCallback_EndInvoke_m35D1EBBFF55BAC420142369AEB4B24D44D031B3F (void);
// 0x00000684 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::.ctor(System.Int32)
extern void U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C (void);
// 0x00000685 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::System.IDisposable.Dispose()
extern void U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB (void);
// 0x00000686 System.Boolean Obi.ObiActorBlueprint/<GetConstraints>d__50::MoveNext()
extern void U3CGetConstraintsU3Ed__50_MoveNext_m440BD75AC003FACCE06308279346A5432EDC9BFC (void);
// 0x00000687 Obi.IObiConstraints Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.Generic.IEnumerator<Obi.IObiConstraints>.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2 (void);
// 0x00000688 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerator.Reset()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A (void);
// 0x00000689 System.Object Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730 (void);
// 0x0000068A System.Collections.Generic.IEnumerator`1<Obi.IObiConstraints> Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.Generic.IEnumerable<Obi.IObiConstraints>.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF (void);
// 0x0000068B System.Collections.IEnumerator Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D (void);
// 0x0000068C System.Void Obi.ObiActorBlueprint/<Generate>d__59::.ctor(System.Int32)
extern void U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1 (void);
// 0x0000068D System.Void Obi.ObiActorBlueprint/<Generate>d__59::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22 (void);
// 0x0000068E System.Boolean Obi.ObiActorBlueprint/<Generate>d__59::MoveNext()
extern void U3CGenerateU3Ed__59_MoveNext_m1B248A89761AD00C954EAEB25335090B6378CB9E (void);
// 0x0000068F System.Object Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E (void);
// 0x00000690 System.Void Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6 (void);
// 0x00000691 System.Object Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734 (void);
// 0x00000692 System.Void Obi.ObiMeshBasedActorBlueprint::.ctor()
extern void ObiMeshBasedActorBlueprint__ctor_m68ACFAD2ACE6D36D748D434E2BD130B742B17BE8 (void);
// 0x00000693 Obi.ObiActorBlueprint Obi.ObiParticleGroup::get_blueprint()
extern void ObiParticleGroup_get_blueprint_m7B969093BC47FD5B58C6D6839D8F9E3BDA3119FF (void);
// 0x00000694 System.Void Obi.ObiParticleGroup::SetSourceBlueprint(Obi.ObiActorBlueprint)
extern void ObiParticleGroup_SetSourceBlueprint_m83016ECF67DCB8BDF873C154BB4A0EE7A8091011 (void);
// 0x00000695 System.Int32 Obi.ObiParticleGroup::get_Count()
extern void ObiParticleGroup_get_Count_m98490363FF19DE1FFEBC361D7302AEF05B6F21B4 (void);
// 0x00000696 System.Boolean Obi.ObiParticleGroup::ContainsParticle(System.Int32)
extern void ObiParticleGroup_ContainsParticle_mFF1BABB2B3D4D3CD4AE1E5B44C3DC83765EB11AC (void);
// 0x00000697 System.Void Obi.ObiParticleGroup::.ctor()
extern void ObiParticleGroup__ctor_mD6FDCA405D8CBEB22044CFD3635E3202AF1A45A2 (void);
// 0x00000698 System.Void Obi.ObiBoxShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.BoxCollider2D)
extern void ObiBoxShapeTracker2D__ctor_m628217C8133AD0BA22DE8815770C03CD31236C8F (void);
// 0x00000699 System.Boolean Obi.ObiBoxShapeTracker2D::UpdateIfNeeded()
extern void ObiBoxShapeTracker2D_UpdateIfNeeded_m7CF0C0561350D4A21F8FC768C7158E4BAE328434 (void);
// 0x0000069A System.Void Obi.ObiCapsuleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CapsuleCollider2D)
extern void ObiCapsuleShapeTracker2D__ctor_m9F0647AFDA77ACDB5B00DDA8F614F2CD3F61F464 (void);
// 0x0000069B System.Boolean Obi.ObiCapsuleShapeTracker2D::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker2D_UpdateIfNeeded_m65EF7C477B78DF113F5C43446E266DA9790B333A (void);
// 0x0000069C System.Void Obi.ObiCircleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CircleCollider2D)
extern void ObiCircleShapeTracker2D__ctor_m91428FB089519F84073171B1EF2AF27FA7029F13 (void);
// 0x0000069D System.Boolean Obi.ObiCircleShapeTracker2D::UpdateIfNeeded()
extern void ObiCircleShapeTracker2D_UpdateIfNeeded_m855CD60D865E47BD776A38CE2F44554BD5B69E49 (void);
// 0x0000069E System.Void Obi.ObiEdgeShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.EdgeCollider2D)
extern void ObiEdgeShapeTracker2D__ctor_m9CF5E0F349BC73573202F01E32482578DE1C1A10 (void);
// 0x0000069F System.Void Obi.ObiEdgeShapeTracker2D::UpdateEdgeData()
extern void ObiEdgeShapeTracker2D_UpdateEdgeData_m7208F5B1FF03775D5A425836047FB54E9BB747C1 (void);
// 0x000006A0 System.Boolean Obi.ObiEdgeShapeTracker2D::UpdateIfNeeded()
extern void ObiEdgeShapeTracker2D_UpdateIfNeeded_m380635280878C5D86F90898E11CAD1664E3E4BDC (void);
// 0x000006A1 System.Void Obi.ObiEdgeShapeTracker2D::Destroy()
extern void ObiEdgeShapeTracker2D_Destroy_m01DDDC54A11D898BAABA7391098DD4D2458CA946 (void);
// 0x000006A2 System.Void Obi.ObiBoxShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.BoxCollider)
extern void ObiBoxShapeTracker__ctor_mE75CAF1F86BB53D6AEF98085C09D17E66DEEF582 (void);
// 0x000006A3 System.Boolean Obi.ObiBoxShapeTracker::UpdateIfNeeded()
extern void ObiBoxShapeTracker_UpdateIfNeeded_mF819B8E4C30C6AFAF29A4117798FDF1316EA834A (void);
// 0x000006A4 System.Void Obi.ObiCapsuleShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CapsuleCollider)
extern void ObiCapsuleShapeTracker__ctor_mBE6839D0297E242539ABACA7E8D10CC7D1FF90BD (void);
// 0x000006A5 System.Boolean Obi.ObiCapsuleShapeTracker::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker_UpdateIfNeeded_mE44AC47537FB4437697D85609FB76AC306E29C1A (void);
// 0x000006A6 System.Void Obi.ObiCharacterControllerShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CharacterController)
extern void ObiCharacterControllerShapeTracker__ctor_m58178DB03B830C4137C4E297650BAB50EFD3625E (void);
// 0x000006A7 System.Boolean Obi.ObiCharacterControllerShapeTracker::UpdateIfNeeded()
extern void ObiCharacterControllerShapeTracker_UpdateIfNeeded_m37B806A39DB2EEF9B54A5B5F191B7F9C7602E517 (void);
// 0x000006A8 System.Void Obi.ObiDistanceFieldShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.Component,Obi.ObiDistanceField)
extern void ObiDistanceFieldShapeTracker__ctor_mA5B4583A9547EB0FE0C666EBB88E671DEC7F9035 (void);
// 0x000006A9 System.Void Obi.ObiDistanceFieldShapeTracker::UpdateDistanceFieldData()
extern void ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_mA701C6AE3562FD84B9C2C56A60A80A88D8B961E7 (void);
// 0x000006AA System.Boolean Obi.ObiDistanceFieldShapeTracker::UpdateIfNeeded()
extern void ObiDistanceFieldShapeTracker_UpdateIfNeeded_mA4E6477716FF64D462625E6D8F4898A7E47BD384 (void);
// 0x000006AB System.Void Obi.ObiDistanceFieldShapeTracker::Destroy()
extern void ObiDistanceFieldShapeTracker_Destroy_mF2D78CB9BA87BFF2D01E82CA3796B3E975B387C3 (void);
// 0x000006AC System.Void Obi.ObiMeshShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.MeshCollider)
extern void ObiMeshShapeTracker__ctor_m08CD689CF8203745944B75624F6F10B9B5991B41 (void);
// 0x000006AD System.Void Obi.ObiMeshShapeTracker::UpdateMeshData()
extern void ObiMeshShapeTracker_UpdateMeshData_m3FE402BFC2EA1B1AEF97C10345A781D1F1453374 (void);
// 0x000006AE System.Boolean Obi.ObiMeshShapeTracker::UpdateIfNeeded()
extern void ObiMeshShapeTracker_UpdateIfNeeded_m12F1895305D96BA383B3C6D04D0960CDE65BB9DD (void);
// 0x000006AF System.Void Obi.ObiMeshShapeTracker::Destroy()
extern void ObiMeshShapeTracker_Destroy_mF175565DFFD64344104928752D528653AEEF4C1D (void);
// 0x000006B0 System.Void Obi.ObiShapeTracker::Destroy()
extern void ObiShapeTracker_Destroy_m0FF375956D9FB5363A7595568F62FC49288D102A (void);
// 0x000006B1 System.Boolean Obi.ObiShapeTracker::UpdateIfNeeded()
// 0x000006B2 System.Void Obi.ObiShapeTracker::.ctor()
extern void ObiShapeTracker__ctor_mD3C85F1D395CDC2FA8F0FADBF02F5035493C1A7C (void);
// 0x000006B3 System.Void Obi.ObiSphereShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.SphereCollider)
extern void ObiSphereShapeTracker__ctor_m1F91597098DFDE9D3D331BB46F9885EF84816FCC (void);
// 0x000006B4 System.Boolean Obi.ObiSphereShapeTracker::UpdateIfNeeded()
extern void ObiSphereShapeTracker_UpdateIfNeeded_m956A2557E0849CFD2C667CED772595BB3DB2517F (void);
// 0x000006B5 System.Void Obi.ObiTerrainShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.TerrainCollider)
extern void ObiTerrainShapeTracker__ctor_m7BEE7A899503A6A8D1B542BB48B2902966AB22B6 (void);
// 0x000006B6 System.Void Obi.ObiTerrainShapeTracker::UpdateHeightData()
extern void ObiTerrainShapeTracker_UpdateHeightData_mF979C90B8BF4BAA1D77BB55829060BE1D02DB1FB (void);
// 0x000006B7 System.Boolean Obi.ObiTerrainShapeTracker::UpdateIfNeeded()
extern void ObiTerrainShapeTracker_UpdateIfNeeded_mF4287DBBCE367005E4F1EF1342AB50882CDBE460 (void);
// 0x000006B8 System.Void Obi.ObiTerrainShapeTracker::Destroy()
extern void ObiTerrainShapeTracker_Destroy_m778990BC8F8E80F2EAD34786597F7803DA9E55EE (void);
// 0x000006B9 System.Void Obi.ObiCollider::set_sourceCollider(UnityEngine.Collider)
extern void ObiCollider_set_sourceCollider_mE74FCFF39705097FEAD7580CD74ADA13071B3CB1 (void);
// 0x000006BA UnityEngine.Collider Obi.ObiCollider::get_sourceCollider()
extern void ObiCollider_get_sourceCollider_m9128E7F1E524ACFDD350E7347570A6253DFDB19D (void);
// 0x000006BB System.Void Obi.ObiCollider::set_distanceField(Obi.ObiDistanceField)
extern void ObiCollider_set_distanceField_m1E7758FFA96690595BB90B1BEB48B7AD958EB53F (void);
// 0x000006BC Obi.ObiDistanceField Obi.ObiCollider::get_distanceField()
extern void ObiCollider_get_distanceField_mC9798980E3932FBA4649A14AB780CA819D06B050 (void);
// 0x000006BD System.Void Obi.ObiCollider::CreateTracker()
extern void ObiCollider_CreateTracker_mD3862A5A96ACE781C43578C71C7F3B34597B72B5 (void);
// 0x000006BE UnityEngine.Component Obi.ObiCollider::GetUnityCollider(System.Boolean&)
extern void ObiCollider_GetUnityCollider_m8F9125F3CE4C77B54A293FCB58A506F9003F1645 (void);
// 0x000006BF System.Void Obi.ObiCollider::FindSourceCollider()
extern void ObiCollider_FindSourceCollider_m637BA0D9C7416963F07A6C91C46898E63A2AA7E6 (void);
// 0x000006C0 System.Void Obi.ObiCollider::.ctor()
extern void ObiCollider__ctor_mC40B217031806CE6CC171DB8519F2BDC13237937 (void);
// 0x000006C1 System.Void Obi.ObiCollider2D::set_SourceCollider(UnityEngine.Collider2D)
extern void ObiCollider2D_set_SourceCollider_m1018D5A5D84BEE9BFF223E7C96DFEF6391A61025 (void);
// 0x000006C2 UnityEngine.Collider2D Obi.ObiCollider2D::get_SourceCollider()
extern void ObiCollider2D_get_SourceCollider_m0F0FA6FE50C824AA1D508B0624CFC78BC5CB1324 (void);
// 0x000006C3 System.Void Obi.ObiCollider2D::CreateTracker()
extern void ObiCollider2D_CreateTracker_mF85B5C235CC7FD0AA628697696F24E75B0677D95 (void);
// 0x000006C4 UnityEngine.Component Obi.ObiCollider2D::GetUnityCollider(System.Boolean&)
extern void ObiCollider2D_GetUnityCollider_mC7022D7CE3DD5DAFC483FE44489C1826C6F719A9 (void);
// 0x000006C5 System.Void Obi.ObiCollider2D::FindSourceCollider()
extern void ObiCollider2D_FindSourceCollider_m1C3542EAB959FFB8DD376825B89F1FCF6EF5B71A (void);
// 0x000006C6 System.Void Obi.ObiCollider2D::.ctor()
extern void ObiCollider2D__ctor_m1D8E050EE358DD5E91FBCBC06CEEA010D1096877 (void);
// 0x000006C7 System.Void Obi.ObiColliderBase::set_CollisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiColliderBase_set_CollisionMaterial_m79AC8EC4C2B24D56D2EEB8E7E3EC8758D71DBCEF (void);
// 0x000006C8 Obi.ObiCollisionMaterial Obi.ObiColliderBase::get_CollisionMaterial()
extern void ObiColliderBase_get_CollisionMaterial_m0DF69ECB8E1D3B265A93C3411558938452320DB8 (void);
// 0x000006C9 System.Void Obi.ObiColliderBase::set_Filter(System.Int32)
extern void ObiColliderBase_set_Filter_m7EFACC9D0DDC53860DD8EC7711CA2B537861B143 (void);
// 0x000006CA System.Int32 Obi.ObiColliderBase::get_Filter()
extern void ObiColliderBase_get_Filter_mC113E8E2C6A721BDC36A1380CDA7B1274877FA9F (void);
// 0x000006CB System.Void Obi.ObiColliderBase::set_Thickness(System.Single)
extern void ObiColliderBase_set_Thickness_m8B643679A1B8F3502E0983283BFAFC788F59FED7 (void);
// 0x000006CC System.Single Obi.ObiColliderBase::get_Thickness()
extern void ObiColliderBase_get_Thickness_m39EFFE2661BFC77996AFE413C11C9842813B0CCD (void);
// 0x000006CD Obi.ObiShapeTracker Obi.ObiColliderBase::get_Tracker()
extern void ObiColliderBase_get_Tracker_mC248A17400AE5158EBE13D85059023F2777EF96E (void);
// 0x000006CE Obi.ObiColliderHandle Obi.ObiColliderBase::get_Handle()
extern void ObiColliderBase_get_Handle_mC54EFB3D8F0D65205AECE01C1523F9684610FDF9 (void);
// 0x000006CF System.IntPtr Obi.ObiColliderBase::get_OniCollider()
extern void ObiColliderBase_get_OniCollider_m1A453A14EEE1A71AD174EA6FD490CBF11FB13516 (void);
// 0x000006D0 Obi.ObiRigidbodyBase Obi.ObiColliderBase::get_Rigidbody()
extern void ObiColliderBase_get_Rigidbody_mFE29C0B5E72DE47B2F8CECC10D691E3CB1113260 (void);
// 0x000006D1 System.Void Obi.ObiColliderBase::CreateTracker()
// 0x000006D2 UnityEngine.Component Obi.ObiColliderBase::GetUnityCollider(System.Boolean&)
// 0x000006D3 System.Void Obi.ObiColliderBase::FindSourceCollider()
// 0x000006D4 System.Void Obi.ObiColliderBase::CreateRigidbody()
extern void ObiColliderBase_CreateRigidbody_m28447722765BD28A1F74FD197E44CD73585CFA55 (void);
// 0x000006D5 System.Void Obi.ObiColliderBase::OnTransformParentChanged()
extern void ObiColliderBase_OnTransformParentChanged_mB24B89C5A1D09A5CDE05559DDDE3AE584CB45385 (void);
// 0x000006D6 System.Void Obi.ObiColliderBase::AddCollider()
extern void ObiColliderBase_AddCollider_mAE172D1DDA35D52A9D36D68E68CB739157F34870 (void);
// 0x000006D7 System.Void Obi.ObiColliderBase::RemoveCollider()
extern void ObiColliderBase_RemoveCollider_m95F3C0AD19C848FFF82DF5DB8584D5E949F808DC (void);
// 0x000006D8 System.Void Obi.ObiColliderBase::UpdateIfNeeded()
extern void ObiColliderBase_UpdateIfNeeded_mF9F82931F7D1F7703B77125BB3612E1781EF4622 (void);
// 0x000006D9 System.Void Obi.ObiColliderBase::OnEnable()
extern void ObiColliderBase_OnEnable_m9D06C6503D3AA87D2ED910B831C6927655449D5C (void);
// 0x000006DA System.Void Obi.ObiColliderBase::OnDisable()
extern void ObiColliderBase_OnDisable_mDCA28DAF071BB2F8AB35E5A290B1935BB47F320F (void);
// 0x000006DB System.Void Obi.ObiColliderBase::.ctor()
extern void ObiColliderBase__ctor_m0425A5B3CFADDE3908C4CDE7564045D2E49156C6 (void);
// 0x000006DC System.Boolean Obi.ObiResourceHandle`1::get_isValid()
// 0x000006DD System.Void Obi.ObiResourceHandle`1::Invalidate()
// 0x000006DE System.Void Obi.ObiResourceHandle`1::Reference()
// 0x000006DF System.Boolean Obi.ObiResourceHandle`1::Dereference()
// 0x000006E0 System.Void Obi.ObiResourceHandle`1::.ctor(System.Int32)
// 0x000006E1 System.Void Obi.ObiColliderHandle::.ctor(System.Int32)
extern void ObiColliderHandle__ctor_m928271CA0B5407C025BBCC1A4C1B41A772842B98 (void);
// 0x000006E2 System.Void Obi.ObiCollisionMaterialHandle::.ctor(System.Int32)
extern void ObiCollisionMaterialHandle__ctor_mF9EF7E13E41C1CFA35A66984FEAE0EA7A6E01622 (void);
// 0x000006E3 System.Void Obi.ObiRigidbodyHandle::.ctor(System.Int32)
extern void ObiRigidbodyHandle__ctor_m7A187DF1938668726DB0353C6DF5A404B9661FDB (void);
// 0x000006E4 Obi.ObiColliderWorld Obi.ObiColliderWorld::GetInstance()
extern void ObiColliderWorld_GetInstance_mDCD3CE9448DD243AF9314FC3AE6EFF25A0A9D804 (void);
// 0x000006E5 System.Void Obi.ObiColliderWorld::Initialize()
extern void ObiColliderWorld_Initialize_mA686CF0330B087A58794A38B02A5C474C0ACE80D (void);
// 0x000006E6 System.Void Obi.ObiColliderWorld::Destroy()
extern void ObiColliderWorld_Destroy_mB2941E38388F04B017C98D209866BF6234D85C66 (void);
// 0x000006E7 System.Void Obi.ObiColliderWorld::DestroyIfUnused()
extern void ObiColliderWorld_DestroyIfUnused_mF7FB902FE93CE094938D57DC0048249F810AFAB0 (void);
// 0x000006E8 System.Void Obi.ObiColliderWorld::RegisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_RegisterImplementation_m04E6D07B4FE9DB89CB602DDE1177100646AB5F80 (void);
// 0x000006E9 System.Void Obi.ObiColliderWorld::UnregisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_UnregisterImplementation_m1FB0CC2F87786339836A3FE23662CE7E87AFD313 (void);
// 0x000006EA Obi.ObiColliderHandle Obi.ObiColliderWorld::CreateCollider()
extern void ObiColliderWorld_CreateCollider_m209298226F546EF09F80E9DC3C9CEDB9EA6A3ECF (void);
// 0x000006EB Obi.ObiRigidbodyHandle Obi.ObiColliderWorld::CreateRigidbody()
extern void ObiColliderWorld_CreateRigidbody_m1A6A020A8B5E9F2333767813122FAD64F13E58C7 (void);
// 0x000006EC Obi.ObiCollisionMaterialHandle Obi.ObiColliderWorld::CreateCollisionMaterial()
extern void ObiColliderWorld_CreateCollisionMaterial_m88809726D6F5777CA5CA979375202E079FBA523F (void);
// 0x000006ED Obi.ObiTriangleMeshHandle Obi.ObiColliderWorld::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiColliderWorld_GetOrCreateTriangleMesh_m68BA906C938414D9FA39EF9434D16EDD5BC6B008 (void);
// 0x000006EE System.Void Obi.ObiColliderWorld::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiColliderWorld_DestroyTriangleMesh_m0BAE4F6D11B77B8B13C2D4C1DE5A6140E76E582C (void);
// 0x000006EF Obi.ObiEdgeMeshHandle Obi.ObiColliderWorld::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiColliderWorld_GetOrCreateEdgeMesh_mD3188273479E4DC678356643F456C0AA97BFBD1B (void);
// 0x000006F0 System.Void Obi.ObiColliderWorld::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiColliderWorld_DestroyEdgeMesh_mC35DD757DCFE047FCE86C3190EEFA0B19A891306 (void);
// 0x000006F1 Obi.ObiDistanceFieldHandle Obi.ObiColliderWorld::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiColliderWorld_GetOrCreateDistanceField_mD90630455B72C5BAD3E94DAE4F3FE4E979E2C431 (void);
// 0x000006F2 System.Void Obi.ObiColliderWorld::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiColliderWorld_DestroyDistanceField_mB645511AF520AEF6AB93B8EE03F0A2A7D62ECEE8 (void);
// 0x000006F3 Obi.ObiHeightFieldHandle Obi.ObiColliderWorld::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiColliderWorld_GetOrCreateHeightField_m67768E9620255D9407B6D664A21B5F46A06DA9DD (void);
// 0x000006F4 System.Void Obi.ObiColliderWorld::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiColliderWorld_DestroyHeightField_mEDED727FD4CD60E71555E1F0B7223CF0C2F060D7 (void);
// 0x000006F5 System.Void Obi.ObiColliderWorld::DestroyCollider(Obi.ObiColliderHandle)
extern void ObiColliderWorld_DestroyCollider_m834E6E9C211C47C4746C0658D072D11488444C7D (void);
// 0x000006F6 System.Void Obi.ObiColliderWorld::DestroyRigidbody(Obi.ObiRigidbodyHandle)
extern void ObiColliderWorld_DestroyRigidbody_m06DB0A63D4471A043E38A1BFEC429E693E36E82B (void);
// 0x000006F7 System.Void Obi.ObiColliderWorld::DestroyCollisionMaterial(Obi.ObiCollisionMaterialHandle)
extern void ObiColliderWorld_DestroyCollisionMaterial_m95080D33292635B836E68FDF7DBD877FF51A3302 (void);
// 0x000006F8 System.Void Obi.ObiColliderWorld::UpdateColliders()
extern void ObiColliderWorld_UpdateColliders_mFE5C2781107A2BF876523BC7D1A371930FA231DA (void);
// 0x000006F9 System.Void Obi.ObiColliderWorld::UpdateRigidbodies(System.Collections.Generic.List`1<Obi.ObiSolver>,System.Single)
extern void ObiColliderWorld_UpdateRigidbodies_m8561FC0652FF0142400D2886A02C9EFAEB685819 (void);
// 0x000006FA System.Void Obi.ObiColliderWorld::UpdateWorld(System.Single)
extern void ObiColliderWorld_UpdateWorld_m864B8D4C74E8309F9FA29064D39B1A37FCC9538D (void);
// 0x000006FB System.Void Obi.ObiColliderWorld::UpdateRigidbodyVelocities(System.Collections.Generic.List`1<Obi.ObiSolver>)
extern void ObiColliderWorld_UpdateRigidbodyVelocities_m98240C979197C4339F335E642B3DF480F7A281E3 (void);
// 0x000006FC System.Void Obi.ObiColliderWorld::.ctor()
extern void ObiColliderWorld__ctor_mB72724E3C4F8ABB4A7845EA540DF7FE796E1E971 (void);
// 0x000006FD Obi.ObiCollisionMaterialHandle Obi.ObiCollisionMaterial::get_handle()
extern void ObiCollisionMaterial_get_handle_m594CB055313A49F8543490E78B1F48E7C21C7462 (void);
// 0x000006FE System.Void Obi.ObiCollisionMaterial::OnEnable()
extern void ObiCollisionMaterial_OnEnable_m8FA0F5FEF8BDA6E093C8A903CD09B45F107B87F3 (void);
// 0x000006FF System.Void Obi.ObiCollisionMaterial::OnDisable()
extern void ObiCollisionMaterial_OnDisable_m835688D4697C04CFE7B9F09ECBA5E0DD0201EE4E (void);
// 0x00000700 System.Void Obi.ObiCollisionMaterial::OnValidate()
extern void ObiCollisionMaterial_OnValidate_m39E140FEFC698F6790902D5698BCCDAA4C037250 (void);
// 0x00000701 System.Void Obi.ObiCollisionMaterial::UpdateMaterial()
extern void ObiCollisionMaterial_UpdateMaterial_m01C9A16AD952A5AC4E018F035CD98AD7C3F7D6F2 (void);
// 0x00000702 System.Void Obi.ObiCollisionMaterial::CreateMaterialIfNeeded()
extern void ObiCollisionMaterial_CreateMaterialIfNeeded_m7B6E786F93918CFCBCAFE79D73C6D39DB7D2E467 (void);
// 0x00000703 System.Void Obi.ObiCollisionMaterial::.ctor()
extern void ObiCollisionMaterial__ctor_m72400798714AEFF19085712777080ABFBB13BAC6 (void);
// 0x00000704 System.Boolean Obi.ObiDistanceField::get_Initialized()
extern void ObiDistanceField_get_Initialized_mAABFFF3B58AB387E5CB5FB8C1AB746B0247F983C (void);
// 0x00000705 UnityEngine.Bounds Obi.ObiDistanceField::get_FieldBounds()
extern void ObiDistanceField_get_FieldBounds_m9863DC2138A3DBE77E04380C54236345A1133CBD (void);
// 0x00000706 System.Single Obi.ObiDistanceField::get_EffectiveSampleSize()
extern void ObiDistanceField_get_EffectiveSampleSize_mB9F3DB29086DB4AD2249B28C1C8C888484833AAD (void);
// 0x00000707 System.Void Obi.ObiDistanceField::set_InputMesh(UnityEngine.Mesh)
extern void ObiDistanceField_set_InputMesh_mD2BFC4B7BAE4B8EBEFF52AC8E89CF319F03D3780 (void);
// 0x00000708 UnityEngine.Mesh Obi.ObiDistanceField::get_InputMesh()
extern void ObiDistanceField_get_InputMesh_m9B9ABB7CAE379468D3FACDBD886581F843FA22B5 (void);
// 0x00000709 System.Void Obi.ObiDistanceField::Reset()
extern void ObiDistanceField_Reset_m00EC5397C61DF8D42A0E2201EBE65C30AB391DF8 (void);
// 0x0000070A System.Collections.IEnumerator Obi.ObiDistanceField::Generate()
extern void ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266 (void);
// 0x0000070B UnityEngine.Texture3D Obi.ObiDistanceField::GetVolumeTexture(System.Int32)
extern void ObiDistanceField_GetVolumeTexture_m3C63485A3F2BB3E0D84E496DFD190836DD7C9A64 (void);
// 0x0000070C System.Void Obi.ObiDistanceField::.ctor()
extern void ObiDistanceField__ctor_m79451FE12A75F2ED6B511F1DFC382E6493E09DB2 (void);
// 0x0000070D System.Void Obi.ObiDistanceField/<Generate>d__16::.ctor(System.Int32)
extern void U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8 (void);
// 0x0000070E System.Void Obi.ObiDistanceField/<Generate>d__16::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73 (void);
// 0x0000070F System.Boolean Obi.ObiDistanceField/<Generate>d__16::MoveNext()
extern void U3CGenerateU3Ed__16_MoveNext_mE09C7584F62ED7F884B6289D784C5474BAB6648D (void);
// 0x00000710 System.Object Obi.ObiDistanceField/<Generate>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1 (void);
// 0x00000711 System.Void Obi.ObiDistanceField/<Generate>d__16::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB (void);
// 0x00000712 System.Object Obi.ObiDistanceField/<Generate>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC (void);
// 0x00000713 System.Void Obi.ObiDistanceFieldHandle::.ctor(Obi.ObiDistanceField,System.Int32)
extern void ObiDistanceFieldHandle__ctor_m6E0E72314F9B3CD649F6E52743A19A05BDD53486 (void);
// 0x00000714 System.Void Obi.DistanceFieldHeader::.ctor(System.Int32,System.Int32)
extern void DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0 (void);
// 0x00000715 System.Void Obi.ObiDistanceFieldContainer::.ctor()
extern void ObiDistanceFieldContainer__ctor_m3770E97A1E94FE7F79D6C4476A4B5538615321FC (void);
// 0x00000716 Obi.ObiDistanceFieldHandle Obi.ObiDistanceFieldContainer::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiDistanceFieldContainer_GetOrCreateDistanceField_mACCA0F4E84E3D53991F18FEC18CCC8230451C913 (void);
// 0x00000717 System.Void Obi.ObiDistanceFieldContainer::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiDistanceFieldContainer_DestroyDistanceField_m85914D4DE23293E844CC8082A6B7B4F427C8505B (void);
// 0x00000718 System.Void Obi.ObiDistanceFieldContainer::Dispose()
extern void ObiDistanceFieldContainer_Dispose_m7A92A1441B9CB196E1D25BF4587B01FDFF2CA2A8 (void);
// 0x00000719 System.Void Obi.Edge::.ctor(System.Int32,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern void Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088 (void);
// 0x0000071A Obi.Aabb Obi.Edge::GetBounds()
extern void Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16 (void);
// 0x0000071B System.Void Obi.ObiEdgeMeshHandle::.ctor(UnityEngine.EdgeCollider2D,System.Int32)
extern void ObiEdgeMeshHandle__ctor_m055ABDB8025F78423ED6926D1E316EB48FABC4AE (void);
// 0x0000071C System.Void Obi.EdgeMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124 (void);
// 0x0000071D System.Void Obi.ObiEdgeMeshContainer::.ctor()
extern void ObiEdgeMeshContainer__ctor_m7E7182023FCAB6FD0840A93D3F33CD76868DEC2D (void);
// 0x0000071E Obi.ObiEdgeMeshHandle Obi.ObiEdgeMeshContainer::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m08E80D7BB9E4C1AD3040ECCB3E3F80BB0EBC5127 (void);
// 0x0000071F System.Void Obi.ObiEdgeMeshContainer::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiEdgeMeshContainer_DestroyEdgeMesh_m6A49B7BD3DACF76A1B0E7F591A523795F55758EC (void);
// 0x00000720 System.Void Obi.ObiEdgeMeshContainer::Dispose()
extern void ObiEdgeMeshContainer_Dispose_m1005586E5D7BE477D43008737857278C3088223A (void);
// 0x00000721 System.Void Obi.ObiEdgeMeshContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_m319B04707AB4F191DD80BB15D8CE099EE0A1A7B4 (void);
// 0x00000722 System.Void Obi.ObiEdgeMeshContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m0A7F956BDB67E4A8C2BCD9D83C276D02DC561279 (void);
// 0x00000723 Obi.Edge Obi.ObiEdgeMeshContainer/<>c::<GetOrCreateEdgeMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_mDEAC05CDAF4D92AFBB301FED8F1C6099004401DD (void);
// 0x00000724 System.Void Obi.ObiHeightFieldHandle::.ctor(UnityEngine.TerrainData,System.Int32)
extern void ObiHeightFieldHandle__ctor_m7487BDAEEC65A9BBFE0A38658CA884E309BC6FEA (void);
// 0x00000725 System.Void Obi.HeightFieldHeader::.ctor(System.Int32,System.Int32)
extern void HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421 (void);
// 0x00000726 System.Void Obi.ObiHeightFieldContainer::.ctor()
extern void ObiHeightFieldContainer__ctor_m5666C4D77395B12D8510DE3C1C91F7DF7080917E (void);
// 0x00000727 Obi.ObiHeightFieldHandle Obi.ObiHeightFieldContainer::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiHeightFieldContainer_GetOrCreateHeightField_m24A1A0266B0B0558347D317ECAF37AE00CF5F73D (void);
// 0x00000728 System.Void Obi.ObiHeightFieldContainer::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiHeightFieldContainer_DestroyHeightField_m0C864AEC3E3DF063366196C10432FC0E9582C124 (void);
// 0x00000729 System.Void Obi.ObiHeightFieldContainer::Dispose()
extern void ObiHeightFieldContainer_Dispose_m381A36CB411FE4B112A455EB52BEF459C07B542E (void);
// 0x0000072A System.Void Obi.ObiRigidbody::OnEnable()
extern void ObiRigidbody_OnEnable_m9C2FD7B18650A2752E66572F0470C3B0C4F31649 (void);
// 0x0000072B System.Void Obi.ObiRigidbody::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody_UpdateKinematicVelocities_mCB621E407E54750602D501EF243278687D054900 (void);
// 0x0000072C System.Void Obi.ObiRigidbody::UpdateIfNeeded(System.Single)
extern void ObiRigidbody_UpdateIfNeeded_mE8E08B343D9013FC5168AC526FDD2FD60713C63F (void);
// 0x0000072D System.Void Obi.ObiRigidbody::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody_UpdateVelocities_m8383481CAC453BA89BF272E3DA3AC19A575ED21F (void);
// 0x0000072E System.Void Obi.ObiRigidbody::.ctor()
extern void ObiRigidbody__ctor_mD7D0C954158E4444C3068E83B3327DF0C19F22B4 (void);
// 0x0000072F System.Void Obi.ObiRigidbody2D::OnEnable()
extern void ObiRigidbody2D_OnEnable_m250ABC526CD54A0CE3FD5924DBB5168CDFDE1233 (void);
// 0x00000730 System.Void Obi.ObiRigidbody2D::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody2D_UpdateKinematicVelocities_m9FA8BC7541C8841BA05CEFB2E5153BEF8D0F0998 (void);
// 0x00000731 System.Void Obi.ObiRigidbody2D::UpdateIfNeeded(System.Single)
extern void ObiRigidbody2D_UpdateIfNeeded_m2B7DDB040171C3260191C90FCA6E67EA1658C21F (void);
// 0x00000732 System.Void Obi.ObiRigidbody2D::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody2D_UpdateVelocities_m9E81C9B71FCBB4C7F67940267609771648B6FC02 (void);
// 0x00000733 System.Void Obi.ObiRigidbody2D::.ctor()
extern void ObiRigidbody2D__ctor_m4B1B8A6B4DC629D021CE278C6D8926801ABF2B5A (void);
// 0x00000734 System.Void Obi.ObiRigidbodyBase::OnEnable()
extern void ObiRigidbodyBase_OnEnable_m0DB75700CD39CA195D17CE1222B274FCF94F0484 (void);
// 0x00000735 System.Void Obi.ObiRigidbodyBase::OnDisable()
extern void ObiRigidbodyBase_OnDisable_mA49D0E74F0FFE4A33FF74D3DF8ED1BC97F92BC9E (void);
// 0x00000736 System.Void Obi.ObiRigidbodyBase::UpdateIfNeeded(System.Single)
// 0x00000737 System.Void Obi.ObiRigidbodyBase::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
// 0x00000738 System.Void Obi.ObiRigidbodyBase::.ctor()
extern void ObiRigidbodyBase__ctor_m97C098719F7244A8923BA99EB88178D5BC81E375 (void);
// 0x00000739 System.Void Obi.Triangle::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E (void);
// 0x0000073A Obi.Aabb Obi.Triangle::GetBounds()
extern void Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D (void);
// 0x0000073B System.Void Obi.ObiTriangleMeshHandle::.ctor(UnityEngine.Mesh,System.Int32)
extern void ObiTriangleMeshHandle__ctor_m4C8FBE34410F6DE0216D655610C66D865609FD82 (void);
// 0x0000073C System.Void Obi.TriangleMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4 (void);
// 0x0000073D System.Void Obi.ObiTriangleMeshContainer::.ctor()
extern void ObiTriangleMeshContainer__ctor_m7744CBB6264FB551DA0E62E0AE2B14750075FAFC (void);
// 0x0000073E Obi.ObiTriangleMeshHandle Obi.ObiTriangleMeshContainer::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m7966A3DEF57A37FDCD4EAB1254350FCF3C443C54 (void);
// 0x0000073F System.Void Obi.ObiTriangleMeshContainer::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiTriangleMeshContainer_DestroyTriangleMesh_mC9C784E4C7DD7154C4F28D56E3D49B0CCFDFA4A8 (void);
// 0x00000740 System.Void Obi.ObiTriangleMeshContainer::Dispose()
extern void ObiTriangleMeshContainer_Dispose_m5B8C0A01AFA58E44642169B3B0230566C3901B80 (void);
// 0x00000741 System.Void Obi.ObiTriangleMeshContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE (void);
// 0x00000742 System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8 (void);
// 0x00000743 Obi.Triangle Obi.ObiTriangleMeshContainer/<>c::<GetOrCreateTriangleMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42 (void);
// 0x00000744 System.Collections.IEnumerator Obi.ASDF::Build(System.Single,System.Int32,UnityEngine.Vector3[],System.Int32[],System.Collections.Generic.List`1<Obi.DFNode>,System.Int32)
extern void ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4 (void);
// 0x00000745 System.Single Obi.ASDF::Sample(System.Collections.Generic.List`1<Obi.DFNode>,UnityEngine.Vector3)
extern void ASDF_Sample_mA68406A166BFA7E95D0C62E769CB30FC8F2C78C0 (void);
// 0x00000746 System.Void Obi.ASDF::.ctor()
extern void ASDF__ctor_mC6D0B839501A5338ADE298E18D27946D91973849 (void);
// 0x00000747 System.Void Obi.ASDF::.cctor()
extern void ASDF__cctor_m3BDC04F77E29E81E3A541CFA21851E2153E7EC1A (void);
// 0x00000748 System.Void Obi.ASDF/<>c::.cctor()
extern void U3CU3Ec__cctor_m02BD313FFAEEDA3D68EE3E46312C75EB44777102 (void);
// 0x00000749 System.Void Obi.ASDF/<>c::.ctor()
extern void U3CU3Ec__ctor_m4CEC5676823706F8C33924A47217BB2E3C584BB8 (void);
// 0x0000074A Obi.Triangle Obi.ASDF/<>c::<Build>b__3_0(Obi.IBounded)
extern void U3CU3Ec_U3CBuildU3Eb__3_0_m2F53BFE53C74A23E70A52AA23A3625EA199232CA (void);
// 0x0000074B System.Void Obi.ASDF/<Build>d__3::.ctor(System.Int32)
extern void U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3 (void);
// 0x0000074C System.Void Obi.ASDF/<Build>d__3::System.IDisposable.Dispose()
extern void U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4 (void);
// 0x0000074D System.Boolean Obi.ASDF/<Build>d__3::MoveNext()
extern void U3CBuildU3Ed__3_MoveNext_m2878CF634F624CD9D2E068EB30A27BFF838B1AE9 (void);
// 0x0000074E System.Object Obi.ASDF/<Build>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D (void);
// 0x0000074F System.Void Obi.ASDF/<Build>d__3::System.Collections.IEnumerator.Reset()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A (void);
// 0x00000750 System.Object Obi.ASDF/<Build>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B (void);
// 0x00000751 System.Void Obi.DFNode::.ctor(UnityEngine.Vector4)
extern void DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609 (void);
// 0x00000752 System.Single Obi.DFNode::Sample(UnityEngine.Vector3)
extern void DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3 (void);
// 0x00000753 UnityEngine.Vector3 Obi.DFNode::GetNormalizedPos(UnityEngine.Vector3)
extern void DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B (void);
// 0x00000754 System.Int32 Obi.DFNode::GetOctant(UnityEngine.Vector3)
extern void DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192 (void);
// 0x00000755 UnityEngine.Vector4 Obi.Aabb::get_center()
extern void Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113 (void);
// 0x00000756 UnityEngine.Vector4 Obi.Aabb::get_size()
extern void Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7 (void);
// 0x00000757 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4 (void);
// 0x00000758 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4)
extern void Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B (void);
// 0x00000759 System.Void Obi.Aabb::Encapsulate(UnityEngine.Vector4)
extern void Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08 (void);
// 0x0000075A System.Void Obi.Aabb::Encapsulate(Obi.Aabb)
extern void Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8 (void);
// 0x0000075B System.Void Obi.Aabb::FromBounds(UnityEngine.Bounds,System.Single,System.Boolean)
extern void Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664 (void);
// 0x0000075C System.Void Obi.AffineTransform::.ctor(UnityEngine.Vector4,UnityEngine.Quaternion,UnityEngine.Vector4)
extern void AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914 (void);
// 0x0000075D System.Void Obi.AffineTransform::FromTransform(UnityEngine.Transform,System.Boolean)
extern void AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB (void);
// 0x0000075E Obi.BIHNode[] Obi.BIH::Build(Obi.IBounded[]&,System.Int32,System.Single)
extern void BIH_Build_m543AD7EA6BD7D11FCF4DD88B06747F4B797784D9 (void);
// 0x0000075F System.Int32 Obi.BIH::HoarePartition(Obi.IBounded[],System.Int32,System.Int32,System.Single,Obi.BIHNode&,System.Int32)
extern void BIH_HoarePartition_m26E2AC749940517A897AF2963BE5322BB38A3CF9 (void);
// 0x00000760 System.Single Obi.BIH::DistanceToSurface(Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905 (void);
// 0x00000761 System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178 (void);
// 0x00000762 System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05 (void);
// 0x00000763 System.Void Obi.BIH::.ctor()
extern void BIH__ctor_m6D1B7878A01D15CA2622A366286C9308BFB07727 (void);
// 0x00000764 System.Single Obi.BIH::<DistanceToSurface>g__MinSignedDistance|4_0(System.Single,System.Single)
extern void BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F (void);
// 0x00000765 System.Void Obi.BIHNode::.ctor(System.Int32,System.Int32)
extern void BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A (void);
// 0x00000766 Obi.Aabb Obi.IBounded::GetBounds()
// 0x00000767 System.Void Obi.CellSpan::.ctor(Obi.VInt4,Obi.VInt4)
extern void CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B (void);
// 0x00000768 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB (void);
// 0x00000769 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody2D,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E (void);
// 0x0000076A System.Void Obi.CollisionMaterial::FromObiCollisionMaterial(Obi.ObiCollisionMaterial)
extern void CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530 (void);
// 0x0000076B System.Void Obi.ObiNativeAabbList::.ctor()
extern void ObiNativeAabbList__ctor_mCDE9CAA207D408AC843A08BBE600AB4F0D6B75D6 (void);
// 0x0000076C System.Void Obi.ObiNativeAabbList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAabbList__ctor_m59E302FAD9AEFD8A3BE4E2C51B322FB0420E1E2F (void);
// 0x0000076D System.Void Obi.ObiNativeAffineTransformList::.ctor()
extern void ObiNativeAffineTransformList__ctor_mA3B6D7D19DD4E487461C969B9C76DDCAF9564018 (void);
// 0x0000076E System.Void Obi.ObiNativeAffineTransformList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAffineTransformList__ctor_mCB63BE2A5D0F613E12C647669D05EB6BE52FC83E (void);
// 0x0000076F System.Void Obi.ObiNativeBIHNodeList::.ctor()
extern void ObiNativeBIHNodeList__ctor_m9615EC2E166FBD0154489436203D9AC793BFB907 (void);
// 0x00000770 System.Void Obi.ObiNativeBIHNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeBIHNodeList__ctor_m6CB7A19C221489E05D62A7996A07E287439F3D73 (void);
// 0x00000771 System.Void Obi.ObiNativeBoneWeightList::.ctor()
extern void ObiNativeBoneWeightList__ctor_m3412BBD683C143994278F1461106F9696B1D6835 (void);
// 0x00000772 System.Void Obi.ObiNativeBoneWeightList::.ctor(System.Int32,System.Int32)
extern void ObiNativeBoneWeightList__ctor_mCDE322908FD4B65971533ABDFB277CDB0DF40B49 (void);
// 0x00000773 System.Void Obi.ObiNativeByteList::.ctor()
extern void ObiNativeByteList__ctor_mE766091F60DF309D51D8C33F4C12AF80D4B9EB42 (void);
// 0x00000774 System.Void Obi.ObiNativeByteList::.ctor(System.Int32,System.Int32)
extern void ObiNativeByteList__ctor_mEA31E3AD637138D0161DB670D7DF7ACDC613B402 (void);
// 0x00000775 System.Void Obi.ObiNativeCellSpanList::.ctor()
extern void ObiNativeCellSpanList__ctor_m7A0292E9784EED2B0011E83FA5A7D22EC9DC9EEC (void);
// 0x00000776 System.Void Obi.ObiNativeCellSpanList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCellSpanList__ctor_m9810884859E402D959A6369EEBD22320DC40D2D6 (void);
// 0x00000777 System.Void Obi.ObiNativeColliderShapeList::.ctor()
extern void ObiNativeColliderShapeList__ctor_m9E21BF5F9EF261AE3C9C92E53F15264713584DC7 (void);
// 0x00000778 System.Void Obi.ObiNativeColliderShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeColliderShapeList__ctor_m79BC575A7059E83119DCE5DFC6D7CE8E308245C3 (void);
// 0x00000779 System.Void Obi.ObiNativeCollisionMaterialList::.ctor()
extern void ObiNativeCollisionMaterialList__ctor_m1C70BE9BA86B92206DB617392F15D7B8BDEC9597 (void);
// 0x0000077A System.Void Obi.ObiNativeCollisionMaterialList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCollisionMaterialList__ctor_m90089B05CB810F2C23D47F0077910F26FCC61888 (void);
// 0x0000077B System.Void Obi.ObiNativeContactShapeList::.ctor()
extern void ObiNativeContactShapeList__ctor_mD865C20C0CFC9AE7557CD251D78C03101CCF2695 (void);
// 0x0000077C System.Void Obi.ObiNativeContactShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeContactShapeList__ctor_mDB12EB79069ADCBD7B0F4377C8C5A1545F646DE8 (void);
// 0x0000077D System.Void Obi.ObiNativeDFNodeList::.ctor()
extern void ObiNativeDFNodeList__ctor_m9C5F3593C316319486AAF621BDD3A6C77A7AF4ED (void);
// 0x0000077E System.Void Obi.ObiNativeDFNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDFNodeList__ctor_mCDC4549E10B99A08FD83058EFD46F45299C71FEC (void);
// 0x0000077F System.Void Obi.ObiNativeDistanceFieldHeaderList::.ctor()
extern void ObiNativeDistanceFieldHeaderList__ctor_m12DE4CCCF75D20BB404B8083EF45503F964599EB (void);
// 0x00000780 System.Void Obi.ObiNativeDistanceFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDistanceFieldHeaderList__ctor_mF1E073829DBEAD5BC9ECC60BB9AF6463019282EC (void);
// 0x00000781 System.Void Obi.ObiNativeEdgeList::.ctor()
extern void ObiNativeEdgeList__ctor_m376EED5071F0D70AB6595F5C98AC9FB16FCCCF46 (void);
// 0x00000782 System.Void Obi.ObiNativeEdgeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeList__ctor_m1DFD5ED07389DD12179C9D3494C4E2BF3D62D563 (void);
// 0x00000783 System.Void Obi.ObiNativeEdgeMeshHeaderList::.ctor()
extern void ObiNativeEdgeMeshHeaderList__ctor_m7A49700958AB248E4ABC1CCD1FA83731241FD1DA (void);
// 0x00000784 System.Void Obi.ObiNativeEdgeMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeMeshHeaderList__ctor_m372AE7E0664D6BE77453AB0C52023FC77C720E8C (void);
// 0x00000785 System.Void Obi.ObiNativeFloatList::.ctor()
extern void ObiNativeFloatList__ctor_mB44D5F56545917B35AD1ECA717DF61D0C12047DC (void);
// 0x00000786 System.Void Obi.ObiNativeFloatList::.ctor(System.Int32,System.Int32)
extern void ObiNativeFloatList__ctor_m109DA72958D0EBB1AEF825F9A43C34F2BD1A42E5 (void);
// 0x00000787 System.Void Obi.ObiNativeHeightFieldHeaderList::.ctor()
extern void ObiNativeHeightFieldHeaderList__ctor_m0ED84840FACD5EF875EE422AD5274E42CE87D779 (void);
// 0x00000788 System.Void Obi.ObiNativeHeightFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeHeightFieldHeaderList__ctor_m5EC50E82D2C0FE9DBB46618885DE4D9A4FB490A1 (void);
// 0x00000789 System.Void Obi.ObiNativeInt4List::.ctor()
extern void ObiNativeInt4List__ctor_mAD172784A90BDA971380CFA6F7BECB381A268EA6 (void);
// 0x0000078A System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeInt4List__ctor_mF3060DFCCBC09AF993C3C96608B51FA83537EDC8 (void);
// 0x0000078B System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32,Obi.VInt4)
extern void ObiNativeInt4List__ctor_m24F99A45DDF792A152AF6A140C910F32C460E40A (void);
// 0x0000078C System.Void Obi.ObiNativeIntList::.ctor()
extern void ObiNativeIntList__ctor_mDF10DEDB2E6CE5F2D27135F263B2DB96A9123F0E (void);
// 0x0000078D System.Void Obi.ObiNativeIntList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntList__ctor_m25ADBFB84E370EE592FFF05D343541D84462FED6 (void);
// 0x0000078E System.Void Obi.ObiNativeIntPtrList::.ctor()
extern void ObiNativeIntPtrList__ctor_m2188368B2E04FE7C32F8D069405C9F483C27D465 (void);
// 0x0000078F System.Void Obi.ObiNativeIntPtrList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntPtrList__ctor_mE54F7AA88D3E5B4AD478C37640EDFAD145057FB8 (void);
// 0x00000790 System.Void Obi.ObiNativeList`1::set_count(System.Int32)
// 0x00000791 System.Int32 Obi.ObiNativeList`1::get_count()
// 0x00000792 System.Void Obi.ObiNativeList`1::set_capacity(System.Int32)
// 0x00000793 System.Int32 Obi.ObiNativeList`1::get_capacity()
// 0x00000794 System.Boolean Obi.ObiNativeList`1::get_isCreated()
// 0x00000795 T Obi.ObiNativeList`1::get_Item(System.Int32)
// 0x00000796 System.Void Obi.ObiNativeList`1::set_Item(System.Int32,T)
// 0x00000797 System.Void Obi.ObiNativeList`1::.ctor()
// 0x00000798 System.Void Obi.ObiNativeList`1::.ctor(System.Int32,System.Int32)
// 0x00000799 System.Void Obi.ObiNativeList`1::Finalize()
// 0x0000079A System.Void Obi.ObiNativeList`1::Dispose(System.Boolean)
// 0x0000079B System.Void Obi.ObiNativeList`1::Dispose()
// 0x0000079C System.Void Obi.ObiNativeList`1::OnBeforeSerialize()
// 0x0000079D System.Void Obi.ObiNativeList`1::OnAfterDeserialize()
// 0x0000079E Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray()
// 0x0000079F Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray(System.Int32)
// 0x000007A0 UnityEngine.ComputeBuffer Obi.ObiNativeList`1::AsComputeBuffer()
// 0x000007A1 System.Void Obi.ObiNativeList`1::ChangeCapacity(System.Int32)
// 0x000007A2 System.Boolean Obi.ObiNativeList`1::Compare(Obi.ObiNativeList`1<T>)
// 0x000007A3 System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>)
// 0x000007A4 System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>,System.Int32,System.Int32,System.Int32)
// 0x000007A5 System.Void Obi.ObiNativeList`1::CopyReplicate(T,System.Int32,System.Int32)
// 0x000007A6 System.Void Obi.ObiNativeList`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000007A7 System.Void Obi.ObiNativeList`1::Clear()
// 0x000007A8 System.Void Obi.ObiNativeList`1::Add(T)
// 0x000007A9 System.Void Obi.ObiNativeList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x000007AA System.Void Obi.ObiNativeList`1::RemoveRange(System.Int32,System.Int32)
// 0x000007AB System.Void Obi.ObiNativeList`1::RemoveAt(System.Int32)
// 0x000007AC System.Boolean Obi.ObiNativeList`1::ResizeUninitialized(System.Int32)
// 0x000007AD System.Boolean Obi.ObiNativeList`1::ResizeInitialized(System.Int32,T)
// 0x000007AE System.Boolean Obi.ObiNativeList`1::EnsureCapacity(System.Int32)
// 0x000007AF System.Void Obi.ObiNativeList`1::WipeToZero()
// 0x000007B0 System.String Obi.ObiNativeList`1::ToString()
// 0x000007B1 System.Void* Obi.ObiNativeList`1::AddressOfElement(System.Int32)
// 0x000007B2 System.IntPtr Obi.ObiNativeList`1::GetIntPtr()
// 0x000007B3 System.Void Obi.ObiNativeList`1::Swap(System.Int32,System.Int32)
// 0x000007B4 System.Collections.Generic.IEnumerator`1<T> Obi.ObiNativeList`1::GetEnumerator()
// 0x000007B5 System.Collections.IEnumerator Obi.ObiNativeList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000007B6 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__47::.ctor(System.Int32)
// 0x000007B7 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__47::System.IDisposable.Dispose()
// 0x000007B8 System.Boolean Obi.ObiNativeList`1/<GetEnumerator>d__47::MoveNext()
// 0x000007B9 T Obi.ObiNativeList`1/<GetEnumerator>d__47::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000007BA System.Void Obi.ObiNativeList`1/<GetEnumerator>d__47::System.Collections.IEnumerator.Reset()
// 0x000007BB System.Object Obi.ObiNativeList`1/<GetEnumerator>d__47::System.Collections.IEnumerator.get_Current()
// 0x000007BC System.Void Obi.ObiNativeMatrix4x4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeMatrix4x4List__ctor_m8315DA2799111EE4624C6AD421E912B9E2C0D775 (void);
// 0x000007BD System.Void Obi.ObiNativeQuaternionList::.ctor()
extern void ObiNativeQuaternionList__ctor_m07E6B8255331075366483E69997B1E8FCC8812EE (void);
// 0x000007BE System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQuaternionList__ctor_mF8798CFC21A168F77FDE7E4E433E4ED07BB42B0B (void);
// 0x000007BF System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32,UnityEngine.Quaternion)
extern void ObiNativeQuaternionList__ctor_m2733D11CD8396D239DBC16995D1E9404EA29AADA (void);
// 0x000007C0 System.Void Obi.ObiNativeQueryResultList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryResultList__ctor_m5B404B4FBEB82A5A119AC92DFC671C161598266D (void);
// 0x000007C1 System.Void Obi.ObiNativeQueryShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryShapeList__ctor_m1FC70B84E766982F194F6DC2FE87D15DA79493E6 (void);
// 0x000007C2 System.Void Obi.ObiNativeRigidbodyList::.ctor()
extern void ObiNativeRigidbodyList__ctor_mE10B19341E895E8DD38465DE2FC0E2EF5FEC1FA9 (void);
// 0x000007C3 System.Void Obi.ObiNativeRigidbodyList::.ctor(System.Int32,System.Int32)
extern void ObiNativeRigidbodyList__ctor_m21C628B6ADABDBDFA0D64D291A00D1BBC923B686 (void);
// 0x000007C4 System.Void Obi.ObiNativeTriangleList::.ctor()
extern void ObiNativeTriangleList__ctor_m5FDA4E153D9F3E8B6479666E321DDA194CAA2A25 (void);
// 0x000007C5 System.Void Obi.ObiNativeTriangleList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleList__ctor_m9FD16BE51A73FCC0ECD34FE1E3E60DE410644782 (void);
// 0x000007C6 System.Void Obi.ObiNativeTriangleMeshHeaderList::.ctor()
extern void ObiNativeTriangleMeshHeaderList__ctor_mDAD0784830EED0C0268F2009CEC90E04EB39B72C (void);
// 0x000007C7 System.Void Obi.ObiNativeTriangleMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleMeshHeaderList__ctor_m4F5742A78ACC6F92F44E484750F2710E45D32A8F (void);
// 0x000007C8 System.Void Obi.ObiNativeVector2List::.ctor()
extern void ObiNativeVector2List__ctor_mC9341AEC2A2B1610138A7763453BF7CDC2512299 (void);
// 0x000007C9 System.Void Obi.ObiNativeVector2List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector2List__ctor_m3326D8FF9523937A0D58CCEB0AA53A7F715AB740 (void);
// 0x000007CA System.Void Obi.ObiNativeVector3List::.ctor()
extern void ObiNativeVector3List__ctor_mC37EA7399E0090469F5014DE5EF277117A8B40E4 (void);
// 0x000007CB System.Void Obi.ObiNativeVector3List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector3List__ctor_m454CD1792A828386A51E51AC80EF30B4EC59DB25 (void);
// 0x000007CC System.Void Obi.ObiNativeVector4List::.ctor()
extern void ObiNativeVector4List__ctor_mC18F1ECE7AE0C5F9E3D080D41011CCE123F29C55 (void);
// 0x000007CD System.Void Obi.ObiNativeVector4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector4List__ctor_m4E5196643444C3F0983F4C1D35EC054085549834 (void);
// 0x000007CE UnityEngine.Vector3 Obi.ObiNativeVector4List::GetVector3(System.Int32)
extern void ObiNativeVector4List_GetVector3_m8EAE7F64879DD9A415D25E05C258ADEBC921A55D (void);
// 0x000007CF System.Void Obi.ObiNativeVector4List::SetVector3(System.Int32,UnityEngine.Vector3)
extern void ObiNativeVector4List_SetVector3_mEA4A6008125CDE1854000FBF64CEA0887E8A7D94 (void);
// 0x000007D0 System.Collections.Generic.IEnumerator`1<T> Obi.ObiList`1::GetEnumerator()
// 0x000007D1 System.Collections.IEnumerator Obi.ObiList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000007D2 System.Void Obi.ObiList`1::Add(T)
// 0x000007D3 System.Void Obi.ObiList`1::Clear()
// 0x000007D4 System.Boolean Obi.ObiList`1::Contains(T)
// 0x000007D5 System.Void Obi.ObiList`1::CopyTo(T[],System.Int32)
// 0x000007D6 System.Boolean Obi.ObiList`1::Remove(T)
// 0x000007D7 System.Int32 Obi.ObiList`1::get_Count()
// 0x000007D8 System.Boolean Obi.ObiList`1::get_IsReadOnly()
// 0x000007D9 System.Int32 Obi.ObiList`1::IndexOf(T)
// 0x000007DA System.Void Obi.ObiList`1::Insert(System.Int32,T)
// 0x000007DB System.Void Obi.ObiList`1::RemoveAt(System.Int32)
// 0x000007DC T Obi.ObiList`1::get_Item(System.Int32)
// 0x000007DD System.Void Obi.ObiList`1::set_Item(System.Int32,T)
// 0x000007DE T[] Obi.ObiList`1::get_Data()
// 0x000007DF System.Void Obi.ObiList`1::SetCount(System.Int32)
// 0x000007E0 System.Void Obi.ObiList`1::EnsureCapacity(System.Int32)
// 0x000007E1 System.Void Obi.ObiList`1::.ctor()
// 0x000007E2 System.Void Obi.ObiList`1/<GetEnumerator>d__2::.ctor(System.Int32)
// 0x000007E3 System.Void Obi.ObiList`1/<GetEnumerator>d__2::System.IDisposable.Dispose()
// 0x000007E4 System.Boolean Obi.ObiList`1/<GetEnumerator>d__2::MoveNext()
// 0x000007E5 T Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000007E6 System.Void Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
// 0x000007E7 System.Object Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
// 0x000007E8 System.Void Obi.ParticlePair::.ctor(System.Int32,System.Int32)
extern void ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA (void);
// 0x000007E9 System.Int32 Obi.ParticlePair::get_Item(System.Int32)
extern void ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9 (void);
// 0x000007EA System.Void Obi.ParticlePair::set_Item(System.Int32,System.Int32)
extern void ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1 (void);
// 0x000007EB System.Void Obi.QueryShape::.ctor(Obi.QueryShape/QueryType,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32)
extern void QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453 (void);
// 0x000007EC System.Int32 Obi.SimplexCounts::get_simplexCount()
extern void SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86 (void);
// 0x000007ED System.Void Obi.SimplexCounts::.ctor(System.Int32,System.Int32,System.Int32)
extern void SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066 (void);
// 0x000007EE System.Int32 Obi.SimplexCounts::GetSimplexStartAndSize(System.Int32,System.Int32&)
extern void SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95 (void);
// 0x000007EF System.Void Obi.VInt4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB (void);
// 0x000007F0 System.Void Obi.VInt4::.ctor(System.Int32)
extern void VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C (void);
// 0x000007F1 UnityEngine.Vector3Int Obi.MeshVoxelizer::get_Origin()
extern void MeshVoxelizer_get_Origin_m98D23A3FAE59358DCED36A586C0AB6B4F3E77B4D (void);
// 0x000007F2 System.Int32 Obi.MeshVoxelizer::get_voxelCount()
extern void MeshVoxelizer_get_voxelCount_m1590512CB3A933F6FD26076643E8FBBF72CD4565 (void);
// 0x000007F3 System.Void Obi.MeshVoxelizer::.ctor(UnityEngine.Mesh,System.Single)
extern void MeshVoxelizer__ctor_m2E9885D259222CEE4CB38497584FE8F3EBB0E2B4 (void);
// 0x000007F4 Obi.MeshVoxelizer/Voxel Obi.MeshVoxelizer::get_Item(System.Int32,System.Int32,System.Int32)
extern void MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356 (void);
// 0x000007F5 System.Void Obi.MeshVoxelizer::set_Item(System.Int32,System.Int32,System.Int32,Obi.MeshVoxelizer/Voxel)
extern void MeshVoxelizer_set_Item_m6E59067E37D364C147DEC0335408F67C39286DBD (void);
// 0x000007F6 System.Single Obi.MeshVoxelizer::GetDistanceToNeighbor(System.Int32)
extern void MeshVoxelizer_GetDistanceToNeighbor_mAC3238234874D704EEDE7B39EA4E2C557043FA8F (void);
// 0x000007F7 System.Int32 Obi.MeshVoxelizer::GetVoxelIndex(System.Int32,System.Int32,System.Int32)
extern void MeshVoxelizer_GetVoxelIndex_m03964EDAA758946CBAFB630ECD6AC4DAF9B03921 (void);
// 0x000007F8 UnityEngine.Vector3 Obi.MeshVoxelizer::GetVoxelCenter(UnityEngine.Vector3Int&)
extern void MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089 (void);
// 0x000007F9 UnityEngine.Bounds Obi.MeshVoxelizer::GetTriangleBounds(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06 (void);
// 0x000007FA System.Collections.Generic.List`1<System.Int32> Obi.MeshVoxelizer::GetTrianglesOverlappingVoxel(System.Int32)
extern void MeshVoxelizer_GetTrianglesOverlappingVoxel_mBEA8964FBC06F1AC96902CAB7A18AD815C4FBAEE (void);
// 0x000007FB UnityEngine.Vector3Int Obi.MeshVoxelizer::GetPointVoxel(UnityEngine.Vector3&)
extern void MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1 (void);
// 0x000007FC System.Boolean Obi.MeshVoxelizer::VoxelExists(UnityEngine.Vector3Int&)
extern void MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B (void);
// 0x000007FD System.Boolean Obi.MeshVoxelizer::VoxelExists(System.Int32,System.Int32,System.Int32)
extern void MeshVoxelizer_VoxelExists_m6075ED6BD1CD96FBC57C1935A45231C09ABCFA70 (void);
// 0x000007FE System.Void Obi.MeshVoxelizer::AppendOverlappingVoxels(UnityEngine.Bounds&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Int32)
extern void MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7 (void);
// 0x000007FF System.Collections.IEnumerator Obi.MeshVoxelizer::Voxelize(UnityEngine.Matrix4x4,System.Boolean)
extern void MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A (void);
// 0x00000800 System.Void Obi.MeshVoxelizer::BoundaryThinning()
extern void MeshVoxelizer_BoundaryThinning_m4E5359902ABE3CD1C2541C9FCEADE519E69482D5 (void);
// 0x00000801 System.Collections.IEnumerator Obi.MeshVoxelizer::FloodFill()
extern void MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51 (void);
// 0x00000802 System.Boolean Obi.MeshVoxelizer::IsIntersecting(UnityEngine.Bounds&,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC (void);
// 0x00000803 System.Boolean Obi.MeshVoxelizer::TriangleAabbSATTest(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10 (void);
// 0x00000804 System.Void Obi.MeshVoxelizer::.cctor()
extern void MeshVoxelizer__cctor_m221669B76FE23A351F3F850B1B8B6BA2FD840D15 (void);
// 0x00000805 System.Void Obi.MeshVoxelizer/<Voxelize>d__29::.ctor(System.Int32)
extern void U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0 (void);
// 0x00000806 System.Void Obi.MeshVoxelizer/<Voxelize>d__29::System.IDisposable.Dispose()
extern void U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD (void);
// 0x00000807 System.Boolean Obi.MeshVoxelizer/<Voxelize>d__29::MoveNext()
extern void U3CVoxelizeU3Ed__29_MoveNext_m6AC2C7AA56B9E60005E531B6AFBC225E89351F62 (void);
// 0x00000808 System.Object Obi.MeshVoxelizer/<Voxelize>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036 (void);
// 0x00000809 System.Void Obi.MeshVoxelizer/<Voxelize>d__29::System.Collections.IEnumerator.Reset()
extern void U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C (void);
// 0x0000080A System.Object Obi.MeshVoxelizer/<Voxelize>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7 (void);
// 0x0000080B System.Void Obi.MeshVoxelizer/<FloodFill>d__31::.ctor(System.Int32)
extern void U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97 (void);
// 0x0000080C System.Void Obi.MeshVoxelizer/<FloodFill>d__31::System.IDisposable.Dispose()
extern void U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2 (void);
// 0x0000080D System.Boolean Obi.MeshVoxelizer/<FloodFill>d__31::MoveNext()
extern void U3CFloodFillU3Ed__31_MoveNext_m35FC429F6648DE25CB9A4F53A3547E84ECAEEB8B (void);
// 0x0000080E System.Object Obi.MeshVoxelizer/<FloodFill>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189 (void);
// 0x0000080F System.Void Obi.MeshVoxelizer/<FloodFill>d__31::System.Collections.IEnumerator.Reset()
extern void U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3 (void);
// 0x00000810 System.Object Obi.MeshVoxelizer/<FloodFill>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E (void);
// 0x00000811 System.Void Obi.PriorityQueue`1::.ctor()
// 0x00000812 System.Void Obi.PriorityQueue`1::Enqueue(T)
// 0x00000813 T Obi.PriorityQueue`1::Dequeue()
// 0x00000814 T Obi.PriorityQueue`1::Peek()
// 0x00000815 System.Collections.Generic.IEnumerable`1<T> Obi.PriorityQueue`1::GetEnumerator()
// 0x00000816 System.Void Obi.PriorityQueue`1::Clear()
// 0x00000817 System.Int32 Obi.PriorityQueue`1::Count()
// 0x00000818 System.String Obi.PriorityQueue`1::ToString()
// 0x00000819 System.Boolean Obi.PriorityQueue`1::IsConsistent()
// 0x0000081A System.Void Obi.PriorityQueue`1/<GetEnumerator>d__5::.ctor(System.Int32)
// 0x0000081B System.Void Obi.PriorityQueue`1/<GetEnumerator>d__5::System.IDisposable.Dispose()
// 0x0000081C System.Boolean Obi.PriorityQueue`1/<GetEnumerator>d__5::MoveNext()
// 0x0000081D T Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000081E System.Void Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.IEnumerator.Reset()
// 0x0000081F System.Object Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.IEnumerator.get_Current()
// 0x00000820 System.Collections.Generic.IEnumerator`1<T> Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000821 System.Collections.IEnumerator Obi.PriorityQueue`1/<GetEnumerator>d__5::System.Collections.IEnumerable.GetEnumerator()
// 0x00000822 System.Void Obi.VoxelDistanceField::.ctor(Obi.MeshVoxelizer)
extern void VoxelDistanceField__ctor_mF001EDBFAD1E407A198F85AE578DC94C131DC940 (void);
// 0x00000823 System.Single Obi.VoxelDistanceField::SampleUnfiltered(System.Int32,System.Int32,System.Int32)
extern void VoxelDistanceField_SampleUnfiltered_m1DCFD20D36F447D804A7DBE566E82FD4E6A134AA (void);
// 0x00000824 UnityEngine.Vector4 Obi.VoxelDistanceField::SampleFiltered(System.Single,System.Single,System.Single)
extern void VoxelDistanceField_SampleFiltered_mF03122890E95B4323F7318B9C27A8C65DA3F463E (void);
// 0x00000825 System.Collections.IEnumerator Obi.VoxelDistanceField::JumpFlood()
extern void VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6 (void);
// 0x00000826 System.Void Obi.VoxelDistanceField::JumpFloodPass(System.Int32,UnityEngine.Vector3Int[0...,0...,0...],UnityEngine.Vector3Int[0...,0...,0...])
extern void VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3 (void);
// 0x00000827 System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::.ctor(System.Int32)
extern void U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654 (void);
// 0x00000828 System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.IDisposable.Dispose()
extern void U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6 (void);
// 0x00000829 System.Boolean Obi.VoxelDistanceField/<JumpFlood>d__5::MoveNext()
extern void U3CJumpFloodU3Ed__5_MoveNext_mCF3E269446E8A96E7F313896B714A111CBBD3FA3 (void);
// 0x0000082A System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0 (void);
// 0x0000082B System.Void Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.Reset()
extern void U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22 (void);
// 0x0000082C System.Object Obi.VoxelDistanceField/<JumpFlood>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18 (void);
// 0x0000082D System.Void Obi.VoxelPathFinder::.ctor(Obi.MeshVoxelizer)
extern void VoxelPathFinder__ctor_mEB3020C477C2C93B2F00C9CFC38CFE47068AE443 (void);
// 0x0000082E Obi.VoxelPathFinder/TargetVoxel Obi.VoxelPathFinder::AStar(UnityEngine.Vector3Int&,System.Func`2<Obi.VoxelPathFinder/TargetVoxel,System.Boolean>,System.Func`2<UnityEngine.Vector3Int,System.Single>)
extern void VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC (void);
// 0x0000082F Obi.VoxelPathFinder/TargetVoxel Obi.VoxelPathFinder::FindClosestNonEmptyVoxel(UnityEngine.Vector3Int&)
extern void VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714 (void);
// 0x00000830 Obi.VoxelPathFinder/TargetVoxel Obi.VoxelPathFinder::FindPath(UnityEngine.Vector3Int&,UnityEngine.Vector3Int)
extern void VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5 (void);
// 0x00000831 System.Boolean Obi.VoxelPathFinder::<FindClosestNonEmptyVoxel>b__6_0(Obi.VoxelPathFinder/TargetVoxel)
extern void VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC (void);
// 0x00000832 System.Single Obi.VoxelPathFinder/TargetVoxel::get_cost()
extern void TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F (void);
// 0x00000833 System.Void Obi.VoxelPathFinder/TargetVoxel::.ctor(UnityEngine.Vector3Int,System.Single,System.Single)
extern void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF (void);
// 0x00000834 System.Boolean Obi.VoxelPathFinder/TargetVoxel::Equals(Obi.VoxelPathFinder/TargetVoxel)
extern void TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681 (void);
// 0x00000835 System.Int32 Obi.VoxelPathFinder/TargetVoxel::CompareTo(Obi.VoxelPathFinder/TargetVoxel)
extern void TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8 (void);
// 0x00000836 System.Void Obi.VoxelPathFinder/<>c::.cctor()
extern void U3CU3Ec__cctor_m2E9966CB04100D59E489C39A64FAC67F8FD97311 (void);
// 0x00000837 System.Void Obi.VoxelPathFinder/<>c::.ctor()
extern void U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888 (void);
// 0x00000838 System.Single Obi.VoxelPathFinder/<>c::<FindClosestNonEmptyVoxel>b__6_1(UnityEngine.Vector3Int)
extern void U3CU3Ec_U3CFindClosestNonEmptyVoxelU3Eb__6_1_mF780311AA35327276D353A5C472EBABD63FB6D10 (void);
// 0x00000839 System.Void Obi.VoxelPathFinder/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mCCDBC132E254A9EA49862CDDDB93E94F182AF58C (void);
// 0x0000083A System.Boolean Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__0(Obi.VoxelPathFinder/TargetVoxel)
extern void U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__0_m0CBCD40678980CB309FD296C8DCA140B2368C158 (void);
// 0x0000083B System.Single Obi.VoxelPathFinder/<>c__DisplayClass7_0::<FindPath>b__1(UnityEngine.Vector3Int)
extern void U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__1_m93C19DF998EC9A85989D9980D3C89F8EB7724361 (void);
// 0x0000083C System.Void Obi.ObiDistanceFieldRenderer::Awake()
extern void ObiDistanceFieldRenderer_Awake_m02202AAABFC3B738E4965E9B2B897A228E6AC210 (void);
// 0x0000083D System.Void Obi.ObiDistanceFieldRenderer::OnEnable()
extern void ObiDistanceFieldRenderer_OnEnable_m5A75F9F5EE526E06FF0D410741740FCB6B6478E1 (void);
// 0x0000083E System.Void Obi.ObiDistanceFieldRenderer::OnDisable()
extern void ObiDistanceFieldRenderer_OnDisable_m7C13494A2B29D5E6827B2757573C7370FC80A394 (void);
// 0x0000083F System.Void Obi.ObiDistanceFieldRenderer::Cleanup()
extern void ObiDistanceFieldRenderer_Cleanup_mFE94B31A3E0EED155DC9650B112587501A88D532 (void);
// 0x00000840 System.Void Obi.ObiDistanceFieldRenderer::ResizeTexture()
extern void ObiDistanceFieldRenderer_ResizeTexture_mE50107B1ECBAB2706311F61EB8EDFAA5A47FB02C (void);
// 0x00000841 System.Void Obi.ObiDistanceFieldRenderer::CreatePlaneMesh(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_CreatePlaneMesh_m7DABB1814E6572C53BCBCC12B382369FA534AAA4 (void);
// 0x00000842 System.Void Obi.ObiDistanceFieldRenderer::RefreshCutawayTexture(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_RefreshCutawayTexture_mFE87BC7C9D4D36CAE8BA3A2BB5CCFE9195422F93 (void);
// 0x00000843 System.Void Obi.ObiDistanceFieldRenderer::DrawCutawayPlane(Obi.ObiDistanceField,UnityEngine.Matrix4x4)
extern void ObiDistanceFieldRenderer_DrawCutawayPlane_m881EB8355EA4D1D21C65297319180645AD0BBB3A (void);
// 0x00000844 System.Void Obi.ObiDistanceFieldRenderer::OnDrawGizmos()
extern void ObiDistanceFieldRenderer_OnDrawGizmos_m216C9ED791D6583CFB5AC31CCF96FC999FEB065B (void);
// 0x00000845 System.Void Obi.ObiDistanceFieldRenderer::.ctor()
extern void ObiDistanceFieldRenderer__ctor_m5BDF67924CA8661E32DCAE76825AECE42C4E08AA (void);
// 0x00000846 System.Void Obi.ObiInstancedParticleRenderer::OnEnable()
extern void ObiInstancedParticleRenderer_OnEnable_mE355745DF976D2E5FB662C641FFF1207CFF849A8 (void);
// 0x00000847 System.Void Obi.ObiInstancedParticleRenderer::OnDisable()
extern void ObiInstancedParticleRenderer_OnDisable_mB6727244E6FCBAA7A72470799C34118F47F54C26 (void);
// 0x00000848 System.Void Obi.ObiInstancedParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiInstancedParticleRenderer_DrawParticles_m006744E372DC552B4BBE90BDA10C2936E58A7CEB (void);
// 0x00000849 System.Void Obi.ObiInstancedParticleRenderer::.ctor()
extern void ObiInstancedParticleRenderer__ctor_m36160262281CB03A2EF5F4F65B1599650E20D198 (void);
// 0x0000084A System.Void Obi.ObiInstancedParticleRenderer::.cctor()
extern void ObiInstancedParticleRenderer__cctor_m8F094F8AD40C59C077031DB8223AF95B8123B2B7 (void);
// 0x0000084B System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ObiParticleRenderer::get_ParticleMeshes()
extern void ObiParticleRenderer_get_ParticleMeshes_m19B21A66F62AF9565B5610A7E406A9F4D9AB2624 (void);
// 0x0000084C Obi.ParticleImpostorRendering Obi.ObiParticleRenderer::get_impostors()
extern void ObiParticleRenderer_get_impostors_mE9035EE0C41C3AFB9849C8CF46A35BAA97E6F594 (void);
// 0x0000084D UnityEngine.Material Obi.ObiParticleRenderer::get_ParticleMaterial()
extern void ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530 (void);
// 0x0000084E System.Void Obi.ObiParticleRenderer::set_ParticleMaterial(UnityEngine.Material)
extern void ObiParticleRenderer_set_ParticleMaterial_m271A889A40C59DDA14EF2DCA96DD1B93E40CC3F9 (void);
// 0x0000084F System.Void Obi.ObiParticleRenderer::OnEnable()
extern void ObiParticleRenderer_OnEnable_m5936C65AFFDE90197548673B64DCDB559B634084 (void);
// 0x00000850 System.Void Obi.ObiParticleRenderer::OnDisable()
extern void ObiParticleRenderer_OnDisable_mAA99859EE95AC59E3CCEBDAEE3C9F3D1EBE9F2A3 (void);
// 0x00000851 System.Void Obi.ObiParticleRenderer::CreateMaterialIfNeeded()
extern void ObiParticleRenderer_CreateMaterialIfNeeded_m3F402725EA811E7191F495C02BC1D42C9D3508E2 (void);
// 0x00000852 System.Void Obi.ObiParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiParticleRenderer_DrawParticles_m968118939B2050BAB32AF92EFCB004552F31FCAE (void);
// 0x00000853 System.Void Obi.ObiParticleRenderer::DrawParticles()
extern void ObiParticleRenderer_DrawParticles_m10909ABED462F26F971463C7A569BC19C8115FD8 (void);
// 0x00000854 System.Void Obi.ObiParticleRenderer::.ctor()
extern void ObiParticleRenderer__ctor_mFC28AF85E8C3264440883850C411477777E11A29 (void);
// 0x00000855 System.Void Obi.ObiParticleRenderer::.cctor()
extern void ObiParticleRenderer__cctor_m25FE6E62F8C43957CDFA579FE81399B4270C8DDC (void);
// 0x00000856 System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ParticleImpostorRendering::get_Meshes()
extern void ParticleImpostorRendering_get_Meshes_m755745FEE84971387953757DE9D15D9D32204C2F (void);
// 0x00000857 System.Void Obi.ParticleImpostorRendering::Apply(UnityEngine.Mesh)
extern void ParticleImpostorRendering_Apply_mC4B8B70A312A115D0D3264BA0F4D6CB8920AD9D5 (void);
// 0x00000858 System.Void Obi.ParticleImpostorRendering::ClearMeshes()
extern void ParticleImpostorRendering_ClearMeshes_m425FD6121111FFDD2834713DDE69988571B30BD8 (void);
// 0x00000859 System.Void Obi.ParticleImpostorRendering::UpdateMeshes(Obi.IObiParticleCollection,System.Boolean[],UnityEngine.Color[])
extern void ParticleImpostorRendering_UpdateMeshes_m352C0F940E2857058D6A6B447A03AB70258E8A60 (void);
// 0x0000085A System.Void Obi.ParticleImpostorRendering::.ctor()
extern void ParticleImpostorRendering__ctor_m83C3D5BED9E2463E4E916753E01D78C90E7C9BBE (void);
// 0x0000085B System.Void Obi.ParticleImpostorRendering::.cctor()
extern void ParticleImpostorRendering__cctor_m096389C9C5C4FB998DFF6B43A943B8F29D300B72 (void);
// 0x0000085C System.Void Obi.ShadowmapExposer::Awake()
extern void ShadowmapExposer_Awake_mA4E68783C4BCFD7B7FCCF2FB2047921F739DA519 (void);
// 0x0000085D System.Void Obi.ShadowmapExposer::OnEnable()
extern void ShadowmapExposer_OnEnable_mA68155C92E4F83954076D9000038D58A265100F5 (void);
// 0x0000085E System.Void Obi.ShadowmapExposer::OnDisable()
extern void ShadowmapExposer_OnDisable_m7CC85B1D1729AA03CDAC1816F9A84E3350254554 (void);
// 0x0000085F System.Void Obi.ShadowmapExposer::Cleanup()
extern void ShadowmapExposer_Cleanup_m32D90F4E8CAC5C2EB13BFAD141A056F8D3B143E5 (void);
// 0x00000860 System.Void Obi.ShadowmapExposer::SetupFluidShadowsCommandBuffer()
extern void ShadowmapExposer_SetupFluidShadowsCommandBuffer_mE799C5800875D6B7CA60B3775768C9F07B2288B4 (void);
// 0x00000861 System.Void Obi.ShadowmapExposer::Update()
extern void ShadowmapExposer_Update_mBFA72090D0E7D36367BCA31B001E2965D1B264E2 (void);
// 0x00000862 System.Void Obi.ShadowmapExposer::.ctor()
extern void ShadowmapExposer__ctor_m10E8C4FAA46FCB5CC04085336279E049BC51FAFA (void);
// 0x00000863 System.Void Obi.ObiSolver::add_OnCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5 (void);
// 0x00000864 System.Void Obi.ObiSolver::remove_OnCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674 (void);
// 0x00000865 System.Void Obi.ObiSolver::add_OnParticleCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52 (void);
// 0x00000866 System.Void Obi.ObiSolver::remove_OnParticleCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7 (void);
// 0x00000867 System.Void Obi.ObiSolver::add_OnUpdateParameters(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B (void);
// 0x00000868 System.Void Obi.ObiSolver::remove_OnUpdateParameters(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1 (void);
// 0x00000869 System.Void Obi.ObiSolver::add_OnPrepareFrame(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69 (void);
// 0x0000086A System.Void Obi.ObiSolver::remove_OnPrepareFrame(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6 (void);
// 0x0000086B System.Void Obi.ObiSolver::add_OnPrepareStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E (void);
// 0x0000086C System.Void Obi.ObiSolver::remove_OnPrepareStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250 (void);
// 0x0000086D System.Void Obi.ObiSolver::add_OnBeginStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72 (void);
// 0x0000086E System.Void Obi.ObiSolver::remove_OnBeginStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D (void);
// 0x0000086F System.Void Obi.ObiSolver::add_OnSubstep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E (void);
// 0x00000870 System.Void Obi.ObiSolver::remove_OnSubstep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33 (void);
// 0x00000871 System.Void Obi.ObiSolver::add_OnEndStep(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D (void);
// 0x00000872 System.Void Obi.ObiSolver::remove_OnEndStep(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3 (void);
// 0x00000873 System.Void Obi.ObiSolver::add_OnInterpolate(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D (void);
// 0x00000874 System.Void Obi.ObiSolver::remove_OnInterpolate(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626 (void);
// 0x00000875 Obi.ISolverImpl Obi.ObiSolver::get_implementation()
extern void ObiSolver_get_implementation_m9F3A6A2A2D6921B6A59A82F935039FC29F24E422 (void);
// 0x00000876 System.Boolean Obi.ObiSolver::get_initialized()
extern void ObiSolver_get_initialized_m245E2320CEE2189A248BB0DE795CD9FE80839C78 (void);
// 0x00000877 Obi.IObiBackend Obi.ObiSolver::get_simulationBackend()
extern void ObiSolver_get_simulationBackend_m67AEE52B9C07C78C1231FE5074F7125E47A8109A (void);
// 0x00000878 System.Void Obi.ObiSolver::set_backendType(Obi.ObiSolver/BackendType)
extern void ObiSolver_set_backendType_mE714E4CD3D72BE093E2ECBB2B418D7095A3B11B4 (void);
// 0x00000879 Obi.ObiSolver/BackendType Obi.ObiSolver::get_backendType()
extern void ObiSolver_get_backendType_m49EDD493C5D45A0D0728C26F95F1E955FEFB4104 (void);
// 0x0000087A Obi.SimplexCounts Obi.ObiSolver::get_simplexCounts()
extern void ObiSolver_get_simplexCounts_m78AF46D65457FB56118D4ECD884FEAD71D3E28C4 (void);
// 0x0000087B UnityEngine.Bounds Obi.ObiSolver::get_Bounds()
extern void ObiSolver_get_Bounds_m4C8914413BBBC0968BA6ADCF3E7149AA5F7FC7D3 (void);
// 0x0000087C System.Boolean Obi.ObiSolver::get_IsVisible()
extern void ObiSolver_get_IsVisible_mA12ECA2DFE503F721181D2A8A5F5F9065BABAF0B (void);
// 0x0000087D System.Single Obi.ObiSolver::get_maxScale()
extern void ObiSolver_get_maxScale_m2118E5D74E45C017D6D91E33790768FC10FE9B18 (void);
// 0x0000087E System.Int32 Obi.ObiSolver::get_allocParticleCount()
extern void ObiSolver_get_allocParticleCount_m98C18E7A785C31F32283F6A52AB1372313E3E16F (void);
// 0x0000087F System.Int32 Obi.ObiSolver::get_contactCount()
extern void ObiSolver_get_contactCount_mAE6F8DD8070340148DCC11C0F729F1B38C4AF654 (void);
// 0x00000880 System.Int32 Obi.ObiSolver::get_particleContactCount()
extern void ObiSolver_get_particleContactCount_mFDF3ADAB6B54D7FECF6F1943E4AE93C0AE10F18F (void);
// 0x00000881 Obi.ObiSolver/ParticleInActor[] Obi.ObiSolver::get_particleToActor()
extern void ObiSolver_get_particleToActor_mFCEA216986C39AD5264352D73B10017FE5E7F3D3 (void);
// 0x00000882 Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyLinearDeltas()
extern void ObiSolver_get_rigidbodyLinearDeltas_mEB6D6F68DEAD8616EA48FA36AEA9B37520CA9367 (void);
// 0x00000883 Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyAngularDeltas()
extern void ObiSolver_get_rigidbodyAngularDeltas_m05353966D848B59BC9586B0C19CAE476834502C9 (void);
// 0x00000884 UnityEngine.Color[] Obi.ObiSolver::get_colors()
extern void ObiSolver_get_colors_m3263CBAEB79EE1F583593969A2EAD32549CB875D (void);
// 0x00000885 Obi.ObiNativeInt4List Obi.ObiSolver::get_cellCoords()
extern void ObiSolver_get_cellCoords_m6BB3F63111C716E45AAC8FC0BE4AB0CD17331B28 (void);
// 0x00000886 Obi.ObiNativeVector4List Obi.ObiSolver::get_positions()
extern void ObiSolver_get_positions_mB7EA5209FDB8289380DBA07A2F28E640D485F557 (void);
// 0x00000887 Obi.ObiNativeVector4List Obi.ObiSolver::get_restPositions()
extern void ObiSolver_get_restPositions_m1BCA73DCC44BA1B9181838370F0E546B4D66CFC4 (void);
// 0x00000888 Obi.ObiNativeVector4List Obi.ObiSolver::get_prevPositions()
extern void ObiSolver_get_prevPositions_mA99888F076C4FB374B14DEE1178C625A3F8F9B00 (void);
// 0x00000889 Obi.ObiNativeVector4List Obi.ObiSolver::get_startPositions()
extern void ObiSolver_get_startPositions_m3B9144D015BDB7F0F676F065AAC9A22F0DA9CC27 (void);
// 0x0000088A Obi.ObiNativeVector4List Obi.ObiSolver::get_renderablePositions()
extern void ObiSolver_get_renderablePositions_m56EE654A3E7ECEF99EE3F1481E759B01DAB697BE (void);
// 0x0000088B Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientations()
extern void ObiSolver_get_orientations_mAF1A79C344CC44789900361936635BBD508D2973 (void);
// 0x0000088C Obi.ObiNativeQuaternionList Obi.ObiSolver::get_restOrientations()
extern void ObiSolver_get_restOrientations_m5A079EC8140D809FB02223ECCA6986917B333AE4 (void);
// 0x0000088D Obi.ObiNativeQuaternionList Obi.ObiSolver::get_prevOrientations()
extern void ObiSolver_get_prevOrientations_mBB52AEB5D484B0A4BAE23213D65585BE498B5F68 (void);
// 0x0000088E Obi.ObiNativeQuaternionList Obi.ObiSolver::get_startOrientations()
extern void ObiSolver_get_startOrientations_mED3EA83E62A458AE8F95A89CEC15ECA81FDF28D1 (void);
// 0x0000088F Obi.ObiNativeQuaternionList Obi.ObiSolver::get_renderableOrientations()
extern void ObiSolver_get_renderableOrientations_mB71AEBAF05CD2CA3FED8936554686F5A0414C06A (void);
// 0x00000890 Obi.ObiNativeVector4List Obi.ObiSolver::get_velocities()
extern void ObiSolver_get_velocities_m73051C86B492795133F138E42FF08698222FCCD6 (void);
// 0x00000891 Obi.ObiNativeVector4List Obi.ObiSolver::get_angularVelocities()
extern void ObiSolver_get_angularVelocities_mE565A9561A09435E294755AA2F7E31993BD932DB (void);
// 0x00000892 Obi.ObiNativeFloatList Obi.ObiSolver::get_invMasses()
extern void ObiSolver_get_invMasses_m403082935BEB22CE4C8A086C31B748FC46B8C319 (void);
// 0x00000893 Obi.ObiNativeFloatList Obi.ObiSolver::get_invRotationalMasses()
extern void ObiSolver_get_invRotationalMasses_m35049317216303EC2D97F143D98DBB60C9EADF58 (void);
// 0x00000894 Obi.ObiNativeVector4List Obi.ObiSolver::get_invInertiaTensors()
extern void ObiSolver_get_invInertiaTensors_m66E7A977E525B6628FA0D14015A52DE6CC26AADA (void);
// 0x00000895 Obi.ObiNativeVector4List Obi.ObiSolver::get_externalForces()
extern void ObiSolver_get_externalForces_m6DE0779EAA775DB7E7A853B844E07464240BED59 (void);
// 0x00000896 Obi.ObiNativeVector4List Obi.ObiSolver::get_externalTorques()
extern void ObiSolver_get_externalTorques_m7A057FD836A21C5CA1DAEA2258E05F81D15F8991 (void);
// 0x00000897 Obi.ObiNativeVector4List Obi.ObiSolver::get_wind()
extern void ObiSolver_get_wind_m72179C4670DBCE8A1838066EA134F02A16DC371C (void);
// 0x00000898 Obi.ObiNativeVector4List Obi.ObiSolver::get_positionDeltas()
extern void ObiSolver_get_positionDeltas_mA020DD1DBE7DDC042F8C730EBE9A71D6AE213AF9 (void);
// 0x00000899 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientationDeltas()
extern void ObiSolver_get_orientationDeltas_m747EE331713678E42AE2A16F9201AF96E8CFAF21 (void);
// 0x0000089A Obi.ObiNativeIntList Obi.ObiSolver::get_positionConstraintCounts()
extern void ObiSolver_get_positionConstraintCounts_mFBC0F55E2F573CC7A38F71703D8617DF5C9BF895 (void);
// 0x0000089B Obi.ObiNativeIntList Obi.ObiSolver::get_orientationConstraintCounts()
extern void ObiSolver_get_orientationConstraintCounts_m6752E0D0CAA0272BD7FF4242E5A426F4310FAECA (void);
// 0x0000089C Obi.ObiNativeIntList Obi.ObiSolver::get_collisionMaterials()
extern void ObiSolver_get_collisionMaterials_mE98DA539A9F5EC9BE8B35D9AA8C6F4C4FDA211A1 (void);
// 0x0000089D Obi.ObiNativeIntList Obi.ObiSolver::get_phases()
extern void ObiSolver_get_phases_m1DFABF0F6107CA7A301BBC35A0896C69FA1D7422 (void);
// 0x0000089E Obi.ObiNativeIntList Obi.ObiSolver::get_filters()
extern void ObiSolver_get_filters_m35DFCECD0FEF501CB8903CF986F67CD3487DFEBB (void);
// 0x0000089F Obi.ObiNativeVector4List Obi.ObiSolver::get_anisotropies()
extern void ObiSolver_get_anisotropies_mDD48FB3AC63921409AAF55FB2FED3833FC458009 (void);
// 0x000008A0 Obi.ObiNativeVector4List Obi.ObiSolver::get_principalRadii()
extern void ObiSolver_get_principalRadii_m6C3307DA982056B2AF918D77B539E22580E5F179 (void);
// 0x000008A1 Obi.ObiNativeVector4List Obi.ObiSolver::get_normals()
extern void ObiSolver_get_normals_m241E9FFCE3A531BFF2679A3420EBCE7B313FF52C (void);
// 0x000008A2 Obi.ObiNativeVector4List Obi.ObiSolver::get_vorticities()
extern void ObiSolver_get_vorticities_mDC8CC9D454FBD7AABF51AEDD83B63D62D74D2E62 (void);
// 0x000008A3 Obi.ObiNativeVector4List Obi.ObiSolver::get_fluidData()
extern void ObiSolver_get_fluidData_m70D3FEC3C44F6EA5D68AB6B39BD72A08B41EF349 (void);
// 0x000008A4 Obi.ObiNativeVector4List Obi.ObiSolver::get_userData()
extern void ObiSolver_get_userData_m63E5DF119EE501A9380C6A176079C9FD1361B561 (void);
// 0x000008A5 Obi.ObiNativeFloatList Obi.ObiSolver::get_smoothingRadii()
extern void ObiSolver_get_smoothingRadii_mEE720BA5CB3140CCB508FC97466DDC06F18A9380 (void);
// 0x000008A6 Obi.ObiNativeFloatList Obi.ObiSolver::get_buoyancies()
extern void ObiSolver_get_buoyancies_m14142F7AA18DAE0CD2BA25EA5F3F7E5796938365 (void);
// 0x000008A7 Obi.ObiNativeFloatList Obi.ObiSolver::get_restDensities()
extern void ObiSolver_get_restDensities_mA85A3F3E9C265A9E3B01B957691808F072123C96 (void);
// 0x000008A8 Obi.ObiNativeFloatList Obi.ObiSolver::get_viscosities()
extern void ObiSolver_get_viscosities_mBCDE34198266ABADF85472134F8A69F897BB37D6 (void);
// 0x000008A9 Obi.ObiNativeFloatList Obi.ObiSolver::get_surfaceTension()
extern void ObiSolver_get_surfaceTension_m06BBC45CF2C65E927490672F37C89AD69ACE039A (void);
// 0x000008AA Obi.ObiNativeFloatList Obi.ObiSolver::get_vortConfinement()
extern void ObiSolver_get_vortConfinement_mBCA2B8E737E63A3DADDC34F1E7E7C4D26F7A71D0 (void);
// 0x000008AB Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericDrag()
extern void ObiSolver_get_atmosphericDrag_m81584993AAF86345E489C4D833D1BAF3E704CB16 (void);
// 0x000008AC Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericPressure()
extern void ObiSolver_get_atmosphericPressure_m363045FB67ED30D2413C84317FBF2033FF9BE1B6 (void);
// 0x000008AD Obi.ObiNativeFloatList Obi.ObiSolver::get_diffusion()
extern void ObiSolver_get_diffusion_m614EC192C40E1DB26E20E2B7AEEB1E3853E1CBAA (void);
// 0x000008AE System.Void Obi.ObiSolver::Update()
extern void ObiSolver_Update_m3432EE148E4943BFEA7F248CC1ADF410EB009532 (void);
// 0x000008AF System.Void Obi.ObiSolver::OnDestroy()
extern void ObiSolver_OnDestroy_m10E800119F5492F06C454965798031E2959B4D91 (void);
// 0x000008B0 System.Void Obi.ObiSolver::CreateBackend()
extern void ObiSolver_CreateBackend_m91B701B5D03356B869ECCC5E5B4E0F9E82CA7377 (void);
// 0x000008B1 System.Void Obi.ObiSolver::Initialize()
extern void ObiSolver_Initialize_m69393E4E890B2AF174E313FAB09AF20C8858713B (void);
// 0x000008B2 System.Void Obi.ObiSolver::Teardown()
extern void ObiSolver_Teardown_m7AFA06EFC9E0BA723690DF91837C3E3B8D6C30C1 (void);
// 0x000008B3 System.Void Obi.ObiSolver::UpdateBackend()
extern void ObiSolver_UpdateBackend_mED822610A9CB49AC6197C9EB12A70EBF7F5959A5 (void);
// 0x000008B4 System.Void Obi.ObiSolver::FreeRigidbodyArrays()
extern void ObiSolver_FreeRigidbodyArrays_m75E7122190DD0CFFE6594F39BED2EE9685488FA2 (void);
// 0x000008B5 System.Void Obi.ObiSolver::EnsureRigidbodyArraysCapacity(System.Int32)
extern void ObiSolver_EnsureRigidbodyArraysCapacity_mC0A5656D8985DE06BC6084750EC6AE266B7E059A (void);
// 0x000008B6 System.Void Obi.ObiSolver::FreeParticleArrays()
extern void ObiSolver_FreeParticleArrays_mC803A0F5E3291D91B643E13F40F56A1DD906DB19 (void);
// 0x000008B7 System.Void Obi.ObiSolver::EnsureParticleArraysCapacity(System.Int32)
extern void ObiSolver_EnsureParticleArraysCapacity_mA3FF2925ACDE72EC10DF9853E51FF3440F27EB67 (void);
// 0x000008B8 System.Void Obi.ObiSolver::AllocateParticles(System.Int32[])
extern void ObiSolver_AllocateParticles_m2437F6049C3AB8F791378C25BB220F3D447E9209 (void);
// 0x000008B9 System.Void Obi.ObiSolver::FreeParticles(System.Int32[])
extern void ObiSolver_FreeParticles_m4CD1BFC3A67A853618E2BC48B666735D57D69CBB (void);
// 0x000008BA System.Boolean Obi.ObiSolver::AddActor(Obi.ObiActor)
extern void ObiSolver_AddActor_m33CCB3774385993AD4756D44613032EDF7602076 (void);
// 0x000008BB System.Boolean Obi.ObiSolver::RemoveActor(Obi.ObiActor)
extern void ObiSolver_RemoveActor_m9816A116E0848361565F850D70B3B8A876ACC5A2 (void);
// 0x000008BC System.Void Obi.ObiSolver::PushSolverParameters()
extern void ObiSolver_PushSolverParameters_mC566D7A04AABE0FD982060FDDCDF94BF0330A898 (void);
// 0x000008BD Oni/ConstraintParameters Obi.ObiSolver::GetConstraintParameters(Oni/ConstraintType)
extern void ObiSolver_GetConstraintParameters_m612C27EA24521064F789D15FABB06E8320EC50DD (void);
// 0x000008BE Obi.IObiConstraints Obi.ObiSolver::GetConstraintsByType(Oni/ConstraintType)
extern void ObiSolver_GetConstraintsByType_mE11A373E6629FB18A577363D79AE2626806FB095 (void);
// 0x000008BF System.Void Obi.ObiSolver::PushActiveParticles()
extern void ObiSolver_PushActiveParticles_m699BFA15ECA1837EB8E6FB0C4C06564BC00A4356 (void);
// 0x000008C0 System.Void Obi.ObiSolver::PushSimplices()
extern void ObiSolver_PushSimplices_mCF761C06FD3C0023D8AC8CAF847F22856CA29F2B (void);
// 0x000008C1 System.Void Obi.ObiSolver::PushConstraints()
extern void ObiSolver_PushConstraints_m1FE372176ADB5BDDB3C11F042850CA126D62EA7D (void);
// 0x000008C2 System.Void Obi.ObiSolver::UpdateVisibility()
extern void ObiSolver_UpdateVisibility_m78B2DB3B14730D673C823699F0AF2CEB06524552 (void);
// 0x000008C3 System.Void Obi.ObiSolver::InitializeTransformFrame()
extern void ObiSolver_InitializeTransformFrame_m37F3F63EE2DD9DDE17329B3FDF750A4C9AFF317C (void);
// 0x000008C4 System.Void Obi.ObiSolver::UpdateTransformFrame(System.Single)
extern void ObiSolver_UpdateTransformFrame_m95D29D02A287E86629B2BF566891B519A5D3B2F4 (void);
// 0x000008C5 System.Void Obi.ObiSolver::PrepareFrame()
extern void ObiSolver_PrepareFrame_mC4616463C08C47D37E9B5A9610850BD0853BCF59 (void);
// 0x000008C6 Obi.IObiJobHandle Obi.ObiSolver::BeginStep(System.Single)
extern void ObiSolver_BeginStep_m08735BDB104DA6C7B894DDCBCE685C8F08EAE8C5 (void);
// 0x000008C7 Obi.IObiJobHandle Obi.ObiSolver::Substep(System.Single,System.Single,System.Int32)
extern void ObiSolver_Substep_m8EF42B8FCB4A92B66A18967A5E30C0FAF9D91226 (void);
// 0x000008C8 System.Void Obi.ObiSolver::EndStep(System.Single)
extern void ObiSolver_EndStep_m12D95AB19D02AB5C1E2ED1D8F3B1E586831DBA64 (void);
// 0x000008C9 System.Void Obi.ObiSolver::Interpolate(System.Single,System.Single)
extern void ObiSolver_Interpolate_mAE7A83C120C8BCD50676E781D540026B6BAEEDE2 (void);
// 0x000008CA System.Void Obi.ObiSolver::ReleaseJobHandles()
extern void ObiSolver_ReleaseJobHandles_m7624C20714357B7110E0A1268120AEDD7CF46A77 (void);
// 0x000008CB System.Void Obi.ObiSolver::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void ObiSolver_SpatialQuery_m6D0D03F289F49B89217E8A449DAABC0CCB90D83B (void);
// 0x000008CC Obi.QueryResult[] Obi.ObiSolver::SpatialQuery(Obi.QueryShape,Obi.AffineTransform)
extern void ObiSolver_SpatialQuery_m5C0D958E05B3BE7A66EFA6E320C0CB6E3A196607 (void);
// 0x000008CD System.Boolean Obi.ObiSolver::Raycast(UnityEngine.Ray,Obi.QueryResult&,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_m8F28F2F638DC71BC4716816A590D3D86E0274409 (void);
// 0x000008CE Obi.QueryResult[] Obi.ObiSolver::Raycast(System.Collections.Generic.List`1<UnityEngine.Ray>,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_mBABB28101E4819C4AC8C9D3189DB8636FD255007 (void);
// 0x000008CF System.Void Obi.ObiSolver::.ctor()
extern void ObiSolver__ctor_mA963ED6A2F8A02E33B2E3BACB178FC2F0436F753 (void);
// 0x000008D0 System.Void Obi.ObiSolver::.cctor()
extern void ObiSolver__cctor_m4023459B57CD95EEF9656B8BDA9B18BDD558C46B (void);
// 0x000008D1 System.Void Obi.ObiSolver/ObiCollisionEventArgs::.ctor()
extern void ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17 (void);
// 0x000008D2 System.Void Obi.ObiSolver/ParticleInActor::.ctor()
extern void ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA (void);
// 0x000008D3 System.Void Obi.ObiSolver/ParticleInActor::.ctor(Obi.ObiActor,System.Int32)
extern void ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3 (void);
// 0x000008D4 System.Void Obi.ObiSolver/SolverCallback::.ctor(System.Object,System.IntPtr)
extern void SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7 (void);
// 0x000008D5 System.Void Obi.ObiSolver/SolverCallback::Invoke(Obi.ObiSolver)
extern void SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057 (void);
// 0x000008D6 System.IAsyncResult Obi.ObiSolver/SolverCallback::BeginInvoke(Obi.ObiSolver,System.AsyncCallback,System.Object)
extern void SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08 (void);
// 0x000008D7 System.Void Obi.ObiSolver/SolverCallback::EndInvoke(System.IAsyncResult)
extern void SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2 (void);
// 0x000008D8 System.Void Obi.ObiSolver/SolverStepCallback::.ctor(System.Object,System.IntPtr)
extern void SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3 (void);
// 0x000008D9 System.Void Obi.ObiSolver/SolverStepCallback::Invoke(Obi.ObiSolver,System.Single)
extern void SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754 (void);
// 0x000008DA System.IAsyncResult Obi.ObiSolver/SolverStepCallback::BeginInvoke(Obi.ObiSolver,System.Single,System.AsyncCallback,System.Object)
extern void SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40 (void);
// 0x000008DB System.Void Obi.ObiSolver/SolverStepCallback::EndInvoke(System.IAsyncResult)
extern void SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533 (void);
// 0x000008DC System.Void Obi.ObiSolver/CollisionCallback::.ctor(System.Object,System.IntPtr)
extern void CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E (void);
// 0x000008DD System.Void Obi.ObiSolver/CollisionCallback::Invoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs)
extern void CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C (void);
// 0x000008DE System.IAsyncResult Obi.ObiSolver/CollisionCallback::BeginInvoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs,System.AsyncCallback,System.Object)
extern void CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB (void);
// 0x000008DF System.Void Obi.ObiSolver/CollisionCallback::EndInvoke(System.IAsyncResult)
extern void CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A (void);
// 0x000008E0 System.Void Obi.ObiSolver/<>c::.cctor()
extern void U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE (void);
// 0x000008E1 System.Void Obi.ObiSolver/<>c::.ctor()
extern void U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2 (void);
// 0x000008E2 System.Boolean Obi.ObiSolver/<>c::<get_allocParticleCount>b__148_0(Obi.ObiSolver/ParticleInActor)
extern void U3CU3Ec_U3Cget_allocParticleCountU3Eb__148_0_mDB5D6EA59542D260F8AA00864066AC567AF3613B (void);
// 0x000008E3 System.Void Obi.ObiFixedUpdater::OnValidate()
extern void ObiFixedUpdater_OnValidate_m5F6FE3B9B8E80E3B125B39F142D2DA57A4DD8BFB (void);
// 0x000008E4 System.Void Obi.ObiFixedUpdater::OnEnable()
extern void ObiFixedUpdater_OnEnable_m48B49909B75BD8080EE823F221FA21F5C98E8829 (void);
// 0x000008E5 System.Void Obi.ObiFixedUpdater::OnDisable()
extern void ObiFixedUpdater_OnDisable_mEAE5E8BA5B2A650527D99517C37FBF98173259E9 (void);
// 0x000008E6 System.Void Obi.ObiFixedUpdater::FixedUpdate()
extern void ObiFixedUpdater_FixedUpdate_m9CB26DEA62FD0EA6E7FBF86A2CFB12C375B81CE0 (void);
// 0x000008E7 System.Void Obi.ObiFixedUpdater::Update()
extern void ObiFixedUpdater_Update_m485E215A24D5B8A2C3B191F2F65615D4C2A27A90 (void);
// 0x000008E8 System.Void Obi.ObiFixedUpdater::.ctor()
extern void ObiFixedUpdater__ctor_m129BDE7A45025352A7238A317F1A3E7B31A00C9F (void);
// 0x000008E9 System.Void Obi.ObiLateFixedUpdater::OnValidate()
extern void ObiLateFixedUpdater_OnValidate_m4B24969542BF8BAA45ABF2DD0FC90CD910FA27AC (void);
// 0x000008EA System.Void Obi.ObiLateFixedUpdater::OnEnable()
extern void ObiLateFixedUpdater_OnEnable_m7AA55EFB2018C0FE7A7450DE8B124E9947735430 (void);
// 0x000008EB System.Void Obi.ObiLateFixedUpdater::OnDisable()
extern void ObiLateFixedUpdater_OnDisable_m508D776D30650C73FB72E47AEB5F2F7EFB64DFB9 (void);
// 0x000008EC System.Void Obi.ObiLateFixedUpdater::FixedUpdate()
extern void ObiLateFixedUpdater_FixedUpdate_m347A98DB1FFCE6D2654B99E6E228F26BC0646F21 (void);
// 0x000008ED System.Collections.IEnumerator Obi.ObiLateFixedUpdater::RunLateFixedUpdate()
extern void ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB (void);
// 0x000008EE System.Void Obi.ObiLateFixedUpdater::LateFixedUpdate()
extern void ObiLateFixedUpdater_LateFixedUpdate_mC459F999C033D7BC22CD0A0BDA9569E93C2C6F2A (void);
// 0x000008EF System.Void Obi.ObiLateFixedUpdater::Update()
extern void ObiLateFixedUpdater_Update_mE16274CC3FCABACAA0430D79903C2298050907B9 (void);
// 0x000008F0 System.Void Obi.ObiLateFixedUpdater::.ctor()
extern void ObiLateFixedUpdater__ctor_mA0140EBDF2452F0A415601A43DAE9822CCA249C6 (void);
// 0x000008F1 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::.ctor(System.Int32)
extern void U3CRunLateFixedUpdateU3Ed__6__ctor_m3269F746C194951D4FFCB5CFE81A956F7C6913F4 (void);
// 0x000008F2 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.IDisposable.Dispose()
extern void U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_mFEF635C7933D3DD06A06668B01022B79D7348076 (void);
// 0x000008F3 System.Boolean Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::MoveNext()
extern void U3CRunLateFixedUpdateU3Ed__6_MoveNext_m5E828028CAFAD2A27BAD42F54BD73085E6C7F802 (void);
// 0x000008F4 System.Object Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BC60167DF9644F2049A2591D4130F2190A2A4C9 (void);
// 0x000008F5 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_m55403139467BF8AF39A859E7823363583E55162B (void);
// 0x000008F6 System.Object Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_m8E98B873A2E9B9BE3DEE0CA669677BA2ECDFE0FF (void);
// 0x000008F7 System.Void Obi.ObiLateUpdater::OnValidate()
extern void ObiLateUpdater_OnValidate_m6360BD327E35B0C56A590D51CB066B811DAD1774 (void);
// 0x000008F8 System.Void Obi.ObiLateUpdater::Update()
extern void ObiLateUpdater_Update_mC52FDF37EE579A4D667D80C5327FD48FF06A7400 (void);
// 0x000008F9 System.Void Obi.ObiLateUpdater::LateUpdate()
extern void ObiLateUpdater_LateUpdate_m04DE9233394AE2CC13CF1DA4F136364E81FE4C06 (void);
// 0x000008FA System.Void Obi.ObiLateUpdater::.ctor()
extern void ObiLateUpdater__ctor_mD001E04F8FFF222B20A8ECDBE7A8C2507C2F0894 (void);
// 0x000008FB System.Void Obi.ObiUpdater::PrepareFrame()
extern void ObiUpdater_PrepareFrame_m38DA2E247B4C02F4E18CA1E5890493598A019D65 (void);
// 0x000008FC System.Void Obi.ObiUpdater::BeginStep(System.Single)
extern void ObiUpdater_BeginStep_m871A82CAEA561457F1FB19B27ADC346661839AA5 (void);
// 0x000008FD System.Void Obi.ObiUpdater::Substep(System.Single,System.Single,System.Int32)
extern void ObiUpdater_Substep_m44F3765E77796D6D71287FF2883FF2E496E0767F (void);
// 0x000008FE System.Void Obi.ObiUpdater::EndStep(System.Single)
extern void ObiUpdater_EndStep_mD6CEC404A973E002CD4D81FC87442C77F537E55E (void);
// 0x000008FF System.Void Obi.ObiUpdater::Interpolate(System.Single,System.Single)
extern void ObiUpdater_Interpolate_m38784F191B281CBF7A3DE79B71EA71ED2FE60681 (void);
// 0x00000900 System.Void Obi.ObiUpdater::.ctor()
extern void ObiUpdater__ctor_m3A352531659D7BCCB0CF2AFF0732D3B28AB26092 (void);
// 0x00000901 System.Void Obi.ObiUpdater::.cctor()
extern void ObiUpdater__cctor_m272383BDBA28D7F89B9965940A9188BDE81EA1A9 (void);
// 0x00000902 System.Void Obi.ChildrenOnly::.ctor()
extern void ChildrenOnly__ctor_m4D3FAAA742E61B7FA2323CE9F448A1877F87CAE0 (void);
// 0x00000903 System.Void Obi.Indent::.ctor()
extern void Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7 (void);
// 0x00000904 System.Single Obi.InspectorButtonAttribute::get_ButtonWidth()
extern void InspectorButtonAttribute_get_ButtonWidth_mE4C1BF731F1203ABCDC7FF911FBC9AA619ECC4FB (void);
// 0x00000905 System.Void Obi.InspectorButtonAttribute::set_ButtonWidth(System.Single)
extern void InspectorButtonAttribute_set_ButtonWidth_mD7A12A99D234B3911E6D035C0224BC6AB4CC773A (void);
// 0x00000906 System.Void Obi.InspectorButtonAttribute::.ctor(System.String)
extern void InspectorButtonAttribute__ctor_m975A49E0C15C48813192407A712BDB21DCC86987 (void);
// 0x00000907 System.Void Obi.InspectorButtonAttribute::.cctor()
extern void InspectorButtonAttribute__cctor_mD9D19B7D28E89349026C4FBCAF129D59060E0349 (void);
// 0x00000908 System.Void Obi.MinMaxAttribute::.ctor(System.Single,System.Single)
extern void MinMaxAttribute__ctor_mAEC8FD68CEFC58D324E8BBF941E6940654EF2F78 (void);
// 0x00000909 System.Void Obi.MultiDelayed::.ctor()
extern void MultiDelayed__ctor_mC38D0EAB98C18CE8CCEBCC3767678783367BC0F5 (void);
// 0x0000090A System.Void Obi.MultiPropertyAttribute::.ctor()
extern void MultiPropertyAttribute__ctor_m0CEA425DBF3D690C2FBF18251EA3D45B8A2EEEFD (void);
// 0x0000090B System.Void Obi.MultiRange::.ctor(System.Single,System.Single)
extern void MultiRange__ctor_mB97A29FE677F7549CF13B4C38DEAA868640876C1 (void);
// 0x0000090C System.String Obi.SerializeProperty::get_PropertyName()
extern void SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21 (void);
// 0x0000090D System.Void Obi.SerializeProperty::set_PropertyName(System.String)
extern void SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55 (void);
// 0x0000090E System.Void Obi.SerializeProperty::.ctor(System.String)
extern void SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3 (void);
// 0x0000090F System.String Obi.VisibleIf::get_MethodName()
extern void VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79 (void);
// 0x00000910 System.Void Obi.VisibleIf::set_MethodName(System.String)
extern void VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD (void);
// 0x00000911 System.Boolean Obi.VisibleIf::get_Negate()
extern void VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C (void);
// 0x00000912 System.Void Obi.VisibleIf::set_Negate(System.Boolean)
extern void VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E (void);
// 0x00000913 System.Void Obi.VisibleIf::.ctor(System.String,System.Boolean)
extern void VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6 (void);
// 0x00000914 System.Object Obi.CoroutineJob::get_Result()
extern void CoroutineJob_get_Result_mD748A43832803B6F5A24988519355E0B5E960BD3 (void);
// 0x00000915 System.Boolean Obi.CoroutineJob::get_IsDone()
extern void CoroutineJob_get_IsDone_m4278531E6DBB4F3ED38BEB44A7D3365F6008420B (void);
// 0x00000916 System.Boolean Obi.CoroutineJob::get_RaisedException()
extern void CoroutineJob_get_RaisedException_mB7118E9ABE0C6CC7C00D741FC8BC5E859CC81F9A (void);
// 0x00000917 System.Void Obi.CoroutineJob::Init()
extern void CoroutineJob_Init_m08092162CA93D4B06D6F23B48138F018CA204416 (void);
// 0x00000918 System.Object Obi.CoroutineJob::RunSynchronously(System.Collections.IEnumerator)
extern void CoroutineJob_RunSynchronously_mC0C37C33DF39624CE6811D4D2229BF1DBB004DAB (void);
// 0x00000919 System.Collections.IEnumerator Obi.CoroutineJob::Start(System.Collections.IEnumerator)
extern void CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F (void);
// 0x0000091A System.Void Obi.CoroutineJob::Stop()
extern void CoroutineJob_Stop_m503EBD4DCD2DB059CA0681D562A66E37442C7E4B (void);
// 0x0000091B System.Void Obi.CoroutineJob::.ctor()
extern void CoroutineJob__ctor_m3B1FC813C2F585D8B745A74C9F40059C41C72437 (void);
// 0x0000091C System.Void Obi.CoroutineJob/ProgressInfo::.ctor(System.String,System.Single)
extern void ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1 (void);
// 0x0000091D System.Void Obi.CoroutineJob/<Start>d__15::.ctor(System.Int32)
extern void U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD (void);
// 0x0000091E System.Void Obi.CoroutineJob/<Start>d__15::System.IDisposable.Dispose()
extern void U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1 (void);
// 0x0000091F System.Boolean Obi.CoroutineJob/<Start>d__15::MoveNext()
extern void U3CStartU3Ed__15_MoveNext_mF349A26AF1025AF7487E1BB1D725340CD887C18F (void);
// 0x00000920 System.Object Obi.CoroutineJob/<Start>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55 (void);
// 0x00000921 System.Void Obi.CoroutineJob/<Start>d__15::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3 (void);
// 0x00000922 System.Object Obi.CoroutineJob/<Start>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283 (void);
// 0x00000923 System.Void Obi.EditorCoroutine::ShowCoroutineProgressBar(System.String,System.Collections.IEnumerator&)
extern void EditorCoroutine_ShowCoroutineProgressBar_mBD8BA0BAB4A8F6DBA32D5EB38A2AC2598992E522 (void);
// 0x00000924 System.Void Obi.EditorCoroutine::.ctor()
extern void EditorCoroutine__ctor_mDA14C1513A94EABA131D822C5BC96C78411A872E (void);
// 0x00000925 System.Void Obi.ObiAmbientForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiAmbientForceZone_ApplyForcesToActor_mAE4A4FC41002FA3D906FEA74194C377D2C60A8D8 (void);
// 0x00000926 System.Void Obi.ObiAmbientForceZone::OnDrawGizmosSelected()
extern void ObiAmbientForceZone_OnDrawGizmosSelected_m9F4A990AC63D31EFB4E459DDDE79DA6D99ED98DE (void);
// 0x00000927 System.Void Obi.ObiAmbientForceZone::.ctor()
extern void ObiAmbientForceZone__ctor_mD887D3FBC82BCD8DF7720A9B25BF72572118351E (void);
// 0x00000928 System.Void Obi.ObiExternalForce::OnEnable()
extern void ObiExternalForce_OnEnable_m0FB8D535D2ED4F394E48424E48C073B8E3FBD10F (void);
// 0x00000929 System.Void Obi.ObiExternalForce::OnDisable()
extern void ObiExternalForce_OnDisable_mD1867136686D828057C7DF70B6A77B5DE81E459C (void);
// 0x0000092A System.Void Obi.ObiExternalForce::Solver_OnStepBegin(Obi.ObiSolver,System.Single)
extern void ObiExternalForce_Solver_OnStepBegin_mC46587312AE49CD7183DAD9D16BBB66C9BF64E0E (void);
// 0x0000092B System.Single Obi.ObiExternalForce::GetTurbulence(System.Single)
extern void ObiExternalForce_GetTurbulence_m36B324BC86245A69B5FFB5D924E08E35BCEC135D (void);
// 0x0000092C System.Void Obi.ObiExternalForce::ApplyForcesToActor(Obi.ObiActor)
// 0x0000092D System.Void Obi.ObiExternalForce::.ctor()
extern void ObiExternalForce__ctor_mD434F0876366D23C0D68DAB6A8949E018A42ACBD (void);
// 0x0000092E System.Void Obi.ObiSphericalForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiSphericalForceZone_ApplyForcesToActor_m506A142D9EF2B82CD7310E150EF302F8A6B2FA2B (void);
// 0x0000092F System.Void Obi.ObiSphericalForceZone::OnDrawGizmosSelected()
extern void ObiSphericalForceZone_OnDrawGizmosSelected_m428130DB0C2E2D23A31E3D0FFE00EDA0FB6744B2 (void);
// 0x00000930 System.Void Obi.ObiSphericalForceZone::.ctor()
extern void ObiSphericalForceZone__ctor_mCB4320E2FDC045FAC6D25069AA629243434775F0 (void);
// 0x00000931 System.Int32 Obi.ObiContactEventDispatcher::CompareByRef(Oni/Contact&,Oni/Contact&,Obi.ObiSolver)
extern void ObiContactEventDispatcher_CompareByRef_mCC27E2711928972C0B16573985711C3432D1719E (void);
// 0x00000932 System.Void Obi.ObiContactEventDispatcher::Awake()
extern void ObiContactEventDispatcher_Awake_m4C9DC422D39BD21B3BE61C3BE4EB262BD0FECAD9 (void);
// 0x00000933 System.Void Obi.ObiContactEventDispatcher::OnEnable()
extern void ObiContactEventDispatcher_OnEnable_m9E49028DB875725C353943480CB5C447179953DC (void);
// 0x00000934 System.Void Obi.ObiContactEventDispatcher::OnDisable()
extern void ObiContactEventDispatcher_OnDisable_m8F75848B0553FD8776A22ABD1253D891B592CFA0 (void);
// 0x00000935 System.Int32 Obi.ObiContactEventDispatcher::FilterOutDistantContacts(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_FilterOutDistantContacts_m2088093F9BFC093F73ECB24BDC9E6DA7A748962B (void);
// 0x00000936 System.Int32 Obi.ObiContactEventDispatcher::RemoveDuplicates(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_RemoveDuplicates_m394D908408DBDED555F844001682FD217E94C5F9 (void);
// 0x00000937 System.Void Obi.ObiContactEventDispatcher::InvokeCallbacks(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_InvokeCallbacks_mB588492B596248DBC043C8235D2AD8D3AC3AC423 (void);
// 0x00000938 System.Void Obi.ObiContactEventDispatcher::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiContactEventDispatcher_Solver_OnCollision_m12E8F9442701D7911DE4F0D18CB0F4EC82F263FC (void);
// 0x00000939 System.Void Obi.ObiContactEventDispatcher::.ctor()
extern void ObiContactEventDispatcher__ctor_m357D739552531DA027D8F320CCFAC444814BD791 (void);
// 0x0000093A System.Void Obi.ObiContactEventDispatcher/ContactComparer::.ctor(Obi.ObiSolver)
extern void ContactComparer__ctor_m5DC2082E03C9DA3062C2D13D3F1702B3763C4379 (void);
// 0x0000093B System.Int32 Obi.ObiContactEventDispatcher/ContactComparer::Compare(Oni/Contact,Oni/Contact)
extern void ContactComparer_Compare_mB97171F79E54851FEF29ACC2ED3F06C5A1452362 (void);
// 0x0000093C System.Void Obi.ObiContactEventDispatcher/ContactCallback::.ctor()
extern void ContactCallback__ctor_m77F3DD3B53374589F094279143B1638E57919AD3 (void);
// 0x0000093D Obi.ObiActor Obi.ObiParticleAttachment::get_actor()
extern void ObiParticleAttachment_get_actor_m8A6C711886370813BD7BC301BFB6C7812B6B193A (void);
// 0x0000093E UnityEngine.Transform Obi.ObiParticleAttachment::get_target()
extern void ObiParticleAttachment_get_target_m87F09D825C3B834F3F32869BA35539B4525FA08E (void);
// 0x0000093F System.Void Obi.ObiParticleAttachment::set_target(UnityEngine.Transform)
extern void ObiParticleAttachment_set_target_m05F5DB1086FA979873CCECE322EDE77C26550DF7 (void);
// 0x00000940 Obi.ObiParticleGroup Obi.ObiParticleAttachment::get_particleGroup()
extern void ObiParticleAttachment_get_particleGroup_mE66342EC326E748311096EBF6508598B2D0E9CC1 (void);
// 0x00000941 System.Void Obi.ObiParticleAttachment::set_particleGroup(Obi.ObiParticleGroup)
extern void ObiParticleAttachment_set_particleGroup_mD2ACDCEDFFDECA0B1737C92F4E2941CD93BEFDE7 (void);
// 0x00000942 System.Boolean Obi.ObiParticleAttachment::get_isBound()
extern void ObiParticleAttachment_get_isBound_mD1067521C804A334B7A28CA3A9BD65237C0FD29D (void);
// 0x00000943 Obi.ObiParticleAttachment/AttachmentType Obi.ObiParticleAttachment::get_attachmentType()
extern void ObiParticleAttachment_get_attachmentType_m3AE549A02BCF14D72CE6EDCC93541FD03EA3B271 (void);
// 0x00000944 System.Void Obi.ObiParticleAttachment::set_attachmentType(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_set_attachmentType_m5631ABD4AF1A1AFB91F5851BF869AD3974C2586F (void);
// 0x00000945 System.Boolean Obi.ObiParticleAttachment::get_constrainOrientation()
extern void ObiParticleAttachment_get_constrainOrientation_mD07EF64C3AE8F1F9CE99F747CF0D12C2DE7D6E1D (void);
// 0x00000946 System.Void Obi.ObiParticleAttachment::set_constrainOrientation(System.Boolean)
extern void ObiParticleAttachment_set_constrainOrientation_m17A07949BE7D8BBF51FCE020DFB2B07DA73A722F (void);
// 0x00000947 System.Single Obi.ObiParticleAttachment::get_compliance()
extern void ObiParticleAttachment_get_compliance_mF04CA0E1C82403AC5039D8F3816CB750E16E22D9 (void);
// 0x00000948 System.Void Obi.ObiParticleAttachment::set_compliance(System.Single)
extern void ObiParticleAttachment_set_compliance_m559FB99156912E50F36AE6239AEEF8DCE995A132 (void);
// 0x00000949 System.Single Obi.ObiParticleAttachment::get_breakThreshold()
extern void ObiParticleAttachment_get_breakThreshold_mA89F93A18D03F1DADE699A97634716D15617B02F (void);
// 0x0000094A System.Void Obi.ObiParticleAttachment::set_breakThreshold(System.Single)
extern void ObiParticleAttachment_set_breakThreshold_mF3F55D89A7E9E6EEF600D372A34DB537A752EB06 (void);
// 0x0000094B System.Void Obi.ObiParticleAttachment::OnEnable()
extern void ObiParticleAttachment_OnEnable_mE67B8CA340675DC78907D05D206A0B32B47C8F6E (void);
// 0x0000094C System.Void Obi.ObiParticleAttachment::OnDisable()
extern void ObiParticleAttachment_OnDisable_m970C2F665946F9526288866F57E00845E57F6231 (void);
// 0x0000094D System.Void Obi.ObiParticleAttachment::OnValidate()
extern void ObiParticleAttachment_OnValidate_m8F4069C273A48281199B02A40AECFF1D83EA0C32 (void);
// 0x0000094E System.Void Obi.ObiParticleAttachment::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiParticleAttachment_Actor_OnBlueprintLoaded_m5736115ED7F6C5E1935D2217E7E5DBFDB5B20487 (void);
// 0x0000094F System.Void Obi.ObiParticleAttachment::Actor_OnPrepareStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnPrepareStep_m9EA64FC23D3365CA10F37123637E56D333D6A2A8 (void);
// 0x00000950 System.Void Obi.ObiParticleAttachment::Actor_OnEndStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnEndStep_m09B95B61E9325F33E9DF9B85945979C11962E0C1 (void);
// 0x00000951 System.Void Obi.ObiParticleAttachment::Bind()
extern void ObiParticleAttachment_Bind_m41671D060988813057E5D6CB710621B387C3B4C7 (void);
// 0x00000952 System.Void Obi.ObiParticleAttachment::EnableAttachment(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_EnableAttachment_mB6BAA968F14899C9E41D3827506F5A05E45247CF (void);
// 0x00000953 System.Void Obi.ObiParticleAttachment::DisableAttachment(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_DisableAttachment_m91236D7F5CFA0439C6EB8D250816CB587AD6392A (void);
// 0x00000954 System.Void Obi.ObiParticleAttachment::UpdateAttachment()
extern void ObiParticleAttachment_UpdateAttachment_m5537ED01556A4DC62E3F46AB5D43A9E2C875856A (void);
// 0x00000955 System.Void Obi.ObiParticleAttachment::BreakDynamicAttachment(System.Single)
extern void ObiParticleAttachment_BreakDynamicAttachment_m3AA55F0956AA144B3AC657224DF303EBACBB9022 (void);
// 0x00000956 System.Void Obi.ObiParticleAttachment::.ctor()
extern void ObiParticleAttachment__ctor_mAC181D9F03BE52C0DB29C07F539A74C0DEA1744B (void);
// 0x00000957 System.Void Obi.ObiParticleDragger::OnEnable()
extern void ObiParticleDragger_OnEnable_m72B8C23953592A731785106A8459284845816A55 (void);
// 0x00000958 System.Void Obi.ObiParticleDragger::OnDisable()
extern void ObiParticleDragger_OnDisable_mB11CAEAA8554518A0B474976561C8FEF0D6E8E49 (void);
// 0x00000959 System.Void Obi.ObiParticleDragger::FixedUpdate()
extern void ObiParticleDragger_FixedUpdate_m9851F3C063BF90687B18E10701B7B1F5193A8E6E (void);
// 0x0000095A System.Void Obi.ObiParticleDragger::Picker_OnParticleDragged(Obi.ObiParticlePicker/ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleDragged_mECCECAF2424C1D2656A6601ED602BFEA343EBB7C (void);
// 0x0000095B System.Void Obi.ObiParticleDragger::Picker_OnParticleReleased(Obi.ObiParticlePicker/ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleReleased_mD5AFBB9569AFDD6F33D4E148A5A63B542331C87C (void);
// 0x0000095C System.Void Obi.ObiParticleDragger::.ctor()
extern void ObiParticleDragger__ctor_mC7F5FD7D2B50D344D2D85CDA6CB55EA76DFD7A73 (void);
// 0x0000095D System.Void Obi.ObiParticleGridDebugger::OnEnable()
extern void ObiParticleGridDebugger_OnEnable_mDCE0CF67BDC98D81F9FB3B4AA3DA73485F24A8E3 (void);
// 0x0000095E System.Void Obi.ObiParticleGridDebugger::OnDisable()
extern void ObiParticleGridDebugger_OnDisable_m6EEAA88C141739F08763B496873E4FB8583ABA2D (void);
// 0x0000095F System.Void Obi.ObiParticleGridDebugger::LateUpdate()
extern void ObiParticleGridDebugger_LateUpdate_m7A114715F2660DE5C8004F414580ABAB4B6BD796 (void);
// 0x00000960 System.Void Obi.ObiParticleGridDebugger::OnDrawGizmos()
extern void ObiParticleGridDebugger_OnDrawGizmos_mFF5DD489989676AD3C6BACA195FE47F4E18C22AC (void);
// 0x00000961 System.Void Obi.ObiParticleGridDebugger::.ctor()
extern void ObiParticleGridDebugger__ctor_mC3547171B3F6C6C08F8E759186D7007E186A29B8 (void);
// 0x00000962 System.Void Obi.ObiParticlePicker::Awake()
extern void ObiParticlePicker_Awake_m74E07553D81756450A0D1B8351E1CBF8142F8F11 (void);
// 0x00000963 System.Void Obi.ObiParticlePicker::LateUpdate()
extern void ObiParticlePicker_LateUpdate_m9E75050EBAC6B52C81851303DEE0B2EA70353CF1 (void);
// 0x00000964 System.Void Obi.ObiParticlePicker::.ctor()
extern void ObiParticlePicker__ctor_m5818ABF9055AD22D294ED1EB13021BC9FE8AB3A9 (void);
// 0x00000965 System.Void Obi.ObiParticlePicker/ParticlePickEventArgs::.ctor(System.Int32,UnityEngine.Vector3)
extern void ParticlePickEventArgs__ctor_m64293DD03DECF493CE8EBD7844C21D2746C09EF0 (void);
// 0x00000966 System.Void Obi.ObiParticlePicker/ParticlePickUnityEvent::.ctor()
extern void ParticlePickUnityEvent__ctor_m8E79C656B1CDE3FB655B899990D215C91879BFA0 (void);
// 0x00000967 System.Void Obi.ObiProfiler::Awake()
extern void ObiProfiler_Awake_mAB7F0E70026EC600D00A9130AEA40B9F2DD4D0D7 (void);
// 0x00000968 System.Void Obi.ObiProfiler::OnDestroy()
extern void ObiProfiler_OnDestroy_m952E74CFF016FB1DB495D76228352ABED2E56166 (void);
// 0x00000969 System.Void Obi.ObiProfiler::OnEnable()
extern void ObiProfiler_OnEnable_m2FA73A238837F2034B8E2907F2E87C347A07EFDC (void);
// 0x0000096A System.Void Obi.ObiProfiler::OnDisable()
extern void ObiProfiler_OnDisable_m677BD3207054B10CDE9BAFA6EDC6D7C52DAD7B09 (void);
// 0x0000096B System.Void Obi.ObiProfiler::EnableProfiler()
extern void ObiProfiler_EnableProfiler_mE07D4D3052ADC88FA8D0D882DF933E094D9F25FD (void);
// 0x0000096C System.Void Obi.ObiProfiler::DisableProfiler()
extern void ObiProfiler_DisableProfiler_m6E225ACE957C1D38336D2C4FFEEBAC52540241E9 (void);
// 0x0000096D System.Void Obi.ObiProfiler::BeginSample(System.String,System.Byte)
extern void ObiProfiler_BeginSample_mCA17FA232FD590BF488C6304FC5C4B4B77105DEC (void);
// 0x0000096E System.Void Obi.ObiProfiler::EndSample()
extern void ObiProfiler_EndSample_mA76E83B9E7BB9DCEA7626780607D999D34868425 (void);
// 0x0000096F System.Void Obi.ObiProfiler::UpdateProfilerInfo()
extern void ObiProfiler_UpdateProfilerInfo_m89B2E0A641CDEA86AB5195C8CB8CDE7ED666B9DF (void);
// 0x00000970 System.Void Obi.ObiProfiler::OnGUI()
extern void ObiProfiler_OnGUI_m975F6EE95578505D3E6F1792AD1AFBBAD1375139 (void);
// 0x00000971 System.Void Obi.ObiProfiler::.ctor()
extern void ObiProfiler__ctor_m3EEB8D1C03DFE08C0EC0E23E334C190F41E40424 (void);
// 0x00000972 System.Void Obi.ObiStitcher::set_Actor1(Obi.ObiActor)
extern void ObiStitcher_set_Actor1_mABB05BE8A26AE51871FE07C7EAA9D53359016679 (void);
// 0x00000973 Obi.ObiActor Obi.ObiStitcher::get_Actor1()
extern void ObiStitcher_get_Actor1_mF7B8097B74CDE0D5491AA8CA0BFB9EA6F6521DE7 (void);
// 0x00000974 System.Void Obi.ObiStitcher::set_Actor2(Obi.ObiActor)
extern void ObiStitcher_set_Actor2_m8D4021102EDF55C787F2AFB5CBD83AEA4443C95E (void);
// 0x00000975 Obi.ObiActor Obi.ObiStitcher::get_Actor2()
extern void ObiStitcher_get_Actor2_mF6FCD44A6E57935EF04782FE6E80FF30A500356E (void);
// 0x00000976 System.Int32 Obi.ObiStitcher::get_StitchCount()
extern void ObiStitcher_get_StitchCount_m0A3C3380AA1B0536814453807186D85002F57744 (void);
// 0x00000977 System.Collections.Generic.IEnumerable`1<Obi.ObiStitcher/Stitch> Obi.ObiStitcher::get_Stitches()
extern void ObiStitcher_get_Stitches_mD04C880005B8B5F6261CFF2E5ABB4C713C27CDFA (void);
// 0x00000978 System.Void Obi.ObiStitcher::RegisterActor(Obi.ObiActor)
extern void ObiStitcher_RegisterActor_m6CF833604BDC8434EA68BD461456A891CCE1A6B6 (void);
// 0x00000979 System.Void Obi.ObiStitcher::UnregisterActor(Obi.ObiActor)
extern void ObiStitcher_UnregisterActor_m1C06E9E8003BEAEF75F59ED25B8B56561E5D4A4C (void);
// 0x0000097A System.Void Obi.ObiStitcher::OnEnable()
extern void ObiStitcher_OnEnable_mDEE87492FEFF02F28EC6D8CB231D8DA3893A8BBC (void);
// 0x0000097B System.Void Obi.ObiStitcher::OnDisable()
extern void ObiStitcher_OnDisable_m44319848A4EA3CB9AF55AE503389703F57250C00 (void);
// 0x0000097C System.Int32 Obi.ObiStitcher::AddStitch(System.Int32,System.Int32)
extern void ObiStitcher_AddStitch_m9CDD6C005F4A71C8E942AFCF0EE588AC47CBA1D7 (void);
// 0x0000097D System.Void Obi.ObiStitcher::RemoveStitch(System.Int32)
extern void ObiStitcher_RemoveStitch_m050637591BDE1882809E3902CD2E7C0FF62EB0CC (void);
// 0x0000097E System.Void Obi.ObiStitcher::Clear()
extern void ObiStitcher_Clear_mC2BBA753DCB0899C7C486C20565D05F6ED1F1B2F (void);
// 0x0000097F System.Void Obi.ObiStitcher::Actor_OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintUnloaded_m6CE4420D85CBD27371B1BCD548FC50F983C7DDC6 (void);
// 0x00000980 System.Void Obi.ObiStitcher::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintLoaded_mF598296F75D17D0F818E4D7F7A1E98F7F3B8200A (void);
// 0x00000981 System.Void Obi.ObiStitcher::AddToSolver(Obi.ObiSolver)
extern void ObiStitcher_AddToSolver_mACC6329F09C6AC34703CC2C3CDCD6531D7ADCA63 (void);
// 0x00000982 System.Void Obi.ObiStitcher::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStitcher_RemoveFromSolver_m5B3C5E8739E3393D3654A34D0D84972D7582C457 (void);
// 0x00000983 System.Void Obi.ObiStitcher::PushDataToSolver()
extern void ObiStitcher_PushDataToSolver_mA3B052D96FECDB53E4616B27E72A44F77A88EB31 (void);
// 0x00000984 System.Void Obi.ObiStitcher::.ctor()
extern void ObiStitcher__ctor_m8703E582D33DF3E6FB4265F91A5061420D984F97 (void);
// 0x00000985 System.Void Obi.ObiStitcher/Stitch::.ctor(System.Int32,System.Int32)
extern void Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800 (void);
// 0x00000986 System.Void Obi.ObiUtils::DrawArrowGizmo(System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_DrawArrowGizmo_mB0F16371F7CA701F1934AB1D68E2C543BBEDC6D7 (void);
// 0x00000987 System.Void Obi.ObiUtils::DebugDrawCross(UnityEngine.Vector3,System.Single,UnityEngine.Color)
extern void ObiUtils_DebugDrawCross_m56B5756A7B53EF07A54DA1521D785F4B447A0794 (void);
// 0x00000988 System.Void Obi.ObiUtils::Swap(T&,T&)
// 0x00000989 System.Void Obi.ObiUtils::Swap(T[],System.Int32,System.Int32)
// 0x0000098A System.Void Obi.ObiUtils::Swap(System.Collections.Generic.IList`1<T>,System.Int32,System.Int32)
// 0x0000098B System.Void Obi.ObiUtils::ShiftLeft(T[],System.Int32,System.Int32,System.Int32)
// 0x0000098C System.Void Obi.ObiUtils::ShiftRight(T[],System.Int32,System.Int32,System.Int32)
// 0x0000098D System.Boolean Obi.ObiUtils::AreValid(UnityEngine.Bounds)
extern void ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6 (void);
// 0x0000098E UnityEngine.Bounds Obi.ObiUtils::Transform(UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern void ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88 (void);
// 0x0000098F System.Int32 Obi.ObiUtils::CountTrailingZeroes(System.Int32)
extern void ObiUtils_CountTrailingZeroes_mD04646F64AE816B0BBE5FE3044C2DED6E90A0333 (void);
// 0x00000990 System.Void Obi.ObiUtils::Add(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_Add_m33CB21C40D66A37C46501B8D0F17D03411F7A6B8 (void);
// 0x00000991 System.Single Obi.ObiUtils::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F (void);
// 0x00000992 System.Single Obi.ObiUtils::Mod(System.Single,System.Single)
extern void ObiUtils_Mod_mD2EF1D694D52B0B1882C801A2FD576D8FC49D90E (void);
// 0x00000993 UnityEngine.Matrix4x4 Obi.ObiUtils::Add(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern void ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4 (void);
// 0x00000994 System.Single Obi.ObiUtils::FrobeniusNorm(UnityEngine.Matrix4x4)
extern void ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4 (void);
// 0x00000995 UnityEngine.Matrix4x4 Obi.ObiUtils::ScalarMultiply(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51 (void);
// 0x00000996 UnityEngine.Vector3 Obi.ObiUtils::ProjectPointLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Boolean)
extern void ObiUtils_ProjectPointLine_m39534F2B61F19A53C004F9F72C2D5561C3BFCA15 (void);
// 0x00000997 System.Boolean Obi.ObiUtils::LinePlaneIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_LinePlaneIntersection_mA331CE0E861BFF21738B92A85B6009EA4641AD91 (void);
// 0x00000998 System.Single Obi.ObiUtils::RaySphereIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiUtils_RaySphereIntersection_m878269850BD66F1202937DEFF22C3A31ED3B5996 (void);
// 0x00000999 System.Single Obi.ObiUtils::InvMassToMass(System.Single)
extern void ObiUtils_InvMassToMass_m1ECAAAEDD4E27ADA1FDE1594DACCD249742ED136 (void);
// 0x0000099A System.Single Obi.ObiUtils::MassToInvMass(System.Single)
extern void ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68 (void);
// 0x0000099B System.Int32 Obi.ObiUtils::PureSign(System.Single)
extern void ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5 (void);
// 0x0000099C System.Void Obi.ObiUtils::NearestPointOnTri(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED (void);
// 0x0000099D System.Single Obi.ObiUtils::TriangleArea(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_TriangleArea_m560C3409898B4D67699FCFD0338C3ECDB244BCEB (void);
// 0x0000099E System.Single Obi.ObiUtils::EllipsoidVolume(UnityEngine.Vector3)
extern void ObiUtils_EllipsoidVolume_m406A1FFC6FADF93B25B2B072064B345E5DA5D2C5 (void);
// 0x0000099F UnityEngine.Quaternion Obi.ObiUtils::RestDarboux(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void ObiUtils_RestDarboux_mA5FBE38C5DA10FBD3F920E2F0E96683D3EF91507 (void);
// 0x000009A0 System.Single Obi.ObiUtils::RestBendingConstraint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78 (void);
// 0x000009A1 System.Collections.IEnumerable Obi.ObiUtils::BilateralInterleaved(System.Int32)
extern void ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453 (void);
// 0x000009A2 System.Void Obi.ObiUtils::BarycentricCoordinates(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197 (void);
// 0x000009A3 System.Void Obi.ObiUtils::BarycentricInterpolation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1 (void);
// 0x000009A4 System.Single Obi.ObiUtils::BarycentricInterpolation(System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern void ObiUtils_BarycentricInterpolation_mAC20CA1234A3FB1D1D9AB2AA29D7987C0DF12361 (void);
// 0x000009A5 System.Single Obi.ObiUtils::BarycentricExtrapolationScale(UnityEngine.Vector3)
extern void ObiUtils_BarycentricExtrapolationScale_m84AD9B6B2173A5538FDB174D72AA5CE127045D33 (void);
// 0x000009A6 UnityEngine.Vector3[] Obi.ObiUtils::CalculateAngleWeightedNormals(UnityEngine.Vector3[],System.Int32[])
extern void ObiUtils_CalculateAngleWeightedNormals_m97CD00C800AA1BD2705A48113DD44B57E450DD34 (void);
// 0x000009A7 System.Int32 Obi.ObiUtils::MakePhase(System.Int32,Obi.ObiUtils/ParticleFlags)
extern void ObiUtils_MakePhase_mED5844CB11913FC70EC9041F512C75E0B2F4E167 (void);
// 0x000009A8 System.Int32 Obi.ObiUtils::GetGroupFromPhase(System.Int32)
extern void ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054 (void);
// 0x000009A9 Obi.ObiUtils/ParticleFlags Obi.ObiUtils::GetFlagsFromPhase(System.Int32)
extern void ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57 (void);
// 0x000009AA System.Int32 Obi.ObiUtils::MakeFilter(System.Int32,System.Int32)
extern void ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3 (void);
// 0x000009AB System.Int32 Obi.ObiUtils::GetCategoryFromFilter(System.Int32)
extern void ObiUtils_GetCategoryFromFilter_mE5495C835432A19EE7A32B4E72D104A5179AD9ED (void);
// 0x000009AC System.Int32 Obi.ObiUtils::GetMaskFromFilter(System.Int32)
extern void ObiUtils_GetMaskFromFilter_mD6E7EF064AF96C8238D05A7EB5504E72CA1ECFCD (void);
// 0x000009AD System.Void Obi.ObiUtils::EigenSolve(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern void ObiUtils_EigenSolve_m52D85DC48A8905EEFA96A9E0618B6569757370F7 (void);
// 0x000009AE UnityEngine.Vector3 Obi.ObiUtils::unitOrthogonal(UnityEngine.Vector3)
extern void ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529 (void);
// 0x000009AF UnityEngine.Vector3 Obi.ObiUtils::EigenVector(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_EigenVector_m650F505BE39681115F5A78F3AB4C76D6B766E360 (void);
// 0x000009B0 UnityEngine.Vector3 Obi.ObiUtils::EigenValues(UnityEngine.Matrix4x4)
extern void ObiUtils_EigenValues_m4B78523278F5C36F028CC1E7AB8FA13BE10FB476 (void);
// 0x000009B1 UnityEngine.Vector3 Obi.ObiUtils::GetPointCloudCentroid(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiUtils_GetPointCloudCentroid_m8C1020D867B29A5A516F0DF741330765D4B47BB5 (void);
// 0x000009B2 System.Void Obi.ObiUtils::GetPointCloudAnisotropy(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2 (void);
// 0x000009B3 System.Void Obi.ObiUtils::.cctor()
extern void ObiUtils__cctor_mE9D4CD7AA4BC90F980D0AC4F4DA0B543DFD75D01 (void);
// 0x000009B4 System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::.ctor(System.Int32)
extern void U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10 (void);
// 0x000009B5 System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.IDisposable.Dispose()
extern void U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C (void);
// 0x000009B6 System.Boolean Obi.ObiUtils/<BilateralInterleaved>d__40::MoveNext()
extern void U3CBilateralInterleavedU3Ed__40_MoveNext_m77C2C019B232853A6CC9102657015E05AC815D61 (void);
// 0x000009B7 System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126 (void);
// 0x000009B8 System.Void Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.Reset()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE (void);
// 0x000009B9 System.Object Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474 (void);
// 0x000009BA System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279 (void);
// 0x000009BB System.Collections.IEnumerator Obi.ObiUtils/<BilateralInterleaved>d__40::System.Collections.IEnumerable.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6 (void);
// 0x000009BC System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_m23CD1AEE1CC6B74D7930C98AA1D280513238A553 (void);
// 0x000009BD System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Cross_mA47A8583DC381295371BD66F8397DD209C2B7731 (void);
// 0x000009BE System.Void Obi.ObiVectorMath::Cross(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_m4C1FC1059C8BF006437BAA9D3CBBCD7323016E19 (void);
// 0x000009BF System.Void Obi.ObiVectorMath::Subtract(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Subtract_mD703C48EC9EAAAF9901BCB1F1D0BF46E23A8925A (void);
// 0x000009C0 System.Void Obi.ObiVectorMath::BarycentricInterpolation(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3&)
extern void ObiVectorMath_BarycentricInterpolation_m7CD9E0E0E11533DEB84C7003A1FBC5E415B6CAD4 (void);
// 0x000009C1 System.Void Obi.SetCategory::Awake()
extern void SetCategory_Awake_mC91A0F14152A3306C3D58FFDB3268FC42CA75A1B (void);
// 0x000009C2 System.Void Obi.SetCategory::OnDestroy()
extern void SetCategory_OnDestroy_m7E095279FCAA7AC75B26DBC31335E8C1B6156A5F (void);
// 0x000009C3 System.Void Obi.SetCategory::OnValidate()
extern void SetCategory_OnValidate_m96641FEA78031AF96A7904C7B89F5526989E129A (void);
// 0x000009C4 System.Void Obi.SetCategory::OnLoad(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void SetCategory_OnLoad_m105F18C77751C59F67759FE5006D36A29B4480AF (void);
// 0x000009C5 System.Void Obi.SetCategory::.ctor()
extern void SetCategory__ctor_m5F2C7ADC7EAEF4C3E7B89A74858C43C67F3F9AD5 (void);
static Il2CppMethodPointer s_methodPointers[2501] = 
{
	EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C,
	IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D,
	IsUnmanagedAttribute__ctor_m2C7592EF4A769E56015DDE4C3669E0B9B7042AEF,
	NULL,
	NULL,
	GridHash_Hash_m4DB19DB7B792B51169CF490DE8087E17100D38A5,
	GridHash_Quantize_m67808299EA16142EAEEEAF0E4D4B9B4CADA9E320,
	GridHash_Hash_mE1AA8FB3CE469C0D1D7D83404CAA0C97BEF6E24A,
	GridHash_Quantize_m865B39F8CE4C3A0091ACD6ADCB0656BE54442284,
	GridHash_Hash_m755039C35AA0B62A86AD1D19A0C15CE7ACAA064D,
	GridHash_Hash_m835A98B5B854051EEB7B912A554F24CB46B0D259,
	GridHash_Hash_m8F60FDB29079F3C1B3FB38496CCF27681AE19159,
	GridHash__cctor_mCF343F9A0800C2F30089E283179AAA2538BE52F0,
	ObiContactGrabber_get_grabbed_mB390126778E0A789B441B372D5F525DB318BC301,
	ObiContactGrabber_Awake_m81FAB1A7D0894DFF410B881A4BD34C377CCDF33D,
	ObiContactGrabber_OnEnable_m0CDF84CEFA0997E0C363D99406A72B7F6617015D,
	ObiContactGrabber_OnDisable_m70E63109356461451716F2656B1F703ABDF30B67,
	ObiContactGrabber_Solver_OnCollision_m02CB781C94F8F518ADC1B61E01487BBDC62BA026,
	ObiContactGrabber_UpdateParticleProperties_m874B590742CF4C376FDD13678A387918D098B615,
	ObiContactGrabber_GrabParticle_mA112EAB2920DB3B64AA31FCF9455C6F4702858B5,
	ObiContactGrabber_Grab_mF88B7A50127D874AC0295C6D7D98FFEBE6D8F618,
	ObiContactGrabber_Release_m82B5D7B992BC9423026E8CE0BD6DE0884D72BA68,
	ObiContactGrabber_FixedUpdate_mCAD4BA24875E897F95967F4540B06AC26800BF01,
	ObiContactGrabber__ctor_m9C1D048C7D8633509DB48156260CB765A7E6E364,
	GrabbedParticle__ctor_m29866A846BC99D62A7E53A7F55FD6234B7DB7567,
	GrabbedParticle_Equals_mFF2582C3BFDDE99366B35AC5DF73C20ECE8E430D,
	GrabbedParticle_GetHashCode_m37D33E44CE7086BE8CD4AF64B4270BE0FADDACC9,
	Oni_PinMemory_m03E1EA6D2AD2B2501D052E52FDE072606EDEB6EF,
	Oni_UnpinMemory_m029F0BA710E2DBE187A2300ED9F5CFA9DE37B62F,
	Oni_UpdateColliderGrid_m75034617DEA373FD3AAC6D3F57A9530AA426895D,
	Oni_SetColliders_m901960CF23BF96A6DCE6751F09B4425431091A65,
	Oni_SetRigidbodies_mF84FEC7A0C4A850D6CD1F9CF39A29C2D6AFEAECF,
	Oni_SetCollisionMaterials_mAAE3428F89A91AF30BB753E728556AD1F592F3B1,
	Oni_SetTriangleMeshData_mFFF4366C8A8B13EDA5301EDE1CED9A2CD92AE43D,
	Oni_SetEdgeMeshData_m7D6EBD72BA2169E3C697F341B75BC716161524D0,
	Oni_SetDistanceFieldData_mED7CC8724B49A2AF36F817A57152CD95AA6E0200,
	Oni_SetHeightFieldData_m3890F2905A2990080378C1C2F28A9AD2DA077BEE,
	Oni_CreateSolver_m95484CA4C8E4048EB871BB224918540A317C885A,
	Oni_DestroySolver_m0ADEE02261B34DC1C348911FCDC53E4471B37D0B,
	Oni_SetCapacity_m4804EBFCAF115506C8DD5332214C6A2E0CB4810B,
	Oni_InitializeFrame_mB306333E4A867678C088DACC72B039EF58E80D31,
	Oni_UpdateFrame_m680BE80814872FABDBFDC7ABBE5FF1E740C5773D,
	Oni_ApplyFrame_mD136F376FE68D93651459FB1B946CE18BFAA8D10,
	Oni_RecalculateInertiaTensors_m5D778943EF3E20DCF00A85175E39DD691A8AFA16,
	Oni_ResetForces_m38A179189C49CCAC6142D9F38F234BA8D03C5F64,
	Oni_SetRigidbodyLinearDeltas_m3D5B23B5969BC13392B4E7E019D03730E61E3639,
	Oni_SetRigidbodyAngularDeltas_m4A436C37D1F6F9FE39F1184279883463F586CBF0,
	Oni_GetBounds_m363A96EE8BAB3FB760C636B8C5F361B8EE1E53B5,
	Oni_GetParticleGridSize_m2BD9A7939A0F7733323CA9C9EDE32C53FC7B6ABD,
	Oni_GetParticleGrid_m21D4AA3548CF77E25C5E456096E16162C1BAD4C0,
	Oni_SpatialQuery_m526F90093505F1A4FB3E31A2AC6EC6B8C1F850A5,
	Oni_GetQueryResults_mB69A3B8645A8218700E75C49956E15AC27B125B7,
	Oni_SetSolverParameters_mA78FF4C834E23D50A56D989521B278AA6B11BD48,
	Oni_GetSolverParameters_mD299B385DF9770D77C9E0D4F4781EFADE20C30E4,
	Oni_SetActiveParticles_mA8361CFE331459188A7701673465FCEC0770F84B,
	Oni_CollisionDetection_mD890F2AED814E8B7F3766DDD603916EDA4B1B861,
	Oni_Step_m73AE7B946E5053FC14C8061FCD6508D4824213B7,
	Oni_ApplyPositionInterpolation_mA7A631314B43D3A62FAF81D209F1CCB00A3A901D,
	Oni_UpdateSkeletalAnimation_m0F0F49630F0ABF1B6C742D13FEDB8A69B3C71A31,
	Oni_GetConstraintCount_m046D7B8D04B0EDDC24A336FA1AD6F93B181B143A,
	Oni_SetRenderableParticlePositions_mF10457D7F9724C687D3AB6E83DEEBB4C9937F138,
	Oni_SetParticlePhases_m330FC442852B31A851FB05486D9D5D4A9E85080C,
	Oni_SetParticleFilters_m4689F2DCACACC85971902111BF779817074E976C,
	Oni_SetParticleCollisionMaterials_m64562F4539FF5805E44B181ED119B61FB636DB91,
	Oni_SetParticlePositions_mBF4B08453E99AACC5D3C934F3B7F1EDB2AA3934A,
	Oni_SetParticlePreviousPositions_mF7643A2DD9F3D10409DF62601E0D8EE2D0DBD7E4,
	Oni_SetParticleOrientations_m92BF1084EB29939C992A0666D1E75C6AAB14801D,
	Oni_SetParticlePreviousOrientations_m08E1313630723606D179B6FB44025E49B838D2BB,
	Oni_SetRenderableParticleOrientations_mE90A2228D4CDC6641661C730F1164F65609788B8,
	Oni_SetParticleInverseMasses_mA5F3998095FBD44B558C75B634E18628520D2385,
	Oni_SetParticleInverseRotationalMasses_mBB6281669729C39BEF623DDDEDCC12EC93C31FBB,
	Oni_SetParticlePrincipalRadii_mCE141D972ABA98C2CCC7B66419BC0009E227F8CE,
	Oni_SetParticleVelocities_m806F66F28AB0D4E632921B7E0CF3464C777B00C7,
	Oni_SetParticleAngularVelocities_mD376C89032B153B1C2EB36FAF673C90600871052,
	Oni_SetParticleExternalForces_mABE204855B05F30D980984785EFC178E8178657B,
	Oni_SetParticleExternalTorques_m52F453DFC994BB2CB946C0ECFCA90755E8CE81BF,
	Oni_SetParticleWinds_m4D8B34B1F691B0D06B66B5AC4A9CE7A47C438D7A,
	Oni_SetParticlePositionDeltas_m44290488FA81D82A7B5E34C6C419183E4AC4C7F3,
	Oni_SetParticleOrientationDeltas_m80BA163C8E315CDE4D535070E044140582F4FB75,
	Oni_SetParticlePositionConstraintCounts_mC6C3DF0767ABFE55B7D56664BD2B85CACC2F2AF3,
	Oni_SetParticleOrientationConstraintCounts_mE8DE30A217492D6C2961351F94471600E84DDF33,
	Oni_SetParticleNormals_mC7DB3812E3D8DA4944D5783D612327C14DAA1407,
	Oni_SetParticleInverseInertiaTensors_m107AC92B3D36DA0EC84A2BC81EAAFDE1A87645F3,
	Oni_SetParticleSmoothingRadii_m23C24AAADB64BB87F5AB6C0F29146E045BD15F32,
	Oni_SetParticleBuoyancy_m1D5C0C42C6E1F7F9F0009907C3C47777493BFFFD,
	Oni_SetParticleRestDensities_mC2C5BF07F3F31A9A9BD30F1E0FD65FA90DAB8668,
	Oni_SetParticleViscosities_m40D16C05777FCCBA236F8812E90FCE61732F8F65,
	Oni_SetParticleSurfaceTension_mEB650D756ACFD8AFCE0B0CA1FA516B8A04166803,
	Oni_SetParticleVorticityConfinement_m490207BCEB92C2090FA6F812903E9DC720C542CD,
	Oni_SetParticleAtmosphericDragPressure_m0DE4F75155DE8255D542463CED62BBA10DE95991,
	Oni_SetParticleDiffusion_m2E7A56B1429ABF2C68BCBEAF9A8B903BCFBBEB97,
	Oni_SetParticleVorticities_mBFAFD13B80D055251ECE3BB09331FEE93EC096CE,
	Oni_SetParticleFluidData_m5475F465341FBD9D3646CF21F640D31DBBDA52BC,
	Oni_SetParticleUserData_m1C95E2860A342CC2408369BE71453D688A4940E2,
	Oni_SetParticleAnisotropies_m4F40683E247F34D06C6FAC46E7BB2D100699D943,
	Oni_SetSimplices_mFA1EA60EF9F96B8AD384993D7AC15B73A2B08D95,
	Oni_GetDeformableTriangleCount_m3D720CBB04BADD1E4AAE5D0F630E7E8A1AB8D398,
	Oni_SetDeformableTriangles_m93D02F1E05CEF46D51296532191387392C8885C6,
	Oni_RemoveDeformableTriangles_mA0A4A28EA77A35743C37CF80A333559E7C8CC8AF,
	Oni_SetConstraintGroupParameters_m7402C64CA0198FD7D07E4F2BBFC81F8C9DB926B3,
	Oni_GetConstraintGroupParameters_m841DD84B6D78A0B694506D815E4756313934B29F,
	Oni_SetRestPositions_m819342E902C330762AEB5BE9D4C68F31DA6CA8A1,
	Oni_SetRestOrientations_m87B3C65B2FA89627A6526A5C22B674280DC3C577,
	Oni_CreateBatch_m289C846274F339F1F3E4A026FE857DAAAC56ABE8,
	Oni_DestroyBatch_m0C065F052A4E0B16A426180BF8EC84E74845007E,
	Oni_AddBatch_m22AFEB35DB5CD69377763213EEED12183545A8DE,
	Oni_RemoveBatch_m62A5EE513FF64B56579A035F1463C0308DD01A48,
	Oni_EnableBatch_mE5AAEE57C0386681A3016600946C205D90CCBD0B,
	Oni_GetBatchConstraintForces_m2F4FB4C6732CFC54C859D7DB5C4A3AC75E4400FE,
	Oni_SetBatchConstraintCount_m31FD93985340AF90A5D98742034D010888144B41,
	Oni_GetBatchConstraintCount_mCDA7F7C7B72B04711AAD3435FFE3AB6FDF657966,
	Oni_SetDistanceConstraints_m6BAF3B1FE95B48E8E681121385077A369140BA88,
	Oni_SetBendingConstraints_m4DD1607554632126E50CA62B31DE223000DEE579,
	Oni_SetSkinConstraints_m980F260DC4107F86B16555CB40B9B63A196F558C,
	Oni_SetAerodynamicConstraints_m97BED4EFAF9B73A677757503EF1AF7EA59CB712C,
	Oni_SetVolumeConstraints_m7914A0DFEEF418BE4C6C81DE6A27EEC21701F511,
	Oni_SetShapeMatchingConstraints_mE2D86779FF689D2FBEB8F39FF1683CEAA9D7547E,
	Oni_CalculateRestShapeMatching_mD268677C88F2E8ABB2C0E8149F0683FFF9AF3BDD,
	Oni_SetStretchShearConstraints_m49F7345551857151AE2572A3F59A90FCA236CDA8,
	Oni_SetBendTwistConstraints_mE4976251409BFB7882CC39AD1F21D4A97B7F4D03,
	Oni_SetTetherConstraints_m10810C34086C7751FB8A5600EFBBC9C70887449E,
	Oni_SetPinConstraints_mC5929BC4A3B8861A32C47072223BD7C88CAC8843,
	Oni_SetStitchConstraints_m8576B5213542C38E501BD55E1C0936DF69506253,
	Oni_SetChainConstraints_m8F08E1B952B69C7A6D3F1957CA4511BE3D95D25D,
	Oni_GetCollisionContacts_mD80B55291B76FB42F97EE2C75EB2E5089540AE63,
	Oni_GetParticleCollisionContacts_mCDAC4DB2F8B1A798CEC15191044C4FE761AF69D1,
	Oni_InterpolateDiffuseParticles_m4A3F61BFA0B073204F17E45D12E58B292B3C5D44,
	Oni_MakePhase_m9FFA8D86C974D669797A0DF8FF1E6A6270554D71,
	Oni_GetGroupFromPhase_m1E9E33EEBF211AD24F224C4DDF044F55DEC4E6CF,
	Oni_GetFlagsFromPhase_m4EC4AEB3B15B30111080A14FB54D7939A4924F0B,
	Oni_BendingConstraintRest_mBF9E6E4CB4E998E291B956B4304FFB31A1ED6058,
	Oni_CompleteAll_m24DA6FE9992164ABDA19A2E0E5BB58280FD02582,
	Oni_Complete_m4909573C5EF85056474F9EEB3A8C91F6F62050B5,
	Oni_CreateEmpty_mEEB6A72C1C211426318B7A3164BD1697F1C0859C,
	Oni_Schedule_mF1BBBF5D226699D18E8EB55A8F31A08BE1DE528B,
	Oni_AddChild_m2AA3FE95D394A0AB211C7FAF46DA6DDA34645B49,
	Oni_GetMaxSystemConcurrency_mA467F4FF67759D65BBACC64B265BBD53DBEE3177,
	Oni_ClearProfiler_m3CE1E415966CFAA8CBF71A57B412B980CE3B5E11,
	Oni_EnableProfiler_m50B45E5B063E18108D99A6851C2C441A8FEDCFD4,
	Oni_BeginSample_m584963073106DEA213EA5679DA42F89AB29B0931,
	Oni_EndSample_mE3D8141E3F15F6DEEB30C16BDA9BC9E82368F8EE,
	Oni_GetProfilingInfoCount_m89525656BF980E088FBF35150F29DC2BCAF974EB,
	Oni_GetProfilingInfo_m5450B203396A0BD0042264E198B358D3FA41BA23,
	SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3,
	ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566,
	ObiCloth_get_sourceBlueprint_m7A60AB633A6899056FE31A30FDF9126DEE4A32CF,
	ObiCloth_get_clothBlueprintBase_mCB3ED81DB059E3BA6A2A05A04671D346B2E80E1E,
	ObiCloth_get_volumeConstraintsEnabled_mF12200D8E63D163F8D3D585D7AC479860DCE7DC6,
	ObiCloth_set_volumeConstraintsEnabled_mC125B982F69E1B9542006432130B902596A76E6C,
	ObiCloth_get_compressionCompliance_m9186FB60A86F801C2423F01E5A464258B045CE7F,
	ObiCloth_set_compressionCompliance_mF125B6A8FD286D33892DF1E7AD4112E4D0A8D066,
	ObiCloth_get_pressure_mFF6475950C18617020944507D0E53E8DEF0D832B,
	ObiCloth_set_pressure_m16C6A0415FA0D7705031E593A5DF2E25DEB4399B,
	ObiCloth_get_tetherConstraintsEnabled_m659B71F2615E9745A37683F80EBC511DBE98A0CF,
	ObiCloth_set_tetherConstraintsEnabled_m578A93337CA1C606418FFD36B1F8D42A63BAF131,
	ObiCloth_get_tetherCompliance_mC27665C4D2558899A84241992C1487432FDE4808,
	ObiCloth_set_tetherCompliance_m79AB074264748F775FACB6EBC910420341528253,
	ObiCloth_get_tetherScale_m0422EEC36F6C734B99A97E8C41E4F8666AE9095E,
	ObiCloth_set_tetherScale_m4C63B9250EDF777173EFCEDB5DA08286D16F892A,
	ObiCloth_get_clothBlueprint_m95CC97BDC2C2AA18A02730FDE298D79D2399438C,
	ObiCloth_set_clothBlueprint_m9C13230540CA0035412B8F415261C483560BBA16,
	ObiCloth_OnValidate_m09DF476881D69E1B90AFAC8B1C95DE21D8C1240E,
	ObiCloth_SetupRuntimeConstraints_m9E9D082292F0E747E5C7C87FD814663C27677278,
	ObiCloth__ctor_m8F253D91A3CB7CCA5D0993BD5C1A94ECA76DA10C,
	NULL,
	ObiClothBase_get_oneSided_mCF71829A68D6C17A6DDABD9FE5FD4953CB096A9E,
	ObiClothBase_set_oneSided_mEAB625E767A38419887FDA8C17F4E7E0132BB335,
	ObiClothBase_get_selfCollisions_mA8188702FAC8AB35A10EA440CAACE6402650981F,
	ObiClothBase_set_selfCollisions_m565AD30B9E80A9C10ADB69715A77992DA236F4E3,
	ObiClothBase_get_distanceConstraintsEnabled_mA71C6AB3A7A15023B0F25DA31D8F068CB7056B0B,
	ObiClothBase_set_distanceConstraintsEnabled_m0AD906019BCEA7BB359643832B5C6FEBC90F41B3,
	ObiClothBase_get_stretchingScale_mEEE3C91265561E074058EA7EF4E4FE91A273454D,
	ObiClothBase_set_stretchingScale_m9FDB17864055E9ABCEC250BEFC87FD5AA5D9C7FE,
	ObiClothBase_get_stretchCompliance_m986E8274AEBE7D18FD43D9EC2FF810B00E7614AE,
	ObiClothBase_set_stretchCompliance_mEF00294CCF1A3D9F708EEDEC951F1B39DD5FF03A,
	ObiClothBase_get_maxCompression_m7EED40DF37E302E1CD3DC5890E3A5EE75241068E,
	ObiClothBase_set_maxCompression_m549EDB7CFA059EDDBAD3F6FDF049BED83C0FEBF4,
	ObiClothBase_get_bendConstraintsEnabled_m488BC08F2F2DA4970B170EBA3853BF4236833F6B,
	ObiClothBase_set_bendConstraintsEnabled_m5B6532793182F92B6B11E3895FF060E8863132A9,
	ObiClothBase_get_bendCompliance_m646783573274ACC26EC85C56EEEFEAC2C987D259,
	ObiClothBase_set_bendCompliance_mF16739C7F7B77E19CADB5806DD2A61DADA849C84,
	ObiClothBase_get_maxBending_mD311AD66F421152BAC8EA4262D644DBCF1300394,
	ObiClothBase_set_maxBending_m20B4EFE0A496F15EC228E3064F8BC865359F521C,
	ObiClothBase_get_plasticYield_mC14422183FE80C52498410EFB928D2955279F54E,
	ObiClothBase_set_plasticYield_m79DD5850D6059FE3D7A793E2CF11C89FD6786B9F,
	ObiClothBase_get_plasticCreep_m8926FF8313698F646B2FC20E1C6E5D1A1E036F63,
	ObiClothBase_set_plasticCreep_mF2A78749BD78479F98B34EB517A7A100A28F43D1,
	ObiClothBase_get_aerodynamicsEnabled_m8AD844A743A70B3C29168739D93422C132494B39,
	ObiClothBase_set_aerodynamicsEnabled_m50A7F807E8441451D866444A85910E863488B64F,
	ObiClothBase_get_drag_m14DE68E55D8E00EF647709EB52C88A8A5A7B11F7,
	ObiClothBase_set_drag_mEF0A2D9B6B8BC041F853B1CF19FFC94CFF448C61,
	ObiClothBase_get_lift_mE6CCF75F4E2D2987AA6E74569AB7282C72F12C16,
	ObiClothBase_set_lift_m29FD4958A66AE91B0CA98791F0B5C21759370D90,
	ObiClothBase_get_usesCustomExternalForces_m40DE22CFE82E2CC2A588254D8E3D8D80AF259BF7,
	ObiClothBase_LoadBlueprint_m1997F75430C7EDA2E6F53C95C0236A5EEC4FCE82,
	ObiClothBase_UnloadBlueprint_mA3989EAD9E5BC68E4714ED61C583554B937E5E2B,
	ObiClothBase_UpdateDeformableTriangles_mD4772368FDE132FEB030FE07538DE949C0E71595,
	ObiClothBase__ctor_mDA1B5958E1609E7D236766AD66D45B40630F0500,
	ObiSkinnedCloth_get_sourceBlueprint_m13328271C367E75F468A5CB821E46B516518FED7,
	ObiSkinnedCloth_get_clothBlueprintBase_mC361EC10AE593A55928FBC327BBA63C42BEA5D9D,
	ObiSkinnedCloth_get_tetherConstraintsEnabled_m7D5D97F93312AD609B63C443A42C57076675D6D4,
	ObiSkinnedCloth_set_tetherConstraintsEnabled_m5323B4430C10747FE22EA963D06B7230604156FA,
	ObiSkinnedCloth_get_tetherCompliance_m7920AFD1C3D359FAE653E7445E5A9ADC3E7AEBD4,
	ObiSkinnedCloth_set_tetherCompliance_m17CE38B39DB2E5E70D5FDBDA5639742A1F9D525D,
	ObiSkinnedCloth_get_tetherScale_m7CE8A2EC5FD8E5BB3E2B2AA313658130C865334B,
	ObiSkinnedCloth_set_tetherScale_m5A8979CF63152BB15C971717E8C1AC2C57982820,
	ObiSkinnedCloth_get_skinConstraintsEnabled_mD7B825F96326630C3CE232B495F7FF5007DA95C5,
	ObiSkinnedCloth_set_skinConstraintsEnabled_m90CDD0C2015E0221D7DF97938A16AB9783ABA7B0,
	ObiSkinnedCloth_get_skinnedClothBlueprint_mEDC031943522A53F8C3F23987722ED415ECE3366,
	ObiSkinnedCloth_set_skinnedClothBlueprint_m2E4A135DF3669904458562CD66A118BD02CDF2C8,
	ObiSkinnedCloth_LoadBlueprint_mDD2DCF50FE65F33D6E620579C8920ADDF28FC7A8,
	ObiSkinnedCloth_OnValidate_m2FB46885B796AC72D9EADB3F09A744BE95065202,
	ObiSkinnedCloth_SetupRuntimeConstraints_m0226D8DA4DDFB1A3FED8F30A9AB98DCB7E5F19E0,
	ObiSkinnedCloth_OnEnable_mE6C374E90EE387B5C68EFAE8104D0CB1B17610E6,
	ObiSkinnedCloth_OnDisable_mBE078E76699C913C05A75F52A521B0320570757B,
	ObiSkinnedCloth_GetSkinRadiiBackstop_m46D90477638A5D51D42926241416EA31A816F86B,
	ObiSkinnedCloth_GetSkinCompliance_mA3B6AF93037C08FCC5386B7482B28921B7449C29,
	ObiSkinnedCloth_CreateBakedMeshIfNeeded_m32D7C98608AC99DC501E5B407628D29F60F40DCB,
	ObiSkinnedCloth_SetTargetSkin_m69DDCC57FC91662996DB98FB1AB2FBBCB9CD6327,
	ObiSkinnedCloth_BeginStep_m809CA572F7AAE52160D52456C9A7366EFBEE95EE,
	ObiSkinnedCloth__ctor_mCF9EB2A6435C578912D11ED86DA9FA4CFA3CC746,
	ObiSkinnedCloth__cctor_m4D88F6E59065C9E8AA4D18FFADDE7998603008F9,
	ObiTearableCloth_get_sourceBlueprint_m55F81480B527CF8C758ABD4D2F7F929F2695B26C,
	ObiTearableCloth_get_clothBlueprintBase_m006E40329FC8205C0365BB51A98DAB915418AB1B,
	ObiTearableCloth_get_clothBlueprint_m8220EDF63D73810E436E64556C9DAC603592A319,
	ObiTearableCloth_set_clothBlueprint_mC2FDC91099716CDCC7874B65540F166FBA1043AE,
	ObiTearableCloth_add_OnClothTorn_m6224CDA7378B8DB12B0333F7FED7B7227E6E3694,
	ObiTearableCloth_remove_OnClothTorn_mB5CE17825525987AFCBD938933DC1248DD4D39E9,
	ObiTearableCloth_LoadBlueprint_m1EF48A55B465C2152873E8016AF3246D09A9B6B0,
	ObiTearableCloth_UnloadBlueprint_m2C5B272467836FBE613772B64769919BAEAE405E,
	ObiTearableCloth_SetupRuntimeConstraints_m352FA8CB0E8A4C38B93748260EF2809F6D999F5B,
	ObiTearableCloth_OnValidate_mC48629E37DE0E229B250AD3D02867D30D1D594C8,
	ObiTearableCloth_Substep_m84B4C10A833B11DF53CE4A21472EC6618EC06ECE,
	ObiTearableCloth_ApplyTearing_m58E1240278F594B2879FA2FAC25111E15BD6B92C,
	ObiTearableCloth_Tear_mBB5C8D532C81E303B4CE3248420B41CE8A3BFFC1,
	ObiTearableCloth_TopologySplitAttempt_m012C38617352C8DC07B3FC6D3458493AAFC5041C,
	ObiTearableCloth_SplitParticle_mC3907A3EC8864E637067EA7D77A7CA30802044E9,
	ObiTearableCloth_WeakenCutPoint_m05580FE624C9F5DC2FE3DB60191E8EA6EE4D0A53,
	ObiTearableCloth_ClassifyFaces_m6A1EB9D5D3BAEE86E0017E48163380A6F1446A87,
	ObiTearableCloth_SplitTopologyAtVertex_m70A2A43A0F3EC4030C822C5AECCB618B37EC3D13,
	ObiTearableCloth_UpdateTornDistanceConstraints_m0650A836BF04788C4A67D0017569FD2D66D7DBD8,
	ObiTearableCloth_UpdateTornBendConstraints_m6366F12EFB3806E932D03202CB07ECF729DF9611,
	ObiTearableCloth_UpdateDeformableTriangles_m719EBB29E6926ED8D1AD437B74C746D34DD6C6BE,
	ObiTearableCloth_OnDrawGizmosSelected_m167AACF10A6B65D3ED9A2F132EFFD93F68BF0011,
	ObiTearableCloth_Update_m336DD8214A6E58666ED7A3B971FA9D5251D24155,
	ObiTearableCloth__ctor_m2F7DADF87737E8DEDBCAE4BD55383CE966AD925B,
	ObiTearableCloth__cctor_m68C6A57547A2A8B14B87BA2954B8E1F8FB0D5EC4,
	ClothTornCallback__ctor_mFE1A20090B1772B8603F4E182EDD9A74B3317BB9,
	ClothTornCallback_Invoke_m8722453F21056D8FB4D156A024E92CC9C4870543,
	ClothTornCallback_BeginInvoke_m909A628C8D63E1E980C7F3A619C86E6D532F5572,
	ClothTornCallback_EndInvoke_m4EE457920329435479957813272A88931CC2914F,
	ObiClothTornEventArgs__ctor_m6807C72DB2AA9763890282409B9C3B199663AFDB,
	U3CU3Ec__cctor_m4D75FDE740AC5CF001A128CC9C5C2C017FB3946B,
	U3CU3Ec__ctor_m18EF89102B8FF84031744A82A5EE4C1007BC7E31,
	U3CU3Ec_U3CApplyTearingU3Eb__24_0_m9CC26D4E8A6074AA2D35CC247E51C1B3FF0D97CB,
	ObiClothBlueprint_get_usesTethers_mEF8E423B48ED084C6C645ECC5A44886C1ADF69AA,
	ObiClothBlueprint_Initialize_mB541B50B4B6C0D58EDE43368C99D89F1690B4AF7,
	ObiClothBlueprint_CreateDistanceConstraints_m1570C2E0E2E30FF955FAED78EB8DF6C12ED36B65,
	ObiClothBlueprint_CreateAerodynamicConstraints_mB62C6FAE788BF62F4C9B9D2D437136E90995A256,
	ObiClothBlueprint_CreateBendingConstraints_m94A0CAEBCCD4E334449EA330C0D77D22333B0B31,
	ObiClothBlueprint_CreateVolumeConstraints_m61301FCFCDA8C8B2F6B27808ED4DBEB2F4EE5540,
	ObiClothBlueprint_ClearTethers_m39BE3E2A63684E6D4A9801AA297EA4259978FA6A,
	ObiClothBlueprint_GenerateIslands_m464C75ADB17A516E2F605ABDC87C34F65D984247,
	ObiClothBlueprint_GenerateTethers_m09AA686B87C84120527DA07B296D7AC75337E698,
	ObiClothBlueprint_GenerateTethersForIsland_m2D9E0618A7595F13C233B6F082A7FB39774E5E4F,
	ObiClothBlueprint__ctor_m7BC51165B1F52A611E3C44A8C72C8C56DB7FC8FD,
	U3CInitializeU3Ed__2__ctor_mD89C5AE9C9C80916FB8FFD79CFC095D19AE5D57D,
	U3CInitializeU3Ed__2_System_IDisposable_Dispose_m7004C46EC7D295E0968AF94ABE4F7E8BF273593F,
	U3CInitializeU3Ed__2_MoveNext_m8709F8E8082C3720C72A9D9297E2E124E10F925E,
	U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E02322F5A0D23311828213F94F9CAE0E0750934,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mFB62B5B536F9DCCFFA18FAD799255FBB349E0CB2,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m74CF369892ADB61833B9A3B086E1C2EC78434A01,
	U3CCreateDistanceConstraintsU3Ed__3__ctor_m09BD3C57322A5EF62A54F6731CDF444FA9BB2E09,
	U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_mDAD5189CF5BE3E2386462303C5D8363F9695B6E9,
	U3CCreateDistanceConstraintsU3Ed__3_MoveNext_m4284389740A92E0CF3454BE0990E2093E68CAFB7,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D1E0E87E7FD71C4EC798103826D6140BFD871F7,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mC8A761BFD80CE17A1F65517143C99E6A20387CDB,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8896FC2746D379EEBF8E8501E3C010284F085BC2,
	U3CCreateAerodynamicConstraintsU3Ed__4__ctor_m4FC51372D57AB411957AE946A83F3EFDC329A358,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_IDisposable_Dispose_m75085E8EEE584CA48781239A3A035272ED0D28E4,
	U3CCreateAerodynamicConstraintsU3Ed__4_MoveNext_m1D5C0373C79C2575C46F7DCDEB6EA324288F2F0A,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADC37B2982CCFCDA50AB1C551B944F7022C3FF0F,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_mB2CC4104E9C7AB68C42D91F0D25AAD0316D92347,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m993A90ABEEA80806AA028F49D6215A00BD8CAB0C,
	U3CCreateBendingConstraintsU3Ed__5__ctor_m216136A55AF138945D5561DC181CE943371035F6,
	U3CCreateBendingConstraintsU3Ed__5_System_IDisposable_Dispose_m6821B7A23670BE0F2DE199C41EE831168CA242AD,
	U3CCreateBendingConstraintsU3Ed__5_MoveNext_m7329FBDB0B5B5BCC7DE22B993DF8A03791187568,
	U3CCreateBendingConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE75E1D32AFBD9B07503268FC3E0BA64851456C6,
	U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m731DAB5B414FCB6F5253FAE15495E32006F1400E,
	U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_m5767B962F68DA952AABEA5D0B178933A58E445E5,
	U3CCreateVolumeConstraintsU3Ed__6__ctor_m88D1946A7A914EC0CC61A5D2BAB66310AB4C807C,
	U3CCreateVolumeConstraintsU3Ed__6_System_IDisposable_Dispose_m051782DF73D0AA384FAEB7DC1642C3598EC916AE,
	U3CCreateVolumeConstraintsU3Ed__6_MoveNext_m7CE8307D10CFABCD59A9D95C245F2EF758EB9FFF,
	U3CCreateVolumeConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m243C532028DFB65774C84E5FDFA0528EC46DE0B1,
	U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_mED3CB28B77CD40AE79F199DE53151239982A3BBD,
	U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_m49682C0DC4ADBD851E1ED3B1631283CBC211B9E4,
	U3CU3Ec__DisplayClass10_0__ctor_mB4D1F41E7404154EE84C80A3FE7A1C124DDA0F98,
	U3CU3Ec__DisplayClass10_0_U3CGenerateTethersForIslandU3Eb__0_m64F724014094E65DF04665BAF91C1C927C3DDB6E,
	U3CU3Ec__cctor_m10D357B87EFBAE919239FF9DEF0C29DC529976AD,
	U3CU3Ec__ctor_mCFA8AD25CE5EF86DA9F38676268211EF9967F0D9,
	U3CU3Ec_U3CGenerateTethersForIslandU3Eb__10_1_m874449CF067336C582EDBBAD64435C2AA7E0E70B,
	ObiClothBlueprintBase_get_Topology_mE37FD318FEB32A3D2B3B5CE3CBFCCE98D6F3C15A,
	ObiClothBlueprintBase_SwapWithFirstInactiveParticle_mC690773C23D9B72FA5D4C4C3838219FEF0E797DD,
	ObiClothBlueprintBase_GenerateDeformableTriangles_m412F7EDE785459A5F256F70B1BCC07A6DE86E97A,
	ObiClothBlueprintBase_CreateSimplices_m792C9C38FCE60CB1F28D70F757E38C50C8B381DD,
	ObiClothBlueprintBase__ctor_m80CB63BEF2CCBA303A83241741195400E4568E38,
	U3CGenerateDeformableTrianglesU3Ed__8__ctor_m0B4BEE607BAA1A2C0EBFEC240D52F91801C0B17C,
	U3CGenerateDeformableTrianglesU3Ed__8_System_IDisposable_Dispose_m9FACA3940826A6F604CE1ED095A420F3B70BEF31,
	U3CGenerateDeformableTrianglesU3Ed__8_MoveNext_mAF7BF73A00CF8BE7EA65AA18680FFDCC98DD3971,
	U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94D6F01A98AAF5D8ACB13EB4E5C66B583F950A17,
	U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_Reset_mFF432431540D581589CE78CB502104BA515F44A5,
	U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_get_Current_mCAC139C7F6132507DA4DD623B3F44AB50ABED2F6,
	U3CCreateSimplicesU3Ed__9__ctor_m6BDB71B25348530DD662CD094363B8BD7290B3F8,
	U3CCreateSimplicesU3Ed__9_System_IDisposable_Dispose_m9D9E3AF192D6A21176BC771CD6B070A775A15627,
	U3CCreateSimplicesU3Ed__9_MoveNext_m8E143004A4C17F2DC72AD5CE7F77847338308DD5,
	U3CCreateSimplicesU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF2A1B6D8EB60FC887ACEA5CA1A98501EFC8DBA8,
	U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_Reset_m168698F93B24A0D1D8BF0D33260667A9AE613315,
	U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_get_Current_mA5534B33C95835ED9EE2DD141CC15F742A981072,
	ObiSkinnedClothBlueprint_get_usesTethers_mBD787F53836BFDEAA10AD88B6EBBB3B8370EBECB,
	ObiSkinnedClothBlueprint_Initialize_mEEEBD3D9C13B8A98A3761D7E712512A3CCC377B5,
	ObiSkinnedClothBlueprint_CreateSkinConstraints_m58C14E2205A69B9015B3F01ADF03CB072338C3A7,
	ObiSkinnedClothBlueprint__ctor_m2E1C425AA22A7BCBE3322AEA18CADF7A4B159F4F,
	U3CInitializeU3Ed__2__ctor_mBE818931D3B01A168B9747C7573E4B406C0EB303,
	U3CInitializeU3Ed__2_System_IDisposable_Dispose_m0BDE2AC7C8E038318E8B83BAC143BD3363720DEC,
	U3CInitializeU3Ed__2_MoveNext_mB04B335715B998CC2A7BB06BD1AFF7D17FAB8FEC,
	U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB92A210007613B2909D214DF7B445C938F4C6EF,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mC9AF6B8BE46FC43D8A1334E15A189A796056CBE6,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m22FAD500DB712F880583DA421A4A69DE13357712,
	U3CCreateSkinConstraintsU3Ed__3__ctor_mD52C4DA6057FEE9E99D0D32779C65C6EF756FA28,
	U3CCreateSkinConstraintsU3Ed__3_System_IDisposable_Dispose_m2188832D0686304E2FA1C1E3CBC9244B11DB4EA2,
	U3CCreateSkinConstraintsU3Ed__3_MoveNext_mD485952F9778BAFF68B8A405A65CA0874C48730C,
	U3CCreateSkinConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE5966D180DA31A5B8CC609EA3CC94FD482431528,
	U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m3423E9E200FEB13FEF7BEC122845FB66403E2A30,
	U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8C97E9A1F24B2A98644A9E03FD1DE0A49AB21A05,
	ObiTearableClothBlueprint_get_PooledParticles_m0184AD47C028F8E14C23E2922244E1640215DDC3,
	ObiTearableClothBlueprint_get_usesTethers_m025E36BFB3E22AEA5B754ECCBDAC2EBC472FD2BF,
	ObiTearableClothBlueprint_Initialize_mEE76304F3987B091FC3F24395B52A9776C85B7B6,
	ObiTearableClothBlueprint_CreateDistanceConstraints_mFD063AE5F8C647760AC90B516949FF31D0238EB5,
	ObiTearableClothBlueprint_CreateInitialDistanceConstraints_mA1AD7123377815A2D7DCADC9CEFE1C7E450B718D,
	ObiTearableClothBlueprint_CreatePooledDistanceConstraints_mC50382B6EF8301BB863CB65BBF73FC20FD8B83BC,
	ObiTearableClothBlueprint__ctor_mAFEF2B7C7182E5D4B8C2E44D862699556AD80D15,
	U3CInitializeU3Ed__8__ctor_mE1FD39996C2F57CF4C0B2C10C7CB001423DE2381,
	U3CInitializeU3Ed__8_System_IDisposable_Dispose_m6A03B2E04499DB25B9E769956B700DB301ECA55A,
	U3CInitializeU3Ed__8_MoveNext_m2028F8C3F5AB82D80D35ABCDAEF7DBF1767E1171,
	U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D1131F06EECDDE2B78AD00FB36522B66AD3538C,
	U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0,
	U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m6ADA7C8F48F23CC6603E2ECDDF2999F8B85CF87F,
	U3CCreateDistanceConstraintsU3Ed__9__ctor_m225975D257A624A724A08C3E1FAF8C2DF4654668,
	U3CCreateDistanceConstraintsU3Ed__9_System_IDisposable_Dispose_mF9F663E01209D2ADCFC88D6EB3697151496279D4,
	U3CCreateDistanceConstraintsU3Ed__9_MoveNext_mB7DADFE9E579B8C1A858C9EEC79439CDD69EABCC,
	U3CCreateDistanceConstraintsU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC86A1722C558C6E0F887CE3CE9B2CDFA9D75BE,
	U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_Reset_mDA4DBAC64C3B419B5FD761D86BEB3A05984873E7,
	U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_get_Current_mA37DF615EFA87C9F148D307AD7FC73526A9207CE,
	U3CCreateInitialDistanceConstraintsU3Ed__10__ctor_mB3B7E5310F4928BB37C9512E9F40ACFC3F140C9F,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_IDisposable_Dispose_m39D16A16F80402BC68F47E0A9F83F7BC9CDFE417,
	U3CCreateInitialDistanceConstraintsU3Ed__10_MoveNext_mD78D4A1D7991FB01E3150A015B30B3C8E19ECF0F,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF3452EB8DE1B9490F900218D00937A623C33312,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_Reset_mC4356FC9C856276AFA630708CF08D9DB17A3E189,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D3BB3B7750F054BA802A619B79F0A538D1D6FE,
	U3CCreatePooledDistanceConstraintsU3Ed__11__ctor_m3C790EFEB6A41148BFA287BBF9EE4C00327BEB64,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_IDisposable_Dispose_m5B3AC37A9F1F1BAED5319C961D4121B7B17D08A0,
	U3CCreatePooledDistanceConstraintsU3Ed__11_MoveNext_m39BB550821EBBBF28571E7BA29467CC2F9204F11,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7BCE5ABEDC2654A1A0453DE030EF2ED8CDC3FC,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_Reset_m824F230E26816CB43250CD6BA6C491C34058CD38,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_get_Current_m684F6B83E2834AA135F20041DDCF51BFE4ABB3A0,
	HalfEdgeMesh_get_ContainsData_mFDC9D71E52A1481FA1D537067E1DC7A061354B36,
	HalfEdgeMesh_get_closed_m2661313240CDAE0D2AFBD3015313CC467F901519,
	HalfEdgeMesh_get_area_m30E2F4CBCA561EA3811FAC94E781D84C0141FD3A,
	HalfEdgeMesh_get_volume_m7E0D5685C7E215D5AB749185F4EFB3B909C1A20B,
	HalfEdgeMesh__ctor_m641C71B020EB960E23164DEABD3019B25C6F5404,
	HalfEdgeMesh__ctor_m58E71C3E236533676CBC924DB3E03E914BBFB8BF,
	HalfEdgeMesh_Generate_mD60A36F035F35EBE9F282926F0C8E06962265CC7,
	HalfEdgeMesh_CalculateRestNormals_mBD6C6FB703AFD8CDC27D5C247EDC36210DC72F25,
	HalfEdgeMesh_CalculateRestOrientations_mFAE7DA9079678E610B968168F3BCFC49794D32C4,
	HalfEdgeMesh_SwapVertices_m7F3C9ED658FE5675EDEA0FE43227BCACB6034413,
	HalfEdgeMesh_GetHalfEdgeStartVertex_m4BB747F2E34E9DAA7686C4EBE135F0ACF5CC96EC,
	HalfEdgeMesh_GetFaceArea_m27C2ADF35DAB7C31E951DE1EED24C9CA6472FB6E,
	HalfEdgeMesh_GetNeighbourVerticesEnumerator_m5D851186C1DD0DDF3C79F394DC9946C6664AE4E2,
	HalfEdgeMesh_GetNeighbourEdgesEnumerator_m46006FB9A047B16085C6EF70CC20209053622818,
	HalfEdgeMesh_GetNeighbourFacesEnumerator_m055B1E4B59B1C20B1C6F542EE88DA13DBE7DF26D,
	HalfEdgeMesh_GetEdgeList_m96FD781747444BC98F4E83CC20DFF87FBEDBB0D2,
	HalfEdgeMesh_IsSplit_m4ADC42896318F8A8193FE1776008C804CE230C3F,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31__ctor_mF8ED85D8453E3677145D2F9202A2F2CBFC6929E9,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_IDisposable_Dispose_mF5E674694F95EB02F8B41CC0A6A0AF7D3C1276DE,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_MoveNext_mAFAABF824BEB8AE1F0EB2DBA7CEFB03ABF6140B1,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_VertexU3E_get_Current_mB8974C8C5F9B458C49BBBD99546E03A15E277212,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_Reset_mF9C32E78CBDC8991CB3AA3E8517D2434CB4060C6,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_get_Current_mE029EC97DE5EBDB2302D6C9C4E042A8D402111CB,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_VertexU3E_GetEnumerator_mAF607573C51602451E445A9210B356A40C744B6E,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerable_GetEnumerator_m3F71BE162D53C1A40174A26D4BEB7864B8D1213B,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32__ctor_mD46F103C7C9B4F9B8A0AD31F7AD7D11C248D9E45,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_IDisposable_Dispose_m2DCA4922446B3F5F983E88E30DE79278DEDE5613,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_MoveNext_m97A4063AAC540677687D09D05E0E4922FA65094D,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_HalfEdgeU3E_get_Current_mCD5275CC511E91FCA17A93FC067DD0201E6C87E7,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_Reset_mE09272AC7F4A5074F95EC162B00DA8152F551EFA,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_get_Current_mD8A977C5E95E52F283E5BB7BCAB03DB08B4363BA,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_HalfEdgeU3E_GetEnumerator_mC4B54D7954EA17442D6D38B91B6E903FB1C54C3B,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerable_GetEnumerator_m2F65C8046D5AC6277F7723BF04D7AEF6A27B507E,
	U3CGetNeighbourFacesEnumeratorU3Ed__33__ctor_m4C8DC2E13D54FFEEB86EF6AE20DEEA7A1E918A6D,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_IDisposable_Dispose_m972ADD603D342F7AD8F1317F6B20179617A042DF,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_MoveNext_mB03F1EEE1294832A224C3F46D64F77F89F662CD0,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_FaceU3E_get_Current_m11DEA6ABA872F8CBB0E39CAA35823EE6B5E5C15A,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_Reset_m9E664CABCDA715691724536236FAEBBB657DD313,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_get_Current_mBBBADC2F55A8568A51101245F7598E952B837304,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_FaceU3E_GetEnumerator_mF45B315F345EC2F18D84B7400ECD0FC83B7BE26F,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m66EB03D0F227B3A19E153D78CC869BA345527948,
	ObiTriangleSkinMap_set_master_m39BC02C26C0865DBE331455792D67D6BD8AD56C7,
	ObiTriangleSkinMap_get_master_m4DD18C8AECFC32446FD30A35C5646DE6E752A323,
	ObiTriangleSkinMap_set_slave_mA52CD5B38D9EC50DA0E98BDDA5F35B4E971B11D7,
	ObiTriangleSkinMap_get_slave_m79A9565E4D228B1C03755268575E17443B6F9E37,
	ObiTriangleSkinMap_OnEnable_m1A2A5D0FA393ACCB977142E487098C16DAEA712D,
	ObiTriangleSkinMap_Clear_mAE6BED8941D24054DD5ACAF5C13431FF25515143,
	ObiTriangleSkinMap_ValidateMasterChannels_mA6CDFE2BEDBC318272453C575BF1BC406929442C,
	ObiTriangleSkinMap_ValidateSlaveChannels_m1971F2714B57EE50C970D39CD5D6BA903E8B79A6,
	ObiTriangleSkinMap_CopyChannel_m1B53803DFBEC32A0B39F6C61774D965DB2B01D47,
	ObiTriangleSkinMap_FillChannel_mA0F0F131BF0D10D9DDC2FB2E7AA2CAAF79576D6F,
	ObiTriangleSkinMap_ClearChannel_m526C2803E32B79B5E868B1BC99F9B48C42711F36,
	ObiTriangleSkinMap_BindToFace_mC6D3795B2BEA297D2DE57744229DBEEA82E3DEF3,
	ObiTriangleSkinMap_GetBarycentricError_mC39FA869436CE0D85F611B3C0DE58228E72A4319,
	ObiTriangleSkinMap_GetFaceMappingError_mEE274B631EC04C834C233252FFA299316F01F697,
	ObiTriangleSkinMap_FindSkinBarycentricCoords_m50355A18A36BEB74CD8B45EB49D85A06B18B5920,
	ObiTriangleSkinMap_Bind_m2DE9C31E8338896A2BFD8CCD2C47D0561FA663F9,
	ObiTriangleSkinMap__ctor_mD9FC75F05FC3C960044BBA2051378DF793185411,
	MasterFace_CacheBarycentricData_m05DB432E4FAE84E0A5D863267B948CCDDE4FB120,
	MasterFace_BarycentricCoords_m9D062C8071B60CDBB21459F906FCB9EF23E5EC46,
	MasterFace__ctor_mB4AB2008F0F78D8A27CBCD816423C06862A6510E,
	SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895,
	SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80,
	SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0,
	SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE,
	SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E,
	BarycentricPoint_get_zero_mE5602C2C071D3E692351C7190D01E6DA24FFEE97,
	BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F,
	SlaveVertex_get_empty_m6B4C7246C11E79FAFFF65C981540626F82F4C5E5,
	SlaveVertex_get_isEmpty_m1B2229CA5E3375CCEDB4898EEBDB72F0EAF2432F,
	SlaveVertex__ctor_mB7C3D7EBC3A235D3D58769095378AE46286BC7A7,
	U3CBindU3Ed__31__ctor_mE5C6B10FCF9C0277DFC6D366EB0FD7BE84FB77BE,
	U3CBindU3Ed__31_System_IDisposable_Dispose_m0470FFE716CE5F6D58F2070F1980E5DEF012C319,
	U3CBindU3Ed__31_MoveNext_m8C37AFBF1649B64A19A3A721B1324FAB72700C9F,
	U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m799C177A7037BA730C8BC7776C400B5065B64A16,
	U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50,
	U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_m937FC6C298B8CA31EA931CA6B7205A747ABFC996,
	ObiClothProxy_set_master_mA5D20E853B2DAD3E6DF07C2A457477EB4CCB08C1,
	ObiClothProxy_get_master_m682D27ED96F155E4640E7FB680820C35850D7F8D,
	ObiClothProxy_OnEnable_m30F59AE3858AB8FE5CDA82FD83EB93952C43FB08,
	ObiClothProxy_OnDisable_m97C875EB28CCDEC05EF380AA8816B4AEDF05EE3B,
	ObiClothProxy_GetSlaveMeshIfNeeded_mC2E1F607B22ACFD8775999EBA93DB56BF994A793,
	ObiClothProxy_UpdateSkinning_m89A644C901FE3F5E6A9DE6D977D18846C11E15A5,
	ObiClothProxy__ctor_m6A724D2010C7C1126914C3998F85787C7F26C73B,
	ObiClothRenderer__ctor_mB8C905CC003D1E9ADCB98277CEEE8885244B3A69,
	ObiClothRendererBase_add_OnRendererUpdated_mD0F78B5164967A7079E170FB1758688B3B7B9A53,
	ObiClothRendererBase_remove_OnRendererUpdated_m31F9D492473F2A469ECDDE8815A8A4ED54AB2FC2,
	ObiClothRendererBase_get_topology_mC1EA7DC745031C7E2659C8A85C450234FEF7A887,
	ObiClothRendererBase_get_renderMatrix_m59D1188D4E7843373753816E380BE810C87C72A4,
	ObiClothRendererBase_OnEnable_m22AA0397E99355C620ADF810AE18F4CE79BAB36A,
	ObiClothRendererBase_OnDisable_m46AFAB09595CD3D1D53C4E62BC90D6B1D592D799,
	ObiClothRendererBase_GetClothMeshData_m88074B0F7E2C1AB37B926B5D5CA19C0B2CC5CD02,
	ObiClothRendererBase_SetClothMeshData_m55D26DF37258885C4FAC39CDA70CC4C9BF834BA1,
	ObiClothRendererBase_OnBlueprintLoaded_mA41298CF8E3348C30A4935C1F78FB405CDB0C6E5,
	ObiClothRendererBase_OnBlueprintUnloaded_m080F6520843923778C773CFE98CEE64D27BA3E6E,
	ObiClothRendererBase_SetupUpdate_mF41CB3C0F730521B3CB97E185B322B53A1A48221,
	ObiClothRendererBase_UpdateActiveVertex_mE26734D59A1227D5C281409282047F59FC80A0F3,
	ObiClothRendererBase_UpdateInactiveVertex_mC796782B675D43C4DFE91E69BB990B105791AD58,
	ObiClothRendererBase_UpdateRenderer_m52FE3EDD4EC4660F1A264944E52A2BB1C559A3FC,
	ObiClothRendererBase__ctor_m97CACE1219B97C09CD47EAD9DBBD8EF0B980458B,
	ObiClothRendererBase__cctor_mD95E153B9F2B07840872EE13E7B3C6B2F44EC0D3,
	ObiClothRendererMeshFilter_OnEnable_m14C0C404EB0B054FC3860DDE0BEA2B21D19B6BDE,
	ObiClothRendererMeshFilter_OnBlueprintLoaded_m7DC0E650090A6BD7DF5CE19BBD09442A13B14894,
	ObiClothRendererMeshFilter_OnBlueprintUnloaded_m324AD1AC22239216957C1FD2A96CD5CDA1063B15,
	ObiClothRendererMeshFilter__ctor_m281C31755A0C1BFD98FFA51A69BC5C072F9BD5ED,
	ObiSkinnedClothRenderer_get_renderMatrix_m362E5F3D96F17C448B1C2397AECA82DE33DBEAD0,
	ObiSkinnedClothRenderer_OnEnable_m30D6ADEFBB33BCB831251C795FB6586AB326294E,
	ObiSkinnedClothRenderer_OnBlueprintLoaded_mC4F8EEA70CBE24C043D9D3D7903CC89510BBB1BE,
	ObiSkinnedClothRenderer_SetupUpdate_mF06DCF83FCB097EE7DDF39D959D51B808913A9EB,
	ObiSkinnedClothRenderer_UpdateInactiveVertex_m709D8133409DDA6637C10CBD201B275655DC8EF4,
	ObiSkinnedClothRenderer_UpdateRenderer_mA3E40089F829AF3B5F2CB9C0C280F8D6014C75A2,
	ObiSkinnedClothRenderer__ctor_m7BF0C3CC512D893A909772BA480CA0CA7FB9436F,
	ObiTearableClothRenderer_get_topology_mD2F198C1CCF5966CD5DA3CC2FE6C48521A7519AE,
	ObiTearableClothRenderer_OnEnable_mA0B0F805992B6983E009DFB1CA0B17B1F25A0668,
	ObiTearableClothRenderer_OnDisable_m361038B61B8468A4EABC1977849C77CA88AEF16B,
	ObiTearableClothRenderer_GetClothMeshData_m5FD8A359120B88E96E2763B9629DBBCBAA7496C0,
	ObiTearableClothRenderer_SetClothMeshData_m85FE724DF5D42CD8DD7521331F365315001285F7,
	ObiTearableClothRenderer_UpdateMesh_mD6A0A8E00E54BEFE5AB2965C25746CBA2128BA9A,
	ObiTearableClothRenderer_GetTornMeshVertices_mE3ECE20DD4227DD7A668F294467CBEFCDA7AC5FC,
	ObiTearableClothRenderer_UpdateTornMeshVertices_mB56215EAAECDB24A3ABAA9EB45E18D455E57410D,
	ObiTearableClothRenderer__ctor_mBB2CB662624757C06735294FB3333BA63E47C6B5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E,
	ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8,
	ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601,
	ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB,
	ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E,
	ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB,
	ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E,
	ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC,
	ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10,
	ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13,
	ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F,
	ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592,
	ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208,
	ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9,
	ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65,
	ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D,
	ObiActor_get_solver_mDE668FD9EA16553E2252B51FC4AA0EE5F2476940,
	ObiActor_get_isLoaded_mE37E960D0EE5EF278241FE730E4E6D56FBBE217E,
	ObiActor_get_collisionMaterial_mFEBA9610A92672D379939CCF56E5A07800EF7AB7,
	ObiActor_set_collisionMaterial_m04C98A6B84C31070613652C4D12C3EFC0D98A0C4,
	ObiActor_get_surfaceCollisions_m280DE5273E14E28B1037C4CEA893F3BFFBEB2CB8,
	ObiActor_set_surfaceCollisions_m40EE79424632CAE1EBA8FB1C3087D54FD9FC5046,
	ObiActor_get_particleCount_m8A511B9B8EE21FE09B45EA484ADE54CE1A469C24,
	ObiActor_get_activeParticleCount_m330540C2CFF296103F759A43A156960A14EF40E3,
	ObiActor_get_usesOrientedParticles_m1F5AC23A705419FFA18109ACE2CD45861FFCADDE,
	ObiActor_get_usesAnisotropicParticles_m54157FE27C8DBE561125BC87EA21BA034F246B69,
	ObiActor_get_usesCustomExternalForces_m63A0FBED9893C8176AA7DB6B5567978FB1D71DCA,
	ObiActor_get_actorLocalToSolverMatrix_mCF3080C74BC381BF19F21BB6204A19DBDDFB82DE,
	ObiActor_get_actorSolverToLocalMatrix_m467C1F4C003B591FBE2C93BA9BDB33C0371962CE,
	NULL,
	ObiActor_get_sharedBlueprint_mE4DB059D49A26A1EF8D5AFD8CECEDB63CDACD863,
	ObiActor_get_blueprint_m47A228582F43BE1A2AF98A4CFBF052C0DCF852F5,
	ObiActor_Awake_m241F3014CB9150A6234C8BBFE5F7F1506CC4A4A8,
	ObiActor_OnDestroy_m2F3D4A01290E0E5EB2F3753AB9913AC96249A655,
	ObiActor_OnEnable_m971216E2924B472ED6E1EF5CA46C20711919C967,
	ObiActor_OnDisable_mCD8A0D978C2AD361EA37D62139DD083606463FD7,
	ObiActor_OnValidate_m52B606D1428A6597252E13B0A7664A6A2A9AC196,
	ObiActor_OnTransformParentChanged_mE56EB6C5787A9B68CC2DCBF59A69FC5DBAC340B7,
	ObiActor_AddToSolver_m4612A91281D3B2EEA44B2654DF515E757FA4D3FF,
	ObiActor_RemoveFromSolver_m6E84D3DEBFE722DD4EC517EB2622FC80350D4FEA,
	ObiActor_SetSolver_m6163A3076A049EAC233DCD2BB28B05C282DA55B6,
	ObiActor_OnBlueprintRegenerate_mC7B6851CA42C438CCA449E6F6B675C88F4515C71,
	ObiActor_UpdateCollisionMaterials_m5E96C2AE2B9F7811085F85D0D25A5621C46D6817,
	ObiActor_CopyParticle_m8AA520BF621655A1D7FD60BE63769B097E1996D6,
	ObiActor_TeleportParticle_mD6150ED98C32F82A0ADB734DDE58A37BA0B78E39,
	ObiActor_Teleport_mE63A190E1F4170CF5E1BE19522E3B6061AE96A04,
	ObiActor_SwapWithFirstInactiveParticle_mC6593AA19F1C4AF00040F46D4EC723008B97B52B,
	ObiActor_ActivateParticle_m45FF9EA8B2788D3C9EAE0EDE77DA4FA2133BF9BA,
	ObiActor_DeactivateParticle_m04DDF95A73A8AE09950848D4A8DBB2A57EC42C59,
	ObiActor_IsParticleActive_mFD14C2BCE7E86131404B2EEDA413555B7288C0E3,
	ObiActor_SetSelfCollisions_m45F0B0F5D1A4306D161C7EEC3C1435D9D52E3686,
	ObiActor_SetOneSided_m99DA6DD46C17B2357C1815E76EA6FB70B6525D33,
	ObiActor_SetSimplicesDirty_m1FA63CB84FD2F2219BB54CB69CC5E8473FE2167A,
	ObiActor_SetConstraintsDirty_mF1D5DEE2405AE6F94AE264FC9CD0A63C31189D0F,
	ObiActor_GetConstraintsByType_mA485A63E65B2E5A863562BA515B15F6DE11138E7,
	ObiActor_UpdateParticleProperties_m9B2C616C0275DB62285F9E0CB0F1FF615D9FFAFF,
	ObiActor_GetParticleRuntimeIndex_m4AECF54626524359EE8CA930022DDFA864946B0D,
	ObiActor_GetParticlePosition_m02493A97C489BA4E448F1B3BE3AAB59DB1E9039A,
	ObiActor_GetParticleOrientation_mC35CD07B08A81AC2159E02DF583118B7ACF8E9C3,
	ObiActor_GetParticleAnisotropy_mC8BAD8C2ACCEB1DF3284AEF13CC5E12ABEFF727A,
	ObiActor_GetParticleMaxRadius_m834AF87A1D3204613AC0A6658EDBAF54871C13BD,
	ObiActor_GetParticleColor_m08A638A989F579EFADEA293B32C74635EECD25EA,
	ObiActor_SetFilterCategory_mAE60934D51EBD397225139EE05F2E80F08118D7D,
	ObiActor_SetFilterMask_m7FED73FD99F34632090C9174E5EB278DC5D76F08,
	ObiActor_SetMass_m8D79A26F4FBEF1A895C6F482808A23A02B8BFAC4,
	ObiActor_GetMass_m2E9973F1EE3CBB300948F36489B7A7637B768F98,
	ObiActor_AddForce_mE87CF6C0A21C03ED45209758E7BFE0E1AB2B1A00,
	ObiActor_AddTorque_m43A494B40EA87E0EC7A6274A0931F9CF27B5D77D,
	ObiActor_LoadBlueprintParticles_m11D7669681BB9F35863E2BE6B9B50F9C7F8CFE88,
	ObiActor_UnloadBlueprintParticles_mAFE44CA41C0A763A6142420F2145AAEDCFD296A3,
	ObiActor_ResetParticles_m081DAAF528B4EF4F9E79AA17ECBEFC2C0BCB03D2,
	ObiActor_SaveStateToBlueprint_m86BB211764276B407A42BF4C603D75AEBD260F84,
	ObiActor_StoreState_mDF522A37AED6A366CD98C42E6D12EB524E5A6B67,
	ObiActor_ClearState_m2708E40E0CC1259F0A4A23A134D7C08A34D64D9E,
	ObiActor_LoadBlueprint_mD8204B2EFE860EADB85ABAAEA380D0EB7DE52966,
	ObiActor_UnloadBlueprint_m0380FCD5E41E56876B933D34F94EE112FE49D14E,
	ObiActor_PrepareFrame_mB2B1C5E2FB1A0570C0B7527B5654C17ADE677A53,
	ObiActor_PrepareStep_mB06CED03B998A63A4AC43A5318D99D1AE8ABA400,
	ObiActor_BeginStep_m2CB8338B9754937B0F151D9F06AF97263B2D313F,
	ObiActor_Substep_m784F85C0BD4464D6E4B1BCE24E44047956C78486,
	ObiActor_EndStep_m5076BADB8970EC68040EA131738946988F140513,
	ObiActor_Interpolate_m589CB41AB063846C5C621944F1B14EACB90C3683,
	ObiActor_OnSolverVisibilityChanged_mA9B03F298B37CD3BB66DD5527F9E3CDB43A728EB,
	ObiActor__ctor_mE992775E0773F687C886A5C462E2EAB7FA5D740E,
	ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF,
	ObiActorSolverArgs__ctor_m645D2D61FF1A0780D4B70C2494C8C90607C7FA1B,
	ActorCallback__ctor_mA6CF95A70DAB4FFF21823741E58A598FD0A3CC61,
	ActorCallback_Invoke_m44385DCB93F7AC483E18B7477BDEB1C44993D508,
	ActorCallback_BeginInvoke_m511CEA8353354A1869B3548D98C3EA166C757E30,
	ActorCallback_EndInvoke_m44778EF05F7C946EE7ACD0315B4F14F101980024,
	ActorStepCallback__ctor_m48119AF13D841B3D5476543A1C79108F94D53E00,
	ActorStepCallback_Invoke_m25120AB25E0465B7AF5976D51946EED8935E2A38,
	ActorStepCallback_BeginInvoke_mE4197C4E139BFCC95434E2C3E20BE882EB5F2989,
	ActorStepCallback_EndInvoke_m8AA8AAD3C0E68BB1AF5F7E6A61EA2269F3A57AFB,
	ActorBlueprintCallback__ctor_mADC829707A4F631EADEE895A25D2AAEA59A4D64D,
	ActorBlueprintCallback_Invoke_m7BFF8526B4E3B31969016C044E63EA9AF3FFE7AD,
	ActorBlueprintCallback_BeginInvoke_m32D286D241BEC1F0C4FD9CF6C04C5E7BF1823485,
	ActorBlueprintCallback_EndInvoke_m7B651D23DA455A93DCC87B95CCE21A5789433013,
	BurstBackend_CreateSolver_m365F45F03BBF4E610276C6F35CF0E9571A34ECA9,
	BurstBackend_DestroySolver_mE387C5978ADCE197E5A514ABFCFC8B208DDA8918,
	BurstBackend__ctor_mEF1F6C9604213C51BE39D80F166780E5F6308E51,
	BurstIntegration_IntegrateLinear_m921E2CDA30530CC03E67061FCB3E58B4CD580F19,
	BurstIntegration_DifferentiateLinear_m18FFA3260D236DE1A749C1982054E4358A92F668,
	BurstIntegration_AngularVelocityToSpinQuaternion_mECA9B9F387A94F998143099B85244F25A7C71C3A,
	BurstIntegration_IntegrateAngular_m1AC7507CA63312B4AED6B0D3D41026A58E286DA9,
	BurstIntegration_DifferentiateAngular_m8E1D447491B00E55C4AA5337E06EDD9E04FA391F,
	BurstJobHandle_SetHandle_m57F25376A67FD363BF9E2FA481C34F33BE928479,
	BurstJobHandle_Complete_m2443FB0658D5667B94E3E26494792DFA42E7C9C1,
	BurstJobHandle_Release_m70B6B8DEE092DA24E3526CE9F558FF44B064D231,
	BurstJobHandle__ctor_m66E9504D512A2946F639E96C611289F618F1F820,
	BurstMath_multrnsp_m6DCBD243896AD27CA2286015B3D35ED6B678FE12,
	BurstMath_multrnsp4_mFDFD027BEF04401275FFFBFDA8BEEBADA78C2755,
	BurstMath_project_m1A54A528F3FC9B0309892E868CF525734419BB49,
	BurstMath_TransformInertiaTensor_m54DB5D9D60F182D81A1A94D4AE87C537BB5B716B,
	BurstMath_RotationalInvMass_m77B1237CBA82520C99CFCEF39BB74697B2E2CE6A,
	BurstMath_GetParticleVelocityAtPoint_m533DAD00C56A4ADEA7DC7F2155446CF97BFFCE9E,
	BurstMath_GetParticleVelocityAtPoint_mEE6D03ADB42973E2DADB2CF67BB6AEEAE643F584,
	BurstMath_GetRigidbodyVelocityAtPoint_m6D7F526FF3A3868816B466207E631CBEF3A723BC,
	BurstMath_GetRigidbodyVelocityAtPoint_m0F22193AA325748DBB42F5633911298C651B0938,
	BurstMath_ApplyImpulse_m203334B30F3ACFBAAD67BF9A6D76FB1216D06DB5,
	BurstMath_ApplyDeltaQuaternion_m5697C6202036D32860B3C613DE6A089EC28C850A,
	BurstMath_OneSidedNormal_m8551E1209D5159A5CB34E47F9D47F3FCE7186A68,
	BurstMath_EllipsoidRadius_mB707721A79E5ED040C1DFF300DF0028F72CDF65D,
	BurstMath_ExtractRotation_m94C5F740BF10C6707B30C7C228E5AF9905A2BBEF,
	BurstMath_SwingTwist_mA729CD3BDD82BF4422C6DED532B204F590DF55DF,
	BurstMath_toMatrix_mAA0372FB0B34939F42DA7D8C213E694BC0C7DF4D,
	BurstMath_asDiagonal_mED83A376E4FB3DD5D01304739CF1209A86E846AE,
	BurstMath_diagonal_m2495565B886EDB95594F3A6D3D3D04413E407984,
	BurstMath_frobeniusNorm_mD8B2D136717389B775B17E51B995F2EA8818F106,
	BurstMath_EigenSolve_m80C6B3EEE5D40D6D991C2D75C06994D64A47870E,
	BurstMath_unitOrthogonal_m0CF287F50224232FE30E0E6C09BAB1F314F35F53,
	BurstMath_EigenVector_mFD07DD0849851A35E3352CFE18907AC042F21951,
	BurstMath_EigenValues_mF9B2E812F51CC5AD5A9D58B57A7F89F68C67C13F,
	BurstMath_NearestPointOnTri_m15F131F6314B94E56B30D27DC3FFFBE574379AF6,
	BurstMath_NearestPointOnEdge_mFBAE1492DA93CE465DDCCD5235C17B338EB00A6F,
	BurstMath_NearestPointsTwoEdges_m1B56CCBBE464C3541D9D3F96F377E0F8E0654F25,
	BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73,
	BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A,
	BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9,
	BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740,
	BurstMath_BaryIntrpl_m38A47800E85DBDC519BB0BEE48E77F26C7ADACC8,
	BurstMath_BaryIntrpl_mAAA9279D994329F44077A24887823FE8F110C10E,
	BurstMath_BaryScale_m97EA0DB01CB35CD7B8E25408543E3DBD2E60FAF5,
	BurstMath_BarycenterForSimplexOfSize_m26CB914EE114D2DE19C4F75C97009A695F6C076A,
	NULL,
	BurstMath__cctor_m99432CC5A4D9AB0AD2645ACA2D65A07749F55B8C,
	CachedTri_Cache_mAE403285E691BBA51817154E6CF25A99BF9D24F1,
	BurstBox_Evaluate_m9A7975B75E4F6E739D90F73F2CB2209B369FDBFA,
	BurstBox_Contacts_m9D3DED1FA7E7454E9EB7A52A7ED94B8A08C99CE3,
	BurstBox_Obi_IBurstCollider_Contacts_m97856F2BFA4EBA5278F111B41C7F5CA5A8C58573,
	BurstCapsule_Evaluate_mB2A54FDA05AAE32DF69B20A013BDCF441F379D46,
	BurstCapsule_Contacts_mE461E9B2E609E1A411ED668A5C6380B29FCCE4D6,
	BurstCapsule_Obi_IBurstCollider_Contacts_mADF596860548ABE29238ADEA0F82324B6D1A6A28,
	BurstColliderWorld_get_referenceCount_m5410ABFAEEF5B2F079D12F85BA4BC00EBBA25A18,
	BurstColliderWorld_Awake_mF0444BFAAC7B1AEB3C8D6AF3C16FCA8079C41D7F,
	BurstColliderWorld_OnDestroy_m37CCCE53E7CC76AEA2F9C46BE1E23DEC6E1EE507,
	BurstColliderWorld_IncreaseReferenceCount_mE35A203B38FF99F444DC3DDC2E5260EB324E6D66,
	BurstColliderWorld_DecreaseReferenceCount_m641A956FDB4496B991213DE60E9843A212993A25,
	BurstColliderWorld_SetColliders_mE6D28C17E1BD553ACAF043209701869C53E0B16B,
	BurstColliderWorld_SetRigidbodies_m6179D6A032B77BAF16651CF801C99D069927A4ED,
	BurstColliderWorld_SetCollisionMaterials_m56B61F9B00642E85C44EDFE7E45A8FD0566B6D48,
	BurstColliderWorld_SetTriangleMeshData_m802E995C73CDDDF449C5FFA86CA2E9058430E847,
	BurstColliderWorld_SetEdgeMeshData_m9A88483ACF3E325F8F71E7AFB8B5F3E60BBF2BA7,
	BurstColliderWorld_SetDistanceFieldData_m78614076EB71DEC2C03A944DD308F95BB1179F8F,
	BurstColliderWorld_SetHeightFieldData_m507B16C528E0397A6A794CE2C82A8B3AAEF2AC13,
	BurstColliderWorld_UpdateWorld_mFCE3E12510C2C0D767280588509E55A17E360AC1,
	BurstColliderWorld_GenerateContacts_mB8B09872216AF5D0CA158E500AB1B291ACA23BD7,
	BurstColliderWorld__ctor_mB4D7132EC2AB0DC806D426C3D38035DB919504FA,
	IdentifyMovingColliders_Execute_m6536871CD5E23F8233E90C455F8B29C3839BA490,
	UpdateMovingColliders_Execute_m05B0B145094896C8C5D0A421ECFAB255D1C7D4D6,
	GenerateContactsJob_Execute_m1A30B956B328522C42AAB9316CFE714A83873DE5,
	GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E,
	BurstDFNode_SampleWithGradient_m383A3E8C19EBD56E7660F1D957022D53CC0D5173,
	BurstDFNode_GetNormalizedPos_mD01E0D79BAC83035CC86DC3706B9B7AA98B3CF9D,
	BurstDFNode_GetOctant_m76BF4D7E298B1E7238848063468AFE583C403408,
	BurstDistanceField_Evaluate_mC93945AD7F3206A1EC39A627C982644CF24673B9,
	BurstDistanceField_Contacts_mC98735809C55B8C503AC05F45BEC80420352057E,
	BurstDistanceField_DFTraverse_m21F88015BB4FB3D50DD96EC893D5A10CD6827850,
	BurstDistanceField_Obi_IBurstCollider_Contacts_m212E14857D066BE00574BE85A69F636A0B5A14BF,
	BurstEdgeMesh_Evaluate_mB1DE344E5F95F0D35C81FCEAAFD439CCA1A0281B,
	BurstEdgeMesh_Contacts_mDEAD5DA052578CD2794227155F0CFDEFB76DD9BF,
	BurstEdgeMesh_BIHTraverse_m8E806DD0BD9B370CC1862099121BC5B3F83B9CA9,
	BurstEdgeMesh_Obi_IBurstCollider_Contacts_mE8D91763243332F3BBE8D636493C232F3085C1C1,
	BurstHeightField_Evaluate_m37E0774FD60539E9B60C6265C5289EABA40FEE16,
	BurstHeightField_Contacts_m8D519E8FEE86E9D62A2F24AFE65643D75C82E321,
	BurstHeightField_Obi_IBurstCollider_Contacts_mAF9CC53C9637FA7BEFAACAB317DEECC4325EFEA8,
	BurstLocalOptimization_GetInterpolatedSimplexData_m711A70635DA122946C2ED3E19F344B5C34B307D9,
	NULL,
	NULL,
	NULL,
	NULL,
	BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA,
	BurstSimplex_Evaluate_m96C3BF51F11E624BC054D9BA276F177A9DBBBE73,
	BurstSphere_Evaluate_mD03D68FDA6CCD8B6481B8CF75FB5C1528BA034B8,
	BurstSphere_Contacts_mDA37B8F9954ECD2EFD7BF8F9A51A43CA73A869AE,
	BurstSphere_Obi_IBurstCollider_Contacts_m16229652DCB867F59A4F4360F064FDA00691D32B,
	BurstTriangleMesh_Evaluate_mB03B52D7D8BA26D3C383CB9334A57EDD03B03DE5,
	BurstTriangleMesh_Contacts_m03A033B4760F0729757BF20D322FC8D112802A00,
	BurstTriangleMesh_BIHTraverse_m53B6E0B2D8D09B465000034E751D2BB96E194907,
	BurstTriangleMesh_Obi_IBurstCollider_Contacts_m0414B63AB8F72DB613D9240E6B5AA847FB4B296D,
	NULL,
	BurstAerodynamicConstraints__ctor_m171DB4C41AB61E57D6CFBCBF8F1EFBEECA50060E,
	BurstAerodynamicConstraints_CreateConstraintsBatch_m5953E0EE649D7A6BCDFB1001111E82776F5AD4FC,
	BurstAerodynamicConstraints_RemoveBatch_mF45FB4F617887EA56C19CDD0A5599C3E5AF81309,
	BurstAerodynamicConstraintsBatch__ctor_mAB73CE6EA7FA450AB8D8F9746F2DAF4D94301CE6,
	BurstAerodynamicConstraintsBatch_SetAerodynamicConstraints_mD81E86E3899A743FBFF0D573642F9A31C36AD2DE,
	BurstAerodynamicConstraintsBatch_Initialize_m52DE3A5FFF09C7C19AFF49C578CD68B3BBA5A21A,
	BurstAerodynamicConstraintsBatch_Evaluate_m8313BC819CC1002CD147C384E8B1FD6186E1B362,
	BurstAerodynamicConstraintsBatch_Apply_m9B000CDF164C57958AB1FE3D032F672500B32F4A,
	AerodynamicConstraintsBatchJob_Execute_mA12D60F5737F1B49A118E40DEBB6D5BE5D9FDCE1,
	BurstBendConstraints__ctor_mBB3175AC8830813B19E7C7406E322F622AC44B95,
	BurstBendConstraints_CreateConstraintsBatch_m9482B32B4FFC637B8FACDB69E2E5187E1D7A2AD7,
	BurstBendConstraints_RemoveBatch_m524A898FC77AD6343137E8318D873E45CF4DDC95,
	BurstBendConstraintsBatch__ctor_m4A37A5130C84D585906D468AB0E8629E00088E10,
	BurstBendConstraintsBatch_SetBendConstraints_m0D61DFB4075472908BF4B7AD7F7A5B91E9DDF4DB,
	BurstBendConstraintsBatch_Evaluate_m8EA7F55220FE67E51678D8D753326CAD0A468980,
	BurstBendConstraintsBatch_Apply_m04AB64084DD5979B3C016A69AA5E324D8FA6A2E8,
	BendConstraintsBatchJob_Execute_mF6E997FD25554FC9F4071D28B24E8804DEF39870,
	ApplyBendConstraintsBatchJob_Execute_m99F086827B78551FE09B2FBC6881B2894BCB7682,
	BurstBendTwistConstraints__ctor_mFF24BA182B06546A107E4746E9A16E003676E284,
	BurstBendTwistConstraints_CreateConstraintsBatch_m0F238B5239F33DBBFF455F99E7ECA3C68042AB6B,
	BurstBendTwistConstraints_RemoveBatch_mF7931B74B2FA5847F36D90151AA4771ACCF8F88C,
	BurstBendTwistConstraintsBatch__ctor_mAD28426736368584FBBF5F0B3F1A050017051A6D,
	BurstBendTwistConstraintsBatch_SetBendTwistConstraints_m83ECC4A3EB27AF0B0DD7286BBBBE9D3F5B9F595A,
	BurstBendTwistConstraintsBatch_Evaluate_m5A18EA6B83F56965C44CC599B6F11196EA9FC14F,
	BurstBendTwistConstraintsBatch_Apply_mB66593567CAA16CD1E737D5F960CB3E6DAEC3AF5,
	BendTwistConstraintsBatchJob_Execute_m72C33183ADAB152169BB181429D90663B0DB648A,
	ApplyBendTwistConstraintsBatchJob_Execute_m74A218C4F257CB77BB42C70892690A866BC97861,
	BurstConstraintsBatchImpl_get_constraintType_m2590C7D4F84F01AE788FF492011B71322F061925,
	BurstConstraintsBatchImpl_set_enabled_mD1431710ABDE3B26BCC8B23B0D5C435C272B96D5,
	BurstConstraintsBatchImpl_get_enabled_m8D8BD7F684A9A2F366378F484FFB173C5A9A2388,
	BurstConstraintsBatchImpl_get_constraints_m216AF95B2EC1563DA5C512071259B3D3EB52602B,
	BurstConstraintsBatchImpl_get_solverAbstraction_m71CCA0338078FDB7879A9C7EE6EDBE36213746C2,
	BurstConstraintsBatchImpl_get_solverImplementation_mC0CFD60D058F313752ECD6AFFBDA5B019C7B92BA,
	BurstConstraintsBatchImpl_Initialize_m3902E95C54BF2D85419EE6D4FF20324FD0A4113F,
	NULL,
	NULL,
	BurstConstraintsBatchImpl_Destroy_mA32FB47CE96F9AF44D3491F65C2ABC3282BFBC3C,
	BurstConstraintsBatchImpl_SetConstraintCount_m56168F91898DD35463CE2F34E28420EDA26B08AC,
	BurstConstraintsBatchImpl_GetConstraintCount_mECC340A005EB3FACC0D768D75B2AF91F3C537BBD,
	BurstConstraintsBatchImpl_ApplyPositionDelta_mB768C6B64D66437DF5CD923CE2954F39C13EAF80,
	BurstConstraintsBatchImpl_ApplyOrientationDelta_m5D183E7D73228BC30AF7F29AEA97093451DB3229,
	BurstConstraintsBatchImpl__ctor_m4CC32FC741A8F1B78E1AD38A2CE9A173390D06F7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BurstChainConstraints__ctor_m6BD031F1AF74A05ED788A97BD2E29DCC5405F252,
	BurstChainConstraints_CreateConstraintsBatch_m6FA686222F58ADF6A8FB9DA868EF1ECB274EE794,
	BurstChainConstraints_RemoveBatch_m08C05C7C473E97B0EBD28FB927A087C22B30942C,
	BurstChainConstraintsBatch__ctor_m3ED7DC4EFF029DCA737750DEDD59C96B8C5BEE93,
	BurstChainConstraintsBatch_SetChainConstraints_m7700742D24FE5E03135B25C8D0B077BD478BD329,
	BurstChainConstraintsBatch_Evaluate_m4DF6B73D65C12C2A211E71F0B35B6218057EBBAD,
	BurstChainConstraintsBatch_Apply_m159D14CB6EB95D9D87E6E02D0EFA91451FEE15FF,
	ChainConstraintsBatchJob_Execute_m576FF3805857AEBF06581865F975D5D982550907,
	ApplyChainConstraintsBatchJob_Execute_mA038CCA6A34C6A73AECD256274108E245F5FE91A,
	ApplyCollisionConstraintsBatchJob_Execute_m875BE8298CCA64DB1EC6FA478647C40913DD330D,
	BurstColliderCollisionConstraints__ctor_m4A7EE0A944FE1C78D90F82014E5023ECAE2E6F31,
	BurstColliderCollisionConstraints_CreateConstraintsBatch_m885FF083ECF3133071C7360CC0EE6C27ED2465A3,
	BurstColliderCollisionConstraints_RemoveBatch_m316998BCB1F7E4271675FD5DBBBD5BA7C9B4D6DE,
	BurstColliderCollisionConstraints_GetConstraintCount_m5BC945E5958AA1BB663354D0BCA87839CC7B8CAA,
	BurstColliderCollisionConstraintsBatch__ctor_m72900719130F8A4CEB14F342487A809771B2E85F,
	BurstColliderCollisionConstraintsBatch_Initialize_m7066076D2BE9129C48624E1F828AB929F5725813,
	BurstColliderCollisionConstraintsBatch_Evaluate_mA980331AF0B4F032C3F5C6753D2DCCAB369C4580,
	BurstColliderCollisionConstraintsBatch_Apply_mB6FFEB95B0DBDE98C8E3C632A9A9A1A57A6B85C5,
	UpdateContactsJob_Execute_mBDE78DEE6DE6D94DC3B6684199C0292D4C915E0D,
	CollisionConstraintsBatchJob_Execute_m7E251F652F57C72F5A552957040597FA6DCD6688,
	CollisionConstraintsBatchJob_CombineCollisionMaterials_mF1338B4C889E6A783168F52825A68777DBAE0C99,
	BurstColliderFrictionConstraints__ctor_m1577CBB923541040E3A27C93F0805CB4A88DE8AF,
	BurstColliderFrictionConstraints_CreateConstraintsBatch_m1473DB1005C33E2C3DE67DF92361EB8A74DEF544,
	BurstColliderFrictionConstraints_RemoveBatch_mAB3660C122E2A7BF08E0BAF080A3FAF5850391F1,
	BurstColliderFrictionConstraints_GetConstraintCount_m5194E2B5E8B1CBC581875DA5D490BDE5B15170CE,
	BurstColliderFrictionConstraintsBatch__ctor_mE6913E769AC6B04E509535FA82C20B3F2FC774E0,
	BurstColliderFrictionConstraintsBatch_Initialize_m7DD945D2223E2EECCCA54488772BF58A10E3BB0D,
	BurstColliderFrictionConstraintsBatch_Evaluate_mD324C6CB95633E67336AFE9EDAD78658DA1BFB84,
	BurstColliderFrictionConstraintsBatch_Apply_m11BFC339A0FEC51B4D71D12CEE63F057511F38CF,
	FrictionConstraintsBatchJob_Execute_mA6E69C218FC0969B77330BD6B4D5C15A6F49E056,
	FrictionConstraintsBatchJob_CombineCollisionMaterials_mE4F955E2ED7F5DD7834726D2014A7555573FDB3C,
	BurstDensityConstraints__ctor_mD9F106B23B9C178D8A545615DA87111141AAB4D9,
	BurstDensityConstraints_CreateConstraintsBatch_m52015A08886A596B1A39E3C5F2BA1989A89CF3BF,
	BurstDensityConstraints_Dispose_mF70DE3A0F0AC9B3A0674E49030A9E4C437D398AC,
	BurstDensityConstraints_RemoveBatch_mA9C71BF83BC9090BFBDEDCDF4A7FBB00CF79EB1F,
	BurstDensityConstraints_EvaluateSequential_m95B6E2581F0A959D02937D9B1C54BB9B4759B617,
	BurstDensityConstraints_EvaluateParallel_mE21B9F82D9A68EC930698C9E620CFEBEA2405C8D,
	BurstDensityConstraints_ApplyVelocityCorrections_m6C4774683C02BC70687BB40104626F422032DAB2,
	BurstDensityConstraints_CalculateAnisotropyLaplacianSmoothing_m75B282B819AFEE8C68D7C882C5BDE8398CBA2C90,
	BurstDensityConstraints_UpdateInteractions_mE27800B95C636B6903FC35B9398934F80505744A,
	BurstDensityConstraints_CalculateLambdas_m9EAAC7EE53D7E59D292F18AC46DEE931D474F291,
	BurstDensityConstraints_ApplyVorticityAndAtmosphere_mB08E5CC7602CE03DC15C33792AC0320047269AB3,
	BurstDensityConstraints_IdentityAnisotropy_mF4B1F55394D53099A14A4F0919ED5ECBF7E631AA,
	BurstDensityConstraints_AverageSmoothPositions_m3CCD94810EA90442C6D242384973857DDBA53163,
	BurstDensityConstraints_AverageAnisotropy_m13C3ECFDF3234BC1A14104920A85AD970209CE2B,
	ClearFluidDataJob_Execute_mD48A32AF77242D2E6CDFF920317494A459561F18,
	UpdateInteractionsJob_Execute_m1EEE83F71148CE066BB48348ECCA642F2A1E80CE,
	CalculateLambdasJob_Execute_m2B425DF0EEAD9932568604F718B2921296DA4A79,
	ApplyVorticityConfinementAndAtmosphere_Execute_mB2A90B74BE8FF621E0A2E25863B37740AF5D95D9,
	IdentityAnisotropyJob_Execute_m8BCDAC71C9652E2C66490C406047751573B95B40,
	AverageSmoothPositionsJob_Execute_m8530DC393B3E014A32A67CF6BE5657D78714B493,
	AverageAnisotropyJob_Execute_m8E8F860CE0895F6EEE07E14E2C09507F15CAF28C,
	BurstDensityConstraintsBatch__ctor_mB6C3F53C5168D7331932D08C931EC7B602712A0A,
	BurstDensityConstraintsBatch_Initialize_m7884A93CD87D7006DA805934BC141B511F535AD9,
	BurstDensityConstraintsBatch_Evaluate_m951FDA2E0C0B4109E7675F5E0F61C84CDA2E151A,
	BurstDensityConstraintsBatch_Apply_mAB46573B2FB97CB81902232085C3703950F4E8D1,
	BurstDensityConstraintsBatch_CalculateViscosityAndNormals_mB6B1E6A8977BE8DC76932C65C3E06124A7140EBB,
	BurstDensityConstraintsBatch_CalculateVorticity_m5BAEBAE4AC97FD2EC47D95A216003E30D72D1FE9,
	BurstDensityConstraintsBatch_AccumulateSmoothPositions_mAEB7E673A4E53C02447AAAA70199A9D2487A9020,
	BurstDensityConstraintsBatch_AccumulateAnisotropy_m7C86504F22194F961A7113A8DDF60366DA670727,
	UpdateDensitiesJob_Execute_m59352095C34B63F24D23B524548542B60EF8B6F7,
	ApplyDensityConstraintsJob_Execute_m9CE8FB7550A4AFC0C47CD7569ABB38E69B978F16,
	NormalsViscosityAndVorticityJob_Execute_mBABF5C9E0E64304EEA30D40729CEAB8C02E78C86,
	CalculateVorticityEta_Execute_mBD1C201BA56247D5A29070646ECD1C54D8FA3B31,
	AccumulateSmoothPositionsJob_Execute_m198C016973ED826593021549CB78790D8D97590C,
	AccumulateAnisotropyJob_Execute_mDC2644AC6278164D69AF0BF1AB94E439BA2B1396,
	Poly6Kernel__ctor_m458F1673FE9C7B44144243984BF8D3351EF76641,
	Poly6Kernel_W_mA9FD1FFD1D90D74DB6803EC97F570DAB31EAE849,
	SpikyKernel__ctor_mA630C18D4CA7AEE79027ADBEB1BFCBF26A7452BE,
	SpikyKernel_W_mA7FD6FEFAF094558177A73FB38603EFC70E8D697,
	BurstDistanceConstraints__ctor_m069D55C3879EBD80F0DA9EBE04BF2E4665C92F4C,
	BurstDistanceConstraints_CreateConstraintsBatch_mAC76A2B7CAE8D6D745BC72BCFCF66DFFB59E2918,
	BurstDistanceConstraints_RemoveBatch_m16908B57FCA878AFB863730B2456FBB4BD1CA3DE,
	BurstDistanceConstraintsBatch__ctor_mF0B06C747DC7B62A981276C4451DCB5C1E9C7AAE,
	BurstDistanceConstraintsBatch_SetDistanceConstraints_m89D1EFCA556E894E49CAEBC9506145FA5F1062DE,
	BurstDistanceConstraintsBatch_Evaluate_m5F116504938B3902C4729AF320BBCF699E91685B,
	BurstDistanceConstraintsBatch_Apply_mB228CA05BF444CFAC93F865BFE0818079108F43A,
	DistanceConstraintsBatchJob_Execute_m8974C56ADBED13F088ADEC56AF17CBF723258A16,
	ApplyDistanceConstraintsBatchJob_Execute_m3D79DE4E8EDE7541DC1B61399471933671AFAFDF,
	ApplyBatchedCollisionConstraintsBatchJob_Execute_m6B349C47BB948DA01570848A8C29F1D3BEF4C3D5,
	BurstParticleCollisionConstraints__ctor_m0E59AC63D9FD308E957F8C5A346C4B0FC225F1EE,
	BurstParticleCollisionConstraints_CreateConstraintsBatch_m7B3464699C43C7C53DADF07C2AE43F8F65EA5D8D,
	BurstParticleCollisionConstraints_RemoveBatch_m64170A2A7F0002FF237F21EF5B42DD1F8CB12833,
	BurstParticleCollisionConstraints_GetConstraintCount_mDD050203A0C51B1F67186B24DF38F5764EF90467,
	BurstParticleCollisionConstraintsBatch__ctor_mB4FE8DE0B3E4C9E89E6CB5A1DAD86AD86E1F8D15,
	BurstParticleCollisionConstraintsBatch__ctor_m62534C1226C33861E70E8151B60BC1FE4A3CA1E6,
	BurstParticleCollisionConstraintsBatch_Initialize_mA8DA2295D258754046BA9D17FB19FEDFC945986F,
	BurstParticleCollisionConstraintsBatch_Evaluate_m5156FEC0808BA2A778CC11EDE1E15A05BB71EEE2,
	BurstParticleCollisionConstraintsBatch_Apply_m51688D061B17E5E33A86D643F16BB3F17A186C92,
	UpdateParticleContactsJob_Execute_mA60AE07BFCE3A0F563D19A316C153C7F9887ED7E,
	ParticleCollisionConstraintsBatchJob_Execute_m8485003051D23F0D75146E26663B7CA0AF2386F1,
	ParticleCollisionConstraintsBatchJob_CombineCollisionMaterials_mEC293E92648E63BD95F591444FDFF4B18F33D703,
	BurstParticleFrictionConstraints__ctor_mF28E936F7126378219D48275D4453F81FB9ED434,
	BurstParticleFrictionConstraints_CreateConstraintsBatch_m8346A4E3AEFEAAA93A6F7EE4395C8E578CD5A44D,
	BurstParticleFrictionConstraints_RemoveBatch_m79E022580926338212A7B5F8369F4308F675A1A8,
	BurstParticleFrictionConstraints_GetConstraintCount_mFD9B9FFCBB8FC509CABDC56CDAC151DDD2894AC4,
	BurstParticleFrictionConstraintsBatch__ctor_m5DBFED861162777EA753172BE5C49734B6AEF2A4,
	BurstParticleFrictionConstraintsBatch__ctor_mA8F4839E9244CFC1FC091AB7FE7E4C8CF9A13A4A,
	BurstParticleFrictionConstraintsBatch_Initialize_m3AAE395541CD850FD9709DA038F1E595B91182B8,
	BurstParticleFrictionConstraintsBatch_Evaluate_m3A757A216EB2A595E353BEE0424AD014B13AD2B1,
	BurstParticleFrictionConstraintsBatch_Apply_mB336EF2223B6F7525003DA96ED14AA3BFC2DD30B,
	ParticleFrictionConstraintsBatchJob_Execute_m79FE4542796B3C2B9146A83500647A9D0680830C,
	ParticleFrictionConstraintsBatchJob_CombineCollisionMaterials_mF0CD7C3874B720CC924931A83AB1EE44FBF0797D,
	BurstPinConstraints__ctor_m16AF01D598A8930881C5164CD7F8BEFF85046751,
	BurstPinConstraints_CreateConstraintsBatch_mCF848FC65CC60655B498F6B6C85FC7FBF79D265A,
	BurstPinConstraints_RemoveBatch_mC49FA4F97486F185171751D0A036BE8B737201CC,
	BurstPinConstraintsBatch__ctor_m1A637B9849DA051CE9C6E3AB44F0B5609217B115,
	BurstPinConstraintsBatch_SetPinConstraints_mC82D58B41EAB2D7DB4D6E479003BCD81D5EE898F,
	BurstPinConstraintsBatch_Evaluate_m4EEDF2C7D717FF5D55781B855290A83D12CE3BE5,
	BurstPinConstraintsBatch_Apply_m6B9F30FD5304D83F40C52E4AD11F2BF5DEB35E74,
	PinConstraintsBatchJob_Execute_mEF5E6798264EF1F83CC82A6A98B54DECCD4D1E36,
	ApplyPinConstraintsBatchJob_Execute_mB647701B0C09CE1D750AF92DA09F9F8E78FD52DB,
	BurstShapeMatchingConstraints__ctor_m0B050ABA0169470B1A7FDC3AE039E9486251D29A,
	BurstShapeMatchingConstraints_CreateConstraintsBatch_m419791AEEE7452103FF7878019023EA0F6739BD5,
	BurstShapeMatchingConstraints_RemoveBatch_m5BBE41B473B96570BA54EE7F63A9E3C7ECF640BD,
	BurstShapeMatchingConstraintsBatch__ctor_mD3A6AA7CDA7637D9B8D9AA4276876A9FC2126522,
	BurstShapeMatchingConstraintsBatch_SetShapeMatchingConstraints_mD7B426D930EA25257837F4352448D97907AD8CC0,
	BurstShapeMatchingConstraintsBatch_Destroy_mA07771DB7398466BCD0C10936A03823C43253C16,
	BurstShapeMatchingConstraintsBatch_Initialize_m12CBDA7C9CCE459EA9149D3DDA2F3C63BF63F394,
	BurstShapeMatchingConstraintsBatch_Evaluate_m2054022121F711252557EF1F696843DD1DF63590,
	BurstShapeMatchingConstraintsBatch_Apply_m579EEF8C0773D60CAD1333EAE243B9276473EA13,
	BurstShapeMatchingConstraintsBatch_CalculateRestShapeMatching_m134076AF07B375506EBC37128FE4D1CEA936AEF4,
	BurstShapeMatchingConstraintsBatch_RecalculateRestData_mFE475921ECBA487E5BB3C157D059C0718FF34084,
	ShapeMatchingCalculateRestJob_Execute_mF557395226A5479543E3F443C1787EE2F5D4AB24,
	ShapeMatchingConstraintsBatchJob_Execute_mE253B06332A40DCA4C6453A876DB428FA345B047,
	ApplyShapeMatchingConstraintsBatchJob_Execute_m1ED0E064C42CCD032FDE804F098C1250B8DDE687,
	BurstSkinConstraints__ctor_m7203B844BA1CA2592BE81D0D05B692CC2AC7E191,
	BurstSkinConstraints_CreateConstraintsBatch_m1FC6B0D73F7FC806E76F0545926DE742008E8FBA,
	BurstSkinConstraints_RemoveBatch_m34D9EA06E8213228121EEEA8CC782C87C7372F51,
	BurstSkinConstraintsBatch__ctor_mC2146DD3CDAFA928092F86F972B6EA5FC642123C,
	BurstSkinConstraintsBatch_SetSkinConstraints_m45A1CE4D67EE3CC055D11F20D330BCD340F0AF19,
	BurstSkinConstraintsBatch_Evaluate_mBD987ABEA52B5113BB442929413ECB04674E99A2,
	BurstSkinConstraintsBatch_Apply_m267C40110D1FB9E8EDEB5099443F59D59F5B893C,
	SkinConstraintsBatchJob_Execute_mE9464013B468FA413A33380F7080A6E8984B03A5,
	ApplySkinConstraintsBatchJob_Execute_m65A2A62E22D2996164E8F0E34B60EAB4C923CDCD,
	BurstStitchConstraints__ctor_mB747CF967BA6E4EE50397F14D46B8B7DA14A804B,
	BurstStitchConstraints_CreateConstraintsBatch_m8C527A61912F3EAACCBED5D7984AB355ED672600,
	BurstStitchConstraints_RemoveBatch_m618BBA385C7B77C3D7C98F23B672452465100330,
	BurstStitchConstraintsBatch__ctor_m503248B040EE8C6F74F7A5880AF7D5063BE0AD74,
	BurstStitchConstraintsBatch_SetStitchConstraints_m9324CC010E744DF4A39DB9F37F2FCE97B678F793,
	BurstStitchConstraintsBatch_Evaluate_mF3C332AFB74C39800785FCB3EF9AD0037B611860,
	BurstStitchConstraintsBatch_Apply_m6F3AE07C4231D6F0D0A5716D84F84744381009B6,
	StitchConstraintsBatchJob_Execute_mA5D5BEA6FB1DA22EEAB3C0CD7CEB52145DBE1BCD,
	ApplyStitchConstraintsBatchJob_Execute_m50E228A24C8D90CE705CA819CBCB9F3D1FF7E7B6,
	BurstStretchShearConstraints__ctor_m2C91556F549F1C111EC012472476B9156DE41A12,
	BurstStretchShearConstraints_CreateConstraintsBatch_m8A10C037510E47B5F17C1C454263A95B893FCDC1,
	BurstStretchShearConstraints_RemoveBatch_m09C8EFF7AF589EED5ECB673986EFC4A53DC98D0A,
	BurstStretchShearConstraintsBatch__ctor_m0B7DCD678CEB70494E6B9639BB1DC964B410C0F1,
	BurstStretchShearConstraintsBatch_SetStretchShearConstraints_mBCE29CFCF8BC3A52C5168F861D0D928975DB54CA,
	BurstStretchShearConstraintsBatch_Evaluate_m7ABCD9857963B8964AD4313EBA15893DB12DB078,
	BurstStretchShearConstraintsBatch_Apply_mBB933D7AD6511C7F8BA82292C137240A5F76F2A3,
	StretchShearConstraintsBatchJob_Execute_m38FE03FB561A839F2E3C895AC52308E3374C50CC,
	ApplyStretchShearConstraintsBatchJob_Execute_mB1D83D142361E62A2D687C29EE346B26AD985A70,
	BurstTetherConstraints__ctor_m34FD8D32D12C33A6401F8DB8932DFD387471F22E,
	BurstTetherConstraints_CreateConstraintsBatch_m0116EEF8DC8CE5CA2562DC4E6A091755A9E9D138,
	BurstTetherConstraints_RemoveBatch_m650ED58729D5166338B77B9CE30850ABCD3E9155,
	BurstTetherConstraintsBatch__ctor_m70E75DB508A77183C1CD484EBF9E63A8676D07E0,
	BurstTetherConstraintsBatch_SetTetherConstraints_m988FCF9B205603EE496CA5181F037F902331AAC5,
	BurstTetherConstraintsBatch_Evaluate_m06654C031806619BF5FF50F07D02B7BA549630A0,
	BurstTetherConstraintsBatch_Apply_m99D86EAFFB29B296AA8F9E7553E4A584C3F4D76C,
	TetherConstraintsBatchJob_Execute_mEF1FE91E81FCED26D0A12BF953AFB35780ED52C8,
	ApplyTetherConstraintsBatchJob_Execute_mBF97BD7B8E8E46587578F03ADCD029486BD104CD,
	BurstVolumeConstraints__ctor_m572903003FCEC3CE2A23EFCE28FA3BED68810DB6,
	BurstVolumeConstraints_CreateConstraintsBatch_m6C62BE9A7681C03DA2A70D091F1374679B77578B,
	BurstVolumeConstraints_RemoveBatch_m2F1D3097AD2249B40FF0C0BBA64F2FA026E4ECFD,
	BurstVolumeConstraintsBatch__ctor_m2A07696AD34BBDF0104A55B12AD247C0AF0BC8C2,
	BurstVolumeConstraintsBatch_SetVolumeConstraints_m683F87D414D09123A02BD6C122EF1D40B6DBB43A,
	BurstVolumeConstraintsBatch_Evaluate_mB2CF2EFCFED1E8A493A0FCE4FDEE99472BA9CC0D,
	BurstVolumeConstraintsBatch_Apply_m539BB6E964A7D666939558EDCA2442AA17B780E0,
	VolumeConstraintsBatchJob_Execute_mB6BC30FDA2A00B126A2939FE5841FABE48971AB6,
	ApplyVolumeConstraintsBatchJob_Execute_m8A7F092F18089C81D544CD0E08BCA2EE41AB7C25,
	BurstAabb_get_size_m5E23B4420CB21B196C931459BECA271D6567E28C,
	BurstAabb_get_center_mEF7385D666BE5EE77B04D4C1B507CC277498DF83,
	BurstAabb__ctor_m5EA38EB6DA406EE85C1CDCC3B4053EFDA9AF999C,
	BurstAabb__ctor_m25051D2B2D8D634E1497B4FBA7DA5855CA630293,
	BurstAabb__ctor_mBFC57B41EDB0B6AD54F1CEED8ABDFCE2CF73FD6D,
	BurstAabb__ctor_m20D651928C007C6CDCF1FA28E061578888324215,
	BurstAabb_AverageAxisLength_mC3D10DA8BEC0F0BA5B418EDB620BDCD4A2951C8E,
	BurstAabb_MaxAxisLength_m00B824BC8D68124AD6362187459834CD10F2C9AD,
	BurstAabb_EncapsulateParticle_m2BF518A13C65F7BC9B5EB0175BA00D3A07FA3350,
	BurstAabb_EncapsulateParticle_m9154ADC7C45964966B05F06F5B92412630067FA7,
	BurstAabb_EncapsulateBounds_m9AFA483BA1B7BEBF77C1B85D57549FB36375E1D7,
	BurstAabb_Expand_mD6FD7B154B8700A8F46316D8ACFFD928F92DAD49,
	BurstAabb_Sweep_mCB51F0C277EA7EF3C7F033CF125C1A0B04E7541E,
	BurstAabb_Transform_m972887B0B52D4B08B4806F6BB8196E17B64C0B90,
	BurstAabb_Transform_m20348C56E245E750A6399FA6F7C2D767EE11B91C,
	BurstAabb_Transformed_m09422ADB914E6917056FB53FEDDC172E7AEE88C1,
	BurstAabb_Transformed_mE44D84A994DBAAD9EED74F026F8220ADCD9A249E,
	BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6,
	BurstAabb_IntersectsRay_m94E7181E1827E8BE7F727FBCFB48EB11CC40CFAD,
	BurstAffineTransform__ctor_m05A35A300C65D6551377AD2C948ACA1FC2F8667C,
	BurstAffineTransform_op_Multiply_mB96F7410C6F8B9421CC886A7C77CF07B41CABEA6,
	BurstAffineTransform_Inverse_mDD81DD6911040D1CA2FE9F0D1F315F7812DF27B5,
	BurstAffineTransform_Interpolate_mECFFF4666C1393B072387A61B32573B56BCFE676,
	BurstAffineTransform_TransformPoint_m391756FAA0719FE03FA3ABFD002CF41953DFE610,
	BurstAffineTransform_InverseTransformPoint_m62FF98D4C1F4B903143CBEBB8BE560DD8973E955,
	BurstAffineTransform_TransformPointUnscaled_m88D0781E4CA7ACAB74CC46049AF03EEE34AF17D8,
	BurstAffineTransform_InverseTransformPointUnscaled_mB57EB7ACC6E6630D7AB86C3B3C4B330CDA781675,
	BurstAffineTransform_TransformDirection_m6194FE90F0734E4FAC3D9EC2F18B1418E054B284,
	BurstAffineTransform_InverseTransformDirection_mD556AD3447707CC7AF2C21B42B1009B169B07E13,
	BurstAffineTransform_TransformVector_m93FE5B596EE6E92173AEF3E44B265907B0371AF0,
	BurstAffineTransform_InverseTransformVector_m5F477834E781A85ADC934DE24BC0FEB89A6BDDF9,
	BurstCellSpan__ctor_m06DA83A6F5C75A3D3B7B8E2F97C86D245EB3DD9D,
	BurstCellSpan__ctor_m285AB617964F7D42BF536E07ED69BEA63327EE37,
	BurstCellSpan_get_level_mEA8299C260E4E7D5A362075EAFDBAEC20CDCF21B,
	BurstCellSpan_Equals_mE730FAAEE94E797BCE3F326B494C301956A7014D,
	BurstCellSpan_Equals_mACAF86A3EDB8CCB7033E573669BBE26088CCF696,
	BurstCellSpan_GetHashCode_mACB4CF48DD8B4D70211F28DACDEE8DC6A7F421EA,
	BurstCellSpan_op_Equality_mEB4B1016C43F6753ABE279B48DF7349FC3AE6E02,
	BurstCellSpan_op_Inequality_m571B820F6562829C7EC1A00B80BE1E158013612A,
	BurstCollisionMaterial_CombineWith_mC472F029D139FD8BC35E71EB37CA9CEF821B2AFD,
	BurstInertialFrame__ctor_mA98B43A3120665CC585634E855B81B740024C066,
	BurstInertialFrame__ctor_m23E7F3F42FD0210FF6C53F7D04954946BC11A943,
	BurstInertialFrame_Update_mFDDBF9FACE375ABB5982E879EDC404B54CFF6E34,
	BatchLUT__ctor_m93F109809773B154AF095575B0C00C35E224346C,
	BatchLUT_Dispose_mCFABB1B7385B31C2C7DD8AD9123205A3732D13BE,
	BatchData__ctor_mD6B3E7F16DF9B5364DDCC3E92B5CDF971E4A69BD,
	BatchData_GetConstraintRange_m685408EAF447F1B6A4AA9C17803E061A58752449,
	ConstraintBatcher__ctor_mEEC00AF4C13C2B7D3E750958E9F78A639DCFBC76,
	ConstraintBatcher_Dispose_m832285B8BE78DFA00F4E2253150C447F9E546889,
	NULL,
	WorkItem_Add_m8A0EF0EB1E6104C1CD4E12708761C75BCE6281DD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ContactProvider_GetConstraintCount_mACC69AA75EE8103A1AAED3B3924CB26DF9798FEF,
	ContactProvider_GetParticleCount_m3C3C405609067B4AF80D1D4269CDB486BA02D9FC,
	ContactProvider_GetParticle_m0B5FB5F90C47141EF186E6ECE7169AD3F0B5E54E,
	ContactProvider_WriteSortedConstraint_m5568D339D5163D4BAF3BF92562D993B463670F5E,
	FluidInteractionProvider_GetConstraintCount_m010E43F859A7E41424DDDD5B8C03B75C3B05F3FA,
	FluidInteractionProvider_GetParticleCount_m7E6514985CBFF6300D92399124419655353DB050,
	FluidInteractionProvider_GetParticle_mBB30B78F2DEA50E3E3BDEABAE3B2CB39DBA52DCF,
	FluidInteractionProvider_WriteSortedConstraint_m2CAE0F1AC443F672E9503FC238EEC9A73C84DFE3,
	NULL,
	NULL,
	NULL,
	NULL,
	FluidInteraction_GetParticleCount_m986AE991B21B5250E354ADCE40955BB274A954C0,
	FluidInteraction_GetParticle_mE71B2BF68C60B06FFB723114CCB4874EFBAD83E9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParticleGrid__ctor_mA92DC689B3AF2090AB5ADE796C79C50B5CB6A90E,
	ParticleGrid_Update_m4A130AAE52E137C3271369A2D3E5151AEC2831E9,
	ParticleGrid_GenerateContacts_m68B980DE9BB7EE1557C792974E48F09E790F3BF5,
	ParticleGrid_InterpolateDiffuseProperties_m22B1DE675771F78C98B6972C3963A814405BA539,
	ParticleGrid_SpatialQuery_m8342F98C377CD26D3670BFFA545A612B5CF35F38,
	ParticleGrid_GetCells_m5BB4AF028FA1E1E1F94AA6782F6074BE41A9CE2E,
	ParticleGrid_Dispose_mB2A4C6486565763B3AD1D3A8863302F2D6D88645,
	CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955,
	UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47,
	GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2,
	GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB,
	GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194,
	GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418,
	GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937,
	GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7,
	InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A,
	BurstContact_GetParticleCount_m07601B081D4B2D33AA274A224230DDF7FA41DA12,
	BurstContact_GetParticle_mA6D14AB9D6B0C1C0E2D489D9AEAC4F1DACF44976,
	BurstContact_ToString_m082A762EDA6C99C0D046A08E1456DC09B503654B,
	BurstContact_CompareTo_m4AAE9C314CA58F9ECD6DAC6E1FE8C4046C00A550,
	BurstContact_get_TotalNormalInvMass_m8DB8BB9F92C654E952F6EFD3E30695C049FADAE4,
	BurstContact_get_TotalTangentInvMass_m23A1474CCF4F8B7EFD804072C2B8C1081570C36A,
	BurstContact_get_TotalBitangentInvMass_m6584B11DFF4F6F649C7F2048CA928C7321528FEB,
	BurstContact_CalculateBasis_m7756F0C85D337D0FCC1301EF91A9DDA3AC1F11FF,
	BurstContact_CalculateContactMassesA_m6442A92150D7BACA30A83170C952918D5941B9B7,
	BurstContact_CalculateContactMassesB_m525CB01618D4402EFE433CDD47AC61735B04A24D,
	BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524,
	BurstContact_SolveAdhesion_m1CFCDDFF32CBB02EA4050AA3184E9AB1BF33B39B,
	BurstContact_SolvePenetration_mA388AC5B79F15B50ED2678A37DFA62CF8D7D12D3,
	BurstContact_SolveFriction_m50255C39E593D5D9ABAAE76C19E618577F5E24F4,
	BurstContact_SolveRollingFriction_mE67B076235FF4D42A16E7624EDF3B4ED4E32463A,
	BurstBoxQuery_Evaluate_mB454C8F8076A2A41E291FAE3D4FD31B2DC810F62,
	BurstBoxQuery_Query_m78C0813809BC44E8027D68204D187FD1DA7242DB,
	BurstRay_Evaluate_mE001829B68C458A87581958A3B9C6D7DBC85EC68,
	BurstRay_Query_m0B4F7997C30EF099D2F72584B7EC3DB15EFBBEA9,
	BurstSphereQuery_Evaluate_mA4AC455ADF4F22279A7667EFC7731B78EF1ECB9C,
	BurstSphereQuery_Query_mC1E1903D249FD6440ACFB80FB3D2EBDB75F81875,
	SpatialQueryJob_Execute_mF6926D6C57FF21B92A42A0CF712D5DE2808238CF,
	SpatialQueryJob_CalculateShapeAABB_mD4BB4AAFB5B7C57689D4C530F27046BAEEBBF9D2,
	SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8,
	CalculateQueryDistances_Execute_m8DAEA0919E33220185485804208ED37F68B12355,
	ApplyInertialForcesJob_Execute_m6D16C9CD6820B5E2E5B69F722A4CEB47D3109B74,
	ParticleToBoundsJob_Execute_mF06E5D2A133701BF1984DAD08A9CAA0A53FF1F0C,
	BoundsReductionJob_Execute_m560D9C4E0E5F1D2C870425DF2282FB1278ADB1C6,
	BuildSimplexAabbs_Execute_mEBBE064B8F9A20CC5507809D8B1A4D30C3A4EA7B,
	BurstSolverImpl_get_abstraction_m71B8AF402C0DA85EED28CC6FF0137DAC7FD3BC12,
	BurstSolverImpl_get_particleCount_m7AE56747FF242C220B343A722C00A755CF171453,
	BurstSolverImpl_get_activeParticleCount_m72D861358815D53ED2376649B20BA6D0039CC857,
	BurstSolverImpl_get_inertialFrame_mD1E672E98D01E2AAE88930F5993E6493A0D26B1A,
	BurstSolverImpl_get_solverToWorld_mDA781F49C5AAADD01FB3E43DF3AFDF923943D4C4,
	BurstSolverImpl_get_worldToSolver_m9C7E959103476BEE3E8508768EA6219B1CA116EB,
	BurstSolverImpl__ctor_mBE8B17A47282B2B675D48FD685A329F8EBBC15F3,
	BurstSolverImpl_Destroy_mD83C292B1BB19506F7AA063174E7A5BFCFAC34C8,
	BurstSolverImpl_ReleaseJobHandles_m83684E4DD509A99D072B49C5696B1F71D25DA659,
	BurstSolverImpl_ScheduleBatchedJobsIfNeeded_m572239F65137E8364015720187D55DD3A7112F05,
	BurstSolverImpl_GetOrCreateColliderWorld_m18279617CF056BA1CBAC23488A9D5A2256F6C792,
	BurstSolverImpl_InitializeFrame_mAD36C218F6792712B52B215262A12F6B588DDC7F,
	BurstSolverImpl_UpdateFrame_m6357FD16BAD403BF8F93C863FC9E695D19F52B86,
	BurstSolverImpl_ApplyFrame_m0B2202471F03C50E65C88590302A3096D9264DC0,
	BurstSolverImpl_GetDeformableTriangleCount_m6100E41F13329539ED29A671B488ED9B9F621F97,
	BurstSolverImpl_SetDeformableTriangles_mF51EF61CF41D64C93E2FA42BDB4F7EF8E31737CA,
	BurstSolverImpl_RemoveDeformableTriangles_m2AD1A93F0CAB50206B6EAE64367918E161BD1AE6,
	BurstSolverImpl_SetSimplices_mF7242E46022196A5B3C894483C6047C91E5B42C2,
	BurstSolverImpl_SetActiveParticles_mEBA625A10B170D8D1E9BF455C1E60E4111FC1E0E,
	BurstSolverImpl_ClampArrayAccess_m5AC5E1904640B1FECE5466DC705AE69905A2A692,
	BurstSolverImpl_RecalculateInertiaTensors_m3072558AE7AE81271459440FAA651C5EF7224DE0,
	BurstSolverImpl_GetBounds_m6B2DAA13B1AE81002985C2B41A586057AAF221BA,
	BurstSolverImpl_ResetForces_m3DA78D31EAF1FF66296946CB9DE2397DDB649B29,
	BurstSolverImpl_GetConstraintCount_mF317E95D1AE7B8AD23F8C109C47FC8AAFD72B37D,
	BurstSolverImpl_GetCollisionContacts_m819998819D1BEFE35B28D69A8A50AF2E2AE4C852,
	BurstSolverImpl_GetParticleCollisionContacts_mBC98CFEE63E49BBB7D1616101E8273A5F0149632,
	BurstSolverImpl_SetParameters_mB64780E8C06460A178D23ECDC0EA518FCFF104E9,
	BurstSolverImpl_SetConstraintGroupParameters_m7CF6BDF3BB22607D2FF38C33116979BCACC1AC59,
	BurstSolverImpl_ParticleCountChanged_m6A87552C2D94DD0D11E529012D3AC9C28742B746,
	BurstSolverImpl_SetRigidbodyArrays_mC3F8D877C7555BD269C6F09B2A80419C273EEE8B,
	BurstSolverImpl_CreateConstraintsBatch_m92521EF7923BAD7D60C29676B3D86A2AB6092482,
	BurstSolverImpl_DestroyConstraintsBatch_m046232143A4ABA1047A88EC2EC11B7970F30168B,
	BurstSolverImpl_CollisionDetection_m845F7FDE95A99E26B4078E764F5D3C794D6FD624,
	BurstSolverImpl_FindFluidParticles_m0FA802C04A57D9118A9488DBB2D040A1D0DB78F5,
	BurstSolverImpl_UpdateSimplexBounds_m412233B3DB56438B9771BC882C2946FAA9AF3EC5,
	BurstSolverImpl_GenerateContacts_m62D3E6316F7D5C4B7C6244BE0CEEA6EC65DAA35E,
	BurstSolverImpl_Substep_mD745D7E31C329A826DA018630CE5C69EE3C3F9C9,
	BurstSolverImpl_ApplyVelocityCorrections_mF209AD82E3FB34F43C25C66A14AE533787D2D31D,
	BurstSolverImpl_ApplyConstraints_m4CC5581B725747E6960B7B0C87A7A7E0B08A2B60,
	BurstSolverImpl_ApplyInterpolation_mDD491C3B1397FAF54EED5EAAF90A45E9E75234B0,
	BurstSolverImpl_InterpolateDiffuseProperties_m6BA4E4470E4D368EBBDE37428865798935659789,
	BurstSolverImpl_SpatialQuery_mF06E870F8951DF84B8B6B48711ECA386F6E1C66B,
	BurstSolverImpl_GetParticleGridSize_m66C2C2B27C2EC8B3ECFB0A6C8BD6BD968F244132,
	BurstSolverImpl_GetParticleGrid_m6AFAB13B76899AF4A38587AF036F3ADE3CCB2610,
	NULL,
	FindFluidParticlesJob_Execute_m477B14F5ADFE05D34CC12D3351EC114D4C03408E,
	InterpolationJob_Execute_mF037213986C8166D161BEBF6CA5836F2F42FFAF6,
	PredictPositionsJob_Execute_m42A393C96C2581FB6A24356ECC0E1807B43E5B8E,
	UpdateInertiaTensorsJob_Execute_mB727A2BFE75FE34ACF7D46675FA0D766EF6C4D7B,
	UpdateNormalsJob_Execute_m96B1F05E2523C94475A069C1D707F97A21B1EA81,
	UpdatePositionsJob_Execute_m8DF43769C21749D1195FD8A483D9C8440A26614D,
	UpdatePrincipalAxisJob_Execute_mE9809E4137D315000D2A3E570BCC7573B050ABDC,
	UpdateVelocitiesJob_Execute_mBDA6FF10947E19ACC17D971540182892E043B621,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NullBackend_CreateSolver_m149A74012C4CF9DEC9EC68F1C16EE99F3D6E88AF,
	NullBackend_DestroySolver_m5E51947F4BEC3D57534FD796732535ED2B83D3F6,
	NullBackend__ctor_m7C4DE499807A5119E6914F1A7B6666E7BBFE44A9,
	NullSolverImpl_Destroy_mCEF74F887CB2BF0E13C8176A052B1539F51BD6AB,
	NullSolverImpl_InitializeFrame_m1A9943BBB1200D17394A300365060CC1C180C333,
	NullSolverImpl_UpdateFrame_mE9842C5421EB8BCEF09BEF66B7ECE4C6BBF68453,
	NullSolverImpl_ApplyFrame_m6E1688FD679EF4B17A6C885CD116D47885BA9A6D,
	NullSolverImpl_GetDeformableTriangleCount_m5B5658F9B5D43BC6CF7131229FE97D16E931E5AB,
	NullSolverImpl_SetDeformableTriangles_m3555C196B17530814FC47EB0D65B0B19428AE73E,
	NullSolverImpl_RemoveDeformableTriangles_m327A23C82D523DB2799DC060C88746468C9CB012,
	NullSolverImpl_SetSimplices_m31A164287A45DD5E59CD0C787A24C85DAB4C3D40,
	NullSolverImpl_ParticleCountChanged_m8557EBD352BB202123699487E00B843F3CA0DC0B,
	NullSolverImpl_SetRigidbodyArrays_mEC64908F48D6E3EE3F03975F21201DAD33E36848,
	NullSolverImpl_SetActiveParticles_m270CC166EEFCBB7494E52DFD1F7385B091AFB05B,
	NullSolverImpl_ResetForces_m02F4803EE6AB50D5563B60A00E5535B12F8E3B11,
	NullSolverImpl_GetBounds_m4D24F01134C67A078F93680525FB8DC5804A3DBB,
	NullSolverImpl_SetParameters_mDCC55383F8800DB49278298ABF8A176E6F5B3CAA,
	NullSolverImpl_GetConstraintCount_mA5540FE96215F61ADEFE829931F4D927E70019E0,
	NullSolverImpl_GetCollisionContacts_mD2C3B675F93A95F33D2472E11AD4F48C503B2B54,
	NullSolverImpl_GetParticleCollisionContacts_m55885C928BC24A887BD63F783CD979F085E07381,
	NullSolverImpl_SetConstraintGroupParameters_m3A8213C3F85B7A2187B6030CC6EDC4DE5CB505BE,
	NullSolverImpl_CreateConstraintsBatch_mB98D773275819E868529278DC289D6E084CC913D,
	NullSolverImpl_DestroyConstraintsBatch_m42D46BA79047F3A0DBD2B305549E97FA654EF2B7,
	NullSolverImpl_CollisionDetection_m3BE61763707C061EB9912C7E9DCD44C3F135C0FB,
	NullSolverImpl_Substep_m46BCCC5D64A69DD461D4EF42A3D227A2F53A6A08,
	NullSolverImpl_ApplyInterpolation_mB63837B2AE0542F306D50ABF7A05868F2A9A75BC,
	NullSolverImpl_InterpolateDiffuseProperties_m5D4052E47F4C0CE896A55E4932A7AB35F0001133,
	NullSolverImpl_GetParticleGridSize_mD74EA2D59BC1B4472754D76EFF3B4FBC969B2A66,
	NullSolverImpl_GetParticleGrid_mCBCC36302C00CB01775017E21F63754021E04BEC,
	NullSolverImpl_SpatialQuery_m252FFC078850F330BB8276D6996CD2303F90D6F1,
	NullSolverImpl_ReleaseJobHandles_m021F504138409DDC3785A8FEB37B25B3DA81BE24,
	NullSolverImpl__ctor_m835D73EC52194B7861F10BCEC2C8F3D30E98D6EF,
	OniAerodynamicConstraintsBatchImpl__ctor_m08CF6D0353900E2F5161D01B30D6D4FD181EAF67,
	OniAerodynamicConstraintsBatchImpl_SetAerodynamicConstraints_m78BC6505E02E608B6E7E64C31C785C0653DCCDD5,
	OniAerodynamicConstraintsImpl__ctor_m525B8236B34A2BEB8A129BC2DFCAD6433E10673D,
	OniAerodynamicConstraintsImpl_CreateConstraintsBatch_m3C15118FE42A98BE00C420EFF5CC108133E8C9D1,
	OniAerodynamicConstraintsImpl_RemoveBatch_m7312BFD8301F75F44EF449DAB1CA208CDF256820,
	OniBendConstraintsBatchImpl__ctor_mD888A9E984F69E1F75358F7C87F224E5BBFEF9C1,
	OniBendConstraintsBatchImpl_SetBendConstraints_mACD9168310968682EC7F9FAF4FAF01D439561251,
	OniBendConstraintsImpl__ctor_mDF7E828956758CC442DFCE530D6ACB3DA28C7520,
	OniBendConstraintsImpl_CreateConstraintsBatch_mE943BA310CF0508FA4B457AE9C4CEDE436C0256C,
	OniBendConstraintsImpl_RemoveBatch_mE65A138E34D866312EA8D6811377B0D2A5A8F759,
	OniBendTwistConstraintsBatchImpl__ctor_m8F131CD09F6F7F09BBDA55AF3E953E5EA2D1F52F,
	OniBendTwistConstraintsBatchImpl_SetBendTwistConstraints_m93208190B4C9F21B66804826F3F388365464C7E6,
	OniBendTwistConstraintsImpl__ctor_m2D810DA4120C0AB84298B4A542CA5B932E774DCB,
	OniBendTwistConstraintsImpl_CreateConstraintsBatch_m5F1FC4850DB914A408732085F9AAEB1FE7D9DEF9,
	OniBendTwistConstraintsImpl_RemoveBatch_m571856C1E93A0B2507FD9309A2984CAD3143BB55,
	OniChainConstraintsBatchImpl__ctor_m11A76775D8DA7D09B7DACBF1D1168D4FEF4F1694,
	OniChainConstraintsBatchImpl_SetChainConstraints_mA704113F26DE40220400DE5B85B1475C2B3D0A82,
	OniChainConstraintsImpl__ctor_mAE3D234A2597D468A4621529AECFC4CD2D1BB328,
	OniChainConstraintsImpl_CreateConstraintsBatch_m84FCCD1FC3FF8E6DE2FF68D3B259A0AF6E595B9D,
	OniChainConstraintsImpl_RemoveBatch_mD1FF35D0BD359EB12FC8A74DE8A560E487A9057B,
	OniDistanceConstraintsBatchImpl__ctor_m9005CD0907A9E6BACBBDA3777B6717B8611AFA18,
	OniDistanceConstraintsBatchImpl_SetDistanceConstraints_m2305BC0CB077A2F81D031356D983305E8344B34B,
	OniDistanceConstraintsImpl__ctor_m46EF17FABD8A1BFB1EDE24EDE0F6EB61473907F8,
	OniDistanceConstraintsImpl_CreateConstraintsBatch_m68ACFFA63F7B3ACF12200ACCDD9796BC1E00453D,
	OniDistanceConstraintsImpl_RemoveBatch_m4F114348DEFF33909A2AFCF15D10D98187F7912C,
	OniConstraintsBatchImpl_get_oniBatch_mD3068ADFBFA72D2C5AEC1E4493AF44A61BAAB3D6,
	OniConstraintsBatchImpl_get_constraintType_mA4D89C531D6E50262AAB808ED481F01FCBC4554F,
	OniConstraintsBatchImpl_get_constraints_mC86F66AEBA1CCFE2EE1484C2CA0844257A7A0CCD,
	OniConstraintsBatchImpl_set_enabled_mE3FE79E17C8786437000CF9DF7ED7CC24C58DD1F,
	OniConstraintsBatchImpl_get_enabled_m74C3931C9C466399F511D1258F6DF49FAF007D8F,
	OniConstraintsBatchImpl__ctor_m7E856D436221DA306E7EFF8409F799DC8F926C38,
	OniConstraintsBatchImpl_Destroy_mB934F5194DA3A35DAC45581D183BE8C1BB056A1A,
	OniConstraintsBatchImpl_SetConstraintCount_m9A9C7B081DBCAFE727F2682F742B32A0A5CD1FC7,
	OniConstraintsBatchImpl_GetConstraintCount_mBC3CD543643EBE713BEDBCEC5266DE52F0E6E53E,
	NULL,
	NULL,
	OniConstraintsImpl_get_solver_m44585FB563C805AF280120F7FDE3B5737F4C0411,
	OniConstraintsImpl_get_constraintType_mF0F59F28976DF29B32C94C4156BD7752105087CC,
	OniConstraintsImpl__ctor_m9A0E8C6A195C96B8CB7EF27D99B7A0988672BF61,
	NULL,
	NULL,
	OniConstraintsImpl_GetConstraintCount_mDE65EE94DCFAB8905A9AEF5543E7119928F2900A,
	OniPinConstraintsBatchImpl__ctor_mA95003D5E91633FA72AA1F912FFCC947CBFC25CF,
	OniPinConstraintsBatchImpl_SetPinConstraints_mB28FD52B804E30D4B06AADE5A8764C9F3F4DC223,
	OniPinConstraintsImpl__ctor_m2F0E0AF0C30CE5AA9BBA5B0F2C170582A8442ABE,
	OniPinConstraintsImpl_CreateConstraintsBatch_mF0305992D09B680091BD1EEF5519A801F19F69D0,
	OniPinConstraintsImpl_RemoveBatch_m1EA7BE417BEF16951FAD0CA1106DB5BC257F38E1,
	OniShapeMatchingConstraintsBatchImpl__ctor_mA35A5ADBE7591DFAFCD6775D89CD7C7BBEB7E25D,
	OniShapeMatchingConstraintsBatchImpl_SetShapeMatchingConstraints_mDDBCB5CFAB68DDF0E19581524119A7981D214588,
	OniShapeMatchingConstraintsBatchImpl_CalculateRestShapeMatching_m6E5073835BB3D1CED559A45D987CC3FCD9D4C21D,
	OniShapeMatchingConstraintsImpl__ctor_m945AA881838FE9644266E98CA531247FE004C1D7,
	OniShapeMatchingConstraintsImpl_CreateConstraintsBatch_mB055C2DDEB21A68DC4E3A56E2A989231D1109BBD,
	OniShapeMatchingConstraintsImpl_RemoveBatch_m0C9FC7FFB2680541A78037980C49C907F3598756,
	OniSkinConstraintsBatchImpl__ctor_m5DDA5D3F347C6EC9FF501C52A4374AD88A10AABD,
	OniSkinConstraintsBatchImpl_SetSkinConstraints_m8065C2605A9D036F95C2F1B234883144F9C84CD3,
	OniSkinConstraintsImpl__ctor_m9A29E8FD8F9F99978DFF2EB7149829624A7939A5,
	OniSkinConstraintsImpl_CreateConstraintsBatch_m9FB2EAD640C26270C35A54351F354F8AA1D7E043,
	OniSkinConstraintsImpl_RemoveBatch_mC0102FC9F40941F1DDEBCDA28632CBDD43660523,
	OniStitchConstraintsBatchImpl__ctor_mC565125A40106134B8A526F21E536F819AACDB34,
	OniStitchConstraintsBatchImpl_SetStitchConstraints_m850BB5F12FBB10D113073FFD360406906FE84F55,
	OniStitchConstraintsImpl__ctor_m5A0F9B6CD43ECC7D7AE65AD107B559178559308C,
	OniStitchConstraintsImpl_CreateConstraintsBatch_m94119E21DE281CD292B556C5077176B4D9C9284F,
	OniStitchConstraintsImpl_RemoveBatch_m7B1ED49DA5FB278734D0C9F8597E820B3E284FAC,
	OniStretchShearConstraintsBatchImpl__ctor_m06A2F02D467608368981ECD3800852E4B1229908,
	OniStretchShearConstraintsBatchImpl_SetStretchShearConstraints_mACC9CB76D4FDD53A4FDD96FA2031EBD5F893B6AA,
	OniStretchShearConstraintsImpl__ctor_m743767DB7C25C334E9584FACAF87392343A39ACC,
	OniStretchShearConstraintsImpl_CreateConstraintsBatch_m78706F1FEE7FEAA5EF471FAB390E0E0D71861127,
	OniStretchShearConstraintsImpl_RemoveBatch_mF8F67B57D02D49DEC0DC0197146B243A44090457,
	OniTetherConstraintsBatchImpl__ctor_mA18D6CEBB82288A3B578C8C4C52F4E0119DAB0B4,
	OniTetherConstraintsBatchImpl_SetTetherConstraints_mCA9A516027A94559796B9C29CD716F10CB4BEFD7,
	OniTetherConstraintsImpl__ctor_m79D07270D5F4E7E62395AA8BE2B0F5B2C5B9AD25,
	OniTetherConstraintsImpl_CreateConstraintsBatch_mD49AD0DA234A2CBAF481401273DC88CF16D87AD7,
	OniTetherConstraintsImpl_RemoveBatch_m4B4909C5425342565704DFD35EDD47C5A1E2FB20,
	OniVolumeConstraintsBatchImpl__ctor_m17E79E7DCFE92922A8861BE569C3FF460D175CAE,
	OniVolumeConstraintsBatchImpl_SetVolumeConstraints_m9C580C7D330A95E2032129920D22CC13D70E8AFC,
	OniVolumeConstraintsImpl__ctor_m5147F9E64F13F4D205515CE6F1906887F787BA74,
	OniVolumeConstraintsImpl_CreateConstraintsBatch_mF7765774D05626A213C729351AE71CF664A0A8B2,
	OniVolumeConstraintsImpl_RemoveBatch_m726F19C3F55138E1BB32C8D3C97804E44013547C,
	OniBackend_CreateSolver_m5E3C44A4EA9BCDD470E7E10E684F0AF897C8AF5F,
	OniBackend_DestroySolver_mC6AFB023EC9F9532FF2B840DDF955E89C71A0D01,
	OniBackend_GetOrCreateColliderWorld_m92C5F279126E62C80B84C7ACAC6FEB209751EF1D,
	OniBackend__ctor_m45B8DE94A53B0740C6704A51A4BC4FDEB7C7D4F7,
	OniColliderWorld_get_referenceCount_m2E1D878672ADF99B997AA9EF3050BD41B27DC9FD,
	OniColliderWorld_Awake_mBB4F8B9A43CB88C337CD9DB344E0B7169D47E923,
	OniColliderWorld_OnDestroy_mAF83AA5D931A38B0E55E2226B94527B592FD9D83,
	OniColliderWorld_IncreaseReferenceCount_m78FCC3BFF315F84942D3994F5092A54A8D7F678E,
	OniColliderWorld_DecreaseReferenceCount_m35AE9297895C720AC03E0D0F320DDAD1366BF64C,
	OniColliderWorld_UpdateWorld_m07BB5250B08FE6F9B66FBDC04C2B54A98C4B4251,
	OniColliderWorld_SetColliders_mBC49AA3ED5E83E70CE733DCEC0683FF672872263,
	OniColliderWorld_SetRigidbodies_m28F9E1B58302DB5A756C243EF52E788E708DA85F,
	OniColliderWorld_SetCollisionMaterials_mEB26C8AC30F1B5CE27D8F19A77ABB7B252115E0D,
	OniColliderWorld_SetTriangleMeshData_m4965F3D38925BFCACB96B6B3479DD4F232ED7104,
	OniColliderWorld_SetEdgeMeshData_mD9F54F0603E44D518BC9243FF1979EC24E25FD44,
	OniColliderWorld_SetDistanceFieldData_m2C1ADF6E94752BC41CE632A804CBAE1AC19A9EDF,
	OniColliderWorld_SetHeightFieldData_m4C593F78531A7AE191FA9F8D4A8BBFC23E84F68A,
	OniColliderWorld__ctor_m4DE663B94B5C376427CEB42288BA88428E549A52,
	OniJobHandle_SetPointer_mA85FEFE91D585455A4BA43722BD3AB87A838603C,
	OniJobHandle_Complete_mB9E3D94D50CF207A0F961C9F4ADEB30B2FA8A6DE,
	OniJobHandle_Release_m7B54D7F8A274FFC942C4860D5E28DFF47284129B,
	OniJobHandle__ctor_m514FBCF3D1AAF7DA9034107984D0DED6C5510C73,
	OniSolverImpl_get_oniSolver_mA021288BE5461FA7F14399158965ECDD9ECA1706,
	OniSolverImpl__ctor_m2FE0413D6AF7939DBE480B789D43360A39DDFAD4,
	OniSolverImpl_Destroy_m317E88AD3A919043461CE59F399DF3670A904961,
	OniSolverImpl_InitializeFrame_mC8E921CBD6A70A2F9F46310EB5556586B6749DC7,
	OniSolverImpl_UpdateFrame_m52DA894215FDD134640AD29208E780EEBD65180A,
	OniSolverImpl_ApplyFrame_m67E28FA4B4FBE5C072E359F3574F562C59820DB4,
	OniSolverImpl_GetDeformableTriangleCount_m230CCC63F4275668FF4EF1B34F56859D5C70E68F,
	OniSolverImpl_SetDeformableTriangles_mE67CB6225F084DE826F48AA4D27E43FA43BE9827,
	OniSolverImpl_RemoveDeformableTriangles_m3B80FB7C6FA7DB0890B5BC37670151643F54C650,
	OniSolverImpl_SetSimplices_mD571A7AFB950F771AE96A6A30743D2E7CE5BAE5B,
	OniSolverImpl_ParticleCountChanged_mC72028DD674A3E54C4E08E758A3CE3D26E3E7577,
	OniSolverImpl_SetRigidbodyArrays_mFA7655D4B61E7E1C65C4E42C8B77EA55DFC70AEE,
	OniSolverImpl_SetActiveParticles_m850250C4A4DE24AB05AB59AADCD43D6F232077E8,
	OniSolverImpl_ResetForces_m5E8F36B6FCA63BD1E720DBFD5B8764863E3CE626,
	OniSolverImpl_GetBounds_m478CF8A2FAB372FE347E9899D262869163ACCB57,
	OniSolverImpl_SetParameters_mF506A029DB22FF674610C76D892D387BAD7178A5,
	OniSolverImpl_GetConstraintCount_m7C95C3E195679C7076DEBAB096213874408B03EF,
	OniSolverImpl_GetCollisionContacts_m3179A5E075C13134C753E47D58F150C004D15906,
	OniSolverImpl_GetParticleCollisionContacts_m4B99957B57A3E6BAF05C9E3D64DF28B5E0748259,
	OniSolverImpl_SetConstraintGroupParameters_mB58C6320D75A44B682B4AEA82214F8ACA62E53DA,
	OniSolverImpl_CreateConstraintsBatch_m388EEDF658EF8F63CD78DC8DC33F0DC642688911,
	OniSolverImpl_DestroyConstraintsBatch_m7769244161551FB17B31600D453AC2F68F156860,
	OniSolverImpl_CollisionDetection_m56F7F7DC6C224E1DEE5CF8A1437ED6FE02BAB5E2,
	OniSolverImpl_Substep_mCA3F4A59045D0A506692A48C78ACE6BB4E884D93,
	OniSolverImpl_ApplyInterpolation_mAD4820F8A797FF35107C1C920D5B50D4415C67AD,
	OniSolverImpl_InterpolateDiffuseProperties_m0678F52DF465E6E9B34DECE91C6C3D48F96E8821,
	OniSolverImpl_GetParticleGridSize_mC858C1BFF95D86B1B0841E40378F882A014E8E78,
	OniSolverImpl_GetParticleGrid_m3A454F1EA5EDCF6A590F99A29EEAC84F0D65B790,
	OniSolverImpl_SpatialQuery_m6BD4BCC1AACE17D8D66365F9FE7F16012A1E8F7F,
	OniSolverImpl_ReleaseJobHandles_mC05A2D743B7E73A54FBBD5BBB93CA5C8FFC3548F,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsBatch_get_constraintType_mBC6B3C472BCF899A37C3C9F15B61F0B22DB160F4,
	ObiAerodynamicConstraintsBatch_get_implementation_mD6185053ABF60610B561CE15B51770F6E2556E08,
	ObiAerodynamicConstraintsBatch__ctor_m9DE858EA294B273AEED18201A289AEC50F28F02E,
	ObiAerodynamicConstraintsBatch_AddConstraint_mB9A927126FB700DC1E84DDA91D9132310EFEE124,
	ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m430311A6B436224B845F2F196525AD41A67EB605,
	ObiAerodynamicConstraintsBatch_Clear_m5ACDB0CA34591C9F454153521B67AC2478467EA2,
	ObiAerodynamicConstraintsBatch_SwapConstraints_m3F24E8A02E9F7E06130C7E27A8438623CBC36B99,
	ObiAerodynamicConstraintsBatch_Merge_m4CEBEA6CE4426002B0326CFCAE2C25E3F4954CD0,
	ObiAerodynamicConstraintsBatch_AddToSolver_mF2BA5925F8D160264B0EC1995C4EF9856E4E48FB,
	ObiAerodynamicConstraintsBatch_RemoveFromSolver_m3288141E2907B24EF4337F9C513C6C69D68D8684,
	ObiBendConstraintsBatch_get_constraintType_mA302627388787213E045E481476B7C9EB7F43D92,
	ObiBendConstraintsBatch_get_implementation_mE6CC10221A772592842A23E0EE91871FAA8ED5FA,
	ObiBendConstraintsBatch__ctor_mA90DDA31688111131E370BE9154E15ED99CF09E9,
	ObiBendConstraintsBatch_Merge_mC7EBF91A7220B4C03792E048ABB718337C6F03AA,
	ObiBendConstraintsBatch_AddConstraint_m59872A1ADDD39CF3FB2C303EA08FD4E6E9AFAA0C,
	ObiBendConstraintsBatch_Clear_mA0DC32AE2AE98E09AF547DE49FB25D8CDFF0DB97,
	ObiBendConstraintsBatch_GetParticlesInvolved_mA9C31748143F96BC5E341BE711B68E9F78204C4A,
	ObiBendConstraintsBatch_SwapConstraints_mD42CD848B36CEB0540B9F211A1C7592348949487,
	ObiBendConstraintsBatch_AddToSolver_m65C74A506765E2DFFD6575EE0E9D8AF1F723EBC1,
	ObiBendConstraintsBatch_RemoveFromSolver_m771E799A2D261FFC3AB2686EB70676ABEBCE21F7,
	ObiBendTwistConstraintsBatch_get_constraintType_mAC26F20B9CE45F7ACE966652B0ACAB8337A9703D,
	ObiBendTwistConstraintsBatch_get_implementation_m293D659D51624728D74EB2EC1BEF2EBA93A301D2,
	ObiBendTwistConstraintsBatch__ctor_mA84FE1A64BC385B93B82ACDB515147D1BFD9D8E9,
	ObiBendTwistConstraintsBatch_AddConstraint_m04DEFCAA48B91E8849ECC4FB1F151C17740B3E78,
	ObiBendTwistConstraintsBatch_Clear_mEA7D8BB6BCCA8C72D2CB42CF1D9C197F3C3FB905,
	ObiBendTwistConstraintsBatch_GetParticlesInvolved_m7F039D1FBFA40A6E0EFFB3F66FAA2A5D4D621393,
	ObiBendTwistConstraintsBatch_SwapConstraints_m19754F194196E8509DE60D4C6A4E01CDE61951CC,
	ObiBendTwistConstraintsBatch_Merge_m65D2EB86911176AE0DBCFC755950330B0FF4D9C4,
	ObiBendTwistConstraintsBatch_AddToSolver_m00517D42056B5BD7E11AF70F1C617FD33E313B35,
	ObiBendTwistConstraintsBatch_RemoveFromSolver_m89781A48762FBA02AFF8BE9423981220F26A147B,
	ObiChainConstraintsBatch_get_constraintType_mBAD7C6C6A0862F3A03015BCA2BA2B969706672C7,
	ObiChainConstraintsBatch_get_implementation_m2883B97D7E6175B2C1FE458CD3E667789C1FEC92,
	ObiChainConstraintsBatch__ctor_m8E8E6310A3DDA3527AFF7F0DF6A851F75B52AEDC,
	ObiChainConstraintsBatch_AddConstraint_m8615D25C5C8628CEE00BD28F9D2D465A635813CB,
	ObiChainConstraintsBatch_Clear_mC5BA1FD8A165468C43063493EF8BFB93A3D2CE21,
	ObiChainConstraintsBatch_GetParticlesInvolved_m1B0852EE90B8C0089C7793F4915B9D5C5E739187,
	ObiChainConstraintsBatch_SwapConstraints_m56826B5809EDF1D888AC022EC80813AB5E8A8029,
	ObiChainConstraintsBatch_Merge_m7C8E59CC49A14B6A3F20B923CB9983A18709369E,
	ObiChainConstraintsBatch_AddToSolver_m8E12FD5F4ACAA95E5FFA4E57A15C6D01796354AA,
	ObiChainConstraintsBatch_RemoveFromSolver_m97FD3AAF49F7296DC655C1BE2B0C866D40330AAB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_get_constraintCount_mA8FC55D7B67C6DE94D5B7383C39D4330CEC3C4A8,
	ObiConstraintsBatch_get_activeConstraintCount_mC2764F79C4929BA7245103D0DB1CFB1D6203FC02,
	ObiConstraintsBatch_set_activeConstraintCount_m5C26A4A50F8C1542DD4EA73BDACF3814B758155A,
	ObiConstraintsBatch_get_initialActiveConstraintCount_m5F0841BDCCB934AB148E2A870F3EAA2F8552189B,
	ObiConstraintsBatch_set_initialActiveConstraintCount_mF8081C7DCFDC4CF1B2A306517632F6E9C026912C,
	NULL,
	NULL,
	ObiConstraintsBatch_Merge_m0157340350A52C6574BBB2D68D612AA6852ADEC3,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_CopyConstraint_m204FC07814B69BA73E78A0E743D059F565B85874,
	ObiConstraintsBatch_InnerSwapConstraints_m72121F6E08F53AA93A94891FB98F3B6FF2D23A91,
	ObiConstraintsBatch_RegisterConstraint_m8EABAFCE01074053D942CE05D9CF5CEC9381F5FB,
	ObiConstraintsBatch_Clear_mC782D142E91B8F81F511A011339846EC1310C2D5,
	ObiConstraintsBatch_GetConstraintIndex_mA7193601AF174D3A9323DCBC41B86CF2F9EDF24B,
	ObiConstraintsBatch_IsConstraintActive_m86CFCB36A5A1AEBBE1FA877DFEE386BB244A7C65,
	ObiConstraintsBatch_ActivateConstraint_m715C1DA819EA321555CC3E50E102E4130B0DF12E,
	ObiConstraintsBatch_DeactivateConstraint_m75EE8E2E70006545791D2BA14E1E4513F9193025,
	ObiConstraintsBatch_DeactivateAllConstraints_mC7259C1E725CF3BFB8EC6ECFD548E6C70C2F63C5,
	ObiConstraintsBatch_RemoveConstraint_m5AAEA90963297F57C4BFF0C6858AA1E13AE6EC46,
	ObiConstraintsBatch_ParticlesSwapped_mFA21282517AF5D3D422315E8748273AC39BC7ED1,
	ObiConstraintsBatch__ctor_m2B0D34B60AB411AA8D929BA04F3BF98231678A81,
	ObiDistanceConstraintsBatch_get_constraintType_m738D4B915ECC57433D1FBE3736930F510797A6D5,
	ObiDistanceConstraintsBatch_get_implementation_mB2318A9886BF7C3D0513E65A18526B4F28740515,
	ObiDistanceConstraintsBatch__ctor_mE47DA7819812295CB5E274C6A916E54E705A0EC3,
	ObiDistanceConstraintsBatch_AddConstraint_m118C1575A23FFAC933204048E9DD894F44CDD021,
	ObiDistanceConstraintsBatch_Clear_mEE9F3DDD52ECC58C3A9B4391667F88D3FF4B48C4,
	ObiDistanceConstraintsBatch_GetRestLength_m5E1C408F549700872BADA871F7301C1A368E0F12,
	ObiDistanceConstraintsBatch_SetRestLength_m1CA836FD76DCC1AC15BECCF86DD92E487F71397E,
	ObiDistanceConstraintsBatch_GetParticleIndices_mB6664FFE2333714B74F7BFEE6D915DC49C3812C6,
	ObiDistanceConstraintsBatch_GetParticlesInvolved_mC73EBE0B6DB236EC4BE938CB43ACE50F2AE65EFE,
	ObiDistanceConstraintsBatch_CopyConstraint_m0EBF810160015C8A19A39367C329D343DBD1BF44,
	ObiDistanceConstraintsBatch_SwapConstraints_mFC45320687BA195BF90408368D14ED2B804DC370,
	ObiDistanceConstraintsBatch_Merge_m4D30F883821CC7CF022584BEC90B548D14C6DC15,
	ObiDistanceConstraintsBatch_AddToSolver_m3308FD903CF4BC955B8D2F2E0B387D2C8A79B7B8,
	ObiDistanceConstraintsBatch_RemoveFromSolver_m6BC9C6E426F431C44B10DCA8CBFFEF4F67E0936E,
	ObiPinConstraintsBatch_get_constraintType_m41C00D52B2D27084965D72E1F35D4E075444E482,
	ObiPinConstraintsBatch_get_implementation_m4578AAC572F0C5466E210AC1BC23930F8C09C0E6,
	ObiPinConstraintsBatch__ctor_mEE99790C9B386AE46D24F129CDDCF2C1C5B5F5FC,
	ObiPinConstraintsBatch_AddConstraint_mF22DD07678C73491928469A7741BD8B076071DCC,
	ObiPinConstraintsBatch_Clear_mB0A07E7DF21B6C3CF9C5E054F58510FFDAA522AE,
	ObiPinConstraintsBatch_GetParticlesInvolved_m91C044A47CCA58E91BD3E72C7DE0ECE74BC37DA8,
	ObiPinConstraintsBatch_SwapConstraints_mAE0389E323855EC02CB78CE15E73076B72CB1D85,
	ObiPinConstraintsBatch_Merge_m2E0E93A262BDA66314A1CF7125B02B0E3D3AA3D7,
	ObiPinConstraintsBatch_AddToSolver_mDAB05B2150F51AA80C2911CEF1BCE5E104D37E86,
	ObiPinConstraintsBatch_RemoveFromSolver_mC766AF925EE815328507994A26FC0DA6399A473C,
	ObiShapeMatchingConstraintsBatch_get_constraintType_m1D1B013631E72B441D2F440FABC9A47260BC1EFC,
	ObiShapeMatchingConstraintsBatch_get_implementation_mDE72A361D520AEF2417D5FA3D99265B24AE622BE,
	ObiShapeMatchingConstraintsBatch__ctor_m07C5E1D77AD00968700D64371442119BD0EFD860,
	ObiShapeMatchingConstraintsBatch_AddConstraint_m36DB2368774B955435601E74A559F9194797B73E,
	ObiShapeMatchingConstraintsBatch_Clear_m7613DFE3A4A449971A4C9C0E7F78D8ED7D99B7CF,
	ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m460EACD5CA047B22B48866E7980990188EF04B88,
	ObiShapeMatchingConstraintsBatch_RemoveParticleFromConstraint_mD284790EE70116086A1EF3AF9565322E629039FF,
	ObiShapeMatchingConstraintsBatch_SwapConstraints_m203CA70E418F8DF2BF506ED333695247E286BA2B,
	ObiShapeMatchingConstraintsBatch_Merge_m5A9DEA535310B3863D91C0FBE5A52C051E0357A7,
	ObiShapeMatchingConstraintsBatch_AddToSolver_mA64BACE29C81AC0D914642A2326C5A768BCAD690,
	ObiShapeMatchingConstraintsBatch_RemoveFromSolver_m64F2CB468C81ED844CEE845FC280A5497A18AD33,
	ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_m49C8363C8157E30089548C4BDF8BDA633D26DDB7,
	ObiSkinConstraintsBatch_get_constraintType_mB8336F19B6DBE7777042FA4814EA12115D899223,
	ObiSkinConstraintsBatch_get_implementation_m8EFE8C0407125BE90F4A544FD0BBD6801F606924,
	ObiSkinConstraintsBatch__ctor_m1C9F633F90BE267DDA66B580DED05A7F19BD61C5,
	ObiSkinConstraintsBatch_AddConstraint_m5D3D3C2490F09B0C7B72F763AA56E3AE9D485134,
	ObiSkinConstraintsBatch_Clear_m9760EF8B82970CC9CDE9A840BD9EAF78BD982393,
	ObiSkinConstraintsBatch_GetParticlesInvolved_m77E2085BC3B66F68FE4B9453F0D76A9566718597,
	ObiSkinConstraintsBatch_SwapConstraints_m90010A1D7C64381F5575F0F393B7030C2201DF8F,
	ObiSkinConstraintsBatch_Merge_m5AC677F66016C8CC6DCC768E4A60ECEE633075A6,
	ObiSkinConstraintsBatch_AddToSolver_mF9FF0C39D2BA6C21C642F2F0880004B3D719DC2C,
	ObiSkinConstraintsBatch_RemoveFromSolver_m146DF16BA35D6BBBAE5781504D744C59D61D80FE,
	ObiStretchShearConstraintsBatch_get_constraintType_mC6DEA77B39D02D597E2CA898182A9BD560B545C6,
	ObiStretchShearConstraintsBatch_get_implementation_m0DAC376106BC35C967BE578A40049E3C35E27024,
	ObiStretchShearConstraintsBatch__ctor_m8927550DA897869840F27FAE27254EF2961AEFB7,
	ObiStretchShearConstraintsBatch_AddConstraint_m3EDB080395E5167A460038CE0B00CEEC306C19D5,
	ObiStretchShearConstraintsBatch_Clear_m827C6A5985D286459089DC4635044609BC6B098C,
	ObiStretchShearConstraintsBatch_GetRestLength_mC0368700D80E14C1F758085E1051265DB7E6F140,
	ObiStretchShearConstraintsBatch_SetRestLength_m4401CCB79E8593DE77C2F66191AE967C6DC7E5DC,
	ObiStretchShearConstraintsBatch_GetParticleIndices_mC265643A55371F01BD13A0C3FC34A11D9FFE838C,
	ObiStretchShearConstraintsBatch_GetParticlesInvolved_m304EEFB3092A0112EAE84B4CECF3F6A948D9833E,
	ObiStretchShearConstraintsBatch_SwapConstraints_m2C8677D96612ADAE5BFCC4825A9BA4E42685609E,
	ObiStretchShearConstraintsBatch_Merge_mC487E3A37D51D78F57A1880A27B8168E45F05BB8,
	ObiStretchShearConstraintsBatch_AddToSolver_mA25B51FDA1EBF1E1BE6AD594D45C153216AA8C94,
	ObiStretchShearConstraintsBatch_RemoveFromSolver_m45797BE9F2A7CC2B0BD983E810A313C126621382,
	ObiTetherConstraintsBatch_get_constraintType_m8483A4DC0E6B622F67EC74C1BB113C00981EFA6C,
	ObiTetherConstraintsBatch_get_implementation_mC6C6A9923482CF3A380B6D250E1A63C0EC69E02F,
	ObiTetherConstraintsBatch__ctor_m8866FC2DA610911877A83CE2904D78CACB19FC51,
	ObiTetherConstraintsBatch_AddConstraint_m1BF702B37FD24AD678D201379C614BFA7C24824A,
	ObiTetherConstraintsBatch_Clear_m2F60F212808B2D2566D9486751BA43D287D7A616,
	ObiTetherConstraintsBatch_GetParticlesInvolved_m77FB1AD6E9F76D24F63C6CF2FCA60F8627D497BA,
	ObiTetherConstraintsBatch_SwapConstraints_mE3AD347DF0A9C8FA3D0467F87FFEC0B13FD61E72,
	ObiTetherConstraintsBatch_Merge_m1AD265CC0EE520D80F27DEFBE161FF9E44B80A76,
	ObiTetherConstraintsBatch_AddToSolver_mD57866191E80F88A3D272C0E0AC76364326A80E3,
	ObiTetherConstraintsBatch_RemoveFromSolver_mEE3D5BD7C2CF365225947E773A815095121EF2C6,
	ObiTetherConstraintsBatch_SetParameters_m1577626B4E1C0111CF067DABC451BC0503615466,
	ObiVolumeConstraintsBatch_get_constraintType_m381ED3FD4921DB235D1AE2E0D93D354C840CE86A,
	ObiVolumeConstraintsBatch_get_implementation_mA945BB5C6B7A7EE83AC6C01C6B5DD21207FB0325,
	ObiVolumeConstraintsBatch__ctor_mA44A358BC97BD1830050A02DFFDF5108A6FD9715,
	ObiVolumeConstraintsBatch_AddConstraint_m411C62C1E1117BF0B3B6954C3641F5B418583424,
	ObiVolumeConstraintsBatch_Clear_m53B029F54892E0D5B9D4D2BCB647A4176FE793A0,
	ObiVolumeConstraintsBatch_GetParticlesInvolved_mC38E81ADD236D1D8841141D16BA6989DA3A489F2,
	ObiVolumeConstraintsBatch_SwapConstraints_m42B669FC7CBDFA494210C6486682BD7A3CFFDDB4,
	ObiVolumeConstraintsBatch_Merge_m1DDAB49D1408ADCD12D27C72737072480252DA7B,
	ObiVolumeConstraintsBatch_AddToSolver_m1070A8D58A5B37CC00B5290D1448490F3DD9CE29,
	ObiVolumeConstraintsBatch_RemoveFromSolver_m01AB99DBC04273CEB49EFAAFE595F93D5F05B726,
	ObiVolumeConstraintsBatch_SetParameters_m8B574D29F6430355A0AF20B229AA493E0119AEF5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsData_CreateBatch_mD91F60B676E69FE28DD53AA57D5E64E621F7E489,
	ObiAerodynamicConstraintsData__ctor_mD5BD0D829EC39F1CACD35CF16E040852B4B4784B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendConstraintsData_CreateBatch_mD0A372B63F6C4F88AC48D4E4E9304944D3FEA0F2,
	ObiBendConstraintsData__ctor_mFE69ED43DBE4731FAAAF8C32658B04596342980A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendTwistConstraintsData_CreateBatch_m9A87A7382C1A63A339F0C8B17CF14D1CB03B0D46,
	ObiBendTwistConstraintsData__ctor_mC6E27E26B877C88510C9E27FB9ED17BF94E98B9A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiChainConstraintsData_CreateBatch_mC2FB00A47639537C6959AE16661DAAF1AD4C3F7B,
	ObiChainConstraintsData__ctor_m9D1C51412A1878F85D8C173777A60EC621A9FFDD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiDistanceConstraintsData_CreateBatch_mA63574AD8629D5C6907EB15B3FFA9F0D1616D026,
	ObiDistanceConstraintsData__ctor_m10E8E535222BCC55670623BBB7DA36CAF7A2A95E,
	ObiPinConstraintsData_CreateBatch_mC8E21D95D7C5E27097C6B75B20DC74CBDD2D345B,
	ObiPinConstraintsData__ctor_mA86A434A33F8CF37FA8E7E2410BF8223116F03A0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiShapeMatchingConstraintsData_CreateBatch_mDD0753126D9E04AB8FD0D7F4CB4D6C9ACA8BF359,
	ObiShapeMatchingConstraintsData__ctor_m39A87174E47FB428D6589800F2F18FE1F79D340A,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiSkinConstraintsData_CreateBatch_m0420AD7727A873F46A9E43F8BF1F94895AB14369,
	ObiSkinConstraintsData__ctor_mCB9A63E7B637DEAEB1063E52913985321ABBCAA3,
	NULL,
	NULL,
	NULL,
	ObiStretchShearConstraintsData_CreateBatch_mFBE3B0B12D6BD8E226076AB93E16906F92B60C68,
	ObiStretchShearConstraintsData__ctor_m81576073ED6B14D835150C4666A3588AEDBD29FC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiTetherConstraintsData_CreateBatch_m28C477BD204B78B77C867473CB959C5340B918D7,
	ObiTetherConstraintsData__ctor_m7488642C0CCCDA9C252605F19F887C961D3BB5C4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiVolumeConstraintsData_CreateBatch_m1DDE0FAC895B8FC54D095EB773D7AEBDE107A061,
	ObiVolumeConstraintsData__ctor_m09F646B51D450E9B87B194F9E29B8B5FE9C2404B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57,
	StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF,
	StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322,
	GraphColoring_Colorize_mC1E72C7E56EE6F1F72682546D5CB94934287958C,
	ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E,
	ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E,
	ObiActorBlueprint_get_particleCount_mCC6F47C83D7F2BE2BBF1667511B5C96BAA5A94B2,
	ObiActorBlueprint_get_activeParticleCount_m22AD50CE827DB1E01A7937D968032E6AF08B228A,
	ObiActorBlueprint_get_usesOrientedParticles_m8E951A7BDC8EEA54633C2AA8BE8E41F01A418233,
	ObiActorBlueprint_get_usesTethers_m7A16422C66E95F3984D2A3A8F950305E9D5C2026,
	ObiActorBlueprint_IsParticleActive_mD079A02C9E95DB8F8900FA0C1FE9A057C1F1D05A,
	ObiActorBlueprint_SwapWithFirstInactiveParticle_mCBD4A74BAC682B6DB6228F3B53FCC5D90B069126,
	ObiActorBlueprint_ActivateParticle_mA772B1D5DA1364AA8A889F069D9F903E565A3F02,
	ObiActorBlueprint_DeactivateParticle_m2EFB2F10CE118C9041593C482C6FC80F24A9C8A7,
	ObiActorBlueprint_get_empty_m5407EABADC0A7CFE5EBBA320717C3B4609F2FDD1,
	ObiActorBlueprint_RecalculateBounds_mC32E8C37E376B8342D46E8894A36161EDF37E48A,
	ObiActorBlueprint_get_bounds_m8F16C4CEAFB92614DEC83B9F41D565B4FE542C15,
	ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597,
	ObiActorBlueprint_GetConstraintsByType_mD32BF0C8BE5453BF0F534BCC92F2BD30477E7761,
	ObiActorBlueprint_GetParticleRuntimeIndex_m6495778E4FF39D59DA86401684ABED0361E6803B,
	ObiActorBlueprint_GetParticlePosition_m4B9F2D62EEF90A610C7DC8E1B07D4525ED90A081,
	ObiActorBlueprint_GetParticleOrientation_mE80B9B4333A1EE1670B7DAD73FD55F8C8F6F178D,
	ObiActorBlueprint_GetParticleAnisotropy_mBFB7DD70BF52D6DEC5E08DEFC5A8F07BBFCBE6B9,
	ObiActorBlueprint_GetParticleMaxRadius_m1106A97F11068907833E40301081A52749BC79F4,
	ObiActorBlueprint_GetParticleColor_mA1CA0E89A089B4CEBAFFD4EB83CBFC5EB5A6EB0D,
	ObiActorBlueprint_GenerateImmediate_mD3FF977E24F4965AB776BF1E1DF0272F3D6CCA33,
	ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A,
	ObiActorBlueprint_Clear_mD6317D8274462CCEF04D76D0957286FF73D692EB,
	ObiActorBlueprint_InsertNewParticleGroup_m5C512B2A9ED1227695249B3DDC9C19A9D421AAAD,
	ObiActorBlueprint_AppendNewParticleGroup_m33A05025BCF9CD281364DDA0E3563AE5091E6D79,
	ObiActorBlueprint_RemoveParticleGroupAt_m583F9722B398FB85BFFB3AA17AB4E53424FD4E18,
	ObiActorBlueprint_SetParticleGroupName_mEC41E6FFFC12BB73B6BA214F81CF70CE0A034A34,
	ObiActorBlueprint_ClearParticleGroups_mBC589A70CC5E642C10FD126B482590D27FEC8B18,
	ObiActorBlueprint_IsParticleSharedInConstraint_m9F71E2E0E9B16D9CCB47B7D270391F65232D64A1,
	ObiActorBlueprint_DoesParticleShareConstraints_mCCD929D6E8D050B9B2A377A3965B6F8E64538883,
	ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_m2F8B411DA9FE9FCDC8116158247E0E83EEA513E1,
	ObiActorBlueprint_ParticlesSwappedInGroups_m9104E8E53B4D76CFFD62EF8277E5313A73867EE5,
	ObiActorBlueprint_RemoveSelectedParticles_m1FD86679B5F6C5DEDED32F79658292F13566C63F,
	ObiActorBlueprint_RestoreRemovedParticles_m6968118C8E330062E76B6F79155E07B86C091400,
	ObiActorBlueprint_GenerateTethers_mE0CECBEF8A33A7E44D7EFD24A87697C45209B668,
	ObiActorBlueprint_ClearTethers_mA11CB91D97A72D5311C67FCC335ACA3900020EEB,
	NULL,
	ObiActorBlueprint__ctor_m9D95F18391AC084CE03CF008E1D47864DF75A636,
	BlueprintCallback__ctor_m75F65AE60505CE5651A0F020168360371AD0D0EE,
	BlueprintCallback_Invoke_m0F0A317733BA923A00DCF82C10E2AF745667377A,
	BlueprintCallback_BeginInvoke_mD8A830F18B9F7B76E81DB436AF1462092202B3F3,
	BlueprintCallback_EndInvoke_m35D1EBBFF55BAC420142369AEB4B24D44D031B3F,
	U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C,
	U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB,
	U3CGetConstraintsU3Ed__50_MoveNext_m440BD75AC003FACCE06308279346A5432EDC9BFC,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D,
	U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1,
	U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22,
	U3CGenerateU3Ed__59_MoveNext_m1B248A89761AD00C954EAEB25335090B6378CB9E,
	U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734,
	ObiMeshBasedActorBlueprint__ctor_m68ACFAD2ACE6D36D748D434E2BD130B742B17BE8,
	ObiParticleGroup_get_blueprint_m7B969093BC47FD5B58C6D6839D8F9E3BDA3119FF,
	ObiParticleGroup_SetSourceBlueprint_m83016ECF67DCB8BDF873C154BB4A0EE7A8091011,
	ObiParticleGroup_get_Count_m98490363FF19DE1FFEBC361D7302AEF05B6F21B4,
	ObiParticleGroup_ContainsParticle_mFF1BABB2B3D4D3CD4AE1E5B44C3DC83765EB11AC,
	ObiParticleGroup__ctor_mD6FDCA405D8CBEB22044CFD3635E3202AF1A45A2,
	ObiBoxShapeTracker2D__ctor_m628217C8133AD0BA22DE8815770C03CD31236C8F,
	ObiBoxShapeTracker2D_UpdateIfNeeded_m7CF0C0561350D4A21F8FC768C7158E4BAE328434,
	ObiCapsuleShapeTracker2D__ctor_m9F0647AFDA77ACDB5B00DDA8F614F2CD3F61F464,
	ObiCapsuleShapeTracker2D_UpdateIfNeeded_m65EF7C477B78DF113F5C43446E266DA9790B333A,
	ObiCircleShapeTracker2D__ctor_m91428FB089519F84073171B1EF2AF27FA7029F13,
	ObiCircleShapeTracker2D_UpdateIfNeeded_m855CD60D865E47BD776A38CE2F44554BD5B69E49,
	ObiEdgeShapeTracker2D__ctor_m9CF5E0F349BC73573202F01E32482578DE1C1A10,
	ObiEdgeShapeTracker2D_UpdateEdgeData_m7208F5B1FF03775D5A425836047FB54E9BB747C1,
	ObiEdgeShapeTracker2D_UpdateIfNeeded_m380635280878C5D86F90898E11CAD1664E3E4BDC,
	ObiEdgeShapeTracker2D_Destroy_m01DDDC54A11D898BAABA7391098DD4D2458CA946,
	ObiBoxShapeTracker__ctor_mE75CAF1F86BB53D6AEF98085C09D17E66DEEF582,
	ObiBoxShapeTracker_UpdateIfNeeded_mF819B8E4C30C6AFAF29A4117798FDF1316EA834A,
	ObiCapsuleShapeTracker__ctor_mBE6839D0297E242539ABACA7E8D10CC7D1FF90BD,
	ObiCapsuleShapeTracker_UpdateIfNeeded_mE44AC47537FB4437697D85609FB76AC306E29C1A,
	ObiCharacterControllerShapeTracker__ctor_m58178DB03B830C4137C4E297650BAB50EFD3625E,
	ObiCharacterControllerShapeTracker_UpdateIfNeeded_m37B806A39DB2EEF9B54A5B5F191B7F9C7602E517,
	ObiDistanceFieldShapeTracker__ctor_mA5B4583A9547EB0FE0C666EBB88E671DEC7F9035,
	ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_mA701C6AE3562FD84B9C2C56A60A80A88D8B961E7,
	ObiDistanceFieldShapeTracker_UpdateIfNeeded_mA4E6477716FF64D462625E6D8F4898A7E47BD384,
	ObiDistanceFieldShapeTracker_Destroy_mF2D78CB9BA87BFF2D01E82CA3796B3E975B387C3,
	ObiMeshShapeTracker__ctor_m08CD689CF8203745944B75624F6F10B9B5991B41,
	ObiMeshShapeTracker_UpdateMeshData_m3FE402BFC2EA1B1AEF97C10345A781D1F1453374,
	ObiMeshShapeTracker_UpdateIfNeeded_m12F1895305D96BA383B3C6D04D0960CDE65BB9DD,
	ObiMeshShapeTracker_Destroy_mF175565DFFD64344104928752D528653AEEF4C1D,
	ObiShapeTracker_Destroy_m0FF375956D9FB5363A7595568F62FC49288D102A,
	NULL,
	ObiShapeTracker__ctor_mD3C85F1D395CDC2FA8F0FADBF02F5035493C1A7C,
	ObiSphereShapeTracker__ctor_m1F91597098DFDE9D3D331BB46F9885EF84816FCC,
	ObiSphereShapeTracker_UpdateIfNeeded_m956A2557E0849CFD2C667CED772595BB3DB2517F,
	ObiTerrainShapeTracker__ctor_m7BEE7A899503A6A8D1B542BB48B2902966AB22B6,
	ObiTerrainShapeTracker_UpdateHeightData_mF979C90B8BF4BAA1D77BB55829060BE1D02DB1FB,
	ObiTerrainShapeTracker_UpdateIfNeeded_mF4287DBBCE367005E4F1EF1342AB50882CDBE460,
	ObiTerrainShapeTracker_Destroy_m778990BC8F8E80F2EAD34786597F7803DA9E55EE,
	ObiCollider_set_sourceCollider_mE74FCFF39705097FEAD7580CD74ADA13071B3CB1,
	ObiCollider_get_sourceCollider_m9128E7F1E524ACFDD350E7347570A6253DFDB19D,
	ObiCollider_set_distanceField_m1E7758FFA96690595BB90B1BEB48B7AD958EB53F,
	ObiCollider_get_distanceField_mC9798980E3932FBA4649A14AB780CA819D06B050,
	ObiCollider_CreateTracker_mD3862A5A96ACE781C43578C71C7F3B34597B72B5,
	ObiCollider_GetUnityCollider_m8F9125F3CE4C77B54A293FCB58A506F9003F1645,
	ObiCollider_FindSourceCollider_m637BA0D9C7416963F07A6C91C46898E63A2AA7E6,
	ObiCollider__ctor_mC40B217031806CE6CC171DB8519F2BDC13237937,
	ObiCollider2D_set_SourceCollider_m1018D5A5D84BEE9BFF223E7C96DFEF6391A61025,
	ObiCollider2D_get_SourceCollider_m0F0FA6FE50C824AA1D508B0624CFC78BC5CB1324,
	ObiCollider2D_CreateTracker_mF85B5C235CC7FD0AA628697696F24E75B0677D95,
	ObiCollider2D_GetUnityCollider_mC7022D7CE3DD5DAFC483FE44489C1826C6F719A9,
	ObiCollider2D_FindSourceCollider_m1C3542EAB959FFB8DD376825B89F1FCF6EF5B71A,
	ObiCollider2D__ctor_m1D8E050EE358DD5E91FBCBC06CEEA010D1096877,
	ObiColliderBase_set_CollisionMaterial_m79AC8EC4C2B24D56D2EEB8E7E3EC8758D71DBCEF,
	ObiColliderBase_get_CollisionMaterial_m0DF69ECB8E1D3B265A93C3411558938452320DB8,
	ObiColliderBase_set_Filter_m7EFACC9D0DDC53860DD8EC7711CA2B537861B143,
	ObiColliderBase_get_Filter_mC113E8E2C6A721BDC36A1380CDA7B1274877FA9F,
	ObiColliderBase_set_Thickness_m8B643679A1B8F3502E0983283BFAFC788F59FED7,
	ObiColliderBase_get_Thickness_m39EFFE2661BFC77996AFE413C11C9842813B0CCD,
	ObiColliderBase_get_Tracker_mC248A17400AE5158EBE13D85059023F2777EF96E,
	ObiColliderBase_get_Handle_mC54EFB3D8F0D65205AECE01C1523F9684610FDF9,
	ObiColliderBase_get_OniCollider_m1A453A14EEE1A71AD174EA6FD490CBF11FB13516,
	ObiColliderBase_get_Rigidbody_mFE29C0B5E72DE47B2F8CECC10D691E3CB1113260,
	NULL,
	NULL,
	NULL,
	ObiColliderBase_CreateRigidbody_m28447722765BD28A1F74FD197E44CD73585CFA55,
	ObiColliderBase_OnTransformParentChanged_mB24B89C5A1D09A5CDE05559DDDE3AE584CB45385,
	ObiColliderBase_AddCollider_mAE172D1DDA35D52A9D36D68E68CB739157F34870,
	ObiColliderBase_RemoveCollider_m95F3C0AD19C848FFF82DF5DB8584D5E949F808DC,
	ObiColliderBase_UpdateIfNeeded_mF9F82931F7D1F7703B77125BB3612E1781EF4622,
	ObiColliderBase_OnEnable_m9D06C6503D3AA87D2ED910B831C6927655449D5C,
	ObiColliderBase_OnDisable_mDCA28DAF071BB2F8AB35E5A290B1935BB47F320F,
	ObiColliderBase__ctor_m0425A5B3CFADDE3908C4CDE7564045D2E49156C6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiColliderHandle__ctor_m928271CA0B5407C025BBCC1A4C1B41A772842B98,
	ObiCollisionMaterialHandle__ctor_mF9EF7E13E41C1CFA35A66984FEAE0EA7A6E01622,
	ObiRigidbodyHandle__ctor_m7A187DF1938668726DB0353C6DF5A404B9661FDB,
	ObiColliderWorld_GetInstance_mDCD3CE9448DD243AF9314FC3AE6EFF25A0A9D804,
	ObiColliderWorld_Initialize_mA686CF0330B087A58794A38B02A5C474C0ACE80D,
	ObiColliderWorld_Destroy_mB2941E38388F04B017C98D209866BF6234D85C66,
	ObiColliderWorld_DestroyIfUnused_mF7FB902FE93CE094938D57DC0048249F810AFAB0,
	ObiColliderWorld_RegisterImplementation_m04E6D07B4FE9DB89CB602DDE1177100646AB5F80,
	ObiColliderWorld_UnregisterImplementation_m1FB0CC2F87786339836A3FE23662CE7E87AFD313,
	ObiColliderWorld_CreateCollider_m209298226F546EF09F80E9DC3C9CEDB9EA6A3ECF,
	ObiColliderWorld_CreateRigidbody_m1A6A020A8B5E9F2333767813122FAD64F13E58C7,
	ObiColliderWorld_CreateCollisionMaterial_m88809726D6F5777CA5CA979375202E079FBA523F,
	ObiColliderWorld_GetOrCreateTriangleMesh_m68BA906C938414D9FA39EF9434D16EDD5BC6B008,
	ObiColliderWorld_DestroyTriangleMesh_m0BAE4F6D11B77B8B13C2D4C1DE5A6140E76E582C,
	ObiColliderWorld_GetOrCreateEdgeMesh_mD3188273479E4DC678356643F456C0AA97BFBD1B,
	ObiColliderWorld_DestroyEdgeMesh_mC35DD757DCFE047FCE86C3190EEFA0B19A891306,
	ObiColliderWorld_GetOrCreateDistanceField_mD90630455B72C5BAD3E94DAE4F3FE4E979E2C431,
	ObiColliderWorld_DestroyDistanceField_mB645511AF520AEF6AB93B8EE03F0A2A7D62ECEE8,
	ObiColliderWorld_GetOrCreateHeightField_m67768E9620255D9407B6D664A21B5F46A06DA9DD,
	ObiColliderWorld_DestroyHeightField_mEDED727FD4CD60E71555E1F0B7223CF0C2F060D7,
	ObiColliderWorld_DestroyCollider_m834E6E9C211C47C4746C0658D072D11488444C7D,
	ObiColliderWorld_DestroyRigidbody_m06DB0A63D4471A043E38A1BFEC429E693E36E82B,
	ObiColliderWorld_DestroyCollisionMaterial_m95080D33292635B836E68FDF7DBD877FF51A3302,
	ObiColliderWorld_UpdateColliders_mFE5C2781107A2BF876523BC7D1A371930FA231DA,
	ObiColliderWorld_UpdateRigidbodies_m8561FC0652FF0142400D2886A02C9EFAEB685819,
	ObiColliderWorld_UpdateWorld_m864B8D4C74E8309F9FA29064D39B1A37FCC9538D,
	ObiColliderWorld_UpdateRigidbodyVelocities_m98240C979197C4339F335E642B3DF480F7A281E3,
	ObiColliderWorld__ctor_mB72724E3C4F8ABB4A7845EA540DF7FE796E1E971,
	ObiCollisionMaterial_get_handle_m594CB055313A49F8543490E78B1F48E7C21C7462,
	ObiCollisionMaterial_OnEnable_m8FA0F5FEF8BDA6E093C8A903CD09B45F107B87F3,
	ObiCollisionMaterial_OnDisable_m835688D4697C04CFE7B9F09ECBA5E0DD0201EE4E,
	ObiCollisionMaterial_OnValidate_m39E140FEFC698F6790902D5698BCCDAA4C037250,
	ObiCollisionMaterial_UpdateMaterial_m01C9A16AD952A5AC4E018F035CD98AD7C3F7D6F2,
	ObiCollisionMaterial_CreateMaterialIfNeeded_m7B6E786F93918CFCBCAFE79D73C6D39DB7D2E467,
	ObiCollisionMaterial__ctor_m72400798714AEFF19085712777080ABFBB13BAC6,
	ObiDistanceField_get_Initialized_mAABFFF3B58AB387E5CB5FB8C1AB746B0247F983C,
	ObiDistanceField_get_FieldBounds_m9863DC2138A3DBE77E04380C54236345A1133CBD,
	ObiDistanceField_get_EffectiveSampleSize_mB9F3DB29086DB4AD2249B28C1C8C888484833AAD,
	ObiDistanceField_set_InputMesh_mD2BFC4B7BAE4B8EBEFF52AC8E89CF319F03D3780,
	ObiDistanceField_get_InputMesh_m9B9ABB7CAE379468D3FACDBD886581F843FA22B5,
	ObiDistanceField_Reset_m00EC5397C61DF8D42A0E2201EBE65C30AB391DF8,
	ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266,
	ObiDistanceField_GetVolumeTexture_m3C63485A3F2BB3E0D84E496DFD190836DD7C9A64,
	ObiDistanceField__ctor_m79451FE12A75F2ED6B511F1DFC382E6493E09DB2,
	U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8,
	U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73,
	U3CGenerateU3Ed__16_MoveNext_mE09C7584F62ED7F884B6289D784C5474BAB6648D,
	U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC,
	ObiDistanceFieldHandle__ctor_m6E0E72314F9B3CD649F6E52743A19A05BDD53486,
	DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0,
	ObiDistanceFieldContainer__ctor_m3770E97A1E94FE7F79D6C4476A4B5538615321FC,
	ObiDistanceFieldContainer_GetOrCreateDistanceField_mACCA0F4E84E3D53991F18FEC18CCC8230451C913,
	ObiDistanceFieldContainer_DestroyDistanceField_m85914D4DE23293E844CC8082A6B7B4F427C8505B,
	ObiDistanceFieldContainer_Dispose_m7A92A1441B9CB196E1D25BF4587B01FDFF2CA2A8,
	Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088,
	Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16,
	ObiEdgeMeshHandle__ctor_m055ABDB8025F78423ED6926D1E316EB48FABC4AE,
	EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124,
	ObiEdgeMeshContainer__ctor_m7E7182023FCAB6FD0840A93D3F33CD76868DEC2D,
	ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m08E80D7BB9E4C1AD3040ECCB3E3F80BB0EBC5127,
	ObiEdgeMeshContainer_DestroyEdgeMesh_m6A49B7BD3DACF76A1B0E7F591A523795F55758EC,
	ObiEdgeMeshContainer_Dispose_m1005586E5D7BE477D43008737857278C3088223A,
	U3CU3Ec__cctor_m319B04707AB4F191DD80BB15D8CE099EE0A1A7B4,
	U3CU3Ec__ctor_m0A7F956BDB67E4A8C2BCD9D83C276D02DC561279,
	U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_mDEAC05CDAF4D92AFBB301FED8F1C6099004401DD,
	ObiHeightFieldHandle__ctor_m7487BDAEEC65A9BBFE0A38658CA884E309BC6FEA,
	HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421,
	ObiHeightFieldContainer__ctor_m5666C4D77395B12D8510DE3C1C91F7DF7080917E,
	ObiHeightFieldContainer_GetOrCreateHeightField_m24A1A0266B0B0558347D317ECAF37AE00CF5F73D,
	ObiHeightFieldContainer_DestroyHeightField_m0C864AEC3E3DF063366196C10432FC0E9582C124,
	ObiHeightFieldContainer_Dispose_m381A36CB411FE4B112A455EB52BEF459C07B542E,
	ObiRigidbody_OnEnable_m9C2FD7B18650A2752E66572F0470C3B0C4F31649,
	ObiRigidbody_UpdateKinematicVelocities_mCB621E407E54750602D501EF243278687D054900,
	ObiRigidbody_UpdateIfNeeded_mE8E08B343D9013FC5168AC526FDD2FD60713C63F,
	ObiRigidbody_UpdateVelocities_m8383481CAC453BA89BF272E3DA3AC19A575ED21F,
	ObiRigidbody__ctor_mD7D0C954158E4444C3068E83B3327DF0C19F22B4,
	ObiRigidbody2D_OnEnable_m250ABC526CD54A0CE3FD5924DBB5168CDFDE1233,
	ObiRigidbody2D_UpdateKinematicVelocities_m9FA8BC7541C8841BA05CEFB2E5153BEF8D0F0998,
	ObiRigidbody2D_UpdateIfNeeded_m2B7DDB040171C3260191C90FCA6E67EA1658C21F,
	ObiRigidbody2D_UpdateVelocities_m9E81C9B71FCBB4C7F67940267609771648B6FC02,
	ObiRigidbody2D__ctor_m4B1B8A6B4DC629D021CE278C6D8926801ABF2B5A,
	ObiRigidbodyBase_OnEnable_m0DB75700CD39CA195D17CE1222B274FCF94F0484,
	ObiRigidbodyBase_OnDisable_mA49D0E74F0FFE4A33FF74D3DF8ED1BC97F92BC9E,
	NULL,
	NULL,
	ObiRigidbodyBase__ctor_m97C098719F7244A8923BA99EB88178D5BC81E375,
	Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E,
	Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D,
	ObiTriangleMeshHandle__ctor_m4C8FBE34410F6DE0216D655610C66D865609FD82,
	TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4,
	ObiTriangleMeshContainer__ctor_m7744CBB6264FB551DA0E62E0AE2B14750075FAFC,
	ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m7966A3DEF57A37FDCD4EAB1254350FCF3C443C54,
	ObiTriangleMeshContainer_DestroyTriangleMesh_mC9C784E4C7DD7154C4F28D56E3D49B0CCFDFA4A8,
	ObiTriangleMeshContainer_Dispose_m5B8C0A01AFA58E44642169B3B0230566C3901B80,
	U3CU3Ec__cctor_m78E18BF63E0FC93B04931361A400A550DF8E42FE,
	U3CU3Ec__ctor_m353F343C4C6BE6C321A97142213FA89DC2CBACA8,
	U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m55F60D5CC64D104ED8E0B37F968D4479C3124E42,
	ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4,
	ASDF_Sample_mA68406A166BFA7E95D0C62E769CB30FC8F2C78C0,
	ASDF__ctor_mC6D0B839501A5338ADE298E18D27946D91973849,
	ASDF__cctor_m3BDC04F77E29E81E3A541CFA21851E2153E7EC1A,
	U3CU3Ec__cctor_m02BD313FFAEEDA3D68EE3E46312C75EB44777102,
	U3CU3Ec__ctor_m4CEC5676823706F8C33924A47217BB2E3C584BB8,
	U3CU3Ec_U3CBuildU3Eb__3_0_m2F53BFE53C74A23E70A52AA23A3625EA199232CA,
	U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3,
	U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4,
	U3CBuildU3Ed__3_MoveNext_m2878CF634F624CD9D2E068EB30A27BFF838B1AE9,
	U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B,
	DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609,
	DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3,
	DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B,
	DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192,
	Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113,
	Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7,
	Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4,
	Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B,
	Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08,
	Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8,
	Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664,
	AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914,
	AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB,
	BIH_Build_m543AD7EA6BD7D11FCF4DD88B06747F4B797784D9,
	BIH_HoarePartition_m26E2AC749940517A897AF2963BE5322BB38A3CF9,
	BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905,
	BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178,
	BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05,
	BIH__ctor_m6D1B7878A01D15CA2622A366286C9308BFB07727,
	BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F,
	BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A,
	NULL,
	CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B,
	ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB,
	ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E,
	CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530,
	ObiNativeAabbList__ctor_mCDE9CAA207D408AC843A08BBE600AB4F0D6B75D6,
	ObiNativeAabbList__ctor_m59E302FAD9AEFD8A3BE4E2C51B322FB0420E1E2F,
	ObiNativeAffineTransformList__ctor_mA3B6D7D19DD4E487461C969B9C76DDCAF9564018,
	ObiNativeAffineTransformList__ctor_mCB63BE2A5D0F613E12C647669D05EB6BE52FC83E,
	ObiNativeBIHNodeList__ctor_m9615EC2E166FBD0154489436203D9AC793BFB907,
	ObiNativeBIHNodeList__ctor_m6CB7A19C221489E05D62A7996A07E287439F3D73,
	ObiNativeBoneWeightList__ctor_m3412BBD683C143994278F1461106F9696B1D6835,
	ObiNativeBoneWeightList__ctor_mCDE322908FD4B65971533ABDFB277CDB0DF40B49,
	ObiNativeByteList__ctor_mE766091F60DF309D51D8C33F4C12AF80D4B9EB42,
	ObiNativeByteList__ctor_mEA31E3AD637138D0161DB670D7DF7ACDC613B402,
	ObiNativeCellSpanList__ctor_m7A0292E9784EED2B0011E83FA5A7D22EC9DC9EEC,
	ObiNativeCellSpanList__ctor_m9810884859E402D959A6369EEBD22320DC40D2D6,
	ObiNativeColliderShapeList__ctor_m9E21BF5F9EF261AE3C9C92E53F15264713584DC7,
	ObiNativeColliderShapeList__ctor_m79BC575A7059E83119DCE5DFC6D7CE8E308245C3,
	ObiNativeCollisionMaterialList__ctor_m1C70BE9BA86B92206DB617392F15D7B8BDEC9597,
	ObiNativeCollisionMaterialList__ctor_m90089B05CB810F2C23D47F0077910F26FCC61888,
	ObiNativeContactShapeList__ctor_mD865C20C0CFC9AE7557CD251D78C03101CCF2695,
	ObiNativeContactShapeList__ctor_mDB12EB79069ADCBD7B0F4377C8C5A1545F646DE8,
	ObiNativeDFNodeList__ctor_m9C5F3593C316319486AAF621BDD3A6C77A7AF4ED,
	ObiNativeDFNodeList__ctor_mCDC4549E10B99A08FD83058EFD46F45299C71FEC,
	ObiNativeDistanceFieldHeaderList__ctor_m12DE4CCCF75D20BB404B8083EF45503F964599EB,
	ObiNativeDistanceFieldHeaderList__ctor_mF1E073829DBEAD5BC9ECC60BB9AF6463019282EC,
	ObiNativeEdgeList__ctor_m376EED5071F0D70AB6595F5C98AC9FB16FCCCF46,
	ObiNativeEdgeList__ctor_m1DFD5ED07389DD12179C9D3494C4E2BF3D62D563,
	ObiNativeEdgeMeshHeaderList__ctor_m7A49700958AB248E4ABC1CCD1FA83731241FD1DA,
	ObiNativeEdgeMeshHeaderList__ctor_m372AE7E0664D6BE77453AB0C52023FC77C720E8C,
	ObiNativeFloatList__ctor_mB44D5F56545917B35AD1ECA717DF61D0C12047DC,
	ObiNativeFloatList__ctor_m109DA72958D0EBB1AEF825F9A43C34F2BD1A42E5,
	ObiNativeHeightFieldHeaderList__ctor_m0ED84840FACD5EF875EE422AD5274E42CE87D779,
	ObiNativeHeightFieldHeaderList__ctor_m5EC50E82D2C0FE9DBB46618885DE4D9A4FB490A1,
	ObiNativeInt4List__ctor_mAD172784A90BDA971380CFA6F7BECB381A268EA6,
	ObiNativeInt4List__ctor_mF3060DFCCBC09AF993C3C96608B51FA83537EDC8,
	ObiNativeInt4List__ctor_m24F99A45DDF792A152AF6A140C910F32C460E40A,
	ObiNativeIntList__ctor_mDF10DEDB2E6CE5F2D27135F263B2DB96A9123F0E,
	ObiNativeIntList__ctor_m25ADBFB84E370EE592FFF05D343541D84462FED6,
	ObiNativeIntPtrList__ctor_m2188368B2E04FE7C32F8D069405C9F483C27D465,
	ObiNativeIntPtrList__ctor_mE54F7AA88D3E5B4AD478C37640EDFAD145057FB8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiNativeMatrix4x4List__ctor_m8315DA2799111EE4624C6AD421E912B9E2C0D775,
	ObiNativeQuaternionList__ctor_m07E6B8255331075366483E69997B1E8FCC8812EE,
	ObiNativeQuaternionList__ctor_mF8798CFC21A168F77FDE7E4E433E4ED07BB42B0B,
	ObiNativeQuaternionList__ctor_m2733D11CD8396D239DBC16995D1E9404EA29AADA,
	ObiNativeQueryResultList__ctor_m5B404B4FBEB82A5A119AC92DFC671C161598266D,
	ObiNativeQueryShapeList__ctor_m1FC70B84E766982F194F6DC2FE87D15DA79493E6,
	ObiNativeRigidbodyList__ctor_mE10B19341E895E8DD38465DE2FC0E2EF5FEC1FA9,
	ObiNativeRigidbodyList__ctor_m21C628B6ADABDBDFA0D64D291A00D1BBC923B686,
	ObiNativeTriangleList__ctor_m5FDA4E153D9F3E8B6479666E321DDA194CAA2A25,
	ObiNativeTriangleList__ctor_m9FD16BE51A73FCC0ECD34FE1E3E60DE410644782,
	ObiNativeTriangleMeshHeaderList__ctor_mDAD0784830EED0C0268F2009CEC90E04EB39B72C,
	ObiNativeTriangleMeshHeaderList__ctor_m4F5742A78ACC6F92F44E484750F2710E45D32A8F,
	ObiNativeVector2List__ctor_mC9341AEC2A2B1610138A7763453BF7CDC2512299,
	ObiNativeVector2List__ctor_m3326D8FF9523937A0D58CCEB0AA53A7F715AB740,
	ObiNativeVector3List__ctor_mC37EA7399E0090469F5014DE5EF277117A8B40E4,
	ObiNativeVector3List__ctor_m454CD1792A828386A51E51AC80EF30B4EC59DB25,
	ObiNativeVector4List__ctor_mC18F1ECE7AE0C5F9E3D080D41011CCE123F29C55,
	ObiNativeVector4List__ctor_m4E5196643444C3F0983F4C1D35EC054085549834,
	ObiNativeVector4List_GetVector3_m8EAE7F64879DD9A415D25E05C258ADEBC921A55D,
	ObiNativeVector4List_SetVector3_mEA4A6008125CDE1854000FBF64CEA0887E8A7D94,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA,
	ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9,
	ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1,
	QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453,
	SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86,
	SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066,
	SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95,
	VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB,
	VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C,
	MeshVoxelizer_get_Origin_m98D23A3FAE59358DCED36A586C0AB6B4F3E77B4D,
	MeshVoxelizer_get_voxelCount_m1590512CB3A933F6FD26076643E8FBBF72CD4565,
	MeshVoxelizer__ctor_m2E9885D259222CEE4CB38497584FE8F3EBB0E2B4,
	MeshVoxelizer_get_Item_mC5F92567FD7C76447DF970069641190AE24C1356,
	MeshVoxelizer_set_Item_m6E59067E37D364C147DEC0335408F67C39286DBD,
	MeshVoxelizer_GetDistanceToNeighbor_mAC3238234874D704EEDE7B39EA4E2C557043FA8F,
	MeshVoxelizer_GetVoxelIndex_m03964EDAA758946CBAFB630ECD6AC4DAF9B03921,
	MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089,
	MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06,
	MeshVoxelizer_GetTrianglesOverlappingVoxel_mBEA8964FBC06F1AC96902CAB7A18AD815C4FBAEE,
	MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1,
	MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B,
	MeshVoxelizer_VoxelExists_m6075ED6BD1CD96FBC57C1935A45231C09ABCFA70,
	MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7,
	MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A,
	MeshVoxelizer_BoundaryThinning_m4E5359902ABE3CD1C2541C9FCEADE519E69482D5,
	MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51,
	MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC,
	MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10,
	MeshVoxelizer__cctor_m221669B76FE23A351F3F850B1B8B6BA2FD840D15,
	U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0,
	U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD,
	U3CVoxelizeU3Ed__29_MoveNext_m6AC2C7AA56B9E60005E531B6AFBC225E89351F62,
	U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036,
	U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C,
	U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7,
	U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97,
	U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2,
	U3CFloodFillU3Ed__31_MoveNext_m35FC429F6648DE25CB9A4F53A3547E84ECAEEB8B,
	U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189,
	U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3,
	U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	VoxelDistanceField__ctor_mF001EDBFAD1E407A198F85AE578DC94C131DC940,
	VoxelDistanceField_SampleUnfiltered_m1DCFD20D36F447D804A7DBE566E82FD4E6A134AA,
	VoxelDistanceField_SampleFiltered_mF03122890E95B4323F7318B9C27A8C65DA3F463E,
	VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6,
	VoxelDistanceField_JumpFloodPass_m0CAE942EA7B7FB237160C57ABAC04D2F77198AC3,
	U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654,
	U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6,
	U3CJumpFloodU3Ed__5_MoveNext_mCF3E269446E8A96E7F313896B714A111CBBD3FA3,
	U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0,
	U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22,
	U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18,
	VoxelPathFinder__ctor_mEB3020C477C2C93B2F00C9CFC38CFE47068AE443,
	VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC,
	VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714,
	VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5,
	VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC,
	TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F,
	TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF,
	TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681,
	TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8,
	U3CU3Ec__cctor_m2E9966CB04100D59E489C39A64FAC67F8FD97311,
	U3CU3Ec__ctor_m7A494B57D1990F9D4FD4EF63281425ABEC97A888,
	U3CU3Ec_U3CFindClosestNonEmptyVoxelU3Eb__6_1_mF780311AA35327276D353A5C472EBABD63FB6D10,
	U3CU3Ec__DisplayClass7_0__ctor_mCCDBC132E254A9EA49862CDDDB93E94F182AF58C,
	U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__0_m0CBCD40678980CB309FD296C8DCA140B2368C158,
	U3CU3Ec__DisplayClass7_0_U3CFindPathU3Eb__1_m93C19DF998EC9A85989D9980D3C89F8EB7724361,
	ObiDistanceFieldRenderer_Awake_m02202AAABFC3B738E4965E9B2B897A228E6AC210,
	ObiDistanceFieldRenderer_OnEnable_m5A75F9F5EE526E06FF0D410741740FCB6B6478E1,
	ObiDistanceFieldRenderer_OnDisable_m7C13494A2B29D5E6827B2757573C7370FC80A394,
	ObiDistanceFieldRenderer_Cleanup_mFE94B31A3E0EED155DC9650B112587501A88D532,
	ObiDistanceFieldRenderer_ResizeTexture_mE50107B1ECBAB2706311F61EB8EDFAA5A47FB02C,
	ObiDistanceFieldRenderer_CreatePlaneMesh_m7DABB1814E6572C53BCBCC12B382369FA534AAA4,
	ObiDistanceFieldRenderer_RefreshCutawayTexture_mFE87BC7C9D4D36CAE8BA3A2BB5CCFE9195422F93,
	ObiDistanceFieldRenderer_DrawCutawayPlane_m881EB8355EA4D1D21C65297319180645AD0BBB3A,
	ObiDistanceFieldRenderer_OnDrawGizmos_m216C9ED791D6583CFB5AC31CCF96FC999FEB065B,
	ObiDistanceFieldRenderer__ctor_m5BDF67924CA8661E32DCAE76825AECE42C4E08AA,
	ObiInstancedParticleRenderer_OnEnable_mE355745DF976D2E5FB662C641FFF1207CFF849A8,
	ObiInstancedParticleRenderer_OnDisable_mB6727244E6FCBAA7A72470799C34118F47F54C26,
	ObiInstancedParticleRenderer_DrawParticles_m006744E372DC552B4BBE90BDA10C2936E58A7CEB,
	ObiInstancedParticleRenderer__ctor_m36160262281CB03A2EF5F4F65B1599650E20D198,
	ObiInstancedParticleRenderer__cctor_m8F094F8AD40C59C077031DB8223AF95B8123B2B7,
	ObiParticleRenderer_get_ParticleMeshes_m19B21A66F62AF9565B5610A7E406A9F4D9AB2624,
	ObiParticleRenderer_get_impostors_mE9035EE0C41C3AFB9849C8CF46A35BAA97E6F594,
	ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530,
	ObiParticleRenderer_set_ParticleMaterial_m271A889A40C59DDA14EF2DCA96DD1B93E40CC3F9,
	ObiParticleRenderer_OnEnable_m5936C65AFFDE90197548673B64DCDB559B634084,
	ObiParticleRenderer_OnDisable_mAA99859EE95AC59E3CCEBDAEE3C9F3D1EBE9F2A3,
	ObiParticleRenderer_CreateMaterialIfNeeded_m3F402725EA811E7191F495C02BC1D42C9D3508E2,
	ObiParticleRenderer_DrawParticles_m968118939B2050BAB32AF92EFCB004552F31FCAE,
	ObiParticleRenderer_DrawParticles_m10909ABED462F26F971463C7A569BC19C8115FD8,
	ObiParticleRenderer__ctor_mFC28AF85E8C3264440883850C411477777E11A29,
	ObiParticleRenderer__cctor_m25FE6E62F8C43957CDFA579FE81399B4270C8DDC,
	ParticleImpostorRendering_get_Meshes_m755745FEE84971387953757DE9D15D9D32204C2F,
	ParticleImpostorRendering_Apply_mC4B8B70A312A115D0D3264BA0F4D6CB8920AD9D5,
	ParticleImpostorRendering_ClearMeshes_m425FD6121111FFDD2834713DDE69988571B30BD8,
	ParticleImpostorRendering_UpdateMeshes_m352C0F940E2857058D6A6B447A03AB70258E8A60,
	ParticleImpostorRendering__ctor_m83C3D5BED9E2463E4E916753E01D78C90E7C9BBE,
	ParticleImpostorRendering__cctor_m096389C9C5C4FB998DFF6B43A943B8F29D300B72,
	ShadowmapExposer_Awake_mA4E68783C4BCFD7B7FCCF2FB2047921F739DA519,
	ShadowmapExposer_OnEnable_mA68155C92E4F83954076D9000038D58A265100F5,
	ShadowmapExposer_OnDisable_m7CC85B1D1729AA03CDAC1816F9A84E3350254554,
	ShadowmapExposer_Cleanup_m32D90F4E8CAC5C2EB13BFAD141A056F8D3B143E5,
	ShadowmapExposer_SetupFluidShadowsCommandBuffer_mE799C5800875D6B7CA60B3775768C9F07B2288B4,
	ShadowmapExposer_Update_mBFA72090D0E7D36367BCA31B001E2965D1B264E2,
	ShadowmapExposer__ctor_m10E8C4FAA46FCB5CC04085336279E049BC51FAFA,
	ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5,
	ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674,
	ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52,
	ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7,
	ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B,
	ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1,
	ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69,
	ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6,
	ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E,
	ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250,
	ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72,
	ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D,
	ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E,
	ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33,
	ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D,
	ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3,
	ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D,
	ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626,
	ObiSolver_get_implementation_m9F3A6A2A2D6921B6A59A82F935039FC29F24E422,
	ObiSolver_get_initialized_m245E2320CEE2189A248BB0DE795CD9FE80839C78,
	ObiSolver_get_simulationBackend_m67AEE52B9C07C78C1231FE5074F7125E47A8109A,
	ObiSolver_set_backendType_mE714E4CD3D72BE093E2ECBB2B418D7095A3B11B4,
	ObiSolver_get_backendType_m49EDD493C5D45A0D0728C26F95F1E955FEFB4104,
	ObiSolver_get_simplexCounts_m78AF46D65457FB56118D4ECD884FEAD71D3E28C4,
	ObiSolver_get_Bounds_m4C8914413BBBC0968BA6ADCF3E7149AA5F7FC7D3,
	ObiSolver_get_IsVisible_mA12ECA2DFE503F721181D2A8A5F5F9065BABAF0B,
	ObiSolver_get_maxScale_m2118E5D74E45C017D6D91E33790768FC10FE9B18,
	ObiSolver_get_allocParticleCount_m98C18E7A785C31F32283F6A52AB1372313E3E16F,
	ObiSolver_get_contactCount_mAE6F8DD8070340148DCC11C0F729F1B38C4AF654,
	ObiSolver_get_particleContactCount_mFDF3ADAB6B54D7FECF6F1943E4AE93C0AE10F18F,
	ObiSolver_get_particleToActor_mFCEA216986C39AD5264352D73B10017FE5E7F3D3,
	ObiSolver_get_rigidbodyLinearDeltas_mEB6D6F68DEAD8616EA48FA36AEA9B37520CA9367,
	ObiSolver_get_rigidbodyAngularDeltas_m05353966D848B59BC9586B0C19CAE476834502C9,
	ObiSolver_get_colors_m3263CBAEB79EE1F583593969A2EAD32549CB875D,
	ObiSolver_get_cellCoords_m6BB3F63111C716E45AAC8FC0BE4AB0CD17331B28,
	ObiSolver_get_positions_mB7EA5209FDB8289380DBA07A2F28E640D485F557,
	ObiSolver_get_restPositions_m1BCA73DCC44BA1B9181838370F0E546B4D66CFC4,
	ObiSolver_get_prevPositions_mA99888F076C4FB374B14DEE1178C625A3F8F9B00,
	ObiSolver_get_startPositions_m3B9144D015BDB7F0F676F065AAC9A22F0DA9CC27,
	ObiSolver_get_renderablePositions_m56EE654A3E7ECEF99EE3F1481E759B01DAB697BE,
	ObiSolver_get_orientations_mAF1A79C344CC44789900361936635BBD508D2973,
	ObiSolver_get_restOrientations_m5A079EC8140D809FB02223ECCA6986917B333AE4,
	ObiSolver_get_prevOrientations_mBB52AEB5D484B0A4BAE23213D65585BE498B5F68,
	ObiSolver_get_startOrientations_mED3EA83E62A458AE8F95A89CEC15ECA81FDF28D1,
	ObiSolver_get_renderableOrientations_mB71AEBAF05CD2CA3FED8936554686F5A0414C06A,
	ObiSolver_get_velocities_m73051C86B492795133F138E42FF08698222FCCD6,
	ObiSolver_get_angularVelocities_mE565A9561A09435E294755AA2F7E31993BD932DB,
	ObiSolver_get_invMasses_m403082935BEB22CE4C8A086C31B748FC46B8C319,
	ObiSolver_get_invRotationalMasses_m35049317216303EC2D97F143D98DBB60C9EADF58,
	ObiSolver_get_invInertiaTensors_m66E7A977E525B6628FA0D14015A52DE6CC26AADA,
	ObiSolver_get_externalForces_m6DE0779EAA775DB7E7A853B844E07464240BED59,
	ObiSolver_get_externalTorques_m7A057FD836A21C5CA1DAEA2258E05F81D15F8991,
	ObiSolver_get_wind_m72179C4670DBCE8A1838066EA134F02A16DC371C,
	ObiSolver_get_positionDeltas_mA020DD1DBE7DDC042F8C730EBE9A71D6AE213AF9,
	ObiSolver_get_orientationDeltas_m747EE331713678E42AE2A16F9201AF96E8CFAF21,
	ObiSolver_get_positionConstraintCounts_mFBC0F55E2F573CC7A38F71703D8617DF5C9BF895,
	ObiSolver_get_orientationConstraintCounts_m6752E0D0CAA0272BD7FF4242E5A426F4310FAECA,
	ObiSolver_get_collisionMaterials_mE98DA539A9F5EC9BE8B35D9AA8C6F4C4FDA211A1,
	ObiSolver_get_phases_m1DFABF0F6107CA7A301BBC35A0896C69FA1D7422,
	ObiSolver_get_filters_m35DFCECD0FEF501CB8903CF986F67CD3487DFEBB,
	ObiSolver_get_anisotropies_mDD48FB3AC63921409AAF55FB2FED3833FC458009,
	ObiSolver_get_principalRadii_m6C3307DA982056B2AF918D77B539E22580E5F179,
	ObiSolver_get_normals_m241E9FFCE3A531BFF2679A3420EBCE7B313FF52C,
	ObiSolver_get_vorticities_mDC8CC9D454FBD7AABF51AEDD83B63D62D74D2E62,
	ObiSolver_get_fluidData_m70D3FEC3C44F6EA5D68AB6B39BD72A08B41EF349,
	ObiSolver_get_userData_m63E5DF119EE501A9380C6A176079C9FD1361B561,
	ObiSolver_get_smoothingRadii_mEE720BA5CB3140CCB508FC97466DDC06F18A9380,
	ObiSolver_get_buoyancies_m14142F7AA18DAE0CD2BA25EA5F3F7E5796938365,
	ObiSolver_get_restDensities_mA85A3F3E9C265A9E3B01B957691808F072123C96,
	ObiSolver_get_viscosities_mBCDE34198266ABADF85472134F8A69F897BB37D6,
	ObiSolver_get_surfaceTension_m06BBC45CF2C65E927490672F37C89AD69ACE039A,
	ObiSolver_get_vortConfinement_mBCA2B8E737E63A3DADDC34F1E7E7C4D26F7A71D0,
	ObiSolver_get_atmosphericDrag_m81584993AAF86345E489C4D833D1BAF3E704CB16,
	ObiSolver_get_atmosphericPressure_m363045FB67ED30D2413C84317FBF2033FF9BE1B6,
	ObiSolver_get_diffusion_m614EC192C40E1DB26E20E2B7AEEB1E3853E1CBAA,
	ObiSolver_Update_m3432EE148E4943BFEA7F248CC1ADF410EB009532,
	ObiSolver_OnDestroy_m10E800119F5492F06C454965798031E2959B4D91,
	ObiSolver_CreateBackend_m91B701B5D03356B869ECCC5E5B4E0F9E82CA7377,
	ObiSolver_Initialize_m69393E4E890B2AF174E313FAB09AF20C8858713B,
	ObiSolver_Teardown_m7AFA06EFC9E0BA723690DF91837C3E3B8D6C30C1,
	ObiSolver_UpdateBackend_mED822610A9CB49AC6197C9EB12A70EBF7F5959A5,
	ObiSolver_FreeRigidbodyArrays_m75E7122190DD0CFFE6594F39BED2EE9685488FA2,
	ObiSolver_EnsureRigidbodyArraysCapacity_mC0A5656D8985DE06BC6084750EC6AE266B7E059A,
	ObiSolver_FreeParticleArrays_mC803A0F5E3291D91B643E13F40F56A1DD906DB19,
	ObiSolver_EnsureParticleArraysCapacity_mA3FF2925ACDE72EC10DF9853E51FF3440F27EB67,
	ObiSolver_AllocateParticles_m2437F6049C3AB8F791378C25BB220F3D447E9209,
	ObiSolver_FreeParticles_m4CD1BFC3A67A853618E2BC48B666735D57D69CBB,
	ObiSolver_AddActor_m33CCB3774385993AD4756D44613032EDF7602076,
	ObiSolver_RemoveActor_m9816A116E0848361565F850D70B3B8A876ACC5A2,
	ObiSolver_PushSolverParameters_mC566D7A04AABE0FD982060FDDCDF94BF0330A898,
	ObiSolver_GetConstraintParameters_m612C27EA24521064F789D15FABB06E8320EC50DD,
	ObiSolver_GetConstraintsByType_mE11A373E6629FB18A577363D79AE2626806FB095,
	ObiSolver_PushActiveParticles_m699BFA15ECA1837EB8E6FB0C4C06564BC00A4356,
	ObiSolver_PushSimplices_mCF761C06FD3C0023D8AC8CAF847F22856CA29F2B,
	ObiSolver_PushConstraints_m1FE372176ADB5BDDB3C11F042850CA126D62EA7D,
	ObiSolver_UpdateVisibility_m78B2DB3B14730D673C823699F0AF2CEB06524552,
	ObiSolver_InitializeTransformFrame_m37F3F63EE2DD9DDE17329B3FDF750A4C9AFF317C,
	ObiSolver_UpdateTransformFrame_m95D29D02A287E86629B2BF566891B519A5D3B2F4,
	ObiSolver_PrepareFrame_mC4616463C08C47D37E9B5A9610850BD0853BCF59,
	ObiSolver_BeginStep_m08735BDB104DA6C7B894DDCBCE685C8F08EAE8C5,
	ObiSolver_Substep_m8EF42B8FCB4A92B66A18967A5E30C0FAF9D91226,
	ObiSolver_EndStep_m12D95AB19D02AB5C1E2ED1D8F3B1E586831DBA64,
	ObiSolver_Interpolate_mAE7A83C120C8BCD50676E781D540026B6BAEEDE2,
	ObiSolver_ReleaseJobHandles_m7624C20714357B7110E0A1268120AEDD7CF46A77,
	ObiSolver_SpatialQuery_m6D0D03F289F49B89217E8A449DAABC0CCB90D83B,
	ObiSolver_SpatialQuery_m5C0D958E05B3BE7A66EFA6E320C0CB6E3A196607,
	ObiSolver_Raycast_m8F28F2F638DC71BC4716816A590D3D86E0274409,
	ObiSolver_Raycast_mBABB28101E4819C4AC8C9D3189DB8636FD255007,
	ObiSolver__ctor_mA963ED6A2F8A02E33B2E3BACB178FC2F0436F753,
	ObiSolver__cctor_m4023459B57CD95EEF9656B8BDA9B18BDD558C46B,
	ObiCollisionEventArgs__ctor_m346881EB85AB69845A91659208E75756E4EF6D17,
	ParticleInActor__ctor_mAA089A9B7EBB63E8671D8AAB97FD0836220D7EEA,
	ParticleInActor__ctor_m3829A4666F38432208F2811BDE98CD7A8C6B25F3,
	SolverCallback__ctor_mE4083F1B124E6A1FFB21E69EE2AEE7318B28AAD7,
	SolverCallback_Invoke_m45F6B3AC4BD61E22AAA076A7DBA5544E5C72F057,
	SolverCallback_BeginInvoke_mD89642A3E1F37C4DC118EA6CACD1B6D68CD83A08,
	SolverCallback_EndInvoke_m395BC54EFE0998B85005E9E4F66858F146C931F2,
	SolverStepCallback__ctor_m2079BE6990F358A8A5837AE21480E58DD4D0EEA3,
	SolverStepCallback_Invoke_m68DD32CB29FBD1EC423CC5A72702D2CB05715754,
	SolverStepCallback_BeginInvoke_m434058E191E84C9A47950451E2B46A1F8F957F40,
	SolverStepCallback_EndInvoke_mF3E0E0DE67ABA86472824000ACA507AB3352A533,
	CollisionCallback__ctor_mA5A4EE9E910214B425B42DD40A2671EB949D661E,
	CollisionCallback_Invoke_m941AD708CC00FD8EA47569A5B061BB862B96F98C,
	CollisionCallback_BeginInvoke_m0A88695225D3C02615D754DC211FD3EE3468BFAB,
	CollisionCallback_EndInvoke_m46F561EE559B214E9D38C29ABCADAB3336B18A8A,
	U3CU3Ec__cctor_m89EAB0A60F1D2D1667AB8DC5FFD689526BA4D3DE,
	U3CU3Ec__ctor_m2D8F2915236AB77A7C36A87C7761B5503DCA10F2,
	U3CU3Ec_U3Cget_allocParticleCountU3Eb__148_0_mDB5D6EA59542D260F8AA00864066AC567AF3613B,
	ObiFixedUpdater_OnValidate_m5F6FE3B9B8E80E3B125B39F142D2DA57A4DD8BFB,
	ObiFixedUpdater_OnEnable_m48B49909B75BD8080EE823F221FA21F5C98E8829,
	ObiFixedUpdater_OnDisable_mEAE5E8BA5B2A650527D99517C37FBF98173259E9,
	ObiFixedUpdater_FixedUpdate_m9CB26DEA62FD0EA6E7FBF86A2CFB12C375B81CE0,
	ObiFixedUpdater_Update_m485E215A24D5B8A2C3B191F2F65615D4C2A27A90,
	ObiFixedUpdater__ctor_m129BDE7A45025352A7238A317F1A3E7B31A00C9F,
	ObiLateFixedUpdater_OnValidate_m4B24969542BF8BAA45ABF2DD0FC90CD910FA27AC,
	ObiLateFixedUpdater_OnEnable_m7AA55EFB2018C0FE7A7450DE8B124E9947735430,
	ObiLateFixedUpdater_OnDisable_m508D776D30650C73FB72E47AEB5F2F7EFB64DFB9,
	ObiLateFixedUpdater_FixedUpdate_m347A98DB1FFCE6D2654B99E6E228F26BC0646F21,
	ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB,
	ObiLateFixedUpdater_LateFixedUpdate_mC459F999C033D7BC22CD0A0BDA9569E93C2C6F2A,
	ObiLateFixedUpdater_Update_mE16274CC3FCABACAA0430D79903C2298050907B9,
	ObiLateFixedUpdater__ctor_mA0140EBDF2452F0A415601A43DAE9822CCA249C6,
	U3CRunLateFixedUpdateU3Ed__6__ctor_m3269F746C194951D4FFCB5CFE81A956F7C6913F4,
	U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_mFEF635C7933D3DD06A06668B01022B79D7348076,
	U3CRunLateFixedUpdateU3Ed__6_MoveNext_m5E828028CAFAD2A27BAD42F54BD73085E6C7F802,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BC60167DF9644F2049A2591D4130F2190A2A4C9,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_m55403139467BF8AF39A859E7823363583E55162B,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_m8E98B873A2E9B9BE3DEE0CA669677BA2ECDFE0FF,
	ObiLateUpdater_OnValidate_m6360BD327E35B0C56A590D51CB066B811DAD1774,
	ObiLateUpdater_Update_mC52FDF37EE579A4D667D80C5327FD48FF06A7400,
	ObiLateUpdater_LateUpdate_m04DE9233394AE2CC13CF1DA4F136364E81FE4C06,
	ObiLateUpdater__ctor_mD001E04F8FFF222B20A8ECDBE7A8C2507C2F0894,
	ObiUpdater_PrepareFrame_m38DA2E247B4C02F4E18CA1E5890493598A019D65,
	ObiUpdater_BeginStep_m871A82CAEA561457F1FB19B27ADC346661839AA5,
	ObiUpdater_Substep_m44F3765E77796D6D71287FF2883FF2E496E0767F,
	ObiUpdater_EndStep_mD6CEC404A973E002CD4D81FC87442C77F537E55E,
	ObiUpdater_Interpolate_m38784F191B281CBF7A3DE79B71EA71ED2FE60681,
	ObiUpdater__ctor_m3A352531659D7BCCB0CF2AFF0732D3B28AB26092,
	ObiUpdater__cctor_m272383BDBA28D7F89B9965940A9188BDE81EA1A9,
	ChildrenOnly__ctor_m4D3FAAA742E61B7FA2323CE9F448A1877F87CAE0,
	Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7,
	InspectorButtonAttribute_get_ButtonWidth_mE4C1BF731F1203ABCDC7FF911FBC9AA619ECC4FB,
	InspectorButtonAttribute_set_ButtonWidth_mD7A12A99D234B3911E6D035C0224BC6AB4CC773A,
	InspectorButtonAttribute__ctor_m975A49E0C15C48813192407A712BDB21DCC86987,
	InspectorButtonAttribute__cctor_mD9D19B7D28E89349026C4FBCAF129D59060E0349,
	MinMaxAttribute__ctor_mAEC8FD68CEFC58D324E8BBF941E6940654EF2F78,
	MultiDelayed__ctor_mC38D0EAB98C18CE8CCEBCC3767678783367BC0F5,
	MultiPropertyAttribute__ctor_m0CEA425DBF3D690C2FBF18251EA3D45B8A2EEEFD,
	MultiRange__ctor_mB97A29FE677F7549CF13B4C38DEAA868640876C1,
	SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21,
	SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55,
	SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3,
	VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79,
	VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD,
	VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C,
	VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E,
	VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6,
	CoroutineJob_get_Result_mD748A43832803B6F5A24988519355E0B5E960BD3,
	CoroutineJob_get_IsDone_m4278531E6DBB4F3ED38BEB44A7D3365F6008420B,
	CoroutineJob_get_RaisedException_mB7118E9ABE0C6CC7C00D741FC8BC5E859CC81F9A,
	CoroutineJob_Init_m08092162CA93D4B06D6F23B48138F018CA204416,
	CoroutineJob_RunSynchronously_mC0C37C33DF39624CE6811D4D2229BF1DBB004DAB,
	CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F,
	CoroutineJob_Stop_m503EBD4DCD2DB059CA0681D562A66E37442C7E4B,
	CoroutineJob__ctor_m3B1FC813C2F585D8B745A74C9F40059C41C72437,
	ProgressInfo__ctor_m61203218594B2063E536F24871C934C1CA813DA1,
	U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD,
	U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1,
	U3CStartU3Ed__15_MoveNext_mF349A26AF1025AF7487E1BB1D725340CD887C18F,
	U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55,
	U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3,
	U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283,
	EditorCoroutine_ShowCoroutineProgressBar_mBD8BA0BAB4A8F6DBA32D5EB38A2AC2598992E522,
	EditorCoroutine__ctor_mDA14C1513A94EABA131D822C5BC96C78411A872E,
	ObiAmbientForceZone_ApplyForcesToActor_mAE4A4FC41002FA3D906FEA74194C377D2C60A8D8,
	ObiAmbientForceZone_OnDrawGizmosSelected_m9F4A990AC63D31EFB4E459DDDE79DA6D99ED98DE,
	ObiAmbientForceZone__ctor_mD887D3FBC82BCD8DF7720A9B25BF72572118351E,
	ObiExternalForce_OnEnable_m0FB8D535D2ED4F394E48424E48C073B8E3FBD10F,
	ObiExternalForce_OnDisable_mD1867136686D828057C7DF70B6A77B5DE81E459C,
	ObiExternalForce_Solver_OnStepBegin_mC46587312AE49CD7183DAD9D16BBB66C9BF64E0E,
	ObiExternalForce_GetTurbulence_m36B324BC86245A69B5FFB5D924E08E35BCEC135D,
	NULL,
	ObiExternalForce__ctor_mD434F0876366D23C0D68DAB6A8949E018A42ACBD,
	ObiSphericalForceZone_ApplyForcesToActor_m506A142D9EF2B82CD7310E150EF302F8A6B2FA2B,
	ObiSphericalForceZone_OnDrawGizmosSelected_m428130DB0C2E2D23A31E3D0FFE00EDA0FB6744B2,
	ObiSphericalForceZone__ctor_mCB4320E2FDC045FAC6D25069AA629243434775F0,
	ObiContactEventDispatcher_CompareByRef_mCC27E2711928972C0B16573985711C3432D1719E,
	ObiContactEventDispatcher_Awake_m4C9DC422D39BD21B3BE61C3BE4EB262BD0FECAD9,
	ObiContactEventDispatcher_OnEnable_m9E49028DB875725C353943480CB5C447179953DC,
	ObiContactEventDispatcher_OnDisable_m8F75848B0553FD8776A22ABD1253D891B592CFA0,
	ObiContactEventDispatcher_FilterOutDistantContacts_m2088093F9BFC093F73ECB24BDC9E6DA7A748962B,
	ObiContactEventDispatcher_RemoveDuplicates_m394D908408DBDED555F844001682FD217E94C5F9,
	ObiContactEventDispatcher_InvokeCallbacks_mB588492B596248DBC043C8235D2AD8D3AC3AC423,
	ObiContactEventDispatcher_Solver_OnCollision_m12E8F9442701D7911DE4F0D18CB0F4EC82F263FC,
	ObiContactEventDispatcher__ctor_m357D739552531DA027D8F320CCFAC444814BD791,
	ContactComparer__ctor_m5DC2082E03C9DA3062C2D13D3F1702B3763C4379,
	ContactComparer_Compare_mB97171F79E54851FEF29ACC2ED3F06C5A1452362,
	ContactCallback__ctor_m77F3DD3B53374589F094279143B1638E57919AD3,
	ObiParticleAttachment_get_actor_m8A6C711886370813BD7BC301BFB6C7812B6B193A,
	ObiParticleAttachment_get_target_m87F09D825C3B834F3F32869BA35539B4525FA08E,
	ObiParticleAttachment_set_target_m05F5DB1086FA979873CCECE322EDE77C26550DF7,
	ObiParticleAttachment_get_particleGroup_mE66342EC326E748311096EBF6508598B2D0E9CC1,
	ObiParticleAttachment_set_particleGroup_mD2ACDCEDFFDECA0B1737C92F4E2941CD93BEFDE7,
	ObiParticleAttachment_get_isBound_mD1067521C804A334B7A28CA3A9BD65237C0FD29D,
	ObiParticleAttachment_get_attachmentType_m3AE549A02BCF14D72CE6EDCC93541FD03EA3B271,
	ObiParticleAttachment_set_attachmentType_m5631ABD4AF1A1AFB91F5851BF869AD3974C2586F,
	ObiParticleAttachment_get_constrainOrientation_mD07EF64C3AE8F1F9CE99F747CF0D12C2DE7D6E1D,
	ObiParticleAttachment_set_constrainOrientation_m17A07949BE7D8BBF51FCE020DFB2B07DA73A722F,
	ObiParticleAttachment_get_compliance_mF04CA0E1C82403AC5039D8F3816CB750E16E22D9,
	ObiParticleAttachment_set_compliance_m559FB99156912E50F36AE6239AEEF8DCE995A132,
	ObiParticleAttachment_get_breakThreshold_mA89F93A18D03F1DADE699A97634716D15617B02F,
	ObiParticleAttachment_set_breakThreshold_mF3F55D89A7E9E6EEF600D372A34DB537A752EB06,
	ObiParticleAttachment_OnEnable_mE67B8CA340675DC78907D05D206A0B32B47C8F6E,
	ObiParticleAttachment_OnDisable_m970C2F665946F9526288866F57E00845E57F6231,
	ObiParticleAttachment_OnValidate_m8F4069C273A48281199B02A40AECFF1D83EA0C32,
	ObiParticleAttachment_Actor_OnBlueprintLoaded_m5736115ED7F6C5E1935D2217E7E5DBFDB5B20487,
	ObiParticleAttachment_Actor_OnPrepareStep_m9EA64FC23D3365CA10F37123637E56D333D6A2A8,
	ObiParticleAttachment_Actor_OnEndStep_m09B95B61E9325F33E9DF9B85945979C11962E0C1,
	ObiParticleAttachment_Bind_m41671D060988813057E5D6CB710621B387C3B4C7,
	ObiParticleAttachment_EnableAttachment_mB6BAA968F14899C9E41D3827506F5A05E45247CF,
	ObiParticleAttachment_DisableAttachment_m91236D7F5CFA0439C6EB8D250816CB587AD6392A,
	ObiParticleAttachment_UpdateAttachment_m5537ED01556A4DC62E3F46AB5D43A9E2C875856A,
	ObiParticleAttachment_BreakDynamicAttachment_m3AA55F0956AA144B3AC657224DF303EBACBB9022,
	ObiParticleAttachment__ctor_mAC181D9F03BE52C0DB29C07F539A74C0DEA1744B,
	ObiParticleDragger_OnEnable_m72B8C23953592A731785106A8459284845816A55,
	ObiParticleDragger_OnDisable_mB11CAEAA8554518A0B474976561C8FEF0D6E8E49,
	ObiParticleDragger_FixedUpdate_m9851F3C063BF90687B18E10701B7B1F5193A8E6E,
	ObiParticleDragger_Picker_OnParticleDragged_mECCECAF2424C1D2656A6601ED602BFEA343EBB7C,
	ObiParticleDragger_Picker_OnParticleReleased_mD5AFBB9569AFDD6F33D4E148A5A63B542331C87C,
	ObiParticleDragger__ctor_mC7F5FD7D2B50D344D2D85CDA6CB55EA76DFD7A73,
	ObiParticleGridDebugger_OnEnable_mDCE0CF67BDC98D81F9FB3B4AA3DA73485F24A8E3,
	ObiParticleGridDebugger_OnDisable_m6EEAA88C141739F08763B496873E4FB8583ABA2D,
	ObiParticleGridDebugger_LateUpdate_m7A114715F2660DE5C8004F414580ABAB4B6BD796,
	ObiParticleGridDebugger_OnDrawGizmos_mFF5DD489989676AD3C6BACA195FE47F4E18C22AC,
	ObiParticleGridDebugger__ctor_mC3547171B3F6C6C08F8E759186D7007E186A29B8,
	ObiParticlePicker_Awake_m74E07553D81756450A0D1B8351E1CBF8142F8F11,
	ObiParticlePicker_LateUpdate_m9E75050EBAC6B52C81851303DEE0B2EA70353CF1,
	ObiParticlePicker__ctor_m5818ABF9055AD22D294ED1EB13021BC9FE8AB3A9,
	ParticlePickEventArgs__ctor_m64293DD03DECF493CE8EBD7844C21D2746C09EF0,
	ParticlePickUnityEvent__ctor_m8E79C656B1CDE3FB655B899990D215C91879BFA0,
	ObiProfiler_Awake_mAB7F0E70026EC600D00A9130AEA40B9F2DD4D0D7,
	ObiProfiler_OnDestroy_m952E74CFF016FB1DB495D76228352ABED2E56166,
	ObiProfiler_OnEnable_m2FA73A238837F2034B8E2907F2E87C347A07EFDC,
	ObiProfiler_OnDisable_m677BD3207054B10CDE9BAFA6EDC6D7C52DAD7B09,
	ObiProfiler_EnableProfiler_mE07D4D3052ADC88FA8D0D882DF933E094D9F25FD,
	ObiProfiler_DisableProfiler_m6E225ACE957C1D38336D2C4FFEEBAC52540241E9,
	ObiProfiler_BeginSample_mCA17FA232FD590BF488C6304FC5C4B4B77105DEC,
	ObiProfiler_EndSample_mA76E83B9E7BB9DCEA7626780607D999D34868425,
	ObiProfiler_UpdateProfilerInfo_m89B2E0A641CDEA86AB5195C8CB8CDE7ED666B9DF,
	ObiProfiler_OnGUI_m975F6EE95578505D3E6F1792AD1AFBBAD1375139,
	ObiProfiler__ctor_m3EEB8D1C03DFE08C0EC0E23E334C190F41E40424,
	ObiStitcher_set_Actor1_mABB05BE8A26AE51871FE07C7EAA9D53359016679,
	ObiStitcher_get_Actor1_mF7B8097B74CDE0D5491AA8CA0BFB9EA6F6521DE7,
	ObiStitcher_set_Actor2_m8D4021102EDF55C787F2AFB5CBD83AEA4443C95E,
	ObiStitcher_get_Actor2_mF6FCD44A6E57935EF04782FE6E80FF30A500356E,
	ObiStitcher_get_StitchCount_m0A3C3380AA1B0536814453807186D85002F57744,
	ObiStitcher_get_Stitches_mD04C880005B8B5F6261CFF2E5ABB4C713C27CDFA,
	ObiStitcher_RegisterActor_m6CF833604BDC8434EA68BD461456A891CCE1A6B6,
	ObiStitcher_UnregisterActor_m1C06E9E8003BEAEF75F59ED25B8B56561E5D4A4C,
	ObiStitcher_OnEnable_mDEE87492FEFF02F28EC6D8CB231D8DA3893A8BBC,
	ObiStitcher_OnDisable_m44319848A4EA3CB9AF55AE503389703F57250C00,
	ObiStitcher_AddStitch_m9CDD6C005F4A71C8E942AFCF0EE588AC47CBA1D7,
	ObiStitcher_RemoveStitch_m050637591BDE1882809E3902CD2E7C0FF62EB0CC,
	ObiStitcher_Clear_mC2BBA753DCB0899C7C486C20565D05F6ED1F1B2F,
	ObiStitcher_Actor_OnBlueprintUnloaded_m6CE4420D85CBD27371B1BCD548FC50F983C7DDC6,
	ObiStitcher_Actor_OnBlueprintLoaded_mF598296F75D17D0F818E4D7F7A1E98F7F3B8200A,
	ObiStitcher_AddToSolver_mACC6329F09C6AC34703CC2C3CDCD6531D7ADCA63,
	ObiStitcher_RemoveFromSolver_m5B3C5E8739E3393D3654A34D0D84972D7582C457,
	ObiStitcher_PushDataToSolver_mA3B052D96FECDB53E4616B27E72A44F77A88EB31,
	ObiStitcher__ctor_m8703E582D33DF3E6FB4265F91A5061420D984F97,
	Stitch__ctor_mA9EBE5BCBF80CAFB90725EF8F4B9E8EA693A9800,
	ObiUtils_DrawArrowGizmo_mB0F16371F7CA701F1934AB1D68E2C543BBEDC6D7,
	ObiUtils_DebugDrawCross_m56B5756A7B53EF07A54DA1521D785F4B447A0794,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6,
	ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88,
	ObiUtils_CountTrailingZeroes_mD04646F64AE816B0BBE5FE3044C2DED6E90A0333,
	ObiUtils_Add_m33CB21C40D66A37C46501B8D0F17D03411F7A6B8,
	ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F,
	ObiUtils_Mod_mD2EF1D694D52B0B1882C801A2FD576D8FC49D90E,
	ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4,
	ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4,
	ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51,
	ObiUtils_ProjectPointLine_m39534F2B61F19A53C004F9F72C2D5561C3BFCA15,
	ObiUtils_LinePlaneIntersection_mA331CE0E861BFF21738B92A85B6009EA4641AD91,
	ObiUtils_RaySphereIntersection_m878269850BD66F1202937DEFF22C3A31ED3B5996,
	ObiUtils_InvMassToMass_m1ECAAAEDD4E27ADA1FDE1594DACCD249742ED136,
	ObiUtils_MassToInvMass_m7E1782BA36AD544C22BE9B80A3D222F6D4000B68,
	ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5,
	ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED,
	ObiUtils_TriangleArea_m560C3409898B4D67699FCFD0338C3ECDB244BCEB,
	ObiUtils_EllipsoidVolume_m406A1FFC6FADF93B25B2B072064B345E5DA5D2C5,
	ObiUtils_RestDarboux_mA5FBE38C5DA10FBD3F920E2F0E96683D3EF91507,
	ObiUtils_RestBendingConstraint_mA2A2E588C9B5B6907DC2670CF3176A0441D95C78,
	ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453,
	ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197,
	ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1,
	ObiUtils_BarycentricInterpolation_mAC20CA1234A3FB1D1D9AB2AA29D7987C0DF12361,
	ObiUtils_BarycentricExtrapolationScale_m84AD9B6B2173A5538FDB174D72AA5CE127045D33,
	ObiUtils_CalculateAngleWeightedNormals_m97CD00C800AA1BD2705A48113DD44B57E450DD34,
	ObiUtils_MakePhase_mED5844CB11913FC70EC9041F512C75E0B2F4E167,
	ObiUtils_GetGroupFromPhase_m95E80301165EB08A322A5043278D698903867054,
	ObiUtils_GetFlagsFromPhase_m6712C3AFEE4371CD66C9A5548C96FEF64A990A57,
	ObiUtils_MakeFilter_m8370C56CCFA7758D6E03671DCC2E8F0546BACEF3,
	ObiUtils_GetCategoryFromFilter_mE5495C835432A19EE7A32B4E72D104A5179AD9ED,
	ObiUtils_GetMaskFromFilter_mD6E7EF064AF96C8238D05A7EB5504E72CA1ECFCD,
	ObiUtils_EigenSolve_m52D85DC48A8905EEFA96A9E0618B6569757370F7,
	ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529,
	ObiUtils_EigenVector_m650F505BE39681115F5A78F3AB4C76D6B766E360,
	ObiUtils_EigenValues_m4B78523278F5C36F028CC1E7AB8FA13BE10FB476,
	ObiUtils_GetPointCloudCentroid_m8C1020D867B29A5A516F0DF741330765D4B47BB5,
	ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2,
	ObiUtils__cctor_mE9D4CD7AA4BC90F980D0AC4F4DA0B543DFD75D01,
	U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10,
	U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C,
	U3CBilateralInterleavedU3Ed__40_MoveNext_m77C2C019B232853A6CC9102657015E05AC815D61,
	U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126,
	U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE,
	U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474,
	U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279,
	U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6,
	ObiVectorMath_Cross_m23CD1AEE1CC6B74D7930C98AA1D280513238A553,
	ObiVectorMath_Cross_mA47A8583DC381295371BD66F8397DD209C2B7731,
	ObiVectorMath_Cross_m4C1FC1059C8BF006437BAA9D3CBBCD7323016E19,
	ObiVectorMath_Subtract_mD703C48EC9EAAAF9901BCB1F1D0BF46E23A8925A,
	ObiVectorMath_BarycentricInterpolation_m7CD9E0E0E11533DEB84C7003A1FBC5E415B6CAD4,
	SetCategory_Awake_mC91A0F14152A3306C3D58FFDB3268FC42CA75A1B,
	SetCategory_OnDestroy_m7E095279FCAA7AC75B26DBC31335E8C1B6156A5F,
	SetCategory_OnValidate_m96641FEA78031AF96A7904C7B89F5526989E129A,
	SetCategory_OnLoad_m105F18C77751C59F67759FE5006D36A29B4480AF,
	SetCategory__ctor_m5F2C7ADC7EAEF4C3E7B89A74858C43C67F3F9AD5,
};
extern void SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk (void);
extern void ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk (void);
extern void SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895_AdjustorThunk (void);
extern void SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80_AdjustorThunk (void);
extern void SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0_AdjustorThunk (void);
extern void SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE_AdjustorThunk (void);
extern void SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E_AdjustorThunk (void);
extern void BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F_AdjustorThunk (void);
extern void CachedTri_Cache_mAE403285E691BBA51817154E6CF25A99BF9D24F1_AdjustorThunk (void);
extern void BurstBox_Evaluate_m9A7975B75E4F6E739D90F73F2CB2209B369FDBFA_AdjustorThunk (void);
extern void BurstBox_Contacts_m9D3DED1FA7E7454E9EB7A52A7ED94B8A08C99CE3_AdjustorThunk (void);
extern void BurstBox_Obi_IBurstCollider_Contacts_m97856F2BFA4EBA5278F111B41C7F5CA5A8C58573_AdjustorThunk (void);
extern void BurstCapsule_Evaluate_mB2A54FDA05AAE32DF69B20A013BDCF441F379D46_AdjustorThunk (void);
extern void BurstCapsule_Contacts_mE461E9B2E609E1A411ED668A5C6380B29FCCE4D6_AdjustorThunk (void);
extern void BurstCapsule_Obi_IBurstCollider_Contacts_mADF596860548ABE29238ADEA0F82324B6D1A6A28_AdjustorThunk (void);
extern void IdentifyMovingColliders_Execute_m6536871CD5E23F8233E90C455F8B29C3839BA490_AdjustorThunk (void);
extern void UpdateMovingColliders_Execute_m05B0B145094896C8C5D0A421ECFAB255D1C7D4D6_AdjustorThunk (void);
extern void GenerateContactsJob_Execute_m1A30B956B328522C42AAB9316CFE714A83873DE5_AdjustorThunk (void);
extern void GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E_AdjustorThunk (void);
extern void BurstDFNode_SampleWithGradient_m383A3E8C19EBD56E7660F1D957022D53CC0D5173_AdjustorThunk (void);
extern void BurstDFNode_GetNormalizedPos_mD01E0D79BAC83035CC86DC3706B9B7AA98B3CF9D_AdjustorThunk (void);
extern void BurstDFNode_GetOctant_m76BF4D7E298B1E7238848063468AFE583C403408_AdjustorThunk (void);
extern void BurstDistanceField_Evaluate_mC93945AD7F3206A1EC39A627C982644CF24673B9_AdjustorThunk (void);
extern void BurstDistanceField_Contacts_mC98735809C55B8C503AC05F45BEC80420352057E_AdjustorThunk (void);
extern void BurstDistanceField_Obi_IBurstCollider_Contacts_m212E14857D066BE00574BE85A69F636A0B5A14BF_AdjustorThunk (void);
extern void BurstEdgeMesh_Evaluate_mB1DE344E5F95F0D35C81FCEAAFD439CCA1A0281B_AdjustorThunk (void);
extern void BurstEdgeMesh_Contacts_mDEAD5DA052578CD2794227155F0CFDEFB76DD9BF_AdjustorThunk (void);
extern void BurstEdgeMesh_BIHTraverse_m8E806DD0BD9B370CC1862099121BC5B3F83B9CA9_AdjustorThunk (void);
extern void BurstEdgeMesh_Obi_IBurstCollider_Contacts_mE8D91763243332F3BBE8D636493C232F3085C1C1_AdjustorThunk (void);
extern void BurstHeightField_Evaluate_m37E0774FD60539E9B60C6265C5289EABA40FEE16_AdjustorThunk (void);
extern void BurstHeightField_Contacts_m8D519E8FEE86E9D62A2F24AFE65643D75C82E321_AdjustorThunk (void);
extern void BurstHeightField_Obi_IBurstCollider_Contacts_mAF9CC53C9637FA7BEFAACAB317DEECC4325EFEA8_AdjustorThunk (void);
extern void BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA_AdjustorThunk (void);
extern void BurstSimplex_Evaluate_m96C3BF51F11E624BC054D9BA276F177A9DBBBE73_AdjustorThunk (void);
extern void BurstSphere_Evaluate_mD03D68FDA6CCD8B6481B8CF75FB5C1528BA034B8_AdjustorThunk (void);
extern void BurstSphere_Contacts_mDA37B8F9954ECD2EFD7BF8F9A51A43CA73A869AE_AdjustorThunk (void);
extern void BurstSphere_Obi_IBurstCollider_Contacts_m16229652DCB867F59A4F4360F064FDA00691D32B_AdjustorThunk (void);
extern void BurstTriangleMesh_Evaluate_mB03B52D7D8BA26D3C383CB9334A57EDD03B03DE5_AdjustorThunk (void);
extern void BurstTriangleMesh_Contacts_m03A033B4760F0729757BF20D322FC8D112802A00_AdjustorThunk (void);
extern void BurstTriangleMesh_BIHTraverse_m53B6E0B2D8D09B465000034E751D2BB96E194907_AdjustorThunk (void);
extern void BurstTriangleMesh_Obi_IBurstCollider_Contacts_m0414B63AB8F72DB613D9240E6B5AA847FB4B296D_AdjustorThunk (void);
extern void AerodynamicConstraintsBatchJob_Execute_mA12D60F5737F1B49A118E40DEBB6D5BE5D9FDCE1_AdjustorThunk (void);
extern void BendConstraintsBatchJob_Execute_mF6E997FD25554FC9F4071D28B24E8804DEF39870_AdjustorThunk (void);
extern void ApplyBendConstraintsBatchJob_Execute_m99F086827B78551FE09B2FBC6881B2894BCB7682_AdjustorThunk (void);
extern void BendTwistConstraintsBatchJob_Execute_m72C33183ADAB152169BB181429D90663B0DB648A_AdjustorThunk (void);
extern void ApplyBendTwistConstraintsBatchJob_Execute_m74A218C4F257CB77BB42C70892690A866BC97861_AdjustorThunk (void);
extern void ChainConstraintsBatchJob_Execute_m576FF3805857AEBF06581865F975D5D982550907_AdjustorThunk (void);
extern void ApplyChainConstraintsBatchJob_Execute_mA038CCA6A34C6A73AECD256274108E245F5FE91A_AdjustorThunk (void);
extern void ApplyCollisionConstraintsBatchJob_Execute_m875BE8298CCA64DB1EC6FA478647C40913DD330D_AdjustorThunk (void);
extern void UpdateContactsJob_Execute_mBDE78DEE6DE6D94DC3B6684199C0292D4C915E0D_AdjustorThunk (void);
extern void CollisionConstraintsBatchJob_Execute_m7E251F652F57C72F5A552957040597FA6DCD6688_AdjustorThunk (void);
extern void CollisionConstraintsBatchJob_CombineCollisionMaterials_mF1338B4C889E6A783168F52825A68777DBAE0C99_AdjustorThunk (void);
extern void FrictionConstraintsBatchJob_Execute_mA6E69C218FC0969B77330BD6B4D5C15A6F49E056_AdjustorThunk (void);
extern void FrictionConstraintsBatchJob_CombineCollisionMaterials_mE4F955E2ED7F5DD7834726D2014A7555573FDB3C_AdjustorThunk (void);
extern void ClearFluidDataJob_Execute_mD48A32AF77242D2E6CDFF920317494A459561F18_AdjustorThunk (void);
extern void UpdateInteractionsJob_Execute_m1EEE83F71148CE066BB48348ECCA642F2A1E80CE_AdjustorThunk (void);
extern void CalculateLambdasJob_Execute_m2B425DF0EEAD9932568604F718B2921296DA4A79_AdjustorThunk (void);
extern void ApplyVorticityConfinementAndAtmosphere_Execute_mB2A90B74BE8FF621E0A2E25863B37740AF5D95D9_AdjustorThunk (void);
extern void IdentityAnisotropyJob_Execute_m8BCDAC71C9652E2C66490C406047751573B95B40_AdjustorThunk (void);
extern void AverageSmoothPositionsJob_Execute_m8530DC393B3E014A32A67CF6BE5657D78714B493_AdjustorThunk (void);
extern void AverageAnisotropyJob_Execute_m8E8F860CE0895F6EEE07E14E2C09507F15CAF28C_AdjustorThunk (void);
extern void UpdateDensitiesJob_Execute_m59352095C34B63F24D23B524548542B60EF8B6F7_AdjustorThunk (void);
extern void ApplyDensityConstraintsJob_Execute_m9CE8FB7550A4AFC0C47CD7569ABB38E69B978F16_AdjustorThunk (void);
extern void NormalsViscosityAndVorticityJob_Execute_mBABF5C9E0E64304EEA30D40729CEAB8C02E78C86_AdjustorThunk (void);
extern void CalculateVorticityEta_Execute_mBD1C201BA56247D5A29070646ECD1C54D8FA3B31_AdjustorThunk (void);
extern void AccumulateSmoothPositionsJob_Execute_m198C016973ED826593021549CB78790D8D97590C_AdjustorThunk (void);
extern void AccumulateAnisotropyJob_Execute_mDC2644AC6278164D69AF0BF1AB94E439BA2B1396_AdjustorThunk (void);
extern void Poly6Kernel__ctor_m458F1673FE9C7B44144243984BF8D3351EF76641_AdjustorThunk (void);
extern void Poly6Kernel_W_mA9FD1FFD1D90D74DB6803EC97F570DAB31EAE849_AdjustorThunk (void);
extern void SpikyKernel__ctor_mA630C18D4CA7AEE79027ADBEB1BFCBF26A7452BE_AdjustorThunk (void);
extern void SpikyKernel_W_mA7FD6FEFAF094558177A73FB38603EFC70E8D697_AdjustorThunk (void);
extern void DistanceConstraintsBatchJob_Execute_m8974C56ADBED13F088ADEC56AF17CBF723258A16_AdjustorThunk (void);
extern void ApplyDistanceConstraintsBatchJob_Execute_m3D79DE4E8EDE7541DC1B61399471933671AFAFDF_AdjustorThunk (void);
extern void ApplyBatchedCollisionConstraintsBatchJob_Execute_m6B349C47BB948DA01570848A8C29F1D3BEF4C3D5_AdjustorThunk (void);
extern void UpdateParticleContactsJob_Execute_mA60AE07BFCE3A0F563D19A316C153C7F9887ED7E_AdjustorThunk (void);
extern void ParticleCollisionConstraintsBatchJob_Execute_m8485003051D23F0D75146E26663B7CA0AF2386F1_AdjustorThunk (void);
extern void ParticleCollisionConstraintsBatchJob_CombineCollisionMaterials_mEC293E92648E63BD95F591444FDFF4B18F33D703_AdjustorThunk (void);
extern void ParticleFrictionConstraintsBatchJob_Execute_m79FE4542796B3C2B9146A83500647A9D0680830C_AdjustorThunk (void);
extern void ParticleFrictionConstraintsBatchJob_CombineCollisionMaterials_mF0CD7C3874B720CC924931A83AB1EE44FBF0797D_AdjustorThunk (void);
extern void PinConstraintsBatchJob_Execute_mEF5E6798264EF1F83CC82A6A98B54DECCD4D1E36_AdjustorThunk (void);
extern void ApplyPinConstraintsBatchJob_Execute_mB647701B0C09CE1D750AF92DA09F9F8E78FD52DB_AdjustorThunk (void);
extern void ShapeMatchingCalculateRestJob_Execute_mF557395226A5479543E3F443C1787EE2F5D4AB24_AdjustorThunk (void);
extern void ShapeMatchingConstraintsBatchJob_Execute_mE253B06332A40DCA4C6453A876DB428FA345B047_AdjustorThunk (void);
extern void ApplyShapeMatchingConstraintsBatchJob_Execute_m1ED0E064C42CCD032FDE804F098C1250B8DDE687_AdjustorThunk (void);
extern void SkinConstraintsBatchJob_Execute_mE9464013B468FA413A33380F7080A6E8984B03A5_AdjustorThunk (void);
extern void ApplySkinConstraintsBatchJob_Execute_m65A2A62E22D2996164E8F0E34B60EAB4C923CDCD_AdjustorThunk (void);
extern void StitchConstraintsBatchJob_Execute_mA5D5BEA6FB1DA22EEAB3C0CD7CEB52145DBE1BCD_AdjustorThunk (void);
extern void ApplyStitchConstraintsBatchJob_Execute_m50E228A24C8D90CE705CA819CBCB9F3D1FF7E7B6_AdjustorThunk (void);
extern void StretchShearConstraintsBatchJob_Execute_m38FE03FB561A839F2E3C895AC52308E3374C50CC_AdjustorThunk (void);
extern void ApplyStretchShearConstraintsBatchJob_Execute_mB1D83D142361E62A2D687C29EE346B26AD985A70_AdjustorThunk (void);
extern void TetherConstraintsBatchJob_Execute_mEF1FE91E81FCED26D0A12BF953AFB35780ED52C8_AdjustorThunk (void);
extern void ApplyTetherConstraintsBatchJob_Execute_mBF97BD7B8E8E46587578F03ADCD029486BD104CD_AdjustorThunk (void);
extern void VolumeConstraintsBatchJob_Execute_mB6BC30FDA2A00B126A2939FE5841FABE48971AB6_AdjustorThunk (void);
extern void ApplyVolumeConstraintsBatchJob_Execute_m8A7F092F18089C81D544CD0E08BCA2EE41AB7C25_AdjustorThunk (void);
extern void BurstAabb_get_size_m5E23B4420CB21B196C931459BECA271D6567E28C_AdjustorThunk (void);
extern void BurstAabb_get_center_mEF7385D666BE5EE77B04D4C1B507CC277498DF83_AdjustorThunk (void);
extern void BurstAabb__ctor_m5EA38EB6DA406EE85C1CDCC3B4053EFDA9AF999C_AdjustorThunk (void);
extern void BurstAabb__ctor_m25051D2B2D8D634E1497B4FBA7DA5855CA630293_AdjustorThunk (void);
extern void BurstAabb__ctor_mBFC57B41EDB0B6AD54F1CEED8ABDFCE2CF73FD6D_AdjustorThunk (void);
extern void BurstAabb__ctor_m20D651928C007C6CDCF1FA28E061578888324215_AdjustorThunk (void);
extern void BurstAabb_AverageAxisLength_mC3D10DA8BEC0F0BA5B418EDB620BDCD4A2951C8E_AdjustorThunk (void);
extern void BurstAabb_MaxAxisLength_m00B824BC8D68124AD6362187459834CD10F2C9AD_AdjustorThunk (void);
extern void BurstAabb_EncapsulateParticle_m2BF518A13C65F7BC9B5EB0175BA00D3A07FA3350_AdjustorThunk (void);
extern void BurstAabb_EncapsulateParticle_m9154ADC7C45964966B05F06F5B92412630067FA7_AdjustorThunk (void);
extern void BurstAabb_EncapsulateBounds_m9AFA483BA1B7BEBF77C1B85D57549FB36375E1D7_AdjustorThunk (void);
extern void BurstAabb_Expand_mD6FD7B154B8700A8F46316D8ACFFD928F92DAD49_AdjustorThunk (void);
extern void BurstAabb_Sweep_mCB51F0C277EA7EF3C7F033CF125C1A0B04E7541E_AdjustorThunk (void);
extern void BurstAabb_Transform_m972887B0B52D4B08B4806F6BB8196E17B64C0B90_AdjustorThunk (void);
extern void BurstAabb_Transform_m20348C56E245E750A6399FA6F7C2D767EE11B91C_AdjustorThunk (void);
extern void BurstAabb_Transformed_m09422ADB914E6917056FB53FEDDC172E7AEE88C1_AdjustorThunk (void);
extern void BurstAabb_Transformed_mE44D84A994DBAAD9EED74F026F8220ADCD9A249E_AdjustorThunk (void);
extern void BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6_AdjustorThunk (void);
extern void BurstAabb_IntersectsRay_m94E7181E1827E8BE7F727FBCFB48EB11CC40CFAD_AdjustorThunk (void);
extern void BurstAffineTransform__ctor_m05A35A300C65D6551377AD2C948ACA1FC2F8667C_AdjustorThunk (void);
extern void BurstAffineTransform_Inverse_mDD81DD6911040D1CA2FE9F0D1F315F7812DF27B5_AdjustorThunk (void);
extern void BurstAffineTransform_Interpolate_mECFFF4666C1393B072387A61B32573B56BCFE676_AdjustorThunk (void);
extern void BurstAffineTransform_TransformPoint_m391756FAA0719FE03FA3ABFD002CF41953DFE610_AdjustorThunk (void);
extern void BurstAffineTransform_InverseTransformPoint_m62FF98D4C1F4B903143CBEBB8BE560DD8973E955_AdjustorThunk (void);
extern void BurstAffineTransform_TransformPointUnscaled_m88D0781E4CA7ACAB74CC46049AF03EEE34AF17D8_AdjustorThunk (void);
extern void BurstAffineTransform_InverseTransformPointUnscaled_mB57EB7ACC6E6630D7AB86C3B3C4B330CDA781675_AdjustorThunk (void);
extern void BurstAffineTransform_TransformDirection_m6194FE90F0734E4FAC3D9EC2F18B1418E054B284_AdjustorThunk (void);
extern void BurstAffineTransform_InverseTransformDirection_mD556AD3447707CC7AF2C21B42B1009B169B07E13_AdjustorThunk (void);
extern void BurstAffineTransform_TransformVector_m93FE5B596EE6E92173AEF3E44B265907B0371AF0_AdjustorThunk (void);
extern void BurstAffineTransform_InverseTransformVector_m5F477834E781A85ADC934DE24BC0FEB89A6BDDF9_AdjustorThunk (void);
extern void BurstCellSpan__ctor_m06DA83A6F5C75A3D3B7B8E2F97C86D245EB3DD9D_AdjustorThunk (void);
extern void BurstCellSpan__ctor_m285AB617964F7D42BF536E07ED69BEA63327EE37_AdjustorThunk (void);
extern void BurstCellSpan_get_level_mEA8299C260E4E7D5A362075EAFDBAEC20CDCF21B_AdjustorThunk (void);
extern void BurstCellSpan_Equals_mE730FAAEE94E797BCE3F326B494C301956A7014D_AdjustorThunk (void);
extern void BurstCellSpan_Equals_mACAF86A3EDB8CCB7033E573669BBE26088CCF696_AdjustorThunk (void);
extern void BurstCellSpan_GetHashCode_mACB4CF48DD8B4D70211F28DACDEE8DC6A7F421EA_AdjustorThunk (void);
extern void BurstInertialFrame__ctor_mA98B43A3120665CC585634E855B81B740024C066_AdjustorThunk (void);
extern void BurstInertialFrame__ctor_m23E7F3F42FD0210FF6C53F7D04954946BC11A943_AdjustorThunk (void);
extern void BurstInertialFrame_Update_mFDDBF9FACE375ABB5982E879EDC404B54CFF6E34_AdjustorThunk (void);
extern void BatchLUT__ctor_m93F109809773B154AF095575B0C00C35E224346C_AdjustorThunk (void);
extern void BatchLUT_Dispose_mCFABB1B7385B31C2C7DD8AD9123205A3732D13BE_AdjustorThunk (void);
extern void BatchData__ctor_mD6B3E7F16DF9B5364DDCC3E92B5CDF971E4A69BD_AdjustorThunk (void);
extern void BatchData_GetConstraintRange_m685408EAF447F1B6A4AA9C17803E061A58752449_AdjustorThunk (void);
extern void ConstraintBatcher__ctor_mEEC00AF4C13C2B7D3E750958E9F78A639DCFBC76_AdjustorThunk (void);
extern void ConstraintBatcher_Dispose_m832285B8BE78DFA00F4E2253150C447F9E546889_AdjustorThunk (void);
extern void WorkItem_Add_m8A0EF0EB1E6104C1CD4E12708761C75BCE6281DD_AdjustorThunk (void);
extern void ContactProvider_GetConstraintCount_mACC69AA75EE8103A1AAED3B3924CB26DF9798FEF_AdjustorThunk (void);
extern void ContactProvider_GetParticleCount_m3C3C405609067B4AF80D1D4269CDB486BA02D9FC_AdjustorThunk (void);
extern void ContactProvider_GetParticle_m0B5FB5F90C47141EF186E6ECE7169AD3F0B5E54E_AdjustorThunk (void);
extern void ContactProvider_WriteSortedConstraint_m5568D339D5163D4BAF3BF92562D993B463670F5E_AdjustorThunk (void);
extern void FluidInteractionProvider_GetConstraintCount_m010E43F859A7E41424DDDD5B8C03B75C3B05F3FA_AdjustorThunk (void);
extern void FluidInteractionProvider_GetParticleCount_m7E6514985CBFF6300D92399124419655353DB050_AdjustorThunk (void);
extern void FluidInteractionProvider_GetParticle_mBB30B78F2DEA50E3E3BDEABAE3B2CB39DBA52DCF_AdjustorThunk (void);
extern void FluidInteractionProvider_WriteSortedConstraint_m2CAE0F1AC443F672E9503FC238EEC9A73C84DFE3_AdjustorThunk (void);
extern void FluidInteraction_GetParticleCount_m986AE991B21B5250E354ADCE40955BB274A954C0_AdjustorThunk (void);
extern void FluidInteraction_GetParticle_mE71B2BF68C60B06FFB723114CCB4874EFBAD83E9_AdjustorThunk (void);
extern void CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955_AdjustorThunk (void);
extern void UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47_AdjustorThunk (void);
extern void GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2_AdjustorThunk (void);
extern void GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB_AdjustorThunk (void);
extern void GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194_AdjustorThunk (void);
extern void GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418_AdjustorThunk (void);
extern void GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937_AdjustorThunk (void);
extern void GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7_AdjustorThunk (void);
extern void InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A_AdjustorThunk (void);
extern void BurstContact_GetParticleCount_m07601B081D4B2D33AA274A224230DDF7FA41DA12_AdjustorThunk (void);
extern void BurstContact_GetParticle_mA6D14AB9D6B0C1C0E2D489D9AEAC4F1DACF44976_AdjustorThunk (void);
extern void BurstContact_ToString_m082A762EDA6C99C0D046A08E1456DC09B503654B_AdjustorThunk (void);
extern void BurstContact_CompareTo_m4AAE9C314CA58F9ECD6DAC6E1FE8C4046C00A550_AdjustorThunk (void);
extern void BurstContact_get_TotalNormalInvMass_m8DB8BB9F92C654E952F6EFD3E30695C049FADAE4_AdjustorThunk (void);
extern void BurstContact_get_TotalTangentInvMass_m23A1474CCF4F8B7EFD804072C2B8C1081570C36A_AdjustorThunk (void);
extern void BurstContact_get_TotalBitangentInvMass_m6584B11DFF4F6F649C7F2048CA928C7321528FEB_AdjustorThunk (void);
extern void BurstContact_CalculateBasis_m7756F0C85D337D0FCC1301EF91A9DDA3AC1F11FF_AdjustorThunk (void);
extern void BurstContact_CalculateContactMassesA_m6442A92150D7BACA30A83170C952918D5941B9B7_AdjustorThunk (void);
extern void BurstContact_CalculateContactMassesB_m525CB01618D4402EFE433CDD47AC61735B04A24D_AdjustorThunk (void);
extern void BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524_AdjustorThunk (void);
extern void BurstContact_SolveAdhesion_m1CFCDDFF32CBB02EA4050AA3184E9AB1BF33B39B_AdjustorThunk (void);
extern void BurstContact_SolvePenetration_mA388AC5B79F15B50ED2678A37DFA62CF8D7D12D3_AdjustorThunk (void);
extern void BurstContact_SolveFriction_m50255C39E593D5D9ABAAE76C19E618577F5E24F4_AdjustorThunk (void);
extern void BurstContact_SolveRollingFriction_mE67B076235FF4D42A16E7624EDF3B4ED4E32463A_AdjustorThunk (void);
extern void BurstBoxQuery_Evaluate_mB454C8F8076A2A41E291FAE3D4FD31B2DC810F62_AdjustorThunk (void);
extern void BurstBoxQuery_Query_m78C0813809BC44E8027D68204D187FD1DA7242DB_AdjustorThunk (void);
extern void BurstRay_Evaluate_mE001829B68C458A87581958A3B9C6D7DBC85EC68_AdjustorThunk (void);
extern void BurstRay_Query_m0B4F7997C30EF099D2F72584B7EC3DB15EFBBEA9_AdjustorThunk (void);
extern void BurstSphereQuery_Evaluate_mA4AC455ADF4F22279A7667EFC7731B78EF1ECB9C_AdjustorThunk (void);
extern void BurstSphereQuery_Query_mC1E1903D249FD6440ACFB80FB3D2EBDB75F81875_AdjustorThunk (void);
extern void SpatialQueryJob_Execute_mF6926D6C57FF21B92A42A0CF712D5DE2808238CF_AdjustorThunk (void);
extern void SpatialQueryJob_CalculateShapeAABB_mD4BB4AAFB5B7C57689D4C530F27046BAEEBBF9D2_AdjustorThunk (void);
extern void SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8_AdjustorThunk (void);
extern void CalculateQueryDistances_Execute_m8DAEA0919E33220185485804208ED37F68B12355_AdjustorThunk (void);
extern void ApplyInertialForcesJob_Execute_m6D16C9CD6820B5E2E5B69F722A4CEB47D3109B74_AdjustorThunk (void);
extern void ParticleToBoundsJob_Execute_mF06E5D2A133701BF1984DAD08A9CAA0A53FF1F0C_AdjustorThunk (void);
extern void BoundsReductionJob_Execute_m560D9C4E0E5F1D2C870425DF2282FB1278ADB1C6_AdjustorThunk (void);
extern void BuildSimplexAabbs_Execute_mEBBE064B8F9A20CC5507809D8B1A4D30C3A4EA7B_AdjustorThunk (void);
extern void FindFluidParticlesJob_Execute_m477B14F5ADFE05D34CC12D3351EC114D4C03408E_AdjustorThunk (void);
extern void InterpolationJob_Execute_mF037213986C8166D161BEBF6CA5836F2F42FFAF6_AdjustorThunk (void);
extern void PredictPositionsJob_Execute_m42A393C96C2581FB6A24356ECC0E1807B43E5B8E_AdjustorThunk (void);
extern void UpdateInertiaTensorsJob_Execute_mB727A2BFE75FE34ACF7D46675FA0D766EF6C4D7B_AdjustorThunk (void);
extern void UpdateNormalsJob_Execute_m96B1F05E2523C94475A069C1D707F97A21B1EA81_AdjustorThunk (void);
extern void UpdatePositionsJob_Execute_m8DF43769C21749D1195FD8A483D9C8440A26614D_AdjustorThunk (void);
extern void UpdatePrincipalAxisJob_Execute_mE9809E4137D315000D2A3E570BCC7573B050ABDC_AdjustorThunk (void);
extern void UpdateVelocitiesJob_Execute_mBDA6FF10947E19ACC17D971540182892E043B621_AdjustorThunk (void);
extern void StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57_AdjustorThunk (void);
extern void StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF_AdjustorThunk (void);
extern void StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322_AdjustorThunk (void);
extern void DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0_AdjustorThunk (void);
extern void Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088_AdjustorThunk (void);
extern void Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16_AdjustorThunk (void);
extern void EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124_AdjustorThunk (void);
extern void HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421_AdjustorThunk (void);
extern void Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E_AdjustorThunk (void);
extern void Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D_AdjustorThunk (void);
extern void TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4_AdjustorThunk (void);
extern void DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609_AdjustorThunk (void);
extern void DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3_AdjustorThunk (void);
extern void DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B_AdjustorThunk (void);
extern void DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192_AdjustorThunk (void);
extern void Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113_AdjustorThunk (void);
extern void Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7_AdjustorThunk (void);
extern void Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4_AdjustorThunk (void);
extern void Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B_AdjustorThunk (void);
extern void Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08_AdjustorThunk (void);
extern void Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8_AdjustorThunk (void);
extern void Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664_AdjustorThunk (void);
extern void AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914_AdjustorThunk (void);
extern void AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB_AdjustorThunk (void);
extern void BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A_AdjustorThunk (void);
extern void CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B_AdjustorThunk (void);
extern void ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB_AdjustorThunk (void);
extern void ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E_AdjustorThunk (void);
extern void CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530_AdjustorThunk (void);
extern void ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA_AdjustorThunk (void);
extern void ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9_AdjustorThunk (void);
extern void ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1_AdjustorThunk (void);
extern void QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453_AdjustorThunk (void);
extern void SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86_AdjustorThunk (void);
extern void SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066_AdjustorThunk (void);
extern void SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95_AdjustorThunk (void);
extern void VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB_AdjustorThunk (void);
extern void VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C_AdjustorThunk (void);
extern void TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F_AdjustorThunk (void);
extern void TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF_AdjustorThunk (void);
extern void TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681_AdjustorThunk (void);
extern void TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[238] = 
{
	{ 0x06000090, SolverParameters__ctor_mBBC8FFC199730BCFDE16BEE7BA9BCC31B01476C3_AdjustorThunk },
	{ 0x06000091, ConstraintParameters__ctor_m3554D9568B4E65EF55CF0E0477821F3FC84C6566_AdjustorThunk },
	{ 0x060001AB, SkinTransform__ctor_m78B02CDF43EE02BE727C8FCC0D6F95604A07A895_AdjustorThunk },
	{ 0x060001AC, SkinTransform__ctor_m001835E2CFA6F2A16C261A2C99D1041F9FC93E80_AdjustorThunk },
	{ 0x060001AD, SkinTransform_Apply_m3525A4C24CE7B7D4F48AE19E5F9017E34ACA2EA0_AdjustorThunk },
	{ 0x060001AE, SkinTransform_GetMatrix4X4_mAF9C7913EC6C7423E4C2352C8757E5E1A7D945EE_AdjustorThunk },
	{ 0x060001AF, SkinTransform_Reset_m9B71F9778C93FB12DB63190688B1CD20C4049E6E_AdjustorThunk },
	{ 0x060001B1, BarycentricPoint__ctor_m085695B98E60C32F4610B8ECDF30EDE4C3A6C30F_AdjustorThunk },
	{ 0x06000282, CachedTri_Cache_mAE403285E691BBA51817154E6CF25A99BF9D24F1_AdjustorThunk },
	{ 0x06000283, BurstBox_Evaluate_m9A7975B75E4F6E739D90F73F2CB2209B369FDBFA_AdjustorThunk },
	{ 0x06000284, BurstBox_Contacts_m9D3DED1FA7E7454E9EB7A52A7ED94B8A08C99CE3_AdjustorThunk },
	{ 0x06000285, BurstBox_Obi_IBurstCollider_Contacts_m97856F2BFA4EBA5278F111B41C7F5CA5A8C58573_AdjustorThunk },
	{ 0x06000286, BurstCapsule_Evaluate_mB2A54FDA05AAE32DF69B20A013BDCF441F379D46_AdjustorThunk },
	{ 0x06000287, BurstCapsule_Contacts_mE461E9B2E609E1A411ED668A5C6380B29FCCE4D6_AdjustorThunk },
	{ 0x06000288, BurstCapsule_Obi_IBurstCollider_Contacts_mADF596860548ABE29238ADEA0F82324B6D1A6A28_AdjustorThunk },
	{ 0x06000298, IdentifyMovingColliders_Execute_m6536871CD5E23F8233E90C455F8B29C3839BA490_AdjustorThunk },
	{ 0x06000299, UpdateMovingColliders_Execute_m05B0B145094896C8C5D0A421ECFAB255D1C7D4D6_AdjustorThunk },
	{ 0x0600029A, GenerateContactsJob_Execute_m1A30B956B328522C42AAB9316CFE714A83873DE5_AdjustorThunk },
	{ 0x0600029B, GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E_AdjustorThunk },
	{ 0x0600029C, BurstDFNode_SampleWithGradient_m383A3E8C19EBD56E7660F1D957022D53CC0D5173_AdjustorThunk },
	{ 0x0600029D, BurstDFNode_GetNormalizedPos_mD01E0D79BAC83035CC86DC3706B9B7AA98B3CF9D_AdjustorThunk },
	{ 0x0600029E, BurstDFNode_GetOctant_m76BF4D7E298B1E7238848063468AFE583C403408_AdjustorThunk },
	{ 0x0600029F, BurstDistanceField_Evaluate_mC93945AD7F3206A1EC39A627C982644CF24673B9_AdjustorThunk },
	{ 0x060002A0, BurstDistanceField_Contacts_mC98735809C55B8C503AC05F45BEC80420352057E_AdjustorThunk },
	{ 0x060002A2, BurstDistanceField_Obi_IBurstCollider_Contacts_m212E14857D066BE00574BE85A69F636A0B5A14BF_AdjustorThunk },
	{ 0x060002A3, BurstEdgeMesh_Evaluate_mB1DE344E5F95F0D35C81FCEAAFD439CCA1A0281B_AdjustorThunk },
	{ 0x060002A4, BurstEdgeMesh_Contacts_mDEAD5DA052578CD2794227155F0CFDEFB76DD9BF_AdjustorThunk },
	{ 0x060002A5, BurstEdgeMesh_BIHTraverse_m8E806DD0BD9B370CC1862099121BC5B3F83B9CA9_AdjustorThunk },
	{ 0x060002A6, BurstEdgeMesh_Obi_IBurstCollider_Contacts_mE8D91763243332F3BBE8D636493C232F3085C1C1_AdjustorThunk },
	{ 0x060002A7, BurstHeightField_Evaluate_m37E0774FD60539E9B60C6265C5289EABA40FEE16_AdjustorThunk },
	{ 0x060002A8, BurstHeightField_Contacts_m8D519E8FEE86E9D62A2F24AFE65643D75C82E321_AdjustorThunk },
	{ 0x060002A9, BurstHeightField_Obi_IBurstCollider_Contacts_mAF9CC53C9637FA7BEFAACAB317DEECC4325EFEA8_AdjustorThunk },
	{ 0x060002AF, BurstSimplex_CacheData_mCC1D44424D3570F494A138737B7CC9B57B56FDBA_AdjustorThunk },
	{ 0x060002B0, BurstSimplex_Evaluate_m96C3BF51F11E624BC054D9BA276F177A9DBBBE73_AdjustorThunk },
	{ 0x060002B1, BurstSphere_Evaluate_mD03D68FDA6CCD8B6481B8CF75FB5C1528BA034B8_AdjustorThunk },
	{ 0x060002B2, BurstSphere_Contacts_mDA37B8F9954ECD2EFD7BF8F9A51A43CA73A869AE_AdjustorThunk },
	{ 0x060002B3, BurstSphere_Obi_IBurstCollider_Contacts_m16229652DCB867F59A4F4360F064FDA00691D32B_AdjustorThunk },
	{ 0x060002B4, BurstTriangleMesh_Evaluate_mB03B52D7D8BA26D3C383CB9334A57EDD03B03DE5_AdjustorThunk },
	{ 0x060002B5, BurstTriangleMesh_Contacts_m03A033B4760F0729757BF20D322FC8D112802A00_AdjustorThunk },
	{ 0x060002B6, BurstTriangleMesh_BIHTraverse_m53B6E0B2D8D09B465000034E751D2BB96E194907_AdjustorThunk },
	{ 0x060002B7, BurstTriangleMesh_Obi_IBurstCollider_Contacts_m0414B63AB8F72DB613D9240E6B5AA847FB4B296D_AdjustorThunk },
	{ 0x060002C1, AerodynamicConstraintsBatchJob_Execute_mA12D60F5737F1B49A118E40DEBB6D5BE5D9FDCE1_AdjustorThunk },
	{ 0x060002C9, BendConstraintsBatchJob_Execute_mF6E997FD25554FC9F4071D28B24E8804DEF39870_AdjustorThunk },
	{ 0x060002CA, ApplyBendConstraintsBatchJob_Execute_m99F086827B78551FE09B2FBC6881B2894BCB7682_AdjustorThunk },
	{ 0x060002D2, BendTwistConstraintsBatchJob_Execute_m72C33183ADAB152169BB181429D90663B0DB648A_AdjustorThunk },
	{ 0x060002D3, ApplyBendTwistConstraintsBatchJob_Execute_m74A218C4F257CB77BB42C70892690A866BC97861_AdjustorThunk },
	{ 0x060002FA, ChainConstraintsBatchJob_Execute_m576FF3805857AEBF06581865F975D5D982550907_AdjustorThunk },
	{ 0x060002FB, ApplyChainConstraintsBatchJob_Execute_mA038CCA6A34C6A73AECD256274108E245F5FE91A_AdjustorThunk },
	{ 0x060002FC, ApplyCollisionConstraintsBatchJob_Execute_m875BE8298CCA64DB1EC6FA478647C40913DD330D_AdjustorThunk },
	{ 0x06000305, UpdateContactsJob_Execute_mBDE78DEE6DE6D94DC3B6684199C0292D4C915E0D_AdjustorThunk },
	{ 0x06000306, CollisionConstraintsBatchJob_Execute_m7E251F652F57C72F5A552957040597FA6DCD6688_AdjustorThunk },
	{ 0x06000307, CollisionConstraintsBatchJob_CombineCollisionMaterials_mF1338B4C889E6A783168F52825A68777DBAE0C99_AdjustorThunk },
	{ 0x06000310, FrictionConstraintsBatchJob_Execute_mA6E69C218FC0969B77330BD6B4D5C15A6F49E056_AdjustorThunk },
	{ 0x06000311, FrictionConstraintsBatchJob_CombineCollisionMaterials_mE4F955E2ED7F5DD7834726D2014A7555573FDB3C_AdjustorThunk },
	{ 0x06000320, ClearFluidDataJob_Execute_mD48A32AF77242D2E6CDFF920317494A459561F18_AdjustorThunk },
	{ 0x06000321, UpdateInteractionsJob_Execute_m1EEE83F71148CE066BB48348ECCA642F2A1E80CE_AdjustorThunk },
	{ 0x06000322, CalculateLambdasJob_Execute_m2B425DF0EEAD9932568604F718B2921296DA4A79_AdjustorThunk },
	{ 0x06000323, ApplyVorticityConfinementAndAtmosphere_Execute_mB2A90B74BE8FF621E0A2E25863B37740AF5D95D9_AdjustorThunk },
	{ 0x06000324, IdentityAnisotropyJob_Execute_m8BCDAC71C9652E2C66490C406047751573B95B40_AdjustorThunk },
	{ 0x06000325, AverageSmoothPositionsJob_Execute_m8530DC393B3E014A32A67CF6BE5657D78714B493_AdjustorThunk },
	{ 0x06000326, AverageAnisotropyJob_Execute_m8E8F860CE0895F6EEE07E14E2C09507F15CAF28C_AdjustorThunk },
	{ 0x0600032F, UpdateDensitiesJob_Execute_m59352095C34B63F24D23B524548542B60EF8B6F7_AdjustorThunk },
	{ 0x06000330, ApplyDensityConstraintsJob_Execute_m9CE8FB7550A4AFC0C47CD7569ABB38E69B978F16_AdjustorThunk },
	{ 0x06000331, NormalsViscosityAndVorticityJob_Execute_mBABF5C9E0E64304EEA30D40729CEAB8C02E78C86_AdjustorThunk },
	{ 0x06000332, CalculateVorticityEta_Execute_mBD1C201BA56247D5A29070646ECD1C54D8FA3B31_AdjustorThunk },
	{ 0x06000333, AccumulateSmoothPositionsJob_Execute_m198C016973ED826593021549CB78790D8D97590C_AdjustorThunk },
	{ 0x06000334, AccumulateAnisotropyJob_Execute_mDC2644AC6278164D69AF0BF1AB94E439BA2B1396_AdjustorThunk },
	{ 0x06000335, Poly6Kernel__ctor_m458F1673FE9C7B44144243984BF8D3351EF76641_AdjustorThunk },
	{ 0x06000336, Poly6Kernel_W_mA9FD1FFD1D90D74DB6803EC97F570DAB31EAE849_AdjustorThunk },
	{ 0x06000337, SpikyKernel__ctor_mA630C18D4CA7AEE79027ADBEB1BFCBF26A7452BE_AdjustorThunk },
	{ 0x06000338, SpikyKernel_W_mA7FD6FEFAF094558177A73FB38603EFC70E8D697_AdjustorThunk },
	{ 0x06000340, DistanceConstraintsBatchJob_Execute_m8974C56ADBED13F088ADEC56AF17CBF723258A16_AdjustorThunk },
	{ 0x06000341, ApplyDistanceConstraintsBatchJob_Execute_m3D79DE4E8EDE7541DC1B61399471933671AFAFDF_AdjustorThunk },
	{ 0x06000342, ApplyBatchedCollisionConstraintsBatchJob_Execute_m6B349C47BB948DA01570848A8C29F1D3BEF4C3D5_AdjustorThunk },
	{ 0x0600034C, UpdateParticleContactsJob_Execute_mA60AE07BFCE3A0F563D19A316C153C7F9887ED7E_AdjustorThunk },
	{ 0x0600034D, ParticleCollisionConstraintsBatchJob_Execute_m8485003051D23F0D75146E26663B7CA0AF2386F1_AdjustorThunk },
	{ 0x0600034E, ParticleCollisionConstraintsBatchJob_CombineCollisionMaterials_mEC293E92648E63BD95F591444FDFF4B18F33D703_AdjustorThunk },
	{ 0x06000358, ParticleFrictionConstraintsBatchJob_Execute_m79FE4542796B3C2B9146A83500647A9D0680830C_AdjustorThunk },
	{ 0x06000359, ParticleFrictionConstraintsBatchJob_CombineCollisionMaterials_mF0CD7C3874B720CC924931A83AB1EE44FBF0797D_AdjustorThunk },
	{ 0x06000361, PinConstraintsBatchJob_Execute_mEF5E6798264EF1F83CC82A6A98B54DECCD4D1E36_AdjustorThunk },
	{ 0x06000362, ApplyPinConstraintsBatchJob_Execute_mB647701B0C09CE1D750AF92DA09F9F8E78FD52DB_AdjustorThunk },
	{ 0x0600036E, ShapeMatchingCalculateRestJob_Execute_mF557395226A5479543E3F443C1787EE2F5D4AB24_AdjustorThunk },
	{ 0x0600036F, ShapeMatchingConstraintsBatchJob_Execute_mE253B06332A40DCA4C6453A876DB428FA345B047_AdjustorThunk },
	{ 0x06000370, ApplyShapeMatchingConstraintsBatchJob_Execute_m1ED0E064C42CCD032FDE804F098C1250B8DDE687_AdjustorThunk },
	{ 0x06000378, SkinConstraintsBatchJob_Execute_mE9464013B468FA413A33380F7080A6E8984B03A5_AdjustorThunk },
	{ 0x06000379, ApplySkinConstraintsBatchJob_Execute_m65A2A62E22D2996164E8F0E34B60EAB4C923CDCD_AdjustorThunk },
	{ 0x06000381, StitchConstraintsBatchJob_Execute_mA5D5BEA6FB1DA22EEAB3C0CD7CEB52145DBE1BCD_AdjustorThunk },
	{ 0x06000382, ApplyStitchConstraintsBatchJob_Execute_m50E228A24C8D90CE705CA819CBCB9F3D1FF7E7B6_AdjustorThunk },
	{ 0x0600038A, StretchShearConstraintsBatchJob_Execute_m38FE03FB561A839F2E3C895AC52308E3374C50CC_AdjustorThunk },
	{ 0x0600038B, ApplyStretchShearConstraintsBatchJob_Execute_mB1D83D142361E62A2D687C29EE346B26AD985A70_AdjustorThunk },
	{ 0x06000393, TetherConstraintsBatchJob_Execute_mEF1FE91E81FCED26D0A12BF953AFB35780ED52C8_AdjustorThunk },
	{ 0x06000394, ApplyTetherConstraintsBatchJob_Execute_mBF97BD7B8E8E46587578F03ADCD029486BD104CD_AdjustorThunk },
	{ 0x0600039C, VolumeConstraintsBatchJob_Execute_mB6BC30FDA2A00B126A2939FE5841FABE48971AB6_AdjustorThunk },
	{ 0x0600039D, ApplyVolumeConstraintsBatchJob_Execute_m8A7F092F18089C81D544CD0E08BCA2EE41AB7C25_AdjustorThunk },
	{ 0x0600039E, BurstAabb_get_size_m5E23B4420CB21B196C931459BECA271D6567E28C_AdjustorThunk },
	{ 0x0600039F, BurstAabb_get_center_mEF7385D666BE5EE77B04D4C1B507CC277498DF83_AdjustorThunk },
	{ 0x060003A0, BurstAabb__ctor_m5EA38EB6DA406EE85C1CDCC3B4053EFDA9AF999C_AdjustorThunk },
	{ 0x060003A1, BurstAabb__ctor_m25051D2B2D8D634E1497B4FBA7DA5855CA630293_AdjustorThunk },
	{ 0x060003A2, BurstAabb__ctor_mBFC57B41EDB0B6AD54F1CEED8ABDFCE2CF73FD6D_AdjustorThunk },
	{ 0x060003A3, BurstAabb__ctor_m20D651928C007C6CDCF1FA28E061578888324215_AdjustorThunk },
	{ 0x060003A4, BurstAabb_AverageAxisLength_mC3D10DA8BEC0F0BA5B418EDB620BDCD4A2951C8E_AdjustorThunk },
	{ 0x060003A5, BurstAabb_MaxAxisLength_m00B824BC8D68124AD6362187459834CD10F2C9AD_AdjustorThunk },
	{ 0x060003A6, BurstAabb_EncapsulateParticle_m2BF518A13C65F7BC9B5EB0175BA00D3A07FA3350_AdjustorThunk },
	{ 0x060003A7, BurstAabb_EncapsulateParticle_m9154ADC7C45964966B05F06F5B92412630067FA7_AdjustorThunk },
	{ 0x060003A8, BurstAabb_EncapsulateBounds_m9AFA483BA1B7BEBF77C1B85D57549FB36375E1D7_AdjustorThunk },
	{ 0x060003A9, BurstAabb_Expand_mD6FD7B154B8700A8F46316D8ACFFD928F92DAD49_AdjustorThunk },
	{ 0x060003AA, BurstAabb_Sweep_mCB51F0C277EA7EF3C7F033CF125C1A0B04E7541E_AdjustorThunk },
	{ 0x060003AB, BurstAabb_Transform_m972887B0B52D4B08B4806F6BB8196E17B64C0B90_AdjustorThunk },
	{ 0x060003AC, BurstAabb_Transform_m20348C56E245E750A6399FA6F7C2D767EE11B91C_AdjustorThunk },
	{ 0x060003AD, BurstAabb_Transformed_m09422ADB914E6917056FB53FEDDC172E7AEE88C1_AdjustorThunk },
	{ 0x060003AE, BurstAabb_Transformed_mE44D84A994DBAAD9EED74F026F8220ADCD9A249E_AdjustorThunk },
	{ 0x060003AF, BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6_AdjustorThunk },
	{ 0x060003B0, BurstAabb_IntersectsRay_m94E7181E1827E8BE7F727FBCFB48EB11CC40CFAD_AdjustorThunk },
	{ 0x060003B1, BurstAffineTransform__ctor_m05A35A300C65D6551377AD2C948ACA1FC2F8667C_AdjustorThunk },
	{ 0x060003B3, BurstAffineTransform_Inverse_mDD81DD6911040D1CA2FE9F0D1F315F7812DF27B5_AdjustorThunk },
	{ 0x060003B4, BurstAffineTransform_Interpolate_mECFFF4666C1393B072387A61B32573B56BCFE676_AdjustorThunk },
	{ 0x060003B5, BurstAffineTransform_TransformPoint_m391756FAA0719FE03FA3ABFD002CF41953DFE610_AdjustorThunk },
	{ 0x060003B6, BurstAffineTransform_InverseTransformPoint_m62FF98D4C1F4B903143CBEBB8BE560DD8973E955_AdjustorThunk },
	{ 0x060003B7, BurstAffineTransform_TransformPointUnscaled_m88D0781E4CA7ACAB74CC46049AF03EEE34AF17D8_AdjustorThunk },
	{ 0x060003B8, BurstAffineTransform_InverseTransformPointUnscaled_mB57EB7ACC6E6630D7AB86C3B3C4B330CDA781675_AdjustorThunk },
	{ 0x060003B9, BurstAffineTransform_TransformDirection_m6194FE90F0734E4FAC3D9EC2F18B1418E054B284_AdjustorThunk },
	{ 0x060003BA, BurstAffineTransform_InverseTransformDirection_mD556AD3447707CC7AF2C21B42B1009B169B07E13_AdjustorThunk },
	{ 0x060003BB, BurstAffineTransform_TransformVector_m93FE5B596EE6E92173AEF3E44B265907B0371AF0_AdjustorThunk },
	{ 0x060003BC, BurstAffineTransform_InverseTransformVector_m5F477834E781A85ADC934DE24BC0FEB89A6BDDF9_AdjustorThunk },
	{ 0x060003BD, BurstCellSpan__ctor_m06DA83A6F5C75A3D3B7B8E2F97C86D245EB3DD9D_AdjustorThunk },
	{ 0x060003BE, BurstCellSpan__ctor_m285AB617964F7D42BF536E07ED69BEA63327EE37_AdjustorThunk },
	{ 0x060003BF, BurstCellSpan_get_level_mEA8299C260E4E7D5A362075EAFDBAEC20CDCF21B_AdjustorThunk },
	{ 0x060003C0, BurstCellSpan_Equals_mE730FAAEE94E797BCE3F326B494C301956A7014D_AdjustorThunk },
	{ 0x060003C1, BurstCellSpan_Equals_mACAF86A3EDB8CCB7033E573669BBE26088CCF696_AdjustorThunk },
	{ 0x060003C2, BurstCellSpan_GetHashCode_mACB4CF48DD8B4D70211F28DACDEE8DC6A7F421EA_AdjustorThunk },
	{ 0x060003C6, BurstInertialFrame__ctor_mA98B43A3120665CC585634E855B81B740024C066_AdjustorThunk },
	{ 0x060003C7, BurstInertialFrame__ctor_m23E7F3F42FD0210FF6C53F7D04954946BC11A943_AdjustorThunk },
	{ 0x060003C8, BurstInertialFrame_Update_mFDDBF9FACE375ABB5982E879EDC404B54CFF6E34_AdjustorThunk },
	{ 0x060003C9, BatchLUT__ctor_m93F109809773B154AF095575B0C00C35E224346C_AdjustorThunk },
	{ 0x060003CA, BatchLUT_Dispose_mCFABB1B7385B31C2C7DD8AD9123205A3732D13BE_AdjustorThunk },
	{ 0x060003CB, BatchData__ctor_mD6B3E7F16DF9B5364DDCC3E92B5CDF971E4A69BD_AdjustorThunk },
	{ 0x060003CC, BatchData_GetConstraintRange_m685408EAF447F1B6A4AA9C17803E061A58752449_AdjustorThunk },
	{ 0x060003CD, ConstraintBatcher__ctor_mEEC00AF4C13C2B7D3E750958E9F78A639DCFBC76_AdjustorThunk },
	{ 0x060003CE, ConstraintBatcher_Dispose_m832285B8BE78DFA00F4E2253150C447F9E546889_AdjustorThunk },
	{ 0x060003D0, WorkItem_Add_m8A0EF0EB1E6104C1CD4E12708761C75BCE6281DD_AdjustorThunk },
	{ 0x060003D7, ContactProvider_GetConstraintCount_mACC69AA75EE8103A1AAED3B3924CB26DF9798FEF_AdjustorThunk },
	{ 0x060003D8, ContactProvider_GetParticleCount_m3C3C405609067B4AF80D1D4269CDB486BA02D9FC_AdjustorThunk },
	{ 0x060003D9, ContactProvider_GetParticle_m0B5FB5F90C47141EF186E6ECE7169AD3F0B5E54E_AdjustorThunk },
	{ 0x060003DA, ContactProvider_WriteSortedConstraint_m5568D339D5163D4BAF3BF92562D993B463670F5E_AdjustorThunk },
	{ 0x060003DB, FluidInteractionProvider_GetConstraintCount_m010E43F859A7E41424DDDD5B8C03B75C3B05F3FA_AdjustorThunk },
	{ 0x060003DC, FluidInteractionProvider_GetParticleCount_m7E6514985CBFF6300D92399124419655353DB050_AdjustorThunk },
	{ 0x060003DD, FluidInteractionProvider_GetParticle_mBB30B78F2DEA50E3E3BDEABAE3B2CB39DBA52DCF_AdjustorThunk },
	{ 0x060003DE, FluidInteractionProvider_WriteSortedConstraint_m2CAE0F1AC443F672E9503FC238EEC9A73C84DFE3_AdjustorThunk },
	{ 0x060003E3, FluidInteraction_GetParticleCount_m986AE991B21B5250E354ADCE40955BB274A954C0_AdjustorThunk },
	{ 0x060003E4, FluidInteraction_GetParticle_mE71B2BF68C60B06FFB723114CCB4874EFBAD83E9_AdjustorThunk },
	{ 0x06000403, CalculateCellCoords_Execute_m9BC5182A60EBE588E765C2A8C2077A2CD313D955_AdjustorThunk },
	{ 0x06000404, UpdateGrid_Execute_m42C3401054B89A0D6054E8D078D1DC7E56D7EB47_AdjustorThunk },
	{ 0x06000405, GenerateParticleParticleContactsJob_Execute_m59F55F68148AECEDED3B330597C345162D55C3B2_AdjustorThunk },
	{ 0x06000406, GenerateParticleParticleContactsJob_IntraCellSearch_mA47CA9BEC6B39199D8F1D9ED7C08DCC43917E1BB_AdjustorThunk },
	{ 0x06000407, GenerateParticleParticleContactsJob_InterCellSearch_m92209119598047DA47D950FBCE83176C7A24A194_AdjustorThunk },
	{ 0x06000408, GenerateParticleParticleContactsJob_IntraLevelSearch_m15B1F86FB9C4F6BEDFE7ED016AE1610C8CA32418_AdjustorThunk },
	{ 0x06000409, GenerateParticleParticleContactsJob_GetSimplexGroup_m29CDF1F792214CEBAA4A9D8F3E428A4CB311E937_AdjustorThunk },
	{ 0x0600040A, GenerateParticleParticleContactsJob_InteractionTest_m28D282C80048AF14CBB578222D4F16809BD85DE7_AdjustorThunk },
	{ 0x0600040B, InterpolateDiffusePropertiesJob_Execute_m003A4B1AF1CDC725C62CCF54A068C5111C35D54A_AdjustorThunk },
	{ 0x0600040C, BurstContact_GetParticleCount_m07601B081D4B2D33AA274A224230DDF7FA41DA12_AdjustorThunk },
	{ 0x0600040D, BurstContact_GetParticle_mA6D14AB9D6B0C1C0E2D489D9AEAC4F1DACF44976_AdjustorThunk },
	{ 0x0600040E, BurstContact_ToString_m082A762EDA6C99C0D046A08E1456DC09B503654B_AdjustorThunk },
	{ 0x0600040F, BurstContact_CompareTo_m4AAE9C314CA58F9ECD6DAC6E1FE8C4046C00A550_AdjustorThunk },
	{ 0x06000410, BurstContact_get_TotalNormalInvMass_m8DB8BB9F92C654E952F6EFD3E30695C049FADAE4_AdjustorThunk },
	{ 0x06000411, BurstContact_get_TotalTangentInvMass_m23A1474CCF4F8B7EFD804072C2B8C1081570C36A_AdjustorThunk },
	{ 0x06000412, BurstContact_get_TotalBitangentInvMass_m6584B11DFF4F6F649C7F2048CA928C7321528FEB_AdjustorThunk },
	{ 0x06000413, BurstContact_CalculateBasis_m7756F0C85D337D0FCC1301EF91A9DDA3AC1F11FF_AdjustorThunk },
	{ 0x06000414, BurstContact_CalculateContactMassesA_m6442A92150D7BACA30A83170C952918D5941B9B7_AdjustorThunk },
	{ 0x06000415, BurstContact_CalculateContactMassesB_m525CB01618D4402EFE433CDD47AC61735B04A24D_AdjustorThunk },
	{ 0x06000416, BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524_AdjustorThunk },
	{ 0x06000417, BurstContact_SolveAdhesion_m1CFCDDFF32CBB02EA4050AA3184E9AB1BF33B39B_AdjustorThunk },
	{ 0x06000418, BurstContact_SolvePenetration_mA388AC5B79F15B50ED2678A37DFA62CF8D7D12D3_AdjustorThunk },
	{ 0x06000419, BurstContact_SolveFriction_m50255C39E593D5D9ABAAE76C19E618577F5E24F4_AdjustorThunk },
	{ 0x0600041A, BurstContact_SolveRollingFriction_mE67B076235FF4D42A16E7624EDF3B4ED4E32463A_AdjustorThunk },
	{ 0x0600041B, BurstBoxQuery_Evaluate_mB454C8F8076A2A41E291FAE3D4FD31B2DC810F62_AdjustorThunk },
	{ 0x0600041C, BurstBoxQuery_Query_m78C0813809BC44E8027D68204D187FD1DA7242DB_AdjustorThunk },
	{ 0x0600041D, BurstRay_Evaluate_mE001829B68C458A87581958A3B9C6D7DBC85EC68_AdjustorThunk },
	{ 0x0600041E, BurstRay_Query_m0B4F7997C30EF099D2F72584B7EC3DB15EFBBEA9_AdjustorThunk },
	{ 0x0600041F, BurstSphereQuery_Evaluate_mA4AC455ADF4F22279A7667EFC7731B78EF1ECB9C_AdjustorThunk },
	{ 0x06000420, BurstSphereQuery_Query_mC1E1903D249FD6440ACFB80FB3D2EBDB75F81875_AdjustorThunk },
	{ 0x06000421, SpatialQueryJob_Execute_mF6926D6C57FF21B92A42A0CF712D5DE2808238CF_AdjustorThunk },
	{ 0x06000422, SpatialQueryJob_CalculateShapeAABB_mD4BB4AAFB5B7C57689D4C530F27046BAEEBBF9D2_AdjustorThunk },
	{ 0x06000423, SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8_AdjustorThunk },
	{ 0x06000424, CalculateQueryDistances_Execute_m8DAEA0919E33220185485804208ED37F68B12355_AdjustorThunk },
	{ 0x06000425, ApplyInertialForcesJob_Execute_m6D16C9CD6820B5E2E5B69F722A4CEB47D3109B74_AdjustorThunk },
	{ 0x06000426, ParticleToBoundsJob_Execute_mF06E5D2A133701BF1984DAD08A9CAA0A53FF1F0C_AdjustorThunk },
	{ 0x06000427, BoundsReductionJob_Execute_m560D9C4E0E5F1D2C870425DF2282FB1278ADB1C6_AdjustorThunk },
	{ 0x06000428, BuildSimplexAabbs_Execute_mEBBE064B8F9A20CC5507809D8B1A4D30C3A4EA7B_AdjustorThunk },
	{ 0x06000456, FindFluidParticlesJob_Execute_m477B14F5ADFE05D34CC12D3351EC114D4C03408E_AdjustorThunk },
	{ 0x06000457, InterpolationJob_Execute_mF037213986C8166D161BEBF6CA5836F2F42FFAF6_AdjustorThunk },
	{ 0x06000458, PredictPositionsJob_Execute_m42A393C96C2581FB6A24356ECC0E1807B43E5B8E_AdjustorThunk },
	{ 0x06000459, UpdateInertiaTensorsJob_Execute_mB727A2BFE75FE34ACF7D46675FA0D766EF6C4D7B_AdjustorThunk },
	{ 0x0600045A, UpdateNormalsJob_Execute_m96B1F05E2523C94475A069C1D707F97A21B1EA81_AdjustorThunk },
	{ 0x0600045B, UpdatePositionsJob_Execute_m8DF43769C21749D1195FD8A483D9C8440A26614D_AdjustorThunk },
	{ 0x0600045C, UpdatePrincipalAxisJob_Execute_mE9809E4137D315000D2A3E570BCC7573B050ABDC_AdjustorThunk },
	{ 0x0600045D, UpdateVelocitiesJob_Execute_mBDA6FF10947E19ACC17D971540182892E043B621_AdjustorThunk },
	{ 0x06000655, StructuralConstraint_get_restLength_mA86CC2512F19A0314F76C459C7027A50DEB66F57_AdjustorThunk },
	{ 0x06000656, StructuralConstraint_set_restLength_mFDDB0536829F1845FAB3F39B80ED0520431D2BDF_AdjustorThunk },
	{ 0x06000657, StructuralConstraint__ctor_m9F7FF404C8685160AC46543FCE7681E91FF6B322_AdjustorThunk },
	{ 0x06000714, DistanceFieldHeader__ctor_mD9023657AE562DC6E93CAEBAEA87D8BBFBE3CAC0_AdjustorThunk },
	{ 0x06000719, Edge__ctor_m58FEEBE96D412AFED6635E824686137251119088_AdjustorThunk },
	{ 0x0600071A, Edge_GetBounds_m3627A8DC0128EA13268432FD42FDFFA5E9E8FC16_AdjustorThunk },
	{ 0x0600071C, EdgeMeshHeader__ctor_mFBC1ECFE2844D2E52FDDEF03C23174C84E12C124_AdjustorThunk },
	{ 0x06000725, HeightFieldHeader__ctor_m68533ABFD4D9CB9C9AE92C04D96F69D03704B421_AdjustorThunk },
	{ 0x06000739, Triangle__ctor_mF3FB7D0163513605C1284E55D476938CFA8D8B3E_AdjustorThunk },
	{ 0x0600073A, Triangle_GetBounds_m96DD7752DB622FBB5D130CC0203D16EF9A16183D_AdjustorThunk },
	{ 0x0600073C, TriangleMeshHeader__ctor_m05352EA77E9896540763C1FC4DA9A86E8FD110E4_AdjustorThunk },
	{ 0x06000751, DFNode__ctor_m5AA5A22FD71D7439C9136C2FBC6F151E25C8A609_AdjustorThunk },
	{ 0x06000752, DFNode_Sample_mD228ACC5980D03D69BA1510383BA88AD4253D1D3_AdjustorThunk },
	{ 0x06000753, DFNode_GetNormalizedPos_m48205296253B504FB1F9D3177EADD8E62A70F31B_AdjustorThunk },
	{ 0x06000754, DFNode_GetOctant_m8E57E952AB3E07E8596E051854C4B9EF62575192_AdjustorThunk },
	{ 0x06000755, Aabb_get_center_m6F29EA934C6E7C6A2557550214746BE327737113_AdjustorThunk },
	{ 0x06000756, Aabb_get_size_m316B3CACF5BB496D6CF8532AF06AF826C78486D7_AdjustorThunk },
	{ 0x06000757, Aabb__ctor_m8B1C8639B79DD7C951EF63FD820BB63B8A6B5DB4_AdjustorThunk },
	{ 0x06000758, Aabb__ctor_m293B0520F59E98F3B14FEAFE282685CA663D2E0B_AdjustorThunk },
	{ 0x06000759, Aabb_Encapsulate_mDCE164B716BFA4A7F8147BC997DDDAFF5A0A9F08_AdjustorThunk },
	{ 0x0600075A, Aabb_Encapsulate_m4F8536D0A22C6A596F1393D02F97BA7C45B9F9A8_AdjustorThunk },
	{ 0x0600075B, Aabb_FromBounds_mB313907D1B69DA3917270DE5C2B39AD5ECCCF664_AdjustorThunk },
	{ 0x0600075C, AffineTransform__ctor_m1113EA596451380FE97BF986F98D180B67B50914_AdjustorThunk },
	{ 0x0600075D, AffineTransform_FromTransform_mF590DA4EFBCE17F0245A9C33FE65B7405154A7EB_AdjustorThunk },
	{ 0x06000765, BIHNode__ctor_mBE805A0C558FDCA2FEDE25366ED721EA3776834A_AdjustorThunk },
	{ 0x06000767, CellSpan__ctor_m93CAC71E3E1DC790B927C8C76F72A3784072746B_AdjustorThunk },
	{ 0x06000768, ColliderRigidbody_FromRigidbody_m4921C838DA8C3C147A91168C9232CB97780260BB_AdjustorThunk },
	{ 0x06000769, ColliderRigidbody_FromRigidbody_m2311F1A25138EA07373D405B7EF2F1EB3859138E_AdjustorThunk },
	{ 0x0600076A, CollisionMaterial_FromObiCollisionMaterial_mBF4E29EA56479F76E396B2B5EA9BDEFED6663530_AdjustorThunk },
	{ 0x060007E8, ParticlePair__ctor_mDDAD78C599D557D2A63715207CBC71A5A93581CA_AdjustorThunk },
	{ 0x060007E9, ParticlePair_get_Item_m96F244D843B43EE4A0E48128BDAE3837B0574AF9_AdjustorThunk },
	{ 0x060007EA, ParticlePair_set_Item_m39EFBD38D2AB94AA009AAF1C18B7EA40077EEDB1_AdjustorThunk },
	{ 0x060007EB, QueryShape__ctor_m10B944E68ED9FE30CB3D7EE090068A33F9EDB453_AdjustorThunk },
	{ 0x060007EC, SimplexCounts_get_simplexCount_m99631648B641C2ECF96D0C8E5DB265D40F522A86_AdjustorThunk },
	{ 0x060007ED, SimplexCounts__ctor_m68F3A4CF7A694C3300E7BA4227020A89A724F066_AdjustorThunk },
	{ 0x060007EE, SimplexCounts_GetSimplexStartAndSize_m27E5A762121E379170DD5C8A757EB3124C259B95_AdjustorThunk },
	{ 0x060007EF, VInt4__ctor_m6FDFB28D4337C36A4A49681BA2E0B85D0B36AFCB_AdjustorThunk },
	{ 0x060007F0, VInt4__ctor_mDF85C8595BC6DCEAAF3A5A4A04704DEFDB238C6C_AdjustorThunk },
	{ 0x06000832, TargetVoxel_get_cost_mA1EEA46FACEFC9CE09774BB718F1B433CBF07A5F_AdjustorThunk },
	{ 0x06000833, TargetVoxel__ctor_m875F88DE21201FC3ABFA3610B0168D7E2D844EDF_AdjustorThunk },
	{ 0x06000834, TargetVoxel_Equals_m0E9AECCF869111BE8E9097F08922DF25A40F2681_AdjustorThunk },
	{ 0x06000835, TargetVoxel_CompareTo_m38B8922551F201F9DA961C6C959404E723B5CCC8_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2501] = 
{
	3780,
	3780,
	3780,
	3705,
	2060,
	5313,
	5846,
	5312,
	5845,
	6032,
	6031,
	5318,
	6299,
	3749,
	3780,
	3780,
	3780,
	1617,
	3780,
	1189,
	3780,
	3780,
	3780,
	3780,
	883,
	1191,
	2075,
	5995,
	6206,
	6217,
	4685,
	6210,
	6210,
	4686,
	4686,
	5693,
	5693,
	6057,
	6210,
	5692,
	4682,
	4289,
	3985,
	6210,
	6210,
	5693,
	5693,
	5158,
	6017,
	5695,
	4406,
	5161,
	5691,
	5691,
	4838,
	5335,
	4474,
	4291,
	6210,
	5302,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5693,
	5163,
	5693,
	5693,
	5693,
	5693,
	5693,
	4292,
	6017,
	4687,
	4836,
	5160,
	5160,
	5693,
	5693,
	6057,
	6210,
	5333,
	5693,
	5484,
	4407,
	5692,
	6017,
	3983,
	3918,
	3895,
	4685,
	3895,
	3849,
	5693,
	3895,
	3918,
	3983,
	3895,
	4290,
	3983,
	5164,
	5164,
	3930,
	5297,
	6015,
	6015,
	6166,
	6299,
	6210,
	6273,
	6210,
	5693,
	6271,
	6299,
	6214,
	5788,
	6299,
	6271,
	5779,
	1540,
	912,
	3723,
	3723,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3723,
	3038,
	3780,
	3780,
	3780,
	3723,
	3749,
	3060,
	3749,
	3060,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3749,
	3038,
	3038,
	3780,
	3780,
	3723,
	3723,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3749,
	3060,
	3723,
	3038,
	3038,
	3780,
	3780,
	3780,
	3780,
	1249,
	1241,
	3780,
	3780,
	3062,
	3780,
	6299,
	3723,
	3723,
	3723,
	3038,
	3038,
	3038,
	3038,
	3038,
	3780,
	3780,
	3062,
	3062,
	2602,
	126,
	3021,
	861,
	622,
	478,
	3038,
	3021,
	3780,
	3780,
	3780,
	3780,
	6299,
	1614,
	1617,
	430,
	3038,
	923,
	6299,
	3780,
	1035,
	3749,
	3723,
	3723,
	3723,
	3723,
	3723,
	3780,
	1103,
	3038,
	579,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	2546,
	6299,
	3780,
	977,
	3723,
	3021,
	3723,
	3723,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3749,
	3723,
	3723,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3705,
	3749,
	3723,
	3723,
	2196,
	2196,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3749,
	3749,
	3753,
	3753,
	3780,
	3038,
	3780,
	3780,
	3780,
	1488,
	2148,
	2715,
	2213,
	2213,
	2213,
	3723,
	2546,
	3021,
	3780,
	3749,
	3822,
	3780,
	3723,
	3723,
	3723,
	3021,
	3780,
	3749,
	3821,
	3780,
	3723,
	3723,
	3723,
	3021,
	3780,
	3749,
	3820,
	3780,
	3723,
	3723,
	3723,
	3038,
	3723,
	3038,
	3723,
	3780,
	3780,
	3060,
	3060,
	879,
	1612,
	1612,
	127,
	2713,
	805,
	210,
	3723,
	3780,
	3780,
	1217,
	3780,
	934,
	3038,
	3038,
	3717,
	3780,
	6312,
	1663,
	6278,
	3749,
	240,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3038,
	3723,
	3780,
	3780,
	3780,
	3038,
	3780,
	3780,
	3038,
	3038,
	3723,
	3717,
	3780,
	3780,
	3780,
	3780,
	1617,
	1617,
	3780,
	879,
	879,
	3038,
	3780,
	6299,
	3780,
	1617,
	1617,
	3780,
	3717,
	3780,
	1617,
	3780,
	879,
	3038,
	3780,
	3723,
	3780,
	3780,
	3780,
	3780,
	1617,
	1092,
	1617,
	3780,
	3705,
	3705,
	3749,
	2060,
	2750,
	2223,
	540,
	2708,
	1868,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3723,
	3749,
	3723,
	3038,
	3749,
	3060,
	3705,
	3705,
	3749,
	3749,
	3749,
	3717,
	3717,
	3723,
	3723,
	3723,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	3780,
	1162,
	1538,
	1662,
	3021,
	2546,
	2546,
	2546,
	3060,
	3060,
	3780,
	3021,
	2192,
	3780,
	2060,
	2750,
	2223,
	540,
	2708,
	1868,
	3021,
	3021,
	3062,
	2707,
	1661,
	1661,
	1612,
	3780,
	3780,
	3038,
	3780,
	3780,
	3038,
	3038,
	3780,
	3062,
	3062,
	3062,
	3062,
	3780,
	3060,
	3780,
	3723,
	3038,
	1614,
	3038,
	707,
	3038,
	1614,
	1623,
	435,
	3038,
	1614,
	1617,
	430,
	3038,
	1101,
	3038,
	3780,
	5203,
	5203,
	5209,
	5209,
	5205,
	2195,
	3780,
	3780,
	3780,
	5832,
	5841,
	5837,
	5842,
	4973,
	4716,
	3995,
	3992,
	4713,
	3917,
	3982,
	5800,
	4972,
	5208,
	4711,
	6243,
	6241,
	6237,
	6174,
	5196,
	6229,
	5827,
	6230,
	5201,
	4387,
	3994,
	4712,
	5200,
	4712,
	5200,
	4619,
	4969,
	6173,
	6233,
	-1,
	6299,
	948,
	620,
	5,
	5,
	620,
	5,
	5,
	3705,
	3780,
	3780,
	3780,
	3780,
	579,
	3038,
	3038,
	580,
	580,
	1617,
	1617,
	3062,
	653,
	3780,
	3021,
	3780,
	3021,
	48,
	3189,
	3189,
	2137,
	620,
	5,
	4715,
	5,
	620,
	5,
	6,
	5,
	620,
	5,
	5,
	3864,
	-1,
	-1,
	-1,
	620,
	3780,
	620,
	620,
	5,
	5,
	620,
	5,
	3,
	5,
	5,
	3038,
	3723,
	3038,
	3038,
	887,
	1079,
	391,
	1079,
	3021,
	3038,
	3723,
	3038,
	3038,
	163,
	391,
	1079,
	3021,
	3021,
	3038,
	3723,
	3038,
	3038,
	163,
	391,
	1079,
	3021,
	3021,
	3705,
	3060,
	3749,
	3723,
	3723,
	3723,
	1079,
	391,
	1079,
	3780,
	3021,
	3705,
	4287,
	4287,
	3780,
	1079,
	391,
	3780,
	3723,
	3038,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3038,
	3723,
	3038,
	3038,
	261,
	391,
	1079,
	3021,
	3021,
	3780,
	3038,
	3723,
	3038,
	3705,
	3038,
	1079,
	391,
	1079,
	3021,
	3780,
	967,
	3038,
	3723,
	3038,
	3705,
	3038,
	1079,
	391,
	1079,
	3780,
	967,
	3038,
	3723,
	3780,
	3038,
	391,
	391,
	1079,
	2173,
	2173,
	1079,
	1079,
	2173,
	2173,
	2173,
	3021,
	3021,
	3021,
	3021,
	3021,
	3021,
	3021,
	3038,
	1079,
	391,
	1079,
	1079,
	2173,
	2173,
	2173,
	3021,
	3021,
	3021,
	3021,
	3021,
	3021,
	3060,
	1244,
	3060,
	1244,
	3038,
	3723,
	3038,
	3038,
	261,
	391,
	1079,
	3021,
	3021,
	3021,
	3038,
	3723,
	3038,
	3705,
	3038,
	2958,
	1079,
	391,
	1079,
	3021,
	3021,
	967,
	3038,
	3723,
	3038,
	3705,
	3038,
	2958,
	1079,
	391,
	1079,
	3021,
	967,
	3038,
	3723,
	3038,
	3038,
	96,
	391,
	1079,
	3780,
	3780,
	3038,
	3723,
	3038,
	3038,
	9,
	3780,
	1079,
	391,
	1079,
	3780,
	3857,
	3021,
	3021,
	3021,
	3038,
	3723,
	3038,
	3038,
	96,
	391,
	1079,
	3021,
	3021,
	3038,
	3723,
	3038,
	3038,
	579,
	391,
	1079,
	3780,
	3780,
	3038,
	3723,
	3038,
	3038,
	96,
	391,
	1079,
	3021,
	3021,
	3038,
	3723,
	3038,
	3038,
	261,
	391,
	1079,
	3021,
	3021,
	3038,
	3723,
	3038,
	3038,
	96,
	391,
	1079,
	3021,
	3021,
	3804,
	3804,
	1672,
	618,
	945,
	947,
	3753,
	3753,
	1671,
	947,
	2930,
	3119,
	3119,
	2930,
	2930,
	1852,
	1852,
	1127,
	801,
	950,
	5237,
	3652,
	279,
	3189,
	3189,
	3189,
	3189,
	3189,
	3189,
	3189,
	3189,
	2976,
	1674,
	3705,
	2493,
	2570,
	3705,
	5461,
	5461,
	5241,
	949,
	2964,
	621,
	3021,
	3780,
	1488,
	841,
	3021,
	3780,
	-1,
	2546,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3705,
	2060,
	1007,
	1488,
	3705,
	2060,
	1007,
	1488,
	3705,
	2060,
	1007,
	1488,
	3705,
	2060,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3780,
	900,
	1081,
	108,
	392,
	3038,
	3780,
	3021,
	3780,
	3021,
	1434,
	843,
	1434,
	104,
	843,
	3021,
	3705,
	2060,
	3723,
	2020,
	3753,
	3753,
	3753,
	3119,
	169,
	169,
	1329,
	213,
	806,
	623,
	131,
	620,
	11,
	620,
	11,
	620,
	11,
	3021,
	1852,
	132,
	3021,
	3021,
	3021,
	3021,
	3021,
	3723,
	3705,
	3705,
	3658,
	3652,
	3652,
	3038,
	3780,
	3780,
	3780,
	3780,
	942,
	615,
	922,
	3705,
	879,
	1007,
	1622,
	1612,
	639,
	2173,
	1329,
	3780,
	2060,
	1612,
	1612,
	3140,
	1434,
	3038,
	3038,
	2192,
	3038,
	2199,
	3710,
	1079,
	1079,
	722,
	1079,
	391,
	588,
	261,
	888,
	3705,
	3038,
	-1,
	3780,
	3021,
	3021,
	3021,
	3780,
	3021,
	3021,
	3021,
	3705,
	3062,
	579,
	3038,
	3038,
	580,
	580,
	1617,
	1617,
	887,
	163,
	163,
	261,
	3705,
	3723,
	3060,
	3749,
	3780,
	3021,
	3705,
	3705,
	3723,
	3705,
	261,
	96,
	9,
	3780,
	96,
	579,
	96,
	261,
	96,
	1101,
	3038,
	3780,
	3780,
	-1,
	-1,
	-1,
	3780,
	942,
	615,
	922,
	3038,
	1612,
	261,
	3038,
	2192,
	3038,
	2060,
	1612,
	1612,
	1434,
	2199,
	722,
	588,
	3705,
	879,
	1007,
	1622,
	3140,
	1329,
	3780,
	3705,
	3038,
	888,
	3780,
	1101,
	3038,
	3780,
	3780,
	942,
	615,
	922,
	3705,
	879,
	1007,
	1622,
	3038,
	3038,
	1612,
	3780,
	1329,
	3140,
	2060,
	1612,
	1612,
	1434,
	2192,
	3038,
	2199,
	722,
	588,
	261,
	3705,
	3038,
	888,
	3780,
	3780,
	3038,
	887,
	3038,
	3723,
	3038,
	3038,
	163,
	3038,
	3723,
	3038,
	3038,
	163,
	3038,
	3723,
	3038,
	3038,
	261,
	3038,
	3723,
	3038,
	3038,
	261,
	3038,
	3723,
	3038,
	3707,
	3705,
	3723,
	3060,
	3749,
	1612,
	3780,
	3021,
	3705,
	3723,
	3038,
	3723,
	3705,
	1612,
	3723,
	3038,
	3705,
	3038,
	96,
	3038,
	3723,
	3038,
	3038,
	9,
	3780,
	3038,
	3723,
	3038,
	3038,
	96,
	3038,
	3723,
	3038,
	3038,
	579,
	3038,
	3723,
	3038,
	3038,
	96,
	3038,
	3723,
	3038,
	3038,
	261,
	3038,
	3723,
	3038,
	3038,
	96,
	3038,
	3723,
	3038,
	1101,
	3038,
	3780,
	3780,
	3705,
	3780,
	3780,
	3780,
	3780,
	3062,
	579,
	3038,
	3038,
	580,
	580,
	1617,
	1617,
	3780,
	2194,
	3780,
	3780,
	3780,
	3707,
	3023,
	3780,
	942,
	615,
	922,
	3705,
	879,
	1007,
	1622,
	3038,
	3038,
	1612,
	3780,
	1329,
	3140,
	2060,
	1612,
	1612,
	1434,
	2192,
	3038,
	2199,
	722,
	588,
	261,
	3705,
	3038,
	888,
	3780,
	2708,
	1517,
	2216,
	3705,
	3723,
	3038,
	560,
	1502,
	3780,
	1488,
	1617,
	3038,
	3038,
	3705,
	3723,
	3038,
	1617,
	1666,
	3780,
	1502,
	1488,
	3038,
	3038,
	3705,
	3723,
	3038,
	1658,
	3780,
	1502,
	1488,
	1617,
	3038,
	3038,
	3705,
	3723,
	3038,
	593,
	3780,
	1502,
	1488,
	1617,
	3038,
	3038,
	3705,
	3705,
	3021,
	3705,
	3021,
	3705,
	3723,
	3038,
	3038,
	1617,
	2546,
	2546,
	3780,
	3780,
	1502,
	1488,
	3705,
	3705,
	3021,
	3705,
	3021,
	3705,
	3723,
	1617,
	1488,
	1502,
	3038,
	3038,
	1612,
	1488,
	3780,
	3780,
	2060,
	2546,
	2546,
	2546,
	3780,
	3021,
	1488,
	3780,
	3705,
	3723,
	3021,
	1659,
	3780,
	2708,
	1517,
	2216,
	1502,
	1612,
	1488,
	1617,
	3038,
	3038,
	3705,
	3723,
	3038,
	81,
	3780,
	1502,
	1488,
	1617,
	3038,
	3038,
	3705,
	3723,
	3038,
	1621,
	3780,
	1502,
	1488,
	1488,
	1617,
	3038,
	3038,
	3780,
	3705,
	3723,
	3038,
	82,
	3780,
	1502,
	1488,
	1617,
	3038,
	3038,
	3705,
	3723,
	3038,
	612,
	3780,
	2708,
	1517,
	2216,
	1502,
	1488,
	1617,
	3038,
	3038,
	3705,
	3723,
	3038,
	930,
	3780,
	1502,
	1488,
	1617,
	3038,
	3038,
	1650,
	3705,
	3723,
	3038,
	1623,
	3780,
	1502,
	1488,
	1617,
	3038,
	3038,
	1650,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	2196,
	3780,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	2196,
	3780,
	3749,
	3060,
	1249,
	1246,
	2196,
	3780,
	3749,
	3060,
	3753,
	3062,
	2196,
	3780,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	2196,
	3780,
	2196,
	3780,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	3753,
	3062,
	2196,
	3780,
	3749,
	3060,
	1249,
	1241,
	2196,
	3780,
	3749,
	3060,
	1249,
	2196,
	3780,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	2196,
	3780,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	2196,
	3780,
	3610,
	2192,
	3705,
	3780,
	2570,
	3749,
	3705,
	3705,
	3780,
	1617,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3753,
	3062,
	883,
	5395,
	3038,
	3038,
	3705,
	3705,
	3749,
	3749,
	2546,
	3021,
	2546,
	2546,
	3749,
	3780,
	3650,
	3723,
	2192,
	2060,
	2750,
	2223,
	540,
	2708,
	1868,
	3780,
	3723,
	3780,
	704,
	1105,
	1168,
	767,
	3060,
	766,
	488,
	1617,
	1488,
	1332,
	3780,
	3038,
	3780,
	3723,
	3780,
	1614,
	3038,
	707,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3723,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3723,
	3038,
	3705,
	2546,
	3780,
	1617,
	3749,
	1617,
	3749,
	1617,
	3749,
	1617,
	3780,
	3749,
	3780,
	1617,
	3749,
	1617,
	3749,
	1617,
	3749,
	888,
	3780,
	3749,
	3780,
	1617,
	3780,
	3749,
	3780,
	3780,
	3749,
	3780,
	1617,
	3749,
	1617,
	3780,
	3749,
	3780,
	3038,
	3723,
	3038,
	3723,
	3780,
	2187,
	3780,
	3780,
	3038,
	3723,
	3780,
	2187,
	3780,
	3780,
	3038,
	3723,
	3021,
	3705,
	3062,
	3753,
	3723,
	3723,
	3707,
	3723,
	3780,
	2187,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	-1,
	-1,
	-1,
	-1,
	-1,
	3021,
	3021,
	3021,
	6278,
	3780,
	3780,
	3780,
	3038,
	3038,
	3723,
	3723,
	3723,
	2196,
	3038,
	2196,
	3038,
	2196,
	3038,
	2196,
	3038,
	3038,
	3038,
	3038,
	3780,
	1623,
	3062,
	3038,
	3780,
	3723,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3749,
	3650,
	3753,
	3038,
	3723,
	3780,
	3723,
	2192,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	1612,
	1488,
	3780,
	2196,
	3038,
	3780,
	554,
	3643,
	1612,
	136,
	3780,
	2196,
	3038,
	3780,
	6299,
	3780,
	1896,
	1612,
	1488,
	3780,
	2196,
	3038,
	3780,
	3780,
	3062,
	3062,
	1664,
	3780,
	3780,
	3062,
	3062,
	1664,
	3780,
	3780,
	3780,
	3062,
	1664,
	3780,
	142,
	3643,
	1612,
	136,
	3780,
	2196,
	3038,
	3780,
	6299,
	3780,
	2735,
	3957,
	5525,
	3780,
	6299,
	6299,
	3780,
	2735,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3088,
	2713,
	2753,
	2112,
	3778,
	3778,
	1667,
	3088,
	3088,
	2955,
	827,
	940,
	1621,
	4880,
	3936,
	4118,
	4119,
	3968,
	3780,
	5527,
	1488,
	3643,
	1655,
	1621,
	1621,
	3038,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	852,
	3780,
	1488,
	3780,
	1488,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1488,
	3780,
	1488,
	849,
	1488,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	3780,
	1488,
	2750,
	1538,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1488,
	2060,
	1488,
	144,
	3705,
	846,
	1006,
	545,
	3021,
	3777,
	3705,
	1623,
	639,
	545,
	2708,
	639,
	2749,
	629,
	2192,
	2754,
	2442,
	762,
	215,
	1097,
	3780,
	3723,
	4582,
	4095,
	6299,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3038,
	803,
	810,
	3723,
	855,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3038,
	952,
	3226,
	1680,
	2693,
	3753,
	938,
	2693,
	2163,
	6299,
	3780,
	2714,
	3780,
	2693,
	2714,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	1615,
	3780,
	3780,
	3780,
	3780,
	3038,
	3780,
	6299,
	3723,
	3723,
	3723,
	3038,
	3780,
	3780,
	3780,
	3038,
	3780,
	3780,
	6299,
	3723,
	3038,
	3780,
	888,
	3780,
	6299,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3038,
	3723,
	3749,
	3723,
	3021,
	3705,
	3752,
	3650,
	3749,
	3753,
	3705,
	3705,
	3705,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3021,
	3780,
	3021,
	3038,
	3038,
	2570,
	2570,
	3780,
	3210,
	2192,
	3780,
	3780,
	3780,
	3780,
	3780,
	3062,
	3780,
	2199,
	722,
	3062,
	1650,
	3780,
	888,
	1107,
	211,
	426,
	3780,
	6299,
	3780,
	3780,
	1612,
	1614,
	3038,
	707,
	3038,
	1614,
	1623,
	435,
	3038,
	1614,
	1617,
	430,
	3038,
	6299,
	3780,
	2570,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3723,
	3780,
	3780,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3780,
	3780,
	3780,
	3062,
	920,
	3062,
	1650,
	3780,
	6299,
	3780,
	3780,
	3753,
	3062,
	3038,
	6299,
	1650,
	3780,
	3780,
	1650,
	3723,
	3038,
	3038,
	3723,
	3038,
	3749,
	3060,
	1621,
	3723,
	3749,
	3749,
	3780,
	6113,
	2196,
	3780,
	3780,
	1623,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	5775,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	1623,
	2711,
	3038,
	3780,
	3038,
	3780,
	3780,
	4826,
	3780,
	3780,
	3780,
	1020,
	1020,
	1612,
	1617,
	3780,
	3038,
	1065,
	3780,
	3723,
	3723,
	3038,
	3723,
	3038,
	3749,
	3705,
	3021,
	3749,
	3060,
	3753,
	3062,
	3753,
	3062,
	3780,
	3780,
	3780,
	1617,
	1623,
	1623,
	3780,
	3021,
	3021,
	3780,
	3062,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	1538,
	3780,
	3780,
	3780,
	3780,
	3780,
	6299,
	6299,
	5788,
	6299,
	3780,
	3780,
	3780,
	3038,
	3723,
	3038,
	3723,
	3705,
	3723,
	3038,
	3038,
	3780,
	3780,
	1007,
	3021,
	3780,
	1617,
	1617,
	3038,
	3038,
	3780,
	3780,
	1488,
	4709,
	5193,
	-1,
	-1,
	-1,
	-1,
	-1,
	6137,
	5233,
	6015,
	5194,
	4120,
	5527,
	5370,
	6165,
	5371,
	4121,
	4116,
	4620,
	6168,
	6168,
	6024,
	4274,
	4970,
	6169,
	5409,
	4970,
	6109,
	4274,
	4274,
	4618,
	6169,
	5395,
	5297,
	6015,
	6015,
	5297,
	6015,
	6015,
	5166,
	6191,
	5557,
	6187,
	6188,
	3924,
	6299,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3723,
	3723,
	4385,
	5194,
	3872,
	5194,
	3873,
	3780,
	3780,
	3780,
	1617,
	3780,
};
static const Il2CppTokenRangePair s_rgctxIndices[28] = 
{
	{ 0x0200006F, { 9, 11 } },
	{ 0x020000C2, { 22, 1 } },
	{ 0x020000C4, { 25, 1 } },
	{ 0x020000C5, { 26, 4 } },
	{ 0x020000C6, { 30, 8 } },
	{ 0x020000CB, { 38, 21 } },
	{ 0x020000CC, { 59, 4 } },
	{ 0x020000DF, { 63, 3 } },
	{ 0x020000FE, { 66, 7 } },
	{ 0x02000145, { 73, 17 } },
	{ 0x02000198, { 90, 24 } },
	{ 0x02000199, { 118, 3 } },
	{ 0x020001A4, { 121, 8 } },
	{ 0x020001A5, { 129, 1 } },
	{ 0x020001B0, { 130, 13 } },
	{ 0x020001B1, { 143, 6 } },
	{ 0x06000280, { 0, 4 } },
	{ 0x060002AB, { 4, 3 } },
	{ 0x060002AC, { 7, 1 } },
	{ 0x060002AD, { 8, 1 } },
	{ 0x060003CF, { 20, 2 } },
	{ 0x060003D2, { 23, 2 } },
	{ 0x0600079E, { 114, 1 } },
	{ 0x0600079F, { 115, 1 } },
	{ 0x060007A0, { 116, 2 } },
	{ 0x0600098A, { 149, 2 } },
	{ 0x0600098B, { 151, 1 } },
	{ 0x0600098C, { 152, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[153] = 
{
	{ (Il2CppRGCTXDataType)3, 27470 },
	{ (Il2CppRGCTXDataType)3, 26718 },
	{ (Il2CppRGCTXDataType)3, 16851 },
	{ (Il2CppRGCTXDataType)3, 16850 },
	{ (Il2CppRGCTXDataType)2, 63 },
	{ (Il2CppRGCTXDataType)3, 25900 },
	{ (Il2CppRGCTXDataType)3, 25888 },
	{ (Il2CppRGCTXDataType)2, 64 },
	{ (Il2CppRGCTXDataType)2, 65 },
	{ (Il2CppRGCTXDataType)2, 2659 },
	{ (Il2CppRGCTXDataType)3, 12172 },
	{ (Il2CppRGCTXDataType)3, 12173 },
	{ (Il2CppRGCTXDataType)3, 6057 },
	{ (Il2CppRGCTXDataType)2, 368 },
	{ (Il2CppRGCTXDataType)3, 6056 },
	{ (Il2CppRGCTXDataType)2, 1438 },
	{ (Il2CppRGCTXDataType)3, 12174 },
	{ (Il2CppRGCTXDataType)3, 12175 },
	{ (Il2CppRGCTXDataType)3, 1806 },
	{ (Il2CppRGCTXDataType)3, 1805 },
	{ (Il2CppRGCTXDataType)2, 83 },
	{ (Il2CppRGCTXDataType)3, 26339 },
	{ (Il2CppRGCTXDataType)2, 540 },
	{ (Il2CppRGCTXDataType)3, 26342 },
	{ (Il2CppRGCTXDataType)3, 26364 },
	{ (Il2CppRGCTXDataType)2, 542 },
	{ (Il2CppRGCTXDataType)3, 15142 },
	{ (Il2CppRGCTXDataType)2, 543 },
	{ (Il2CppRGCTXDataType)3, 15143 },
	{ (Il2CppRGCTXDataType)3, 15144 },
	{ (Il2CppRGCTXDataType)3, 15146 },
	{ (Il2CppRGCTXDataType)3, 20979 },
	{ (Il2CppRGCTXDataType)2, 3409 },
	{ (Il2CppRGCTXDataType)3, 26732 },
	{ (Il2CppRGCTXDataType)3, 26774 },
	{ (Il2CppRGCTXDataType)3, 15145 },
	{ (Il2CppRGCTXDataType)2, 544 },
	{ (Il2CppRGCTXDataType)3, 15147 },
	{ (Il2CppRGCTXDataType)2, 3185 },
	{ (Il2CppRGCTXDataType)3, 16860 },
	{ (Il2CppRGCTXDataType)3, 16866 },
	{ (Il2CppRGCTXDataType)3, 16862 },
	{ (Il2CppRGCTXDataType)3, 16863 },
	{ (Il2CppRGCTXDataType)2, 1084 },
	{ (Il2CppRGCTXDataType)3, 1992 },
	{ (Il2CppRGCTXDataType)3, 16861 },
	{ (Il2CppRGCTXDataType)3, 16937 },
	{ (Il2CppRGCTXDataType)3, 16865 },
	{ (Il2CppRGCTXDataType)3, 1996 },
	{ (Il2CppRGCTXDataType)3, 1995 },
	{ (Il2CppRGCTXDataType)3, 16935 },
	{ (Il2CppRGCTXDataType)3, 16864 },
	{ (Il2CppRGCTXDataType)3, 16934 },
	{ (Il2CppRGCTXDataType)2, 3191 },
	{ (Il2CppRGCTXDataType)3, 16938 },
	{ (Il2CppRGCTXDataType)3, 1994 },
	{ (Il2CppRGCTXDataType)3, 16867 },
	{ (Il2CppRGCTXDataType)3, 16936 },
	{ (Il2CppRGCTXDataType)3, 1993 },
	{ (Il2CppRGCTXDataType)3, 27356 },
	{ (Il2CppRGCTXDataType)3, 27102 },
	{ (Il2CppRGCTXDataType)3, 27104 },
	{ (Il2CppRGCTXDataType)3, 27108 },
	{ (Il2CppRGCTXDataType)3, 16955 },
	{ (Il2CppRGCTXDataType)3, 16954 },
	{ (Il2CppRGCTXDataType)3, 15130 },
	{ (Il2CppRGCTXDataType)2, 2663 },
	{ (Il2CppRGCTXDataType)3, 12185 },
	{ (Il2CppRGCTXDataType)3, 22953 },
	{ (Il2CppRGCTXDataType)3, 12186 },
	{ (Il2CppRGCTXDataType)3, 12187 },
	{ (Il2CppRGCTXDataType)3, 12188 },
	{ (Il2CppRGCTXDataType)2, 431 },
	{ (Il2CppRGCTXDataType)2, 3260 },
	{ (Il2CppRGCTXDataType)3, 17289 },
	{ (Il2CppRGCTXDataType)3, 17288 },
	{ (Il2CppRGCTXDataType)3, 17287 },
	{ (Il2CppRGCTXDataType)3, 12214 },
	{ (Il2CppRGCTXDataType)2, 456 },
	{ (Il2CppRGCTXDataType)3, 12213 },
	{ (Il2CppRGCTXDataType)3, 12211 },
	{ (Il2CppRGCTXDataType)3, 6064 },
	{ (Il2CppRGCTXDataType)3, 6063 },
	{ (Il2CppRGCTXDataType)2, 1444 },
	{ (Il2CppRGCTXDataType)3, 17290 },
	{ (Il2CppRGCTXDataType)3, 12210 },
	{ (Il2CppRGCTXDataType)3, 12209 },
	{ (Il2CppRGCTXDataType)3, 12212 },
	{ (Il2CppRGCTXDataType)2, 2667 },
	{ (Il2CppRGCTXDataType)3, 12208 },
	{ (Il2CppRGCTXDataType)3, 17480 },
	{ (Il2CppRGCTXDataType)3, 17478 },
	{ (Il2CppRGCTXDataType)3, 27436 },
	{ (Il2CppRGCTXDataType)3, 27769 },
	{ (Il2CppRGCTXDataType)3, 17474 },
	{ (Il2CppRGCTXDataType)3, 26036 },
	{ (Il2CppRGCTXDataType)3, 17479 },
	{ (Il2CppRGCTXDataType)3, 17485 },
	{ (Il2CppRGCTXDataType)2, 4250 },
	{ (Il2CppRGCTXDataType)3, 17482 },
	{ (Il2CppRGCTXDataType)3, 27498 },
	{ (Il2CppRGCTXDataType)3, 17484 },
	{ (Il2CppRGCTXDataType)3, 17477 },
	{ (Il2CppRGCTXDataType)3, 27118 },
	{ (Il2CppRGCTXDataType)3, 17486 },
	{ (Il2CppRGCTXDataType)2, 1791 },
	{ (Il2CppRGCTXDataType)2, 1996 },
	{ (Il2CppRGCTXDataType)2, 2112 },
	{ (Il2CppRGCTXDataType)3, 17476 },
	{ (Il2CppRGCTXDataType)3, 17483 },
	{ (Il2CppRGCTXDataType)2, 459 },
	{ (Il2CppRGCTXDataType)2, 799 },
	{ (Il2CppRGCTXDataType)3, 92 },
	{ (Il2CppRGCTXDataType)3, 17481 },
	{ (Il2CppRGCTXDataType)3, 17475 },
	{ (Il2CppRGCTXDataType)3, 26552 },
	{ (Il2CppRGCTXDataType)3, 17473 },
	{ (Il2CppRGCTXDataType)3, 26035 },
	{ (Il2CppRGCTXDataType)3, 17487 },
	{ (Il2CppRGCTXDataType)3, 17488 },
	{ (Il2CppRGCTXDataType)2, 587 },
	{ (Il2CppRGCTXDataType)2, 797 },
	{ (Il2CppRGCTXDataType)3, 72 },
	{ (Il2CppRGCTXDataType)3, 17436 },
	{ (Il2CppRGCTXDataType)3, 17435 },
	{ (Il2CppRGCTXDataType)2, 458 },
	{ (Il2CppRGCTXDataType)3, 23087 },
	{ (Il2CppRGCTXDataType)3, 25664 },
	{ (Il2CppRGCTXDataType)2, 4248 },
	{ (Il2CppRGCTXDataType)2, 585 },
	{ (Il2CppRGCTXDataType)2, 2669 },
	{ (Il2CppRGCTXDataType)3, 12215 },
	{ (Il2CppRGCTXDataType)3, 12216 },
	{ (Il2CppRGCTXDataType)3, 12219 },
	{ (Il2CppRGCTXDataType)3, 12220 },
	{ (Il2CppRGCTXDataType)2, 468 },
	{ (Il2CppRGCTXDataType)2, 1819 },
	{ (Il2CppRGCTXDataType)3, 9582 },
	{ (Il2CppRGCTXDataType)3, 12221 },
	{ (Il2CppRGCTXDataType)3, 12218 },
	{ (Il2CppRGCTXDataType)2, 800 },
	{ (Il2CppRGCTXDataType)3, 261 },
	{ (Il2CppRGCTXDataType)3, 12217 },
	{ (Il2CppRGCTXDataType)3, 12226 },
	{ (Il2CppRGCTXDataType)3, 12225 },
	{ (Il2CppRGCTXDataType)2, 591 },
	{ (Il2CppRGCTXDataType)2, 801 },
	{ (Il2CppRGCTXDataType)3, 262 },
	{ (Il2CppRGCTXDataType)3, 263 },
	{ (Il2CppRGCTXDataType)2, 1783 },
	{ (Il2CppRGCTXDataType)2, 2430 },
	{ (Il2CppRGCTXDataType)3, 26830 },
	{ (Il2CppRGCTXDataType)3, 26831 },
};
extern const CustomAttributesCacheGenerator g_Obi_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Obi_CodeGenModule;
const Il2CppCodeGenModule g_Obi_CodeGenModule = 
{
	"Obi.dll",
	2501,
	s_methodPointers,
	238,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	28,
	s_rgctxIndices,
	153,
	s_rgctxValues,
	NULL,
	g_Obi_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
