﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"


extern const RuntimeMethod* RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE_RuntimeMethod_var;
extern const RuntimeMethod* RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066_RuntimeMethod_var;
extern const RuntimeMethod* SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5_RuntimeMethod_var;
extern const RuntimeMethod* SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8_RuntimeMethod_var;
extern const RuntimeMethod* StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B_RuntimeMethod_var;
extern const RuntimeMethod* StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B_RuntimeMethod_var;



// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m34A120993044E67D397DB90FF22BBF030B5C19DC (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.IsUnmanagedAttribute::.ctor()
extern void IsUnmanagedAttribute__ctor_m05BDDBB49F005C47C815CD32668381083A1F5C43 (void);
// 0x00000004 Unity.Collections.AllocatorManager/Block Unity.Collections.AllocatorManager::AllocateBlock(T&,System.Int32,System.Int32,System.Int32)
// 0x00000005 System.Void* Unity.Collections.AllocatorManager::Allocate(T&,System.Int32,System.Int32,System.Int32)
// 0x00000006 U* Unity.Collections.AllocatorManager::Allocate(T&,U,System.Int32)
// 0x00000007 System.Void* Unity.Collections.AllocatorManager::AllocateStruct(T&,U,System.Int32)
// 0x00000008 System.Void Unity.Collections.AllocatorManager::FreeBlock(T&,Unity.Collections.AllocatorManager/Block&)
// 0x00000009 System.Void Unity.Collections.AllocatorManager::Free(T&,System.Void*,System.Int32,System.Int32,System.Int32)
// 0x0000000A System.Void Unity.Collections.AllocatorManager::Free(T&,U*,System.Int32)
// 0x0000000B System.Void Unity.Collections.AllocatorManager::Free(Unity.Collections.AllocatorManager/AllocatorHandle,System.Void*)
extern void AllocatorManager_Free_mA11A118EB0DF4B66CCE15F0F2535A47B382C7C0C (void);
// 0x0000000C System.Void Unity.Collections.AllocatorManager::Free(Unity.Collections.AllocatorManager/AllocatorHandle,T*,System.Int32)
// 0x0000000D System.Int32 Unity.Collections.AllocatorManager::allocate_block(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorManager_allocate_block_mF37B0FD92BC1BF98C2A5C765E4F2AB9532300EE7 (void);
// 0x0000000E Unity.Collections.Allocator Unity.Collections.AllocatorManager::LegacyOf(Unity.Collections.AllocatorManager/AllocatorHandle)
extern void AllocatorManager_LegacyOf_m9F8300F5AEED87F3B9FCD28C81A52B53D58B697B (void);
// 0x0000000F System.Int32 Unity.Collections.AllocatorManager::TryLegacy(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorManager_TryLegacy_m7086EEBC033BDCAB24CAC49D695180EF1BA46895 (void);
// 0x00000010 System.Int32 Unity.Collections.AllocatorManager::Try(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorManager_Try_m67E1175F6B4340CF45D552DDC90CDFE0DA1912E3 (void);
// 0x00000011 System.Boolean Unity.Collections.AllocatorManager::IsCustomAllocator(Unity.Collections.AllocatorManager/AllocatorHandle)
extern void AllocatorManager_IsCustomAllocator_mF039AC474E11E8A175A703B3285EBDC12626C6CD (void);
// 0x00000012 System.Void Unity.Collections.AllocatorManager::.cctor()
extern void AllocatorManager__cctor_m704686DC15590B3752449701AC79493E5DBB4681 (void);
// 0x00000013 System.Void Unity.Collections.AllocatorManager/TryFunction::.ctor(System.Object,System.IntPtr)
extern void TryFunction__ctor_m61B0A7506F9D0C752E09C91E3E3CC16034A41AA0 (void);
// 0x00000014 System.Int32 Unity.Collections.AllocatorManager/TryFunction::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void TryFunction_Invoke_m62DB13101BCEC040485DBD4F68E9B4B9406368DE (void);
// 0x00000015 System.IAsyncResult Unity.Collections.AllocatorManager/TryFunction::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void TryFunction_BeginInvoke_mDB5760D98471188127E578B6598BBD7D498ACCF2 (void);
// 0x00000016 System.Int32 Unity.Collections.AllocatorManager/TryFunction::EndInvoke(Unity.Collections.AllocatorManager/Block&,System.IAsyncResult)
extern void TryFunction_EndInvoke_m26A14616127F9673336FE6BB8C1202F6A7F27C44 (void);
// 0x00000017 Unity.Collections.AllocatorManager/TableEntry& Unity.Collections.AllocatorManager/AllocatorHandle::get_TableEntry()
extern void AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D (void);
// 0x00000018 System.Void Unity.Collections.AllocatorManager/AllocatorHandle::Rewind()
extern void AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060 (void);
// 0x00000019 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/AllocatorHandle::op_Implicit(Unity.Collections.Allocator)
extern void AllocatorHandle_op_Implicit_m63D8E96033A00071E8FDEC80D6956ADBE627067C (void);
// 0x0000001A System.Int32 Unity.Collections.AllocatorManager/AllocatorHandle::get_Value()
extern void AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B (void);
// 0x0000001B System.Int32 Unity.Collections.AllocatorManager/AllocatorHandle::Try(Unity.Collections.AllocatorManager/Block&)
extern void AllocatorHandle_Try_m2F88758592B176EF3A7CFDCB93599C0CE6A97148 (void);
// 0x0000001C Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/AllocatorHandle::get_Handle()
extern void AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E (void);
// 0x0000001D Unity.Collections.Allocator Unity.Collections.AllocatorManager/AllocatorHandle::get_ToAllocator()
extern void AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF (void);
// 0x0000001E System.Void Unity.Collections.AllocatorManager/AllocatorHandle::Dispose()
extern void AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1 (void);
// 0x0000001F System.Void Unity.Collections.AllocatorManager/Range::Dispose()
extern void Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42 (void);
// 0x00000020 System.Int64 Unity.Collections.AllocatorManager/Block::get_Bytes()
extern void Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA (void);
// 0x00000021 System.Int64 Unity.Collections.AllocatorManager/Block::get_AllocatedBytes()
extern void Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907 (void);
// 0x00000022 System.Int32 Unity.Collections.AllocatorManager/Block::get_Alignment()
extern void Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF (void);
// 0x00000023 System.Void Unity.Collections.AllocatorManager/Block::set_Alignment(System.Int32)
extern void Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730 (void);
// 0x00000024 System.Void Unity.Collections.AllocatorManager/Block::Dispose()
extern void Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9 (void);
// 0x00000025 System.Int32 Unity.Collections.AllocatorManager/Block::TryFree()
extern void Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1 (void);
// 0x00000026 System.Int32 Unity.Collections.AllocatorManager/IAllocator::Try(Unity.Collections.AllocatorManager/Block&)
// 0x00000027 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/IAllocator::get_Handle()
// 0x00000028 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/StackAllocator::get_Handle()
extern void StackAllocator_get_Handle_m22001B4045E018527C5B35D6715B550B6002C7ED (void);
// 0x00000029 System.Int32 Unity.Collections.AllocatorManager/StackAllocator::Try(Unity.Collections.AllocatorManager/Block&)
extern void StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D (void);
// 0x0000002A System.Int32 Unity.Collections.AllocatorManager/StackAllocator::Try(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B (void);
// 0x0000002B System.Void Unity.Collections.AllocatorManager/StackAllocator::Dispose()
extern void StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678 (void);
// 0x0000002C System.Int32 Unity.Collections.AllocatorManager/StackAllocator::Try$BurstManaged(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B (void);
// 0x0000002D System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Try_0000097CU24PostfixBurstDelegate__ctor_mAA320D634CED9F26B0CC1428BF090559BE8934E1 (void);
// 0x0000002E System.Int32 Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$PostfixBurstDelegate::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_0000097CU24PostfixBurstDelegate_Invoke_m8836609D09F0C38AB420878A7B6754115378D6D0 (void);
// 0x0000002F System.IAsyncResult Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$PostfixBurstDelegate::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void Try_0000097CU24PostfixBurstDelegate_BeginInvoke_m3685A28D448B2A4B4025CD4DB398B7619E9808E1 (void);
// 0x00000030 System.Int32 Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Try_0000097CU24PostfixBurstDelegate_EndInvoke_mF1640C598B250AF701145A142CCB88DEBC090853 (void);
// 0x00000031 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Try_0000097CU24BurstDirectCall_GetFunctionPointerDiscard_m2C0C37505D3506A20017DD7AAF0325981C13843B (void);
// 0x00000032 System.IntPtr Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$BurstDirectCall::GetFunctionPointer()
extern void Try_0000097CU24BurstDirectCall_GetFunctionPointer_mCB548E2A910C98504A3FD8F634FC38B9063687CF (void);
// 0x00000033 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$BurstDirectCall::Constructor()
extern void Try_0000097CU24BurstDirectCall_Constructor_m63CEB7C44FD76BA267FF5A11296F4A23A931877B (void);
// 0x00000034 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$BurstDirectCall::Initialize()
extern void Try_0000097CU24BurstDirectCall_Initialize_m531523A932F78ABC115A2F335D77EB9C0104CB41 (void);
// 0x00000035 System.Void Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$BurstDirectCall::.cctor()
extern void Try_0000097CU24BurstDirectCall__cctor_m6D3327B8BCEB5DEF423B1D3FC7ACDF5EE8F95ADC (void);
// 0x00000036 System.Int32 Unity.Collections.AllocatorManager/StackAllocator/Try_0000097C$BurstDirectCall::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_0000097CU24BurstDirectCall_Invoke_m2244B715FBC670D973BF5055CECCCB808D9E8AC2 (void);
// 0x00000037 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.AllocatorManager/SlabAllocator::get_Handle()
extern void SlabAllocator_get_Handle_m1BAE636499EF06990B084B49FF05100F4D70C6D7 (void);
// 0x00000038 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::get_SlabSizeInBytes()
extern void SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685 (void);
// 0x00000039 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::Try(Unity.Collections.AllocatorManager/Block&)
extern void SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3 (void);
// 0x0000003A System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::Try(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8 (void);
// 0x0000003B System.Void Unity.Collections.AllocatorManager/SlabAllocator::Dispose()
extern void SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F (void);
// 0x0000003C System.Int32 Unity.Collections.AllocatorManager/SlabAllocator::Try$BurstManaged(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5 (void);
// 0x0000003D System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Try_0000098AU24PostfixBurstDelegate__ctor_m5A93C4242680E10FC666DB9F620F784BB22F73C9 (void);
// 0x0000003E System.Int32 Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$PostfixBurstDelegate::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_0000098AU24PostfixBurstDelegate_Invoke_mA39A4D1D130F738B61D9A016E5ABBAFFC1187234 (void);
// 0x0000003F System.IAsyncResult Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$PostfixBurstDelegate::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void Try_0000098AU24PostfixBurstDelegate_BeginInvoke_mCB5CE8A6C3D8F5B55015BB18E8EBC90F5BB08624 (void);
// 0x00000040 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Try_0000098AU24PostfixBurstDelegate_EndInvoke_mD0B3C72304E48AEA0276A675BDD447A1A0EC4161 (void);
// 0x00000041 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Try_0000098AU24BurstDirectCall_GetFunctionPointerDiscard_m93981F95497ABE761434CAD6EFCECEF5AECF4C49 (void);
// 0x00000042 System.IntPtr Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$BurstDirectCall::GetFunctionPointer()
extern void Try_0000098AU24BurstDirectCall_GetFunctionPointer_mDB7B835EB018B1ABD8A9B1933B8316F875882F2B (void);
// 0x00000043 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$BurstDirectCall::Constructor()
extern void Try_0000098AU24BurstDirectCall_Constructor_m61221DAA74FDBC40942C978903CDE5E4C5A8526D (void);
// 0x00000044 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$BurstDirectCall::Initialize()
extern void Try_0000098AU24BurstDirectCall_Initialize_m5532852BC3FBCFF98707C2A1CDCD2A42C9790337 (void);
// 0x00000045 System.Void Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$BurstDirectCall::.cctor()
extern void Try_0000098AU24BurstDirectCall__cctor_mB2E65EDB9A4D5A75E69819C5626DE5D41C1F8F3B (void);
// 0x00000046 System.Int32 Unity.Collections.AllocatorManager/SlabAllocator/Try_0000098A$BurstDirectCall::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_0000098AU24BurstDirectCall_Invoke_m56FC230573AFF15A0269E4392BF9EB82975D219D (void);
// 0x00000047 T& Unity.Collections.AllocatorManager/Array32768`1::ElementAt(System.Int32)
// 0x00000048 System.Void Unity.Collections.AllocatorManager/SharedStatics/TableEntry::.cctor()
extern void TableEntry__cctor_m4E930B1CD3F5349C2EE468379393D62B7F40908F (void);
// 0x00000049 System.Void Unity.Collections.CreatePropertyAttribute::.ctor()
extern void CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68 (void);
// 0x0000004A System.Void Unity.Collections.BurstCompatibleAttribute::set_GenericTypeArguments(System.Type[])
extern void BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A (void);
// 0x0000004B System.Void Unity.Collections.BurstCompatibleAttribute::.ctor()
extern void BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C (void);
// 0x0000004C System.Void Unity.Collections.NotBurstCompatibleAttribute::.ctor()
extern void NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63 (void);
// 0x0000004D System.Int32 Unity.Collections.CollectionHelper::Log2Floor(System.Int32)
extern void CollectionHelper_Log2Floor_mB2378B012C2572398533B336D8C73AC3E1DBD0D5 (void);
// 0x0000004E System.Int32 Unity.Collections.CollectionHelper::Align(System.Int32,System.Int32)
extern void CollectionHelper_Align_m8D94703680FB74636F02BEF660C4D913597CE0F3 (void);
// 0x0000004F System.UInt32 Unity.Collections.CollectionHelper::Hash(System.Void*,System.Int32)
extern void CollectionHelper_Hash_m8132774EA770BB87BF148F3B4900F64CCA47F576 (void);
// 0x00000050 System.Boolean Unity.Collections.CollectionHelper::ShouldDeallocate(Unity.Collections.AllocatorManager/AllocatorHandle)
extern void CollectionHelper_ShouldDeallocate_m41C16802B8B5846A4C78633AA05B2FF04D733234 (void);
// 0x00000051 System.Int32 Unity.Collections.CollectionHelper::AssumePositive(System.Int32)
extern void CollectionHelper_AssumePositive_mD8785D4C9E69993993EA9982A5F6F2ADAFE5B110 (void);
// 0x00000052 Unity.Collections.NativeArray`1<T> Unity.Collections.CollectionHelper::CreateNativeArray(System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle,Unity.Collections.NativeArrayOptions)
// 0x00000053 System.Int32 Unity.Collections.FixedList::PaddingBytes()
// 0x00000054 System.Int32 Unity.Collections.FixedList32Bytes`1::get_Length()
// 0x00000055 System.Int32 Unity.Collections.FixedList32Bytes`1::get_LengthInBytes()
// 0x00000056 System.Byte* Unity.Collections.FixedList32Bytes`1::get_Buffer()
// 0x00000057 System.Int32 Unity.Collections.FixedList32Bytes`1::GetHashCode()
// 0x00000058 System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x00000059 System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000005A System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000005B System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000005C System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000005D System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000005E System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x0000005F System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000060 System.Int32 Unity.Collections.FixedList32Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000061 System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000062 System.Boolean Unity.Collections.FixedList32Bytes`1::Equals(System.Object)
// 0x00000063 System.Collections.IEnumerator Unity.Collections.FixedList32Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000064 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList32Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000065 System.Int32 Unity.Collections.FixedList64Bytes`1::get_Length()
// 0x00000066 System.Int32 Unity.Collections.FixedList64Bytes`1::get_LengthInBytes()
// 0x00000067 System.Byte* Unity.Collections.FixedList64Bytes`1::get_Buffer()
// 0x00000068 System.Int32 Unity.Collections.FixedList64Bytes`1::GetHashCode()
// 0x00000069 System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000006A System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000006B System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000006C System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000006D System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000006E System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000006F System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000070 System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000071 System.Int32 Unity.Collections.FixedList64Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000072 System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000073 System.Boolean Unity.Collections.FixedList64Bytes`1::Equals(System.Object)
// 0x00000074 System.Collections.IEnumerator Unity.Collections.FixedList64Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000075 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList64Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000076 System.Int32 Unity.Collections.FixedList128Bytes`1::get_Length()
// 0x00000077 System.Int32 Unity.Collections.FixedList128Bytes`1::get_LengthInBytes()
// 0x00000078 System.Byte* Unity.Collections.FixedList128Bytes`1::get_Buffer()
// 0x00000079 System.Int32 Unity.Collections.FixedList128Bytes`1::GetHashCode()
// 0x0000007A System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000007B System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000007C System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000007D System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000007E System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x0000007F System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000080 System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000081 System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000082 System.Int32 Unity.Collections.FixedList128Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000083 System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000084 System.Boolean Unity.Collections.FixedList128Bytes`1::Equals(System.Object)
// 0x00000085 System.Collections.IEnumerator Unity.Collections.FixedList128Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000086 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList128Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000087 System.Int32 Unity.Collections.FixedList512Bytes`1::get_Length()
// 0x00000088 System.Int32 Unity.Collections.FixedList512Bytes`1::get_LengthInBytes()
// 0x00000089 System.Byte* Unity.Collections.FixedList512Bytes`1::get_Buffer()
// 0x0000008A System.Int32 Unity.Collections.FixedList512Bytes`1::GetHashCode()
// 0x0000008B System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000008C System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000008D System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000008E System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x0000008F System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000090 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x00000091 System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000092 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x00000093 System.Int32 Unity.Collections.FixedList512Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000094 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x00000095 System.Boolean Unity.Collections.FixedList512Bytes`1::Equals(System.Object)
// 0x00000096 System.Collections.IEnumerator Unity.Collections.FixedList512Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000097 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList512Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000098 System.Int32 Unity.Collections.FixedList4096Bytes`1::get_Length()
// 0x00000099 System.Int32 Unity.Collections.FixedList4096Bytes`1::get_LengthInBytes()
// 0x0000009A System.Byte* Unity.Collections.FixedList4096Bytes`1::get_Buffer()
// 0x0000009B T Unity.Collections.FixedList4096Bytes`1::get_Item(System.Int32)
// 0x0000009C System.Void Unity.Collections.FixedList4096Bytes`1::set_Item(System.Int32,T)
// 0x0000009D System.Int32 Unity.Collections.FixedList4096Bytes`1::GetHashCode()
// 0x0000009E System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList32Bytes`1<T>)
// 0x0000009F System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList32Bytes`1<T>)
// 0x000000A0 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList64Bytes`1<T>)
// 0x000000A1 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList64Bytes`1<T>)
// 0x000000A2 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList128Bytes`1<T>)
// 0x000000A3 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList128Bytes`1<T>)
// 0x000000A4 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList512Bytes`1<T>)
// 0x000000A5 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList512Bytes`1<T>)
// 0x000000A6 System.Int32 Unity.Collections.FixedList4096Bytes`1::CompareTo(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x000000A7 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(Unity.Collections.FixedList4096Bytes`1<T>)
// 0x000000A8 System.Boolean Unity.Collections.FixedList4096Bytes`1::Equals(System.Object)
// 0x000000A9 System.Collections.IEnumerator Unity.Collections.FixedList4096Bytes`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Collections.Generic.IEnumerator`1<T> Unity.Collections.FixedList4096Bytes`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000AB System.Void* Unity.Collections.Memory/Unmanaged::Allocate(System.Int64,System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle)
extern void Unmanaged_Allocate_mE42F0479C571BE76773614A408329CFCB51FB7F5 (void);
// 0x000000AC System.Void Unity.Collections.Memory/Unmanaged::Free(System.Void*,Unity.Collections.AllocatorManager/AllocatorHandle)
extern void Unmanaged_Free_mE141FBBD8FC68C23121409BD2187EDDAB7849045 (void);
// 0x000000AD System.Void Unity.Collections.Memory/Unmanaged::Free(T*,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000AE System.Boolean Unity.Collections.Memory/Unmanaged/Array::IsCustom(Unity.Collections.AllocatorManager/AllocatorHandle)
extern void Array_IsCustom_m5553247492EBFB5ECECF39E326F1EC5C3F7FA892 (void);
// 0x000000AF System.Void* Unity.Collections.Memory/Unmanaged/Array::CustomResize(System.Void*,System.Int64,System.Int64,Unity.Collections.AllocatorManager/AllocatorHandle,System.Int64,System.Int32)
extern void Array_CustomResize_mD9B4173A8E9495BECF4359DE1B09E72428F2C521 (void);
// 0x000000B0 System.Void* Unity.Collections.Memory/Unmanaged/Array::Resize(System.Void*,System.Int64,System.Int64,Unity.Collections.AllocatorManager/AllocatorHandle,System.Int64,System.Int32)
extern void Array_Resize_m7334DEAC65445477E1D278AC2DFEBD53C06C2C1B (void);
// 0x000000B1 T* Unity.Collections.Memory/Unmanaged/Array::Resize(T*,System.Int64,System.Int64,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000B2 System.Int32 Unity.Collections.NativeArrayExtensions::IndexOf(Unity.Collections.NativeArray`1<T>,U)
// 0x000000B3 System.Int32 Unity.Collections.NativeArrayExtensions::IndexOf(System.Void*,System.Int32,U)
// 0x000000B4 Unity.Collections.NativeArray`1<U> Unity.Collections.NativeArrayExtensions::Reinterpret(Unity.Collections.NativeArray`1<T>)
// 0x000000B5 System.Void Unity.Collections.NativeArrayExtensions::Initialize(Unity.Collections.NativeArray`1<T>&,System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle,Unity.Collections.NativeArrayOptions)
// 0x000000B6 System.Void Unity.Collections.NativeHashMap`2::.ctor(System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000B7 System.Void Unity.Collections.NativeHashMap`2::.ctor(System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle,System.Int32)
// 0x000000B8 System.Void Unity.Collections.NativeHashMap`2::Clear()
// 0x000000B9 System.Boolean Unity.Collections.NativeHashMap`2::TryAdd(TKey,TValue)
// 0x000000BA System.Boolean Unity.Collections.NativeHashMap`2::Remove(TKey)
// 0x000000BB System.Boolean Unity.Collections.NativeHashMap`2::TryGetValue(TKey,TValue&)
// 0x000000BC System.Void Unity.Collections.NativeHashMap`2::Dispose()
// 0x000000BD Unity.Collections.NativeArray`1<TKey> Unity.Collections.NativeHashMap`2::GetKeyArray(Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000BE System.Collections.Generic.IEnumerator`1<Unity.Collections.LowLevel.Unsafe.KeyValue`2<TKey,TValue>> Unity.Collections.NativeHashMap`2::System.Collections.Generic.IEnumerable<Unity.Collections.LowLevel.Unsafe.KeyValue<TKey,TValue>>.GetEnumerator()
// 0x000000BF System.Collections.IEnumerator Unity.Collections.NativeHashMap`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C0 System.Int32 Unity.Collections.NativeHashMapExtensions::Unique(Unity.Collections.NativeArray`1<T>)
// 0x000000C1 System.Void Unity.Collections.NativeList`1::.ctor(Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000C2 System.Void Unity.Collections.NativeList`1::.ctor(System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000C3 System.Void Unity.Collections.NativeList`1::Initialize(System.Int32,U&,System.Int32)
// 0x000000C4 System.Void Unity.Collections.NativeList`1::.ctor(System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle,System.Int32)
// 0x000000C5 T Unity.Collections.NativeList`1::get_Item(System.Int32)
// 0x000000C6 System.Void Unity.Collections.NativeList`1::set_Item(System.Int32,T)
// 0x000000C7 System.Int32 Unity.Collections.NativeList`1::get_Length()
// 0x000000C8 System.Void Unity.Collections.NativeList`1::set_Capacity(System.Int32)
// 0x000000C9 System.Void Unity.Collections.NativeList`1::Add(T&)
// 0x000000CA System.Void Unity.Collections.NativeList`1::AddRange(System.Void*,System.Int32)
// 0x000000CB System.Void Unity.Collections.NativeList`1::RemoveAtSwapBack(System.Int32)
// 0x000000CC System.Boolean Unity.Collections.NativeList`1::get_IsCreated()
// 0x000000CD System.Void Unity.Collections.NativeList`1::Dispose()
// 0x000000CE System.Void Unity.Collections.NativeList`1::Clear()
// 0x000000CF Unity.Collections.NativeArray`1<T> Unity.Collections.NativeList`1::op_Implicit(Unity.Collections.NativeList`1<T>)
// 0x000000D0 Unity.Collections.NativeArray`1<T> Unity.Collections.NativeList`1::AsArray()
// 0x000000D1 Unity.Collections.NativeArray`1<T> Unity.Collections.NativeList`1::AsDeferredJobArray()
// 0x000000D2 System.Collections.IEnumerator Unity.Collections.NativeList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D3 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.NativeList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000D4 System.Void Unity.Collections.NativeList`1::CopyFrom(T[])
// 0x000000D5 System.Void Unity.Collections.NativeList`1::Resize(System.Int32,Unity.Collections.NativeArrayOptions)
// 0x000000D6 System.Void Unity.Collections.NativeList`1::ResizeUninitialized(System.Int32)
// 0x000000D7 Unity.Collections.NativeQueueBlockHeader* Unity.Collections.NativeQueueBlockPoolData::AllocateBlock()
extern void NativeQueueBlockPoolData_AllocateBlock_mEE2500587CBF0C5D7DF01862F5608578F3E1A41D (void);
// 0x000000D8 System.Void Unity.Collections.NativeQueueBlockPoolData::FreeBlock(Unity.Collections.NativeQueueBlockHeader*)
extern void NativeQueueBlockPoolData_FreeBlock_m79B8FBE865EF9B2477F98C29F078417436BEED0E (void);
// 0x000000D9 Unity.Collections.NativeQueueBlockPoolData* Unity.Collections.NativeQueueBlockPool::GetQueueBlockPool()
extern void NativeQueueBlockPool_GetQueueBlockPool_mF092D3178F1886254B0DE9D2605313A47E33746B (void);
// 0x000000DA System.Void Unity.Collections.NativeQueueBlockPool::AppDomainOnDomainUnload()
extern void NativeQueueBlockPool_AppDomainOnDomainUnload_m07AB4B933463598F22A818F971FDD43BD2C67A27 (void);
// 0x000000DB System.Void Unity.Collections.NativeQueueBlockPool::OnDomainUnload(System.Object,System.EventArgs)
extern void NativeQueueBlockPool_OnDomainUnload_m4071B7A24C6080006CA7F847B3B70084B50DF058 (void);
// 0x000000DC System.Void Unity.Collections.NativeQueueBlockPool::.cctor()
extern void NativeQueueBlockPool__cctor_m2A968855F74268AE053F8F37C14E73C5047F92FB (void);
// 0x000000DD Unity.Collections.NativeQueueBlockHeader* Unity.Collections.NativeQueueData::GetCurrentWriteBlockTLS(System.Int32)
extern void NativeQueueData_GetCurrentWriteBlockTLS_m75502C82FDFF25E26AA4773759A824A0D22E1411 (void);
// 0x000000DE System.Void Unity.Collections.NativeQueueData::SetCurrentWriteBlockTLS(System.Int32,Unity.Collections.NativeQueueBlockHeader*)
extern void NativeQueueData_SetCurrentWriteBlockTLS_m9C78BEF5415CD20F6265F199C04B0D59D590131B (void);
// 0x000000DF Unity.Collections.NativeQueueBlockHeader* Unity.Collections.NativeQueueData::AllocateWriteBlockMT(Unity.Collections.NativeQueueData*,Unity.Collections.NativeQueueBlockPoolData*,System.Int32)
// 0x000000E0 System.Void Unity.Collections.NativeQueueData::AllocateQueue(Unity.Collections.AllocatorManager/AllocatorHandle,Unity.Collections.NativeQueueData*&)
// 0x000000E1 System.Void Unity.Collections.NativeQueueData::DeallocateQueue(Unity.Collections.NativeQueueData*,Unity.Collections.NativeQueueBlockPoolData*,Unity.Collections.AllocatorManager/AllocatorHandle)
extern void NativeQueueData_DeallocateQueue_mFE6EAED9FB35AB70EFEA53BCBCF089EF36D5A816 (void);
// 0x000000E2 System.Void Unity.Collections.NativeQueue`1::.ctor(Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x000000E3 System.Int32 Unity.Collections.NativeQueue`1::get_Count()
// 0x000000E4 T Unity.Collections.NativeQueue`1::Dequeue()
// 0x000000E5 System.Boolean Unity.Collections.NativeQueue`1::TryDequeue(T&)
// 0x000000E6 System.Void Unity.Collections.NativeQueue`1::Dispose()
// 0x000000E7 Unity.Collections.NativeQueue`1/ParallelWriter<T> Unity.Collections.NativeQueue`1::AsParallelWriter()
// 0x000000E8 System.Void Unity.Collections.NativeQueue`1/ParallelWriter::Enqueue(T)
// 0x000000E9 System.Void Unity.Collections.NativeSortExtension::Sort(Unity.Collections.NativeArray`1<T>)
// 0x000000EA System.Void Unity.Collections.NativeSortExtension::Sort(Unity.Collections.NativeList`1<T>)
// 0x000000EB System.Void Unity.Collections.NativeSortExtension::Sort(Unity.Collections.NativeList`1<T>,U)
// 0x000000EC System.Void Unity.Collections.NativeSortExtension::Sort(Unity.Collections.NativeSlice`1<T>,U)
// 0x000000ED System.Void Unity.Collections.NativeSortExtension::IntroSort(System.Void*,System.Int32,U)
// 0x000000EE System.Void Unity.Collections.NativeSortExtension::IntroSort(System.Void*,System.Int32,System.Int32,System.Int32,U)
// 0x000000EF System.Void Unity.Collections.NativeSortExtension::InsertionSort(System.Void*,System.Int32,System.Int32,U)
// 0x000000F0 System.Int32 Unity.Collections.NativeSortExtension::Partition(System.Void*,System.Int32,System.Int32,U)
// 0x000000F1 System.Void Unity.Collections.NativeSortExtension::HeapSort(System.Void*,System.Int32,System.Int32,U)
// 0x000000F2 System.Void Unity.Collections.NativeSortExtension::Heapify(System.Void*,System.Int32,System.Int32,System.Int32,U)
// 0x000000F3 System.Void Unity.Collections.NativeSortExtension::Swap(System.Void*,System.Int32,System.Int32)
// 0x000000F4 System.Void Unity.Collections.NativeSortExtension::SwapIfGreaterWithItems(System.Void*,System.Int32,System.Int32,U)
// 0x000000F5 System.Void Unity.Collections.NativeSortExtension::IntroSortStruct(System.Void*,System.Int32,U)
// 0x000000F6 System.Void Unity.Collections.NativeSortExtension::IntroSortStruct(System.Void*,System.Int32,System.Int32,System.Int32,U)
// 0x000000F7 System.Void Unity.Collections.NativeSortExtension::InsertionSortStruct(System.Void*,System.Int32,System.Int32,U)
// 0x000000F8 System.Int32 Unity.Collections.NativeSortExtension::PartitionStruct(System.Void*,System.Int32,System.Int32,U)
// 0x000000F9 System.Void Unity.Collections.NativeSortExtension::HeapSortStruct(System.Void*,System.Int32,System.Int32,U)
// 0x000000FA System.Void Unity.Collections.NativeSortExtension::HeapifyStruct(System.Void*,System.Int32,System.Int32,System.Int32,U)
// 0x000000FB System.Void Unity.Collections.NativeSortExtension::SwapStruct(System.Void*,System.Int32,System.Int32)
// 0x000000FC System.Void Unity.Collections.NativeSortExtension::SwapIfGreaterWithItemsStruct(System.Void*,System.Int32,System.Int32,U)
// 0x000000FD System.Int32 Unity.Collections.NativeSortExtension/DefaultComparer`1::Compare(T,T)
// 0x000000FE System.Void Unity.Collections.Spinner::Lock()
extern void Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1 (void);
// 0x000000FF System.Void Unity.Collections.Spinner::Unlock()
extern void Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32 (void);
// 0x00000100 System.Void Unity.Collections.UnmanagedArray`1::Dispose()
// 0x00000101 T& Unity.Collections.UnmanagedArray`1::get_Item(System.Int32)
// 0x00000102 System.Void Unity.Collections.RewindableAllocator::Rewind()
extern void RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580 (void);
// 0x00000103 System.Void Unity.Collections.RewindableAllocator::Dispose()
extern void RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678 (void);
// 0x00000104 System.Int32 Unity.Collections.RewindableAllocator::Try(Unity.Collections.AllocatorManager/Block&)
extern void RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE (void);
// 0x00000105 System.Int32 Unity.Collections.RewindableAllocator::Try(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066 (void);
// 0x00000106 Unity.Collections.AllocatorManager/AllocatorHandle Unity.Collections.RewindableAllocator::get_Handle()
extern void RewindableAllocator_get_Handle_m056BEDCB4F2BEA70D8DB4B91FAE1C9B7A44D174F (void);
// 0x00000107 System.Int32 Unity.Collections.RewindableAllocator::Try$BurstManaged(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE (void);
// 0x00000108 System.Void Unity.Collections.RewindableAllocator/MemoryBlock::.ctor(System.Int64)
extern void MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3 (void);
// 0x00000109 System.Void Unity.Collections.RewindableAllocator/MemoryBlock::Rewind()
extern void MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958 (void);
// 0x0000010A System.Void Unity.Collections.RewindableAllocator/MemoryBlock::Dispose()
extern void MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF (void);
// 0x0000010B System.Int32 Unity.Collections.RewindableAllocator/MemoryBlock::TryAllocate(Unity.Collections.AllocatorManager/Block&)
extern void MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F (void);
// 0x0000010C System.Boolean Unity.Collections.RewindableAllocator/MemoryBlock::Contains(System.IntPtr)
extern void MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB (void);
// 0x0000010D System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Try_000006E4U24PostfixBurstDelegate__ctor_mEBA79173E3C2DAAEA8326078C999FA402F4ACC64 (void);
// 0x0000010E System.Int32 Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$PostfixBurstDelegate::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_000006E4U24PostfixBurstDelegate_Invoke_m4395B8E38DF7729CB7CBC91CB84DC6F35A799CFC (void);
// 0x0000010F System.IAsyncResult Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$PostfixBurstDelegate::BeginInvoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&,System.AsyncCallback,System.Object)
extern void Try_000006E4U24PostfixBurstDelegate_BeginInvoke_mA033F943066DE34D29BB0A17603449D75ABC3A5E (void);
// 0x00000110 System.Int32 Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Try_000006E4U24PostfixBurstDelegate_EndInvoke_m81754140E9F3202F9B8F3720FFCB4592D75CF5FD (void);
// 0x00000111 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Try_000006E4U24BurstDirectCall_GetFunctionPointerDiscard_mA84B7EC78BABDEF9453035B2DD6B5FA73560CACE (void);
// 0x00000112 System.IntPtr Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$BurstDirectCall::GetFunctionPointer()
extern void Try_000006E4U24BurstDirectCall_GetFunctionPointer_m3438AD325B9DCF1309368BD7D66E871135E96FF6 (void);
// 0x00000113 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$BurstDirectCall::Constructor()
extern void Try_000006E4U24BurstDirectCall_Constructor_m5BA546922E672DD27325F241328D72512C5F1D2F (void);
// 0x00000114 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$BurstDirectCall::Initialize()
extern void Try_000006E4U24BurstDirectCall_Initialize_m30A3D24D62B6CA3D49C9C4C41AB8AA4DAD0924BF (void);
// 0x00000115 System.Void Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$BurstDirectCall::.cctor()
extern void Try_000006E4U24BurstDirectCall__cctor_mBED8730ECF3371B5F5BCA6414221C87862757969 (void);
// 0x00000116 System.Int32 Unity.Collections.RewindableAllocator/Unity.Collections.Try_000006E4$BurstDirectCall::Invoke(System.IntPtr,Unity.Collections.AllocatorManager/Block&)
extern void Try_000006E4U24BurstDirectCall_Invoke_m2899E236ABAAE0838051D96F021EFD9C1463FF0C (void);
// 0x00000117 System.Void Unity.Collections.xxHash3::Avx2HashLongInternalLoop(System.UInt64*,System.Byte*,System.Byte*,System.Int64,System.Byte*,System.Int32)
extern void xxHash3_Avx2HashLongInternalLoop_mCCB0DF801328F7B4907CCD550BA0E9C2A971E4AF (void);
// 0x00000118 System.Void Unity.Collections.xxHash3::Avx2ScrambleAcc(System.UInt64*,System.Byte*)
extern void xxHash3_Avx2ScrambleAcc_mA6DBBF7E016635A901F80FC07FF1851618D6C3D5 (void);
// 0x00000119 System.Void Unity.Collections.xxHash3::Avx2Accumulate(System.UInt64*,System.Byte*,System.Byte*,System.Byte*,System.Int64,System.Int32)
extern void xxHash3_Avx2Accumulate_m140D43D5123949B4A98F707CE310C744F78BC35B (void);
// 0x0000011A System.Void Unity.Collections.xxHash3::Avx2Accumulate512(System.UInt64*,System.Byte*,System.Byte*,System.Byte*)
extern void xxHash3_Avx2Accumulate512_m7C23025DBFD191FE18600B12063730E6739850C7 (void);
// 0x0000011B System.UInt64 Unity.Collections.xxHash3::Hash64Long(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7 (void);
// 0x0000011C System.Void Unity.Collections.xxHash3::Hash128Long(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D (void);
// 0x0000011D Unity.Mathematics.uint4 Unity.Collections.xxHash3::ToUint4(System.UInt64,System.UInt64)
extern void xxHash3_ToUint4_m8417C90298E7430A8DCD51069984F8AE71C9FB56 (void);
// 0x0000011E System.UInt64 Unity.Collections.xxHash3::Read64LE(System.Void*)
extern void xxHash3_Read64LE_m215CF04016BB327534223E32E2D4B451DDA1AACC (void);
// 0x0000011F System.Void Unity.Collections.xxHash3::Write64LE(System.Void*,System.UInt64)
extern void xxHash3_Write64LE_mA44006BF77A500B0ADF6E3B2FC4CF238E7939E5D (void);
// 0x00000120 System.UInt64 Unity.Collections.xxHash3::Mul32To64(System.UInt32,System.UInt32)
extern void xxHash3_Mul32To64_m8E59492BD1C7B9C66F5F907D165C27255EFC5A8F (void);
// 0x00000121 System.UInt64 Unity.Collections.xxHash3::XorShift64(System.UInt64,System.Int32)
extern void xxHash3_XorShift64_m14CFB98B253542B96241F24A1BAA59BC660F10C2 (void);
// 0x00000122 System.UInt64 Unity.Collections.xxHash3::Mul128Fold64(System.UInt64,System.UInt64)
extern void xxHash3_Mul128Fold64_mE4D677D2E0DACC2DAB3E33B876928A61E4E66036 (void);
// 0x00000123 System.UInt64 Unity.Collections.xxHash3::Avalanche(System.UInt64)
extern void xxHash3_Avalanche_m8AB7E61594297A37DDBEB74555B9B6E937CD91B9 (void);
// 0x00000124 System.UInt64 Unity.Collections.xxHash3::Mix2Acc(System.UInt64,System.UInt64,System.Byte*)
extern void xxHash3_Mix2Acc_mCC33F3919D7C753AC9997592EA6D6F95D35D72C3 (void);
// 0x00000125 System.UInt64 Unity.Collections.xxHash3::MergeAcc(System.UInt64*,System.Byte*,System.UInt64)
extern void xxHash3_MergeAcc_mF5CA25B12DC5478CDA428FDDD8EDEA7B070B1CDD (void);
// 0x00000126 System.Void Unity.Collections.xxHash3::DefaultHashLongInternalLoop(System.UInt64*,System.Byte*,System.Byte*,System.Int64,System.Byte*,System.Int32)
extern void xxHash3_DefaultHashLongInternalLoop_m25D46D5E62D5A75BF6B18D459552A4F639D7B070 (void);
// 0x00000127 System.Void Unity.Collections.xxHash3::DefaultAccumulate(System.UInt64*,System.Byte*,System.Byte*,System.Byte*,System.Int64,System.Int32)
extern void xxHash3_DefaultAccumulate_m89B15352C5BBD7848BEF7D1DE2B93275637B22A9 (void);
// 0x00000128 System.Void Unity.Collections.xxHash3::DefaultAccumulate512(System.UInt64*,System.Byte*,System.Byte*,System.Byte*,System.Int32)
extern void xxHash3_DefaultAccumulate512_mE2FA5E5ACA719367800C367B7B17CFAF94F38253 (void);
// 0x00000129 System.Void Unity.Collections.xxHash3::DefaultScrambleAcc(System.UInt64*,System.Byte*)
extern void xxHash3_DefaultScrambleAcc_m5F9168BDE58B95E6C98B107B341D0D822AAAEC37 (void);
// 0x0000012A System.UInt64 Unity.Collections.xxHash3::Hash64Long$BurstManaged(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void xxHash3_Hash64LongU24BurstManaged_m9385EE12B31C1382FF320BCDBE32657220250568 (void);
// 0x0000012B System.Void Unity.Collections.xxHash3::Hash128Long$BurstManaged(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void xxHash3_Hash128LongU24BurstManaged_mCC1E11085C5906FEAF9C4151C7A410701166563D (void);
// 0x0000012C System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Hash64Long_0000071BU24PostfixBurstDelegate__ctor_m30DADBCC700DEEA88A45176EFE5359E05E57B1CD (void);
// 0x0000012D System.UInt64 Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$PostfixBurstDelegate::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void Hash64Long_0000071BU24PostfixBurstDelegate_Invoke_m6FD507538031D026575207557ED16AF8825A1BC2 (void);
// 0x0000012E System.IAsyncResult Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$PostfixBurstDelegate::BeginInvoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,System.AsyncCallback,System.Object)
extern void Hash64Long_0000071BU24PostfixBurstDelegate_BeginInvoke_m3C6C5073170122FCBBABBD69A07C65B78F9F7179 (void);
// 0x0000012F System.UInt64 Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Hash64Long_0000071BU24PostfixBurstDelegate_EndInvoke_mC271DD74FAB05AB1A499ABE1299FFE38C728544A (void);
// 0x00000130 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Hash64Long_0000071BU24BurstDirectCall_GetFunctionPointerDiscard_m86EA21E2A4B5EC643E5638EE6296418CE32FC2AA (void);
// 0x00000131 System.IntPtr Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$BurstDirectCall::GetFunctionPointer()
extern void Hash64Long_0000071BU24BurstDirectCall_GetFunctionPointer_mF12235CDD54BE26636BE1B43D6280202317BC913 (void);
// 0x00000132 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$BurstDirectCall::Constructor()
extern void Hash64Long_0000071BU24BurstDirectCall_Constructor_m31DCFDFBC7582590BE81032BADE30C208523264A (void);
// 0x00000133 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$BurstDirectCall::Initialize()
extern void Hash64Long_0000071BU24BurstDirectCall_Initialize_mEA8B497BDCAB0B50B5D724584F4F92AF82F25BED (void);
// 0x00000134 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$BurstDirectCall::.cctor()
extern void Hash64Long_0000071BU24BurstDirectCall__cctor_mA2D2256E835433B4BF0CA2E9B5D06394C8548F69 (void);
// 0x00000135 System.UInt64 Unity.Collections.xxHash3/Unity.Collections.Hash64Long_0000071B$BurstDirectCall::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*)
extern void Hash64Long_0000071BU24BurstDirectCall_Invoke_m6D13FD2119A9BE113579A8EAEF8C2097EED80934 (void);
// 0x00000136 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$PostfixBurstDelegate::.ctor(System.Object,System.IntPtr)
extern void Hash128Long_00000722U24PostfixBurstDelegate__ctor_m8B58C18C182DA819093F36B4B1B3D82A5AE3636E (void);
// 0x00000137 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$PostfixBurstDelegate::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void Hash128Long_00000722U24PostfixBurstDelegate_Invoke_m1DBD2DD67368C8A8507087CF09F22479D05FA77C (void);
// 0x00000138 System.IAsyncResult Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$PostfixBurstDelegate::BeginInvoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&,System.AsyncCallback,System.Object)
extern void Hash128Long_00000722U24PostfixBurstDelegate_BeginInvoke_mD2D4C962A7C7D04EE13C74C0B2E5FD833152024A (void);
// 0x00000139 System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$PostfixBurstDelegate::EndInvoke(System.IAsyncResult)
extern void Hash128Long_00000722U24PostfixBurstDelegate_EndInvoke_m3685C155A9FC24F3277FC18910E8E37DB97C0E6B (void);
// 0x0000013A System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$BurstDirectCall::GetFunctionPointerDiscard(System.IntPtr&)
extern void Hash128Long_00000722U24BurstDirectCall_GetFunctionPointerDiscard_mA651E5D271E4D3BF37B8E4F34D0AED96EACEA810 (void);
// 0x0000013B System.IntPtr Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$BurstDirectCall::GetFunctionPointer()
extern void Hash128Long_00000722U24BurstDirectCall_GetFunctionPointer_mA55764DA549432D4E19FCC1B46E99B650445A1B6 (void);
// 0x0000013C System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$BurstDirectCall::Constructor()
extern void Hash128Long_00000722U24BurstDirectCall_Constructor_mCA8D5DB3F6D0ACDC758DEE1503E22FFD1D203CE3 (void);
// 0x0000013D System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$BurstDirectCall::Initialize()
extern void Hash128Long_00000722U24BurstDirectCall_Initialize_mA653807BCE1131661D20040D565029B425E7899D (void);
// 0x0000013E System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$BurstDirectCall::.cctor()
extern void Hash128Long_00000722U24BurstDirectCall__cctor_m36A1FE007ADBCFE8DCC6168C0E7EA4EB77B609CA (void);
// 0x0000013F System.Void Unity.Collections.xxHash3/Unity.Collections.Hash128Long_00000722$BurstDirectCall::Invoke(System.Byte*,System.Byte*,System.Int64,System.Byte*,Unity.Mathematics.uint4&)
extern void Hash128Long_00000722U24BurstDirectCall_Invoke_m7D680C06A0D7FBC34D9EFA0783B12789787C40A3 (void);
// 0x00000140 System.Void Unity.Collections.NotBurstCompatible.Extensions::CopyFromNBC(Unity.Collections.NativeList`1<T>,T[])
// 0x00000141 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::.ctor(Unity.Collections.Allocator)
extern void UnsafeList__ctor_m5240789DAB3DD5F68C467109E4936006FA2661CF (void);
// 0x00000142 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::Dispose()
extern void UnsafeList_Dispose_m622096B7E176E917588CF25DAE72085A98401579 (void);
// 0x00000143 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::Resize(System.Int32,System.Int32,System.Int32,Unity.Collections.NativeArrayOptions)
extern void UnsafeList_Resize_m53CE313205849622B3356EBD85E428E5DE269799 (void);
// 0x00000144 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::Resize(System.Int32,Unity.Collections.NativeArrayOptions)
// 0x00000145 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::Realloc(U&,System.Int32,System.Int32,System.Int32)
// 0x00000146 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::SetCapacity(U&,System.Int32,System.Int32,System.Int32)
// 0x00000147 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::SetCapacity(System.Int32,System.Int32,System.Int32)
extern void UnsafeList_SetCapacity_mB0556B23A585A61FBF77ADFD61BC7EC2C43B7C1D (void);
// 0x00000148 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeList::IndexOf(T)
// 0x00000149 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::Add(T)
// 0x0000014A System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::RemoveRangeSwapBackWithBeginEnd(System.Int32,System.Int32,System.Int32)
extern void UnsafeList_RemoveRangeSwapBackWithBeginEnd_m27C8CE2CDB273AFC66DC30A3DA535345E7B35AC7 (void);
// 0x0000014B System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::RemoveAtSwapBack(System.Int32)
// 0x0000014C System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList::RemoveRangeSwapBackWithBeginEnd(System.Int32,System.Int32)
// 0x0000014D System.Void* Unity.Collections.LowLevel.Unsafe.NativeListUnsafeUtility::GetUnsafePtr(Unity.Collections.NativeList`1<T>)
// 0x0000014E System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::GetBucketSize(System.Int32)
extern void UnsafeHashMapData_GetBucketSize_m8D69EA245E3F2BAAF064E9BE4BBF778BB8DAE812 (void);
// 0x0000014F System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::GrowCapacity(System.Int32)
extern void UnsafeHashMapData_GrowCapacity_mC54E03209825349B27EAD2472347149FEF5E29FE (void);
// 0x00000150 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::AllocateHashMap(System.Int32,System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle,Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*&)
// 0x00000151 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::ReallocateHashMap(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,System.Int32,System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x00000152 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::DeallocateHashMap(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,Unity.Collections.AllocatorManager/AllocatorHandle)
extern void UnsafeHashMapData_DeallocateHashMap_m293BA856234D0D2A0900EF5803C0B268ED5318F7 (void);
// 0x00000153 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::CalculateDataSize(System.Int32,System.Int32,System.Int32&,System.Int32&,System.Int32&)
// 0x00000154 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::GetCount(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*)
extern void UnsafeHashMapData_GetCount_m4BC7698D560DBD99E00EBDC01664E14C66C11BCE (void);
// 0x00000155 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData::GetKeyArray(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,Unity.Collections.NativeArray`1<TKey>)
// 0x00000156 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMapBase`2::Clear(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*)
// 0x00000157 System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeHashMapBase`2::TryAdd(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,TKey,TValue,System.Boolean,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x00000158 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeHashMapBase`2::Remove(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,TKey,System.Boolean)
// 0x00000159 System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeHashMapBase`2::TryGetFirstValueAtomic(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,TKey,TValue&,Unity.Collections.NativeMultiHashMapIterator`1<TKey>&)
// 0x0000015A System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeHashMapBase`2::TryGetNextValueAtomic(Unity.Collections.LowLevel.Unsafe.UnsafeHashMapData*,TValue&,Unity.Collections.NativeMultiHashMapIterator`1<TKey>&)
// 0x0000015B System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::.ctor(System.Int32,Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x0000015C System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::Clear()
// 0x0000015D System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::TryAdd(TKey,TValue)
// 0x0000015E System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::Remove(TKey)
// 0x0000015F System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::TryGetValue(TKey,TValue&)
// 0x00000160 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::Dispose()
// 0x00000161 Unity.Collections.NativeArray`1<TKey> Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::GetKeyArray(Unity.Collections.AllocatorManager/AllocatorHandle)
// 0x00000162 System.Collections.Generic.IEnumerator`1<Unity.Collections.LowLevel.Unsafe.KeyValue`2<TKey,TValue>> Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::System.Collections.Generic.IEnumerable<Unity.Collections.LowLevel.Unsafe.KeyValue<TKey,TValue>>.GetEnumerator()
// 0x00000163 System.Collections.IEnumerator Unity.Collections.LowLevel.Unsafe.UnsafeHashMap`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000164 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeList`1::get_Length()
// 0x00000165 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeList`1::get_Capacity()
// 0x00000166 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::set_Capacity(System.Int32)
// 0x00000167 T Unity.Collections.LowLevel.Unsafe.UnsafeList`1::get_Item(System.Int32)
// 0x00000168 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::set_Item(System.Int32,T)
// 0x00000169 Unity.Collections.LowLevel.Unsafe.UnsafeList`1<T>* Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Create(System.Int32,U&,Unity.Collections.NativeArrayOptions)
// 0x0000016A System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Destroy(Unity.Collections.LowLevel.Unsafe.UnsafeList`1<T>*)
// 0x0000016B System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Dispose()
// 0x0000016C System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Clear()
// 0x0000016D System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Resize(System.Int32,Unity.Collections.NativeArrayOptions)
// 0x0000016E System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Realloc(U&,System.Int32)
// 0x0000016F System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::SetCapacity(U&,System.Int32)
// 0x00000170 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::SetCapacity(System.Int32)
// 0x00000171 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::Add(T&)
// 0x00000172 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::AddRange(System.Void*,System.Int32)
// 0x00000173 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::RemoveAtSwapBack(System.Int32)
// 0x00000174 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeList`1::RemoveRangeSwapBack(System.Int32,System.Int32)
// 0x00000175 System.Collections.IEnumerator Unity.Collections.LowLevel.Unsafe.UnsafeList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000176 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.LowLevel.Unsafe.UnsafeList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000177 System.Void $BurstDirectCallInitializer::Initialize()
extern void U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C (void);
static Il2CppMethodPointer s_methodPointers[375] = 
{
	EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D,
	IsReadOnlyAttribute__ctor_m34A120993044E67D397DB90FF22BBF030B5C19DC,
	IsUnmanagedAttribute__ctor_m05BDDBB49F005C47C815CD32668381083A1F5C43,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AllocatorManager_Free_mA11A118EB0DF4B66CCE15F0F2535A47B382C7C0C,
	NULL,
	AllocatorManager_allocate_block_mF37B0FD92BC1BF98C2A5C765E4F2AB9532300EE7,
	AllocatorManager_LegacyOf_m9F8300F5AEED87F3B9FCD28C81A52B53D58B697B,
	AllocatorManager_TryLegacy_m7086EEBC033BDCAB24CAC49D695180EF1BA46895,
	AllocatorManager_Try_m67E1175F6B4340CF45D552DDC90CDFE0DA1912E3,
	AllocatorManager_IsCustomAllocator_mF039AC474E11E8A175A703B3285EBDC12626C6CD,
	AllocatorManager__cctor_m704686DC15590B3752449701AC79493E5DBB4681,
	TryFunction__ctor_m61B0A7506F9D0C752E09C91E3E3CC16034A41AA0,
	TryFunction_Invoke_m62DB13101BCEC040485DBD4F68E9B4B9406368DE,
	TryFunction_BeginInvoke_mDB5760D98471188127E578B6598BBD7D498ACCF2,
	TryFunction_EndInvoke_m26A14616127F9673336FE6BB8C1202F6A7F27C44,
	AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D,
	AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060,
	AllocatorHandle_op_Implicit_m63D8E96033A00071E8FDEC80D6956ADBE627067C,
	AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B,
	AllocatorHandle_Try_m2F88758592B176EF3A7CFDCB93599C0CE6A97148,
	AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E,
	AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF,
	AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1,
	Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42,
	Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA,
	Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907,
	Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF,
	Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730,
	Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9,
	Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1,
	NULL,
	NULL,
	StackAllocator_get_Handle_m22001B4045E018527C5B35D6715B550B6002C7ED,
	StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D,
	StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B,
	StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678,
	StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B,
	Try_0000097CU24PostfixBurstDelegate__ctor_mAA320D634CED9F26B0CC1428BF090559BE8934E1,
	Try_0000097CU24PostfixBurstDelegate_Invoke_m8836609D09F0C38AB420878A7B6754115378D6D0,
	Try_0000097CU24PostfixBurstDelegate_BeginInvoke_m3685A28D448B2A4B4025CD4DB398B7619E9808E1,
	Try_0000097CU24PostfixBurstDelegate_EndInvoke_mF1640C598B250AF701145A142CCB88DEBC090853,
	Try_0000097CU24BurstDirectCall_GetFunctionPointerDiscard_m2C0C37505D3506A20017DD7AAF0325981C13843B,
	Try_0000097CU24BurstDirectCall_GetFunctionPointer_mCB548E2A910C98504A3FD8F634FC38B9063687CF,
	Try_0000097CU24BurstDirectCall_Constructor_m63CEB7C44FD76BA267FF5A11296F4A23A931877B,
	Try_0000097CU24BurstDirectCall_Initialize_m531523A932F78ABC115A2F335D77EB9C0104CB41,
	Try_0000097CU24BurstDirectCall__cctor_m6D3327B8BCEB5DEF423B1D3FC7ACDF5EE8F95ADC,
	Try_0000097CU24BurstDirectCall_Invoke_m2244B715FBC670D973BF5055CECCCB808D9E8AC2,
	SlabAllocator_get_Handle_m1BAE636499EF06990B084B49FF05100F4D70C6D7,
	SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685,
	SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3,
	SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8,
	SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F,
	SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5,
	Try_0000098AU24PostfixBurstDelegate__ctor_m5A93C4242680E10FC666DB9F620F784BB22F73C9,
	Try_0000098AU24PostfixBurstDelegate_Invoke_mA39A4D1D130F738B61D9A016E5ABBAFFC1187234,
	Try_0000098AU24PostfixBurstDelegate_BeginInvoke_mCB5CE8A6C3D8F5B55015BB18E8EBC90F5BB08624,
	Try_0000098AU24PostfixBurstDelegate_EndInvoke_mD0B3C72304E48AEA0276A675BDD447A1A0EC4161,
	Try_0000098AU24BurstDirectCall_GetFunctionPointerDiscard_m93981F95497ABE761434CAD6EFCECEF5AECF4C49,
	Try_0000098AU24BurstDirectCall_GetFunctionPointer_mDB7B835EB018B1ABD8A9B1933B8316F875882F2B,
	Try_0000098AU24BurstDirectCall_Constructor_m61221DAA74FDBC40942C978903CDE5E4C5A8526D,
	Try_0000098AU24BurstDirectCall_Initialize_m5532852BC3FBCFF98707C2A1CDCD2A42C9790337,
	Try_0000098AU24BurstDirectCall__cctor_mB2E65EDB9A4D5A75E69819C5626DE5D41C1F8F3B,
	Try_0000098AU24BurstDirectCall_Invoke_m56FC230573AFF15A0269E4392BF9EB82975D219D,
	NULL,
	TableEntry__cctor_m4E930B1CD3F5349C2EE468379393D62B7F40908F,
	CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68,
	BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A,
	BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C,
	NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63,
	CollectionHelper_Log2Floor_mB2378B012C2572398533B336D8C73AC3E1DBD0D5,
	CollectionHelper_Align_m8D94703680FB74636F02BEF660C4D913597CE0F3,
	CollectionHelper_Hash_m8132774EA770BB87BF148F3B4900F64CCA47F576,
	CollectionHelper_ShouldDeallocate_m41C16802B8B5846A4C78633AA05B2FF04D733234,
	CollectionHelper_AssumePositive_mD8785D4C9E69993993EA9982A5F6F2ADAFE5B110,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Unmanaged_Allocate_mE42F0479C571BE76773614A408329CFCB51FB7F5,
	Unmanaged_Free_mE141FBBD8FC68C23121409BD2187EDDAB7849045,
	NULL,
	Array_IsCustom_m5553247492EBFB5ECECF39E326F1EC5C3F7FA892,
	Array_CustomResize_mD9B4173A8E9495BECF4359DE1B09E72428F2C521,
	Array_Resize_m7334DEAC65445477E1D278AC2DFEBD53C06C2C1B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeQueueBlockPoolData_AllocateBlock_mEE2500587CBF0C5D7DF01862F5608578F3E1A41D,
	NativeQueueBlockPoolData_FreeBlock_m79B8FBE865EF9B2477F98C29F078417436BEED0E,
	NativeQueueBlockPool_GetQueueBlockPool_mF092D3178F1886254B0DE9D2605313A47E33746B,
	NativeQueueBlockPool_AppDomainOnDomainUnload_m07AB4B933463598F22A818F971FDD43BD2C67A27,
	NativeQueueBlockPool_OnDomainUnload_m4071B7A24C6080006CA7F847B3B70084B50DF058,
	NativeQueueBlockPool__cctor_m2A968855F74268AE053F8F37C14E73C5047F92FB,
	NativeQueueData_GetCurrentWriteBlockTLS_m75502C82FDFF25E26AA4773759A824A0D22E1411,
	NativeQueueData_SetCurrentWriteBlockTLS_m9C78BEF5415CD20F6265F199C04B0D59D590131B,
	NULL,
	NULL,
	NativeQueueData_DeallocateQueue_mFE6EAED9FB35AB70EFEA53BCBCF089EF36D5A816,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1,
	Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32,
	NULL,
	NULL,
	RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580,
	RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678,
	RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE,
	RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066,
	RewindableAllocator_get_Handle_m056BEDCB4F2BEA70D8DB4B91FAE1C9B7A44D174F,
	RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE,
	MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3,
	MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958,
	MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF,
	MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F,
	MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB,
	Try_000006E4U24PostfixBurstDelegate__ctor_mEBA79173E3C2DAAEA8326078C999FA402F4ACC64,
	Try_000006E4U24PostfixBurstDelegate_Invoke_m4395B8E38DF7729CB7CBC91CB84DC6F35A799CFC,
	Try_000006E4U24PostfixBurstDelegate_BeginInvoke_mA033F943066DE34D29BB0A17603449D75ABC3A5E,
	Try_000006E4U24PostfixBurstDelegate_EndInvoke_m81754140E9F3202F9B8F3720FFCB4592D75CF5FD,
	Try_000006E4U24BurstDirectCall_GetFunctionPointerDiscard_mA84B7EC78BABDEF9453035B2DD6B5FA73560CACE,
	Try_000006E4U24BurstDirectCall_GetFunctionPointer_m3438AD325B9DCF1309368BD7D66E871135E96FF6,
	Try_000006E4U24BurstDirectCall_Constructor_m5BA546922E672DD27325F241328D72512C5F1D2F,
	Try_000006E4U24BurstDirectCall_Initialize_m30A3D24D62B6CA3D49C9C4C41AB8AA4DAD0924BF,
	Try_000006E4U24BurstDirectCall__cctor_mBED8730ECF3371B5F5BCA6414221C87862757969,
	Try_000006E4U24BurstDirectCall_Invoke_m2899E236ABAAE0838051D96F021EFD9C1463FF0C,
	xxHash3_Avx2HashLongInternalLoop_mCCB0DF801328F7B4907CCD550BA0E9C2A971E4AF,
	xxHash3_Avx2ScrambleAcc_mA6DBBF7E016635A901F80FC07FF1851618D6C3D5,
	xxHash3_Avx2Accumulate_m140D43D5123949B4A98F707CE310C744F78BC35B,
	xxHash3_Avx2Accumulate512_m7C23025DBFD191FE18600B12063730E6739850C7,
	xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7,
	xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D,
	xxHash3_ToUint4_m8417C90298E7430A8DCD51069984F8AE71C9FB56,
	xxHash3_Read64LE_m215CF04016BB327534223E32E2D4B451DDA1AACC,
	xxHash3_Write64LE_mA44006BF77A500B0ADF6E3B2FC4CF238E7939E5D,
	xxHash3_Mul32To64_m8E59492BD1C7B9C66F5F907D165C27255EFC5A8F,
	xxHash3_XorShift64_m14CFB98B253542B96241F24A1BAA59BC660F10C2,
	xxHash3_Mul128Fold64_mE4D677D2E0DACC2DAB3E33B876928A61E4E66036,
	xxHash3_Avalanche_m8AB7E61594297A37DDBEB74555B9B6E937CD91B9,
	xxHash3_Mix2Acc_mCC33F3919D7C753AC9997592EA6D6F95D35D72C3,
	xxHash3_MergeAcc_mF5CA25B12DC5478CDA428FDDD8EDEA7B070B1CDD,
	xxHash3_DefaultHashLongInternalLoop_m25D46D5E62D5A75BF6B18D459552A4F639D7B070,
	xxHash3_DefaultAccumulate_m89B15352C5BBD7848BEF7D1DE2B93275637B22A9,
	xxHash3_DefaultAccumulate512_mE2FA5E5ACA719367800C367B7B17CFAF94F38253,
	xxHash3_DefaultScrambleAcc_m5F9168BDE58B95E6C98B107B341D0D822AAAEC37,
	xxHash3_Hash64LongU24BurstManaged_m9385EE12B31C1382FF320BCDBE32657220250568,
	xxHash3_Hash128LongU24BurstManaged_mCC1E11085C5906FEAF9C4151C7A410701166563D,
	Hash64Long_0000071BU24PostfixBurstDelegate__ctor_m30DADBCC700DEEA88A45176EFE5359E05E57B1CD,
	Hash64Long_0000071BU24PostfixBurstDelegate_Invoke_m6FD507538031D026575207557ED16AF8825A1BC2,
	Hash64Long_0000071BU24PostfixBurstDelegate_BeginInvoke_m3C6C5073170122FCBBABBD69A07C65B78F9F7179,
	Hash64Long_0000071BU24PostfixBurstDelegate_EndInvoke_mC271DD74FAB05AB1A499ABE1299FFE38C728544A,
	Hash64Long_0000071BU24BurstDirectCall_GetFunctionPointerDiscard_m86EA21E2A4B5EC643E5638EE6296418CE32FC2AA,
	Hash64Long_0000071BU24BurstDirectCall_GetFunctionPointer_mF12235CDD54BE26636BE1B43D6280202317BC913,
	Hash64Long_0000071BU24BurstDirectCall_Constructor_m31DCFDFBC7582590BE81032BADE30C208523264A,
	Hash64Long_0000071BU24BurstDirectCall_Initialize_mEA8B497BDCAB0B50B5D724584F4F92AF82F25BED,
	Hash64Long_0000071BU24BurstDirectCall__cctor_mA2D2256E835433B4BF0CA2E9B5D06394C8548F69,
	Hash64Long_0000071BU24BurstDirectCall_Invoke_m6D13FD2119A9BE113579A8EAEF8C2097EED80934,
	Hash128Long_00000722U24PostfixBurstDelegate__ctor_m8B58C18C182DA819093F36B4B1B3D82A5AE3636E,
	Hash128Long_00000722U24PostfixBurstDelegate_Invoke_m1DBD2DD67368C8A8507087CF09F22479D05FA77C,
	Hash128Long_00000722U24PostfixBurstDelegate_BeginInvoke_mD2D4C962A7C7D04EE13C74C0B2E5FD833152024A,
	Hash128Long_00000722U24PostfixBurstDelegate_EndInvoke_m3685C155A9FC24F3277FC18910E8E37DB97C0E6B,
	Hash128Long_00000722U24BurstDirectCall_GetFunctionPointerDiscard_mA651E5D271E4D3BF37B8E4F34D0AED96EACEA810,
	Hash128Long_00000722U24BurstDirectCall_GetFunctionPointer_mA55764DA549432D4E19FCC1B46E99B650445A1B6,
	Hash128Long_00000722U24BurstDirectCall_Constructor_mCA8D5DB3F6D0ACDC758DEE1503E22FFD1D203CE3,
	Hash128Long_00000722U24BurstDirectCall_Initialize_mA653807BCE1131661D20040D565029B425E7899D,
	Hash128Long_00000722U24BurstDirectCall__cctor_m36A1FE007ADBCFE8DCC6168C0E7EA4EB77B609CA,
	Hash128Long_00000722U24BurstDirectCall_Invoke_m7D680C06A0D7FBC34D9EFA0783B12789787C40A3,
	NULL,
	UnsafeList__ctor_m5240789DAB3DD5F68C467109E4936006FA2661CF,
	UnsafeList_Dispose_m622096B7E176E917588CF25DAE72085A98401579,
	UnsafeList_Resize_m53CE313205849622B3356EBD85E428E5DE269799,
	NULL,
	NULL,
	NULL,
	UnsafeList_SetCapacity_mB0556B23A585A61FBF77ADFD61BC7EC2C43B7C1D,
	NULL,
	NULL,
	UnsafeList_RemoveRangeSwapBackWithBeginEnd_m27C8CE2CDB273AFC66DC30A3DA535345E7B35AC7,
	NULL,
	NULL,
	NULL,
	UnsafeHashMapData_GetBucketSize_m8D69EA245E3F2BAAF064E9BE4BBF778BB8DAE812,
	UnsafeHashMapData_GrowCapacity_mC54E03209825349B27EAD2472347149FEF5E29FE,
	NULL,
	NULL,
	UnsafeHashMapData_DeallocateHashMap_m293BA856234D0D2A0900EF5803C0B268ED5318F7,
	NULL,
	UnsafeHashMapData_GetCount_m4BC7698D560DBD99E00EBDC01664E14C66C11BCE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C,
};
extern void AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D_AdjustorThunk (void);
extern void AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060_AdjustorThunk (void);
extern void AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B_AdjustorThunk (void);
extern void AllocatorHandle_Try_m2F88758592B176EF3A7CFDCB93599C0CE6A97148_AdjustorThunk (void);
extern void AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E_AdjustorThunk (void);
extern void AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF_AdjustorThunk (void);
extern void AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1_AdjustorThunk (void);
extern void Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42_AdjustorThunk (void);
extern void Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA_AdjustorThunk (void);
extern void Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907_AdjustorThunk (void);
extern void Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF_AdjustorThunk (void);
extern void Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730_AdjustorThunk (void);
extern void Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9_AdjustorThunk (void);
extern void Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1_AdjustorThunk (void);
extern void StackAllocator_get_Handle_m22001B4045E018527C5B35D6715B550B6002C7ED_AdjustorThunk (void);
extern void StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D_AdjustorThunk (void);
extern void StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678_AdjustorThunk (void);
extern void SlabAllocator_get_Handle_m1BAE636499EF06990B084B49FF05100F4D70C6D7_AdjustorThunk (void);
extern void SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685_AdjustorThunk (void);
extern void SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3_AdjustorThunk (void);
extern void SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F_AdjustorThunk (void);
extern void NativeQueueBlockPoolData_AllocateBlock_mEE2500587CBF0C5D7DF01862F5608578F3E1A41D_AdjustorThunk (void);
extern void NativeQueueBlockPoolData_FreeBlock_m79B8FBE865EF9B2477F98C29F078417436BEED0E_AdjustorThunk (void);
extern void NativeQueueData_GetCurrentWriteBlockTLS_m75502C82FDFF25E26AA4773759A824A0D22E1411_AdjustorThunk (void);
extern void NativeQueueData_SetCurrentWriteBlockTLS_m9C78BEF5415CD20F6265F199C04B0D59D590131B_AdjustorThunk (void);
extern void Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1_AdjustorThunk (void);
extern void Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32_AdjustorThunk (void);
extern void RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580_AdjustorThunk (void);
extern void RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678_AdjustorThunk (void);
extern void RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE_AdjustorThunk (void);
extern void RewindableAllocator_get_Handle_m056BEDCB4F2BEA70D8DB4B91FAE1C9B7A44D174F_AdjustorThunk (void);
extern void MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3_AdjustorThunk (void);
extern void MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958_AdjustorThunk (void);
extern void MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF_AdjustorThunk (void);
extern void MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F_AdjustorThunk (void);
extern void MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB_AdjustorThunk (void);
extern void UnsafeList__ctor_m5240789DAB3DD5F68C467109E4936006FA2661CF_AdjustorThunk (void);
extern void UnsafeList_Dispose_m622096B7E176E917588CF25DAE72085A98401579_AdjustorThunk (void);
extern void UnsafeList_Resize_m53CE313205849622B3356EBD85E428E5DE269799_AdjustorThunk (void);
extern void UnsafeList_SetCapacity_mB0556B23A585A61FBF77ADFD61BC7EC2C43B7C1D_AdjustorThunk (void);
extern void UnsafeList_RemoveRangeSwapBackWithBeginEnd_m27C8CE2CDB273AFC66DC30A3DA535345E7B35AC7_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[41] = 
{
	{ 0x06000017, AllocatorHandle_get_TableEntry_mF3BC93F69AA3E1764A9770FEE595E263239B703D_AdjustorThunk },
	{ 0x06000018, AllocatorHandle_Rewind_mDBC3DC2236265F7F712121F0F3683F73B8609060_AdjustorThunk },
	{ 0x0600001A, AllocatorHandle_get_Value_m5F4A923E36A6E1C8983F187DFF73AA659350790B_AdjustorThunk },
	{ 0x0600001B, AllocatorHandle_Try_m2F88758592B176EF3A7CFDCB93599C0CE6A97148_AdjustorThunk },
	{ 0x0600001C, AllocatorHandle_get_Handle_m9CBE1298F12DFCBD71E6DBE77B08E5D0FD2F4A5E_AdjustorThunk },
	{ 0x0600001D, AllocatorHandle_get_ToAllocator_mC2F7F3B23A30D63C2A14984F5D25DDF117C5FEFF_AdjustorThunk },
	{ 0x0600001E, AllocatorHandle_Dispose_m21567B9257F67FFE3EA2A5C44BE860BE641B0FA1_AdjustorThunk },
	{ 0x0600001F, Range_Dispose_m466C86ACD4956014EE550CEC4245993E10629D42_AdjustorThunk },
	{ 0x06000020, Block_get_Bytes_m64C2E4525C2C3D7BE7B397B6492567A36E01A0DA_AdjustorThunk },
	{ 0x06000021, Block_get_AllocatedBytes_mC7DC93B1995B6837136BE97871E344CFDD32B907_AdjustorThunk },
	{ 0x06000022, Block_get_Alignment_m4EC57A8787D59AADAD695E0AFACF6346B05738FF_AdjustorThunk },
	{ 0x06000023, Block_set_Alignment_m0B1F5E27F5621271C8F5007C547061F9AD9FE730_AdjustorThunk },
	{ 0x06000024, Block_Dispose_mE083CE7318FC04B02E006375040E0389B72148A9_AdjustorThunk },
	{ 0x06000025, Block_TryFree_mA87B5FC0C11DE355D660CFA5C698DCF77182E0C1_AdjustorThunk },
	{ 0x06000028, StackAllocator_get_Handle_m22001B4045E018527C5B35D6715B550B6002C7ED_AdjustorThunk },
	{ 0x06000029, StackAllocator_Try_m9E77FFC5B3DB94A6A738F9659B23FB48F675825D_AdjustorThunk },
	{ 0x0600002B, StackAllocator_Dispose_m121AA556092D97A553B56BE62D67332183F4F678_AdjustorThunk },
	{ 0x06000037, SlabAllocator_get_Handle_m1BAE636499EF06990B084B49FF05100F4D70C6D7_AdjustorThunk },
	{ 0x06000038, SlabAllocator_get_SlabSizeInBytes_m09758AFE572F9BA8007BB7ED308086BA629DE685_AdjustorThunk },
	{ 0x06000039, SlabAllocator_Try_m18522CF564EE22D2C7FD7C098087906032E53CE3_AdjustorThunk },
	{ 0x0600003B, SlabAllocator_Dispose_m5FD3C0E10FE09952A8F99656821B2D7F3B3E578F_AdjustorThunk },
	{ 0x060000D7, NativeQueueBlockPoolData_AllocateBlock_mEE2500587CBF0C5D7DF01862F5608578F3E1A41D_AdjustorThunk },
	{ 0x060000D8, NativeQueueBlockPoolData_FreeBlock_m79B8FBE865EF9B2477F98C29F078417436BEED0E_AdjustorThunk },
	{ 0x060000DD, NativeQueueData_GetCurrentWriteBlockTLS_m75502C82FDFF25E26AA4773759A824A0D22E1411_AdjustorThunk },
	{ 0x060000DE, NativeQueueData_SetCurrentWriteBlockTLS_m9C78BEF5415CD20F6265F199C04B0D59D590131B_AdjustorThunk },
	{ 0x060000FE, Spinner_Lock_m455E583C6650190C9F5D9211C134A8D7898006D1_AdjustorThunk },
	{ 0x060000FF, Spinner_Unlock_m1A26CB58AE3E733421698B9F8750D882C649EE32_AdjustorThunk },
	{ 0x06000102, RewindableAllocator_Rewind_mCE9856063EE3638DC82C4CF8605DBEBEBF130580_AdjustorThunk },
	{ 0x06000103, RewindableAllocator_Dispose_m9938A91127A11581DA09794A2C5E8DF6283F2678_AdjustorThunk },
	{ 0x06000104, RewindableAllocator_Try_m1C0B2B6033081280DA48B6475F2A0B764E4F57BE_AdjustorThunk },
	{ 0x06000106, RewindableAllocator_get_Handle_m056BEDCB4F2BEA70D8DB4B91FAE1C9B7A44D174F_AdjustorThunk },
	{ 0x06000108, MemoryBlock__ctor_mE607093E03F964839EE507282CED5F6ACB9E64D3_AdjustorThunk },
	{ 0x06000109, MemoryBlock_Rewind_mD4E938ABCCD22E3430DEDEA620DFEF56E7542958_AdjustorThunk },
	{ 0x0600010A, MemoryBlock_Dispose_m8B9123CC2024106EE5104952C15DA2E4902033EF_AdjustorThunk },
	{ 0x0600010B, MemoryBlock_TryAllocate_m3B55864CA6A89C02E25B9C7B49E2652B66FE3A7F_AdjustorThunk },
	{ 0x0600010C, MemoryBlock_Contains_m93E7B8DC04D2AD9AFDD8589FFC7CD205031C90BB_AdjustorThunk },
	{ 0x06000141, UnsafeList__ctor_m5240789DAB3DD5F68C467109E4936006FA2661CF_AdjustorThunk },
	{ 0x06000142, UnsafeList_Dispose_m622096B7E176E917588CF25DAE72085A98401579_AdjustorThunk },
	{ 0x06000143, UnsafeList_Resize_m53CE313205849622B3356EBD85E428E5DE269799_AdjustorThunk },
	{ 0x06000147, UnsafeList_SetCapacity_mB0556B23A585A61FBF77ADFD61BC7EC2C43B7C1D_AdjustorThunk },
	{ 0x0600014A, UnsafeList_RemoveRangeSwapBackWithBeginEnd_m27C8CE2CDB273AFC66DC30A3DA535345E7B35AC7_AdjustorThunk },
};
static const int32_t s_InvokerIndices[375] = 
{
	3780,
	3780,
	3780,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5801,
	-1,
	6010,
	6038,
	6010,
	6010,
	6156,
	6299,
	1614,
	1014,
	414,
	982,
	3636,
	3780,
	6256,
	3705,
	2004,
	3811,
	3705,
	3780,
	3780,
	3706,
	3706,
	3705,
	3021,
	3780,
	3705,
	2004,
	3811,
	3811,
	2004,
	5301,
	3780,
	5301,
	1614,
	1014,
	414,
	2075,
	6203,
	6273,
	6299,
	6299,
	6299,
	5301,
	3811,
	3705,
	2004,
	5301,
	3780,
	5301,
	1614,
	1014,
	414,
	2075,
	6203,
	6273,
	6299,
	6299,
	6299,
	5301,
	-1,
	6299,
	3780,
	3038,
	3780,
	3780,
	6015,
	5297,
	5291,
	6156,
	6015,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4798,
	5674,
	-1,
	6156,
	3925,
	3925,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3636,
	2930,
	6261,
	6299,
	5784,
	6299,
	1837,
	1434,
	-1,
	-1,
	5065,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3780,
	3780,
	-1,
	-1,
	3780,
	3780,
	2004,
	5301,
	3811,
	5301,
	3022,
	3780,
	3780,
	2004,
	2548,
	1614,
	1014,
	414,
	2075,
	6203,
	6273,
	6299,
	6299,
	6299,
	5301,
	3974,
	5667,
	3973,
	4626,
	4466,
	4278,
	5857,
	6039,
	5669,
	5316,
	5317,
	5318,
	6045,
	4858,
	4854,
	3974,
	3973,
	4275,
	5667,
	4466,
	4278,
	1614,
	388,
	109,
	2167,
	6203,
	6273,
	6299,
	6299,
	6299,
	4466,
	1614,
	216,
	69,
	3038,
	6203,
	6273,
	6299,
	6299,
	6299,
	4278,
	-1,
	3021,
	3780,
	545,
	-1,
	-1,
	-1,
	846,
	-1,
	-1,
	846,
	-1,
	-1,
	-1,
	6015,
	6015,
	-1,
	-1,
	5674,
	-1,
	6010,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6299,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[6] = 
{
	{ 0x0600002A, 25,  (void**)&StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B_RuntimeMethod_var, 0 },
	{ 0x0600002C, 26,  (void**)&StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B_RuntimeMethod_var, 0 },
	{ 0x0600003A, 23,  (void**)&SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8_RuntimeMethod_var, 0 },
	{ 0x0600003C, 24,  (void**)&SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5_RuntimeMethod_var, 0 },
	{ 0x06000105, 20,  (void**)&RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066_RuntimeMethod_var, 0 },
	{ 0x06000107, 21,  (void**)&RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE_RuntimeMethod_var, 0 },
};
static const Il2CppTokenRangePair s_rgctxIndices[69] = 
{
	{ 0x02000015, { 14, 2 } },
	{ 0x0200001D, { 19, 25 } },
	{ 0x0200001F, { 44, 25 } },
	{ 0x02000021, { 69, 25 } },
	{ 0x02000023, { 94, 25 } },
	{ 0x02000025, { 119, 27 } },
	{ 0x02000031, { 163, 9 } },
	{ 0x02000034, { 178, 17 } },
	{ 0x0200003B, { 198, 3 } },
	{ 0x0200003C, { 201, 2 } },
	{ 0x0200003E, { 275, 3 } },
	{ 0x02000040, { 278, 2 } },
	{ 0x0200004F, { 305, 11 } },
	{ 0x02000051, { 316, 9 } },
	{ 0x02000053, { 325, 12 } },
	{ 0x06000004, { 0, 1 } },
	{ 0x06000005, { 1, 1 } },
	{ 0x06000006, { 2, 3 } },
	{ 0x06000007, { 5, 3 } },
	{ 0x06000008, { 8, 1 } },
	{ 0x06000009, { 9, 1 } },
	{ 0x0600000A, { 10, 3 } },
	{ 0x0600000C, { 13, 1 } },
	{ 0x06000052, { 16, 2 } },
	{ 0x06000053, { 18, 1 } },
	{ 0x060000AD, { 146, 1 } },
	{ 0x060000B1, { 147, 2 } },
	{ 0x060000B2, { 149, 3 } },
	{ 0x060000B3, { 152, 4 } },
	{ 0x060000B4, { 156, 5 } },
	{ 0x060000B5, { 161, 2 } },
	{ 0x060000C0, { 172, 6 } },
	{ 0x060000C3, { 195, 2 } },
	{ 0x060000E0, { 197, 1 } },
	{ 0x060000E9, { 203, 3 } },
	{ 0x060000EA, { 206, 1 } },
	{ 0x060000EB, { 207, 3 } },
	{ 0x060000EC, { 210, 3 } },
	{ 0x060000ED, { 213, 1 } },
	{ 0x060000EE, { 214, 5 } },
	{ 0x060000EF, { 219, 5 } },
	{ 0x060000F0, { 224, 6 } },
	{ 0x060000F1, { 230, 2 } },
	{ 0x060000F2, { 232, 5 } },
	{ 0x060000F3, { 237, 2 } },
	{ 0x060000F4, { 239, 5 } },
	{ 0x060000F5, { 244, 1 } },
	{ 0x060000F6, { 245, 5 } },
	{ 0x060000F7, { 250, 5 } },
	{ 0x060000F8, { 255, 6 } },
	{ 0x060000F9, { 261, 2 } },
	{ 0x060000FA, { 263, 5 } },
	{ 0x060000FB, { 268, 2 } },
	{ 0x060000FC, { 270, 5 } },
	{ 0x06000140, { 280, 4 } },
	{ 0x06000144, { 284, 2 } },
	{ 0x06000145, { 286, 2 } },
	{ 0x06000146, { 288, 1 } },
	{ 0x06000148, { 289, 1 } },
	{ 0x06000149, { 290, 2 } },
	{ 0x0600014B, { 292, 1 } },
	{ 0x0600014C, { 293, 1 } },
	{ 0x06000150, { 294, 1 } },
	{ 0x06000151, { 295, 5 } },
	{ 0x06000153, { 300, 2 } },
	{ 0x06000155, { 302, 3 } },
	{ 0x06000169, { 337, 3 } },
	{ 0x0600016E, { 340, 2 } },
	{ 0x0600016F, { 342, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[343] = 
{
	{ (Il2CppRGCTXDataType)2, 8 },
	{ (Il2CppRGCTXDataType)3, 22967 },
	{ (Il2CppRGCTXDataType)3, 27502 },
	{ (Il2CppRGCTXDataType)3, 27251 },
	{ (Il2CppRGCTXDataType)3, 22965 },
	{ (Il2CppRGCTXDataType)3, 27501 },
	{ (Il2CppRGCTXDataType)3, 27250 },
	{ (Il2CppRGCTXDataType)3, 22964 },
	{ (Il2CppRGCTXDataType)2, 6 },
	{ (Il2CppRGCTXDataType)3, 22992 },
	{ (Il2CppRGCTXDataType)3, 27500 },
	{ (Il2CppRGCTXDataType)3, 27249 },
	{ (Il2CppRGCTXDataType)3, 22990 },
	{ (Il2CppRGCTXDataType)3, 22981 },
	{ (Il2CppRGCTXDataType)2, 528 },
	{ (Il2CppRGCTXDataType)3, 27336 },
	{ (Il2CppRGCTXDataType)3, 15104 },
	{ (Il2CppRGCTXDataType)3, 26545 },
	{ (Il2CppRGCTXDataType)3, 27473 },
	{ (Il2CppRGCTXDataType)3, 8795 },
	{ (Il2CppRGCTXDataType)3, 27492 },
	{ (Il2CppRGCTXDataType)3, 26239 },
	{ (Il2CppRGCTXDataType)3, 8794 },
	{ (Il2CppRGCTXDataType)3, 8796 },
	{ (Il2CppRGCTXDataType)2, 397 },
	{ (Il2CppRGCTXDataType)3, 8785 },
	{ (Il2CppRGCTXDataType)3, 8888 },
	{ (Il2CppRGCTXDataType)3, 8788 },
	{ (Il2CppRGCTXDataType)3, 8762 },
	{ (Il2CppRGCTXDataType)3, 8784 },
	{ (Il2CppRGCTXDataType)3, 8854 },
	{ (Il2CppRGCTXDataType)3, 8787 },
	{ (Il2CppRGCTXDataType)3, 8818 },
	{ (Il2CppRGCTXDataType)3, 8786 },
	{ (Il2CppRGCTXDataType)2, 1557 },
	{ (Il2CppRGCTXDataType)3, 8790 },
	{ (Il2CppRGCTXDataType)2, 1573 },
	{ (Il2CppRGCTXDataType)3, 8793 },
	{ (Il2CppRGCTXDataType)2, 1552 },
	{ (Il2CppRGCTXDataType)3, 8789 },
	{ (Il2CppRGCTXDataType)2, 1568 },
	{ (Il2CppRGCTXDataType)3, 8792 },
	{ (Il2CppRGCTXDataType)2, 1562 },
	{ (Il2CppRGCTXDataType)3, 8791 },
	{ (Il2CppRGCTXDataType)3, 8902 },
	{ (Il2CppRGCTXDataType)3, 27495 },
	{ (Il2CppRGCTXDataType)3, 26242 },
	{ (Il2CppRGCTXDataType)3, 8901 },
	{ (Il2CppRGCTXDataType)3, 8903 },
	{ (Il2CppRGCTXDataType)3, 8799 },
	{ (Il2CppRGCTXDataType)2, 400 },
	{ (Il2CppRGCTXDataType)3, 8892 },
	{ (Il2CppRGCTXDataType)3, 8895 },
	{ (Il2CppRGCTXDataType)3, 8765 },
	{ (Il2CppRGCTXDataType)3, 8891 },
	{ (Il2CppRGCTXDataType)3, 8869 },
	{ (Il2CppRGCTXDataType)3, 8894 },
	{ (Il2CppRGCTXDataType)3, 8833 },
	{ (Il2CppRGCTXDataType)3, 8893 },
	{ (Il2CppRGCTXDataType)2, 1560 },
	{ (Il2CppRGCTXDataType)3, 8897 },
	{ (Il2CppRGCTXDataType)2, 1576 },
	{ (Il2CppRGCTXDataType)3, 8900 },
	{ (Il2CppRGCTXDataType)2, 1555 },
	{ (Il2CppRGCTXDataType)3, 8896 },
	{ (Il2CppRGCTXDataType)2, 1571 },
	{ (Il2CppRGCTXDataType)3, 8899 },
	{ (Il2CppRGCTXDataType)2, 1565 },
	{ (Il2CppRGCTXDataType)3, 8898 },
	{ (Il2CppRGCTXDataType)3, 8760 },
	{ (Il2CppRGCTXDataType)3, 27491 },
	{ (Il2CppRGCTXDataType)3, 26238 },
	{ (Il2CppRGCTXDataType)3, 8759 },
	{ (Il2CppRGCTXDataType)3, 8761 },
	{ (Il2CppRGCTXDataType)3, 8783 },
	{ (Il2CppRGCTXDataType)2, 396 },
	{ (Il2CppRGCTXDataType)3, 8750 },
	{ (Il2CppRGCTXDataType)3, 8887 },
	{ (Il2CppRGCTXDataType)3, 8753 },
	{ (Il2CppRGCTXDataType)3, 8749 },
	{ (Il2CppRGCTXDataType)3, 8853 },
	{ (Il2CppRGCTXDataType)3, 8752 },
	{ (Il2CppRGCTXDataType)3, 8817 },
	{ (Il2CppRGCTXDataType)3, 8751 },
	{ (Il2CppRGCTXDataType)2, 1556 },
	{ (Il2CppRGCTXDataType)3, 8755 },
	{ (Il2CppRGCTXDataType)2, 1572 },
	{ (Il2CppRGCTXDataType)3, 8758 },
	{ (Il2CppRGCTXDataType)2, 1551 },
	{ (Il2CppRGCTXDataType)3, 8754 },
	{ (Il2CppRGCTXDataType)2, 1567 },
	{ (Il2CppRGCTXDataType)3, 8757 },
	{ (Il2CppRGCTXDataType)2, 1561 },
	{ (Il2CppRGCTXDataType)3, 8756 },
	{ (Il2CppRGCTXDataType)3, 8867 },
	{ (Il2CppRGCTXDataType)3, 27494 },
	{ (Il2CppRGCTXDataType)3, 26241 },
	{ (Il2CppRGCTXDataType)3, 8866 },
	{ (Il2CppRGCTXDataType)3, 8868 },
	{ (Il2CppRGCTXDataType)3, 8798 },
	{ (Il2CppRGCTXDataType)2, 399 },
	{ (Il2CppRGCTXDataType)3, 8857 },
	{ (Il2CppRGCTXDataType)3, 8890 },
	{ (Il2CppRGCTXDataType)3, 8860 },
	{ (Il2CppRGCTXDataType)3, 8764 },
	{ (Il2CppRGCTXDataType)3, 8856 },
	{ (Il2CppRGCTXDataType)3, 8859 },
	{ (Il2CppRGCTXDataType)3, 8832 },
	{ (Il2CppRGCTXDataType)3, 8858 },
	{ (Il2CppRGCTXDataType)2, 1559 },
	{ (Il2CppRGCTXDataType)3, 8862 },
	{ (Il2CppRGCTXDataType)2, 1575 },
	{ (Il2CppRGCTXDataType)3, 8865 },
	{ (Il2CppRGCTXDataType)2, 1554 },
	{ (Il2CppRGCTXDataType)3, 8861 },
	{ (Il2CppRGCTXDataType)2, 1570 },
	{ (Il2CppRGCTXDataType)3, 8864 },
	{ (Il2CppRGCTXDataType)2, 1564 },
	{ (Il2CppRGCTXDataType)3, 8863 },
	{ (Il2CppRGCTXDataType)3, 8830 },
	{ (Il2CppRGCTXDataType)3, 27493 },
	{ (Il2CppRGCTXDataType)3, 26240 },
	{ (Il2CppRGCTXDataType)3, 8829 },
	{ (Il2CppRGCTXDataType)3, 27351 },
	{ (Il2CppRGCTXDataType)3, 27684 },
	{ (Il2CppRGCTXDataType)3, 8831 },
	{ (Il2CppRGCTXDataType)3, 8797 },
	{ (Il2CppRGCTXDataType)2, 398 },
	{ (Il2CppRGCTXDataType)3, 8820 },
	{ (Il2CppRGCTXDataType)3, 8889 },
	{ (Il2CppRGCTXDataType)3, 8823 },
	{ (Il2CppRGCTXDataType)3, 8763 },
	{ (Il2CppRGCTXDataType)3, 8819 },
	{ (Il2CppRGCTXDataType)3, 8855 },
	{ (Il2CppRGCTXDataType)3, 8822 },
	{ (Il2CppRGCTXDataType)3, 8821 },
	{ (Il2CppRGCTXDataType)2, 1558 },
	{ (Il2CppRGCTXDataType)3, 8825 },
	{ (Il2CppRGCTXDataType)2, 1574 },
	{ (Il2CppRGCTXDataType)3, 8828 },
	{ (Il2CppRGCTXDataType)2, 1553 },
	{ (Il2CppRGCTXDataType)3, 8824 },
	{ (Il2CppRGCTXDataType)2, 1569 },
	{ (Il2CppRGCTXDataType)3, 8827 },
	{ (Il2CppRGCTXDataType)2, 1563 },
	{ (Il2CppRGCTXDataType)3, 8826 },
	{ (Il2CppRGCTXDataType)3, 27897 },
	{ (Il2CppRGCTXDataType)3, 27487 },
	{ (Il2CppRGCTXDataType)3, 27246 },
	{ (Il2CppRGCTXDataType)3, 26641 },
	{ (Il2CppRGCTXDataType)3, 15128 },
	{ (Il2CppRGCTXDataType)3, 26543 },
	{ (Il2CppRGCTXDataType)3, 27341 },
	{ (Il2CppRGCTXDataType)2, 323 },
	{ (Il2CppRGCTXDataType)2, 2214 },
	{ (Il2CppRGCTXDataType)3, 9608 },
	{ (Il2CppRGCTXDataType)3, 27488 },
	{ (Il2CppRGCTXDataType)3, 27503 },
	{ (Il2CppRGCTXDataType)3, 15129 },
	{ (Il2CppRGCTXDataType)3, 26607 },
	{ (Il2CppRGCTXDataType)3, 26555 },
	{ (Il2CppRGCTXDataType)3, 22969 },
	{ (Il2CppRGCTXDataType)3, 27478 },
	{ (Il2CppRGCTXDataType)3, 16829 },
	{ (Il2CppRGCTXDataType)2, 3703 },
	{ (Il2CppRGCTXDataType)3, 22316 },
	{ (Il2CppRGCTXDataType)3, 22317 },
	{ (Il2CppRGCTXDataType)3, 22321 },
	{ (Il2CppRGCTXDataType)3, 22320 },
	{ (Il2CppRGCTXDataType)3, 22322 },
	{ (Il2CppRGCTXDataType)3, 22318 },
	{ (Il2CppRGCTXDataType)3, 22319 },
	{ (Il2CppRGCTXDataType)3, 15119 },
	{ (Il2CppRGCTXDataType)3, 15118 },
	{ (Il2CppRGCTXDataType)2, 199 },
	{ (Il2CppRGCTXDataType)2, 2200 },
	{ (Il2CppRGCTXDataType)3, 9603 },
	{ (Il2CppRGCTXDataType)3, 15120 },
	{ (Il2CppRGCTXDataType)3, 16857 },
	{ (Il2CppRGCTXDataType)2, 3705 },
	{ (Il2CppRGCTXDataType)3, 16856 },
	{ (Il2CppRGCTXDataType)3, 22349 },
	{ (Il2CppRGCTXDataType)3, 22352 },
	{ (Il2CppRGCTXDataType)3, 22350 },
	{ (Il2CppRGCTXDataType)3, 22351 },
	{ (Il2CppRGCTXDataType)3, 22343 },
	{ (Il2CppRGCTXDataType)3, 22344 },
	{ (Il2CppRGCTXDataType)3, 22347 },
	{ (Il2CppRGCTXDataType)3, 22346 },
	{ (Il2CppRGCTXDataType)3, 22345 },
	{ (Il2CppRGCTXDataType)3, 16858 },
	{ (Il2CppRGCTXDataType)3, 26554 },
	{ (Il2CppRGCTXDataType)3, 26221 },
	{ (Il2CppRGCTXDataType)3, 22348 },
	{ (Il2CppRGCTXDataType)3, 16859 },
	{ (Il2CppRGCTXDataType)3, 22342 },
	{ (Il2CppRGCTXDataType)2, 201 },
	{ (Il2CppRGCTXDataType)3, 27481 },
	{ (Il2CppRGCTXDataType)3, 26721 },
	{ (Il2CppRGCTXDataType)3, 16956 },
	{ (Il2CppRGCTXDataType)3, 27353 },
	{ (Il2CppRGCTXDataType)3, 26726 },
	{ (Il2CppRGCTXDataType)3, 27688 },
	{ (Il2CppRGCTXDataType)3, 26612 },
	{ (Il2CppRGCTXDataType)3, 15121 },
	{ (Il2CppRGCTXDataType)3, 26758 },
	{ (Il2CppRGCTXDataType)3, 26773 },
	{ (Il2CppRGCTXDataType)3, 26719 },
	{ (Il2CppRGCTXDataType)3, 16855 },
	{ (Il2CppRGCTXDataType)3, 26753 },
	{ (Il2CppRGCTXDataType)3, 26735 },
	{ (Il2CppRGCTXDataType)3, 16983 },
	{ (Il2CppRGCTXDataType)3, 26759 },
	{ (Il2CppRGCTXDataType)3, 26756 },
	{ (Il2CppRGCTXDataType)3, 26782 },
	{ (Il2CppRGCTXDataType)3, 26748 },
	{ (Il2CppRGCTXDataType)3, 26738 },
	{ (Il2CppRGCTXDataType)3, 26766 },
	{ (Il2CppRGCTXDataType)3, 26755 },
	{ (Il2CppRGCTXDataType)3, 27346 },
	{ (Il2CppRGCTXDataType)3, 27682 },
	{ (Il2CppRGCTXDataType)2, 650 },
	{ (Il2CppRGCTXDataType)2, 1888 },
	{ (Il2CppRGCTXDataType)3, 9589 },
	{ (Il2CppRGCTXDataType)3, 26781 },
	{ (Il2CppRGCTXDataType)3, 27342 },
	{ (Il2CppRGCTXDataType)3, 26777 },
	{ (Il2CppRGCTXDataType)2, 644 },
	{ (Il2CppRGCTXDataType)2, 1882 },
	{ (Il2CppRGCTXDataType)3, 9585 },
	{ (Il2CppRGCTXDataType)3, 26743 },
	{ (Il2CppRGCTXDataType)3, 26778 },
	{ (Il2CppRGCTXDataType)3, 27344 },
	{ (Il2CppRGCTXDataType)2, 648 },
	{ (Il2CppRGCTXDataType)2, 1886 },
	{ (Il2CppRGCTXDataType)3, 9587 },
	{ (Il2CppRGCTXDataType)3, 27680 },
	{ (Il2CppRGCTXDataType)3, 27338 },
	{ (Il2CppRGCTXDataType)3, 27677 },
	{ (Il2CppRGCTXDataType)3, 27348 },
	{ (Il2CppRGCTXDataType)2, 658 },
	{ (Il2CppRGCTXDataType)2, 1896 },
	{ (Il2CppRGCTXDataType)3, 9591 },
	{ (Il2CppRGCTXDataType)3, 26779 },
	{ (Il2CppRGCTXDataType)3, 26763 },
	{ (Il2CppRGCTXDataType)3, 26785 },
	{ (Il2CppRGCTXDataType)3, 26750 },
	{ (Il2CppRGCTXDataType)3, 26740 },
	{ (Il2CppRGCTXDataType)3, 26768 },
	{ (Il2CppRGCTXDataType)3, 26762 },
	{ (Il2CppRGCTXDataType)3, 27347 },
	{ (Il2CppRGCTXDataType)3, 27683 },
	{ (Il2CppRGCTXDataType)2, 651 },
	{ (Il2CppRGCTXDataType)2, 1889 },
	{ (Il2CppRGCTXDataType)3, 9590 },
	{ (Il2CppRGCTXDataType)3, 26784 },
	{ (Il2CppRGCTXDataType)3, 27343 },
	{ (Il2CppRGCTXDataType)3, 26788 },
	{ (Il2CppRGCTXDataType)2, 645 },
	{ (Il2CppRGCTXDataType)2, 1883 },
	{ (Il2CppRGCTXDataType)3, 9586 },
	{ (Il2CppRGCTXDataType)3, 26745 },
	{ (Il2CppRGCTXDataType)3, 26789 },
	{ (Il2CppRGCTXDataType)3, 27345 },
	{ (Il2CppRGCTXDataType)2, 649 },
	{ (Il2CppRGCTXDataType)2, 1887 },
	{ (Il2CppRGCTXDataType)3, 9588 },
	{ (Il2CppRGCTXDataType)3, 27681 },
	{ (Il2CppRGCTXDataType)3, 27339 },
	{ (Il2CppRGCTXDataType)3, 27678 },
	{ (Il2CppRGCTXDataType)3, 27349 },
	{ (Il2CppRGCTXDataType)2, 659 },
	{ (Il2CppRGCTXDataType)2, 1897 },
	{ (Il2CppRGCTXDataType)3, 9592 },
	{ (Il2CppRGCTXDataType)3, 26790 },
	{ (Il2CppRGCTXDataType)2, 584 },
	{ (Il2CppRGCTXDataType)2, 1820 },
	{ (Il2CppRGCTXDataType)3, 9583 },
	{ (Il2CppRGCTXDataType)3, 27889 },
	{ (Il2CppRGCTXDataType)2, 517 },
	{ (Il2CppRGCTXDataType)3, 16853 },
	{ (Il2CppRGCTXDataType)3, 16854 },
	{ (Il2CppRGCTXDataType)3, 16852 },
	{ (Il2CppRGCTXDataType)3, 15107 },
	{ (Il2CppRGCTXDataType)3, 27485 },
	{ (Il2CppRGCTXDataType)3, 27245 },
	{ (Il2CppRGCTXDataType)3, 22962 },
	{ (Il2CppRGCTXDataType)3, 22989 },
	{ (Il2CppRGCTXDataType)3, 27106 },
	{ (Il2CppRGCTXDataType)3, 26542 },
	{ (Il2CppRGCTXDataType)3, 27112 },
	{ (Il2CppRGCTXDataType)3, 27679 },
	{ (Il2CppRGCTXDataType)3, 27110 },
	{ (Il2CppRGCTXDataType)3, 27484 },
	{ (Il2CppRGCTXDataType)3, 27092 },
	{ (Il2CppRGCTXDataType)3, 27093 },
	{ (Il2CppRGCTXDataType)3, 27505 },
	{ (Il2CppRGCTXDataType)3, 27490 },
	{ (Il2CppRGCTXDataType)3, 27350 },
	{ (Il2CppRGCTXDataType)2, 349 },
	{ (Il2CppRGCTXDataType)3, 27504 },
	{ (Il2CppRGCTXDataType)3, 27489 },
	{ (Il2CppRGCTXDataType)3, 15122 },
	{ (Il2CppRGCTXDataType)3, 27340 },
	{ (Il2CppRGCTXDataType)3, 15123 },
	{ (Il2CppRGCTXDataType)3, 22300 },
	{ (Il2CppRGCTXDataType)2, 3701 },
	{ (Il2CppRGCTXDataType)3, 27099 },
	{ (Il2CppRGCTXDataType)3, 27686 },
	{ (Il2CppRGCTXDataType)3, 27689 },
	{ (Il2CppRGCTXDataType)2, 518 },
	{ (Il2CppRGCTXDataType)3, 27354 },
	{ (Il2CppRGCTXDataType)2, 2210 },
	{ (Il2CppRGCTXDataType)3, 9607 },
	{ (Il2CppRGCTXDataType)3, 22301 },
	{ (Il2CppRGCTXDataType)3, 27355 },
	{ (Il2CppRGCTXDataType)3, 27089 },
	{ (Il2CppRGCTXDataType)3, 22323 },
	{ (Il2CppRGCTXDataType)3, 22302 },
	{ (Il2CppRGCTXDataType)2, 3702 },
	{ (Il2CppRGCTXDataType)3, 22304 },
	{ (Il2CppRGCTXDataType)3, 22303 },
	{ (Il2CppRGCTXDataType)3, 22305 },
	{ (Il2CppRGCTXDataType)3, 25930 },
	{ (Il2CppRGCTXDataType)3, 27096 },
	{ (Il2CppRGCTXDataType)3, 22359 },
	{ (Il2CppRGCTXDataType)2, 521 },
	{ (Il2CppRGCTXDataType)2, 3706 },
	{ (Il2CppRGCTXDataType)3, 22360 },
	{ (Il2CppRGCTXDataType)3, 22356 },
	{ (Il2CppRGCTXDataType)3, 22974 },
	{ (Il2CppRGCTXDataType)3, 22972 },
	{ (Il2CppRGCTXDataType)3, 27248 },
	{ (Il2CppRGCTXDataType)3, 22355 },
	{ (Il2CppRGCTXDataType)3, 22358 },
	{ (Il2CppRGCTXDataType)3, 27687 },
	{ (Il2CppRGCTXDataType)3, 22357 },
	{ (Il2CppRGCTXDataType)3, 22958 },
	{ (Il2CppRGCTXDataType)2, 277 },
	{ (Il2CppRGCTXDataType)3, 22354 },
	{ (Il2CppRGCTXDataType)3, 22963 },
	{ (Il2CppRGCTXDataType)3, 22980 },
	{ (Il2CppRGCTXDataType)3, 22353 },
};
extern const CustomAttributesCacheGenerator g_Unity_Collections_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_Collections_CodeGenModule;
const Il2CppCodeGenModule g_Unity_Collections_CodeGenModule = 
{
	"Unity.Collections.dll",
	375,
	s_methodPointers,
	41,
	s_adjustorThunks,
	s_InvokerIndices,
	6,
	s_reversePInvokeIndices,
	69,
	s_rgctxIndices,
	343,
	s_rgctxValues,
	NULL,
	g_Unity_Collections_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
