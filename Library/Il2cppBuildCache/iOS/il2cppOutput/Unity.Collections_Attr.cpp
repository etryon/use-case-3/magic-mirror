﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Unity.Collections.BurstCompatibleAttribute
struct BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82;
// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1;
// Unity.Burst.BurstDiscardAttribute
struct BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// Unity.Collections.CreatePropertyAttribute
struct CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F;
// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Runtime.CompilerServices.FixedBufferAttribute
struct FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E;
// Unity.Collections.LowLevel.Unsafe.NativeContainerAttribute
struct NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6;
// Unity.Collections.LowLevel.Unsafe.NativeContainerIsAtomicWriteOnlyAttribute
struct NativeContainerIsAtomicWriteOnlyAttribute_t2DB74DA0C416DD897E6F282B6F604646E0B344AB;
// Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestrictionAttribute
struct NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0;
// Unity.Collections.LowLevel.Unsafe.NativeSetThreadIndexAttribute
struct NativeSetThreadIndexAttribute_t7681C9225114E2B1478DE516F9FE1CD44B3681E8;
// Unity.Collections.NotBurstCompatibleAttribute
struct NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F;
// System.Runtime.CompilerServices.UnsafeValueTypeAttribute
struct UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Unity.Burst.BurstCompiler/StaticTypeReinitAttribute
struct StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2;

IL2CPP_EXTERN_C RuntimeClass* TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList128BytesDebugView_1_t8C40E82934244DDBB383E0CA35A81F2B00E40CE4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList32BytesDebugView_1_t21779F4BD4450CD45B481796502C35BAD7A7E4A9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList4096BytesDebugView_1_t70625E554752F30063D44798522354A66D3696D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList512BytesDebugView_1_tF41AD0D481A169839B889922C2E0A3424163B79F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* FixedList64BytesDebugView_1_t6B9BE5EABF537E5C3D57F0702FB979A60B91A443_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Hash128Long_00000722U24BurstDirectCall_tF9D993B09CC7EDAA9AE8BA4994C43ADB573A579A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Hash64Long_0000071BU24BurstDirectCall_t631230C77478A8ECEC15538227FC959EB3CE07FF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* NativeHashMapDebuggerTypeProxy_2_tB7FCF24B1602A95C822E7E65CEA35C45410A02D7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* NativeListDebugView_1_t5A689FBCF98D06A6219070C4B1BFB971776DE7BD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Try_000006E4U24BurstDirectCall_t9065BD9F58F9F94D743B0378F7BADFD2382570C2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Try_0000097CU24BurstDirectCall_t812D64B490B18957289F162D28186E4384B9066C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Try_0000098AU24BurstDirectCall_t6CD59618A003C605DBBCA95CF255CE53693BE1EE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UnsafeHashMapDebuggerTypeProxy_2_t39B21CBB744848DA31F2DF015FB564FA25632F1B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UnsafeListTDebugView_1_tAB2A35CE4BDB7898FFF42F088207433052DFF310_0_0_0_var;

struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// Unity.Collections.BurstCompatibleAttribute
struct BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type[] Unity.Collections.BurstCompatibleAttribute::<GenericTypeArguments>k__BackingField
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___U3CGenericTypeArgumentsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CGenericTypeArgumentsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82, ___U3CGenericTypeArgumentsU3Ek__BackingField_0)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_U3CGenericTypeArgumentsU3Ek__BackingField_0() const { return ___U3CGenericTypeArgumentsU3Ek__BackingField_0; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_U3CGenericTypeArgumentsU3Ek__BackingField_0() { return &___U3CGenericTypeArgumentsU3Ek__BackingField_0; }
	inline void set_U3CGenericTypeArgumentsU3Ek__BackingField_0(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___U3CGenericTypeArgumentsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGenericTypeArgumentsU3Ek__BackingField_0), (void*)value);
	}
};


// Unity.Burst.BurstDiscardAttribute
struct BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.CreatePropertyAttribute
struct CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Diagnostics.DebuggerDisplayAttribute::name
	String_t* ___name_0;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::value
	String_t* ___value_1;
	// System.String System.Diagnostics.DebuggerDisplayAttribute::type
	String_t* ___type_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_2), (void*)value);
	}
};


// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Diagnostics.DebuggerTypeProxyAttribute::typeName
	String_t* ___typeName_0;

public:
	inline static int32_t get_offset_of_typeName_0() { return static_cast<int32_t>(offsetof(DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014, ___typeName_0)); }
	inline String_t* get_typeName_0() const { return ___typeName_0; }
	inline String_t** get_address_of_typeName_0() { return &___typeName_0; }
	inline void set_typeName_0(String_t* value)
	{
		___typeName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeName_0), (void*)value);
	}
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.FixedBufferAttribute
struct FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.FixedBufferAttribute::elementType
	Type_t * ___elementType_0;
	// System.Int32 System.Runtime.CompilerServices.FixedBufferAttribute::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_elementType_0() { return static_cast<int32_t>(offsetof(FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C, ___elementType_0)); }
	inline Type_t * get_elementType_0() const { return ___elementType_0; }
	inline Type_t ** get_address_of_elementType_0() { return &___elementType_0; }
	inline void set_elementType_0(Type_t * value)
	{
		___elementType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elementType_0), (void*)value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Runtime.CompilerServices.InternalsVisibleToAttribute::_assemblyName
	String_t* ____assemblyName_0;
	// System.Boolean System.Runtime.CompilerServices.InternalsVisibleToAttribute::_allInternalsVisible
	bool ____allInternalsVisible_1;

public:
	inline static int32_t get_offset_of__assemblyName_0() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____assemblyName_0)); }
	inline String_t* get__assemblyName_0() const { return ____assemblyName_0; }
	inline String_t** get_address_of__assemblyName_0() { return &____assemblyName_0; }
	inline void set__assemblyName_0(String_t* value)
	{
		____assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____assemblyName_0), (void*)value);
	}

	inline static int32_t get_offset_of__allInternalsVisible_1() { return static_cast<int32_t>(offsetof(InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C, ____allInternalsVisible_1)); }
	inline bool get__allInternalsVisible_1() const { return ____allInternalsVisible_1; }
	inline bool* get_address_of__allInternalsVisible_1() { return &____allInternalsVisible_1; }
	inline void set__allInternalsVisible_1(bool value)
	{
		____allInternalsVisible_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// AOT.MonoPInvokeCallbackAttribute
struct MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.LowLevel.Unsafe.NativeContainerAttribute
struct NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.LowLevel.Unsafe.NativeContainerIsAtomicWriteOnlyAttribute
struct NativeContainerIsAtomicWriteOnlyAttribute_t2DB74DA0C416DD897E6F282B6F604646E0B344AB  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestrictionAttribute
struct NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.LowLevel.Unsafe.NativeSetThreadIndexAttribute
struct NativeSetThreadIndexAttribute_t7681C9225114E2B1478DE516F9FE1CD44B3681E8  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.NotBurstCompatibleAttribute
struct NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.UnsafeValueTypeAttribute
struct UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Unity.Burst.BurstCompiler/StaticTypeReinitAttribute
struct StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type Unity.Burst.BurstCompiler/StaticTypeReinitAttribute::reinitType
	Type_t * ___reinitType_0;

public:
	inline static int32_t get_offset_of_reinitType_0() { return static_cast<int32_t>(offsetof(StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2, ___reinitType_0)); }
	inline Type_t * get_reinitType_0() const { return ___reinitType_0; }
	inline Type_t ** get_address_of_reinitType_0() { return &___reinitType_0; }
	inline void set_reinitType_0(Type_t * value)
	{
		___reinitType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reinitType_0), (void*)value);
	}
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.CallingConvention
struct CallingConvention_tCD05DC1A211D9713286784F4DDDE1BA18B839924 
{
public:
	// System.Int32 System.Runtime.InteropServices.CallingConvention::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CallingConvention_tCD05DC1A211D9713286784F4DDDE1BA18B839924, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.InteropServices.CharSet
struct CharSet_tF37E3433B83409C49A52A325333BFBC08ACD6E4B 
{
public:
	// System.Int32 System.Runtime.InteropServices.CharSet::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharSet_tF37E3433B83409C49A52A325333BFBC08ACD6E4B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Burst.FloatMode
struct FloatMode_t38741ACC50724A284056372B5D90095D40ACB1E4 
{
public:
	// System.Int32 Unity.Burst.FloatMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatMode_t38741ACC50724A284056372B5D90095D40ACB1E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Burst.FloatPrecision
struct FloatPrecision_tF6B76A9F4B20E5525B4B38902AA661AAB9E199F5 
{
public:
	// System.Int32 Unity.Burst.FloatPrecision::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatPrecision_tF6B76A9F4B20E5525B4B38902AA661AAB9E199F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RuntimeInitializeLoadType
struct RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5 
{
public:
	// System.Int32 UnityEngine.RuntimeInitializeLoadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RuntimeInitializeLoadType_t78BE0E3079AE8955C97DF6A9814A6E6BFA146EA5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// Unity.Burst.FloatMode Unity.Burst.BurstCompileAttribute::<FloatMode>k__BackingField
	int32_t ___U3CFloatModeU3Ek__BackingField_0;
	// Unity.Burst.FloatPrecision Unity.Burst.BurstCompileAttribute::<FloatPrecision>k__BackingField
	int32_t ___U3CFloatPrecisionU3Ek__BackingField_1;
	// System.Nullable`1<System.Boolean> Unity.Burst.BurstCompileAttribute::_compileSynchronously
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ____compileSynchronously_2;
	// System.String[] Unity.Burst.BurstCompileAttribute::<Options>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFloatModeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3CFloatModeU3Ek__BackingField_0)); }
	inline int32_t get_U3CFloatModeU3Ek__BackingField_0() const { return ___U3CFloatModeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFloatModeU3Ek__BackingField_0() { return &___U3CFloatModeU3Ek__BackingField_0; }
	inline void set_U3CFloatModeU3Ek__BackingField_0(int32_t value)
	{
		___U3CFloatModeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFloatPrecisionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3CFloatPrecisionU3Ek__BackingField_1)); }
	inline int32_t get_U3CFloatPrecisionU3Ek__BackingField_1() const { return ___U3CFloatPrecisionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CFloatPrecisionU3Ek__BackingField_1() { return &___U3CFloatPrecisionU3Ek__BackingField_1; }
	inline void set_U3CFloatPrecisionU3Ek__BackingField_1(int32_t value)
	{
		___U3CFloatPrecisionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__compileSynchronously_2() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ____compileSynchronously_2)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get__compileSynchronously_2() const { return ____compileSynchronously_2; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of__compileSynchronously_2() { return &____compileSynchronously_2; }
	inline void set__compileSynchronously_2(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		____compileSynchronously_2 = value;
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3COptionsU3Ek__BackingField_3)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COptionsU3Ek__BackingField_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D  : public PreserveAttribute_tD3CDF1454F8E64CEF59CF7094B45BBACE2C69948
{
public:
	// UnityEngine.RuntimeInitializeLoadType UnityEngine.RuntimeInitializeOnLoadMethodAttribute::m_LoadType
	int32_t ___m_LoadType_0;

public:
	inline static int32_t get_offset_of_m_LoadType_0() { return static_cast<int32_t>(offsetof(RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D, ___m_LoadType_0)); }
	inline int32_t get_m_LoadType_0() const { return ___m_LoadType_0; }
	inline int32_t* get_address_of_m_LoadType_0() { return &___m_LoadType_0; }
	inline void set_m_LoadType_0(int32_t value)
	{
		___m_LoadType_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute
struct UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Runtime.InteropServices.CallingConvention System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::m_callingConvention
	int32_t ___m_callingConvention_0;
	// System.Runtime.InteropServices.CharSet System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::CharSet
	int32_t ___CharSet_1;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::BestFitMapping
	bool ___BestFitMapping_2;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::ThrowOnUnmappableChar
	bool ___ThrowOnUnmappableChar_3;
	// System.Boolean System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::SetLastError
	bool ___SetLastError_4;

public:
	inline static int32_t get_offset_of_m_callingConvention_0() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___m_callingConvention_0)); }
	inline int32_t get_m_callingConvention_0() const { return ___m_callingConvention_0; }
	inline int32_t* get_address_of_m_callingConvention_0() { return &___m_callingConvention_0; }
	inline void set_m_callingConvention_0(int32_t value)
	{
		___m_callingConvention_0 = value;
	}

	inline static int32_t get_offset_of_CharSet_1() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___CharSet_1)); }
	inline int32_t get_CharSet_1() const { return ___CharSet_1; }
	inline int32_t* get_address_of_CharSet_1() { return &___CharSet_1; }
	inline void set_CharSet_1(int32_t value)
	{
		___CharSet_1 = value;
	}

	inline static int32_t get_offset_of_BestFitMapping_2() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___BestFitMapping_2)); }
	inline bool get_BestFitMapping_2() const { return ___BestFitMapping_2; }
	inline bool* get_address_of_BestFitMapping_2() { return &___BestFitMapping_2; }
	inline void set_BestFitMapping_2(bool value)
	{
		___BestFitMapping_2 = value;
	}

	inline static int32_t get_offset_of_ThrowOnUnmappableChar_3() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___ThrowOnUnmappableChar_3)); }
	inline bool get_ThrowOnUnmappableChar_3() const { return ___ThrowOnUnmappableChar_3; }
	inline bool* get_address_of_ThrowOnUnmappableChar_3() { return &___ThrowOnUnmappableChar_3; }
	inline void set_ThrowOnUnmappableChar_3(bool value)
	{
		___ThrowOnUnmappableChar_3 = value;
	}

	inline static int32_t get_offset_of_SetLastError_4() { return static_cast<int32_t>(offsetof(UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F, ___SetLastError_4)); }
	inline bool get_SetLastError_4() const { return ___SetLastError_4; }
	inline bool* get_address_of_SetLastError_4() { return &___SetLastError_4; }
	inline void set_SetLastError_4(bool value)
	{
		___SetLastError_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9 (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * __this, String_t* ___assemblyName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompiler/StaticTypeReinitAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1 (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * __this, Type_t * ___toReinit0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.UnmanagedFunctionPointerAttribute::.ctor(System.Runtime.InteropServices.CallingConvention)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7 (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * __this, int32_t ___callingConvention0, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompileAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628 (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * __this, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompileAttribute::set_CompileSynchronously(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025 (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void AOT.MonoPInvokeCallbackAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344 (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstDiscardAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9 (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.BurstCompatibleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.BurstCompatibleAttribute::set_GenericTypeArguments(System.Type[])
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * __this, TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167 (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.NotBurstCompatibleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63 (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.CreatePropertyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68 (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633 (NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988 (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestrictionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_m34A120993044E67D397DB90FF22BBF030B5C19DC (IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerIsAtomicWriteOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeContainerIsAtomicWriteOnlyAttribute__ctor_m646CF821D0504179A7A0421EEB1E88341A794846 (NativeContainerIsAtomicWriteOnlyAttribute_t2DB74DA0C416DD897E6F282B6F604646E0B344AB * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.LowLevel.Unsafe.NativeSetThreadIndexAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeSetThreadIndexAttribute__ctor_m5C5D75B3D697718961FC0C74770B9ABFFD849C4E (NativeSetThreadIndexAttribute_t7681C9225114E2B1478DE516F9FE1CD44B3681E8 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.FixedBufferAttribute::.ctor(System.Type,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FixedBufferAttribute__ctor_m7767B7379CFADD0D12551A5A891BF1916A07BE51 (FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C * __this, Type_t * ___elementType0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.UnsafeValueTypeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeValueTypeAttribute__ctor_mA5A3D4443A6B4BE3B31E8A8919809719991A7EC4 (UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * __this, int32_t ___loadType0, const RuntimeMethod* method);
static void Unity_Collections_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Hash128Long_00000722U24BurstDirectCall_tF9D993B09CC7EDAA9AE8BA4994C43ADB573A579A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Hash64Long_0000071BU24BurstDirectCall_t631230C77478A8ECEC15538227FC959EB3CE07FF_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Try_000006E4U24BurstDirectCall_t9065BD9F58F9F94D743B0378F7BADFD2382570C2_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Try_0000097CU24BurstDirectCall_t812D64B490B18957289F162D28186E4384B9066C_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Try_0000098AU24BurstDirectCall_t6CD59618A003C605DBBCA95CF255CE53693BE1EE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[0];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x53\x63\x65\x6E\x65\x73"), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[2];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[4];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[5];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x42\x75\x72\x73\x74\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[6];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x73\x2E\x42\x75\x72\x73\x74\x43\x6F\x6D\x70\x61\x74\x69\x62\x69\x6C\x69\x74\x79\x54\x65\x73\x74\x43\x6F\x64\x65\x47\x65\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[7];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x73\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[8];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[9];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x43\x6F\x64\x65\x47\x65\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[10];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[11];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[12];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x45\x64\x69\x74\x6F\x72\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[13];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x65\x6E\x64\x65\x72\x69\x6E\x67\x2E\x48\x79\x62\x72\x69\x64"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[14];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[15];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[16];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x53\x63\x65\x6E\x65\x73\x2E\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[17];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Hash64Long_0000071BU24BurstDirectCall_t631230C77478A8ECEC15538227FC959EB3CE07FF_0_0_0_var), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[18];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Try_000006E4U24BurstDirectCall_t9065BD9F58F9F94D743B0378F7BADFD2382570C2_0_0_0_var), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[19];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Try_0000098AU24BurstDirectCall_t6CD59618A003C605DBBCA95CF255CE53693BE1EE_0_0_0_var), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[20];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Try_0000097CU24BurstDirectCall_t812D64B490B18957289F162D28186E4384B9066C_0_0_0_var), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[21];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x45\x6E\x74\x69\x74\x69\x65\x73\x2E\x50\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[22];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x6D\x70\x6C\x65\x73\x2E\x47\x72\x69\x64\x50\x61\x74\x68\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[23];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65\x2E\x49\x4F\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 * tmp = (StaticTypeReinitAttribute_t44EBC424CAE299F7DA16C5FAA87819AB80848CD2 *)cache->attributes[24];
		StaticTypeReinitAttribute__ctor_m208E150603714E94BFBF65BFC68CEB220D5414D1(tmp, il2cpp_codegen_type_get_object(Hash128Long_00000722U24BurstDirectCall_tF9D993B09CC7EDAA9AE8BA4994C43ADB573A579A_0_0_0_var), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[25];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x52\x65\x6E\x64\x65\x72\x69\x6E\x67\x2E\x4E\x61\x74\x69\x76\x65"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[26];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x47\x61\x6D\x65\x53\x61\x76\x65\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[27];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x47\x61\x6D\x65\x53\x61\x76\x65"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[28];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x43\x6F\x72\x65\x2E\x54\x65\x73\x74\x73"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[29];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x54\x69\x6E\x79\x2E\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C * tmp = (InternalsVisibleToAttribute_t1D9772A02892BAC440952F880A43C257E6C3E68C *)cache->attributes[30];
		InternalsVisibleToAttribute__ctor_m420071A75DCEEC72356490C64B4B0B9270DA32B9(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x79\x2E\x52\x75\x6E\x74\x69\x6D\x65\x2E\x55\x6E\x69\x74\x79\x49\x6E\x73\x74\x61\x6E\x63\x65"), NULL);
	}
}
static void EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * tmp = (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D(tmp, NULL);
	}
}
static void IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * tmp = (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D(tmp, NULL);
	}
}
static void IsUnmanagedAttribute_tC3711779D00EFADD8F826DD8391CFD36FC1B912F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 * tmp = (EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6 *)cache->attributes[0];
		EmbeddedAttribute__ctor_mE19BFF00D03833D46FC6E6B83A9C5E708E7E665D(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_AllocateBlock_m17083F1A92463339C4EA1D53C12EFF5DFC5C4213(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Allocate_m5B9C4A6A6800F08238BC20FA6AE3EA55CB878487(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Allocate_m5F989B2F908DD8683932B5572669FCF3EC642817(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_AllocateStruct_mD4B50E13A746F4F3051DAACDF86CCA38434E03AB(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_FreeBlock_m3F4FDC6BAD76C6EB87630B80A41CDC3343C6A547(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Free_m28A15947CC70B0815DB7D4BB8E1F3734C863B0E0(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Free_m5CF662F92A7F51AC32BBC2B911A958E913C27951(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F * tmp = (UnmanagedFunctionPointerAttribute_t3361C55E19F9905230FD9C1691B0FE0FD341B43F *)cache->attributes[0];
		UnmanagedFunctionPointerAttribute__ctor_m4EE271163D421DF82BBCD7D91ED68D8EE26544F7(tmp, 2LL, NULL);
	}
}
static void StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
}
static void StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[1];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void Try_0000097CU24BurstDirectCall_t812D64B490B18957289F162D28186E4384B9066C_CustomAttributesCacheGenerator_Try_0000097CU24BurstDirectCall_GetFunctionPointerDiscard_m2C0C37505D3506A20017DD7AAF0325981C13843B(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
}
static void SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[1];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
		BurstCompileAttribute_set_CompileSynchronously_mB83EBCE3125C95CB2A5B429B07F66F2310056025(tmp, true, NULL);
	}
}
static void SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void Try_0000098AU24BurstDirectCall_t6CD59618A003C605DBBCA95CF255CE53693BE1EE_CustomAttributesCacheGenerator_Try_0000098AU24BurstDirectCall_GetFunctionPointerDiscard_m93981F95497ABE761434CAD6EFCECEF5AECF4C49(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 236LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_U3CGenericTypeArgumentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 224LL, NULL);
	}
}
static void CollectionHelper_tC31F1DBFA15312C1B0C4806A93AD460DBBC4FFB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void CollectionHelper_tC31F1DBFA15312C1B0C4806A93AD460DBBC4FFB9_CustomAttributesCacheGenerator_CollectionHelper_CreateNativeArray_mED8C45F5AE29CC89FBD356682AB33C8125154F84(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator_FixedList_PaddingBytes_m5094730BB004E6550AC23A4E96392121DB77FA7C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList32BytesDebugView_1_t21779F4BD4450CD45B481796502C35BAD7A7E4A9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList32BytesDebugView_1_t21779F4BD4450CD45B481796502C35BAD7A7E4A9_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_Equals_m3AE49BA608D98AFD99FAE97509B7A341DD7A4B5C(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList64BytesDebugView_1_t6B9BE5EABF537E5C3D57F0702FB979A60B91A443_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList64BytesDebugView_1_t6B9BE5EABF537E5C3D57F0702FB979A60B91A443_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_Equals_m370E69052A57C18F0FC4A26AC65588E3E7E5E562(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList128BytesDebugView_1_t8C40E82934244DDBB383E0CA35A81F2B00E40CE4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[0];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList128BytesDebugView_1_t8C40E82934244DDBB383E0CA35A81F2B00E40CE4_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[2];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_Equals_m98E0AA2048211F341CA8272EDE35FD28E1A024CD(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList512BytesDebugView_1_tF41AD0D481A169839B889922C2E0A3424163B79F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[2];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList512BytesDebugView_1_tF41AD0D481A169839B889922C2E0A3424163B79F_0_0_0_var), NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_Equals_m360A4228798FD89819FC89B23A6095312A80C7F2(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FixedList4096BytesDebugView_1_t70625E554752F30063D44798522354A66D3696D0_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(FixedList4096BytesDebugView_1_t70625E554752F30063D44798522354A66D3696D0_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_buffer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_Equals_m32CF680F6D80FB99E1C8659269C9313745A41312(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
}
static void FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8____Length_PropertyInfo(CustomAttributesCache* cache)
{
	{
		CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 * tmp = (CreatePropertyAttribute_tA337B6139835FF6B23F9F2E7A7434144BC7DAFA9 *)cache->attributes[0];
		CreatePropertyAttribute__ctor_mE5AD2035801359532F3ABA06B2E67CB664930A68(tmp, NULL);
	}
}
static void FixedBytes16_t6F3DA12A9BFDF36F711B4EE37C752953B2125C37_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes30_tCCED5D93977503BC77E394236A7023519785DC80_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes62_tC77E73BB842294F4F9A4807C0857B8F332D52A72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes126_t10EF6170BCD75D7F5463F602828B457A3C98EC39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes510_t71D0D7868DFFD20E428EBBB5F8AF60636FD16D8F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void FixedBytes4094_tA9F73D0C7311C392EEC2D43FCABB33894D30A150_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Memory_tCB9F7958AD19492CF5CC6A72E2B7635AECE2A1E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator_Unmanaged_Free_m8F82A7CFBFD0519251F3AEF8C4B1B536633A2F2F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator_Array_Resize_mE8E09A071494DB398D0B83EE15E49A55F0FFBE32(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_IndexOf_mCE30A61E2C1F24BC28595FB319606498CA5BF353(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_IndexOf_m997CFE5102EC891FF29551BACE1FDAC937852035(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_Reinterpret_m558EC85E279AC0E9E7D6B85D686F03C045C16B35(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_Initialize_m7E007E2F2EFD5F322B8CA4AC019E2CA500182E0B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeHashMap_2_tB0C1FB60253817C4E140EC97A3E3A0AB61A63F2A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeHashMapDebuggerTypeProxy_2_tB7FCF24B1602A95C822E7E65CEA35C45410A02D7_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 * tmp = (NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 *)cache->attributes[0];
		NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633(tmp, NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x75\x6E\x74\x20\x3D\x20\x7B\x6D\x5F\x48\x61\x73\x68\x4D\x61\x70\x44\x61\x74\x61\x2E\x43\x6F\x75\x6E\x74\x28\x29\x7D\x2C\x20\x43\x61\x70\x61\x63\x69\x74\x79\x20\x3D\x20\x7B\x6D\x5F\x48\x61\x73\x68\x4D\x61\x70\x44\x61\x74\x61\x2E\x43\x61\x70\x61\x63\x69\x74\x79\x7D\x2C\x20\x49\x73\x43\x72\x65\x61\x74\x65\x64\x20\x3D\x20\x7B\x6D\x5F\x48\x61\x73\x68\x4D\x61\x70\x44\x61\x74\x61\x2E\x49\x73\x43\x72\x65\x61\x74\x65\x64\x7D\x2C\x20\x49\x73\x45\x6D\x70\x74\x79\x20\x3D\x20\x7B\x49\x73\x45\x6D\x70\x74\x79\x7D"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[2];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(NativeHashMapDebuggerTypeProxy_2_tB7FCF24B1602A95C822E7E65CEA35C45410A02D7_0_0_0_var), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[3];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[4];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void NativeHashMapExtensions_t702C007DD0DFA9C13E91D1D2C979B0D7B1FBBE20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NativeHashMapExtensions_t702C007DD0DFA9C13E91D1D2C979B0D7B1FBBE20_CustomAttributesCacheGenerator_NativeHashMapExtensions_Unique_m4F13ED2A6F66AF5291B6D2DB33843C8C947D6F04(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeListDebugView_1_t5A689FBCF98D06A6219070C4B1BFB971776DE7BD_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[1];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[2];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x6E\x67\x74\x68\x20\x3D\x20\x7B\x4C\x65\x6E\x67\x74\x68\x7D"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[3];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(NativeListDebugView_1_t5A689FBCF98D06A6219070C4B1BFB971776DE7BD_0_0_0_var), NULL);
	}
	{
		NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 * tmp = (NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 *)cache->attributes[4];
		NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633(tmp, NULL);
	}
}
static void NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_m_ListData(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_NativeList_1_Initialize_m2B739C8F3D1596B40144F1CE136176B6CCC07F5E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_NativeList_1_Add_m05C67D2E1D47B1A07F2154B018C1B0C9B2C8F43C____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66 * tmp = (IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m34A120993044E67D397DB90FF22BBF030B5C19DC(tmp, NULL);
	}
}
static void NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_NativeList_1_CopyFrom_m6E0085D2BCE05D61607B2CC42812B68CB1629DE1(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x60\x43\x6F\x70\x79\x46\x72\x6F\x6D\x4E\x42\x43\x60\x20\x66\x72\x6F\x6D\x20\x60\x55\x6E\x69\x74\x79\x2E\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x73\x2E\x4E\x6F\x74\x42\x75\x72\x73\x74\x43\x6F\x6D\x70\x61\x74\x69\x62\x6C\x65\x60\x20\x6E\x61\x6D\x65\x73\x70\x61\x63\x65\x20\x69\x6E\x73\x74\x65\x61\x64\x2E\x20\x28\x52\x65\x6D\x6F\x76\x65\x64\x41\x66\x74\x65\x72\x20\x32\x30\x32\x31\x2D\x30\x36\x2D\x32\x32\x29"), false, NULL);
	}
}
static void NativeMultiHashMapIterator_1_t0912B9817A87A13F993FF384A1EDD119E8A57BC7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void NativeQueueBlockPool_t99FC4CC3D703AAB1870D23882494A877BD3B884E_CustomAttributesCacheGenerator_NativeQueueBlockPool_AppDomainOnDomainUnload_m07AB4B933463598F22A818F971FDD43BD2C67A27(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA_CustomAttributesCacheGenerator_NativeQueueData_AllocateWriteBlockMT_mB4BDD2D6AF74A94FF984BF9137C085DBA973915C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA_CustomAttributesCacheGenerator_NativeQueueData_AllocateQueue_mBAB8054B1126E2C4F8B70FE9A653895060E3413F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeQueue_1_t75C89FEBA67AE1BDB255C95E499FF6A13FC7AE94_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 * tmp = (NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 *)cache->attributes[1];
		NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633(tmp, NULL);
	}
}
static void NativeQueue_1_t75C89FEBA67AE1BDB255C95E499FF6A13FC7AE94_CustomAttributesCacheGenerator_m_Buffer(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void NativeQueue_1_t75C89FEBA67AE1BDB255C95E499FF6A13FC7AE94_CustomAttributesCacheGenerator_m_QueuePool(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 * tmp = (NativeContainerAttribute_t3894E43A49A7B3CED9F729854E36D5683692D3D6 *)cache->attributes[1];
		NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633(tmp, NULL);
	}
	{
		NativeContainerIsAtomicWriteOnlyAttribute_t2DB74DA0C416DD897E6F282B6F604646E0B344AB * tmp = (NativeContainerIsAtomicWriteOnlyAttribute_t2DB74DA0C416DD897E6F282B6F604646E0B344AB *)cache->attributes[2];
		NativeContainerIsAtomicWriteOnlyAttribute__ctor_m646CF821D0504179A7A0421EEB1E88341A794846(tmp, NULL);
	}
}
static void ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator_m_Buffer(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator_m_QueuePool(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator_m_ThreadIndex(CustomAttributesCache* cache)
{
	{
		NativeSetThreadIndexAttribute_t7681C9225114E2B1478DE516F9FE1CD44B3681E8 * tmp = (NativeSetThreadIndexAttribute_t7681C9225114E2B1478DE516F9FE1CD44B3681E8 *)cache->attributes[0];
		NativeSetThreadIndexAttribute__ctor_m5C5D75B3D697718961FC0C74770B9ABFFD849C4E(tmp, NULL);
	}
}
static void NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_m94F9CBC794D5257202110A48F572769FD59DAB57(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_m4A1D07894C3529DDC3C9FF88CC58DC328001B599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_mF4044CE1A8BE1E89202487456A0A7CC55221F2D7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_m74921D26EC2A3A0397E5055CFABACF081641F163(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_IntroSort_mD2490A86302D7B36427624036B1E1DA172EC9FE2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(DefaultComparer_1_t3DC8BC4ACE4C41FA29BA8812B865055AF1D0B494_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void DefaultComparer_1_t0B2E8AB90D0473367FCB35B9B43ED57A630D14DF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnmanagedArray_1_t17817821392C6FC7C52505CDC6F87AE9D52CE526_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[1];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E * tmp = (MonoPInvokeCallbackAttribute_t99C8CC5CE6CC69C51F99A6CE88F4F792D4777B2E *)cache->attributes[0];
		MonoPInvokeCallbackAttribute__ctor_mA23FC56D16F76D90C3F76B1C41D0765BB21FE344(tmp, il2cpp_codegen_type_get_object(TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_0_0_0_var), NULL);
	}
}
static void MemoryBlock_tFE475C7AFDA538E4DE9FF1F547E14BD0E7D0C073_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void Try_000006E4U24BurstDirectCall_t9065BD9F58F9F94D743B0378F7BADFD2382570C2_CustomAttributesCacheGenerator_Try_000006E4U24BurstDirectCall_GetFunctionPointerDiscard_mA84B7EC78BABDEF9453035B2DD6B5FA73560CACE(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[1];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[2];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[3];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void Hash64Long_0000071BU24BurstDirectCall_t631230C77478A8ECEC15538227FC959EB3CE07FF_CustomAttributesCacheGenerator_Hash64Long_0000071BU24BurstDirectCall_GetFunctionPointerDiscard_m86EA21E2A4B5EC643E5638EE6296418CE32FC2AA(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void Hash128Long_00000722U24BurstDirectCall_tF9D993B09CC7EDAA9AE8BA4994C43ADB573A579A_CustomAttributesCacheGenerator_Hash128Long_00000722U24BurstDirectCall_GetFunctionPointerDiscard_mA651E5D271E4D3BF37B8E4F34D0AED96EACEA810(CustomAttributesCache* cache)
{
	{
		BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 * tmp = (BurstDiscardAttribute_t1E78C4B5434B1BD180074F2C42072A8555FCF538 *)cache->attributes[0];
		BurstDiscardAttribute__ctor_m4B4E3E0DA0ED6C9463CE4FDEE7EAA4F4CA4DDDF9(tmp, NULL);
	}
}
static void Extensions_tDF2B73FA398636836DD519BCCC8764151795570E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_tDF2B73FA398636836DD519BCCC8764151795570E_CustomAttributesCacheGenerator_Extensions_CopyFromNBC_m051F1FD744A14BDBD8375579FD4F6CD2240CE88A(CustomAttributesCache* cache)
{
	{
		NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 * tmp = (NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163 *)cache->attributes[0];
		NotBurstCompatibleAttribute__ctor_m1A03DFE74AB05DBB234C15F707ABD0AC4C91ED63(tmp, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x6E\x67\x74\x68\x20\x3D\x20\x7B\x4C\x65\x6E\x67\x74\x68\x7D\x2C\x20\x43\x61\x70\x61\x63\x69\x74\x79\x20\x3D\x20\x7B\x43\x61\x70\x61\x63\x69\x74\x79\x7D\x2C\x20\x49\x73\x43\x72\x65\x61\x74\x65\x64\x20\x3D\x20\x7B\x49\x73\x43\x72\x65\x61\x74\x65\x64\x7D\x2C\x20\x49\x73\x45\x6D\x70\x74\x79\x20\x3D\x20\x7B\x49\x73\x45\x6D\x70\x74\x79\x7D"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x74\x79\x70\x65\x64\x20\x55\x6E\x73\x61\x66\x65\x4C\x69\x73\x74\x20\x69\x73\x20\x64\x65\x70\x72\x65\x63\x61\x74\x65\x64\x2C\x20\x70\x6C\x65\x61\x73\x65\x20\x75\x73\x65\x20\x55\x6E\x73\x61\x66\x65\x4C\x69\x73\x74\x3C\x54\x3E\x20\x69\x6E\x73\x74\x65\x61\x64\x2E\x20\x28\x52\x65\x6D\x6F\x76\x65\x64\x41\x66\x74\x65\x72\x20\x32\x30\x32\x31\x2D\x30\x35\x2D\x31\x38\x29"), false, NULL);
	}
}
static void UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA_CustomAttributesCacheGenerator_Ptr(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void NativeListUnsafeUtility_t92F5085BBA58C1257A91EC8D3A5AE825956F4C29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void NativeListUnsafeUtility_t92F5085BBA58C1257A91EC8D3A5AE825956F4C29_CustomAttributesCacheGenerator_NativeListUnsafeUtility_GetUnsafePtr_m832544E8198268B64D9ABBCE386CBAA9CF13F1CB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
	}
}
static void UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_firstFreeTLS(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C * tmp = (FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C *)cache->attributes[0];
		FixedBufferAttribute__ctor_m7767B7379CFADD0D12551A5A891BF1916A07BE51(tmp, il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var), 2048LL, NULL);
	}
}
static void UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_AllocateHashMap_m6CDCB53AD6ED9510CF13485EB0B64D90AE6718AA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_ReallocateHashMap_m72D130E0256E58E5073EDD1D158A25D78405AAED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_CalculateDataSize_m3BABD79ACAE778C6C7FFCAA132D33FFBFD3ED50D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_GetKeyArray_mD0543CA9391EA799CB6D1D622639001AB9641883(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void U3CfirstFreeTLSU3Ee__FixedBuffer_t157CC73718D14B7A8CBAE223B4682EF928527F0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC * tmp = (UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC *)cache->attributes[1];
		UnsafeValueTypeAttribute__ctor_mA5A3D4443A6B4BE3B31E8A8919809719991A7EC4(tmp, NULL);
	}
}
static void UnsafeHashMapBase_2_tB4D12A60E2C9DFCA7F7A66141D6A7927DB54191F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void KeyValue_2_t42FF489A68A1E1237C309247971BF56AB04C44F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x20\x3D\x20\x7B\x4B\x65\x79\x7D\x2C\x20\x56\x61\x6C\x75\x65\x20\x3D\x20\x7B\x56\x61\x6C\x75\x65\x7D"), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[1];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeHashMap_2_tBE0E2F2F60F5A76DB4EC2C3D866FE3519B682860_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnsafeHashMapDebuggerTypeProxy_2_t39B21CBB744848DA31F2DF015FB564FA25632F1B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[1];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x75\x6E\x74\x20\x3D\x20\x7B\x43\x6F\x75\x6E\x74\x28\x29\x7D\x2C\x20\x43\x61\x70\x61\x63\x69\x74\x79\x20\x3D\x20\x7B\x43\x61\x70\x61\x63\x69\x74\x79\x7D\x2C\x20\x49\x73\x43\x72\x65\x61\x74\x65\x64\x20\x3D\x20\x7B\x49\x73\x43\x72\x65\x61\x74\x65\x64\x7D\x2C\x20\x49\x73\x45\x6D\x70\x74\x79\x20\x3D\x20\x7B\x49\x73\x45\x6D\x70\x74\x79\x7D"), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 2);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[2];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[3];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(UnsafeHashMapDebuggerTypeProxy_2_t39B21CBB744848DA31F2DF015FB564FA25632F1B_0_0_0_var), NULL);
	}
}
static void UnsafeHashMap_2_tBE0E2F2F60F5A76DB4EC2C3D866FE3519B682860_CustomAttributesCacheGenerator_m_Buffer(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnsafeListTDebugView_1_tAB2A35CE4BDB7898FFF42F088207433052DFF310_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F * tmp = (DebuggerDisplayAttribute_tA5070C1A6CAB579DAC66A469530D946F6F42727F *)cache->attributes[0];
		DebuggerDisplayAttribute__ctor_m870C3A98DA4C9FA7FD4411169AF30C55A90B9988(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x6E\x67\x74\x68\x20\x3D\x20\x7B\x4C\x65\x6E\x67\x74\x68\x7D\x2C\x20\x43\x61\x70\x61\x63\x69\x74\x79\x20\x3D\x20\x7B\x43\x61\x70\x61\x63\x69\x74\x79\x7D\x2C\x20\x49\x73\x43\x72\x65\x61\x74\x65\x64\x20\x3D\x20\x7B\x49\x73\x43\x72\x65\x61\x74\x65\x64\x7D\x2C\x20\x49\x73\x45\x6D\x70\x74\x79\x20\x3D\x20\x7B\x49\x73\x45\x6D\x70\x74\x79\x7D"), NULL);
	}
	{
		DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 * tmp = (DebuggerTypeProxyAttribute_t20C961369DAE0E16D87B752F1C04F16FC3B90014 *)cache->attributes[1];
		DebuggerTypeProxyAttribute__ctor_mF05A9CF9DC4A3F95F05938CF6CBF45CC32CF5167(tmp, il2cpp_codegen_type_get_object(UnsafeListTDebugView_1_tAB2A35CE4BDB7898FFF42F088207433052DFF310_0_0_0_var), NULL);
	}
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[2];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[3];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_Ptr(CustomAttributesCache* cache)
{
	{
		NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 * tmp = (NativeDisableUnsafePtrRestrictionAttribute_tEA96E4FE8E1010BE2706F6CEC447E8C55A29DFC0 *)cache->attributes[0];
		NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE(tmp, NULL);
	}
}
static void UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_length(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x4C\x65\x6E\x67\x74\x68\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x28\x55\x6E\x69\x74\x79\x55\x70\x67\x72\x61\x64\x61\x62\x6C\x65\x29\x20\x2D\x3E\x20\x4C\x65\x6E\x67\x74\x68"), true, NULL);
	}
}
static void UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_capacity(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x43\x61\x70\x61\x63\x69\x74\x79\x20\x70\x72\x6F\x70\x65\x72\x74\x79\x20\x28\x55\x6E\x69\x74\x79\x55\x70\x67\x72\x61\x64\x61\x62\x6C\x65\x29\x20\x2D\x3E\x20\x43\x61\x70\x61\x63\x69\x74\x79"), true, NULL);
	}
}
static void UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_UnsafeList_1_Create_m81A5081F42B4EC21D78161D0AE136A0A3EC119C8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* _tmp_GenericTypeArguments = (TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755*)SZArrayNew(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755_il2cpp_TypeInfo_var, 1);
		(_tmp_GenericTypeArguments)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_type_get_object(AllocatorHandle_tAFA82A7B19AC002D983535C10C63DE0AD2EE3F1A_0_0_0_var));
		BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * tmp = (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 *)cache->attributes[0];
		BurstCompatibleAttribute__ctor_m5D7D5245014D5EF879BB02B137717AADED72CA4C(tmp, NULL);
		BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline(tmp, _tmp_GenericTypeArguments, NULL);
	}
}
static void UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_UnsafeList_1_Add_mACC40E8D9C35E0305603F8888C97D753EE324B8C____value0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66 * tmp = (IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m34A120993044E67D397DB90FF22BBF030B5C19DC(tmp, NULL);
	}
}
static void U24BurstDirectCallInitializer_tAC3E0907180450DFB40E146971FACAABE51C2F33_CustomAttributesCacheGenerator_U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C(CustomAttributesCache* cache)
{
	{
		RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D * tmp = (RuntimeInitializeOnLoadMethodAttribute_tDE87D2AA72896514411AC9F8F48A4084536BDC2D *)cache->attributes[0];
		RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516(tmp, 2LL, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Unity_Collections_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Unity_Collections_AttributeGenerators[133] = 
{
	EmbeddedAttribute_t0E30752C5EA9622DD2335FB26295952F020EE7B6_CustomAttributesCacheGenerator,
	IsReadOnlyAttribute_t9F9EA732286F464CB840464092920348A51E2F66_CustomAttributesCacheGenerator,
	IsUnmanagedAttribute_tC3711779D00EFADD8F826DD8391CFD36FC1B912F_CustomAttributesCacheGenerator,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator,
	TryFunction_t08422611F890148F40019711F54EFCB1AAEC5777_CustomAttributesCacheGenerator,
	StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator,
	SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator,
	BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator,
	NotBurstCompatibleAttribute_t08F1FF09483C9258E25C47BE5C4C7A6898F5B163_CustomAttributesCacheGenerator,
	CollectionHelper_tC31F1DBFA15312C1B0C4806A93AD460DBBC4FFB9_CustomAttributesCacheGenerator,
	FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator,
	FixedBytes16_t6F3DA12A9BFDF36F711B4EE37C752953B2125C37_CustomAttributesCacheGenerator,
	FixedBytes30_tCCED5D93977503BC77E394236A7023519785DC80_CustomAttributesCacheGenerator,
	FixedBytes62_tC77E73BB842294F4F9A4807C0857B8F332D52A72_CustomAttributesCacheGenerator,
	FixedBytes126_t10EF6170BCD75D7F5463F602828B457A3C98EC39_CustomAttributesCacheGenerator,
	FixedBytes510_t71D0D7868DFFD20E428EBBB5F8AF60636FD16D8F_CustomAttributesCacheGenerator,
	FixedBytes4094_tA9F73D0C7311C392EEC2D43FCABB33894D30A150_CustomAttributesCacheGenerator,
	Memory_tCB9F7958AD19492CF5CC6A72E2B7635AECE2A1E9_CustomAttributesCacheGenerator,
	Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator,
	Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator,
	NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator,
	NativeHashMap_2_tB0C1FB60253817C4E140EC97A3E3A0AB61A63F2A_CustomAttributesCacheGenerator,
	NativeHashMapExtensions_t702C007DD0DFA9C13E91D1D2C979B0D7B1FBBE20_CustomAttributesCacheGenerator,
	NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator,
	NativeMultiHashMapIterator_1_t0912B9817A87A13F993FF384A1EDD119E8A57BC7_CustomAttributesCacheGenerator,
	NativeQueueBlockPoolData_t77C8BA6277B22DBE22985DE7466BF43468DD34B2_CustomAttributesCacheGenerator,
	NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA_CustomAttributesCacheGenerator,
	NativeQueue_1_t75C89FEBA67AE1BDB255C95E499FF6A13FC7AE94_CustomAttributesCacheGenerator,
	ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator,
	NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator,
	DefaultComparer_1_t0B2E8AB90D0473367FCB35B9B43ED57A630D14DF_CustomAttributesCacheGenerator,
	UnmanagedArray_1_t17817821392C6FC7C52505CDC6F87AE9D52CE526_CustomAttributesCacheGenerator,
	RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator,
	MemoryBlock_tFE475C7AFDA538E4DE9FF1F547E14BD0E7D0C073_CustomAttributesCacheGenerator,
	xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator,
	Extensions_tDF2B73FA398636836DD519BCCC8764151795570E_CustomAttributesCacheGenerator,
	UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA_CustomAttributesCacheGenerator,
	NativeListUnsafeUtility_t92F5085BBA58C1257A91EC8D3A5AE825956F4C29_CustomAttributesCacheGenerator,
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator,
	U3CfirstFreeTLSU3Ee__FixedBuffer_t157CC73718D14B7A8CBAE223B4682EF928527F0C_CustomAttributesCacheGenerator,
	UnsafeHashMapBase_2_tB4D12A60E2C9DFCA7F7A66141D6A7927DB54191F_CustomAttributesCacheGenerator,
	KeyValue_2_t42FF489A68A1E1237C309247971BF56AB04C44F8_CustomAttributesCacheGenerator,
	UnsafeHashMap_2_tBE0E2F2F60F5A76DB4EC2C3D866FE3519B682860_CustomAttributesCacheGenerator,
	UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator,
	BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_U3CGenericTypeArgumentsU3Ek__BackingField,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_length,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_buffer,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_length,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_buffer,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_length,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_buffer,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_length,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_buffer,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_length,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_buffer,
	NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_m_ListData,
	NativeQueue_1_t75C89FEBA67AE1BDB255C95E499FF6A13FC7AE94_CustomAttributesCacheGenerator_m_Buffer,
	NativeQueue_1_t75C89FEBA67AE1BDB255C95E499FF6A13FC7AE94_CustomAttributesCacheGenerator_m_QueuePool,
	ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator_m_Buffer,
	ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator_m_QueuePool,
	ParallelWriter_tCAC8A8235C6F39B01946C88605EF61EBDACFCF85_CustomAttributesCacheGenerator_m_ThreadIndex,
	UnsafeList_t45363E05DB545743D4FBBA9793AA68E6A32634AA_CustomAttributesCacheGenerator_Ptr,
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_firstFreeTLS,
	UnsafeHashMap_2_tBE0E2F2F60F5A76DB4EC2C3D866FE3519B682860_CustomAttributesCacheGenerator_m_Buffer,
	UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_Ptr,
	UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_length,
	UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_capacity,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_AllocateBlock_m17083F1A92463339C4EA1D53C12EFF5DFC5C4213,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Allocate_m5B9C4A6A6800F08238BC20FA6AE3EA55CB878487,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Allocate_m5F989B2F908DD8683932B5572669FCF3EC642817,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_AllocateStruct_mD4B50E13A746F4F3051DAACDF86CCA38434E03AB,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_FreeBlock_m3F4FDC6BAD76C6EB87630B80A41CDC3343C6A547,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Free_m28A15947CC70B0815DB7D4BB8E1F3734C863B0E0,
	AllocatorManager_t24113DD8E4FDBE6A60D5D953A0B063A2B54ADE32_CustomAttributesCacheGenerator_AllocatorManager_Free_m5CF662F92A7F51AC32BBC2B911A958E913C27951,
	StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_Try_mAFB86C8BB80AFE8FB900919DC50FED309738F00B,
	StackAllocator_t1EF5DB6D623D13A2BEB7AC2D74E8AF9C58003849_CustomAttributesCacheGenerator_StackAllocator_TryU24BurstManaged_mD975231208A6EF3BCD65100DF173A3DBD696C89B,
	Try_0000097CU24BurstDirectCall_t812D64B490B18957289F162D28186E4384B9066C_CustomAttributesCacheGenerator_Try_0000097CU24BurstDirectCall_GetFunctionPointerDiscard_m2C0C37505D3506A20017DD7AAF0325981C13843B,
	SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_Try_m10F5613C20657F337568D1786E35E397A89595D8,
	SlabAllocator_tF0808054B859653F98F972483BE12089AF467E6A_CustomAttributesCacheGenerator_SlabAllocator_TryU24BurstManaged_m4D4AAC5D5146A8594091BF2E2FD41E74E059D2B5,
	Try_0000098AU24BurstDirectCall_t6CD59618A003C605DBBCA95CF255CE53693BE1EE_CustomAttributesCacheGenerator_Try_0000098AU24BurstDirectCall_GetFunctionPointerDiscard_m93981F95497ABE761434CAD6EFCECEF5AECF4C49,
	BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82_CustomAttributesCacheGenerator_BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A,
	CollectionHelper_tC31F1DBFA15312C1B0C4806A93AD460DBBC4FFB9_CustomAttributesCacheGenerator_CollectionHelper_CreateNativeArray_mED8C45F5AE29CC89FBD356682AB33C8125154F84,
	FixedList_t876B689B2431A9585FEE52E1F51699EDE5ADDB17_CustomAttributesCacheGenerator_FixedList_PaddingBytes_m5094730BB004E6550AC23A4E96392121DB77FA7C,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_Equals_m3AE49BA608D98AFD99FAE97509B7A341DD7A4B5C,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_Equals_m370E69052A57C18F0FC4A26AC65588E3E7E5E562,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_Equals_m98E0AA2048211F341CA8272EDE35FD28E1A024CD,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_Equals_m360A4228798FD89819FC89B23A6095312A80C7F2,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_Equals_m32CF680F6D80FB99E1C8659269C9313745A41312,
	Unmanaged_tA71911DAC6972F708D21E12EA1689486E171A8AB_CustomAttributesCacheGenerator_Unmanaged_Free_m8F82A7CFBFD0519251F3AEF8C4B1B536633A2F2F,
	Array_t75FDEB340BB6AD48CC32BBD39F096EF05E2C6719_CustomAttributesCacheGenerator_Array_Resize_mE8E09A071494DB398D0B83EE15E49A55F0FFBE32,
	NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_IndexOf_mCE30A61E2C1F24BC28595FB319606498CA5BF353,
	NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_IndexOf_m997CFE5102EC891FF29551BACE1FDAC937852035,
	NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_Reinterpret_m558EC85E279AC0E9E7D6B85D686F03C045C16B35,
	NativeArrayExtensions_t7EE90E3B15B32873A8C19511CDEB18EF287CB122_CustomAttributesCacheGenerator_NativeArrayExtensions_Initialize_m7E007E2F2EFD5F322B8CA4AC019E2CA500182E0B,
	NativeHashMapExtensions_t702C007DD0DFA9C13E91D1D2C979B0D7B1FBBE20_CustomAttributesCacheGenerator_NativeHashMapExtensions_Unique_m4F13ED2A6F66AF5291B6D2DB33843C8C947D6F04,
	NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_NativeList_1_Initialize_m2B739C8F3D1596B40144F1CE136176B6CCC07F5E,
	NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_NativeList_1_CopyFrom_m6E0085D2BCE05D61607B2CC42812B68CB1629DE1,
	NativeQueueBlockPool_t99FC4CC3D703AAB1870D23882494A877BD3B884E_CustomAttributesCacheGenerator_NativeQueueBlockPool_AppDomainOnDomainUnload_m07AB4B933463598F22A818F971FDD43BD2C67A27,
	NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA_CustomAttributesCacheGenerator_NativeQueueData_AllocateWriteBlockMT_mB4BDD2D6AF74A94FF984BF9137C085DBA973915C,
	NativeQueueData_t8AA40A9301F241A8104D9D86980F05D25FE967CA_CustomAttributesCacheGenerator_NativeQueueData_AllocateQueue_mBAB8054B1126E2C4F8B70FE9A653895060E3413F,
	NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_m94F9CBC794D5257202110A48F572769FD59DAB57,
	NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_m4A1D07894C3529DDC3C9FF88CC58DC328001B599,
	NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_mF4044CE1A8BE1E89202487456A0A7CC55221F2D7,
	NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_Sort_m74921D26EC2A3A0397E5055CFABACF081641F163,
	NativeSortExtension_tEBA66CBFA112F710B8F75941F66AA9DD8B8239B9_CustomAttributesCacheGenerator_NativeSortExtension_IntroSort_mD2490A86302D7B36427624036B1E1DA172EC9FE2,
	RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_Try_m6FA51FA56FBBAE6EB3C69E5E5DA79BE120712066,
	RewindableAllocator_t1F0D5802D564B3CCF9C054B93645A64E7EF2335B_CustomAttributesCacheGenerator_RewindableAllocator_TryU24BurstManaged_mFB5F38A7CBB467085EAFAB9430D4D4FFFB7C9AEE,
	Try_000006E4U24BurstDirectCall_t9065BD9F58F9F94D743B0378F7BADFD2382570C2_CustomAttributesCacheGenerator_Try_000006E4U24BurstDirectCall_GetFunctionPointerDiscard_mA84B7EC78BABDEF9453035B2DD6B5FA73560CACE,
	xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash64Long_mEEEA1DD334FF23CABC570F75866691BF29CA0CD7,
	xxHash3_t27DB950BD460657361C30F17C30B38BF2E69092B_CustomAttributesCacheGenerator_xxHash3_Hash128Long_m869E4682C4C9265FCFB625F57197A9B48B11675D,
	Hash64Long_0000071BU24BurstDirectCall_t631230C77478A8ECEC15538227FC959EB3CE07FF_CustomAttributesCacheGenerator_Hash64Long_0000071BU24BurstDirectCall_GetFunctionPointerDiscard_m86EA21E2A4B5EC643E5638EE6296418CE32FC2AA,
	Hash128Long_00000722U24BurstDirectCall_tF9D993B09CC7EDAA9AE8BA4994C43ADB573A579A_CustomAttributesCacheGenerator_Hash128Long_00000722U24BurstDirectCall_GetFunctionPointerDiscard_mA651E5D271E4D3BF37B8E4F34D0AED96EACEA810,
	Extensions_tDF2B73FA398636836DD519BCCC8764151795570E_CustomAttributesCacheGenerator_Extensions_CopyFromNBC_m051F1FD744A14BDBD8375579FD4F6CD2240CE88A,
	NativeListUnsafeUtility_t92F5085BBA58C1257A91EC8D3A5AE825956F4C29_CustomAttributesCacheGenerator_NativeListUnsafeUtility_GetUnsafePtr_m832544E8198268B64D9ABBCE386CBAA9CF13F1CB,
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_AllocateHashMap_m6CDCB53AD6ED9510CF13485EB0B64D90AE6718AA,
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_ReallocateHashMap_m72D130E0256E58E5073EDD1D158A25D78405AAED,
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_CalculateDataSize_m3BABD79ACAE778C6C7FFCAA132D33FFBFD3ED50D,
	UnsafeHashMapData_tBE4CCB191438EEFAF585DE1AD96D342825E64C82_CustomAttributesCacheGenerator_UnsafeHashMapData_GetKeyArray_mD0543CA9391EA799CB6D1D622639001AB9641883,
	UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_UnsafeList_1_Create_m81A5081F42B4EC21D78161D0AE136A0A3EC119C8,
	U24BurstDirectCallInitializer_tAC3E0907180450DFB40E146971FACAABE51C2F33_CustomAttributesCacheGenerator_U24BurstDirectCallInitializer_Initialize_mF922D4BC3A94E7362E69F29E806794E7BBB6972C,
	NativeList_1_t51069D13E7854A472F13FB83184B845754988B53_CustomAttributesCacheGenerator_NativeList_1_Add_m05C67D2E1D47B1A07F2154B018C1B0C9B2C8F43C____value0,
	UnsafeList_1_t9AD2E13DD0DF5E25EEAA8AAF8E8B38423F8ECCE2_CustomAttributesCacheGenerator_UnsafeList_1_Add_mACC40E8D9C35E0305603F8888C97D753EE324B8C____value0,
	FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9_CustomAttributesCacheGenerator_FixedList32Bytes_1_tC55A51AB8E4E27F0E9C7223CBE146440179966A9____Length_PropertyInfo,
	FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52_CustomAttributesCacheGenerator_FixedList64Bytes_1_t3A892D7C9CF990B9BD6345484699D79CD20F9C52____Length_PropertyInfo,
	FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38_CustomAttributesCacheGenerator_FixedList128Bytes_1_t97CA6F56261D641502C7D0D36C175BBD5ECD4A38____Length_PropertyInfo,
	FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5_CustomAttributesCacheGenerator_FixedList512Bytes_1_t00B1F74512224B30F427281E49114E62F6FB24D5____Length_PropertyInfo,
	FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8_CustomAttributesCacheGenerator_FixedList4096Bytes_1_tEB48C1B33D0FC9AEB7A3168CDA598E2B4028AAC8____Length_PropertyInfo,
	Unity_Collections_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void BurstCompatibleAttribute_set_GenericTypeArguments_mEEF019AFEFB1DCC2C6DC2E33C7BE33DECCD8DD5A_inline (BurstCompatibleAttribute_t21F956E439B04822E4E1F6AFAF609CA149E9BD82 * __this, TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___value0, const RuntimeMethod* method)
{
	{
		// public Type[] GenericTypeArguments { get; set; }
		TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* L_0 = ___value0;
		__this->set_U3CGenericTypeArgumentsU3Ek__BackingField_0(L_0);
		return;
	}
}
