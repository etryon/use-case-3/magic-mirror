﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// Unity.Collections.DeallocateOnJobCompletionAttribute
struct DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.DelayedAttribute
struct DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Runtime.CompilerServices.FixedBufferAttribute
struct FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// Obi.Indent
struct Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Unity.Collections.LowLevel.Unsafe.NativeDisableContainerSafetyRestrictionAttribute
struct NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9;
// Unity.Collections.NativeDisableParallelForRestrictionAttribute
struct NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// Unity.Collections.ReadOnlyAttribute
struct ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// Obi.SerializeProperty
struct SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Runtime.CompilerServices.UnsafeValueTypeAttribute
struct UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC;
// Obi.VisibleIf
struct VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Unity.Collections.WriteOnlyAttribute
struct WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA;

IL2CPP_EXTERN_C const RuntimeType* Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiParticlePicker_tB0405EAD1BFCF9EBDD82F494C3DE49EBECD0F732_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Nullable`1<System.Boolean>
struct Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// Unity.Collections.DeallocateOnJobCompletionAttribute
struct DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.FixedBufferAttribute
struct FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.FixedBufferAttribute::elementType
	Type_t * ___elementType_0;
	// System.Int32 System.Runtime.CompilerServices.FixedBufferAttribute::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_elementType_0() { return static_cast<int32_t>(offsetof(FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C, ___elementType_0)); }
	inline Type_t * get_elementType_0() const { return ___elementType_0; }
	inline Type_t ** get_address_of_elementType_0() { return &___elementType_0; }
	inline void set_elementType_0(Type_t * value)
	{
		___elementType_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___elementType_0), (void*)value);
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.LowLevel.Unsafe.NativeDisableContainerSafetyRestrictionAttribute
struct NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.NativeDisableParallelForRestrictionAttribute
struct NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Unity.Collections.ReadOnlyAttribute
struct ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.UnsafeValueTypeAttribute
struct UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Unity.Collections.WriteOnlyAttribute
struct WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.DelayedAttribute
struct DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// Unity.Burst.FloatMode
struct FloatMode_t38741ACC50724A284056372B5D90095D40ACB1E4 
{
public:
	// System.Int32 Unity.Burst.FloatMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatMode_t38741ACC50724A284056372B5D90095D40ACB1E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Burst.FloatPrecision
struct FloatPrecision_tF6B76A9F4B20E5525B4B38902AA661AAB9E199F5 
{
public:
	// System.Int32 Unity.Burst.FloatPrecision::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatPrecision_tF6B76A9F4B20E5525B4B38902AA661AAB9E199F5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// Obi.MultiPropertyAttribute
struct MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Obi.SerializeProperty
struct SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String Obi.SerializeProperty::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0, ___U3CPropertyNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_0() const { return ___U3CPropertyNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_0() { return &___U3CPropertyNameU3Ek__BackingField_0; }
	inline void set_U3CPropertyNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPropertyNameU3Ek__BackingField_0), (void*)value);
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// Unity.Burst.BurstCompileAttribute
struct BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// Unity.Burst.FloatMode Unity.Burst.BurstCompileAttribute::<FloatMode>k__BackingField
	int32_t ___U3CFloatModeU3Ek__BackingField_0;
	// Unity.Burst.FloatPrecision Unity.Burst.BurstCompileAttribute::<FloatPrecision>k__BackingField
	int32_t ___U3CFloatPrecisionU3Ek__BackingField_1;
	// System.Nullable`1<System.Boolean> Unity.Burst.BurstCompileAttribute::_compileSynchronously
	Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  ____compileSynchronously_2;
	// System.String[] Unity.Burst.BurstCompileAttribute::<Options>k__BackingField
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___U3COptionsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CFloatModeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3CFloatModeU3Ek__BackingField_0)); }
	inline int32_t get_U3CFloatModeU3Ek__BackingField_0() const { return ___U3CFloatModeU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CFloatModeU3Ek__BackingField_0() { return &___U3CFloatModeU3Ek__BackingField_0; }
	inline void set_U3CFloatModeU3Ek__BackingField_0(int32_t value)
	{
		___U3CFloatModeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFloatPrecisionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3CFloatPrecisionU3Ek__BackingField_1)); }
	inline int32_t get_U3CFloatPrecisionU3Ek__BackingField_1() const { return ___U3CFloatPrecisionU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CFloatPrecisionU3Ek__BackingField_1() { return &___U3CFloatPrecisionU3Ek__BackingField_1; }
	inline void set_U3CFloatPrecisionU3Ek__BackingField_1(int32_t value)
	{
		___U3CFloatPrecisionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of__compileSynchronously_2() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ____compileSynchronously_2)); }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  get__compileSynchronously_2() const { return ____compileSynchronously_2; }
	inline Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3 * get_address_of__compileSynchronously_2() { return &____compileSynchronously_2; }
	inline void set__compileSynchronously_2(Nullable_1_t1D1CD146BFCBDC2E53E1F700889F8C5C21063EF3  value)
	{
		____compileSynchronously_2 = value;
	}

	inline static int32_t get_offset_of_U3COptionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1, ___U3COptionsU3Ek__BackingField_3)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_U3COptionsU3Ek__BackingField_3() const { return ___U3COptionsU3Ek__BackingField_3; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_U3COptionsU3Ek__BackingField_3() { return &___U3COptionsU3Ek__BackingField_3; }
	inline void set_U3COptionsU3Ek__BackingField_3(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___U3COptionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COptionsU3Ek__BackingField_3), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// Obi.Indent
struct Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584  : public MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Obi.VisibleIf
struct VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3  : public MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E
{
public:
	// System.String Obi.VisibleIf::<MethodName>k__BackingField
	String_t* ___U3CMethodNameU3Ek__BackingField_0;
	// System.Boolean Obi.VisibleIf::<Negate>k__BackingField
	bool ___U3CNegateU3Ek__BackingField_1;
	// System.Reflection.MethodInfo Obi.VisibleIf::eventMethodInfo
	MethodInfo_t * ___eventMethodInfo_2;
	// System.Reflection.FieldInfo Obi.VisibleIf::fieldInfo
	FieldInfo_t * ___fieldInfo_3;

public:
	inline static int32_t get_offset_of_U3CMethodNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___U3CMethodNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CMethodNameU3Ek__BackingField_0() const { return ___U3CMethodNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CMethodNameU3Ek__BackingField_0() { return &___U3CMethodNameU3Ek__BackingField_0; }
	inline void set_U3CMethodNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CMethodNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CMethodNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNegateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___U3CNegateU3Ek__BackingField_1)); }
	inline bool get_U3CNegateU3Ek__BackingField_1() const { return ___U3CNegateU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CNegateU3Ek__BackingField_1() { return &___U3CNegateU3Ek__BackingField_1; }
	inline void set_U3CNegateU3Ek__BackingField_1(bool value)
	{
		___U3CNegateU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_eventMethodInfo_2() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___eventMethodInfo_2)); }
	inline MethodInfo_t * get_eventMethodInfo_2() const { return ___eventMethodInfo_2; }
	inline MethodInfo_t ** get_address_of_eventMethodInfo_2() { return &___eventMethodInfo_2; }
	inline void set_eventMethodInfo_2(MethodInfo_t * value)
	{
		___eventMethodInfo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___eventMethodInfo_2), (void*)value);
	}

	inline static int32_t get_offset_of_fieldInfo_3() { return static_cast<int32_t>(offsetof(VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3, ___fieldInfo_3)); }
	inline FieldInfo_t * get_fieldInfo_3() const { return ___fieldInfo_3; }
	inline FieldInfo_t ** get_address_of_fieldInfo_3() { return &___fieldInfo_3; }
	inline void set_fieldInfo_3(FieldInfo_t * value)
	{
		___fieldInfo_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fieldInfo_3), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C (EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_mC1E929119B039C168A2D1871E3AAAC3EF4205C12 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * __this, const RuntimeMethod* method);
// System.Void Unity.Burst.BurstCompileAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628 (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.WriteOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.NativeDisableParallelForRestrictionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.ReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.DeallocateOnJobCompletionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7 (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * __this, const RuntimeMethod* method);
// System.Void Unity.Collections.LowLevel.Unsafe.NativeDisableContainerSafetyRestrictionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993 (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.FixedBufferAttribute::.ctor(System.Type,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FixedBufferAttribute__ctor_m7767B7379CFADD0D12551A5A891BF1916A07BE51 (FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C * __this, Type_t * ___elementType0, int32_t ___length1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.UnsafeValueTypeAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnsafeValueTypeAttribute__ctor_mA5A3D4443A6B4BE3B31E8A8919809719991A7EC4 (UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void Obi.SerializeProperty::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3 (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * __this, String_t* ___propertyName0, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, const RuntimeMethod* method);
// System.Void Obi.Indent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7 (Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584 * __this, const RuntimeMethod* method);
// System.Void Obi.VisibleIf::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6 (VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3 * __this, String_t* ___methodName0, bool ___negate1, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void UnityEngine.DelayedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DelayedAttribute__ctor_mA73AFD50BA612774A729641927483BC80142A11A (DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
static void Obi_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 * tmp = (EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 *)cache->attributes[1];
		EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C(tmp, NULL);
	}
}
static void IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 * tmp = (EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 *)cache->attributes[0];
		EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IsUnmanagedAttribute_t939FAB91AE2EC449A87EAD0A37591BA55859D86B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 * tmp = (EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82 *)cache->attributes[0];
		EmbeddedAttribute__ctor_mF092B41AF606A0079DB72F589B9F3A4B09A2879C(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiContactGrabber_t14D0338C4C370DF79A94FA1DE76A34470B4DF04A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x20\x32\x44\x20\x6D\x6F\x64\x65\x2C\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x20\x61\x72\x65\x20\x73\x69\x6D\x75\x6C\x61\x74\x65\x64\x20\x6F\x6E\x20\x74\x68\x65\x20\x58\x59\x20\x70\x6C\x61\x6E\x65\x20\x6F\x6E\x6C\x79\x2E\x20\x46\x6F\x72\x20\x75\x73\x65\x20\x69\x6E\x20\x63\x6F\x6E\x6A\x75\x6E\x63\x74\x69\x6F\x6E\x20\x77\x69\x74\x68\x20\x55\x6E\x69\x74\x79\x27\x73\x20\x32\x44\x20\x6D\x6F\x64\x65\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_interpolation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x6D\x65\x20\x61\x73\x20\x52\x69\x67\x69\x64\x62\x6F\x64\x79\x2E\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x69\x6F\x6E\x2E\x20\x53\x65\x74\x20\x74\x6F\x20\x49\x4E\x54\x45\x52\x50\x4F\x4C\x41\x54\x45\x20\x66\x6F\x72\x20\x63\x6C\x6F\x74\x68\x20\x74\x68\x61\x74\x20\x69\x73\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x6F\x6E\x20\x61\x20\x6D\x61\x69\x6E\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x6F\x72\x20\x63\x6C\x6F\x73\x65\x6C\x79\x20\x66\x6F\x6C\x6C\x6F\x77\x65\x64\x20\x62\x79\x20\x61\x20\x63\x61\x6D\x65\x72\x61\x2E\x20\x4E\x4F\x4E\x45\x20\x66\x6F\x72\x20\x65\x76\x65\x72\x79\x74\x68\x69\x6E\x67\x20\x65\x6C\x73\x65\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_gravity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x6D\x75\x6C\x61\x74\x69\x6F\x6E\x20\x67\x72\x61\x76\x69\x74\x79\x20\x65\x78\x70\x72\x65\x73\x73\x65\x64\x20\x69\x6E\x20\x6C\x6F\x63\x61\x6C\x20\x73\x70\x61\x63\x65\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_damping(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x20\x6F\x66\x20\x76\x65\x6C\x6F\x63\x69\x74\x79\x20\x6C\x6F\x73\x74\x20\x70\x65\x72\x20\x73\x65\x63\x6F\x6E\x64\x2C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x25\x20\x28\x30\x29\x20\x61\x6E\x64\x20\x31\x30\x30\x25\x20\x28\x31\x29\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxAnisotropy(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 5.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x20\x72\x61\x74\x69\x6F\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x61\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x27\x73\x20\x6C\x6F\x6E\x67\x65\x73\x74\x20\x61\x6E\x64\x20\x73\x68\x6F\x72\x74\x65\x73\x74\x20\x61\x78\x69\x73\x2E\x20\x55\x73\x65\x20\x31\x20\x66\x6F\x72\x20\x69\x73\x6F\x74\x72\x6F\x70\x69\x63\x20\x28\x63\x6F\x6D\x70\x6C\x65\x74\x65\x6C\x79\x20\x72\x6F\x75\x6E\x64\x29\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_sleepThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x73\x73\x2D\x6E\x6F\x72\x6D\x61\x6C\x69\x7A\x65\x64\x20\x6B\x69\x6E\x65\x74\x69\x63\x20\x65\x6E\x65\x72\x67\x79\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x62\x65\x6C\x6F\x77\x20\x77\x68\x69\x63\x68\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x73\x20\x61\x72\x65\x6E\x27\x74\x20\x75\x70\x64\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_collisionMargin(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x28\x73\x69\x6D\x70\x6C\x69\x63\x65\x73\x2F\x63\x6F\x6C\x6C\x69\x64\x65\x72\x73\x29\x20\x66\x6F\x72\x20\x61\x20\x63\x6F\x6E\x74\x61\x63\x74\x20\x74\x6F\x20\x62\x65\x20\x67\x65\x6E\x65\x72\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxDepenetration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x64\x65\x70\x65\x6E\x65\x74\x72\x61\x74\x69\x6F\x6E\x20\x76\x65\x6C\x6F\x63\x69\x74\x79\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x20\x74\x68\x61\x74\x20\x73\x74\x61\x72\x74\x20\x61\x20\x66\x72\x61\x6D\x65\x20\x69\x6E\x73\x69\x64\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x2E\x20\x4C\x6F\x77\x20\x76\x61\x6C\x75\x65\x73\x20\x65\x6E\x73\x75\x72\x65\x20\x6E\x6F\x20\x27\x65\x78\x70\x6C\x6F\x73\x69\x76\x65\x27\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x72\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E\x2E\x20\x53\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x3E\x20\x30\x20\x75\x6E\x6C\x65\x73\x73\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x66\x6F\x72\x20\x6E\x6F\x6E\x2D\x70\x68\x79\x73\x69\x63\x61\x6C\x20\x65\x66\x66\x65\x63\x74\x73\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_continuousCollisionDetection(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x20\x6F\x66\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x76\x65\x6C\x6F\x63\x69\x74\x69\x65\x73\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x63\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x64\x65\x74\x65\x63\x74\x69\x6F\x6E\x2E\x20\x53\x65\x74\x20\x74\x6F\x20\x30\x20\x66\x6F\x72\x20\x70\x75\x72\x65\x6C\x79\x20\x73\x74\x61\x74\x69\x63\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2C\x20\x73\x65\x74\x20\x74\x6F\x20\x31\x20\x66\x6F\x72\x20\x70\x75\x72\x65\x20\x63\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_shockPropagation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x61\x67\x65\x20\x6F\x66\x20\x73\x68\x6F\x63\x6B\x20\x70\x72\x6F\x70\x61\x67\x61\x74\x69\x6F\x6E\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x2D\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x73\x74\x61\x63\x6B\x69\x6E\x67\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionIterations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x73\x70\x65\x6E\x74\x20\x6F\x6E\x20\x63\x6F\x6E\x76\x65\x78\x20\x6F\x70\x74\x69\x6D\x69\x7A\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x73\x75\x72\x66\x61\x63\x65\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 32.0f, NULL);
	}
}
static void SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionTolerance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x72\x72\x6F\x72\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x6F\x20\x73\x74\x6F\x70\x20\x63\x6F\x6E\x76\x65\x78\x20\x6F\x70\x74\x69\x6D\x69\x7A\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x73\x75\x72\x66\x61\x63\x65\x20\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_evaluationOrder(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x64\x65\x72\x20\x69\x6E\x20\x77\x68\x69\x63\x68\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x73\x20\x61\x72\x65\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x64\x2E\x20\x53\x45\x51\x55\x45\x4E\x54\x49\x41\x4C\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x73\x20\x66\x61\x73\x74\x65\x72\x20\x62\x75\x74\x20\x69\x73\x20\x6E\x6F\x74\x20\x76\x65\x72\x79\x20\x73\x74\x61\x62\x6C\x65\x2E\x20\x50\x41\x52\x41\x4C\x4C\x45\x4C\x20\x69\x73\x20\x76\x65\x72\x79\x20\x73\x74\x61\x62\x6C\x65\x20\x62\x75\x74\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x73\x20\x73\x6C\x6F\x77\x6C\x79\x2C\x20\x72\x65\x71\x75\x69\x72\x69\x6E\x67\x20\x6D\x6F\x72\x65\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x74\x6F\x20\x61\x63\x68\x69\x65\x76\x65\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x72\x65\x73\x75\x6C\x74\x2E"), NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_iterations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x20\x73\x6F\x6C\x76\x65\x72\x2E\x20\x41\x20\x6C\x6F\x77\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x69\x74\x65\x72\x61\x74\x69\x6F\x6E\x73\x20\x77\x69\x6C\x6C\x20\x70\x65\x72\x66\x6F\x72\x6D\x20\x62\x65\x74\x74\x65\x72\x2C\x20\x62\x75\x74\x20\x62\x65\x20\x6C\x65\x73\x73\x20\x61\x63\x63\x75\x72\x61\x74\x65\x2E"), NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_SORFactor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x76\x65\x72\x20\x28\x6F\x72\x20\x75\x6E\x64\x65\x72\x20\x69\x66\x20\x3C\x20\x31\x29\x20\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x66\x61\x63\x74\x6F\x72\x20\x75\x73\x65\x64\x2E\x20\x41\x74\x20\x31\x2C\x20\x6E\x6F\x20\x6F\x76\x65\x72\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x69\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x2E\x20\x41\x74\x20\x32\x2C\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x73\x20\x64\x6F\x75\x62\x6C\x65\x20\x74\x68\x65\x69\x72\x20\x72\x65\x6C\x61\x78\x61\x74\x69\x6F\x6E\x20\x72\x61\x74\x65\x2E\x20\x48\x69\x67\x68\x20\x76\x61\x6C\x75\x65\x73\x20\x72\x65\x64\x75\x63\x65\x20\x73\x74\x61\x62\x69\x6C\x69\x74\x79\x20\x62\x75\x74\x20\x69\x6D\x70\x72\x6F\x76\x65\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 2.0f, NULL);
	}
}
static void ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_enabled(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x74\x20\x67\x72\x6F\x75\x70\x20\x69\x73\x20\x73\x6F\x6C\x76\x65\x64\x20\x6F\x72\x20\x6E\x6F\x74\x2E"), NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x43\x6C\x6F\x74\x68"), 900LL, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator_m_ClothBlueprint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__volumeConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__compressionCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__pressure(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__tetherConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__tetherCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__tetherScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 2.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator_m_renderer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator_m_SelfCollisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator_m_OneSided(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__distanceConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__stretchingScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__stretchCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__maxCompression(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__bendConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__bendCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__maxBending(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.100000001f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__plasticYield(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 0.100000001f, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__plasticCreep(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__aerodynamicsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__drag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__lift(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator_trianglesOffset(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x53\x6B\x69\x6E\x6E\x65\x64\x43\x6C\x6F\x74\x68"), 902LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(SkinnedMeshRenderer_t126F4D6010E0F4B2685A7817B0A9171805D8F496_0_0_0_var), NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_m_SkinnedClothBlueprint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator__tetherConstraintsEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator__tetherCompliance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator__tetherScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 2.0f, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedVertices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedNormals(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedTangents(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedMesh(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x54\x65\x61\x72\x61\x62\x6C\x65\x20\x43\x6C\x6F\x74\x68"), 901LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
}
static void ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_tearDebilitation(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_OnClothTorn(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_ObiTearableCloth_add_OnClothTorn_m6224CDA7378B8DB12B0333F7FED7B7227E6E3694(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_ObiTearableCloth_remove_OnClothTorn_mB5CE17825525987AFCBD938933DC1248DD4D39E9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t5EBFA052AD90FD6A9779D3B8868C4C2B2B11E855_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6C\x6F\x74\x68\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x43\x6C\x6F\x74\x68\x20\x42\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 120LL, NULL);
	}
}
static void ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_Initialize_mB541B50B4B6C0D58EDE43368C99D89F1690B4AF7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_0_0_0_var), NULL);
	}
}
static void ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateDistanceConstraints_m1570C2E0E2E30FF955FAED78EB8DF6C12ED36B65(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_0_0_0_var), NULL);
	}
}
static void ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateAerodynamicConstraints_mB62C6FAE788BF62F4C9B9D2D437136E90995A256(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_0_0_0_var), NULL);
	}
}
static void ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateBendingConstraints_m94A0CAEBCCD4E334449EA330C0D77D22333B0B31(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_0_0_0_var), NULL);
	}
}
static void ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateVolumeConstraints_m61301FCFCDA8C8B2F6B27808ED4DBEB2F4EE5540(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2__ctor_mD89C5AE9C9C80916FB8FFD79CFC095D19AE5D57D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_IDisposable_Dispose_m7004C46EC7D295E0968AF94ABE4F7E8BF273593F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E02322F5A0D23311828213F94F9CAE0E0750934(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mFB62B5B536F9DCCFFA18FAD799255FBB349E0CB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m74CF369892ADB61833B9A3B086E1C2EC78434A01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3__ctor_m09BD3C57322A5EF62A54F6731CDF444FA9BB2E09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_mDAD5189CF5BE3E2386462303C5D8363F9695B6E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D1E0E87E7FD71C4EC798103826D6140BFD871F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mC8A761BFD80CE17A1F65517143C99E6A20387CDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8896FC2746D379EEBF8E8501E3C010284F085BC2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4__ctor_m4FC51372D57AB411957AE946A83F3EFDC329A358(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_IDisposable_Dispose_m75085E8EEE584CA48781239A3A035272ED0D28E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADC37B2982CCFCDA50AB1C551B944F7022C3FF0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_mB2CC4104E9C7AB68C42D91F0D25AAD0316D92347(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m993A90ABEEA80806AA028F49D6215A00BD8CAB0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5__ctor_m216136A55AF138945D5561DC181CE943371035F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_IDisposable_Dispose_m6821B7A23670BE0F2DE199C41EE831168CA242AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE75E1D32AFBD9B07503268FC3E0BA64851456C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m731DAB5B414FCB6F5253FAE15495E32006F1400E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_m5767B962F68DA952AABEA5D0B178933A58E445E5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6__ctor_m88D1946A7A914EC0CC61A5D2BAB66310AB4C807C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_IDisposable_Dispose_m051782DF73D0AA384FAEB7DC1642C3598EC916AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m243C532028DFB65774C84E5FDFA0528EC46DE0B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_mED3CB28B77CD40AE79F199DE53151239982A3BBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_m49682C0DC4ADBD851E1ED3B1631283CBC211B9E4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t04A48055519B751B310989CFBF1ACB6A168C1BF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t2F8F7BD3F1F5DCE39BAAD0D8431328B2888E5FA9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_topology(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_deformableTriangles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_restNormals(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_areaContribution(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_ObiClothBlueprintBase_GenerateDeformableTriangles_m412F7EDE785459A5F256F70B1BCC07A6DE86E97A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_0_0_0_var), NULL);
	}
}
static void ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_ObiClothBlueprintBase_CreateSimplices_m792C9C38FCE60CB1F28D70F757E38C50C8B381DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_0_0_0_var), NULL);
	}
}
static void U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8__ctor_m0B4BEE607BAA1A2C0EBFEC240D52F91801C0B17C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_IDisposable_Dispose_m9FACA3940826A6F604CE1ED095A420F3B70BEF31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94D6F01A98AAF5D8ACB13EB4E5C66B583F950A17(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_Reset_mFF432431540D581589CE78CB502104BA515F44A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_get_Current_mCAC139C7F6132507DA4DD623B3F44AB50ABED2F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9__ctor_m6BDB71B25348530DD662CD094363B8BD7290B3F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_IDisposable_Dispose_m9D9E3AF192D6A21176BC771CD6B070A775A15627(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF2A1B6D8EB60FC887ACEA5CA1A98501EFC8DBA8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_Reset_m168698F93B24A0D1D8BF0D33260667A9AE613315(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_get_Current_mA5534B33C95835ED9EE2DD141CC15F742A981072(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiSkinnedClothBlueprint_t2303B8E824B4492591ADD3E67FD1E78E5C56F197_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x73\x6B\x69\x6E\x6E\x65\x64\x20\x63\x6C\x6F\x74\x68\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x53\x6B\x69\x6E\x6E\x65\x64\x20\x43\x6C\x6F\x74\x68\x20\x42\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 122LL, NULL);
	}
}
static void ObiSkinnedClothBlueprint_t2303B8E824B4492591ADD3E67FD1E78E5C56F197_CustomAttributesCacheGenerator_ObiSkinnedClothBlueprint_Initialize_mEEEBD3D9C13B8A98A3761D7E712512A3CCC377B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_0_0_0_var), NULL);
	}
}
static void ObiSkinnedClothBlueprint_t2303B8E824B4492591ADD3E67FD1E78E5C56F197_CustomAttributesCacheGenerator_ObiSkinnedClothBlueprint_CreateSkinConstraints_m58C14E2205A69B9015B3F01ADF03CB072338C3A7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2__ctor_mBE818931D3B01A168B9747C7573E4B406C0EB303(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_IDisposable_Dispose_m0BDE2AC7C8E038318E8B83BAC143BD3363720DEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB92A210007613B2909D214DF7B445C938F4C6EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mC9AF6B8BE46FC43D8A1334E15A189A796056CBE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m22FAD500DB712F880583DA421A4A69DE13357712(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3__ctor_mD52C4DA6057FEE9E99D0D32779C65C6EF756FA28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_IDisposable_Dispose_m2188832D0686304E2FA1C1E3CBC9244B11DB4EA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE5966D180DA31A5B8CC609EA3CC94FD482431528(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m3423E9E200FEB13FEF7BEC122845FB66403E2A30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8C97E9A1F24B2A98644A9E03FD1DE0A49AB21A05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x61\x72\x61\x62\x6C\x65\x20\x63\x6C\x6F\x74\x68\x20\x62\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x54\x65\x61\x72\x61\x62\x6C\x65\x20\x43\x6C\x6F\x74\x68\x20\x42\x6C\x75\x65\x70\x72\x69\x6E\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 121LL, NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_tearCapacity(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x6D\x65\x6D\x6F\x72\x79\x20\x70\x72\x65\x61\x6C\x6C\x6F\x63\x61\x74\x65\x64\x20\x74\x6F\x20\x63\x72\x65\x61\x74\x65\x20\x65\x78\x74\x72\x61\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x73\x20\x61\x6E\x64\x20\x6D\x65\x73\x68\x20\x64\x61\x74\x61\x20\x77\x68\x65\x6E\x20\x74\x65\x61\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x63\x6C\x6F\x74\x68\x2E\x20\x30\x20\x6D\x65\x61\x6E\x73\x20\x6E\x6F\x20\x65\x78\x74\x72\x61\x20\x6D\x65\x6D\x6F\x72\x79\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x6C\x6C\x6F\x63\x61\x74\x65\x64\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x63\x6C\x6F\x74\x68\x20\x77\x69\x6C\x6C\x20\x6E\x6F\x74\x20\x62\x65\x20\x74\x65\x61\x72\x61\x62\x6C\x65\x2E\x20\x31\x20\x6D\x65\x61\x6E\x73\x20\x61\x6C\x6C\x20\x63\x6C\x6F\x74\x68\x20\x74\x72\x69\x61\x6E\x67\x6C\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x66\x75\x6C\x6C\x79\x20\x74\x65\x61\x72\x61\x62\x6C\x65\x2E"), NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_pooledParticles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_tearResistance(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_distanceConstraintMap(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_Initialize_mEE76304F3987B091FC3F24395B52A9776C85B7B6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_0_0_0_var), NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_CreateDistanceConstraints_mFD063AE5F8C647760AC90B516949FF31D0238EB5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_0_0_0_var), NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_CreateInitialDistanceConstraints_mA1AD7123377815A2D7DCADC9CEFE1C7E450B718D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_0_0_0_var), NULL);
	}
}
static void ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_CreatePooledDistanceConstraints_mC50382B6EF8301BB863CB65BBF73FC20FD8B83BC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_0_0_0_var), NULL);
	}
}
static void U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8__ctor_mE1FD39996C2F57CF4C0B2C10C7CB001423DE2381(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_IDisposable_Dispose_m6A03B2E04499DB25B9E769956B700DB301ECA55A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D1131F06EECDDE2B78AD00FB36522B66AD3538C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m6ADA7C8F48F23CC6603E2ECDDF2999F8B85CF87F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9__ctor_m225975D257A624A724A08C3E1FAF8C2DF4654668(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_IDisposable_Dispose_mF9F663E01209D2ADCFC88D6EB3697151496279D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC86A1722C558C6E0F887CE3CE9B2CDFA9D75BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_Reset_mDA4DBAC64C3B419B5FD761D86BEB3A05984873E7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_get_Current_mA37DF615EFA87C9F148D307AD7FC73526A9207CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10__ctor_mB3B7E5310F4928BB37C9512E9F40ACFC3F140C9F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_IDisposable_Dispose_m39D16A16F80402BC68F47E0A9F83F7BC9CDFE417(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF3452EB8DE1B9490F900218D00937A623C33312(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_Reset_mC4356FC9C856276AFA630708CF08D9DB17A3E189(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D3BB3B7750F054BA802A619B79F0A538D1D6FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11__ctor_m3C790EFEB6A41148BFA287BBF9EE4C00327BEB64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_IDisposable_Dispose_m5B3AC37A9F1F1BAED5319C961D4121B7B17D08A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7BCE5ABEDC2654A1A0453DE030EF2ED8CDC3FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_Reset_m824F230E26816CB43250CD6BA6C491C34058CD38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_get_Current_m684F6B83E2834AA135F20041DDCF51BFE4ABB3A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_containsData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_vertices(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_halfEdges(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_borderEdges(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_faces(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_restNormals(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_rawToWelded(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_HalfEdgeMesh_GetNeighbourVerticesEnumerator_m5D851186C1DD0DDF3C79F394DC9946C6664AE4E2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_0_0_0_var), NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_HalfEdgeMesh_GetNeighbourEdgesEnumerator_m46006FB9A047B16085C6EF70CC20209053622818(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_0_0_0_var), NULL);
	}
}
static void HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_HalfEdgeMesh_GetNeighbourFacesEnumerator_m055B1E4B59B1C20B1C6F542EE88DA13DBE7DF26D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_0_0_0_var), NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31__ctor_mF8ED85D8453E3677145D2F9202A2F2CBFC6929E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_IDisposable_Dispose_mF5E674694F95EB02F8B41CC0A6A0AF7D3C1276DE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_VertexU3E_get_Current_mB8974C8C5F9B458C49BBBD99546E03A15E277212(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_Reset_mF9C32E78CBDC8991CB3AA3E8517D2434CB4060C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_get_Current_mE029EC97DE5EBDB2302D6C9C4E042A8D402111CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_VertexU3E_GetEnumerator_mAF607573C51602451E445A9210B356A40C744B6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerable_GetEnumerator_m3F71BE162D53C1A40174A26D4BEB7864B8D1213B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32__ctor_mD46F103C7C9B4F9B8A0AD31F7AD7D11C248D9E45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_IDisposable_Dispose_m2DCA4922446B3F5F983E88E30DE79278DEDE5613(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_HalfEdgeU3E_get_Current_mCD5275CC511E91FCA17A93FC067DD0201E6C87E7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_Reset_mE09272AC7F4A5074F95EC162B00DA8152F551EFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_get_Current_mD8A977C5E95E52F283E5BB7BCAB03DB08B4363BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_HalfEdgeU3E_GetEnumerator_mC4B54D7954EA17442D6D38B91B6E903FB1C54C3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerable_GetEnumerator_m2F65C8046D5AC6277F7723BF04D7AEF6A27B507E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33__ctor_m4C8DC2E13D54FFEEB86EF6AE20DEEA7A1E918A6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_IDisposable_Dispose_m972ADD603D342F7AD8F1317F6B20179617A042DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_FaceU3E_get_Current_m11DEA6ABA872F8CBB0E39CAA35823EE6B5E5C15A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_Reset_m9E664CABCDA715691724536236FAEBBB657DD313(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_get_Current_mBBBADC2F55A8568A51101245F7598E952B837304(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_FaceU3E_GetEnumerator_mF45B315F345EC2F18D84B7400ECD0FC83B7BE26F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m66EB03D0F227B3A19E153D78CC869BA345527948(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x74\x72\x69\x61\x6E\x67\x6C\x65\x20\x73\x6B\x69\x6E\x6D\x61\x70"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x54\x72\x69\x61\x6E\x67\x6C\x65\x20\x53\x6B\x69\x6E\x6D\x61\x70"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 123LL, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_bound(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_barycentricWeight(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_normalAlignmentWeight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_elevationWeight(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_MasterChannels(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_SlaveChannels(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_SlaveTransform(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_Master(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_Slave(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_skinnedVertices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_ObiTriangleSkinMap_Bind_m2DE9C31E8338896A2BFD8CCD2C47D0561FA663F9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_0_0_0_var), NULL);
	}
}
static void U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31__ctor_mE5C6B10FCF9C0277DFC6D366EB0FD7BE84FB77BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_IDisposable_Dispose_m0470FFE716CE5F6D58F2070F1980E5DEF012C319(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m799C177A7037BA730C8BC7776C400B5065B64A16(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_m937FC6C298B8CA31EA931CA6B7205A747ABFC996(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiClothProxy_t723D5DFCA0F551D6CF9F28B05123D18CC8972126_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x43\x6C\x6F\x74\x68\x20\x50\x72\x6F\x78\x79"), 906LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ObiClothProxy_t723D5DFCA0F551D6CF9F28B05123D18CC8972126_CustomAttributesCacheGenerator_m_Master(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiClothRenderer_t3FA62EDF5E0CBB7F6B8450CF30E5BB98CAE9BB75_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x43\x6C\x6F\x74\x68\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 903LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_OnRendererUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_clothMesh(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_ObiClothRendererBase_add_OnRendererUpdated_mD0F78B5164967A7079E170FB1758688B3B7B9A53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_ObiClothRendererBase_remove_OnRendererUpdated_m31F9D492473F2A469ECDDE8815A8A4ED54AB2FC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiClothRendererMeshFilter_t57EF050279606827C072D4A1B145A6932A2D2CEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_mC1E929119B039C168A2D1871E3AAAC3EF4205C12(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiSkinnedClothRenderer_tC0F584117B4933DA72BC0CFD46C22085D3AA9064_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x53\x6B\x69\x6E\x6E\x65\x64\x20\x43\x6C\x6F\x74\x68\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 905LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiTearableClothRenderer_t05133F45C5382054CD84D54AA4F3A3C208984A4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x54\x65\x61\x72\x61\x62\x6C\x65\x20\x43\x6C\x6F\x74\x68\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 904LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintLoaded(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintUnloaded(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareFrame(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBeginStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnSubstep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnEndStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnInterpolate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_ActiveParticleCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverBatchOffsets(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_CollisionMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_SurfaceCollisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_U3CsolverU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_project_m1A54A528F3FC9B0309892E868CF525734419BB49(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_toMatrix_mAA0372FB0B34939F42DA7D8C213E694BC0C7DF4D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_asDiagonal_mED83A376E4FB3DD5D01304739CF1209A86E846AE(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_diagonal_m2495565B886EDB95594F3A6D3D3D04413E407984(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_frobeniusNorm_mD8B2D136717389B775B17E51B995F2EA8818F106(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_unitOrthogonal_m0CF287F50224232FE30E0E6C09BAB1F314F35F53(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_NearestPointOnTri_m15F131F6314B94E56B30D27DC3FFFBE574379AF6____tri0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____A0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____B1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____C2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____P3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A____A0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A____B1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A____P2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____p10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____p21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____p32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____coords3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740____p10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740____p21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740____coords2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_RemoveRangeBurst_mC15961CC1CC9308F567E98EEA38B52818C534CF7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void BurstBox_tA70C7C054A23857D4B17C3FEED3CA25136446326_CustomAttributesCacheGenerator_BurstBox_Contacts_m9D3DED1FA7E7454E9EB7A52A7ED94B8A08C99CE3____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstBox_tA70C7C054A23857D4B17C3FEED3CA25136446326_CustomAttributesCacheGenerator_BurstBox_Obi_IBurstCollider_Contacts_m97856F2BFA4EBA5278F111B41C7F5CA5A8C58573____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstCapsule_t56BDF841EA369F525D125BD53FC21AD5CD05D4B9_CustomAttributesCacheGenerator_BurstCapsule_Contacts_mE461E9B2E609E1A411ED668A5C6380B29FCCE4D6____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstCapsule_t56BDF841EA369F525D125BD53FC21AD5CD05D4B9_CustomAttributesCacheGenerator_BurstCapsule_Obi_IBurstCollider_Contacts_mADF596860548ABE29238ADEA0F82324B6D1A6A28____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_movingColliders(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_rigidbodies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_colliderCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_dt(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateMovingColliders_tC532706751FD034881CE6740A29342E6BF6287B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateMovingColliders_tC532706751FD034881CE6740A29342E6BF6287B9_CustomAttributesCacheGenerator_colliderCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_colliderGrid(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_gridLevels(CustomAttributesCache* cache)
{
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[0];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[1];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_filters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_simplexBounds(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_rigidbodies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_bounds(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_distanceFieldHeaders(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_distanceFieldNodes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_triangleMeshHeaders(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_bihNodes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_triangles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_vertices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edgeMeshHeaders(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edgeBihNodes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edges(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edgeVertices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_heightFieldHeaders(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_heightFieldSamples(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_contactsQueue(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[1];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_solverToWorld(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_worldToSolver(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_parameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E____shape0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E____colliderToSolver1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E____simplexBoundsSS7(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_Contacts_mC98735809C55B8C503AC05F45BEC80420352057E____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_DFTraverse_m21F88015BB4FB3D50DD96EC893D5A10CD6827850____header2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_DFTraverse_m21F88015BB4FB3D50DD96EC893D5A10CD6827850____dfNodes3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_Obi_IBurstCollider_Contacts_m212E14857D066BE00574BE85A69F636A0B5A14BF____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstEdgeMesh_t8E4721F9266DDAE6EB02C72B3CF21134A98FB906_CustomAttributesCacheGenerator_BurstEdgeMesh_Contacts_mDEAD5DA052578CD2794227155F0CFDEFB76DD9BF____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstEdgeMesh_t8E4721F9266DDAE6EB02C72B3CF21134A98FB906_CustomAttributesCacheGenerator_BurstEdgeMesh_BIHTraverse_m8E806DD0BD9B370CC1862099121BC5B3F83B9CA9____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstEdgeMesh_t8E4721F9266DDAE6EB02C72B3CF21134A98FB906_CustomAttributesCacheGenerator_BurstEdgeMesh_Obi_IBurstCollider_Contacts_mE8D91763243332F3BBE8D636493C232F3085C1C1____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstHeightField_t2FC714F65F4A13D8E0EC730BA386EE5E5A11473F_CustomAttributesCacheGenerator_BurstHeightField_Contacts_m8D519E8FEE86E9D62A2F24AFE65643D75C82E321____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstHeightField_t2FC714F65F4A13D8E0EC730BA386EE5E5A11473F_CustomAttributesCacheGenerator_BurstHeightField_Obi_IBurstCollider_Contacts_mAF9CC53C9637FA7BEFAACAB317DEECC4325EFEA8____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstSphere_tCEDF9209B41A98705F09710ACC8B746A32DD70F6_CustomAttributesCacheGenerator_BurstSphere_Contacts_mDA37B8F9954ECD2EFD7BF8F9A51A43CA73A869AE____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstSphere_tCEDF9209B41A98705F09710ACC8B746A32DD70F6_CustomAttributesCacheGenerator_BurstSphere_Obi_IBurstCollider_Contacts_m16229652DCB867F59A4F4360F064FDA00691D32B____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstTriangleMesh_tD45990F38578999C59B6B3B6E613882E6C303041_CustomAttributesCacheGenerator_BurstTriangleMesh_Contacts_m03A033B4760F0729757BF20D322FC8D112802A00____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstTriangleMesh_tD45990F38578999C59B6B3B6E613882E6C303041_CustomAttributesCacheGenerator_BurstTriangleMesh_BIHTraverse_m53B6E0B2D8D09B465000034E751D2BB96E194907____simplexBounds11(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstTriangleMesh_tD45990F38578999C59B6B3B6E613882E6C303041_CustomAttributesCacheGenerator_BurstTriangleMesh_Obi_IBurstCollider_Contacts_m0414B63AB8F72DB613D9240E6B5AA847FB4B296D____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void IBurstCollider_t81B6D92E3DDAC7367074ABC3CA6413E83F2CDD9B_CustomAttributesCacheGenerator_IBurstCollider_Contacts_mA9084E4B6ABCD88E1208A6E335884F0877424B0B____simplexBounds8(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_aerodynamicCoeffs(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_normals(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_wind(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_plasticity(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientationIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_plasticity(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_invRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientationIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_firstIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_numIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_firstIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_numIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_contacts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_constraintParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_prevOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_invInertiaTensors(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_rigidbodies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_rigidbodyLinearDeltas(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_rigidbodyAngularDeltas(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_inertialFrame(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_prevOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_rigidbodies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_inertialFrame(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_constraintParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_solverParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_stepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_substepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_prevOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_invInertiaTensors(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_rigidbodies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_inertialFrame(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_stepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_substepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ClearFluidDataJob_tAD2A22F6DBC6DE882E7809B1468ABB46F011C22A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ClearFluidDataJob_tAD2A22F6DBC6DE882E7809B1468ABB46F011C22A_CustomAttributesCacheGenerator_fluidParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ClearFluidDataJob_tAD2A22F6DBC6DE882E7809B1468ABB46F011C22A_CustomAttributesCacheGenerator_fluidData(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_densityKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_gradientKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_fluidParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_restDensities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_surfaceTension(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_densityKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_gradientKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_normals(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_vorticity(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_fluidData(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_fluidParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_wind(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_vorticities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_atmosphericDrag(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_atmosphericPressure(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_vorticityConfinement(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_restDensities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_normals(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_fluidData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_eta(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[1];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_dt(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator_fluidParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator_principalAxes(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator_fluidParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator_renderablePositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator_smoothPositions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_fluidParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_principalRadii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_maxAnisotropy(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_smoothPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[1];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_anisotropies(CustomAttributesCache* cache)
{
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[0];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[1];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_renderablePositions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_principalAxes(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_restDensities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_diffusion(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_userData(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_fluidData(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_dt(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_restDensities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_surfaceTension(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_densityKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_fluidData(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_restDensities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_viscosities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_fluidData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_vorticities(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_normals(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_vorticities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_restDensities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_eta(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_renderablePositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_densityKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_smoothPositions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_renderablePositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_smoothPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_pairs(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_anisotropies(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_deltaTimeSqr(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_contacts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_constraintParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_prevOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_invInertiaTensors(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_contacts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_contacts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_constraintParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_solverParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_gravity(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_substepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_prevOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_invInertiaTensors(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_contacts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_batchData(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_substepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_colliderIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_offsets(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_restDarboux(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_invRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_rigidbodies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_inertialFrame(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_stepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_substepTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_activeConstraintCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_activeConstraintCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_firstIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_numIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_coms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_deformation(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_restPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_invInertiaTensors(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_firstIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_numIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_explicitGroup(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_shapeMaterialParameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_restPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_invRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_invInertiaTensors(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_firstIndex(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_numIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinPoints(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinNormals(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinRadiiBackstop(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinCompliance(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_deltaTimeSqr(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_deltaTimeSqr(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_activeConstraintCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_activeConstraintCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientationIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_invRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_deltaTimeSqr(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientationIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientationDeltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientationCounts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_maxLengthScale(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_deltaTimeSqr(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_triangles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_firstTriangle(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_numTriangles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_restVolumes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_pressureStiffness(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_gradients(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_deltaTimeSqr(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_triangles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_firstTriangle(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_numTriangles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_sorFactor(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_deltas(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_counts(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_EncapsulateBounds_m9AFA483BA1B7BEBF77C1B85D57549FB36375E1D7____bounds0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transform_m972887B0B52D4B08B4806F6BB8196E17B64C0B90____transform0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transform_m20348C56E245E750A6399FA6F7C2D767EE11B91C____transform0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transformed_m09422ADB914E6917056FB53FEDDC172E7AEE88C1____transform0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transformed_mE44D84A994DBAAD9EED74F026F8220ADCD9A249E____transform0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6____bounds0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void WorkItem_t56FC5C5B78A8B954F21E1DCF01980FFAA4B2ECE3_CustomAttributesCacheGenerator_constraints(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C * tmp = (FixedBufferAttribute_tA3523076C957FC980B0B4445B25C2D4AA626DC4C *)cache->attributes[0];
		FixedBufferAttribute__ctor_m7767B7379CFADD0D12551A5A891BF1916A07BE51(tmp, il2cpp_codegen_type_get_object(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_0_0_0_var), 64LL, NULL);
	}
}
static void U3CconstraintsU3Ee__FixedBuffer_tCECED784172DA0B4B7B94438FFDA15C8004E3A8E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC * tmp = (UnsafeValueTypeAttribute_tC3B73880876B0FA7C68CE8A678FD4D6440438CAC *)cache->attributes[1];
		UnsafeValueTypeAttribute__ctor_mA5A3D4443A6B4BE3B31E8A8919809719991A7EC4(tmp, NULL);
	}
}
static void BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator_batchMasks(CustomAttributesCache* cache)
{
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[0];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
}
static void BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator_batchIndices(CustomAttributesCache* cache)
{
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[0];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
}
static void BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator_lut(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CountSortPerFirstParticleJob_1_tC2CCC6E46DF5EB7E8FE546B7B4B82A00F94DD081_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void CountSortPerFirstParticleJob_1_tC2CCC6E46DF5EB7E8FE546B7B4B82A00F94DD081_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void CountSortPerFirstParticleJob_1_tC2CCC6E46DF5EB7E8FE546B7B4B82A00F94DD081_CustomAttributesCacheGenerator_digitCount(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator_InOutArray(CustomAttributesCache* cache)
{
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[0];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator_NextElementIndex(CustomAttributesCache* cache)
{
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[0];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
	{
		NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 * tmp = (NativeDisableContainerSafetyRestrictionAttribute_t138EDB45CE62A51C3779A77CDBF6E28309DF59A9 *)cache->attributes[1];
		NativeDisableContainerSafetyRestrictionAttribute__ctor_m7BE01587CDE8989EB499B7A9E0FD8DC35D330993(tmp, NULL);
	}
}
static void SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator_comparer(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void Cell_1_tA5855F4779165EBE0025883E7E0EF8C13B8AF457_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_CustomAttributesCacheGenerator_simplexBounds(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_CustomAttributesCacheGenerator_is2D(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_CustomAttributesCacheGenerator_cellCoords(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_CustomAttributesCacheGenerator_simplexCount(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_grid(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_gridLevels(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[1];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_restPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_normals(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_fluidRadii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_phases(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_filters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_simplexBounds(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_contactsQueue(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_fluidInteractionsQueue(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_dt(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_collisionMargin(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_optimizationIterations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_optimizationTolerance(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_grid(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_cellOffsets(CustomAttributesCache* cache)
{
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[0];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[1];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_properties(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_diffusePositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_densityKernel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_gridLevels(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
	{
		DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 * tmp = (DeallocateOnJobCompletionAttribute_t9DD74D14DC0E26E36F239BC9A51229AEDC02DC59 *)cache->attributes[1];
		DeallocateOnJobCompletionAttribute__ctor_m8C49B79F5DEDA9BC98803C94D4204EA80E2487A7(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_inertialFrame(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_mode2D(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C_CustomAttributesCacheGenerator_BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524____rigidbody0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C_CustomAttributesCacheGenerator_BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524____solver2World1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_grid(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_filters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_shapes(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_transforms(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_results(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[1];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_worldToSolver(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_parameters(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_SpatialQueryJob_CalculateShapeAABB_mD4BB4AAFB5B7C57689D4C530F27046BAEEBBF9D2____shape0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8____shape0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8____shapeToSolver1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_prevPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_prevOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_angularVel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_inertialAccel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_eulerAccel(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_worldLinearInertiaScale(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_worldAngularInertiaScale(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator_bounds(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator_stride(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator_size(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_radii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_fluidRadii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_simplices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_simplexCounts(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_particleMaterialIndices(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_collisionMaterials(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_collisionMargin(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_continuousCollisionDetection(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_dt(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void DequeueIntoArrayJob_1_t8504B045447DE717E5E62E80D8E9EDE0728D980E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void DequeueIntoArrayJob_1_t8504B045447DE717E5E62E80D8E9EDE0728D980E_CustomAttributesCacheGenerator_OutputArray(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
}
static void FindFluidParticlesJob_tAF41C095A577527A7A06493C1BB2C57D57B54657_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void FindFluidParticlesJob_tAF41C095A577527A7A06493C1BB2C57D57B54657_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void FindFluidParticlesJob_tAF41C095A577527A7A06493C1BB2C57D57B54657_CustomAttributesCacheGenerator_phases(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_startPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_renderablePositions(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_startOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_renderableOrientations(CustomAttributesCache* cache)
{
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[0];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_unsimulatedTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_interpolationMode(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_phases(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_buoyancies(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_externalForces(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_inverseMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_previousPositions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_externalTorques(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_inverseRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_previousOrientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_angularVelocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_gravity(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_is2D(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_inverseMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_inverseRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_principalRadii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_inverseInertiaTensors(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdateNormalsJob_tD7CF866E9115C61E3072DC9E489F8ACB86C2DA09_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateNormalsJob_tD7CF866E9115C61E3072DC9E489F8ACB86C2DA09_CustomAttributesCacheGenerator_deformableTriangles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateNormalsJob_tD7CF866E9115C61E3072DC9E489F8ACB86C2DA09_CustomAttributesCacheGenerator_renderPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_previousPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_previousOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_angularVelocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_velocityScale(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_sleepThreshold(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_renderableOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_phases(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_principalRadii(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_principalAxis(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 * tmp = (BurstCompileAttribute_t845F9073B3F7D817A249B1F2191CE96B8ADA00D1 *)cache->attributes[0];
		BurstCompileAttribute__ctor_m75F0EF699FB9E5AD644F26B04ACE674ED2C81628(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_activeParticles(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_inverseMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_previousPositions(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[1];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_inverseRotationalMasses(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_previousOrientations(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_angularVelocities(CustomAttributesCache* cache)
{
	{
		NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 * tmp = (NativeDisableParallelForRestrictionAttribute_t53B8478A2BD79DD7A9C47B1E2EC7DF38501FC743 *)cache->attributes[0];
		NativeDisableParallelForRestrictionAttribute__ctor_m1D60D0ADDB3C5B35ECD76CBBED3A27037FA44BDD(tmp, NULL);
	}
	{
		WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA * tmp = (WriteOnlyAttribute_t6897770F57B21F93E440F44DF3D1A5804D6019FA *)cache->attributes[1];
		WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_deltaTime(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_is2D(CustomAttributesCache* cache)
{
	{
		ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D * tmp = (ReadOnlyAttribute_tCC6735BA1767371FBF636DC57BA8A8A4E4AE7F8D *)cache->attributes[0];
		ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF(tmp, NULL);
	}
}
static void ObiAerodynamicConstraintsBatch_tDA5413517E58B2320FF2DC31BEDA23C71B42E746_CustomAttributesCacheGenerator_aerodynamicCoeffs(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_restBends(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_bendingStiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_plasticity(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_restDarbouxVectors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_plasticity(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_firstParticle(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_numParticles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_lengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDs(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDToIndex(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ActiveConstraintCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_InitialActiveConstraintCount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_lambdas(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_pinBodies(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_colliderIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_offsets(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_restDarbouxVectors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_breakThresholds(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinPoints(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinNormals(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinRadiiBackstop(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinCompliance(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_orientationIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restLengths(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_maxLengthsScales(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_firstTriangle(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_numTriangles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_restVolumes(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_pressureStiffness(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiConstraints_1_t814AA25938B7022E214F5C54272335BCECBACF49_CustomAttributesCacheGenerator_batches(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_OnBlueprintGenerate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_Empty(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_ActiveParticleCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_InitialActiveParticleCount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator__bounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_positions(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restPositions(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_orientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restOrientations(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_velocities(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_angularVelocities(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invMasses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invRotationalMasses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_filters(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x70\x68\x61\x73\x65\x73"), NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_principalRadii(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_colors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_points(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_edges(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_triangles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_distanceConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_skinConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_tetherConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_stretchShearConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendTwistConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_shapeMatchingConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_aerodynamicConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_chainConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_volumeConstraintsData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_groups(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_0_0_0_var), NULL);
	}
}
static void ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_0_0_0_var), NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_SourceCollider(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x73\x6F\x75\x72\x63\x65\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x72\x63\x65\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_DistanceField(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x74\x61\x6E\x63\x65\x46\x69\x65\x6C\x64"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x74\x61\x6E\x63\x65\x46\x69\x65\x6C\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator_sourceCollider(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x72\x63\x65\x43\x6F\x6C\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_thickness(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[1];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x63\x6B\x6E\x65\x73\x73"), NULL);
	}
}
static void ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_material(CustomAttributesCache* cache)
{
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[0];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_filter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiColliderWorld_t097AB009E3BABD5E1B19CBDA94BDBF223049CEA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x63\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x6D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 180LL, NULL);
	}
}
static void ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingContacts(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D(tmp, NULL);
	}
}
static void ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingFriction(CustomAttributesCache* cache)
{
	{
		Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584 * tmp = (Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584 *)cache->attributes[0];
		Indent__ctor_m5A092B8D80C878888D885C6ADC0B3A9D3A90E3C7(tmp, NULL);
	}
	{
		VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3 * tmp = (VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3 *)cache->attributes[1];
		VisibleIf__ctor_m74CCE1E688720090402D0E285F11AA7EE004E6C6(tmp, il2cpp_codegen_string_new_wrapper("\x72\x6F\x6C\x6C\x69\x6E\x67\x43\x6F\x6E\x74\x61\x63\x74\x73"), false, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[1];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x69\x65\x6C\x64"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x69\x2F\x44\x69\x73\x74\x61\x6E\x63\x65\x20\x46\x69\x65\x6C\x64"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 181LL, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 * tmp = (SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0 *)cache->attributes[1];
		SerializeProperty__ctor_mF607BA112BF60A359F1C32ECA1F8F473737FE5E3(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x70\x75\x74\x4D\x65\x73\x68"), NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_minNodeSize(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_bounds(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_nodes(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxError(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.00000001E-07f, 0.100000001f, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxDepth(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 8.0f, NULL);
	}
}
static void ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_0_0_0_var), NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_tA63F9C4000CEB9B4FE78BFDCB600DD468CD3A6BD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiRigidbody_tC8F0A6971374DEE39EE5F6CCD90EBF5DD0EA2660_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_0_0_0_var), NULL);
	}
}
static void ObiRigidbody2D_t43E9E6CD4E39486D4BAA830D7F225D323241A318_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
}
static void ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ASDF_tE13A467BED85E73DEF9E1A637B2471D28DFB9665_CustomAttributesCacheGenerator_ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t9515851623053EA80E03D72E699F5A819F304CD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____node3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____point4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178____point4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____node4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____point5(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_m_AlignBytes(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_ObiNativeList_1_GetEnumerator_mAA681A9160D0F5ABB78834DDCE50A090F9027FFD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47__ctor_m44C213C4523E721D89454E9E0DB188309DAA0C2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_IDisposable_Dispose_m3677A4910B2FBA94AB21EC467EA0F8D017654F32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m577FD8C7518180375EAF528F1953D6A86B76129F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_Reset_m59513FC87FBEC9A3F46BF8F1A27BE3F8C45CA05D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_get_Current_m45C9E58B0197FD608861CCFE12CE9E08134F9D32(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator_ObiList_1_GetEnumerator_m669DC89479304E618D4B41858753616F692A64B8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2__ctor_m220A0BB8B4845E3E03C861BC45721DD12E9E7C7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_m6C1E284C049482492472BDE417FEC2D9BC5F53E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8D4794A9AC960B97A584FB17906AFA27C95E4C0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE6DC01E508903376308CBEEFDB0FD2C1990A7DEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_m34A4E8F5E7BB811C2830337581A6877BD404D22B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ParticlePair_t3ED4B7BFA5D10A75C057CC637E2D4719B6E15A8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_voxels(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089____coords0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1____point0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B____coords0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____bounds0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v11(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v22(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v33(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_0_0_0_var), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_0_0_0_var), NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC____box0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v00(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v11(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v22(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____aabbExtents3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____axis4(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PriorityQueue_1_tDBFAA8299A447FDE7A02B777CB2142412A0808C9_CustomAttributesCacheGenerator_PriorityQueue_1_GetEnumerator_mE59F3123BC2AC2349A2019082A77861F7C5E19B9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_0_0_0_var), NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5__ctor_mE283398E42D28AE61C9072FB8E231BDCA9867986(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_IDisposable_Dispose_m84D4F91D281E15BADD9CF82AE81CE1F0B9175998(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m9BA3FA8D6F2F77E738345A1A1EE37171EF68A96F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_Reset_m01671BA954E40111F5CCF141F7B255B8F41519F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_get_Current_mA8032079CD99DE6D6B0A0C8AF31E894B4C7AADE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mDD221B9EC48424B9BBA073FC887B08439C91DA49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerable_GetEnumerator_m050DABA4F326191228EDB111C0B34B0E6DE60B0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA_CustomAttributesCacheGenerator_VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_0_0_0_var), NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC____start0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714____start0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5____start0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x44\x69\x73\x74\x61\x6E\x63\x65\x20\x46\x69\x65\x6C\x64\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 1003LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_0_0_0_var), NULL);
	}
}
static void ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator_slice(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiInstancedParticleRenderer_t5F00A7A1B77C575661533F2B2DAA2E1DD63BD4DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x49\x6E\x73\x74\x61\x6E\x63\x65\x64\x20\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 1001LL, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x52\x65\x6E\x64\x65\x72\x65\x72"), 1000LL, NULL);
	}
}
static void ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator_U3CParticleMaterialU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator_ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator_ObiParticleRenderer_set_ParticleMaterial_m271A889A40C59DDA14EF2DCA96DD1B93E40CC3F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x53\x6F\x6C\x76\x65\x72"), 800LL, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnCollision(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnParticleCollision(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnUpdateParameters(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareFrame(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnBeginStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnSubstep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnEndStep(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnInterpolate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_simulateWhenInvisible(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x65\x6E\x61\x62\x6C\x65\x64\x2C\x20\x77\x69\x6C\x6C\x20\x66\x6F\x72\x63\x65\x20\x74\x68\x65\x20\x73\x6F\x6C\x76\x65\x72\x20\x74\x6F\x20\x6B\x65\x65\x70\x20\x73\x69\x6D\x75\x6C\x61\x74\x69\x6E\x67\x20\x65\x76\x65\x6E\x20\x77\x68\x65\x6E\x20\x6E\x6F\x74\x20\x76\x69\x73\x69\x62\x6C\x65\x20\x66\x72\x6F\x6D\x20\x61\x6E\x79\x20\x63\x61\x6D\x65\x72\x61\x2E"), NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_Backend(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldLinearInertiaScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldAngularInertiaScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_actors(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_ParticleToActor(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyActiveParticles(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtySimplices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyConstraints(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x46\x69\x78\x65\x64\x20\x55\x70\x64\x61\x74\x65\x72"), 801LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x70\x65\x72\x20\x46\x69\x78\x65\x64\x55\x70\x64\x61\x74\x65\x2E\x20\x49\x6E\x63\x72\x65\x61\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x67\x72\x65\x61\x74\x6C\x79\x20\x69\x6D\x70\x72\x6F\x76\x65\x73\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x61\x6E\x64\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x4C\x61\x74\x65\x20\x46\x69\x78\x65\x64\x20\x55\x70\x64\x61\x74\x65\x72"), 802LL, NULL);
	}
}
static void ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x70\x65\x72\x20\x46\x69\x78\x65\x64\x55\x70\x64\x61\x74\x65\x2E\x20\x49\x6E\x63\x72\x65\x61\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x67\x72\x65\x61\x74\x6C\x79\x20\x69\x6D\x70\x72\x6F\x76\x65\x73\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x61\x6E\x64\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_0_0_0_var), NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6__ctor_m3269F746C194951D4FFCB5CFE81A956F7C6913F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_mFEF635C7933D3DD06A06668B01022B79D7348076(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BC60167DF9644F2049A2591D4130F2190A2A4C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_m55403139467BF8AF39A859E7823363583E55162B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_m8E98B873A2E9B9BE3DEE0CA669677BA2ECDFE0FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x4C\x61\x74\x65\x20\x55\x70\x64\x61\x74\x65\x72"), 802LL, NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_deltaSmoothing(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6D\x6F\x6F\x74\x68\x69\x6E\x67\x20\x66\x61\x63\x74\x6F\x72\x20\x66\x6F\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x73\x74\x65\x70\x20\x28\x73\x6D\x6F\x6F\x74\x68\x44\x65\x6C\x74\x61\x29\x2E\x20\x56\x61\x6C\x75\x65\x73\x20\x63\x6C\x6F\x73\x65\x72\x20\x74\x6F\x20\x31\x20\x77\x69\x6C\x6C\x20\x79\x69\x65\x6C\x64\x20\x73\x74\x61\x62\x6C\x65\x72\x20\x73\x69\x6D\x75\x6C\x61\x74\x69\x6F\x6E\x2C\x20\x62\x75\x74\x20\x69\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6F\x66\x66\x2D\x73\x79\x6E\x63\x20\x77\x69\x74\x68\x20\x72\x65\x6E\x64\x65\x72\x69\x6E\x67\x2E"), NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_smoothDelta(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x20\x74\x69\x6D\x65\x73\x74\x65\x70\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x64\x76\x61\x6E\x63\x65\x20\x74\x68\x65\x20\x73\x69\x6D\x75\x6C\x61\x74\x69\x6F\x6E\x2E\x20\x54\x68\x65\x20\x75\x70\x64\x61\x74\x65\x72\x20\x77\x69\x6C\x6C\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x77\x69\x74\x68\x20\x54\x69\x6D\x65\x2E\x64\x65\x6C\x74\x61\x54\x69\x6D\x65\x20\x74\x6F\x20\x66\x69\x6E\x64\x20\x74\x68\x65\x20\x61\x63\x74\x75\x61\x6C\x20\x74\x69\x6D\x65\x73\x74\x65\x70\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x65\x61\x63\x68\x20\x66\x72\x61\x6D\x65\x2E"), NULL);
	}
}
static void ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_substeps(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x70\x65\x72\x20\x46\x69\x78\x65\x64\x55\x70\x64\x61\x74\x65\x2E\x20\x49\x6E\x63\x72\x65\x61\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x73\x75\x62\x73\x74\x65\x70\x73\x20\x67\x72\x65\x61\x74\x6C\x79\x20\x69\x6D\x70\x72\x6F\x76\x65\x73\x20\x61\x63\x63\x75\x72\x61\x63\x79\x20\x61\x6E\x64\x20\x63\x6F\x6E\x76\x65\x72\x67\x65\x6E\x63\x65\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void ObiUpdater_tEDDEEBBDCA484C0905FCEEF73A513392DA1245B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ChildrenOnly_t27661D32C6D07127A678EA62748D58AEA19F47F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void InspectorButtonAttribute_t361DBAA21C1410ED19C67A38FDA4182A3BF3F36C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MinMaxAttribute_t95F00C66854692E74D4564051A2C5B744E864D74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MultiDelayed_tD671996D67E3E5A430A576B2B670D47D1D4EAD39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void MultiRange_t05AA64C7BC22CB3BFD931E9DE77DFC5C08DC8C2D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_U3CPropertyNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CMethodNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CNegateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CoroutineJob_t80DB8BDF56DB40B9329335C29878C9D46D8C879F_CustomAttributesCacheGenerator_CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ObiContactEventDispatcher_tE03476E199D615BC2771BEBCF78A8D89F74DB738_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x50\x68\x79\x73\x69\x63\x73\x2F\x4F\x62\x69\x2F\x4F\x62\x69\x20\x50\x61\x72\x74\x69\x63\x6C\x65\x20\x41\x74\x74\x61\x63\x68\x6D\x65\x6E\x74"), 820LL, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Actor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ParticleGroup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_AttachmentType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ConstrainOrientation(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Compliance(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_BreakThreshold(CustomAttributesCache* cache)
{
	{
		DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948 * tmp = (DelayedAttribute_t84E6423E615599EDDEAA39BC245C5B871A2D9948 *)cache->attributes[0];
		DelayedAttribute__ctor_mA73AFD50BA612774A729641927483BC80142A11A(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiParticleDragger_tB8EA35A74E226E0A73483C34703F508483E93095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiParticlePicker_tB0405EAD1BFCF9EBDD82F494C3DE49EBECD0F732_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiParticlePicker_tB0405EAD1BFCF9EBDD82F494C3DE49EBECD0F732_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var), NULL);
	}
}
static void ObiParticleGridDebugger_tE80EC21184C40EAE8D508C93B9EEFB00D69B0BB6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_0_0_0_var), NULL);
	}
}
static void ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_skin(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x70\x70\x65\x61\x72\x61\x6E\x63\x65"), NULL);
	}
}
static void ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_showPercentages(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x73\x75\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stitches(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor1(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor2(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_particleIndices(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stiffnesses(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_lambdas(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_mF56D360C3B427C99DB48F907D7B8F1F2D96B2474(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_m4545B5452717048909109CF73C8CAC6880F7F227(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftLeft_m983E32714E08933EE70C58FD314AB2F11C10DB5D(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftRight_m0CCEF49D7BEEB3D21A740CEEF10750BFA3E10ED5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_0_0_0_var), NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____A0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____B1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____C2(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____P3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p10(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p21(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p32(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____coords3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2____hint_normal3(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 * tmp = (IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_mA5B530204715922B8E11EA362EE266FA7E65613D(tmp, NULL);
	}
}
static void ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SetCategory_t5394A96DA7AC622E8E85A161EA2AA22C1D08CDC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_0_0_0_var), NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_tA5C757109DE911EDFAAE6282BDA413CB8C7D1E83_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Obi_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Obi_AttributeGenerators[1352] = 
{
	EmbeddedAttribute_t52C13F777C267E773FD800941E3F3577D0B71C82_CustomAttributesCacheGenerator,
	IsReadOnlyAttribute_tA55FD0A1ADF3731C0573BF14AA86D510FE7648D3_CustomAttributesCacheGenerator,
	IsUnmanagedAttribute_t939FAB91AE2EC449A87EAD0A37591BA55859D86B_CustomAttributesCacheGenerator,
	ObiContactGrabber_t14D0338C4C370DF79A94FA1DE76A34470B4DF04A_CustomAttributesCacheGenerator,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator,
	ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator,
	U3CU3Ec_t5EBFA052AD90FD6A9779D3B8868C4C2B2B11E855_CustomAttributesCacheGenerator,
	ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator,
	U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator,
	U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator,
	U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator,
	U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t04A48055519B751B310989CFBF1ACB6A168C1BF9_CustomAttributesCacheGenerator,
	U3CU3Ec_t2F8F7BD3F1F5DCE39BAAD0D8431328B2888E5FA9_CustomAttributesCacheGenerator,
	U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator,
	U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator,
	ObiSkinnedClothBlueprint_t2303B8E824B4492591ADD3E67FD1E78E5C56F197_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator,
	U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator,
	U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator,
	U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator,
	U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator,
	U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator,
	ObiClothProxy_t723D5DFCA0F551D6CF9F28B05123D18CC8972126_CustomAttributesCacheGenerator,
	ObiClothRenderer_t3FA62EDF5E0CBB7F6B8450CF30E5BB98CAE9BB75_CustomAttributesCacheGenerator,
	ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator,
	ObiClothRendererMeshFilter_t57EF050279606827C072D4A1B145A6932A2D2CEF_CustomAttributesCacheGenerator,
	ObiSkinnedClothRenderer_tC0F584117B4933DA72BC0CFD46C22085D3AA9064_CustomAttributesCacheGenerator,
	ObiTearableClothRenderer_t05133F45C5382054CD84D54AA4F3A3C208984A4F_CustomAttributesCacheGenerator,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator,
	UpdateMovingColliders_tC532706751FD034881CE6740A29342E6BF6287B9_CustomAttributesCacheGenerator,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator,
	ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator,
	ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator,
	ClearFluidDataJob_tAD2A22F6DBC6DE882E7809B1468ABB46F011C22A_CustomAttributesCacheGenerator,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator,
	IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator,
	AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator,
	AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator,
	ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator,
	ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator,
	ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator,
	U3CconstraintsU3Ee__FixedBuffer_tCECED784172DA0B4B7B94438FFDA15C8004E3A8E_CustomAttributesCacheGenerator,
	BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator,
	CountSortPerFirstParticleJob_1_tC2CCC6E46DF5EB7E8FE546B7B4B82A00F94DD081_CustomAttributesCacheGenerator,
	SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator,
	Cell_1_tA5855F4779165EBE0025883E7E0EF8C13B8AF457_CustomAttributesCacheGenerator,
	CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_CustomAttributesCacheGenerator,
	UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_CustomAttributesCacheGenerator,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator,
	CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator,
	ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator,
	BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator,
	DequeueIntoArrayJob_1_t8504B045447DE717E5E62E80D8E9EDE0728D980E_CustomAttributesCacheGenerator,
	FindFluidParticlesJob_tAF41C095A577527A7A06493C1BB2C57D57B54657_CustomAttributesCacheGenerator,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator,
	UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator,
	UpdateNormalsJob_tD7CF866E9115C61E3072DC9E489F8ACB86C2DA09_CustomAttributesCacheGenerator,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator,
	UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator,
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator,
	ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator,
	ObiColliderWorld_t097AB009E3BABD5E1B19CBDA94BDBF223049CEA2_CustomAttributesCacheGenerator,
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator,
	U3CU3Ec_tA63F9C4000CEB9B4FE78BFDCB600DD468CD3A6BD_CustomAttributesCacheGenerator,
	ObiRigidbody_tC8F0A6971374DEE39EE5F6CCD90EBF5DD0EA2660_CustomAttributesCacheGenerator,
	ObiRigidbody2D_t43E9E6CD4E39486D4BAA830D7F225D323241A318_CustomAttributesCacheGenerator,
	ObiRigidbodyBase_tA95EE97A5497D66D571ABA3D52491F363471272A_CustomAttributesCacheGenerator,
	U3CU3Ec_t2057E152C51977AC036D0199FDD3A55EF6894232_CustomAttributesCacheGenerator,
	U3CU3Ec_t9515851623053EA80E03D72E699F5A819F304CD9_CustomAttributesCacheGenerator,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator,
	ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator,
	ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator,
	ParticlePair_t3ED4B7BFA5D10A75C057CC637E2D4719B6E15A8C_CustomAttributesCacheGenerator,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator,
	Voxel_tDC7ED04FB9683A1CAF022400103FC1FD89F6CFE3_CustomAttributesCacheGenerator,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator,
	U3CU3Ec_t177673DEF2FC3A3125B66EE983FF1D10F776D816_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_tA79FD55395E4DEB0F920253ECAC7EE0FD46ECA4E_CustomAttributesCacheGenerator,
	ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator,
	ObiInstancedParticleRenderer_t5F00A7A1B77C575661533F2B2DAA2E1DD63BD4DD_CustomAttributesCacheGenerator,
	ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator,
	U3CU3Ec_tEF9F04C3C79DD355625F5BEE0CA1CFF45BC74E16_CustomAttributesCacheGenerator,
	ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator,
	ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator,
	U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator,
	ObiUpdater_tEDDEEBBDCA484C0905FCEEF73A513392DA1245B1_CustomAttributesCacheGenerator,
	ChildrenOnly_t27661D32C6D07127A678EA62748D58AEA19F47F7_CustomAttributesCacheGenerator,
	Indent_tCA2D04176BA7E8D9F584653DD0400CD256185584_CustomAttributesCacheGenerator,
	InspectorButtonAttribute_t361DBAA21C1410ED19C67A38FDA4182A3BF3F36C_CustomAttributesCacheGenerator,
	MinMaxAttribute_t95F00C66854692E74D4564051A2C5B744E864D74_CustomAttributesCacheGenerator,
	MultiDelayed_tD671996D67E3E5A430A576B2B670D47D1D4EAD39_CustomAttributesCacheGenerator,
	MultiPropertyAttribute_tD831A4A240690E32C80AEF58B3E877919F8BB38E_CustomAttributesCacheGenerator,
	MultiRange_t05AA64C7BC22CB3BFD931E9DE77DFC5C08DC8C2D_CustomAttributesCacheGenerator,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator,
	ObiContactEventDispatcher_tE03476E199D615BC2771BEBCF78A8D89F74DB738_CustomAttributesCacheGenerator,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator,
	ObiParticleDragger_tB8EA35A74E226E0A73483C34703F508483E93095_CustomAttributesCacheGenerator,
	ObiParticleGridDebugger_tE80EC21184C40EAE8D508C93B9EEFB00D69B0BB6_CustomAttributesCacheGenerator,
	ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator,
	ParticleFlags_tE8A495C002D8E7A750F2B0BB2F8F5D815F9F807F_CustomAttributesCacheGenerator,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator,
	SetCategory_t5394A96DA7AC622E8E85A161EA2AA22C1D08CDC2_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_tA5C757109DE911EDFAAE6282BDA413CB8C7D1E83_CustomAttributesCacheGenerator,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_mode,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_interpolation,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_gravity,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_damping,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxAnisotropy,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_sleepThreshold,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_collisionMargin,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_maxDepenetration,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_continuousCollisionDetection,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_shockPropagation,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionIterations,
	SolverParameters_tDAA386F30111ABE609FA412CD31F30F08C19E0E6_CustomAttributesCacheGenerator_surfaceCollisionTolerance,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_evaluationOrder,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_iterations,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_SORFactor,
	ConstraintParameters_t08CCB6D9482C89D2CCDB35203E28CC0C1A43DF22_CustomAttributesCacheGenerator_enabled,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator_m_ClothBlueprint,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__volumeConstraintsEnabled,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__compressionCompliance,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__pressure,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__tetherConstraintsEnabled,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__tetherCompliance,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator__tetherScale,
	ObiCloth_tFCD5F10FDFDAAA1F1EE1E5F1B2CABABB5A2D3096_CustomAttributesCacheGenerator_m_renderer,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator_m_SelfCollisions,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator_m_OneSided,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__distanceConstraintsEnabled,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__stretchingScale,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__stretchCompliance,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__maxCompression,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__bendConstraintsEnabled,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__bendCompliance,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__maxBending,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__plasticYield,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__plasticCreep,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__aerodynamicsEnabled,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__drag,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator__lift,
	ObiClothBase_tF8C3C127C516DE73F794A10A1B9930FE7F712025_CustomAttributesCacheGenerator_trianglesOffset,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_m_SkinnedClothBlueprint,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator__tetherConstraintsEnabled,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator__tetherCompliance,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator__tetherScale,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedVertices,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedNormals,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedTangents,
	ObiSkinnedCloth_t7F16639F42E7BA70146094057AAEFD12E659F608_CustomAttributesCacheGenerator_bakedMesh,
	ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_tearDebilitation,
	ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_OnClothTorn,
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_topology,
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_deformableTriangles,
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_restNormals,
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_areaContribution,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_tearCapacity,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_pooledParticles,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_tearResistance,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_distanceConstraintMap,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_containsData,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_vertices,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_halfEdges,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_borderEdges,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_faces,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_restNormals,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_restOrientations,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_rawToWelded,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_bound,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_barycentricWeight,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_normalAlignmentWeight,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_elevationWeight,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_MasterChannels,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_SlaveChannels,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_SlaveTransform,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_Master,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_m_Slave,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_skinnedVertices,
	ObiClothProxy_t723D5DFCA0F551D6CF9F28B05123D18CC8972126_CustomAttributesCacheGenerator_m_Master,
	ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_OnRendererUpdated,
	ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_clothMesh,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintLoaded,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBlueprintUnloaded,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareFrame,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnPrepareStep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnBeginStep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnSubstep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnEndStep,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_OnInterpolate,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_ActiveParticleCount,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverIndices,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_solverBatchOffsets,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_CollisionMaterial,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_m_SurfaceCollisions,
	ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_U3CsolverU3Ek__BackingField,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_movingColliders,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_shapes,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_rigidbodies,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_collisionMaterials,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_colliderCount,
	IdentifyMovingColliders_tEDDD8B90110C1E5E6BA8D6E036FDA482A206C2F1_CustomAttributesCacheGenerator_dt,
	UpdateMovingColliders_tC532706751FD034881CE6740A29342E6BF6287B9_CustomAttributesCacheGenerator_colliderCount,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_colliderGrid,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_gridLevels,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_velocities,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_positions,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_orientations,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_invMasses,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_radii,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_filters,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_simplices,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_simplexCounts,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_simplexBounds,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_transforms,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_shapes,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_collisionMaterials,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_rigidbodies,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_bounds,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_distanceFieldHeaders,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_distanceFieldNodes,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_triangleMeshHeaders,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_bihNodes,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_triangles,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_vertices,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edgeMeshHeaders,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edgeBihNodes,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edges,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_edgeVertices,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_heightFieldHeaders,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_heightFieldSamples,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_contactsQueue,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_solverToWorld,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_worldToSolver,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_deltaTime,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_parameters,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_particleIndices,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_aerodynamicCoeffs,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_positions,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_normals,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_wind,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_invMasses,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_velocities,
	AerodynamicConstraintsBatchJob_t65A4D8E20953D857E3656617DDE3D5EEB350E68B_CustomAttributesCacheGenerator_deltaTime,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_particleIndices,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_stiffnesses,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_plasticity,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_positions,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_invMasses,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_deltas,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_counts,
	BendConstraintsBatchJob_tF6E2C4BEE9EDD2195F803F1446163236AFDA13DD_CustomAttributesCacheGenerator_deltaTime,
	ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_particleIndices,
	ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_sorFactor,
	ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_positions,
	ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_deltas,
	ApplyBendConstraintsBatchJob_t55D850AAF91D104479296CBEDCFC8F9BEB90997A_CustomAttributesCacheGenerator_counts,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientationIndices,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_stiffnesses,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_plasticity,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientations,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_invRotationalMasses,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientationDeltas,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_orientationCounts,
	BendTwistConstraintsBatchJob_t547735E645FB1BC1B12F435AB2AE2433D5EDD23C_CustomAttributesCacheGenerator_deltaTime,
	ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientationIndices,
	ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_sorFactor,
	ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientations,
	ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientationDeltas,
	ApplyBendTwistConstraintsBatchJob_t3E7D91212732122791FCDCF597AA719E2DC5ACD3_CustomAttributesCacheGenerator_orientationCounts,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_particleIndices,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_firstIndex,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_numIndices,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_restLengths,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_positions,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_invMasses,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_deltas,
	ChainConstraintsBatchJob_t15697B4A0E9DA9374FA7804DD236A8CA183F4AC0_CustomAttributesCacheGenerator_counts,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_particleIndices,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_firstIndex,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_numIndices,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_sorFactor,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_positions,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_deltas,
	ApplyChainConstraintsBatchJob_t77B46BB6E7314D3091F234A728881C577B623156_CustomAttributesCacheGenerator_counts,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_contacts,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_simplices,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_simplexCounts,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_positions,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_deltas,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_counts,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_orientations,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_orientationDeltas,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_orientationCounts,
	ApplyCollisionConstraintsBatchJob_t24F3B6FE059DEE78CEBB73A909373588E9AD8E77_CustomAttributesCacheGenerator_constraintParameters,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_prevPositions,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_prevOrientations,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_velocities,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_radii,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_invMasses,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_invInertiaTensors,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_particleMaterialIndices,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_collisionMaterials,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_simplices,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_simplexCounts,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_shapes,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_transforms,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_rigidbodies,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_rigidbodyLinearDeltas,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_rigidbodyAngularDeltas,
	UpdateContactsJob_t3B14A829C6ED5E261C6A225EAF2E295B730808EE_CustomAttributesCacheGenerator_inertialFrame,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_prevPositions,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_orientations,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_prevOrientations,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_invMasses,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_radii,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_particleMaterialIndices,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_simplices,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_simplexCounts,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_shapes,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_transforms,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_collisionMaterials,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_rigidbodies,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_positions,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_deltas,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_counts,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_inertialFrame,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_constraintParameters,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_solverParameters,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_stepTime,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_substepTime,
	CollisionConstraintsBatchJob_tFCA769A2B64B1C361995E946D1228C5E1AD01CF6_CustomAttributesCacheGenerator_substeps,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_positions,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_prevPositions,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_orientations,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_prevOrientations,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_invMasses,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_invInertiaTensors,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_radii,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_particleMaterialIndices,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_simplices,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_simplexCounts,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_shapes,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_transforms,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_collisionMaterials,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_rigidbodies,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_deltas,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_counts,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_orientationDeltas,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_orientationCounts,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_inertialFrame,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_stepTime,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_substepTime,
	FrictionConstraintsBatchJob_tEF6BAB992DA17A41E09954BA962C5D63032E36F8_CustomAttributesCacheGenerator_substeps,
	ClearFluidDataJob_tAD2A22F6DBC6DE882E7809B1468ABB46F011C22A_CustomAttributesCacheGenerator_fluidParticles,
	ClearFluidDataJob_tAD2A22F6DBC6DE882E7809B1468ABB46F011C22A_CustomAttributesCacheGenerator_fluidData,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_positions,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_radii,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_densityKernel,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_gradientKernel,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_pairs,
	UpdateInteractionsJob_tB1BB3BE865DCA994A67A8CE4067E52F464B972F9_CustomAttributesCacheGenerator_batchData,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_fluidParticles,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_invMasses,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_radii,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_restDensities,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_surfaceTension,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_densityKernel,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_gradientKernel,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_normals,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_vorticity,
	CalculateLambdasJob_t4FCD84E5B292833341B7B19935CC28A59F72FBFC_CustomAttributesCacheGenerator_fluidData,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_fluidParticles,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_wind,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_vorticities,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_atmosphericDrag,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_atmosphericPressure,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_vorticityConfinement,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_restDensities,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_normals,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_fluidData,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_eta,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_velocities,
	ApplyVorticityConfinementAndAtmosphere_tB3720FCA917A99623C789B1684D2CD4F39E67647_CustomAttributesCacheGenerator_dt,
	IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator_fluidParticles,
	IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator_radii,
	IdentityAnisotropyJob_tDEE3966F4508D93D1D5E6D139598B272DA1F0782_CustomAttributesCacheGenerator_principalAxes,
	AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator_fluidParticles,
	AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator_renderablePositions,
	AverageSmoothPositionsJob_t6C393A82FFE6CD54BFF0A9A4CE0069981C918D0B_CustomAttributesCacheGenerator_smoothPositions,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_fluidParticles,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_principalRadii,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_maxAnisotropy,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_smoothPositions,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_anisotropies,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_renderablePositions,
	AverageAnisotropyJob_tCC87873E8D0D15FB82EE6245D9ADBB73CD5E86EE_CustomAttributesCacheGenerator_principalAxes,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_positions,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_invMasses,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_restDensities,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_diffusion,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_pairs,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_userData,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_fluidData,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_batchData,
	UpdateDensitiesJob_t4EAA9441134E6E30D6657001A6032049E0B73BE5_CustomAttributesCacheGenerator_dt,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_invMasses,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_radii,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_restDensities,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_surfaceTension,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_pairs,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_densityKernel,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_positions,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_fluidData,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_batchData,
	ApplyDensityConstraintsJob_tD1BD1977CD1469A23BC5D23D1F6D4F703DEB41DC_CustomAttributesCacheGenerator_sorFactor,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_positions,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_invMasses,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_radii,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_restDensities,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_viscosities,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_fluidData,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_pairs,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_velocities,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_vorticities,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_normals,
	NormalsViscosityAndVorticityJob_tA3D325354DB5C31A62CF9DEE1671B726CD31A71B_CustomAttributesCacheGenerator_batchData,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_vorticities,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_invMasses,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_restDensities,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_pairs,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_eta,
	CalculateVorticityEta_t6C687F6830F24F96672C6C54316A85D41C7DA8EF_CustomAttributesCacheGenerator_batchData,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_renderablePositions,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_radii,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_densityKernel,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_smoothPositions,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_pairs,
	AccumulateSmoothPositionsJob_tB95CE9231D694FDA136107B755B4DD2CE05010F7_CustomAttributesCacheGenerator_batchData,
	AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_renderablePositions,
	AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_smoothPositions,
	AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_pairs,
	AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_anisotropies,
	AccumulateAnisotropyJob_t30813CBCC55ACEA0F86231D868CD92EF3D47F5D8_CustomAttributesCacheGenerator_batchData,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_particleIndices,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_restLengths,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_stiffnesses,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_positions,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_invMasses,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_deltas,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_counts,
	DistanceConstraintsBatchJob_tB5A9217268805A24A9DACC31118E6D7234F43D5A_CustomAttributesCacheGenerator_deltaTimeSqr,
	ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_particleIndices,
	ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_sorFactor,
	ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_positions,
	ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_deltas,
	ApplyDistanceConstraintsBatchJob_t727517ECDB0764E5DB626C3FFFB6F4B947C56181_CustomAttributesCacheGenerator_counts,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_contacts,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_simplices,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_simplexCounts,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_positions,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_deltas,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_counts,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_orientations,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_orientationDeltas,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_orientationCounts,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_constraintParameters,
	ApplyBatchedCollisionConstraintsBatchJob_tADC7A51B04CB1EEDBB7D7A73906928D19ED8D214_CustomAttributesCacheGenerator_batchData,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_prevPositions,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_prevOrientations,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_velocities,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_radii,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_invMasses,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_invInertiaTensors,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_particleMaterialIndices,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_collisionMaterials,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_simplices,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_simplexCounts,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_contacts,
	UpdateParticleContactsJob_t50FE3B02E0170DAFAE336DE4F43AFB8A47D6F1DB_CustomAttributesCacheGenerator_batchData,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_orientations,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_invMasses,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_radii,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_particleMaterialIndices,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_collisionMaterials,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_simplices,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_simplexCounts,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_positions,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_deltas,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_counts,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_contacts,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_constraintParameters,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_solverParameters,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_gravity,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_substepTime,
	ParticleCollisionConstraintsBatchJob_t5470C088B11FAAFFC23CD97C82CDA4FCADBA7032_CustomAttributesCacheGenerator_batchData,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_positions,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_prevPositions,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_orientations,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_prevOrientations,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_invMasses,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_invInertiaTensors,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_radii,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_particleMaterialIndices,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_collisionMaterials,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_simplices,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_simplexCounts,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_deltas,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_counts,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_orientationDeltas,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_orientationCounts,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_contacts,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_batchData,
	ParticleFrictionConstraintsBatchJob_t91093FF1C0A6D05A293740043CEE90EFBD4D4BB9_CustomAttributesCacheGenerator_substepTime,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_particleIndices,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_colliderIndices,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_offsets,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_stiffnesses,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_restDarboux,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_positions,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_prevPositions,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_invMasses,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_orientations,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_invRotationalMasses,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_shapes,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_transforms,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_rigidbodies,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_deltas,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_counts,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_orientationDeltas,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_orientationCounts,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_inertialFrame,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_stepTime,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_substepTime,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_substeps,
	PinConstraintsBatchJob_t5EE81C91474DDCB79C75426B75E018A813119B89_CustomAttributesCacheGenerator_activeConstraintCount,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_particleIndices,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_sorFactor,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_positions,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_deltas,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_counts,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_orientations,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_orientationDeltas,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_orientationCounts,
	ApplyPinConstraintsBatchJob_t9DDDB4E902B6D135504F582203C0D28E1B15BC82_CustomAttributesCacheGenerator_activeConstraintCount,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_particleIndices,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_firstIndex,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_numIndices,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_coms,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_deformation,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_restPositions,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_restOrientations,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_invMasses,
	ShapeMatchingCalculateRestJob_t25112409C8365BB91FC22A564CAFB7DD1172BA56_CustomAttributesCacheGenerator_invInertiaTensors,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_particleIndices,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_firstIndex,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_numIndices,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_explicitGroup,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_shapeMaterialParameters,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_positions,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_restPositions,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_restOrientations,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_invMasses,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_invRotationalMasses,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_invInertiaTensors,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_orientations,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_deltas,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_counts,
	ShapeMatchingConstraintsBatchJob_tB6ACAADC03968163CC256AEA21D7C6B97634FB78_CustomAttributesCacheGenerator_deltaTime,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_particleIndices,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_firstIndex,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_numIndices,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_sorFactor,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_positions,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_deltas,
	ApplyShapeMatchingConstraintsBatchJob_tF7C9055A684DC6A0F62F2BC7CB72C9563EAE4801_CustomAttributesCacheGenerator_counts,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_particleIndices,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinPoints,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinNormals,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinRadiiBackstop,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_skinCompliance,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_positions,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_invMasses,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_deltas,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_counts,
	SkinConstraintsBatchJob_t6C4499FA6FC42EBC353D06F1AFA659CC1DDB79A8_CustomAttributesCacheGenerator_deltaTimeSqr,
	ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_particleIndices,
	ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_sorFactor,
	ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_positions,
	ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_deltas,
	ApplySkinConstraintsBatchJob_t52CF7DD4AE11497B087ABF2A8EBA6A942EF55268_CustomAttributesCacheGenerator_counts,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_particleIndices,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_stiffnesses,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_positions,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_invMasses,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_deltas,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_counts,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_deltaTimeSqr,
	StitchConstraintsBatchJob_tB65F459FAF9AC72AE93DED743492BB9E61860D36_CustomAttributesCacheGenerator_activeConstraintCount,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_particleIndices,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_sorFactor,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_positions,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_deltas,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_counts,
	ApplyStitchConstraintsBatchJob_t22968E229CB94DAC40566114FEFBF51CC9AB36B8_CustomAttributesCacheGenerator_activeConstraintCount,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_particleIndices,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientationIndices,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_restLengths,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_restOrientations,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_stiffnesses,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_positions,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientations,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_invMasses,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_invRotationalMasses,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_deltas,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientationDeltas,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_counts,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_orientationCounts,
	StretchShearConstraintsBatchJob_tFC7FC4D52303B3F2F885CB70570CE217A2BEEDBA_CustomAttributesCacheGenerator_deltaTimeSqr,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_particleIndices,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientationIndices,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_sorFactor,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_positions,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_deltas,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_counts,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientations,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientationDeltas,
	ApplyStretchShearConstraintsBatchJob_t831176EC26869D9099124D0B1CE7DB113130EEAC_CustomAttributesCacheGenerator_orientationCounts,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_particleIndices,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_maxLengthScale,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_stiffnesses,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_positions,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_invMasses,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_deltas,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_counts,
	TetherConstraintsBatchJob_tCAA66777E1F79CF22339681DCFAFC768C4552E8D_CustomAttributesCacheGenerator_deltaTimeSqr,
	ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_particleIndices,
	ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_positions,
	ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_deltas,
	ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_counts,
	ApplyTetherConstraintsBatchJob_t016F13850B7EA457F4136829E8A99DC00FE5A8A8_CustomAttributesCacheGenerator_sorFactor,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_triangles,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_firstTriangle,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_numTriangles,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_restVolumes,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_pressureStiffness,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_positions,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_invMasses,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_gradients,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_deltas,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_counts,
	VolumeConstraintsBatchJob_tFB0F283B71D046AFA6C0B481251F39D2BB895740_CustomAttributesCacheGenerator_deltaTimeSqr,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_triangles,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_firstTriangle,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_numTriangles,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_sorFactor,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_positions,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_deltas,
	ApplyVolumeConstraintsBatchJob_tEDA393A77DCF2BCA3D6349F099B59C3B5F73E9A1_CustomAttributesCacheGenerator_counts,
	WorkItem_t56FC5C5B78A8B954F21E1DCF01980FFAA4B2ECE3_CustomAttributesCacheGenerator_constraints,
	BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator_batchMasks,
	BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator_batchIndices,
	BatchContactsJob_1_t8CC723E5658ABD9DE4CD21FEECC72C9A5B48C196_CustomAttributesCacheGenerator_lut,
	CountSortPerFirstParticleJob_1_tC2CCC6E46DF5EB7E8FE546B7B4B82A00F94DD081_CustomAttributesCacheGenerator_input,
	CountSortPerFirstParticleJob_1_tC2CCC6E46DF5EB7E8FE546B7B4B82A00F94DD081_CustomAttributesCacheGenerator_digitCount,
	SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator_InOutArray,
	SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator_NextElementIndex,
	SortSubArraysJob_1_t2B2ECCF048901E907C94FE0B8940C2F7F1EE8550_CustomAttributesCacheGenerator_comparer,
	CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_CustomAttributesCacheGenerator_simplexBounds,
	CalculateCellCoords_tA5BBF1614A04C56DA14A5E3459D95A1B7C06591B_CustomAttributesCacheGenerator_is2D,
	UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_CustomAttributesCacheGenerator_cellCoords,
	UpdateGrid_t0F58CFB0B347695074103E35638151D43E92D3CA_CustomAttributesCacheGenerator_simplexCount,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_grid,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_gridLevels,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_positions,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_orientations,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_restPositions,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_restOrientations,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_velocities,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_invMasses,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_radii,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_normals,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_fluidRadii,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_phases,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_filters,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_simplices,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_simplexCounts,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_simplexBounds,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_particleMaterialIndices,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_collisionMaterials,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_contactsQueue,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_fluidInteractionsQueue,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_dt,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_collisionMargin,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_optimizationIterations,
	GenerateParticleParticleContactsJob_t3C2E7DB3DBA89A715BFD5B12793AA7DE938312B5_CustomAttributesCacheGenerator_optimizationTolerance,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_grid,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_cellOffsets,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_positions,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_properties,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_diffusePositions,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_densityKernel,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_gridLevels,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_inertialFrame,
	InterpolateDiffusePropertiesJob_t9CA385CC33504800224F00CA99E3D427E9171826_CustomAttributesCacheGenerator_mode2D,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_grid,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_positions,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_orientations,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_radii,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_filters,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_simplices,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_simplexCounts,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_shapes,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_transforms,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_results,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_worldToSolver,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_parameters,
	CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_prevPositions,
	CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_prevOrientations,
	CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_radii,
	CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_simplices,
	CalculateQueryDistances_t81DA328D960E86C2BCCA400839FF7ACAB83EDD9C_CustomAttributesCacheGenerator_simplexCounts,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_activeParticles,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_positions,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_invMasses,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_angularVel,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_inertialAccel,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_eulerAccel,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_worldLinearInertiaScale,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_worldAngularInertiaScale,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_velocities,
	ApplyInertialForcesJob_t8897D31AB0C9B15C3BBB93DFF62B47D1DAB2E31C_CustomAttributesCacheGenerator_deltaTime,
	ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator_activeParticles,
	ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator_positions,
	ParticleToBoundsJob_t72D77310DC997B68715ECDB7385EDCFDE10A5127_CustomAttributesCacheGenerator_radii,
	BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator_bounds,
	BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator_stride,
	BoundsReductionJob_t1E10340C48CC3710B45ACBF8C9B95A2DD7E1AA6C_CustomAttributesCacheGenerator_size,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_radii,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_fluidRadii,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_positions,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_velocities,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_simplices,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_simplexCounts,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_particleMaterialIndices,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_collisionMaterials,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_collisionMargin,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_continuousCollisionDetection,
	BuildSimplexAabbs_t7E68578A3DA5D9E781254F246C82A055FD3CD44B_CustomAttributesCacheGenerator_dt,
	DequeueIntoArrayJob_1_t8504B045447DE717E5E62E80D8E9EDE0728D980E_CustomAttributesCacheGenerator_OutputArray,
	FindFluidParticlesJob_tAF41C095A577527A7A06493C1BB2C57D57B54657_CustomAttributesCacheGenerator_activeParticles,
	FindFluidParticlesJob_tAF41C095A577527A7A06493C1BB2C57D57B54657_CustomAttributesCacheGenerator_phases,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_startPositions,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_positions,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_renderablePositions,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_startOrientations,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_orientations,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_renderableOrientations,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_deltaTime,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_unsimulatedTime,
	InterpolationJob_t549EF590EEE16B0D9052ECD70333D820E8162C67_CustomAttributesCacheGenerator_interpolationMode,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_activeParticles,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_phases,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_buoyancies,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_externalForces,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_inverseMasses,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_previousPositions,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_positions,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_velocities,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_externalTorques,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_inverseRotationalMasses,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_previousOrientations,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_orientations,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_angularVelocities,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_gravity,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_deltaTime,
	PredictPositionsJob_t0314928CF08F37FAF6B87FC7820C22B238153041_CustomAttributesCacheGenerator_is2D,
	UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_activeParticles,
	UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_inverseMasses,
	UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_inverseRotationalMasses,
	UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_principalRadii,
	UpdateInertiaTensorsJob_tF179B2B4F70447E95B92802B2E63B8DFD1AA72A6_CustomAttributesCacheGenerator_inverseInertiaTensors,
	UpdateNormalsJob_tD7CF866E9115C61E3072DC9E489F8ACB86C2DA09_CustomAttributesCacheGenerator_deformableTriangles,
	UpdateNormalsJob_tD7CF866E9115C61E3072DC9E489F8ACB86C2DA09_CustomAttributesCacheGenerator_renderPositions,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_activeParticles,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_positions,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_previousPositions,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_velocities,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_orientations,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_previousOrientations,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_angularVelocities,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_velocityScale,
	UpdatePositionsJob_tC19E9710E82217CA0FA5E2E59726E076911ECBA7_CustomAttributesCacheGenerator_sleepThreshold,
	UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_activeParticles,
	UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_renderableOrientations,
	UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_phases,
	UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_principalRadii,
	UpdatePrincipalAxisJob_t02535E46DE5F0E709C118B5B1EB928D22E5DAC40_CustomAttributesCacheGenerator_principalAxis,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_activeParticles,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_inverseMasses,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_previousPositions,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_positions,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_velocities,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_inverseRotationalMasses,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_previousOrientations,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_orientations,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_angularVelocities,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_deltaTime,
	UpdateVelocitiesJob_t7CC2DD585BE1376D71C7712224802026520861B3_CustomAttributesCacheGenerator_is2D,
	ObiAerodynamicConstraintsBatch_tDA5413517E58B2320FF2DC31BEDA23C71B42E746_CustomAttributesCacheGenerator_aerodynamicCoeffs,
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_restBends,
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_bendingStiffnesses,
	ObiBendConstraintsBatch_tA09BF7FA622E63D6BBF8CCB31310510E53C65C79_CustomAttributesCacheGenerator_plasticity,
	ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_restDarbouxVectors,
	ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_stiffnesses,
	ObiBendTwistConstraintsBatch_t38B02C7F85380351DF10FD94B77A41D0F69D6B1C_CustomAttributesCacheGenerator_plasticity,
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_firstParticle,
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_numParticles,
	ObiChainConstraintsBatch_t2F3399072B180D6668BFA9F1C7C7D7F67C08E55E_CustomAttributesCacheGenerator_lengths,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDs,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_IDToIndex,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ConstraintCount,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_ActiveConstraintCount,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_m_InitialActiveConstraintCount,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_particleIndices,
	ObiConstraintsBatch_t917657ABA72E51ED1DBFC66976ABE0A1A2CFB127_CustomAttributesCacheGenerator_lambdas,
	ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_restLengths,
	ObiDistanceConstraintsBatch_tDE14B4696F41A80AB6910FE9CA17933B97FA6ECE_CustomAttributesCacheGenerator_stiffnesses,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_pinBodies,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_colliderIndices,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_offsets,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_restDarbouxVectors,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_stiffnesses,
	ObiPinConstraintsBatch_t7C9BDD368856556419B9046EAD8465D7AE5F7A2B_CustomAttributesCacheGenerator_breakThresholds,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinPoints,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinNormals,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinRadiiBackstop,
	ObiSkinConstraintsBatch_t76781F431D4AE71F3974CDB205A443851B35A906_CustomAttributesCacheGenerator_skinCompliance,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_orientationIndices,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restLengths,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_restOrientations,
	ObiStretchShearConstraintsBatch_t63A252535CCD53AE7C63D1507283C1628C06BB8B_CustomAttributesCacheGenerator_stiffnesses,
	ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_maxLengthsScales,
	ObiTetherConstraintsBatch_tB72B2385E36323C87F8EA5CC41CEF9D5A3BD826B_CustomAttributesCacheGenerator_stiffnesses,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_firstTriangle,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_numTriangles,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_restVolumes,
	ObiVolumeConstraintsBatch_t3B56FB6B9F608D7CEDF62265ECDEE87B417787C5_CustomAttributesCacheGenerator_pressureStiffness,
	ObiConstraints_1_t814AA25938B7022E214F5C54272335BCECBACF49_CustomAttributesCacheGenerator_batches,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_OnBlueprintGenerate,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_Empty,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_ActiveParticleCount,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_m_InitialActiveParticleCount,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator__bounds,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_positions,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restPositions,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_orientations,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_restOrientations,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_velocities,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_angularVelocities,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invMasses,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_invRotationalMasses,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_filters,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_principalRadii,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_colors,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_points,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_edges,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_triangles,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_distanceConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_skinConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_tetherConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_stretchShearConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_bendTwistConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_shapeMatchingConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_aerodynamicConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_chainConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_volumeConstraintsData,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_groups,
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_SourceCollider,
	ObiCollider_tA297948C5F4777ED9C5A492EFF6BBF7751D2C7B9_CustomAttributesCacheGenerator_m_DistanceField,
	ObiCollider2D_t5C38F1F5146454644E07250E0307C330A1CB4E31_CustomAttributesCacheGenerator_sourceCollider,
	ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_thickness,
	ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_material,
	ObiColliderBase_t5C855805B716B0D19A72AAE329D59D122123BDB4_CustomAttributesCacheGenerator_filter,
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingContacts,
	ObiCollisionMaterial_t3DE1A1F77269D1DA18A0EA042D292F365F715C9F_CustomAttributesCacheGenerator_rollingFriction,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_input,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_minNodeSize,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_bounds,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_nodes,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxError,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_maxDepth,
	ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_m_AlignBytes,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_voxels,
	ObiDistanceFieldRenderer_tF6263B78F384FE44C40CF3D3AAB28EF4C6802E49_CustomAttributesCacheGenerator_slice,
	ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator_U3CParticleMaterialU3Ek__BackingField,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnCollision,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnParticleCollision,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnUpdateParameters,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareFrame,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnPrepareStep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnBeginStep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnSubstep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnEndStep,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_OnInterpolate,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_simulateWhenInvisible,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_Backend,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldLinearInertiaScale,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_worldAngularInertiaScale,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_actors,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_m_ParticleToActor,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyActiveParticles,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtySimplices,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_dirtyConstraints,
	ObiFixedUpdater_t4D2492FAB85AE92AD8329DC2DA70CD416EC235D1_CustomAttributesCacheGenerator_substeps,
	ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_substeps,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_deltaSmoothing,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_smoothDelta,
	ObiLateUpdater_t17130DC6150FE77EF408BB5214887E405AE11AF7_CustomAttributesCacheGenerator_substeps,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_U3CPropertyNameU3Ek__BackingField,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CMethodNameU3Ek__BackingField,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_U3CNegateU3Ek__BackingField,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Actor,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Target,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ParticleGroup,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_AttachmentType,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_ConstrainOrientation,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_Compliance,
	ObiParticleAttachment_tAB0FF7F2B3AA7A685A63B55FCB18A0C310F7F8E3_CustomAttributesCacheGenerator_m_BreakThreshold,
	ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_skin,
	ObiProfiler_t51A07285AB21B6AE6439CA048E39FB322B0A86B8_CustomAttributesCacheGenerator_showPercentages,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stitches,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor1,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_actor2,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_particleIndices,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_stiffnesses,
	ObiStitcher_t360060541556DE8F17403D17A883FB875D7CB242_CustomAttributesCacheGenerator_lambdas,
	ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_ObiTearableCloth_add_OnClothTorn_m6224CDA7378B8DB12B0333F7FED7B7227E6E3694,
	ObiTearableCloth_tD585779EC18B27A766BEE146CE6A21FF632D2BD0_CustomAttributesCacheGenerator_ObiTearableCloth_remove_OnClothTorn_mB5CE17825525987AFCBD938933DC1248DD4D39E9,
	ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_Initialize_mB541B50B4B6C0D58EDE43368C99D89F1690B4AF7,
	ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateDistanceConstraints_m1570C2E0E2E30FF955FAED78EB8DF6C12ED36B65,
	ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateAerodynamicConstraints_mB62C6FAE788BF62F4C9B9D2D437136E90995A256,
	ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateBendingConstraints_m94A0CAEBCCD4E334449EA330C0D77D22333B0B31,
	ObiClothBlueprint_tB5434C1DEFC5DF5592CAEE04BC8791BB789E3C89_CustomAttributesCacheGenerator_ObiClothBlueprint_CreateVolumeConstraints_m61301FCFCDA8C8B2F6B27808ED4DBEB2F4EE5540,
	U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2__ctor_mD89C5AE9C9C80916FB8FFD79CFC095D19AE5D57D,
	U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_IDisposable_Dispose_m7004C46EC7D295E0968AF94ABE4F7E8BF273593F,
	U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E02322F5A0D23311828213F94F9CAE0E0750934,
	U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mFB62B5B536F9DCCFFA18FAD799255FBB349E0CB2,
	U3CInitializeU3Ed__2_t30C73CA341645043DE721F31BF4C53C9EC27C3F5_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m74CF369892ADB61833B9A3B086E1C2EC78434A01,
	U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3__ctor_m09BD3C57322A5EF62A54F6731CDF444FA9BB2E09,
	U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_mDAD5189CF5BE3E2386462303C5D8363F9695B6E9,
	U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D1E0E87E7FD71C4EC798103826D6140BFD871F7,
	U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_mC8A761BFD80CE17A1F65517143C99E6A20387CDB,
	U3CCreateDistanceConstraintsU3Ed__3_t4C609F0B169E5A6DAB7B687E2F87B25A03F40B3B_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8896FC2746D379EEBF8E8501E3C010284F085BC2,
	U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4__ctor_m4FC51372D57AB411957AE946A83F3EFDC329A358,
	U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_IDisposable_Dispose_m75085E8EEE584CA48781239A3A035272ED0D28E4,
	U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mADC37B2982CCFCDA50AB1C551B944F7022C3FF0F,
	U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_mB2CC4104E9C7AB68C42D91F0D25AAD0316D92347,
	U3CCreateAerodynamicConstraintsU3Ed__4_tFFCF443667F978845F76CBBA3F495E3E37F4B764_CustomAttributesCacheGenerator_U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m993A90ABEEA80806AA028F49D6215A00BD8CAB0C,
	U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5__ctor_m216136A55AF138945D5561DC181CE943371035F6,
	U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_IDisposable_Dispose_m6821B7A23670BE0F2DE199C41EE831168CA242AD,
	U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE75E1D32AFBD9B07503268FC3E0BA64851456C6,
	U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m731DAB5B414FCB6F5253FAE15495E32006F1400E,
	U3CCreateBendingConstraintsU3Ed__5_tDF3951054137522643E03569B165A38B5B79D517_CustomAttributesCacheGenerator_U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_m5767B962F68DA952AABEA5D0B178933A58E445E5,
	U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6__ctor_m88D1946A7A914EC0CC61A5D2BAB66310AB4C807C,
	U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_IDisposable_Dispose_m051782DF73D0AA384FAEB7DC1642C3598EC916AE,
	U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m243C532028DFB65774C84E5FDFA0528EC46DE0B1,
	U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_mED3CB28B77CD40AE79F199DE53151239982A3BBD,
	U3CCreateVolumeConstraintsU3Ed__6_t23270BC9BAD28084743D6871239B1ADD982D4DA3_CustomAttributesCacheGenerator_U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_m49682C0DC4ADBD851E1ED3B1631283CBC211B9E4,
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_ObiClothBlueprintBase_GenerateDeformableTriangles_m412F7EDE785459A5F256F70B1BCC07A6DE86E97A,
	ObiClothBlueprintBase_tA69B695BC8FBFDE21485D295359B0144B1DD97FD_CustomAttributesCacheGenerator_ObiClothBlueprintBase_CreateSimplices_m792C9C38FCE60CB1F28D70F757E38C50C8B381DD,
	U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8__ctor_m0B4BEE607BAA1A2C0EBFEC240D52F91801C0B17C,
	U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_IDisposable_Dispose_m9FACA3940826A6F604CE1ED095A420F3B70BEF31,
	U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94D6F01A98AAF5D8ACB13EB4E5C66B583F950A17,
	U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_Reset_mFF432431540D581589CE78CB502104BA515F44A5,
	U3CGenerateDeformableTrianglesU3Ed__8_tB048B8D11EC7A0FCA29FF72275572D1C0DBD564B_CustomAttributesCacheGenerator_U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_get_Current_mCAC139C7F6132507DA4DD623B3F44AB50ABED2F6,
	U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9__ctor_m6BDB71B25348530DD662CD094363B8BD7290B3F8,
	U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_IDisposable_Dispose_m9D9E3AF192D6A21176BC771CD6B070A775A15627,
	U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF2A1B6D8EB60FC887ACEA5CA1A98501EFC8DBA8,
	U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_Reset_m168698F93B24A0D1D8BF0D33260667A9AE613315,
	U3CCreateSimplicesU3Ed__9_tBBC43C9B17C28208395D1B8CA366FE788B8544C7_CustomAttributesCacheGenerator_U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_get_Current_mA5534B33C95835ED9EE2DD141CC15F742A981072,
	ObiSkinnedClothBlueprint_t2303B8E824B4492591ADD3E67FD1E78E5C56F197_CustomAttributesCacheGenerator_ObiSkinnedClothBlueprint_Initialize_mEEEBD3D9C13B8A98A3761D7E712512A3CCC377B5,
	ObiSkinnedClothBlueprint_t2303B8E824B4492591ADD3E67FD1E78E5C56F197_CustomAttributesCacheGenerator_ObiSkinnedClothBlueprint_CreateSkinConstraints_m58C14E2205A69B9015B3F01ADF03CB072338C3A7,
	U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2__ctor_mBE818931D3B01A168B9747C7573E4B406C0EB303,
	U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_IDisposable_Dispose_m0BDE2AC7C8E038318E8B83BAC143BD3363720DEC,
	U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB92A210007613B2909D214DF7B445C938F4C6EF,
	U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_mC9AF6B8BE46FC43D8A1334E15A189A796056CBE6,
	U3CInitializeU3Ed__2_t95D784E8877A7E5683B4B4361E6AACF2D304650F_CustomAttributesCacheGenerator_U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m22FAD500DB712F880583DA421A4A69DE13357712,
	U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3__ctor_mD52C4DA6057FEE9E99D0D32779C65C6EF756FA28,
	U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_IDisposable_Dispose_m2188832D0686304E2FA1C1E3CBC9244B11DB4EA2,
	U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE5966D180DA31A5B8CC609EA3CC94FD482431528,
	U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m3423E9E200FEB13FEF7BEC122845FB66403E2A30,
	U3CCreateSkinConstraintsU3Ed__3_t953B79483681761EACF26BA2C08961268535142A_CustomAttributesCacheGenerator_U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m8C97E9A1F24B2A98644A9E03FD1DE0A49AB21A05,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_Initialize_mEE76304F3987B091FC3F24395B52A9776C85B7B6,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_CreateDistanceConstraints_mFD063AE5F8C647760AC90B516949FF31D0238EB5,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_CreateInitialDistanceConstraints_mA1AD7123377815A2D7DCADC9CEFE1C7E450B718D,
	ObiTearableClothBlueprint_t934D75203856F5DE09B16F4BC5B93BDF4DB648BB_CustomAttributesCacheGenerator_ObiTearableClothBlueprint_CreatePooledDistanceConstraints_mC50382B6EF8301BB863CB65BBF73FC20FD8B83BC,
	U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8__ctor_mE1FD39996C2F57CF4C0B2C10C7CB001423DE2381,
	U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_IDisposable_Dispose_m6A03B2E04499DB25B9E769956B700DB301ECA55A,
	U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D1131F06EECDDE2B78AD00FB36522B66AD3538C,
	U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_mA6A316D6F121F16843F327E134ABB5CC4E804BD0,
	U3CInitializeU3Ed__8_tF940FBEA926F04ACE25D63F62A36BD331D5C4627_CustomAttributesCacheGenerator_U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m6ADA7C8F48F23CC6603E2ECDDF2999F8B85CF87F,
	U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9__ctor_m225975D257A624A724A08C3E1FAF8C2DF4654668,
	U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_IDisposable_Dispose_mF9F663E01209D2ADCFC88D6EB3697151496279D4,
	U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC86A1722C558C6E0F887CE3CE9B2CDFA9D75BE,
	U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_Reset_mDA4DBAC64C3B419B5FD761D86BEB3A05984873E7,
	U3CCreateDistanceConstraintsU3Ed__9_tBFE0C6DBC052AAE29C9426BA3AF2DBA8B14E953A_CustomAttributesCacheGenerator_U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_get_Current_mA37DF615EFA87C9F148D307AD7FC73526A9207CE,
	U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10__ctor_mB3B7E5310F4928BB37C9512E9F40ACFC3F140C9F,
	U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_IDisposable_Dispose_m39D16A16F80402BC68F47E0A9F83F7BC9CDFE417,
	U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF3452EB8DE1B9490F900218D00937A623C33312,
	U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_Reset_mC4356FC9C856276AFA630708CF08D9DB17A3E189,
	U3CCreateInitialDistanceConstraintsU3Ed__10_t20ECC7AA1EA951DBB7CE0FCB9601AC26AFAA7F95_CustomAttributesCacheGenerator_U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_get_Current_mE9D3BB3B7750F054BA802A619B79F0A538D1D6FE,
	U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11__ctor_m3C790EFEB6A41148BFA287BBF9EE4C00327BEB64,
	U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_IDisposable_Dispose_m5B3AC37A9F1F1BAED5319C961D4121B7B17D08A0,
	U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB7BCE5ABEDC2654A1A0453DE030EF2ED8CDC3FC,
	U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_Reset_m824F230E26816CB43250CD6BA6C491C34058CD38,
	U3CCreatePooledDistanceConstraintsU3Ed__11_t7DE80E02DE198C161898C68706E888F084DCCEF5_CustomAttributesCacheGenerator_U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_get_Current_m684F6B83E2834AA135F20041DDCF51BFE4ABB3A0,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_HalfEdgeMesh_GetNeighbourVerticesEnumerator_m5D851186C1DD0DDF3C79F394DC9946C6664AE4E2,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_HalfEdgeMesh_GetNeighbourEdgesEnumerator_m46006FB9A047B16085C6EF70CC20209053622818,
	HalfEdgeMesh_t474A8CE3C1336553539113ABA0A17AE4DFC09AE7_CustomAttributesCacheGenerator_HalfEdgeMesh_GetNeighbourFacesEnumerator_m055B1E4B59B1C20B1C6F542EE88DA13DBE7DF26D,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31__ctor_mF8ED85D8453E3677145D2F9202A2F2CBFC6929E9,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_IDisposable_Dispose_mF5E674694F95EB02F8B41CC0A6A0AF7D3C1276DE,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_VertexU3E_get_Current_mB8974C8C5F9B458C49BBBD99546E03A15E277212,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_Reset_mF9C32E78CBDC8991CB3AA3E8517D2434CB4060C6,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_get_Current_mE029EC97DE5EBDB2302D6C9C4E042A8D402111CB,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_VertexU3E_GetEnumerator_mAF607573C51602451E445A9210B356A40C744B6E,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_t00C5DCF3186A2830EFF015847DDCEFB5D3086DA3_CustomAttributesCacheGenerator_U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerable_GetEnumerator_m3F71BE162D53C1A40174A26D4BEB7864B8D1213B,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32__ctor_mD46F103C7C9B4F9B8A0AD31F7AD7D11C248D9E45,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_IDisposable_Dispose_m2DCA4922446B3F5F983E88E30DE79278DEDE5613,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_HalfEdgeU3E_get_Current_mCD5275CC511E91FCA17A93FC067DD0201E6C87E7,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_Reset_mE09272AC7F4A5074F95EC162B00DA8152F551EFA,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_get_Current_mD8A977C5E95E52F283E5BB7BCAB03DB08B4363BA,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_HalfEdgeU3E_GetEnumerator_mC4B54D7954EA17442D6D38B91B6E903FB1C54C3B,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_tEAA7BA418F9B8A1BC12BE96C11922075A88BAF0D_CustomAttributesCacheGenerator_U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerable_GetEnumerator_m2F65C8046D5AC6277F7723BF04D7AEF6A27B507E,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33__ctor_m4C8DC2E13D54FFEEB86EF6AE20DEEA7A1E918A6D,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_IDisposable_Dispose_m972ADD603D342F7AD8F1317F6B20179617A042DF,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_FaceU3E_get_Current_m11DEA6ABA872F8CBB0E39CAA35823EE6B5E5C15A,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_Reset_m9E664CABCDA715691724536236FAEBBB657DD313,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_get_Current_mBBBADC2F55A8568A51101245F7598E952B837304,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_FaceU3E_GetEnumerator_mF45B315F345EC2F18D84B7400ECD0FC83B7BE26F,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_tD6D7A07B1D44AF3E18FD702F36BDD6DB62FA85D1_CustomAttributesCacheGenerator_U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m66EB03D0F227B3A19E153D78CC869BA345527948,
	ObiTriangleSkinMap_t1AE7DDC6A01CE36AF06A95B1E4BDC55EE6EA4C2F_CustomAttributesCacheGenerator_ObiTriangleSkinMap_Bind_m2DE9C31E8338896A2BFD8CCD2C47D0561FA663F9,
	U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31__ctor_mE5C6B10FCF9C0277DFC6D366EB0FD7BE84FB77BE,
	U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_IDisposable_Dispose_m0470FFE716CE5F6D58F2070F1980E5DEF012C319,
	U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m799C177A7037BA730C8BC7776C400B5065B64A16,
	U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m48142264DC52CB15FDD7A0C6BF9C6828E28C8E50,
	U3CBindU3Ed__31_tDF964D96C599667C72222A3E6EEEEF0AC1730C54_CustomAttributesCacheGenerator_U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_m937FC6C298B8CA31EA931CA6B7205A747ABFC996,
	ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_ObiClothRendererBase_add_OnRendererUpdated_mD0F78B5164967A7079E170FB1758688B3B7B9A53,
	ObiClothRendererBase_t4F366C4CEAA9954E4B55751E078F62D765E7843A_CustomAttributesCacheGenerator_ObiClothRendererBase_remove_OnRendererUpdated_m31F9D492473F2A469ECDDE8815A8A4ED54AB2FC2,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintLoaded_m04B2952178ABFA72F0A7E7219EB78A4ECEAE156E,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintLoaded_m998C17DB7FED249637AC6D40775D9D50ED1CA3F8,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBlueprintUnloaded_mB1182B14609EA8CCD5A9BD7C2F3CEDDAFF78C601,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBlueprintUnloaded_mB1AB8C2B322EB4633B0CD45EF5AAF7448BAD68BB,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareFrame_m3014BCA91C860098AA7A396A7FFCE7AB7CA1049E,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareFrame_m0D90685372167486999B646FC83D4E2C36C570AB,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnPrepareStep_mC586B960142DDDF81A5B1C87D393C7CA379E743E,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnPrepareStep_mCB8B15D01E8DA343CF2DD8E655932C886DD863CC,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnBeginStep_m7A401277ED988AF4C6388B1F768B4E613BA02E10,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnBeginStep_m80E4C1C9DE59FB6075BFB27E9B8D885ADC413C13,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnSubstep_m4E244BDD4F0DA8AFBEF75036632A65CF4EC1286F,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnSubstep_mABFE386CEC0194553AEE608F465F311B21360592,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnEndStep_mE226631AC3F2AA3BE52CCB4AB9972A0620E76208,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnEndStep_m18354446434FA0FE622CFA9F09B7D0D98B7365F9,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_add_OnInterpolate_mA39BB43FF56C2FF95981971698C30856DC25ED65,
	ObiActor_t6E90B91D3D3F2CCF7F017007481A7C3BC21980D6_CustomAttributesCacheGenerator_ObiActor_remove_OnInterpolate_mE2B57508C1595198B4E27E10DA174A0B3792AD8D,
	ObiActorSolverArgs_t652D2183A88C8F5DDF27C6EEE9B4023CC9CE3725_CustomAttributesCacheGenerator_ObiActorSolverArgs_get_solver_m2C65EFA043F915BB25BF5EE5F467EA1119C55BDF,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_project_m1A54A528F3FC9B0309892E868CF525734419BB49,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_toMatrix_mAA0372FB0B34939F42DA7D8C213E694BC0C7DF4D,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_asDiagonal_mED83A376E4FB3DD5D01304739CF1209A86E846AE,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_diagonal_m2495565B886EDB95594F3A6D3D3D04413E407984,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_frobeniusNorm_mD8B2D136717389B775B17E51B995F2EA8818F106,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_unitOrthogonal_m0CF287F50224232FE30E0E6C09BAB1F314F35F53,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_RemoveRangeBurst_mC15961CC1CC9308F567E98EEA38B52818C534CF7,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_add_OnBlueprintGenerate_mA116B88726BA07CDAC7EA40506772CC39F8FF86E,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_remove_OnBlueprintGenerate_m780AFE9B203E214D86769A37EADEE63D5FC9967E,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_GetConstraints_m1C48E3155094EC159203D4F23B25015FCA6F0597,
	ObiActorBlueprint_t4837DEDA2FA0EC941ABC499D1928A95C1707EADD_CustomAttributesCacheGenerator_ObiActorBlueprint_Generate_m14C8F36BC89354F15F0F82AE9546B5024F47FF7A,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50__ctor_m463D73E183DDE1A5C71E6E3CC821D0E7F30E347C,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m1C1B68AF4C49065E226E78E8D46FD68A406FC4DB,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m603F544C865B5A382E4D45071F9EA103E35296E2,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m8E40706068668910D9F7D8A1EACB85F8E6FFF77A,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mD7F6DAA1BCB1AB519151F38ACED86E62BB422730,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_mFFFD55BE544D6CB567D145A4DCB2564DD48B4CDF,
	U3CGetConstraintsU3Ed__50_t143C6C9D65DE970CCBDEC961E92F484415B5E3A3_CustomAttributesCacheGenerator_U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m3284B5FE565ECE74AF7F1FBE65BCD30C41CADA8D,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59__ctor_mD0933DCC8582C89719A5869AA40F1D96BC46A2A1,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_IDisposable_Dispose_m4C2610F3D57B4EB14EE56E2E116536B5A23C8B22,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7A53073B04207358D27C5F279097C3AECBDC48E,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_m326C85F4CC091D9CEBD26372E5491D58808FECB6,
	U3CGenerateU3Ed__59_t2785C43846639EF13C20A070A740F551961389ED_CustomAttributesCacheGenerator_U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_mB78D5553C5C65C889A8A4E2F6FCCADA45472D734,
	ObiDistanceField_t452F44BBABD3FDA852E230785414B372F0FDB482_CustomAttributesCacheGenerator_ObiDistanceField_Generate_m2FF083D72E253BE27D9DFD07C8816B449F778266,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16__ctor_m926CEA2198C2B31432AD7B0C629E0026FD9135A8,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_IDisposable_Dispose_mE8B29A18143A848E1C4C46A4D41CE2EE66999F73,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39320DBD9BADA847C3DC1B06385734720B3ED5D1,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m94171808CA1CAE76B9F0511FE6570131CD857EBB,
	U3CGenerateU3Ed__16_t69755B027FBECBB067D8FC064F6B218FAB39E260_CustomAttributesCacheGenerator_U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mE403FD60E16DACEE30BE06E73A882BBF823618FC,
	ASDF_tE13A467BED85E73DEF9E1A637B2471D28DFB9665_CustomAttributesCacheGenerator_ASDF_Build_m0B3E28D5C89253E2A2FAA9545A0B748B5C69E1D4,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3__ctor_m2AF6E46C3AF1DCE8EB0AC87345B0082720891BF3,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_IDisposable_Dispose_m407D5BEFA6CC050E62B83A69FC1C81FA11062FE4,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FD71D70EB820213EDF7BC3853561DF86E43D30D,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m026B7C1E35013639385A979664E9479DCFB7269A,
	U3CBuildU3Ed__3_t68130B53D11570086A3D1F89860DB1EBAB0365CB_CustomAttributesCacheGenerator_U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_m8730288CA28692A77A479C27FB99AFB9421E825B,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_mED2F171342075378B5B140C14A73A72EF8ACC45F,
	ObiNativeList_1_tE5D0060F94F665E39A7A06F645326ABEC008478D_CustomAttributesCacheGenerator_ObiNativeList_1_GetEnumerator_mAA681A9160D0F5ABB78834DDCE50A090F9027FFD,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47__ctor_m44C213C4523E721D89454E9E0DB188309DAA0C2C,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_IDisposable_Dispose_m3677A4910B2FBA94AB21EC467EA0F8D017654F32,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m577FD8C7518180375EAF528F1953D6A86B76129F,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_Reset_m59513FC87FBEC9A3F46BF8F1A27BE3F8C45CA05D,
	U3CGetEnumeratorU3Ed__47_t21767AA600F3FD95AF74A79226A5713000DA2129_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__47_System_Collections_IEnumerator_get_Current_m45C9E58B0197FD608861CCFE12CE9E08134F9D32,
	ObiList_1_tAFBF97086278598236664F1BB4032FED42C06E1E_CustomAttributesCacheGenerator_ObiList_1_GetEnumerator_m669DC89479304E618D4B41858753616F692A64B8,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2__ctor_m220A0BB8B4845E3E03C861BC45721DD12E9E7C7B,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_IDisposable_Dispose_m6C1E284C049482492472BDE417FEC2D9BC5F53E6,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m8D4794A9AC960B97A584FB17906AFA27C95E4C0B,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_Reset_mE6DC01E508903376308CBEEFDB0FD2C1990A7DEF,
	U3CGetEnumeratorU3Ed__2_t6B00DC0868EAC2A42D7B9CCE735D6956B06D8EB1_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__2_System_Collections_IEnumerator_get_Current_m34A4E8F5E7BB811C2830337581A6877BD404D22B,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_Voxelize_mFD415E5ED30A0BF0CBC964412DBBB37930CFA97A,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_FloodFill_m584C8081EF5F8BC2660348F75BF0C006FBD0EC51,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29__ctor_m8645BD0F9E12E3B4AE36A3A9D757A0AA317666E0,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_IDisposable_Dispose_m68F6D58872E569B35178F3F948D25088FEE404DD,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCADBFC708A9E44E40767E097632FC238158C8036,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_Reset_mF260247FB427D7B3C0DAAFFD3085943EBA202B9C,
	U3CVoxelizeU3Ed__29_t62A4802723006727CF7600C19BC6BB9842B8D9CE_CustomAttributesCacheGenerator_U3CVoxelizeU3Ed__29_System_Collections_IEnumerator_get_Current_m4ED17979436917051AE4058242D7AF43D4A9F2C7,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31__ctor_m70BCBE21D4D47E1482D966F7C9D35F7510603B97,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_IDisposable_Dispose_m874A33578734166DA64BFC7AE8771A031A25F2C2,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m60C01825AF1A7118B99F44611D5A9FFA941E6189,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_Reset_mC4010923219A1BAD954EEAC4B73CDD945265C9A3,
	U3CFloodFillU3Ed__31_t9ECE2A49D4A7B45A5F89E01C3E46C852C1710CA6_CustomAttributesCacheGenerator_U3CFloodFillU3Ed__31_System_Collections_IEnumerator_get_Current_m7964014EF80DB8139871A160F4672E40BED0788E,
	PriorityQueue_1_tDBFAA8299A447FDE7A02B777CB2142412A0808C9_CustomAttributesCacheGenerator_PriorityQueue_1_GetEnumerator_mE59F3123BC2AC2349A2019082A77861F7C5E19B9,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5__ctor_mE283398E42D28AE61C9072FB8E231BDCA9867986,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_IDisposable_Dispose_m84D4F91D281E15BADD9CF82AE81CE1F0B9175998,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m9BA3FA8D6F2F77E738345A1A1EE37171EF68A96F,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_Reset_m01671BA954E40111F5CCF141F7B255B8F41519F8,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerator_get_Current_mA8032079CD99DE6D6B0A0C8AF31E894B4C7AADE3,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_mDD221B9EC48424B9BBA073FC887B08439C91DA49,
	U3CGetEnumeratorU3Ed__5_tB06A0B34A3BF839A49D50D43510A26A1B04D8245_CustomAttributesCacheGenerator_U3CGetEnumeratorU3Ed__5_System_Collections_IEnumerable_GetEnumerator_m050DABA4F326191228EDB111C0B34B0E6DE60B0C,
	VoxelDistanceField_tE742F1E6A0F650C49DC028D54AD9703DF311BDCA_CustomAttributesCacheGenerator_VoxelDistanceField_JumpFlood_m8A165DB8B897C98F4DD564943140A9C71408E1F6,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5__ctor_m9A42F34DE8C7C452C8A53F17D6D572BC2453C654,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_IDisposable_Dispose_m0A919D1AE0822BB1475E5FE78DA4060A236911E6,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF414A3DC08D65DC419082B4635288E1F7E02FC0,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_Reset_mDD6E089F2CD7CCEE8C562ED651C75C2D320D9B22,
	U3CJumpFloodU3Ed__5_t940B7567E710C8F62CC388731C5BB19C38CC2401_CustomAttributesCacheGenerator_U3CJumpFloodU3Ed__5_System_Collections_IEnumerator_get_Current_m0850F7B63FF4B6EB05FB8A60525AB3BC48509D18,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_U3CFindClosestNonEmptyVoxelU3Eb__6_0_mBBEC0743F2CB9A04F014A64C652D3572607A4FDC,
	ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator_ObiParticleRenderer_get_ParticleMaterial_mACE9EAEA565D0D3F7A7BF6C53B8C04111AAFB530,
	ObiParticleRenderer_tF781C9AA9C544382CE049D22AD3B45EBA7A30AFB_CustomAttributesCacheGenerator_ObiParticleRenderer_set_ParticleMaterial_m271A889A40C59DDA14EF2DCA96DD1B93E40CC3F9,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnCollision_mEB324EABAE3F36C43F055950F52827839851DAF5,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnCollision_mA07AA8AA273F50BC4DDAFD0523A7CA23C628D674,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnParticleCollision_m73A739A0544F098FE805CEF2C6ED3E9528060A52,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnParticleCollision_m153C65501BB104D19DFB0BC50CE655A20BB725B7,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnUpdateParameters_mDCE3C3436FCB6E239FC3C229799E1CA65048798B,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnUpdateParameters_m1D7467B121B4968CCB10AE69A8FCE3F601A622E1,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareFrame_mF8245DFAA6E404FFD6135985C1B90BAD5A827B69,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareFrame_mFF9D302946D3647FF7267652E21CCDA784AC68E6,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnPrepareStep_m22DB6F7A692B42DCB0AF33EDDFAF38062A4AB42E,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnPrepareStep_m5D2D4E8CACADB7938868BE6F29546BA497A05250,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnBeginStep_mE11B9F3927E6908AC1C8CA09888FAC4145B60F72,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnBeginStep_m828F85A9556BCF73D5434B9A689918DEF5F5D39D,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnSubstep_mF4D4FE1ED9E867E6F111B8B082E377D950607C6E,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnSubstep_m60FD7FE897C29FEF9EE0EDC9303C62CFF70D2B33,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnEndStep_m85BFAD019C29970E500BB5079DA70527A4EB324D,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnEndStep_m53846A7D3E4D2113D49EAA4EE56FAE9752CB6CA3,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_add_OnInterpolate_mBADEB5D974C090F97884D9FF885C017D0C36CF7D,
	ObiSolver_t98772399C81F01F39B9922F3536D1AEB9AB2A634_CustomAttributesCacheGenerator_ObiSolver_remove_OnInterpolate_m5F9AAE96B45E17989172907EECA1966282A87626,
	ObiLateFixedUpdater_tF73C3F30FFB50BBB6B75B4A4A657FB6F11F20382_CustomAttributesCacheGenerator_ObiLateFixedUpdater_RunLateFixedUpdate_mA207589FB295229010BC3ED39CA0A33B465AE4AB,
	U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6__ctor_m3269F746C194951D4FFCB5CFE81A956F7C6913F4,
	U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_mFEF635C7933D3DD06A06668B01022B79D7348076,
	U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BC60167DF9644F2049A2591D4130F2190A2A4C9,
	U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_m55403139467BF8AF39A859E7823363583E55162B,
	U3CRunLateFixedUpdateU3Ed__6_t3F9B59B42285B85EDE462B66588BEA9437A8426A_CustomAttributesCacheGenerator_U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_m8E98B873A2E9B9BE3DEE0CA669677BA2ECDFE0FF,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_get_PropertyName_mDC8A03791EF5C0FC8DB9BEE2645D05EC456A9B21,
	SerializeProperty_t37386C27AE4E552093E2662C32F3FD3BC8FDFBB0_CustomAttributesCacheGenerator_SerializeProperty_set_PropertyName_m0D097B39C9F982C6DA9CFE395012ED31AE075A55,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_MethodName_mA9D547283CA5003031A6052EC2F01BDD20626C79,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_MethodName_mEEE7DE756E758BB291ECC2AF95FB9331577A89BD,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_get_Negate_m70429C0B2F6E15B7329A21127ED7F095B5B9F79C,
	VisibleIf_t930DD56C00906E5CF83B970D0D6B63A208D1F5B3_CustomAttributesCacheGenerator_VisibleIf_set_Negate_m7AE9FD0E091984A71A593C3FE0A0A4CB89CDDF7E,
	CoroutineJob_t80DB8BDF56DB40B9329335C29878C9D46D8C879F_CustomAttributesCacheGenerator_CoroutineJob_Start_m80C8E17CD749B13B237150583A6280C213F8C39F,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15__ctor_mD7A6880AAEB86FECE339EAA24C276AA5D91FE5AD,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_IDisposable_Dispose_m4542E430FBE633F30A5F1B001DA8C7D01BE636F1,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m857DB02265865988D8A060D28C87EAB01F6DEC55,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_mC889240009E6BB1F36B3BC74F618A35A604608B3,
	U3CStartU3Ed__15_tAFC1372DC21B7835B32F721BE967AD5FB10B8337_CustomAttributesCacheGenerator_U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mE937748101E5BB647A3762B97A9DE2FED38D8283,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_mF56D360C3B427C99DB48F907D7B8F1F2D96B2474,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Swap_m4545B5452717048909109CF73C8CAC6880F7F227,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftLeft_m983E32714E08933EE70C58FD314AB2F11C10DB5D,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ShiftRight_m0CCEF49D7BEEB3D21A740CEEF10750BFA3E10ED5,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_AreValid_m36409B4F74E4DA83A742954433F6CA4EE168BAE6,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Transform_m733A8AC89C2BFBC7A0E0089512FFC4709454DE88,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Remap_m8F506FF64E3B9168DE2B0235E274CC1033B7D86F,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_Add_mC0C822C7DBBF1BA01EE882208292D519B2C9C2D4,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_FrobeniusNorm_m864D4DA65931429E717193CB634FFAAF31C5DCF4,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_ScalarMultiply_mE25A02716B8B63129015ACA756D1DB1AD469DE51,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_PureSign_m92E94D198EF88DC390CCBA619718D6A6C27BA5C5,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BilateralInterleaved_m17A79FDAFD53A3C8E7CB5403AC539CD1BC4D5453,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_unitOrthogonal_m08E2004A970D966D5227E325780C23E6FAB28529,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40__ctor_mC32C3AE2B262CFFB543609235547205303EDDF10,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_IDisposable_Dispose_m8831F3EF2CA071AF13BD9D630BBA7F87306BD89C,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C9E1918A629F5BD2E500DBD47B57BEA70DA7126,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_Reset_mD1B814902C5CB8F328BB1E173AE4B4666C5B7CFE,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerator_get_Current_m82E938AAC7D70CD2629E214DDAC1F71A6BB51474,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_m7BD8049FEA64E988546AE58FB75F23FCF48D2279,
	U3CBilateralInterleavedU3Ed__40_tC516EF55E31EDA110B72CF25ED78201DCD094DEB_CustomAttributesCacheGenerator_U3CBilateralInterleavedU3Ed__40_System_Collections_IEnumerable_GetEnumerator_mBF01BD41B11190F2A9C48BE6A1A9FB04EF151CF6,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_NearestPointOnTri_m15F131F6314B94E56B30D27DC3FFFBE574379AF6____tri0,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____A0,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____B1,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____C2,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords_m84388BF2D91D24649DD2AD3BB854711984DE1D73____P3,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A____A0,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A____B1,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryCoords2_m398F6691D7CBDA81EFF08DA0FE62E0FCCF49227A____P2,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____p10,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____p21,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____p32,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_m754F52E2925727C43DAA1D0D0AAE7A046B538DA9____coords3,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740____p10,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740____p21,
	BurstMath_t4F22F8A47979A19EF1CF3CE9E4B1F665D00FC4D1_CustomAttributesCacheGenerator_BurstMath_BaryIntrpl_mE844FF70942E66C473B214277C71ACC128C06740____coords2,
	BurstBox_tA70C7C054A23857D4B17C3FEED3CA25136446326_CustomAttributesCacheGenerator_BurstBox_Contacts_m9D3DED1FA7E7454E9EB7A52A7ED94B8A08C99CE3____simplexBounds8,
	BurstBox_tA70C7C054A23857D4B17C3FEED3CA25136446326_CustomAttributesCacheGenerator_BurstBox_Obi_IBurstCollider_Contacts_m97856F2BFA4EBA5278F111B41C7F5CA5A8C58573____simplexBounds8,
	BurstCapsule_t56BDF841EA369F525D125BD53FC21AD5CD05D4B9_CustomAttributesCacheGenerator_BurstCapsule_Contacts_mE461E9B2E609E1A411ED668A5C6380B29FCCE4D6____simplexBounds8,
	BurstCapsule_t56BDF841EA369F525D125BD53FC21AD5CD05D4B9_CustomAttributesCacheGenerator_BurstCapsule_Obi_IBurstCollider_Contacts_mADF596860548ABE29238ADEA0F82324B6D1A6A28____simplexBounds8,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E____shape0,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E____colliderToSolver1,
	GenerateContactsJob_t4BE644069C76998EB041A1B32F40C931F0AB33BA_CustomAttributesCacheGenerator_GenerateContactsJob_GenerateContacts_mF0E81A6B91416DEA58C2DFFC98F2588F2A1F3C2E____simplexBoundsSS7,
	BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_Contacts_mC98735809C55B8C503AC05F45BEC80420352057E____simplexBounds8,
	BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_DFTraverse_m21F88015BB4FB3D50DD96EC893D5A10CD6827850____header2,
	BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_DFTraverse_m21F88015BB4FB3D50DD96EC893D5A10CD6827850____dfNodes3,
	BurstDistanceField_t7A4DDED073253DF8C8A5D7279674F382E563DBDC_CustomAttributesCacheGenerator_BurstDistanceField_Obi_IBurstCollider_Contacts_m212E14857D066BE00574BE85A69F636A0B5A14BF____simplexBounds8,
	BurstEdgeMesh_t8E4721F9266DDAE6EB02C72B3CF21134A98FB906_CustomAttributesCacheGenerator_BurstEdgeMesh_Contacts_mDEAD5DA052578CD2794227155F0CFDEFB76DD9BF____simplexBounds8,
	BurstEdgeMesh_t8E4721F9266DDAE6EB02C72B3CF21134A98FB906_CustomAttributesCacheGenerator_BurstEdgeMesh_BIHTraverse_m8E806DD0BD9B370CC1862099121BC5B3F83B9CA9____simplexBounds8,
	BurstEdgeMesh_t8E4721F9266DDAE6EB02C72B3CF21134A98FB906_CustomAttributesCacheGenerator_BurstEdgeMesh_Obi_IBurstCollider_Contacts_mE8D91763243332F3BBE8D636493C232F3085C1C1____simplexBounds8,
	BurstHeightField_t2FC714F65F4A13D8E0EC730BA386EE5E5A11473F_CustomAttributesCacheGenerator_BurstHeightField_Contacts_m8D519E8FEE86E9D62A2F24AFE65643D75C82E321____simplexBounds8,
	BurstHeightField_t2FC714F65F4A13D8E0EC730BA386EE5E5A11473F_CustomAttributesCacheGenerator_BurstHeightField_Obi_IBurstCollider_Contacts_mAF9CC53C9637FA7BEFAACAB317DEECC4325EFEA8____simplexBounds8,
	BurstSphere_tCEDF9209B41A98705F09710ACC8B746A32DD70F6_CustomAttributesCacheGenerator_BurstSphere_Contacts_mDA37B8F9954ECD2EFD7BF8F9A51A43CA73A869AE____simplexBounds8,
	BurstSphere_tCEDF9209B41A98705F09710ACC8B746A32DD70F6_CustomAttributesCacheGenerator_BurstSphere_Obi_IBurstCollider_Contacts_m16229652DCB867F59A4F4360F064FDA00691D32B____simplexBounds8,
	BurstTriangleMesh_tD45990F38578999C59B6B3B6E613882E6C303041_CustomAttributesCacheGenerator_BurstTriangleMesh_Contacts_m03A033B4760F0729757BF20D322FC8D112802A00____simplexBounds8,
	BurstTriangleMesh_tD45990F38578999C59B6B3B6E613882E6C303041_CustomAttributesCacheGenerator_BurstTriangleMesh_BIHTraverse_m53B6E0B2D8D09B465000034E751D2BB96E194907____simplexBounds11,
	BurstTriangleMesh_tD45990F38578999C59B6B3B6E613882E6C303041_CustomAttributesCacheGenerator_BurstTriangleMesh_Obi_IBurstCollider_Contacts_m0414B63AB8F72DB613D9240E6B5AA847FB4B296D____simplexBounds8,
	IBurstCollider_t81B6D92E3DDAC7367074ABC3CA6413E83F2CDD9B_CustomAttributesCacheGenerator_IBurstCollider_Contacts_mA9084E4B6ABCD88E1208A6E335884F0877424B0B____simplexBounds8,
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_EncapsulateBounds_m9AFA483BA1B7BEBF77C1B85D57549FB36375E1D7____bounds0,
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transform_m972887B0B52D4B08B4806F6BB8196E17B64C0B90____transform0,
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transform_m20348C56E245E750A6399FA6F7C2D767EE11B91C____transform0,
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transformed_m09422ADB914E6917056FB53FEDDC172E7AEE88C1____transform0,
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_Transformed_mE44D84A994DBAAD9EED74F026F8220ADCD9A249E____transform0,
	BurstAabb_tD751164BD90F8B843431A5A3FC7EFBA9AF42DC87_CustomAttributesCacheGenerator_BurstAabb_IntersectsAabb_m06B35FE8630AA2B7A1DB26D4CE1B4676050E46C6____bounds0,
	BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C_CustomAttributesCacheGenerator_BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524____rigidbody0,
	BurstContact_tC345ACF1FC4B18DA38F179DD17B23F3694AFC83C_CustomAttributesCacheGenerator_BurstContact_CalculateContactMassesB_m80832C34053A2E4B2024099933EFC198CBB3A524____solver2World1,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_SpatialQueryJob_CalculateShapeAABB_mD4BB4AAFB5B7C57689D4C530F27046BAEEBBF9D2____shape0,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8____shape0,
	SpatialQueryJob_tC0F86C6FF19E02088FA09160B8E58A77F3B5D4D4_CustomAttributesCacheGenerator_SpatialQueryJob_Query_mBCFE65F80130437E9773CE6E43F75CD71E7E7CE8____shapeToSolver1,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____node3,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_m23810E4AED9E58F613C341BC0C9B6D7FECF9D905____point4,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mAE13B1EFAA791602268DFF2A5ED429691EA76178____point4,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____node4,
	BIH_t0060B761219DD4C891793F0457F4CFF350C0807D_CustomAttributesCacheGenerator_BIH_DistanceToSurface_mCDD4F03C3B60D8D084F6EDB334C195DE89C58B05____point5,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetVoxelCenter_m3E452556E6139C67B397A378FDCBD9BA4F425089____coords0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v10,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v21,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetTriangleBounds_mBBA6B5E2F569C2ED7F5876BD84A27B825F559B06____v32,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_GetPointVoxel_mB53686BE0482AC0FE527505F8EC019E21CFAB6B1____point0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_VoxelExists_mBD68D5E79FB132F4304AAE5283DF58F65CDA0A4B____coords0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____bounds0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v11,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v22,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_AppendOverlappingVoxels_mF5C8FF58828D0D99F06F30DE8B995443C901B2E7____v33,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_IsIntersecting_m1F9C81AE3D544C1F8AA465823F7DDA7A32C7FBEC____box0,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v00,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v11,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____v22,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____aabbExtents3,
	MeshVoxelizer_t67823B528FC7BD3C778E661C191F79A008B0D82D_CustomAttributesCacheGenerator_MeshVoxelizer_TriangleAabbSATTest_mF459B5290A8131FB91F831B7105CD2E7DFAFAE10____axis4,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_AStar_mA838A6AEC2878DE3B68FB035FCB31C5ADE9958EC____start0,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindClosestNonEmptyVoxel_m108974C29C2DE5DCB4E8092382271E90E1306714____start0,
	VoxelPathFinder_tCEA8295CF3C4F5CFA16C550CC6A8B76FE381EABF_CustomAttributesCacheGenerator_VoxelPathFinder_FindPath_m12E0CEA61718E42317477C9B7A3825B9EAA590E5____start0,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p10,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p21,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p32,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_NearestPointOnTri_m321A69E9A25CCB719C7215829C81D8D8CBF382ED____p3,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____A0,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____B1,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____C2,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricCoordinates_m3A02D87AC39EA5DB7EC627E892E9F1C3B8801197____P3,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p10,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p21,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____p32,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_BarycentricInterpolation_m3D92C8376763EA18C44BA152EB2C0E19EB7CD4A1____coords3,
	ObiUtils_tFD4A6BA5C584EB1C9F9E6EFA5131977D9335C550_CustomAttributesCacheGenerator_ObiUtils_GetPointCloudAnisotropy_m3D48980B4381A7B54F37C063FE47EF7C737CA9A2____hint_normal3,
	Obi_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
