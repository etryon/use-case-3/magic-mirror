﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void SimpleExample::Start()
extern void SimpleExample_Start_m095C1C3007F35B16A5B101B8BEAF4D3CE4165302 (void);
// 0x00000002 System.Void SimpleExample::.ctor()
extern void SimpleExample__ctor_m34DF183D0053400BADD5B81F25453AC415879D78 (void);
// 0x00000003 System.Void PlaceholdersExample::Start()
extern void PlaceholdersExample_Start_m06206F1262FE27A3553B9322D63E399DC06DB4FE (void);
// 0x00000004 System.Void PlaceholdersExample::.ctor()
extern void PlaceholdersExample__ctor_m09120A410A7AE704237E4EBB5C1A252419A1FE5B (void);
// 0x00000005 System.Void FadingExample::Start()
extern void FadingExample_Start_mA6BD0D92AB1033B0A733F781232555BA21B5E511 (void);
// 0x00000006 System.Void FadingExample::.ctor()
extern void FadingExample__ctor_mA44FD6768BFE69259FBF1AB82EBDBC6AC78C9527 (void);
// 0x00000007 System.Void CallbacksExample::Start()
extern void CallbacksExample_Start_mAF647686E7E912DE8FD6665B299A0E356877951C (void);
// 0x00000008 System.Void CallbacksExample::.ctor()
extern void CallbacksExample__ctor_mA55C1F1043BD03F79C682F7803454B78599CB4CC (void);
// 0x00000009 System.Void CallbacksExample::<Start>b__5_0()
extern void CallbacksExample_U3CStartU3Eb__5_0_mA8EA2B84DA44FCE5D20EDCD476540D9FE79D7E66 (void);
// 0x0000000A System.Void CallbacksExample::<Start>b__5_1(System.Int32)
extern void CallbacksExample_U3CStartU3Eb__5_1_mDAD7A8FE7ACACAB80270E91FFF0AFE80BA923822 (void);
// 0x0000000B System.Void CallbacksExample::<Start>b__5_2()
extern void CallbacksExample_U3CStartU3Eb__5_2_m1EA55EE03648F140001BB6C0C74318DB9191BCA6 (void);
// 0x0000000C System.Void CallbacksExample::<Start>b__5_3()
extern void CallbacksExample_U3CStartU3Eb__5_3_m3C58176A7C17724C2C8DC3C45206A46D663235F8 (void);
// 0x0000000D System.Void CallbacksExample::<Start>b__5_4(System.String)
extern void CallbacksExample_U3CStartU3Eb__5_4_m73A75D4D2C5FB9CE958AA0531FB9301B18BC6254 (void);
// 0x0000000E System.Void CallbacksExample/<>c::.cctor()
extern void U3CU3Ec__cctor_m28E151311BFE4B20F82AE6A27D4BF48C02E9854B (void);
// 0x0000000F System.Void CallbacksExample/<>c::.ctor()
extern void U3CU3Ec__ctor_m1E1050F7832E1CC337C5DB3E1EBD61617A022BDA (void);
// 0x00000010 System.Void CallbacksExample/<>c::<Start>b__5_5()
extern void U3CU3Ec_U3CStartU3Eb__5_5_m1672B2E8407D6CB7B606DF4A1B72041EC50B663C (void);
// 0x00000011 System.Void TextureExamlpe::Start()
extern void TextureExamlpe_Start_mC53A61BFA69D8A5A18CE61FF569039130C5FED53 (void);
// 0x00000012 System.Void TextureExamlpe::.ctor()
extern void TextureExamlpe__ctor_m654FA6145B22B204670127F3A3C7BD1AA855BB5D (void);
// 0x00000013 Davinci Davinci::get()
extern void Davinci_get_m4F17617DB7DF59BC86508BB1EC91CECE84BD899F (void);
// 0x00000014 Davinci Davinci::load(System.String)
extern void Davinci_load_m1855BC2886BF13307026820FBFA198DA1CD356D0 (void);
// 0x00000015 Davinci Davinci::setFadeTime(System.Single)
extern void Davinci_setFadeTime_m5040EFDA4781C5E8F38358595F4C868B37BFCCED (void);
// 0x00000016 Davinci Davinci::into(UnityEngine.UI.Image)
extern void Davinci_into_m54D250C70A9EE1631871D16D42FA901DB69CD037 (void);
// 0x00000017 Davinci Davinci::into(UnityEngine.Renderer)
extern void Davinci_into_mFF34CB96C053C0F7DFCEB90F0F02BE005DC72EE5 (void);
// 0x00000018 Davinci Davinci::into(UnityEngine.SpriteRenderer)
extern void Davinci_into_m0FF5BD988336147F9750086064B9EB2685B53BA8 (void);
// 0x00000019 Davinci Davinci::withStartAction(UnityEngine.Events.UnityAction)
extern void Davinci_withStartAction_mB2FEBD4785FDFD6018D883B5FF31F434CE64613A (void);
// 0x0000001A Davinci Davinci::withDownloadedAction(UnityEngine.Events.UnityAction)
extern void Davinci_withDownloadedAction_mFFE28047FE6DB6E2416D6FA54B21D9CCF4FB58BD (void);
// 0x0000001B Davinci Davinci::withDownloadProgressChangedAction(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void Davinci_withDownloadProgressChangedAction_m4F5ABF6E286A0672CBA4AA239FB8D716D78D9C06 (void);
// 0x0000001C Davinci Davinci::withLoadedAction(UnityEngine.Events.UnityAction)
extern void Davinci_withLoadedAction_m5AE2C36892EFAC76035654EBE6302C3618897BB7 (void);
// 0x0000001D Davinci Davinci::withErrorAction(UnityEngine.Events.UnityAction`1<System.String>)
extern void Davinci_withErrorAction_mB01E04C0D86B35617928D2BFB04C287F2B70AB8A (void);
// 0x0000001E Davinci Davinci::withEndAction(UnityEngine.Events.UnityAction)
extern void Davinci_withEndAction_m1297B51CB93030115FC44C728AE517C92CAB6BFD (void);
// 0x0000001F Davinci Davinci::setEnableLog(System.Boolean)
extern void Davinci_setEnableLog_m4A1CD38507EC33AA3486B591583F1C99B993A60F (void);
// 0x00000020 Davinci Davinci::setLoadingPlaceholder(UnityEngine.Texture2D)
extern void Davinci_setLoadingPlaceholder_mA3E1CAFDF7305AED0AD6CD0DFAFAD9D1C612E451 (void);
// 0x00000021 Davinci Davinci::setErrorPlaceholder(UnityEngine.Texture2D)
extern void Davinci_setErrorPlaceholder_mE3635F64EEA1E5957991484B500AFB3720D1340C (void);
// 0x00000022 Davinci Davinci::setCached(System.Boolean)
extern void Davinci_setCached_m230AD2E40E65EABFAF235951A3277376C27D1B90 (void);
// 0x00000023 System.Void Davinci::start()
extern void Davinci_start_mD79F1F0AC06ED947D37D3F79FE5D3AA2FF4E7139 (void);
// 0x00000024 System.Collections.IEnumerator Davinci::Downloader()
extern void Davinci_Downloader_m15650852A45FBDAF97CC882F463B75CE7647E772 (void);
// 0x00000025 System.Void Davinci::loadSpriteToImage()
extern void Davinci_loadSpriteToImage_mDF18A2121BFDC3EE5C258EE40614037ACF001168 (void);
// 0x00000026 System.Void Davinci::SetLoadingImage()
extern void Davinci_SetLoadingImage_mCF973D3BC2858C72D7A2C897DFC7D3F1FFD4C905 (void);
// 0x00000027 System.Collections.IEnumerator Davinci::ImageLoader(UnityEngine.Texture2D)
extern void Davinci_ImageLoader_m556EEE7C8C84B797F2FEE75F6140CC9EB92900DD (void);
// 0x00000028 System.String Davinci::CreateMD5(System.String)
extern void Davinci_CreateMD5_m5B60E0C8CE93A58FBE0EFD5CEB7D02986AAA6DEC (void);
// 0x00000029 System.Void Davinci::error(System.String)
extern void Davinci_error_m2B026CE7554656E9A34D7C6C63400D2DE740910B (void);
// 0x0000002A System.Void Davinci::finish()
extern void Davinci_finish_m962F2234E00E3216668D7001C91161930EE0BD59 (void);
// 0x0000002B System.Void Davinci::destroyer()
extern void Davinci_destroyer_m4D968D2923B8A98CF2D5FD16551FB44E74F0D622 (void);
// 0x0000002C System.Void Davinci::ClearCache(System.String)
extern void Davinci_ClearCache_m7AFE4493AD866F6C5F39A9B8C5BA87AF09D1E28C (void);
// 0x0000002D System.Void Davinci::ClearAllCachedFiles()
extern void Davinci_ClearAllCachedFiles_m6C5942ABE7FE281DF9CACC74908064E2A5DD97A1 (void);
// 0x0000002E System.Void Davinci::.ctor()
extern void Davinci__ctor_m6D6A5E4AA5577B2B6CB261467D744C8BCAFF4B18 (void);
// 0x0000002F System.Void Davinci::.cctor()
extern void Davinci__cctor_mCBF6FE1F73771438A9ABC324AFD709A37AD7DFFE (void);
// 0x00000030 System.Void Davinci::<start>b__37_0()
extern void Davinci_U3CstartU3Eb__37_0_m8B355C931283442F82EA487DF964E84991D80035 (void);
// 0x00000031 System.Void Davinci/<Downloader>d__38::.ctor(System.Int32)
extern void U3CDownloaderU3Ed__38__ctor_mC0933E4939833D0C437D034A5542BAB979F0D293 (void);
// 0x00000032 System.Void Davinci/<Downloader>d__38::System.IDisposable.Dispose()
extern void U3CDownloaderU3Ed__38_System_IDisposable_Dispose_m7BDA6A678D4C61D5C65745A335E01A40C9377321 (void);
// 0x00000033 System.Boolean Davinci/<Downloader>d__38::MoveNext()
extern void U3CDownloaderU3Ed__38_MoveNext_mFBA98E3B30FB60E1F0E822DA8CE6397B6A0A6658 (void);
// 0x00000034 System.Object Davinci/<Downloader>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloaderU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF5289C7B9DB78D33EA79C46839A9D83A623BBEA (void);
// 0x00000035 System.Void Davinci/<Downloader>d__38::System.Collections.IEnumerator.Reset()
extern void U3CDownloaderU3Ed__38_System_Collections_IEnumerator_Reset_mF1D52F18D10CB6C87FB5481732F1A1EFEFC988EF (void);
// 0x00000036 System.Object Davinci/<Downloader>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CDownloaderU3Ed__38_System_Collections_IEnumerator_get_Current_mC9A6BA27CA0A438B9DFEAA6658C83E8F659DE96E (void);
// 0x00000037 System.Void Davinci/<ImageLoader>d__41::.ctor(System.Int32)
extern void U3CImageLoaderU3Ed__41__ctor_mB990C3A44F2C25ED035517217354815A4702FF57 (void);
// 0x00000038 System.Void Davinci/<ImageLoader>d__41::System.IDisposable.Dispose()
extern void U3CImageLoaderU3Ed__41_System_IDisposable_Dispose_m4E0B05BF9B4A31543CE38260AAB6B24316A762C8 (void);
// 0x00000039 System.Boolean Davinci/<ImageLoader>d__41::MoveNext()
extern void U3CImageLoaderU3Ed__41_MoveNext_m253F0A9FA0F0A676D5FC59785B2266C4DE894301 (void);
// 0x0000003A System.Object Davinci/<ImageLoader>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageLoaderU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB39260EC0C134F7CD19D5DCF10ED803C9ACBCDCC (void);
// 0x0000003B System.Void Davinci/<ImageLoader>d__41::System.Collections.IEnumerator.Reset()
extern void U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_Reset_mA80C9BF77968EC1F27A49592D8F2FCDC749101CB (void);
// 0x0000003C System.Object Davinci/<ImageLoader>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_get_Current_mAB3334FDDC841DB00E54398A582A59001D2FAE97 (void);
// 0x0000003D System.Void CreateObiBP::Start()
extern void CreateObiBP_Start_mA9D0607E12C9870E26D798D69B3E4541F3E7E27C (void);
// 0x0000003E System.Collections.IEnumerator CreateObiBP::AttachOBI()
extern void CreateObiBP_AttachOBI_mE908C1B923E8EBC3C69393E6469B518FD34926EF (void);
// 0x0000003F UnityEngine.Texture2D CreateObiBP::LoadParamImg(System.String)
extern void CreateObiBP_LoadParamImg_m9E2016C240982AAF2AC338DD19EB04BC98742B31 (void);
// 0x00000040 UnityEngine.Texture2D CreateObiBP::duplicateTexture(UnityEngine.Texture2D)
extern void CreateObiBP_duplicateTexture_mB21ED54C6170AE667A2606A0A4B9EEEFDEEC1F7D (void);
// 0x00000041 Obi.ObiSkinnedClothBlueprint CreateObiBP::ReadObiBlueprint(System.String)
extern void CreateObiBP_ReadObiBlueprint_m441101C063382B0E71FF62AF66A42EEC7ABE5615 (void);
// 0x00000042 System.Collections.IEnumerator CreateObiBP::CreateOBI()
extern void CreateObiBP_CreateOBI_m0DFBBACA70566C2FC62BF6D5C649921563471BAE (void);
// 0x00000043 System.Void CreateObiBP::.ctor()
extern void CreateObiBP__ctor_mEDBF209C5AC411E024E792A4D805E142E4CE57E9 (void);
// 0x00000044 System.Void CreateObiBP/OnObiAdded::.ctor(System.Object,System.IntPtr)
extern void OnObiAdded__ctor_m68089FFA3218E04604FEE8EB3DB217EEAE7675EB (void);
// 0x00000045 System.Void CreateObiBP/OnObiAdded::Invoke()
extern void OnObiAdded_Invoke_m365B4AE77F874ED89FF7B17CAEEAC1C86F654724 (void);
// 0x00000046 System.IAsyncResult CreateObiBP/OnObiAdded::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnObiAdded_BeginInvoke_mA4087AF86CF308000D8B9E31F6155C3B6B02EEA0 (void);
// 0x00000047 System.Void CreateObiBP/OnObiAdded::EndInvoke(System.IAsyncResult)
extern void OnObiAdded_EndInvoke_m46CE6F2DE8C64F62DD2EEBF545BA6C59DA325E3F (void);
// 0x00000048 System.Void CreateObiBP/<AttachOBI>d__10::.ctor(System.Int32)
extern void U3CAttachOBIU3Ed__10__ctor_m430968C5EFD2BA0C9B96735FA7AC90263C382180 (void);
// 0x00000049 System.Void CreateObiBP/<AttachOBI>d__10::System.IDisposable.Dispose()
extern void U3CAttachOBIU3Ed__10_System_IDisposable_Dispose_mB2D097C019144FCC309F6414F7076DB3E6772006 (void);
// 0x0000004A System.Boolean CreateObiBP/<AttachOBI>d__10::MoveNext()
extern void U3CAttachOBIU3Ed__10_MoveNext_m59C23EF4B0FD0BFE71F5FD12F8F52BE6351FE553 (void);
// 0x0000004B System.Object CreateObiBP/<AttachOBI>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachOBIU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA42B13CC11372A044675C07BCDC32019A08A352E (void);
// 0x0000004C System.Void CreateObiBP/<AttachOBI>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_Reset_mC3C7E467B258179D51349B0F83385F1AEAECBFDE (void);
// 0x0000004D System.Object CreateObiBP/<AttachOBI>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_get_Current_m1F939691B3EADD74CDA34716DBA3FC4374AC9B15 (void);
// 0x0000004E System.Void CreateObiBP/<CreateOBI>d__14::.ctor(System.Int32)
extern void U3CCreateOBIU3Ed__14__ctor_m8DD675C517F191749940BE2ED1FE999EDF7E465E (void);
// 0x0000004F System.Void CreateObiBP/<CreateOBI>d__14::System.IDisposable.Dispose()
extern void U3CCreateOBIU3Ed__14_System_IDisposable_Dispose_mA7F079A16092731E506A9DC1FCF58913EA7C9EEC (void);
// 0x00000050 System.Boolean CreateObiBP/<CreateOBI>d__14::MoveNext()
extern void U3CCreateOBIU3Ed__14_MoveNext_mE4D02B8A76E4EE639E048716FCB8A51E65CCFBA3 (void);
// 0x00000051 System.Void CreateObiBP/<CreateOBI>d__14::<>m__Finally1()
extern void U3CCreateOBIU3Ed__14_U3CU3Em__Finally1_mFC2D79C4FB1C8D47AA49BFED56A8CD55D16AD833 (void);
// 0x00000052 System.Object CreateObiBP/<CreateOBI>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateOBIU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8F4E6D01EEB24AB539251D4E4711125E71EC0A4 (void);
// 0x00000053 System.Void CreateObiBP/<CreateOBI>d__14::System.Collections.IEnumerator.Reset()
extern void U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_Reset_m94B2BAABBBE38D5B38A23B14F1849F43283A7C52 (void);
// 0x00000054 System.Object CreateObiBP/<CreateOBI>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_get_Current_mE32BA430181E59F701837DD6862D39C8B836F94D (void);
// 0x00000055 System.Void EventStitchDS::Start()
extern void EventStitchDS_Start_mE706C227160B939141B992D3976BD9FFD945D30D (void);
// 0x00000056 System.Void EventStitchDS::Smethod()
extern void EventStitchDS_Smethod_m34F1103586873E5384DFC146E6CA01443BF76508 (void);
// 0x00000057 System.Boolean EventStitchDS::FastApproximately(System.Single,System.Single,System.Single)
extern void EventStitchDS_FastApproximately_mB240F5F641695C64BA6EE5C4989716BDF8695A60 (void);
// 0x00000058 System.Void EventStitchDS::StitchPairs(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,Obi.ObiStitcher,System.Single)
extern void EventStitchDS_StitchPairs_mE5F7B5FC223F878A44F0E015D3ECC6C755126EB9 (void);
// 0x00000059 System.Void EventStitchDS::.ctor()
extern void EventStitchDS__ctor_mECACDB6336EB1589FE9A9A8C98023D460B0EC080 (void);
// 0x0000005A System.Void EventStitchDS/OnStitchfinished::.ctor(System.Object,System.IntPtr)
extern void OnStitchfinished__ctor_m9F960B86B2ADF3E446B48924E920BE32BF672D10 (void);
// 0x0000005B System.Void EventStitchDS/OnStitchfinished::Invoke()
extern void OnStitchfinished_Invoke_mDBBBFCD4534B55D451284D53F9555148CE50161F (void);
// 0x0000005C System.IAsyncResult EventStitchDS/OnStitchfinished::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnStitchfinished_BeginInvoke_m813CBF8561714189C22201738A00E55F0FE1B851 (void);
// 0x0000005D System.Void EventStitchDS/OnStitchfinished::EndInvoke(System.IAsyncResult)
extern void OnStitchfinished_EndInvoke_m54080B43C21BE5BC218B826FBBB773B6811CF521 (void);
// 0x0000005E System.Void SpawnObject::Awake()
extern void SpawnObject_Awake_m8488C00D42B5DEAF841E22C153B011FB324A94B9 (void);
// 0x0000005F System.Collections.IEnumerator SpawnObject::RespawnSize(System.String)
extern void SpawnObject_RespawnSize_mE0E3232D9F0388D51F1DB659898D20142FCE3996 (void);
// 0x00000060 System.Collections.IEnumerator SpawnObject::RespawnColor()
extern void SpawnObject_RespawnColor_mD3B295CF8C5D23AC4BDC05BCCEC5D78D1528CE3F (void);
// 0x00000061 System.Void SpawnObject::AddDrpOpts()
extern void SpawnObject_AddDrpOpts_mCEF87B38F8E0C9E698CC9137B6ABB689966166D0 (void);
// 0x00000062 System.Collections.IEnumerator SpawnObject::AvailableColors()
extern void SpawnObject_AvailableColors_m0D8D4CD97205390CCA950E81B8838C974B4813FD (void);
// 0x00000063 System.Void SpawnObject::OnDisable()
extern void SpawnObject_OnDisable_m39C68380D05C4977A59C1B0A24ED5D658EE2D909 (void);
// 0x00000064 System.Void SpawnObject::Update()
extern void SpawnObject_Update_mBF48C6ACB7DEAD5CA8055E7CE0F3D18558E28712 (void);
// 0x00000065 System.Collections.IEnumerator SpawnObject::GetAssetBundleExtra(System.String,System.String)
extern void SpawnObject_GetAssetBundleExtra_m59ECC3DE93963BD0696FB9CA50F2CA57EF40B5EB (void);
// 0x00000066 System.Collections.IEnumerator SpawnObject::OnPositionContent()
extern void SpawnObject_OnPositionContent_m52D3CE7C460401F9F2CF9E6737906F39E728D157 (void);
// 0x00000067 System.Void SpawnObject::DisablePlanes()
extern void SpawnObject_DisablePlanes_mAA601812A7F841BA41FEC6E38CDE5BC5C2B6F8D5 (void);
// 0x00000068 System.Void SpawnObject::.ctor()
extern void SpawnObject__ctor_m5CDF660BE739E940121CC6E870A389E7F74045EE (void);
// 0x00000069 System.Void SpawnObject::<Awake>b__44_0(System.Threading.Tasks.Task`1<System.Uri>)
extern void SpawnObject_U3CAwakeU3Eb__44_0_mDC6AF6032E050DE844017BEE165EAAFBA04F7BD1 (void);
// 0x0000006A System.Void SpawnObject::<Awake>b__44_1()
extern void SpawnObject_U3CAwakeU3Eb__44_1_mB8992DB84930877FD37617539617E9439AED0E99 (void);
// 0x0000006B System.Void SpawnObject::<Awake>b__44_2()
extern void SpawnObject_U3CAwakeU3Eb__44_2_m0C49CFD50E46835A000B10F07D526ACDD648838A (void);
// 0x0000006C System.Void SpawnObject::<Awake>b__44_3()
extern void SpawnObject_U3CAwakeU3Eb__44_3_m7649A734B850981A28B2DEF385131CCEA6981494 (void);
// 0x0000006D System.Void SpawnObject::<Awake>b__44_4()
extern void SpawnObject_U3CAwakeU3Eb__44_4_mB908BB1BE2E3A1BDF9E0AD81FED28253716603CF (void);
// 0x0000006E System.Void SpawnObject::<Awake>b__44_5()
extern void SpawnObject_U3CAwakeU3Eb__44_5_m50A712DD6056870287C4A7DB9E09435FDD1993E3 (void);
// 0x0000006F System.Void SpawnObject::<Awake>b__44_6(System.Int32)
extern void SpawnObject_U3CAwakeU3Eb__44_6_m7AD4F82BDD6442A8837A750FEF0FEB560EFADB92 (void);
// 0x00000070 System.Void SpawnObject/<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mA8A9A7C0A564846399D4E4A38FAD59BE1ED09953 (void);
// 0x00000071 System.Void SpawnObject/<>c__DisplayClass45_0::<RespawnSize>b__0(System.Threading.Tasks.Task`1<System.Uri>)
extern void U3CU3Ec__DisplayClass45_0_U3CRespawnSizeU3Eb__0_m21BB8776E520E12F271E1D648CABB7720D8D97BA (void);
// 0x00000072 System.Void SpawnObject/<RespawnSize>d__45::.ctor(System.Int32)
extern void U3CRespawnSizeU3Ed__45__ctor_m88BFF80AEC54A20B5231575F9D453F32FCD3B8A9 (void);
// 0x00000073 System.Void SpawnObject/<RespawnSize>d__45::System.IDisposable.Dispose()
extern void U3CRespawnSizeU3Ed__45_System_IDisposable_Dispose_m88C9FEB4B8FB6C1BB4F83458A6AE573C44B74FDB (void);
// 0x00000074 System.Boolean SpawnObject/<RespawnSize>d__45::MoveNext()
extern void U3CRespawnSizeU3Ed__45_MoveNext_m3DE87B22133E8DE0817D55E582149677CDC492C2 (void);
// 0x00000075 System.Object SpawnObject/<RespawnSize>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnSizeU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39AC1153721E7A5AE59D1A77B9900B3A6C5E5EEE (void);
// 0x00000076 System.Void SpawnObject/<RespawnSize>d__45::System.Collections.IEnumerator.Reset()
extern void U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_Reset_m974E70A42014423F08D1565E86B4D39C7E268EFF (void);
// 0x00000077 System.Object SpawnObject/<RespawnSize>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_get_Current_mA773B08876925350D43ECD146FEE0D954D30146C (void);
// 0x00000078 System.Void SpawnObject/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_mDA8B629C905F38F2351B9B8943480D6F3ED1246E (void);
// 0x00000079 System.Void SpawnObject/<>c__DisplayClass46_0::<RespawnColor>b__0(System.Threading.Tasks.Task`1<System.Uri>)
extern void U3CU3Ec__DisplayClass46_0_U3CRespawnColorU3Eb__0_m6FE7A7F6CA4F78C007ED4BE952ABE23CB0C291A0 (void);
// 0x0000007A System.Void SpawnObject/<RespawnColor>d__46::.ctor(System.Int32)
extern void U3CRespawnColorU3Ed__46__ctor_mD8855F765A5C43BEEF30F012AEB49BD1F2A748DE (void);
// 0x0000007B System.Void SpawnObject/<RespawnColor>d__46::System.IDisposable.Dispose()
extern void U3CRespawnColorU3Ed__46_System_IDisposable_Dispose_m249A9D780D8D886614FDED6E01EB42EBEB4A6487 (void);
// 0x0000007C System.Boolean SpawnObject/<RespawnColor>d__46::MoveNext()
extern void U3CRespawnColorU3Ed__46_MoveNext_mD696CA6E0B6E13FBA5223813E47E70C045FBF80D (void);
// 0x0000007D System.Object SpawnObject/<RespawnColor>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnColorU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD97D6E8AAABA5C750DC8CB00D5479AC89A5BFA7 (void);
// 0x0000007E System.Void SpawnObject/<RespawnColor>d__46::System.Collections.IEnumerator.Reset()
extern void U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_Reset_m08FE605DE550D0A3942DF553EFA3454649868582 (void);
// 0x0000007F System.Object SpawnObject/<RespawnColor>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_get_Current_m5376A4C8A91D27D8E3BE69DBF1A0301865167299 (void);
// 0x00000080 System.Void SpawnObject/<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_m29D5138CDFFFE502D3285A1C90AAB1DF8303A1D5 (void);
// 0x00000081 System.Void SpawnObject/<>c__DisplayClass48_0::<AvailableColors>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__0_m79CD505B22CB8A120ECDE7F0289DD7C129451A5F (void);
// 0x00000082 System.Boolean SpawnObject/<>c__DisplayClass48_0::<AvailableColors>b__1()
extern void U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__1_m14F1D91C701A53365E7AF7916F198AC99A8A2C96 (void);
// 0x00000083 System.Void SpawnObject/<>c__DisplayClass48_0::<AvailableColors>b__2(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__2_m3A1394D1DB4454CBC9268EFCB9DDF8A2C7D94829 (void);
// 0x00000084 System.Boolean SpawnObject/<>c__DisplayClass48_0::<AvailableColors>b__3()
extern void U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__3_mAD73710125C7210312A4ABD748968210C4E1EC72 (void);
// 0x00000085 System.Void SpawnObject/<AvailableColors>d__48::.ctor(System.Int32)
extern void U3CAvailableColorsU3Ed__48__ctor_m9ACE7EA35939C6AF56A3F09CAF876C1ADB3E0CE6 (void);
// 0x00000086 System.Void SpawnObject/<AvailableColors>d__48::System.IDisposable.Dispose()
extern void U3CAvailableColorsU3Ed__48_System_IDisposable_Dispose_m55EA1B9E30947A469F5A1B916B43EA3FD3E0AC00 (void);
// 0x00000087 System.Boolean SpawnObject/<AvailableColors>d__48::MoveNext()
extern void U3CAvailableColorsU3Ed__48_MoveNext_m59D8B2E33BC8FED66334A8A7639968EE2A109229 (void);
// 0x00000088 System.Object SpawnObject/<AvailableColors>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAvailableColorsU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE25815C4D63837B720D40C07AE702CCB69C1D38 (void);
// 0x00000089 System.Void SpawnObject/<AvailableColors>d__48::System.Collections.IEnumerator.Reset()
extern void U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_Reset_m22EE8729D104DD16C47EA3D39D7EFD808EAFD7D4 (void);
// 0x0000008A System.Object SpawnObject/<AvailableColors>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_get_Current_m6B887B061C6F11CF5DCDBB3ED3DDFF453CE6EA09 (void);
// 0x0000008B System.Void SpawnObject/<GetAssetBundleExtra>d__51::.ctor(System.Int32)
extern void U3CGetAssetBundleExtraU3Ed__51__ctor_mE87227D1EC63AF5A16DAFC6449A71A27698EA652 (void);
// 0x0000008C System.Void SpawnObject/<GetAssetBundleExtra>d__51::System.IDisposable.Dispose()
extern void U3CGetAssetBundleExtraU3Ed__51_System_IDisposable_Dispose_m67B570BAA9827F4884D1A27EF57D8BCEBC9D7650 (void);
// 0x0000008D System.Boolean SpawnObject/<GetAssetBundleExtra>d__51::MoveNext()
extern void U3CGetAssetBundleExtraU3Ed__51_MoveNext_m3C109060CED75B272A76DA9ED371353C830E6958 (void);
// 0x0000008E System.Object SpawnObject/<GetAssetBundleExtra>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetAssetBundleExtraU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m811274D9475214B976094D0D0AA90CBADAE30578 (void);
// 0x0000008F System.Void SpawnObject/<GetAssetBundleExtra>d__51::System.Collections.IEnumerator.Reset()
extern void U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_Reset_m1FC9B030B7BF99AA20630D8D4E082DD182443C29 (void);
// 0x00000090 System.Object SpawnObject/<GetAssetBundleExtra>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_get_Current_m2AA89470EDD5DAE93E38D463BE1C4D2785AA4106 (void);
// 0x00000091 System.Void SpawnObject/<OnPositionContent>d__52::.ctor(System.Int32)
extern void U3COnPositionContentU3Ed__52__ctor_mFEC8E57EA5F72169E05AAAFB7FFFB7D33B910830 (void);
// 0x00000092 System.Void SpawnObject/<OnPositionContent>d__52::System.IDisposable.Dispose()
extern void U3COnPositionContentU3Ed__52_System_IDisposable_Dispose_m6AC2F351F789E1DA2154C3D796F41C90A7CED279 (void);
// 0x00000093 System.Boolean SpawnObject/<OnPositionContent>d__52::MoveNext()
extern void U3COnPositionContentU3Ed__52_MoveNext_mB98BA0202DD75E80C70217D6AD3E6A6B01D3BB65 (void);
// 0x00000094 System.Object SpawnObject/<OnPositionContent>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnPositionContentU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B864B6D262CCF48697FACB821EF52279A3002AA (void);
// 0x00000095 System.Void SpawnObject/<OnPositionContent>d__52::System.Collections.IEnumerator.Reset()
extern void U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_Reset_m1C71BEAB3372161324E40787AEFDA57E524D6F0F (void);
// 0x00000096 System.Object SpawnObject/<OnPositionContent>d__52::System.Collections.IEnumerator.get_Current()
extern void U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_get_Current_m67A830885BDDC192E16E597A6ED50A3E3C92A416 (void);
// 0x00000097 System.Void changeobi::Start()
extern void changeobi_Start_mCD34167D84D85AD495AB7B3DB2EB00F4D3F06DFF (void);
// 0x00000098 System.Void changeobi::AttachOBI()
extern void changeobi_AttachOBI_m0BD1EEB038499BD33DD5DC25A1CCF287F0FC81F0 (void);
// 0x00000099 UnityEngine.Texture2D changeobi::LoadParamImg(System.String)
extern void changeobi_LoadParamImg_m09C2F6FC69196B1DC5AAFCE8C813AB7F396BF840 (void);
// 0x0000009A UnityEngine.Texture2D changeobi::duplicateTexture(UnityEngine.Texture2D)
extern void changeobi_duplicateTexture_mDEC598CA197414A67BAC47DDB36779E20069622C (void);
// 0x0000009B Obi.ObiSkinnedClothBlueprint changeobi::ReadObiBlueprint(System.String)
extern void changeobi_ReadObiBlueprint_m2B40A5856AE6265FF25DF55D36ECA03EB684F1A1 (void);
// 0x0000009C System.Void changeobi::.ctor()
extern void changeobi__ctor_mE69BAFEE6414E3E0F0C46F2F7FA2EFE72C79AC67 (void);
// 0x0000009D System.Void changeobi/OnObiAdded::.ctor(System.Object,System.IntPtr)
extern void OnObiAdded__ctor_m6F885CF87CB3C67751F7CFCCEF93041B7B988986 (void);
// 0x0000009E System.Void changeobi/OnObiAdded::Invoke()
extern void OnObiAdded_Invoke_m113AFEC3143E467B194018A6FDBCEFE83CC3621E (void);
// 0x0000009F System.IAsyncResult changeobi/OnObiAdded::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnObiAdded_BeginInvoke_m6AE43FFD84BDDD00EC3D50B0251380D4D29EBEA5 (void);
// 0x000000A0 System.Void changeobi/OnObiAdded::EndInvoke(System.IAsyncResult)
extern void OnObiAdded_EndInvoke_mF4A074F28AE3D865DEB003F7ABEFFC27DC8E1FDB (void);
// 0x000000A1 System.Void AutoCannon::Awake()
extern void AutoCannon_Awake_m0B3F9E1A76494A9B990C2592355BCA021182410C (void);
// 0x000000A2 System.Void AutoCannon::Update()
extern void AutoCannon_Update_m29856A10678515925F64F8F37BFC519CB2FC5B3F (void);
// 0x000000A3 System.Collections.IEnumerator AutoCannon::Shoot()
extern void AutoCannon_Shoot_mF63F6DA0A7FB8DA424F38A5853EAA1A6B2255444 (void);
// 0x000000A4 System.Void AutoCannon::Launch(UnityEngine.Rigidbody)
extern void AutoCannon_Launch_mE945B7E518A8168885A2323DB34FAD265EC3BD6A (void);
// 0x000000A5 System.Void AutoCannon::.ctor()
extern void AutoCannon__ctor_m423E23A23D43AD26C4C240167226FDD9FDEC5FDA (void);
// 0x000000A6 System.Void AutoCannon/<Shoot>d__11::.ctor(System.Int32)
extern void U3CShootU3Ed__11__ctor_m884BF51AF4DF8056D4965C028CF7CFF29DC42E43 (void);
// 0x000000A7 System.Void AutoCannon/<Shoot>d__11::System.IDisposable.Dispose()
extern void U3CShootU3Ed__11_System_IDisposable_Dispose_mE4F09F48E287A53FF7E525CD9413A83AC867F28A (void);
// 0x000000A8 System.Boolean AutoCannon/<Shoot>d__11::MoveNext()
extern void U3CShootU3Ed__11_MoveNext_m4309144FBA4D6A962B4D5BEC3214C4D2CDDB9B87 (void);
// 0x000000A9 System.Object AutoCannon/<Shoot>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C060564EA19770828A39D2CF575AE4E6387CF6C (void);
// 0x000000AA System.Void AutoCannon/<Shoot>d__11::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__11_System_Collections_IEnumerator_Reset_m95B67AE27CE9C309D876182ADFCB7EB45FAB2253 (void);
// 0x000000AB System.Object AutoCannon/<Shoot>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__11_System_Collections_IEnumerator_get_Current_mD7F7BAED04E09C7275269837A41C3C5ACEE25825 (void);
// 0x000000AC System.Void BoatController::Update()
extern void BoatController_Update_mDCE9F01212A47882B1989C9AEE0F16FE4E01E22D (void);
// 0x000000AD System.Void BoatController::Respawn(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void BoatController_Respawn_mA6571213D913950C16566945D1A107A4A25CFA85 (void);
// 0x000000AE System.Void BoatController::.ctor()
extern void BoatController__ctor_m4FA5C465BE85611BC18F8A8764479238D10447A1 (void);
// 0x000000AF System.Void BoatFinishLine::OnTriggerEnter(UnityEngine.Collider)
extern void BoatFinishLine_OnTriggerEnter_mEB3243D3FA9435FAEF7C2E53DE4B6C64A61B7197 (void);
// 0x000000B0 System.Void BoatFinishLine::.ctor()
extern void BoatFinishLine__ctor_m9A36971472140FF28E16AF892B672F4C2377C1F6 (void);
// 0x000000B1 System.Void BoatGameController::Die()
extern void BoatGameController_Die_m364402719425090B66310A9A1CDC13781A725C18 (void);
// 0x000000B2 System.Void BoatGameController::Finish()
extern void BoatGameController_Finish_m7EB191C5A00772C7894A8EEE18EF545657763616 (void);
// 0x000000B3 System.Void BoatGameController::Update()
extern void BoatGameController_Update_mE44F8247EA88E1822BDE86E3A5F1511CF93FD200 (void);
// 0x000000B4 System.Void BoatGameController::.ctor()
extern void BoatGameController__ctor_mB8D2B757DF9876305711829C92335AA506F52A58 (void);
// 0x000000B5 System.Void CannonBall::Awake()
extern void CannonBall_Awake_mDF03DD952ABB744D517E5810E3BA6F51422BBFD9 (void);
// 0x000000B6 System.Void CannonBall::OnTriggerEnter(UnityEngine.Collider)
extern void CannonBall_OnTriggerEnter_m91362C26D92B656EE7169489715D45F06799EF89 (void);
// 0x000000B7 System.Void CannonBall::.ctor()
extern void CannonBall__ctor_mBE2D057682E5E5E02573C5DB6FAC7AAE229C2495 (void);
// 0x000000B8 System.Void ClothTornEvent::.ctor()
extern void ClothTornEvent__ctor_m72D4C4974CB71597C3A89C0452D246AE184BEB38 (void);
// 0x000000B9 System.Void Mine::Awake()
extern void Mine_Awake_m58C8F5FDAED78C9F8B65532C6D9EFE9F544E6212 (void);
// 0x000000BA System.Void Mine::Update()
extern void Mine_Update_m79DE943463F60EF84971D18A447EBCB2A4410A12 (void);
// 0x000000BB System.Void Mine::OnCollisionEnter(UnityEngine.Collision)
extern void Mine_OnCollisionEnter_m690D60F3CF73D839847FD50936EFF2A12EDB8251 (void);
// 0x000000BC System.Void Mine::.ctor()
extern void Mine__ctor_m4764AA96D27727EB5A0D1BFE0C8DC7A7A92DA5E6 (void);
// 0x000000BD System.Void RaycastLasers::Update()
extern void RaycastLasers_Update_m21105C5A886E47994304701EAFD2EF7535169044 (void);
// 0x000000BE System.Void RaycastLasers::.ctor()
extern void RaycastLasers__ctor_m7FCFEF7FAABF8B06CBD65E950736359950554A2B (void);
// 0x000000BF System.Void Rudder::Awake()
extern void Rudder_Awake_mC1C287C9FB4AC93C9A939AFA62E063D6F5F31982 (void);
// 0x000000C0 System.Void Rudder::FixedUpdate()
extern void Rudder_FixedUpdate_m9D7C8E45614E9C3DAA0D7AE6821248D1F921DCBB (void);
// 0x000000C1 System.Void Rudder::.ctor()
extern void Rudder__ctor_mD7D5560A62B2EFF3A53FDD201B82E30AEC667BD1 (void);
// 0x000000C2 UnityEngine.Mesh SackGenerator::GenerateSheetMesh()
extern void SackGenerator_GenerateSheetMesh_m01FD9E7358BFAE145976E183AEF2E822112AD881 (void);
// 0x000000C3 System.Collections.IEnumerator SackGenerator::GenerateSack()
extern void SackGenerator_GenerateSack_m5749634500E10A9C49CC02D5B461D5CE4EC62F74 (void);
// 0x000000C4 System.Void SackGenerator::Update()
extern void SackGenerator_Update_m61EB7A1965BBC01ED18EF37657A3F0A90AA25D51 (void);
// 0x000000C5 System.Void SackGenerator::.ctor()
extern void SackGenerator__ctor_m840AC8E93954A7C871F341081AE309B9052DE168 (void);
// 0x000000C6 System.Void SackGenerator/<GenerateSack>d__5::.ctor(System.Int32)
extern void U3CGenerateSackU3Ed__5__ctor_m60AC74DB36D70827F636F2922CB635E9B7F977F5 (void);
// 0x000000C7 System.Void SackGenerator/<GenerateSack>d__5::System.IDisposable.Dispose()
extern void U3CGenerateSackU3Ed__5_System_IDisposable_Dispose_mC3E89951FB26F5B4CE2B7E27251604B8F4376658 (void);
// 0x000000C8 System.Boolean SackGenerator/<GenerateSack>d__5::MoveNext()
extern void U3CGenerateSackU3Ed__5_MoveNext_mB6D8C3958242578BD39A2524139636E33D460A3F (void);
// 0x000000C9 System.Object SackGenerator/<GenerateSack>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateSackU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m748A15AB6A05CC78B95300CA85662577A2EA5F19 (void);
// 0x000000CA System.Void SackGenerator/<GenerateSack>d__5::System.Collections.IEnumerator.Reset()
extern void U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_Reset_mD87A155F07288E5B5248CE7AA284740859004F3F (void);
// 0x000000CB System.Object SackGenerator/<GenerateSack>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_get_Current_m61246B7B8E9A24805CF9F6D7ABA51935CC7BA088 (void);
// 0x000000CC System.Void StretchToColors::Start()
extern void StretchToColors_Start_m7C77408010D4ED8A560CF9C51C6B8A606EAA1561 (void);
// 0x000000CD System.Void StretchToColors::OnDestroy()
extern void StretchToColors_OnDestroy_mEC83FB00FC818CF84333AA4757FEEB4D4BAA71B0 (void);
// 0x000000CE System.Void StretchToColors::Cloth_OnEndStep(Obi.ObiActor,System.Single)
extern void StretchToColors_Cloth_OnEndStep_mCCA1EB30709882BF5ACBEE581725AAA3359BB9B3 (void);
// 0x000000CF System.Void StretchToColors::.ctor()
extern void StretchToColors__ctor_m2620D123D4418AF0F12B010509CBD374A52268E8 (void);
// 0x000000D0 System.Void WaterTrigger::.ctor()
extern void WaterTrigger__ctor_mDF14818F4DDDFA48CE03DD4ED16387C76A0D37F4 (void);
// 0x000000D1 System.Void ActorCOMTransform::Update()
extern void ActorCOMTransform_Update_m42CC68376DF28C96387BACFC64C48515AE9CE469 (void);
// 0x000000D2 System.Void ActorCOMTransform::.ctor()
extern void ActorCOMTransform__ctor_mB7B969737BB7416B0E66C1CD4B8FDF1E8A7186CD (void);
// 0x000000D3 System.Void ActorSpawner::Update()
extern void ActorSpawner_Update_mBB452D66B51B50FE636D74E7FF957A10A1FE2A07 (void);
// 0x000000D4 System.Void ActorSpawner::.ctor()
extern void ActorSpawner__ctor_m48C1E536BA52A4F4EDE74F5B90749B4619F13C86 (void);
// 0x000000D5 System.Void AddRandomVelocity::Update()
extern void AddRandomVelocity_Update_m6584A3F85D2E61F6D420B0B911B3C61637008449 (void);
// 0x000000D6 System.Void AddRandomVelocity::.ctor()
extern void AddRandomVelocity__ctor_mC1CFE0D59EF94BA30BA6AAB81CA6FF0899F84DDF (void);
// 0x000000D7 System.Void Blinker::Awake()
extern void Blinker_Awake_mC92D3CBC36B46F324C8197377BC40A434109C876 (void);
// 0x000000D8 System.Void Blinker::Blink()
extern void Blinker_Blink_m4082F3A19006F464F4C14105BED460D5A1A0E273 (void);
// 0x000000D9 System.Void Blinker::LateUpdate()
extern void Blinker_LateUpdate_mEE40A5EDA3EAFBB9CAD39BC413CB921E15CF6CCB (void);
// 0x000000DA System.Void Blinker::.ctor()
extern void Blinker__ctor_mAE1E307D31F5C3BB83001FBC5B2D61F827A69F3C (void);
// 0x000000DB System.Void ColliderHighlighter::Awake()
extern void ColliderHighlighter_Awake_m5920B8F626704DB40821E9BB9A85FDA1E0BFE4B3 (void);
// 0x000000DC System.Void ColliderHighlighter::OnEnable()
extern void ColliderHighlighter_OnEnable_m6F1425D169D88CC4D90E0A8F144A05C55F7E03B6 (void);
// 0x000000DD System.Void ColliderHighlighter::OnDisable()
extern void ColliderHighlighter_OnDisable_m2780DDE2CFE790FF4B04AD45654BDB74DD752893 (void);
// 0x000000DE System.Void ColliderHighlighter::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ColliderHighlighter_Solver_OnCollision_m8A90B739BAB8461A0179C2B4CD4EE87AB3DE8D30 (void);
// 0x000000DF System.Void ColliderHighlighter::.ctor()
extern void ColliderHighlighter__ctor_mBE8B58BC252B389A907738D87DAD06F094A62587 (void);
// 0x000000E0 System.Void CollisionEventHandler::Awake()
extern void CollisionEventHandler_Awake_m744FDE1AB7D3F6777399B5108896E5E2CC61FE93 (void);
// 0x000000E1 System.Void CollisionEventHandler::OnEnable()
extern void CollisionEventHandler_OnEnable_m2D3F33422D785CEDFCDCF1012A0B37B5F3ACB9F6 (void);
// 0x000000E2 System.Void CollisionEventHandler::OnDisable()
extern void CollisionEventHandler_OnDisable_m75B03CB65B3D0226C04FF77A99D488596144BC32 (void);
// 0x000000E3 System.Void CollisionEventHandler::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void CollisionEventHandler_Solver_OnCollision_m484E685CBD1E502504C567FD2D5CCB6F0860D616 (void);
// 0x000000E4 System.Void CollisionEventHandler::OnDrawGizmos()
extern void CollisionEventHandler_OnDrawGizmos_m7939D47B098838344A41DD08F547354E268E07A8 (void);
// 0x000000E5 System.Void CollisionEventHandler::.ctor()
extern void CollisionEventHandler__ctor_m92805BEBFF6E7114772EF8DFF4D5C244467A479B (void);
// 0x000000E6 System.Void DebugParticleFrames::Awake()
extern void DebugParticleFrames_Awake_mA5C0197560CB8F73D7472974A693020EA29B28B3 (void);
// 0x000000E7 System.Void DebugParticleFrames::OnDrawGizmos()
extern void DebugParticleFrames_OnDrawGizmos_m51378254E7930D9050D3355F810A3BC15A4CBBED (void);
// 0x000000E8 System.Void DebugParticleFrames::.ctor()
extern void DebugParticleFrames__ctor_mBE2F687FB03E680B1957BB4D2C9F19E43F3CF260 (void);
// 0x000000E9 System.Void ExtrapolationCamera::Start()
extern void ExtrapolationCamera_Start_mCB1FD551CBFB047EEA0FA534CE64CA969640D51E (void);
// 0x000000EA System.Void ExtrapolationCamera::FixedUpdate()
extern void ExtrapolationCamera_FixedUpdate_m622D9ECE2C7F91E1C5A70068B94B7C3E03AF4523 (void);
// 0x000000EB System.Void ExtrapolationCamera::LateUpdate()
extern void ExtrapolationCamera_LateUpdate_mB90259A761A12F0D8301E75BC381A6C0B32B443D (void);
// 0x000000EC System.Void ExtrapolationCamera::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ExtrapolationCamera_Teleport_m8A6D37587FD1DF2518851886F5BE70B897DE0B67 (void);
// 0x000000ED System.Void ExtrapolationCamera::.ctor()
extern void ExtrapolationCamera__ctor_mA4E8D57C9E25BC7F9DA5676D79A7FC5505073032 (void);
// 0x000000EE System.Single FPSDisplay::get_CurrentFPS()
extern void FPSDisplay_get_CurrentFPS_mA9019EF8EAA0384D37F2DE714D84E5413E783C72 (void);
// 0x000000EF System.Single FPSDisplay::get_FPSMedian()
extern void FPSDisplay_get_FPSMedian_m6ACF1BBC790E915C8CE3D94C797CE4A4E8F116DD (void);
// 0x000000F0 System.Single FPSDisplay::get_FPSAverage()
extern void FPSDisplay_get_FPSAverage_m52F2116EA4DD6DA75E483FE24D21321CF9F2D4C1 (void);
// 0x000000F1 System.Void FPSDisplay::Start()
extern void FPSDisplay_Start_mF1CE43CCE2AAB57ECD8D8B623B7E7358AB163C55 (void);
// 0x000000F2 System.Void FPSDisplay::Update()
extern void FPSDisplay_Update_m8740416265A05D0C56CA05EC0DE3A12E5132B028 (void);
// 0x000000F3 System.Void FPSDisplay::ResetMedianAndAverage()
extern void FPSDisplay_ResetMedianAndAverage_m82840B2C8D0189EA211AE4B14A8C94B22D932F70 (void);
// 0x000000F4 System.Void FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mC4AD184EF09FAF5C43D89D0F7C211B2754D95B18 (void);
// 0x000000F5 System.Void FirstPersonLauncher::Update()
extern void FirstPersonLauncher_Update_m90772B87D990D9020988335EBC4E68DD68D177F5 (void);
// 0x000000F6 System.Void FirstPersonLauncher::.ctor()
extern void FirstPersonLauncher__ctor_mEE941D947823862AE74E5FA285B42A02799BC5C9 (void);
// 0x000000F7 System.Void ObiActorTeleport::Teleport()
extern void ObiActorTeleport_Teleport_m83F48614A9D7AE20261E92546E28B7C65C26DA1D (void);
// 0x000000F8 System.Void ObiActorTeleport::.ctor()
extern void ObiActorTeleport__ctor_mCC83730596EA6276332F31F582F5C8D1F08EE019 (void);
// 0x000000F9 System.Void ObiParticleCounter::Awake()
extern void ObiParticleCounter_Awake_m42D6C0254A2B27439769A96FE50705D33EC62B98 (void);
// 0x000000FA System.Void ObiParticleCounter::OnEnable()
extern void ObiParticleCounter_OnEnable_mA9798F6382D0620125A95B047C974EBA660926CB (void);
// 0x000000FB System.Void ObiParticleCounter::OnDisable()
extern void ObiParticleCounter_OnDisable_m3224433810D75B192BD03D399ADE656864BBD594 (void);
// 0x000000FC System.Void ObiParticleCounter::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiParticleCounter_Solver_OnCollision_mC585B439D90C23BA5FE74E65D3195651DCAEF392 (void);
// 0x000000FD System.Void ObiParticleCounter::.ctor()
extern void ObiParticleCounter__ctor_m3107B0DABE8BEA1C8EB84C5A8EEEDB3C3E4B1B52 (void);
// 0x000000FE System.Void ObjectDragger::OnMouseDown()
extern void ObjectDragger_OnMouseDown_mC96AA07AABD47631E097AEA08E9A81AE1506EEEE (void);
// 0x000000FF System.Void ObjectDragger::OnMouseDrag()
extern void ObjectDragger_OnMouseDrag_m7E0B861E5546C709DED1ACF6F33ED31D6004FA6E (void);
// 0x00000100 System.Void ObjectDragger::.ctor()
extern void ObjectDragger__ctor_m97083AF9B77C7E2554A3D0701C6FAEE5D2282A6C (void);
// 0x00000101 System.Void ObjectLimit::Update()
extern void ObjectLimit_Update_m6689D4679CA59887B038EE4BD3F2294753C57E38 (void);
// 0x00000102 System.Void ObjectLimit::.ctor()
extern void ObjectLimit__ctor_m8F9F789DA4B949FECADA677D21A26E893A0A5F41 (void);
// 0x00000103 System.Void SlowmoToggler::Slowmo(System.Boolean)
extern void SlowmoToggler_Slowmo_mF683360609D33F0B08F26A1019AAAD2BA21C8404 (void);
// 0x00000104 System.Void SlowmoToggler::.ctor()
extern void SlowmoToggler__ctor_m17E4BB8E004DE90384ADF6074E74BC7C383E7001 (void);
// 0x00000105 System.Void WorldSpaceGravity::Awake()
extern void WorldSpaceGravity_Awake_m20A0105B37EEF41CE69A528E59C13099276E4F2B (void);
// 0x00000106 System.Void WorldSpaceGravity::Update()
extern void WorldSpaceGravity_Update_m9BFD6836B3B4FD9F671F9CC77622279143C1F72C (void);
// 0x00000107 System.Void WorldSpaceGravity::.ctor()
extern void WorldSpaceGravity__ctor_m8A54BF21A4069DBE4B27EA67FDABDC1C26766539 (void);
// 0x00000108 System.Void AddCat::Start()
extern void AddCat_Start_mDA135BBA22B0266142900DCF8DA177E00A87F565 (void);
// 0x00000109 System.Void AddCat::AddCats()
extern void AddCat_AddCats_mC9B200DDB37189AAA7E2FBDE470ACC1B7B50F654 (void);
// 0x0000010A System.Void AddCat::InitItem(UnityEngine.GameObject,System.String,System.Boolean)
extern void AddCat_InitItem_mA181501CA9E0B52243C8E766DE44707295D7C19E (void);
// 0x0000010B System.Void AddCat::MakeItem(System.String,System.Boolean)
extern void AddCat_MakeItem_mC5AA410D891F645085AFD61F7B014A700C3E56A2 (void);
// 0x0000010C System.Void AddCat::.ctor()
extern void AddCat__ctor_m6417B73991CDD9C134D23D047B81372C070C1598 (void);
// 0x0000010D System.Void AddCat/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m115880E935FB7D335129E39E720465FF16A58570 (void);
// 0x0000010E System.Void AddCat/<>c__DisplayClass16_0::<InitItem>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CInitItemU3Eb__0_m2895CBAC8E8C9A9ACB39231D10DC6649EADEB4C1 (void);
// 0x0000010F System.Void AddItemComp::Start()
extern void AddItemComp_Start_m2378DEEE58C3A77F9AA3989853B870A86AAFF92B (void);
// 0x00000110 System.Collections.IEnumerator AddItemComp::SetPrefs(System.Int32,System.Int32)
extern void AddItemComp_SetPrefs_m930EF2A4A0384D344FBA9D9C5EACCB14125E2FB0 (void);
// 0x00000111 System.Collections.IEnumerator AddItemComp::ButNextRoutine()
extern void AddItemComp_ButNextRoutine_m491D1048FB724E16FD51D5C8FA3B6E4864E645F5 (void);
// 0x00000112 System.Collections.IEnumerator AddItemComp::DelItemListWait(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Texture2D>)
extern void AddItemComp_DelItemListWait_m304E9147CFA670BF3F714A6326A4C0D40B9AEFC4 (void);
// 0x00000113 System.Collections.IEnumerator AddItemComp::ButHomeRoutine()
extern void AddItemComp_ButHomeRoutine_m12A488E1C2E634DBFD20C4DF299FF5B998369AB2 (void);
// 0x00000114 System.Void AddItemComp::ManagerReset()
extern void AddItemComp_ManagerReset_m7C0F20A88B512978DF5EF6A53D0CECB0A17C4D01 (void);
// 0x00000115 System.Collections.IEnumerator AddItemComp::ButSignOutRoutine()
extern void AddItemComp_ButSignOutRoutine_mAD978974F12CC9E41D44B7FB3E307E70173CD761 (void);
// 0x00000116 System.Collections.IEnumerator AddItemComp::ButPrevRoutine()
extern void AddItemComp_ButPrevRoutine_mB7E7EF57C738F6AECF963DB4FADFAA20121C4228 (void);
// 0x00000117 System.Boolean AddItemComp::IsFinishedDownloading()
extern void AddItemComp_IsFinishedDownloading_mA3D8DE7943509E57F7A0A2E8DD12BA0E8251E83E (void);
// 0x00000118 System.Collections.IEnumerator AddItemComp::FilRecRoutine(System.Single)
extern void AddItemComp_FilRecRoutine_m9994A64E295BBD9D52D828B6184F2CAC33032C0E (void);
// 0x00000119 System.Collections.IEnumerator AddItemComp::FilTypeRoutine(System.Int32)
extern void AddItemComp_FilTypeRoutine_mDAAE833DE05D0C9210A61E10C69BD790D1951783 (void);
// 0x0000011A System.Collections.IEnumerator AddItemComp::FilSrcRoutine(System.Int32,System.Single)
extern void AddItemComp_FilSrcRoutine_m7A9182A31CA54285EF9BFF3C760E3A76CDED3C90 (void);
// 0x0000011B System.Collections.IEnumerator AddItemComp::FilUpdRoutine(System.Int32,System.Int32)
extern void AddItemComp_FilUpdRoutine_m4ED3B5CE0BA022BEC13C4AA0DBA0393E0C0AB507 (void);
// 0x0000011C System.Void AddItemComp::OnDisable()
extern void AddItemComp_OnDisable_m19DF731B0B194146D181F3D5DE9A4309F58AE3E9 (void);
// 0x0000011D System.Collections.IEnumerator AddItemComp::StopDownloading(System.String)
extern void AddItemComp_StopDownloading_mFE88CA2D4154EE4AC05F5C98970D9E1EE51B4B99 (void);
// 0x0000011E System.Void AddItemComp::Update()
extern void AddItemComp_Update_mB971A2D24EAE16CABF31D48E2A35D91030288680 (void);
// 0x0000011F System.Collections.IEnumerator AddItemComp::ResendRepeat()
extern void AddItemComp_ResendRepeat_mF9585698444E6288F3D0BFEDE9E6212E7240CD06 (void);
// 0x00000120 System.Void AddItemComp::ResetMan()
extern void AddItemComp_ResetMan_mC801238665C33571644CFAAAB85479847A165D1A (void);
// 0x00000121 System.Void AddItemComp::scrollrectCallBack(UnityEngine.Vector2)
extern void AddItemComp_scrollrectCallBack_m067C064D782129521DF003F0AD4F01663EDB5664 (void);
// 0x00000122 System.Collections.IEnumerator AddItemComp::tmpScroll()
extern void AddItemComp_tmpScroll_m86AE102F3384CE2C55BA0B5F9D750F846FD95D9F (void);
// 0x00000123 System.Collections.IEnumerator AddItemComp::BeginGarmentCall()
extern void AddItemComp_BeginGarmentCall_m087BB0472C9CCF6A072EFEFC1C2593AB99A16164 (void);
// 0x00000124 System.Void AddItemComp::AddImages(System.Collections.Generic.List`1<System.String>)
extern void AddItemComp_AddImages_m23FB81BA2924B6546C402D2F80B29DAECE377F5D (void);
// 0x00000125 System.Void AddItemComp::ToggleLoading(System.Boolean)
extern void AddItemComp_ToggleLoading_m3C80531EDA22129E7716FE071C066B9B0FFD9D4D (void);
// 0x00000126 System.Collections.IEnumerator AddItemComp::ImageTexHandler(System.String,System.String,System.Int32)
extern void AddItemComp_ImageTexHandler_mAFE4B133389E581FD3A8C6CF2141FB043A1932BA (void);
// 0x00000127 System.Collections.IEnumerator AddItemComp::ImageAddingProcess(System.Collections.Generic.List`1<System.String>)
extern void AddItemComp_ImageAddingProcess_m7A5ADBEF5D1CC2AE157522E14FD7D6F104BFE1A6 (void);
// 0x00000128 UnityEngine.Texture2D AddItemComp::ScaleTexture(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void AddItemComp_ScaleTexture_mACBB04DE9E10947DD95E02B7AE4C6697338B67A5 (void);
// 0x00000129 System.Collections.IEnumerator AddItemComp::FbaseQueries()
extern void AddItemComp_FbaseQueries_mC730E8CD3FF08272E05325E76D173716C0A20A70 (void);
// 0x0000012A System.Void AddItemComp::LayoutFix(System.Int32,UnityEngine.GameObject)
extern void AddItemComp_LayoutFix_m3B45FC03118B2EE7071CAE387DFE3B91D689A41C (void);
// 0x0000012B System.Void AddItemComp::MakeGarms(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Collections.Generic.List`1<System.String>,UnityEngine.GameObject,System.Int32,System.Collections.Generic.List`1<System.String>)
extern void AddItemComp_MakeGarms_m32492B4F4016722F296A65C8146C3B286D473052 (void);
// 0x0000012C System.Void AddItemComp::AddGarms(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Int32,System.Int32,System.Collections.Generic.List`1<System.String>,Firebase.Firestore.DocumentSnapshot,System.Boolean,System.Collections.Generic.List`1<System.String>)
extern void AddItemComp_AddGarms_m9A28486C6857D4EDCB3B9E8F2C5E1EBB793738D3 (void);
// 0x0000012D System.Void AddItemComp::UpdateGarms()
extern void AddItemComp_UpdateGarms_m7B70106F734C08C934EDE76E6561A0B9B3A5D011 (void);
// 0x0000012E System.Void AddItemComp::DelItemList(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Texture2D>)
extern void AddItemComp_DelItemList_m2A5233C3B457722A0BADAD248EA04A4073975266 (void);
// 0x0000012F System.Collections.IEnumerator AddItemComp::ToProduct(System.String,System.String,System.String,System.Boolean)
extern void AddItemComp_ToProduct_mC0687748A4043499BEEC4066AB0A9578BEF8AE87 (void);
// 0x00000130 System.Void AddItemComp::InitItem(UnityEngine.GameObject,System.String,System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Collections.Generic.List`1<System.String>)
extern void AddItemComp_InitItem_mE2CA196FA40E4EB2E4DADC83BC7E1AD0A8EFA93D (void);
// 0x00000131 System.Collections.IEnumerator AddItemComp::ImportImage(System.String,UnityEngine.UI.Button)
extern void AddItemComp_ImportImage_m193885C921D7C5A2ACD4867D1802C6551B6D375D (void);
// 0x00000132 System.Collections.IEnumerator AddItemComp::tmp()
extern void AddItemComp_tmp_m574D1139763D97D78638D7B17E389CC5E75A734D (void);
// 0x00000133 System.Void AddItemComp::MakeItem(System.String,System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,UnityEngine.GameObject,System.Collections.Generic.List`1<System.String>)
extern void AddItemComp_MakeItem_mBAFE3C5FE3E9CE73029DB9EBDCB504C9B3A07F3C (void);
// 0x00000134 System.Void AddItemComp::clearManager()
extern void AddItemComp_clearManager_mF27F2080DB2BB8371CDF6A3F2287BF1C77FA4A51 (void);
// 0x00000135 System.Void AddItemComp::filterlist(System.String)
extern void AddItemComp_filterlist_m84E3172E7E2EF77D1576A499BC395771B1DCF056 (void);
// 0x00000136 System.Single AddItemComp::rectcalc(System.Int32)
extern void AddItemComp_rectcalc_m7808BA0EE83F3CDD43DCE023BE2A625C185E9910 (void);
// 0x00000137 System.Void AddItemComp::.ctor()
extern void AddItemComp__ctor_mEAC8A40C02E96CE194488F987740DD60B45B727E (void);
// 0x00000138 System.Void AddItemComp::.cctor()
extern void AddItemComp__cctor_mA249536321F3E5E026EB6FA6CC1A9022A3BD8C46 (void);
// 0x00000139 System.Void AddItemComp::<Start>b__102_0(System.Int32)
extern void AddItemComp_U3CStartU3Eb__102_0_m2096C692CD9442FC46902A0F16DC4328A918C7D6 (void);
// 0x0000013A System.Void AddItemComp::<Start>b__102_1(System.Single)
extern void AddItemComp_U3CStartU3Eb__102_1_mC08704029D6F7E6D445DCC23E94F12A359980080 (void);
// 0x0000013B System.Void AddItemComp::<Start>b__102_3(System.Int32)
extern void AddItemComp_U3CStartU3Eb__102_3_mB54E039BC3C1F73EEA1FAB29614E7F3ECD241CC9 (void);
// 0x0000013C System.Void AddItemComp::<Start>b__102_4(System.Int32)
extern void AddItemComp_U3CStartU3Eb__102_4_m3E9597F4595EC78C0B9618C3F09A04B8BA85C040 (void);
// 0x0000013D System.Void AddItemComp::<Start>b__102_5()
extern void AddItemComp_U3CStartU3Eb__102_5_m38832F72F9C87F8CC9D1500BA5733AFE8A343B35 (void);
// 0x0000013E System.Void AddItemComp::<Start>b__102_6(System.Int32)
extern void AddItemComp_U3CStartU3Eb__102_6_m597E16370DAD0288AD7EED4CC61FB32F1C99F481 (void);
// 0x0000013F System.Void AddItemComp::<Start>b__102_7()
extern void AddItemComp_U3CStartU3Eb__102_7_m65B2483950F056B5C1287834E018A66BD1DD4B43 (void);
// 0x00000140 System.Void AddItemComp::<Start>b__102_8()
extern void AddItemComp_U3CStartU3Eb__102_8_mA016DA8AAEB55699B162DD1DF66FD216946D823F (void);
// 0x00000141 System.Void AddItemComp::<Start>b__102_9()
extern void AddItemComp_U3CStartU3Eb__102_9_mE809BBAC55ECD75FFA6A593D4143EFB84ED9C9E5 (void);
// 0x00000142 System.Boolean AddItemComp::<ButNextRoutine>b__104_0()
extern void AddItemComp_U3CButNextRoutineU3Eb__104_0_mBF7052D323E37B73886148348762818ACE5C002E (void);
// 0x00000143 System.Boolean AddItemComp::<ButHomeRoutine>b__106_0()
extern void AddItemComp_U3CButHomeRoutineU3Eb__106_0_m5C45AB4CC871CC952A74FDD92A52CC19E58F8072 (void);
// 0x00000144 System.Boolean AddItemComp::<ButSignOutRoutine>b__108_0()
extern void AddItemComp_U3CButSignOutRoutineU3Eb__108_0_mA7DC2D1B6D727E144DEAB10191F7F5E6F68A96A6 (void);
// 0x00000145 System.Boolean AddItemComp::<ButPrevRoutine>b__109_0()
extern void AddItemComp_U3CButPrevRoutineU3Eb__109_0_m5803B19502851521C955B2930AF40EB36847F124 (void);
// 0x00000146 System.Boolean AddItemComp::<FilRecRoutine>b__111_0()
extern void AddItemComp_U3CFilRecRoutineU3Eb__111_0_m0F1EBEF4BE8FA985651D2F375CBD989D0EEA42F3 (void);
// 0x00000147 System.Boolean AddItemComp::<FilTypeRoutine>b__112_0()
extern void AddItemComp_U3CFilTypeRoutineU3Eb__112_0_m99DB59CD2666C0D6462C28AFD55D09231AAC0741 (void);
// 0x00000148 System.Boolean AddItemComp::<FilSrcRoutine>b__113_0()
extern void AddItemComp_U3CFilSrcRoutineU3Eb__113_0_m30BA0187BE2962F46D35967E21A811E8E14F5922 (void);
// 0x00000149 System.Boolean AddItemComp::<FilUpdRoutine>b__114_0()
extern void AddItemComp_U3CFilUpdRoutineU3Eb__114_0_m5D40CD366F70EA64C5CF7F0FBF39CA0F11B29FCC (void);
// 0x0000014A System.Boolean AddItemComp::<StopDownloading>b__116_0()
extern void AddItemComp_U3CStopDownloadingU3Eb__116_0_mA97AEED6FC22F8068EB6F9021BCCD84D39A30E57 (void);
// 0x0000014B System.Boolean AddItemComp::<tmpScroll>b__121_0()
extern void AddItemComp_U3CtmpScrollU3Eb__121_0_m025E168AD6D3C68E5476FB87A5F4B7E902C67EC7 (void);
// 0x0000014C System.Void AddItemComp::<ImageAddingProcess>b__126_0()
extern void AddItemComp_U3CImageAddingProcessU3Eb__126_0_m440ADB857BC9E9AEAA14DBCF5C2B78C6FA04D3B5 (void);
// 0x0000014D System.Boolean AddItemComp::<ToProduct>b__134_0()
extern void AddItemComp_U3CToProductU3Eb__134_0_mE07CC29E524AD36543895E68382EF7AA4D07132A (void);
// 0x0000014E System.Void AddItemComp/OnReadyToReceive::.ctor(System.Object,System.IntPtr)
extern void OnReadyToReceive__ctor_mF3A576F94D54B15D64697B37BF25728303239D59 (void);
// 0x0000014F System.Void AddItemComp/OnReadyToReceive::Invoke(Firebase.Firestore.DocumentSnapshot)
extern void OnReadyToReceive_Invoke_m6EC5E059540BC5C88493C14066A7B4FEFA0EE300 (void);
// 0x00000150 System.IAsyncResult AddItemComp/OnReadyToReceive::BeginInvoke(Firebase.Firestore.DocumentSnapshot,System.AsyncCallback,System.Object)
extern void OnReadyToReceive_BeginInvoke_m20BB74F40726337268C02A96CA6539DBC2360DCB (void);
// 0x00000151 System.Void AddItemComp/OnReadyToReceive::EndInvoke(System.IAsyncResult)
extern void OnReadyToReceive_EndInvoke_m873E947BD439CD2FF2FC3BA03D0DA5F541E1AF8D (void);
// 0x00000152 System.Void AddItemComp/OnReadyToReceiveBatch::.ctor(System.Object,System.IntPtr)
extern void OnReadyToReceiveBatch__ctor_m00C1EA99DEDA8EB19DDDA0516A67359D6AD1D2D3 (void);
// 0x00000153 System.Void AddItemComp/OnReadyToReceiveBatch::Invoke(Firebase.Firestore.DocumentSnapshot,System.Int32)
extern void OnReadyToReceiveBatch_Invoke_mB38BEB55A0582959CE3F6024935165254F8A8784 (void);
// 0x00000154 System.IAsyncResult AddItemComp/OnReadyToReceiveBatch::BeginInvoke(Firebase.Firestore.DocumentSnapshot,System.Int32,System.AsyncCallback,System.Object)
extern void OnReadyToReceiveBatch_BeginInvoke_mFB82CE2E3DA974D75607AFE17BEA33C3FDD2F4EA (void);
// 0x00000155 System.Void AddItemComp/OnReadyToReceiveBatch::EndInvoke(System.IAsyncResult)
extern void OnReadyToReceiveBatch_EndInvoke_mF47E310BB5333982EFECB732BECF9E4C899F4BDE (void);
// 0x00000156 System.Void AddItemComp/OnCloseNav::.ctor(System.Object,System.IntPtr)
extern void OnCloseNav__ctor_m35F6534D1999A8BD8268527FD478407DCC5F3FD4 (void);
// 0x00000157 System.Void AddItemComp/OnCloseNav::Invoke()
extern void OnCloseNav_Invoke_mECB527F7FB2F00BD9627BF529DDDAF19187B4C86 (void);
// 0x00000158 System.IAsyncResult AddItemComp/OnCloseNav::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCloseNav_BeginInvoke_mBA336430E2A9B372EAA68D34221F5FDD807C9BBB (void);
// 0x00000159 System.Void AddItemComp/OnCloseNav::EndInvoke(System.IAsyncResult)
extern void OnCloseNav_EndInvoke_mA07F88C703000C6BC34BEC45178AFC80230602A2 (void);
// 0x0000015A System.Void AddItemComp/OnCloseFil::.ctor(System.Object,System.IntPtr)
extern void OnCloseFil__ctor_m1D73DB60FF63E9F8FB1B08CC5325EFBAA3A50E6B (void);
// 0x0000015B System.Void AddItemComp/OnCloseFil::Invoke()
extern void OnCloseFil_Invoke_mBBDE8B377170FAF6F5DF388A77E180CF3F266201 (void);
// 0x0000015C System.IAsyncResult AddItemComp/OnCloseFil::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCloseFil_BeginInvoke_m2CE499C0E0715876552AB71AE5A957FAF3DFE4C9 (void);
// 0x0000015D System.Void AddItemComp/OnCloseFil::EndInvoke(System.IAsyncResult)
extern void OnCloseFil_EndInvoke_mF8BA63B13E89747BA27A38F3D168A87CA89F7DCB (void);
// 0x0000015E System.Void AddItemComp/<>c::.cctor()
extern void U3CU3Ec__cctor_m583470F1CA6B150D1A783A354C2D9F14C653B89F (void);
// 0x0000015F System.Void AddItemComp/<>c::.ctor()
extern void U3CU3Ec__ctor_m845929B7233547C64476F81B0A950327499363C3 (void);
// 0x00000160 System.Void AddItemComp/<>c::<Start>b__102_2(System.Int32)
extern void U3CU3Ec_U3CStartU3Eb__102_2_m91173F44425DEE3A805AA673005927559D0D8327 (void);
// 0x00000161 System.Boolean AddItemComp/<>c::<FilRecRoutine>b__111_1()
extern void U3CU3Ec_U3CFilRecRoutineU3Eb__111_1_m216D843F76FC97380F7901E38F3AA443D14611A1 (void);
// 0x00000162 System.Void AddItemComp/<>c::<FilUpdRoutine>b__114_1(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CFilUpdRoutineU3Eb__114_1_m7E0311EEAFF9F13C99577F71BB1C38CA7D6AC1BD (void);
// 0x00000163 System.Void AddItemComp/<>c::<InitItem>b__135_4(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CInitItemU3Eb__135_4_m32A40B9D8EA231CFFD1DE26BB87B917868FA5FA1 (void);
// 0x00000164 System.Void AddItemComp/<>c::<InitItem>b__135_3(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CInitItemU3Eb__135_3_m74B2E2BB3C8AE4C43A338C61D6CB89EBAA8F23AD (void);
// 0x00000165 System.Void AddItemComp/<SetPrefs>d__103::.ctor(System.Int32)
extern void U3CSetPrefsU3Ed__103__ctor_m8FB32A3EA183CB8090C3DBB50B0109656692BBAA (void);
// 0x00000166 System.Void AddItemComp/<SetPrefs>d__103::System.IDisposable.Dispose()
extern void U3CSetPrefsU3Ed__103_System_IDisposable_Dispose_mB4BEB73E7A346D24D2DEF57F701A013AAC8B83EE (void);
// 0x00000167 System.Boolean AddItemComp/<SetPrefs>d__103::MoveNext()
extern void U3CSetPrefsU3Ed__103_MoveNext_mFE0F8D59C1D006153BD286BD482A35B1FA2177D1 (void);
// 0x00000168 System.Object AddItemComp/<SetPrefs>d__103::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetPrefsU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093F4D0EFF81DDC45B7624C209E883E5E07B08E4 (void);
// 0x00000169 System.Void AddItemComp/<SetPrefs>d__103::System.Collections.IEnumerator.Reset()
extern void U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_Reset_mB60350DCF81DA37ECE9FFA39FB691C9836E3E285 (void);
// 0x0000016A System.Object AddItemComp/<SetPrefs>d__103::System.Collections.IEnumerator.get_Current()
extern void U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_get_Current_mCDD4AC07D175D9271B089B5B72A7BAEC106B0F1F (void);
// 0x0000016B System.Void AddItemComp/<ButNextRoutine>d__104::.ctor(System.Int32)
extern void U3CButNextRoutineU3Ed__104__ctor_m0085365472595061903314EC1E4FFF79AC9C485F (void);
// 0x0000016C System.Void AddItemComp/<ButNextRoutine>d__104::System.IDisposable.Dispose()
extern void U3CButNextRoutineU3Ed__104_System_IDisposable_Dispose_m23377156CD91B1D2CFE09DE547413B94CD1B85E3 (void);
// 0x0000016D System.Boolean AddItemComp/<ButNextRoutine>d__104::MoveNext()
extern void U3CButNextRoutineU3Ed__104_MoveNext_m0CA753EBD36434BA824FF41C31ABE77B4A2E680D (void);
// 0x0000016E System.Object AddItemComp/<ButNextRoutine>d__104::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButNextRoutineU3Ed__104_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CC7DBC65BBF1EE481C3463FCC966E79EBAB87A2 (void);
// 0x0000016F System.Void AddItemComp/<ButNextRoutine>d__104::System.Collections.IEnumerator.Reset()
extern void U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_Reset_mDBC944F58A8A27B4EED103ACD698716C6F694C39 (void);
// 0x00000170 System.Object AddItemComp/<ButNextRoutine>d__104::System.Collections.IEnumerator.get_Current()
extern void U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_get_Current_m9EF10872F1E79554771AD85FBB5C87D3E006067B (void);
// 0x00000171 System.Void AddItemComp/<DelItemListWait>d__105::.ctor(System.Int32)
extern void U3CDelItemListWaitU3Ed__105__ctor_mE56DCC6198C2DA9F89186F244E620BE6A4832C24 (void);
// 0x00000172 System.Void AddItemComp/<DelItemListWait>d__105::System.IDisposable.Dispose()
extern void U3CDelItemListWaitU3Ed__105_System_IDisposable_Dispose_mAFF526E03E52DD0CF05DD04626EB5F6DA87EB364 (void);
// 0x00000173 System.Boolean AddItemComp/<DelItemListWait>d__105::MoveNext()
extern void U3CDelItemListWaitU3Ed__105_MoveNext_m389C02AFD8D8FE73EAD4E12066214CC536F8183F (void);
// 0x00000174 System.Object AddItemComp/<DelItemListWait>d__105::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelItemListWaitU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75D67143542BF51250E417F47999162F3082217D (void);
// 0x00000175 System.Void AddItemComp/<DelItemListWait>d__105::System.Collections.IEnumerator.Reset()
extern void U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_Reset_m10DD860E4ED3C27E7B607E6F22AC712A8DED3025 (void);
// 0x00000176 System.Object AddItemComp/<DelItemListWait>d__105::System.Collections.IEnumerator.get_Current()
extern void U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_get_Current_mCF63A193F61F5E9CAEE380812754088DF047E6DE (void);
// 0x00000177 System.Void AddItemComp/<ButHomeRoutine>d__106::.ctor(System.Int32)
extern void U3CButHomeRoutineU3Ed__106__ctor_mF0F347434E4D78C82D94BBCBC18072E688202C2B (void);
// 0x00000178 System.Void AddItemComp/<ButHomeRoutine>d__106::System.IDisposable.Dispose()
extern void U3CButHomeRoutineU3Ed__106_System_IDisposable_Dispose_m9B496850533015E4F608FC096B9B500AC23026F2 (void);
// 0x00000179 System.Boolean AddItemComp/<ButHomeRoutine>d__106::MoveNext()
extern void U3CButHomeRoutineU3Ed__106_MoveNext_mD76BAC02F8E6E099D361501E55F0EDC4D334887B (void);
// 0x0000017A System.Object AddItemComp/<ButHomeRoutine>d__106::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButHomeRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDEDE7FE05FB39A15955781EDD78073457E2CA83 (void);
// 0x0000017B System.Void AddItemComp/<ButHomeRoutine>d__106::System.Collections.IEnumerator.Reset()
extern void U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_Reset_m3BB033F0512E738AEA51BFFF949335B96A414367 (void);
// 0x0000017C System.Object AddItemComp/<ButHomeRoutine>d__106::System.Collections.IEnumerator.get_Current()
extern void U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m4FCD177E7D65FE61EA91F37BBA0049937F2B08BA (void);
// 0x0000017D System.Void AddItemComp/<ButSignOutRoutine>d__108::.ctor(System.Int32)
extern void U3CButSignOutRoutineU3Ed__108__ctor_mF79A9854726851E2C5773958F6873EBD682799D5 (void);
// 0x0000017E System.Void AddItemComp/<ButSignOutRoutine>d__108::System.IDisposable.Dispose()
extern void U3CButSignOutRoutineU3Ed__108_System_IDisposable_Dispose_m8C68B05A8AE1C61C64F9712905CC74FECD3E690D (void);
// 0x0000017F System.Boolean AddItemComp/<ButSignOutRoutine>d__108::MoveNext()
extern void U3CButSignOutRoutineU3Ed__108_MoveNext_mE5D8B4BF0ABFB884CA194B1DB5A19DA2CCDEDCA8 (void);
// 0x00000180 System.Object AddItemComp/<ButSignOutRoutine>d__108::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButSignOutRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m738D1C6B18D3CAA63D518C2B7C0D5DEE4FD8D948 (void);
// 0x00000181 System.Void AddItemComp/<ButSignOutRoutine>d__108::System.Collections.IEnumerator.Reset()
extern void U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_Reset_mB3CE8FBA1B3287E592432EDC4554176C8B3E6690 (void);
// 0x00000182 System.Object AddItemComp/<ButSignOutRoutine>d__108::System.Collections.IEnumerator.get_Current()
extern void U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_mEE50C74269A8013E6987EB9EE0F4249E263BDE85 (void);
// 0x00000183 System.Void AddItemComp/<ButPrevRoutine>d__109::.ctor(System.Int32)
extern void U3CButPrevRoutineU3Ed__109__ctor_m4A20BECF42F8B7F4C026E469599A2F8860953006 (void);
// 0x00000184 System.Void AddItemComp/<ButPrevRoutine>d__109::System.IDisposable.Dispose()
extern void U3CButPrevRoutineU3Ed__109_System_IDisposable_Dispose_mB8627B57D8EDF2A14E07394DE8C70C5C8C559B13 (void);
// 0x00000185 System.Boolean AddItemComp/<ButPrevRoutine>d__109::MoveNext()
extern void U3CButPrevRoutineU3Ed__109_MoveNext_m13B609F386F42BBE73F68A4B5820C6BFDFA77636 (void);
// 0x00000186 System.Object AddItemComp/<ButPrevRoutine>d__109::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButPrevRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFCB870E8A5E90D88232266CDBB7CE407C60B363 (void);
// 0x00000187 System.Void AddItemComp/<ButPrevRoutine>d__109::System.Collections.IEnumerator.Reset()
extern void U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m521533485C83DBEC45F0E4E4609BE8269DEA0434 (void);
// 0x00000188 System.Object AddItemComp/<ButPrevRoutine>d__109::System.Collections.IEnumerator.get_Current()
extern void U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_m9799E1C63B54F2CD96262A73C227B1F136A9D3D5 (void);
// 0x00000189 System.Void AddItemComp/<FilRecRoutine>d__111::.ctor(System.Int32)
extern void U3CFilRecRoutineU3Ed__111__ctor_m9F1A768D3DD7CED2C75103495721D60FDA5281DA (void);
// 0x0000018A System.Void AddItemComp/<FilRecRoutine>d__111::System.IDisposable.Dispose()
extern void U3CFilRecRoutineU3Ed__111_System_IDisposable_Dispose_mE9A6542D040200FB09383043C06B14A0C59EFFCD (void);
// 0x0000018B System.Boolean AddItemComp/<FilRecRoutine>d__111::MoveNext()
extern void U3CFilRecRoutineU3Ed__111_MoveNext_mD5B25DCC95CC845DCE1950796C70AD520DFFF718 (void);
// 0x0000018C System.Object AddItemComp/<FilRecRoutine>d__111::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFilRecRoutineU3Ed__111_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2C803E973C3986774B9663A022FB34250DB0C2F (void);
// 0x0000018D System.Void AddItemComp/<FilRecRoutine>d__111::System.Collections.IEnumerator.Reset()
extern void U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_Reset_m8C8316FC76E78AD0D70ABDF37C157B0874E4B59A (void);
// 0x0000018E System.Object AddItemComp/<FilRecRoutine>d__111::System.Collections.IEnumerator.get_Current()
extern void U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_get_Current_mCA6F124A8453688732770F731E9794591A34EE0F (void);
// 0x0000018F System.Void AddItemComp/<FilTypeRoutine>d__112::.ctor(System.Int32)
extern void U3CFilTypeRoutineU3Ed__112__ctor_m3541807D0A5921CF9362E45C07E1B78D1F5628D5 (void);
// 0x00000190 System.Void AddItemComp/<FilTypeRoutine>d__112::System.IDisposable.Dispose()
extern void U3CFilTypeRoutineU3Ed__112_System_IDisposable_Dispose_m7BB15FC367573484AAAC7342C6F8E7C805787EB7 (void);
// 0x00000191 System.Boolean AddItemComp/<FilTypeRoutine>d__112::MoveNext()
extern void U3CFilTypeRoutineU3Ed__112_MoveNext_mAEFC1ABC86F3BBAE9138A8564976A86F6B185C99 (void);
// 0x00000192 System.Object AddItemComp/<FilTypeRoutine>d__112::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFilTypeRoutineU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD81030DA4DF6EABEF2C3440166062FBFDC1EC41E (void);
// 0x00000193 System.Void AddItemComp/<FilTypeRoutine>d__112::System.Collections.IEnumerator.Reset()
extern void U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_Reset_m01D8D7C4430B1CB6B9D833F137FCB920DA0C41C5 (void);
// 0x00000194 System.Object AddItemComp/<FilTypeRoutine>d__112::System.Collections.IEnumerator.get_Current()
extern void U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_get_Current_m1CED9C1E2ABEF0F14A0E4EFDE9B9E89C79B32D67 (void);
// 0x00000195 System.Void AddItemComp/<FilSrcRoutine>d__113::.ctor(System.Int32)
extern void U3CFilSrcRoutineU3Ed__113__ctor_m4425196A106EDB43B1640CE7D4F9F6769C0A1CA7 (void);
// 0x00000196 System.Void AddItemComp/<FilSrcRoutine>d__113::System.IDisposable.Dispose()
extern void U3CFilSrcRoutineU3Ed__113_System_IDisposable_Dispose_m36F97F0CD2C466182E98033B9D28AAB0BB634CE2 (void);
// 0x00000197 System.Boolean AddItemComp/<FilSrcRoutine>d__113::MoveNext()
extern void U3CFilSrcRoutineU3Ed__113_MoveNext_mEFF05ED821D7AC181DC13B375212EE2789450FBB (void);
// 0x00000198 System.Object AddItemComp/<FilSrcRoutine>d__113::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFilSrcRoutineU3Ed__113_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4503567FA847BD7C5351F587C42DC420F4C1417E (void);
// 0x00000199 System.Void AddItemComp/<FilSrcRoutine>d__113::System.Collections.IEnumerator.Reset()
extern void U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_Reset_m570D349FC4ACAF60F6A794BA378A3E009A92290D (void);
// 0x0000019A System.Object AddItemComp/<FilSrcRoutine>d__113::System.Collections.IEnumerator.get_Current()
extern void U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_get_Current_m5FD53CA1FDE0E65DB09E071A5AC660772BDD5AA8 (void);
// 0x0000019B System.Void AddItemComp/<>c__DisplayClass114_0::.ctor()
extern void U3CU3Ec__DisplayClass114_0__ctor_mC265B3C8289A919A3AA1A4389CD06BEC1385B1EA (void);
// 0x0000019C System.Boolean AddItemComp/<>c__DisplayClass114_0::<FilUpdRoutine>b__2()
extern void U3CU3Ec__DisplayClass114_0_U3CFilUpdRoutineU3Eb__2_m8ADC6A1CBAA29A4B3FD4BCCB4B2EFA921B8B6C4B (void);
// 0x0000019D System.Void AddItemComp/<FilUpdRoutine>d__114::.ctor(System.Int32)
extern void U3CFilUpdRoutineU3Ed__114__ctor_m7E9325EBC3194C561289526D7988BFC13917F6BC (void);
// 0x0000019E System.Void AddItemComp/<FilUpdRoutine>d__114::System.IDisposable.Dispose()
extern void U3CFilUpdRoutineU3Ed__114_System_IDisposable_Dispose_mF09C79B7A5344B289C5788B9EEA124E6960B4FA6 (void);
// 0x0000019F System.Boolean AddItemComp/<FilUpdRoutine>d__114::MoveNext()
extern void U3CFilUpdRoutineU3Ed__114_MoveNext_mDAF940D785828D3150C3F3077C0CC26B42723323 (void);
// 0x000001A0 System.Object AddItemComp/<FilUpdRoutine>d__114::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFilUpdRoutineU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D53B015E411EAB5F586060E9184710FC2B3CEBC (void);
// 0x000001A1 System.Void AddItemComp/<FilUpdRoutine>d__114::System.Collections.IEnumerator.Reset()
extern void U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_Reset_m4A15596167AA539664B1C694F372712A981963CF (void);
// 0x000001A2 System.Object AddItemComp/<FilUpdRoutine>d__114::System.Collections.IEnumerator.get_Current()
extern void U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_get_Current_mC7DDE1817B9D41C593B61E33C0F6E76DE1E213FC (void);
// 0x000001A3 System.Void AddItemComp/<StopDownloading>d__116::.ctor(System.Int32)
extern void U3CStopDownloadingU3Ed__116__ctor_mB8BC0F77581ED0D22C2DC3707858178F36FA1E1C (void);
// 0x000001A4 System.Void AddItemComp/<StopDownloading>d__116::System.IDisposable.Dispose()
extern void U3CStopDownloadingU3Ed__116_System_IDisposable_Dispose_m9A2B019169627DC744778F6A581A1341FBBBF5BB (void);
// 0x000001A5 System.Boolean AddItemComp/<StopDownloading>d__116::MoveNext()
extern void U3CStopDownloadingU3Ed__116_MoveNext_mFFED1CCD93396F904CE1644D742E1FD063873A94 (void);
// 0x000001A6 System.Object AddItemComp/<StopDownloading>d__116::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopDownloadingU3Ed__116_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFAF6C8391D7EF16F8A34DA187B9F19859F769A (void);
// 0x000001A7 System.Void AddItemComp/<StopDownloading>d__116::System.Collections.IEnumerator.Reset()
extern void U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_Reset_mC9B5D5C32A29DA3C1F6822956056ADC748174746 (void);
// 0x000001A8 System.Object AddItemComp/<StopDownloading>d__116::System.Collections.IEnumerator.get_Current()
extern void U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_get_Current_mB8DD0110D702CB79F468A1D31EDE5C4DBA965AEA (void);
// 0x000001A9 System.Void AddItemComp/<ResendRepeat>d__118::.ctor(System.Int32)
extern void U3CResendRepeatU3Ed__118__ctor_mFBD5278226634B801677A62FE848BDDBC9D38DA9 (void);
// 0x000001AA System.Void AddItemComp/<ResendRepeat>d__118::System.IDisposable.Dispose()
extern void U3CResendRepeatU3Ed__118_System_IDisposable_Dispose_m5C96DFF10E9036353B869A60C5B1DC6C77103A8C (void);
// 0x000001AB System.Boolean AddItemComp/<ResendRepeat>d__118::MoveNext()
extern void U3CResendRepeatU3Ed__118_MoveNext_m29F74973751C2D1DA955DB9ED0EC5AEC0BB47A41 (void);
// 0x000001AC System.Object AddItemComp/<ResendRepeat>d__118::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResendRepeatU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D4886779C7D6108537AFC05D1F84B5960FB557B (void);
// 0x000001AD System.Void AddItemComp/<ResendRepeat>d__118::System.Collections.IEnumerator.Reset()
extern void U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_Reset_m005EF473A7FE5497F43D0746E0ECCC6C416EB77D (void);
// 0x000001AE System.Object AddItemComp/<ResendRepeat>d__118::System.Collections.IEnumerator.get_Current()
extern void U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_get_Current_m23FB6E14BEDD84646381D9C2756B6A1DD077C340 (void);
// 0x000001AF System.Void AddItemComp/<tmpScroll>d__121::.ctor(System.Int32)
extern void U3CtmpScrollU3Ed__121__ctor_mC57CFA92A60222C607BBFE9DD9070ECE9FC02308 (void);
// 0x000001B0 System.Void AddItemComp/<tmpScroll>d__121::System.IDisposable.Dispose()
extern void U3CtmpScrollU3Ed__121_System_IDisposable_Dispose_m058B5042FFD6D072E890FD42FDC4685BF9D90978 (void);
// 0x000001B1 System.Boolean AddItemComp/<tmpScroll>d__121::MoveNext()
extern void U3CtmpScrollU3Ed__121_MoveNext_mA4FCA7D434AE4CA87A5DCCC7024D046230359304 (void);
// 0x000001B2 System.Object AddItemComp/<tmpScroll>d__121::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtmpScrollU3Ed__121_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14121AE1F25F2547E709F627B873F6732220D5AA (void);
// 0x000001B3 System.Void AddItemComp/<tmpScroll>d__121::System.Collections.IEnumerator.Reset()
extern void U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_Reset_m8553DC27B6409578E06916683E3CEC4170FDAAB0 (void);
// 0x000001B4 System.Object AddItemComp/<tmpScroll>d__121::System.Collections.IEnumerator.get_Current()
extern void U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_get_Current_m1CCB279ED7CB5FD1F214F85541F1BE979D762957 (void);
// 0x000001B5 System.Void AddItemComp/<BeginGarmentCall>d__122::.ctor(System.Int32)
extern void U3CBeginGarmentCallU3Ed__122__ctor_mF1D34C8ED14D2838ACB9179BFF9B8E25060E347E (void);
// 0x000001B6 System.Void AddItemComp/<BeginGarmentCall>d__122::System.IDisposable.Dispose()
extern void U3CBeginGarmentCallU3Ed__122_System_IDisposable_Dispose_mC2A7529A5F4F8E37EF2543B0160108DAB4BE5EED (void);
// 0x000001B7 System.Boolean AddItemComp/<BeginGarmentCall>d__122::MoveNext()
extern void U3CBeginGarmentCallU3Ed__122_MoveNext_mE2E2CBD84E4257A5F907E93274DD9149C1780346 (void);
// 0x000001B8 System.Object AddItemComp/<BeginGarmentCall>d__122::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBeginGarmentCallU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC18C03DECED7724F0A8C879A81A799DE616B8884 (void);
// 0x000001B9 System.Void AddItemComp/<BeginGarmentCall>d__122::System.Collections.IEnumerator.Reset()
extern void U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_Reset_m33B50615A9C70037B8F1BFAB67DAB62019C6611C (void);
// 0x000001BA System.Object AddItemComp/<BeginGarmentCall>d__122::System.Collections.IEnumerator.get_Current()
extern void U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_get_Current_mD4DB387972B3BB6B64DE01FDBF515D72DDFCD398 (void);
// 0x000001BB System.Void AddItemComp/<ImageTexHandler>d__125::.ctor(System.Int32)
extern void U3CImageTexHandlerU3Ed__125__ctor_m4DC549290A14DEE1FD6853C90B3D95424FE0BA2A (void);
// 0x000001BC System.Void AddItemComp/<ImageTexHandler>d__125::System.IDisposable.Dispose()
extern void U3CImageTexHandlerU3Ed__125_System_IDisposable_Dispose_mB1A2DA2D622318EF7C7882E054DA421CE22421EC (void);
// 0x000001BD System.Boolean AddItemComp/<ImageTexHandler>d__125::MoveNext()
extern void U3CImageTexHandlerU3Ed__125_MoveNext_mE79793F16545C99A58F5E21E1A692A9152904E48 (void);
// 0x000001BE System.Void AddItemComp/<ImageTexHandler>d__125::<>m__Finally1()
extern void U3CImageTexHandlerU3Ed__125_U3CU3Em__Finally1_m83D596986D462FA9C5450C18E0D5B41532773466 (void);
// 0x000001BF System.Object AddItemComp/<ImageTexHandler>d__125::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageTexHandlerU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54E7FF932D9D77128ED4C58634C42ADBF5010C96 (void);
// 0x000001C0 System.Void AddItemComp/<ImageTexHandler>d__125::System.Collections.IEnumerator.Reset()
extern void U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_Reset_mCF9361949B92FA15B21927BD9391E205FBC8A0F6 (void);
// 0x000001C1 System.Object AddItemComp/<ImageTexHandler>d__125::System.Collections.IEnumerator.get_Current()
extern void U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_get_Current_m4FE98BC68F8422625EC9F9C5B37277F30787E340 (void);
// 0x000001C2 System.Void AddItemComp/<ImageAddingProcess>d__126::.ctor(System.Int32)
extern void U3CImageAddingProcessU3Ed__126__ctor_m8FA7206E28AE6338E7F743FD336B4F1EA75A6124 (void);
// 0x000001C3 System.Void AddItemComp/<ImageAddingProcess>d__126::System.IDisposable.Dispose()
extern void U3CImageAddingProcessU3Ed__126_System_IDisposable_Dispose_m49B8A7C618AE71A995D3411AC554ACD5F2232700 (void);
// 0x000001C4 System.Boolean AddItemComp/<ImageAddingProcess>d__126::MoveNext()
extern void U3CImageAddingProcessU3Ed__126_MoveNext_m125B68955309B84358A3FFB4A0B5ED4DDF98EC75 (void);
// 0x000001C5 System.Void AddItemComp/<ImageAddingProcess>d__126::<>m__Finally1()
extern void U3CImageAddingProcessU3Ed__126_U3CU3Em__Finally1_mA9C9EEF03D1989C147C2B1FB2113BB412F86F093 (void);
// 0x000001C6 System.Object AddItemComp/<ImageAddingProcess>d__126::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageAddingProcessU3Ed__126_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC465F56FE788453CE681703A3E164BF809528187 (void);
// 0x000001C7 System.Void AddItemComp/<ImageAddingProcess>d__126::System.Collections.IEnumerator.Reset()
extern void U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_Reset_m0377E1B1D0E57425A4B765F434124AA99B952FF8 (void);
// 0x000001C8 System.Object AddItemComp/<ImageAddingProcess>d__126::System.Collections.IEnumerator.get_Current()
extern void U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_get_Current_m8444CD45133F500A05CCD3393B8594E45FBB4241 (void);
// 0x000001C9 System.Void AddItemComp/<>c__DisplayClass128_0::.ctor()
extern void U3CU3Ec__DisplayClass128_0__ctor_mEE8BCEB6FC5CB5B7C3E8D75ABA3DCA5722190F9B (void);
// 0x000001CA System.Void AddItemComp/<>c__DisplayClass128_0::<FbaseQueries>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass128_0_U3CFbaseQueriesU3Eb__0_m0E40C5FB38920F733F5D0AA17DF1B19148ECBD9C (void);
// 0x000001CB System.Boolean AddItemComp/<>c__DisplayClass128_0::<FbaseQueries>b__1()
extern void U3CU3Ec__DisplayClass128_0_U3CFbaseQueriesU3Eb__1_mE27DC9579501583B41963D59B5A49E077845DEFD (void);
// 0x000001CC System.Void AddItemComp/<FbaseQueries>d__128::.ctor(System.Int32)
extern void U3CFbaseQueriesU3Ed__128__ctor_m25DB5088A39499E23E6A2A1196C7B42564F860F3 (void);
// 0x000001CD System.Void AddItemComp/<FbaseQueries>d__128::System.IDisposable.Dispose()
extern void U3CFbaseQueriesU3Ed__128_System_IDisposable_Dispose_m6D6B952EC69EAE27B82D5C14AE57A6B4B87A01B5 (void);
// 0x000001CE System.Boolean AddItemComp/<FbaseQueries>d__128::MoveNext()
extern void U3CFbaseQueriesU3Ed__128_MoveNext_m9EA4677B133AE5DA0CEDAC31152FDBD1AB6D0AD6 (void);
// 0x000001CF System.Object AddItemComp/<FbaseQueries>d__128::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFbaseQueriesU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7808CAD7B133848AAF612E1D96BF5F2E0E492AF9 (void);
// 0x000001D0 System.Void AddItemComp/<FbaseQueries>d__128::System.Collections.IEnumerator.Reset()
extern void U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_Reset_m78FBE24F99396A51AB662A48447758AB67DE39A6 (void);
// 0x000001D1 System.Object AddItemComp/<FbaseQueries>d__128::System.Collections.IEnumerator.get_Current()
extern void U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_get_Current_mA5F4F972647F1DC794480D6E4908D4F4550E20F7 (void);
// 0x000001D2 System.Void AddItemComp/<ToProduct>d__134::.ctor(System.Int32)
extern void U3CToProductU3Ed__134__ctor_m9900F913A83872687C721DCB5D9B714B700A1079 (void);
// 0x000001D3 System.Void AddItemComp/<ToProduct>d__134::System.IDisposable.Dispose()
extern void U3CToProductU3Ed__134_System_IDisposable_Dispose_m397680E3FE53C7724B8685B74462B56C920316F3 (void);
// 0x000001D4 System.Boolean AddItemComp/<ToProduct>d__134::MoveNext()
extern void U3CToProductU3Ed__134_MoveNext_mCB9E22E732BC00D543B7DFF6E510F4087AA06526 (void);
// 0x000001D5 System.Object AddItemComp/<ToProduct>d__134::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CToProductU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m246861321A3C383AABF3B464F205A15A62B12B00 (void);
// 0x000001D6 System.Void AddItemComp/<ToProduct>d__134::System.Collections.IEnumerator.Reset()
extern void U3CToProductU3Ed__134_System_Collections_IEnumerator_Reset_mBB707E3B20E4A99F1327035DA657A128F51AB0CF (void);
// 0x000001D7 System.Object AddItemComp/<ToProduct>d__134::System.Collections.IEnumerator.get_Current()
extern void U3CToProductU3Ed__134_System_Collections_IEnumerator_get_Current_mA0865F6EACB8D266C253B56FFFF15B13AD085323 (void);
// 0x000001D8 System.Void AddItemComp/<>c__DisplayClass135_0::.ctor()
extern void U3CU3Ec__DisplayClass135_0__ctor_m7A11B18C3DEE1E41289EB635D87F47EC67F3E2D5 (void);
// 0x000001D9 System.Boolean AddItemComp/<>c__DisplayClass135_0::<InitItem>b__0(System.String)
extern void U3CU3Ec__DisplayClass135_0_U3CInitItemU3Eb__0_mB4957E07DFDC12C7299A72F209A06CB4FE2B6692 (void);
// 0x000001DA System.Void AddItemComp/<>c__DisplayClass135_0::<InitItem>b__1()
extern void U3CU3Ec__DisplayClass135_0_U3CInitItemU3Eb__1_m448D902C83B9071956EEEBAE636C66DCBD5DFDBD (void);
// 0x000001DB System.Void AddItemComp/<>c__DisplayClass135_0::<InitItem>b__2()
extern void U3CU3Ec__DisplayClass135_0_U3CInitItemU3Eb__2_mE0C7A6459A1661A0768A40CF27C5B8E73C9133AA (void);
// 0x000001DC System.Void AddItemComp/<>c__DisplayClass136_0::.ctor()
extern void U3CU3Ec__DisplayClass136_0__ctor_m6C6649C0EE977BC5F35B1E1BEBF1A85C08E86607 (void);
// 0x000001DD System.Void AddItemComp/<>c__DisplayClass136_0::<ImportImage>b__0()
extern void U3CU3Ec__DisplayClass136_0_U3CImportImageU3Eb__0_mEAEFB4E0782953DA9861D5C4F0829E5FA8357D6D (void);
// 0x000001DE System.Boolean AddItemComp/<>c__DisplayClass136_0::<ImportImage>b__1()
extern void U3CU3Ec__DisplayClass136_0_U3CImportImageU3Eb__1_m8AC02C432ECD8A736777DF3E14890CEFAA0C5FD7 (void);
// 0x000001DF System.Void AddItemComp/<ImportImage>d__136::.ctor(System.Int32)
extern void U3CImportImageU3Ed__136__ctor_m18CB1E44865D2185DC631E553BBE9CB761AD5195 (void);
// 0x000001E0 System.Void AddItemComp/<ImportImage>d__136::System.IDisposable.Dispose()
extern void U3CImportImageU3Ed__136_System_IDisposable_Dispose_m9BBAB55890CF9448000562B794994367EAFD12E8 (void);
// 0x000001E1 System.Boolean AddItemComp/<ImportImage>d__136::MoveNext()
extern void U3CImportImageU3Ed__136_MoveNext_m35708D5974A45371818D53323B63F1FDA0A1E600 (void);
// 0x000001E2 System.Object AddItemComp/<ImportImage>d__136::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImportImageU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43399C0073CD2064ABA95AC6724AA31CCDA971D2 (void);
// 0x000001E3 System.Void AddItemComp/<ImportImage>d__136::System.Collections.IEnumerator.Reset()
extern void U3CImportImageU3Ed__136_System_Collections_IEnumerator_Reset_m8B0A961330D62896C46FA5268C1506A17F5F32CD (void);
// 0x000001E4 System.Object AddItemComp/<ImportImage>d__136::System.Collections.IEnumerator.get_Current()
extern void U3CImportImageU3Ed__136_System_Collections_IEnumerator_get_Current_mDAD3A362FA8B96F11901C0389A2E0F1AE02C5D26 (void);
// 0x000001E5 System.Void AddItemComp/<tmp>d__137::.ctor(System.Int32)
extern void U3CtmpU3Ed__137__ctor_m64A892AAFE35BB0DB81EBC891907BAD699F14A11 (void);
// 0x000001E6 System.Void AddItemComp/<tmp>d__137::System.IDisposable.Dispose()
extern void U3CtmpU3Ed__137_System_IDisposable_Dispose_m038DB0E832F38E7D0A7438FBFD21621EE4D66A9F (void);
// 0x000001E7 System.Boolean AddItemComp/<tmp>d__137::MoveNext()
extern void U3CtmpU3Ed__137_MoveNext_m575DE591C2B0E4D9109362A4BD00CD5F90BD1F58 (void);
// 0x000001E8 System.Object AddItemComp/<tmp>d__137::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtmpU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FECF7697F97244EE9AC8637AA540E38264CF7FF (void);
// 0x000001E9 System.Void AddItemComp/<tmp>d__137::System.Collections.IEnumerator.Reset()
extern void U3CtmpU3Ed__137_System_Collections_IEnumerator_Reset_m8771E3D830378C9D1BD0A73DB43CA451AA8718CD (void);
// 0x000001EA System.Object AddItemComp/<tmp>d__137::System.Collections.IEnumerator.get_Current()
extern void U3CtmpU3Ed__137_System_Collections_IEnumerator_get_Current_m7FC964576C9960A08657AB667982AC5291A27D51 (void);
// 0x000001EB System.Void AddItemFav::Start()
extern void AddItemFav_Start_m22EB63A4BB574A0F65F624BB93C171FBAC5C0C65 (void);
// 0x000001EC System.Void AddItemFav::Update()
extern void AddItemFav_Update_m8FAB638DFB4FCD4B633D22F644552AB3049A2534 (void);
// 0x000001ED System.Void AddItemFav::Awake()
extern void AddItemFav_Awake_m7A464B617259F8C2A4A65959323B0CA4FAC3EA54 (void);
// 0x000001EE System.Void AddItemFav::Initpop()
extern void AddItemFav_Initpop_m7D6D7D6524CBD5A289DFCEBAE31E8E2E3EC91062 (void);
// 0x000001EF System.Void AddItemFav::MakeItem(System.String)
extern void AddItemFav_MakeItem_m5DC8CC01221D1884BF524D7604B68BF667E3223B (void);
// 0x000001F0 System.Void AddItemFav::InitItem(UnityEngine.GameObject,System.String)
extern void AddItemFav_InitItem_mC97C379AA61BE42FC3C5624675C5B870839350A1 (void);
// 0x000001F1 System.Void AddItemFav::.ctor()
extern void AddItemFav__ctor_m72EE683B90BCF8352BF8308293E98F558FCDBDCC (void);
// 0x000001F2 System.Void AddItemFav/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m8140DC239D6578274C15E3B32239EE9B50B58626 (void);
// 0x000001F3 System.Void AddItemFav/<>c__DisplayClass19_0::<InitItem>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CInitItemU3Eb__0_m1EC3CF5C1F017591611B1691ECA7EE5D262B6D8C (void);
// 0x000001F4 System.Void AddItemFav/<>c__DisplayClass19_0::<InitItem>b__1()
extern void U3CU3Ec__DisplayClass19_0_U3CInitItemU3Eb__1_m19276384489C4DD6C4752C783469686CF1F1CBFC (void);
// 0x000001F5 System.Void AddItemFavB::Start()
extern void AddItemFavB_Start_m30BA9031B6C52DAA3F976BA8C1F0F498A10D98B9 (void);
// 0x000001F6 System.Collections.IEnumerator AddItemFavB::SetPrefs(System.Int32,System.Int32)
extern void AddItemFavB_SetPrefs_mB24E9E1899BEF44D6C33A153F63B240862831A89 (void);
// 0x000001F7 System.Collections.IEnumerator AddItemFavB::ButNextRoutine()
extern void AddItemFavB_ButNextRoutine_mC8332E00005A39E745D65C9ABCE09820CDED8AF2 (void);
// 0x000001F8 System.Collections.IEnumerator AddItemFavB::DelItemListWait(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Texture2D>)
extern void AddItemFavB_DelItemListWait_m34607DECD00D5A8203DD0C9B94A136F0D8C07A43 (void);
// 0x000001F9 System.Collections.IEnumerator AddItemFavB::ButHomeRoutine()
extern void AddItemFavB_ButHomeRoutine_mBDA4FBC56EFB11901F98BF56D39FD62DF1BF5DB9 (void);
// 0x000001FA System.Void AddItemFavB::ManagerReset()
extern void AddItemFavB_ManagerReset_m518ADAC3030F842B494901104A78D75FEEC6E062 (void);
// 0x000001FB System.Collections.IEnumerator AddItemFavB::ButSignOutRoutine()
extern void AddItemFavB_ButSignOutRoutine_mDB66FA14184594255315C663B57AEDC770551A90 (void);
// 0x000001FC System.Collections.IEnumerator AddItemFavB::ButPrevRoutine()
extern void AddItemFavB_ButPrevRoutine_m179C7214D4B09084473108EF4B89F611FFC104B8 (void);
// 0x000001FD System.Boolean AddItemFavB::IsFinishedDownloading()
extern void AddItemFavB_IsFinishedDownloading_m06E39FC6BFB1E80640132961728031376F4E8D4B (void);
// 0x000001FE System.Collections.IEnumerator AddItemFavB::FilSrcRoutine(System.Int32,System.Int32)
extern void AddItemFavB_FilSrcRoutine_m7F3CFD187CE0378C12B5016ED5E472362C740472 (void);
// 0x000001FF System.Collections.IEnumerator AddItemFavB::FilUpdRoutine(System.Int32,System.Int32)
extern void AddItemFavB_FilUpdRoutine_mAB5BA98A6452BA0212A652CFFC4BFB301782F528 (void);
// 0x00000200 System.Void AddItemFavB::checkButtons(System.Int32)
extern void AddItemFavB_checkButtons_m050B20A2818F1A9D7C56A4C2E02F0586D00270FF (void);
// 0x00000201 System.Void AddItemFavB::OnDisable()
extern void AddItemFavB_OnDisable_m5D0489A9423F098B5D20CAE83F544BC12AEBE17D (void);
// 0x00000202 System.Collections.IEnumerator AddItemFavB::StopDownloading(System.String)
extern void AddItemFavB_StopDownloading_m3811835D137C6DF0D3E289687A00193434FF42ED (void);
// 0x00000203 System.Void AddItemFavB::Update()
extern void AddItemFavB_Update_mAE6E83DA4BA2CBE328BA448DDD7C9C17F4B8DA8C (void);
// 0x00000204 System.Collections.IEnumerator AddItemFavB::ResendRepeat()
extern void AddItemFavB_ResendRepeat_m33ACCF6A21E700039301FDCB1F057F8DC0E646B3 (void);
// 0x00000205 System.Void AddItemFavB::ResetMan()
extern void AddItemFavB_ResetMan_m77535E95658CFD87308B1229EB656F72C8F507F3 (void);
// 0x00000206 System.Void AddItemFavB::scrollrectCallBack(UnityEngine.Vector2)
extern void AddItemFavB_scrollrectCallBack_mC113B0C6941FD75B3FDB7325E33D36B172F4CFB4 (void);
// 0x00000207 System.Collections.IEnumerator AddItemFavB::tmpScroll()
extern void AddItemFavB_tmpScroll_m7C77D6633C3C6234DCEA3B50B9ADAB6145D6D743 (void);
// 0x00000208 System.Collections.IEnumerator AddItemFavB::BeginGarmentCall()
extern void AddItemFavB_BeginGarmentCall_mFD0B654FDB658DCFA40C758EADA73B06943ADF53 (void);
// 0x00000209 System.Void AddItemFavB::showGarms(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Collections.Generic.List`1<System.String>)
extern void AddItemFavB_showGarms_mE0E7654AEF7B4250CEDD1157ABB6A3AE8716FB13 (void);
// 0x0000020A System.Void AddItemFavB::AddImages(System.Collections.Generic.List`1<System.String>)
extern void AddItemFavB_AddImages_mB021CB8F15D14E6263F31924E8457A15910962C1 (void);
// 0x0000020B System.Void AddItemFavB::ToggleButtons(System.Boolean)
extern void AddItemFavB_ToggleButtons_m7E924CF0F1FC1782D62FCF4794FEFC27292D6858 (void);
// 0x0000020C System.Collections.IEnumerator AddItemFavB::ImageTexHandler(System.String,System.String,System.Int32)
extern void AddItemFavB_ImageTexHandler_m6CC6A14F463CAA419779D706DCE0EF37BAA578DF (void);
// 0x0000020D System.Collections.IEnumerator AddItemFavB::ImageAddingProcess(System.Collections.Generic.List`1<System.String>)
extern void AddItemFavB_ImageAddingProcess_m15B9C3BB4D6B081000AB2D595049CA71D5B32FFB (void);
// 0x0000020E UnityEngine.Texture2D AddItemFavB::ScaleTexture(UnityEngine.Texture2D,System.Int32,System.Int32)
extern void AddItemFavB_ScaleTexture_mD3CAB23F54712E100F862E0FB36031E975029054 (void);
// 0x0000020F System.Collections.IEnumerator AddItemFavB::FbaseQueries()
extern void AddItemFavB_FbaseQueries_m4D8C6B4A237936B4ED4475DC22E360BB5364D55F (void);
// 0x00000210 System.Void AddItemFavB::LayoutFix(System.Int32,UnityEngine.GameObject)
extern void AddItemFavB_LayoutFix_m2E7B286217AE30C7881433ED82ABCA883914AD8F (void);
// 0x00000211 System.Void AddItemFavB::ToggleLoading(System.Boolean)
extern void AddItemFavB_ToggleLoading_m4781BB2B35354DE33C8EF0848B10CB5500D57F5E (void);
// 0x00000212 System.Collections.IEnumerator AddItemFavB::ToProduct(System.String,System.String,System.String)
extern void AddItemFavB_ToProduct_mA72463E586F425D9C1E93F6C8FAAEF78CC08D407 (void);
// 0x00000213 System.Void AddItemFavB::MakeGarms(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Collections.Generic.List`1<System.String>,UnityEngine.GameObject,System.Int32)
extern void AddItemFavB_MakeGarms_m20D2E830759AEEE45756DA478500F246CAB55334 (void);
// 0x00000214 System.Void AddItemFavB::AddGarms(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Int32,System.Int32,System.Collections.Generic.List`1<System.String>,Firebase.Firestore.DocumentSnapshot,System.Boolean,System.Collections.Generic.List`1<System.String>)
extern void AddItemFavB_AddGarms_mE48544A93022326A75B151244AA0E4DFDB96A97F (void);
// 0x00000215 System.Void AddItemFavB::UpdateGarms()
extern void AddItemFavB_UpdateGarms_m97B0E574EA1F8173BF7B1E5992FB3D19AF2BC8E0 (void);
// 0x00000216 System.Void AddItemFavB::DelItemList(System.Collections.Generic.List`1<System.String>,System.Collections.Generic.List`1<UnityEngine.Texture2D>)
extern void AddItemFavB_DelItemList_mBA5C925C6F8C778173B03A582DE3350FFC965C04 (void);
// 0x00000217 System.Void AddItemFavB::InitItem(UnityEngine.GameObject,System.String,System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>)
extern void AddItemFavB_InitItem_mBEE11E74364AB4014861A630B6D21D63AE0FDE50 (void);
// 0x00000218 System.Collections.IEnumerator AddItemFavB::ImportImage(System.String,UnityEngine.UI.Button)
extern void AddItemFavB_ImportImage_mD2F4F737D7C782E165170EED54985C283FF55D13 (void);
// 0x00000219 System.Collections.IEnumerator AddItemFavB::tmp()
extern void AddItemFavB_tmp_m7751B13DC3FCE7A89EE6396F031D7585480E7509 (void);
// 0x0000021A System.Void AddItemFavB::MakeItem(System.String,System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,UnityEngine.GameObject)
extern void AddItemFavB_MakeItem_mAB1CAE7FED229C3F01A0D7F18ABE4C048311CB82 (void);
// 0x0000021B System.Void AddItemFavB::clearManager()
extern void AddItemFavB_clearManager_mEE1E3A7A10502A7298977E10FD8506C66464C026 (void);
// 0x0000021C System.Void AddItemFavB::filterlist(System.String)
extern void AddItemFavB_filterlist_mACA526106C7784AFFDB5ECE71A171E9BBE1EC73A (void);
// 0x0000021D System.Single AddItemFavB::rectcalc(System.Int32)
extern void AddItemFavB_rectcalc_mC02D85956B8D0AA82C6408244D4F469585EAA53B (void);
// 0x0000021E System.Void AddItemFavB::.ctor()
extern void AddItemFavB__ctor_m5FF4671857685C3AB58859505C9FAFC9FEF92C2D (void);
// 0x0000021F System.Void AddItemFavB::.cctor()
extern void AddItemFavB__cctor_m301EFC4D073F41B37BE3328B72E210069BC9792F (void);
// 0x00000220 System.Void AddItemFavB::<Start>b__99_0(System.Int32)
extern void AddItemFavB_U3CStartU3Eb__99_0_mA8D5B273FC5BA5D94E2BF1ED4497CE6A3DAE824D (void);
// 0x00000221 System.Void AddItemFavB::<Start>b__99_1()
extern void AddItemFavB_U3CStartU3Eb__99_1_m3EF20EAD20C93BB7BE66740C163360D43939729B (void);
// 0x00000222 System.Void AddItemFavB::<Start>b__99_2()
extern void AddItemFavB_U3CStartU3Eb__99_2_mC9A25F8B804079C5FFB41CD8B5870F3AAAAD74DD (void);
// 0x00000223 System.Void AddItemFavB::<Start>b__99_3(System.Int32)
extern void AddItemFavB_U3CStartU3Eb__99_3_mE3A548D498C7971A9E09A67EB2D3A53490C1A218 (void);
// 0x00000224 System.Void AddItemFavB::<Start>b__99_4(System.Int32)
extern void AddItemFavB_U3CStartU3Eb__99_4_mE059827DCAFF42E96F366E0D5A6ABE6DE6218C4E (void);
// 0x00000225 System.Void AddItemFavB::<Start>b__99_5()
extern void AddItemFavB_U3CStartU3Eb__99_5_mCCE5DFBA80917F86A0D33F8F95A90EE68FBD7B8C (void);
// 0x00000226 System.Void AddItemFavB::<Start>b__99_6(System.Int32)
extern void AddItemFavB_U3CStartU3Eb__99_6_m74B31F18DD846810AA503A5E18DFC3338A1EA903 (void);
// 0x00000227 System.Void AddItemFavB::<Start>b__99_7()
extern void AddItemFavB_U3CStartU3Eb__99_7_mEF9860D29D5CE0D20BEA43B5CD887B4C52EFE442 (void);
// 0x00000228 System.Void AddItemFavB::<Start>b__99_8()
extern void AddItemFavB_U3CStartU3Eb__99_8_m79681213EFBBA835E69B65411256690670A91739 (void);
// 0x00000229 System.Void AddItemFavB::<Start>b__99_9()
extern void AddItemFavB_U3CStartU3Eb__99_9_m555BE782F45EBF179B9BABE54B16492B6CEB8812 (void);
// 0x0000022A System.Boolean AddItemFavB::<ButNextRoutine>b__101_0()
extern void AddItemFavB_U3CButNextRoutineU3Eb__101_0_mF0329110041A2D13DD902A94C366BF88A5BB1153 (void);
// 0x0000022B System.Boolean AddItemFavB::<ButHomeRoutine>b__103_0()
extern void AddItemFavB_U3CButHomeRoutineU3Eb__103_0_m3DD2F91A3E0847798137288B2BD5200AD3743A73 (void);
// 0x0000022C System.Boolean AddItemFavB::<ButSignOutRoutine>b__105_0()
extern void AddItemFavB_U3CButSignOutRoutineU3Eb__105_0_mBAC844154801247AF08098C35F5FF884679820A7 (void);
// 0x0000022D System.Boolean AddItemFavB::<ButPrevRoutine>b__106_0()
extern void AddItemFavB_U3CButPrevRoutineU3Eb__106_0_m9C4A5DA6DE7430FFD2CA7B92EF9FF19A2390BCD3 (void);
// 0x0000022E System.Boolean AddItemFavB::<FilSrcRoutine>b__108_0()
extern void AddItemFavB_U3CFilSrcRoutineU3Eb__108_0_mA644BDEF06923340BC7780C8B217A4A256DDB23A (void);
// 0x0000022F System.Boolean AddItemFavB::<FilUpdRoutine>b__109_0()
extern void AddItemFavB_U3CFilUpdRoutineU3Eb__109_0_m49BB9EC280F925B74A31AA25ECC4574E69B5F686 (void);
// 0x00000230 System.Boolean AddItemFavB::<StopDownloading>b__112_0()
extern void AddItemFavB_U3CStopDownloadingU3Eb__112_0_mB21FEE923B57F6D9C1FFA163A7A72EACE957ECD2 (void);
// 0x00000231 System.Boolean AddItemFavB::<tmpScroll>b__117_0()
extern void AddItemFavB_U3CtmpScrollU3Eb__117_0_m40BA1F8A9DBBFF062BFA53257DC02DFE9CD623EB (void);
// 0x00000232 System.Void AddItemFavB::<ImageAddingProcess>b__123_0()
extern void AddItemFavB_U3CImageAddingProcessU3Eb__123_0_mFF88D401B07CC1CDC0FD84D028C5E4A0AD0EB31A (void);
// 0x00000233 System.Boolean AddItemFavB::<ToProduct>b__128_0()
extern void AddItemFavB_U3CToProductU3Eb__128_0_m12AB57D8327CC94A4ACF2D61557E599DAC507D78 (void);
// 0x00000234 System.Void AddItemFavB/OnReadyToReceive::.ctor(System.Object,System.IntPtr)
extern void OnReadyToReceive__ctor_m5BA976264F676FA37C832C0B713BD32E072450BB (void);
// 0x00000235 System.Void AddItemFavB/OnReadyToReceive::Invoke(Firebase.Firestore.DocumentSnapshot)
extern void OnReadyToReceive_Invoke_m90F135426AF33FC5EED6A904D820EF832F23EAF2 (void);
// 0x00000236 System.IAsyncResult AddItemFavB/OnReadyToReceive::BeginInvoke(Firebase.Firestore.DocumentSnapshot,System.AsyncCallback,System.Object)
extern void OnReadyToReceive_BeginInvoke_mA687EEAF7934ACFBD772E83CC7F172530FE1AA84 (void);
// 0x00000237 System.Void AddItemFavB/OnReadyToReceive::EndInvoke(System.IAsyncResult)
extern void OnReadyToReceive_EndInvoke_m8F590E3B448742A7B303D44017DD7CFBA862A184 (void);
// 0x00000238 System.Void AddItemFavB/OnReadyToReceiveBatch::.ctor(System.Object,System.IntPtr)
extern void OnReadyToReceiveBatch__ctor_m6B3F983EB4FD08E5D9186A9585FE6FCDFED7E576 (void);
// 0x00000239 System.Void AddItemFavB/OnReadyToReceiveBatch::Invoke(Firebase.Firestore.DocumentSnapshot,System.Int32)
extern void OnReadyToReceiveBatch_Invoke_m9CF9203113AA748372561176107D1359DAEBFE09 (void);
// 0x0000023A System.IAsyncResult AddItemFavB/OnReadyToReceiveBatch::BeginInvoke(Firebase.Firestore.DocumentSnapshot,System.Int32,System.AsyncCallback,System.Object)
extern void OnReadyToReceiveBatch_BeginInvoke_mAA642CB130478721475B2B8DFC260CB509F5F231 (void);
// 0x0000023B System.Void AddItemFavB/OnReadyToReceiveBatch::EndInvoke(System.IAsyncResult)
extern void OnReadyToReceiveBatch_EndInvoke_m963FA6AD9BC0E1E8E4B9827FEAD11400DF271424 (void);
// 0x0000023C System.Void AddItemFavB/OnReadyToReceiveFav::.ctor(System.Object,System.IntPtr)
extern void OnReadyToReceiveFav__ctor_mF5DF7BE191A2F4B9451957580AB190B6E9EAD4B7 (void);
// 0x0000023D System.Void AddItemFavB/OnReadyToReceiveFav::Invoke()
extern void OnReadyToReceiveFav_Invoke_m32672FE23775F12D8B53314CF9A3F003A7473916 (void);
// 0x0000023E System.IAsyncResult AddItemFavB/OnReadyToReceiveFav::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnReadyToReceiveFav_BeginInvoke_m8BC7E46D0031197EC55CFF49C47DC0940F5FAD7B (void);
// 0x0000023F System.Void AddItemFavB/OnReadyToReceiveFav::EndInvoke(System.IAsyncResult)
extern void OnReadyToReceiveFav_EndInvoke_mEF014CCCAECF737CAB55FE1A9DC7E56468A678DD (void);
// 0x00000240 System.Void AddItemFavB/OnCloseNav::.ctor(System.Object,System.IntPtr)
extern void OnCloseNav__ctor_mCA5E5AA84D53AC37CC5851F51DEE4451A2529C0A (void);
// 0x00000241 System.Void AddItemFavB/OnCloseNav::Invoke()
extern void OnCloseNav_Invoke_m9C60BDB40A53E56EABB5EA7E6F6D5ED5D8BC5EFA (void);
// 0x00000242 System.IAsyncResult AddItemFavB/OnCloseNav::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCloseNav_BeginInvoke_m8882779DBA8E58C3ADAB2D3EE8E9D788C67F7C5B (void);
// 0x00000243 System.Void AddItemFavB/OnCloseNav::EndInvoke(System.IAsyncResult)
extern void OnCloseNav_EndInvoke_m84892566A5E732C0DEC94251A753F5B11261EC51 (void);
// 0x00000244 System.Void AddItemFavB/OnCloseFil::.ctor(System.Object,System.IntPtr)
extern void OnCloseFil__ctor_mDC82FB5AD1A8D0AF64875DADE4F5557266634C08 (void);
// 0x00000245 System.Void AddItemFavB/OnCloseFil::Invoke()
extern void OnCloseFil_Invoke_m861225219A4A40363D3A3C6FA2A801626E9A3C98 (void);
// 0x00000246 System.IAsyncResult AddItemFavB/OnCloseFil::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCloseFil_BeginInvoke_mACF38DAADA380B37FF8D3CD830AAE5AD0C0CB938 (void);
// 0x00000247 System.Void AddItemFavB/OnCloseFil::EndInvoke(System.IAsyncResult)
extern void OnCloseFil_EndInvoke_m2A4D71A9AF5F5C3477DAA504AE8E2E713A46B21D (void);
// 0x00000248 System.Void AddItemFavB/<SetPrefs>d__100::.ctor(System.Int32)
extern void U3CSetPrefsU3Ed__100__ctor_m76E7D60293C830A2EB67DE51B7781AE61C4BE76B (void);
// 0x00000249 System.Void AddItemFavB/<SetPrefs>d__100::System.IDisposable.Dispose()
extern void U3CSetPrefsU3Ed__100_System_IDisposable_Dispose_m7A07429A4ACA08DAF578053504AAAEDD40BE443E (void);
// 0x0000024A System.Boolean AddItemFavB/<SetPrefs>d__100::MoveNext()
extern void U3CSetPrefsU3Ed__100_MoveNext_m77E2F4A8E1593B420D3F219C4583032C8A611740 (void);
// 0x0000024B System.Object AddItemFavB/<SetPrefs>d__100::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetPrefsU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09ECBE0E3957BD7BE662F0A0D8F1423E702C59D (void);
// 0x0000024C System.Void AddItemFavB/<SetPrefs>d__100::System.Collections.IEnumerator.Reset()
extern void U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_Reset_mBF33BE7CA5F9C5F1DA7EF54857F51C9E81D9ABEF (void);
// 0x0000024D System.Object AddItemFavB/<SetPrefs>d__100::System.Collections.IEnumerator.get_Current()
extern void U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_get_Current_m7891B9920CFC41570FFE0F5564EFA91AF3E18D7E (void);
// 0x0000024E System.Void AddItemFavB/<ButNextRoutine>d__101::.ctor(System.Int32)
extern void U3CButNextRoutineU3Ed__101__ctor_m770F9523CADF1BEF7FF7C16013118EF432B0C662 (void);
// 0x0000024F System.Void AddItemFavB/<ButNextRoutine>d__101::System.IDisposable.Dispose()
extern void U3CButNextRoutineU3Ed__101_System_IDisposable_Dispose_m8A144B4DA6125320A42AF0152CB56D7085711F64 (void);
// 0x00000250 System.Boolean AddItemFavB/<ButNextRoutine>d__101::MoveNext()
extern void U3CButNextRoutineU3Ed__101_MoveNext_m341587A342195F33ECC1803E9343ED29911C21A6 (void);
// 0x00000251 System.Object AddItemFavB/<ButNextRoutine>d__101::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButNextRoutineU3Ed__101_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE566BDB6564C589FD9A492BDAF211E8BE551926A (void);
// 0x00000252 System.Void AddItemFavB/<ButNextRoutine>d__101::System.Collections.IEnumerator.Reset()
extern void U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_Reset_m0ECC8CDBA0AB2C92963F60C9C5ACF1D68091BCF3 (void);
// 0x00000253 System.Object AddItemFavB/<ButNextRoutine>d__101::System.Collections.IEnumerator.get_Current()
extern void U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_get_Current_m88FF3464C964E918C4D1B4563158F2E2742BF34B (void);
// 0x00000254 System.Void AddItemFavB/<DelItemListWait>d__102::.ctor(System.Int32)
extern void U3CDelItemListWaitU3Ed__102__ctor_mE4FD584463368E858E6500444E13560D39A8E7E4 (void);
// 0x00000255 System.Void AddItemFavB/<DelItemListWait>d__102::System.IDisposable.Dispose()
extern void U3CDelItemListWaitU3Ed__102_System_IDisposable_Dispose_m84599E19DEB6523032C1B366CE872B1CAD3F029D (void);
// 0x00000256 System.Boolean AddItemFavB/<DelItemListWait>d__102::MoveNext()
extern void U3CDelItemListWaitU3Ed__102_MoveNext_m1AA21BCB909060ED7D3F10A742908DFF9FBC8984 (void);
// 0x00000257 System.Object AddItemFavB/<DelItemListWait>d__102::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelItemListWaitU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E1971853F5E1897DE572CFE2A1F5CE93DCC2E11 (void);
// 0x00000258 System.Void AddItemFavB/<DelItemListWait>d__102::System.Collections.IEnumerator.Reset()
extern void U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_Reset_m9AC6364FB08D83A09BA4918A401DADF38A842113 (void);
// 0x00000259 System.Object AddItemFavB/<DelItemListWait>d__102::System.Collections.IEnumerator.get_Current()
extern void U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_get_Current_m9A7B9240C6F6E8EFA321E3470395DD66C24C5F9D (void);
// 0x0000025A System.Void AddItemFavB/<ButHomeRoutine>d__103::.ctor(System.Int32)
extern void U3CButHomeRoutineU3Ed__103__ctor_m8833F61EF7E0A7572E454A323DF97D20BA2588B1 (void);
// 0x0000025B System.Void AddItemFavB/<ButHomeRoutine>d__103::System.IDisposable.Dispose()
extern void U3CButHomeRoutineU3Ed__103_System_IDisposable_Dispose_m7EEEB03EB525F93260C6A019B8A907A9FEBB3F8F (void);
// 0x0000025C System.Boolean AddItemFavB/<ButHomeRoutine>d__103::MoveNext()
extern void U3CButHomeRoutineU3Ed__103_MoveNext_mFCFE8A14A87214506B84399E3DD0189891AA4727 (void);
// 0x0000025D System.Object AddItemFavB/<ButHomeRoutine>d__103::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButHomeRoutineU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C82185097BE3FEAB452F9C1ABD34D4DCBFF679C (void);
// 0x0000025E System.Void AddItemFavB/<ButHomeRoutine>d__103::System.Collections.IEnumerator.Reset()
extern void U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_Reset_mE8665766A9A6D613DAE425A1054D00FE793FA5EC (void);
// 0x0000025F System.Object AddItemFavB/<ButHomeRoutine>d__103::System.Collections.IEnumerator.get_Current()
extern void U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_get_Current_mD32884E4815C7AEA078E5202A3399B8999BB570E (void);
// 0x00000260 System.Void AddItemFavB/<ButSignOutRoutine>d__105::.ctor(System.Int32)
extern void U3CButSignOutRoutineU3Ed__105__ctor_m007F2C5053AA977BE6D4BA0F4B26EF8168F62BCF (void);
// 0x00000261 System.Void AddItemFavB/<ButSignOutRoutine>d__105::System.IDisposable.Dispose()
extern void U3CButSignOutRoutineU3Ed__105_System_IDisposable_Dispose_mCDB3459813B11DBED7B42C011506ACB7F3FA7FDD (void);
// 0x00000262 System.Boolean AddItemFavB/<ButSignOutRoutine>d__105::MoveNext()
extern void U3CButSignOutRoutineU3Ed__105_MoveNext_m274820FDF5CFE17304F46A2B3ACDA8B6A698D630 (void);
// 0x00000263 System.Object AddItemFavB/<ButSignOutRoutine>d__105::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButSignOutRoutineU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC0D8BF7401FD148B2B40B4D9BF77090FDEE7406 (void);
// 0x00000264 System.Void AddItemFavB/<ButSignOutRoutine>d__105::System.Collections.IEnumerator.Reset()
extern void U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_Reset_m1BEDE53990B83198CB62F9F2246E3AB05D7286F8 (void);
// 0x00000265 System.Object AddItemFavB/<ButSignOutRoutine>d__105::System.Collections.IEnumerator.get_Current()
extern void U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_get_Current_m4768AB7E02D5CD67EEF47DCFE270C258F0454E7B (void);
// 0x00000266 System.Void AddItemFavB/<ButPrevRoutine>d__106::.ctor(System.Int32)
extern void U3CButPrevRoutineU3Ed__106__ctor_mD5DB2A5A9B55B135C05747812341B7DEF3C10503 (void);
// 0x00000267 System.Void AddItemFavB/<ButPrevRoutine>d__106::System.IDisposable.Dispose()
extern void U3CButPrevRoutineU3Ed__106_System_IDisposable_Dispose_m41E2986903131B9738B1D226DE03735A5E5A8393 (void);
// 0x00000268 System.Boolean AddItemFavB/<ButPrevRoutine>d__106::MoveNext()
extern void U3CButPrevRoutineU3Ed__106_MoveNext_m247438388020CEDF9185F8B4940F7570C01AC05F (void);
// 0x00000269 System.Object AddItemFavB/<ButPrevRoutine>d__106::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButPrevRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m677A17883231084C84F5D047340EEBA04A75E6CD (void);
// 0x0000026A System.Void AddItemFavB/<ButPrevRoutine>d__106::System.Collections.IEnumerator.Reset()
extern void U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_Reset_mF8889B67CDD20BE3EC695C1369D51E699AF71F59 (void);
// 0x0000026B System.Object AddItemFavB/<ButPrevRoutine>d__106::System.Collections.IEnumerator.get_Current()
extern void U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m6A78883AB5236917A0C8EEECDB55B0650B83138F (void);
// 0x0000026C System.Void AddItemFavB/<FilSrcRoutine>d__108::.ctor(System.Int32)
extern void U3CFilSrcRoutineU3Ed__108__ctor_m4A95C6E500D70B86FD6E61E285BA194978AACC8C (void);
// 0x0000026D System.Void AddItemFavB/<FilSrcRoutine>d__108::System.IDisposable.Dispose()
extern void U3CFilSrcRoutineU3Ed__108_System_IDisposable_Dispose_m02E3089371909C6F27EE212E7AEFC5AD54F45A63 (void);
// 0x0000026E System.Boolean AddItemFavB/<FilSrcRoutine>d__108::MoveNext()
extern void U3CFilSrcRoutineU3Ed__108_MoveNext_m0880302B0761556C023C4B432F0574F69B3BD192 (void);
// 0x0000026F System.Object AddItemFavB/<FilSrcRoutine>d__108::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFilSrcRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF2BEEC524BF85436365222AE9DCE459A093759C (void);
// 0x00000270 System.Void AddItemFavB/<FilSrcRoutine>d__108::System.Collections.IEnumerator.Reset()
extern void U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_Reset_m1C0E3E34FE3BFF6CE43A3C4711E7498BCB8026C4 (void);
// 0x00000271 System.Object AddItemFavB/<FilSrcRoutine>d__108::System.Collections.IEnumerator.get_Current()
extern void U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_m6C97BB788CFF7957C613C778C2B2224FEB864D37 (void);
// 0x00000272 System.Void AddItemFavB/<>c__DisplayClass109_0::.ctor()
extern void U3CU3Ec__DisplayClass109_0__ctor_m126D2FBAB6967FAF17D28289325A15D4D9AAF168 (void);
// 0x00000273 System.Boolean AddItemFavB/<>c__DisplayClass109_0::<FilUpdRoutine>b__2()
extern void U3CU3Ec__DisplayClass109_0_U3CFilUpdRoutineU3Eb__2_m3E356654F5F2FC6E596A6A43298AB39192116E2C (void);
// 0x00000274 System.Void AddItemFavB/<>c::.cctor()
extern void U3CU3Ec__cctor_m8FE969DD5E66643884520973EBC958190F0DD83E (void);
// 0x00000275 System.Void AddItemFavB/<>c::.ctor()
extern void U3CU3Ec__ctor_m8013E921369BD9D2C9918D14FC091419CA571136 (void);
// 0x00000276 System.Void AddItemFavB/<>c::<FilUpdRoutine>b__109_1(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CFilUpdRoutineU3Eb__109_1_m5C235F8BC6171A3E85E3AFE899B89BDDABF9B169 (void);
// 0x00000277 System.Void AddItemFavB/<>c::<InitItem>b__133_2(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CInitItemU3Eb__133_2_mD29ABCE4171D0F18D7C10242167AEA4E680BC84A (void);
// 0x00000278 System.Void AddItemFavB/<FilUpdRoutine>d__109::.ctor(System.Int32)
extern void U3CFilUpdRoutineU3Ed__109__ctor_m996A8F53737D39937922BAD80D230D2CDA6C1D9E (void);
// 0x00000279 System.Void AddItemFavB/<FilUpdRoutine>d__109::System.IDisposable.Dispose()
extern void U3CFilUpdRoutineU3Ed__109_System_IDisposable_Dispose_m076118330E20CBC1EB2648A81B6B1C1E97160BB6 (void);
// 0x0000027A System.Boolean AddItemFavB/<FilUpdRoutine>d__109::MoveNext()
extern void U3CFilUpdRoutineU3Ed__109_MoveNext_m16D414225DF8EFB45108BFF12B2757C6F99906AD (void);
// 0x0000027B System.Object AddItemFavB/<FilUpdRoutine>d__109::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFilUpdRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m851CADA05D65739DDA283DC975A580F7DDD0CA2D (void);
// 0x0000027C System.Void AddItemFavB/<FilUpdRoutine>d__109::System.Collections.IEnumerator.Reset()
extern void U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m4E5E5700E9A4F96411B9DD5C3F3E0C1D318277B1 (void);
// 0x0000027D System.Object AddItemFavB/<FilUpdRoutine>d__109::System.Collections.IEnumerator.get_Current()
extern void U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_mAB074BD2F4B1C09BDD6D48EFFC3AF7E73D1655A5 (void);
// 0x0000027E System.Void AddItemFavB/<StopDownloading>d__112::.ctor(System.Int32)
extern void U3CStopDownloadingU3Ed__112__ctor_mF6C2EC9ACE60A4839D8D46C1C43BC6281C03AEF8 (void);
// 0x0000027F System.Void AddItemFavB/<StopDownloading>d__112::System.IDisposable.Dispose()
extern void U3CStopDownloadingU3Ed__112_System_IDisposable_Dispose_m0917D6FA891909094BE58CCE929228AB202B62FD (void);
// 0x00000280 System.Boolean AddItemFavB/<StopDownloading>d__112::MoveNext()
extern void U3CStopDownloadingU3Ed__112_MoveNext_m290FD4A7C0D0E1A5B257A3FA63EAEA6153E8734B (void);
// 0x00000281 System.Object AddItemFavB/<StopDownloading>d__112::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStopDownloadingU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DD6FFCB5D8649CB1E542FD833A680725C891CD6 (void);
// 0x00000282 System.Void AddItemFavB/<StopDownloading>d__112::System.Collections.IEnumerator.Reset()
extern void U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_Reset_mA01D081F3449DBD67B05E87BB4B63E3333994878 (void);
// 0x00000283 System.Object AddItemFavB/<StopDownloading>d__112::System.Collections.IEnumerator.get_Current()
extern void U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_get_Current_m6434132FC738B3A11CFAACD960F507329B563756 (void);
// 0x00000284 System.Void AddItemFavB/<ResendRepeat>d__114::.ctor(System.Int32)
extern void U3CResendRepeatU3Ed__114__ctor_m3F5BFEDD90E83C5794F3D831B106A8B58CDC04B8 (void);
// 0x00000285 System.Void AddItemFavB/<ResendRepeat>d__114::System.IDisposable.Dispose()
extern void U3CResendRepeatU3Ed__114_System_IDisposable_Dispose_mC1A4224C9CD3D4C57EA721AF4825AE14184C37B9 (void);
// 0x00000286 System.Boolean AddItemFavB/<ResendRepeat>d__114::MoveNext()
extern void U3CResendRepeatU3Ed__114_MoveNext_mC59492B45F4F6D622F755A54067DEEA42483C77A (void);
// 0x00000287 System.Object AddItemFavB/<ResendRepeat>d__114::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResendRepeatU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m719B58A1C0991C1BA3B02879D4A1A20254AAB6C9 (void);
// 0x00000288 System.Void AddItemFavB/<ResendRepeat>d__114::System.Collections.IEnumerator.Reset()
extern void U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_Reset_m5FCD37DD1EE430EDECE9ED6DAB1B6A56E04C2392 (void);
// 0x00000289 System.Object AddItemFavB/<ResendRepeat>d__114::System.Collections.IEnumerator.get_Current()
extern void U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_get_Current_mA40A27CD9848559171D6093EFB1937A3DCE29D7A (void);
// 0x0000028A System.Void AddItemFavB/<tmpScroll>d__117::.ctor(System.Int32)
extern void U3CtmpScrollU3Ed__117__ctor_m7C268731D5F8756B86633FC062CCC952B6740FE5 (void);
// 0x0000028B System.Void AddItemFavB/<tmpScroll>d__117::System.IDisposable.Dispose()
extern void U3CtmpScrollU3Ed__117_System_IDisposable_Dispose_m831B53BB37B2E9240405FA9BE9F1076F02087F06 (void);
// 0x0000028C System.Boolean AddItemFavB/<tmpScroll>d__117::MoveNext()
extern void U3CtmpScrollU3Ed__117_MoveNext_m228C64A953EF790656960D283398B6697E28244F (void);
// 0x0000028D System.Object AddItemFavB/<tmpScroll>d__117::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtmpScrollU3Ed__117_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m224B1BD9D4CD4FB193F8A17EC08A02C6E8ECE348 (void);
// 0x0000028E System.Void AddItemFavB/<tmpScroll>d__117::System.Collections.IEnumerator.Reset()
extern void U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_Reset_mD408582C33E6F0343EA87EC85DC6FD882F3B218A (void);
// 0x0000028F System.Object AddItemFavB/<tmpScroll>d__117::System.Collections.IEnumerator.get_Current()
extern void U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_get_Current_m7601D10039050BAF1A5EC3F95CF7F794E28876C9 (void);
// 0x00000290 System.Void AddItemFavB/<BeginGarmentCall>d__118::.ctor(System.Int32)
extern void U3CBeginGarmentCallU3Ed__118__ctor_mFF9ECD2897932FB44B8FDED5ED73D79BD14E8E8D (void);
// 0x00000291 System.Void AddItemFavB/<BeginGarmentCall>d__118::System.IDisposable.Dispose()
extern void U3CBeginGarmentCallU3Ed__118_System_IDisposable_Dispose_m4F58A0CDBC68DEF08EE185191E23BE180CD4AD34 (void);
// 0x00000292 System.Boolean AddItemFavB/<BeginGarmentCall>d__118::MoveNext()
extern void U3CBeginGarmentCallU3Ed__118_MoveNext_m0D9996738D698B22F8B4CDCA1BEE7993AA234E32 (void);
// 0x00000293 System.Object AddItemFavB/<BeginGarmentCall>d__118::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBeginGarmentCallU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27291F3DF06BA60F244574ABBB6F46214B0DA02F (void);
// 0x00000294 System.Void AddItemFavB/<BeginGarmentCall>d__118::System.Collections.IEnumerator.Reset()
extern void U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_Reset_m195F2702399FBC0ADC51B2205A1029553D4E87ED (void);
// 0x00000295 System.Object AddItemFavB/<BeginGarmentCall>d__118::System.Collections.IEnumerator.get_Current()
extern void U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_get_Current_mFCDE05B088B0E75E0D13F80DFA5BACD20AF5FF74 (void);
// 0x00000296 System.Void AddItemFavB/<ImageTexHandler>d__122::.ctor(System.Int32)
extern void U3CImageTexHandlerU3Ed__122__ctor_mF7419C5E9365A97FB7C1DA3B0424FDDD07887E25 (void);
// 0x00000297 System.Void AddItemFavB/<ImageTexHandler>d__122::System.IDisposable.Dispose()
extern void U3CImageTexHandlerU3Ed__122_System_IDisposable_Dispose_m9AA69120AE92EE8525CD5258A84ECCB851F6A777 (void);
// 0x00000298 System.Boolean AddItemFavB/<ImageTexHandler>d__122::MoveNext()
extern void U3CImageTexHandlerU3Ed__122_MoveNext_m9E40E4D360125DCD7FB232BE4C4CA9F37A02C024 (void);
// 0x00000299 System.Void AddItemFavB/<ImageTexHandler>d__122::<>m__Finally1()
extern void U3CImageTexHandlerU3Ed__122_U3CU3Em__Finally1_mB71D86C4AC6620BF6537AE82090643CEA2A9AC31 (void);
// 0x0000029A System.Object AddItemFavB/<ImageTexHandler>d__122::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageTexHandlerU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91B0112E2D4FBDA7C9BB2CE5BB6011D358ED2A80 (void);
// 0x0000029B System.Void AddItemFavB/<ImageTexHandler>d__122::System.Collections.IEnumerator.Reset()
extern void U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_Reset_m5BF7D635AFBE4EB47303002CB2C6AF54DB25E364 (void);
// 0x0000029C System.Object AddItemFavB/<ImageTexHandler>d__122::System.Collections.IEnumerator.get_Current()
extern void U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_get_Current_mA1A91EE97DA546664D9806E5363E4A5D640B1F87 (void);
// 0x0000029D System.Void AddItemFavB/<ImageAddingProcess>d__123::.ctor(System.Int32)
extern void U3CImageAddingProcessU3Ed__123__ctor_mD95084F0EADFB19F6539589548840D6AD193B303 (void);
// 0x0000029E System.Void AddItemFavB/<ImageAddingProcess>d__123::System.IDisposable.Dispose()
extern void U3CImageAddingProcessU3Ed__123_System_IDisposable_Dispose_m575C4A8E4B54ABC855E1869356DBE5701BBCFC6B (void);
// 0x0000029F System.Boolean AddItemFavB/<ImageAddingProcess>d__123::MoveNext()
extern void U3CImageAddingProcessU3Ed__123_MoveNext_m3289B02398939735D81DB6A4560AF79314D939DB (void);
// 0x000002A0 System.Void AddItemFavB/<ImageAddingProcess>d__123::<>m__Finally1()
extern void U3CImageAddingProcessU3Ed__123_U3CU3Em__Finally1_m63622B0D3CDA33D0334A4B188FC72E65F21C2A6C (void);
// 0x000002A1 System.Object AddItemFavB/<ImageAddingProcess>d__123::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageAddingProcessU3Ed__123_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8059792712242D80BA76F0E9D00EC3DE621E69E1 (void);
// 0x000002A2 System.Void AddItemFavB/<ImageAddingProcess>d__123::System.Collections.IEnumerator.Reset()
extern void U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_Reset_m13A4AEA4FB51F19C57232D20FE71D9D0E95F965D (void);
// 0x000002A3 System.Object AddItemFavB/<ImageAddingProcess>d__123::System.Collections.IEnumerator.get_Current()
extern void U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_get_Current_mB06D6DA393684E65C8B457E41A43D251E15063C5 (void);
// 0x000002A4 System.Void AddItemFavB/<>c__DisplayClass125_0::.ctor()
extern void U3CU3Ec__DisplayClass125_0__ctor_m9E145E69AC81B1E08A8B1FB9D0C79EE192BA705F (void);
// 0x000002A5 System.Void AddItemFavB/<>c__DisplayClass125_0::<FbaseQueries>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass125_0_U3CFbaseQueriesU3Eb__0_m0B528E761873CBBDCB12BE7EB5C69BCB1E43A144 (void);
// 0x000002A6 System.Boolean AddItemFavB/<>c__DisplayClass125_0::<FbaseQueries>b__1()
extern void U3CU3Ec__DisplayClass125_0_U3CFbaseQueriesU3Eb__1_m1054EF8816FAECBEC57675EF1DC2E4C94DBC176B (void);
// 0x000002A7 System.Void AddItemFavB/<FbaseQueries>d__125::.ctor(System.Int32)
extern void U3CFbaseQueriesU3Ed__125__ctor_m0021C3BB0BEFE90B6EC45561DF67D8B90CC2F05D (void);
// 0x000002A8 System.Void AddItemFavB/<FbaseQueries>d__125::System.IDisposable.Dispose()
extern void U3CFbaseQueriesU3Ed__125_System_IDisposable_Dispose_m16D84B535CF2EE6EBC48C2BEE44FACBB6172BD57 (void);
// 0x000002A9 System.Boolean AddItemFavB/<FbaseQueries>d__125::MoveNext()
extern void U3CFbaseQueriesU3Ed__125_MoveNext_m19C45FBD2C092D6BC734CB3777AF7DDE0B8185B0 (void);
// 0x000002AA System.Object AddItemFavB/<FbaseQueries>d__125::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFbaseQueriesU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m482847F1098B4057D8AE595F730B5DB8CBD9CA56 (void);
// 0x000002AB System.Void AddItemFavB/<FbaseQueries>d__125::System.Collections.IEnumerator.Reset()
extern void U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_Reset_m5A083EF573E4B42EF6EABF3A0F000DB6A9D1970C (void);
// 0x000002AC System.Object AddItemFavB/<FbaseQueries>d__125::System.Collections.IEnumerator.get_Current()
extern void U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_get_Current_mA7E00797E8DF86728C49DC78C62E20956683FAD6 (void);
// 0x000002AD System.Void AddItemFavB/<ToProduct>d__128::.ctor(System.Int32)
extern void U3CToProductU3Ed__128__ctor_m6A9A821B8BABFB4F8CE697657BB2FA1225B4082E (void);
// 0x000002AE System.Void AddItemFavB/<ToProduct>d__128::System.IDisposable.Dispose()
extern void U3CToProductU3Ed__128_System_IDisposable_Dispose_mAA22CFB62E9C7491744FC997FF1567091C6A754A (void);
// 0x000002AF System.Boolean AddItemFavB/<ToProduct>d__128::MoveNext()
extern void U3CToProductU3Ed__128_MoveNext_m5FE41A55947388389BF818B677EFFF5AE0310CF0 (void);
// 0x000002B0 System.Object AddItemFavB/<ToProduct>d__128::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CToProductU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2014580B242F8751BBB63797DD261AC125C09DC (void);
// 0x000002B1 System.Void AddItemFavB/<ToProduct>d__128::System.Collections.IEnumerator.Reset()
extern void U3CToProductU3Ed__128_System_Collections_IEnumerator_Reset_m3C019D7B395DA65959C6C26F5CDD692641DC1156 (void);
// 0x000002B2 System.Object AddItemFavB/<ToProduct>d__128::System.Collections.IEnumerator.get_Current()
extern void U3CToProductU3Ed__128_System_Collections_IEnumerator_get_Current_m866CBF81E26395A49826034FB021CE050BDCCF03 (void);
// 0x000002B3 System.Void AddItemFavB/<>c__DisplayClass133_0::.ctor()
extern void U3CU3Ec__DisplayClass133_0__ctor_m91FE31B5AD72669CAF5C7A22859E7CC11F145FE7 (void);
// 0x000002B4 System.Void AddItemFavB/<>c__DisplayClass133_0::<InitItem>b__0()
extern void U3CU3Ec__DisplayClass133_0_U3CInitItemU3Eb__0_m489DA734BD2163B21D4FF135838CC1698395A9CB (void);
// 0x000002B5 System.Void AddItemFavB/<>c__DisplayClass133_0::<InitItem>b__1()
extern void U3CU3Ec__DisplayClass133_0_U3CInitItemU3Eb__1_m554FC57D514FD3E17EA02ABDE60ECF78D7B9A31A (void);
// 0x000002B6 System.Void AddItemFavB/<>c__DisplayClass134_0::.ctor()
extern void U3CU3Ec__DisplayClass134_0__ctor_m8790328CD4DFF3C1F8520503D4261E1C6E7FA64D (void);
// 0x000002B7 System.Void AddItemFavB/<>c__DisplayClass134_0::<ImportImage>b__0()
extern void U3CU3Ec__DisplayClass134_0_U3CImportImageU3Eb__0_mFF6E310A5A142A5EA23C1C9B2531D66D200465AC (void);
// 0x000002B8 System.Boolean AddItemFavB/<>c__DisplayClass134_0::<ImportImage>b__1()
extern void U3CU3Ec__DisplayClass134_0_U3CImportImageU3Eb__1_mDDE969D220BD447748AE16C11405C4BF2B7C868A (void);
// 0x000002B9 System.Void AddItemFavB/<ImportImage>d__134::.ctor(System.Int32)
extern void U3CImportImageU3Ed__134__ctor_mE4C6E2BC19C6F1318CB4F0BAB54870E62A778064 (void);
// 0x000002BA System.Void AddItemFavB/<ImportImage>d__134::System.IDisposable.Dispose()
extern void U3CImportImageU3Ed__134_System_IDisposable_Dispose_m799A659DD685A41779A1E5664864393915A0AF8C (void);
// 0x000002BB System.Boolean AddItemFavB/<ImportImage>d__134::MoveNext()
extern void U3CImportImageU3Ed__134_MoveNext_m213D02F07B8B79C81B23D28B4F87CEDE2CA3DBA2 (void);
// 0x000002BC System.Object AddItemFavB/<ImportImage>d__134::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImportImageU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96D8D603CFA9A391F5A246114AEFEEDF6664C9F8 (void);
// 0x000002BD System.Void AddItemFavB/<ImportImage>d__134::System.Collections.IEnumerator.Reset()
extern void U3CImportImageU3Ed__134_System_Collections_IEnumerator_Reset_m0005C3A33E2057DF0A8C59A47855A44B385D0865 (void);
// 0x000002BE System.Object AddItemFavB/<ImportImage>d__134::System.Collections.IEnumerator.get_Current()
extern void U3CImportImageU3Ed__134_System_Collections_IEnumerator_get_Current_m2B7726DF26F380FDA790258265D8E5E2831759D8 (void);
// 0x000002BF System.Void AddItemFavB/<tmp>d__135::.ctor(System.Int32)
extern void U3CtmpU3Ed__135__ctor_mE8EA412BD2B6E55EA20FCA793A6446F4CF18BAE9 (void);
// 0x000002C0 System.Void AddItemFavB/<tmp>d__135::System.IDisposable.Dispose()
extern void U3CtmpU3Ed__135_System_IDisposable_Dispose_m800554F20A5F13ABE7D22BAE6F37B0FF286CAF32 (void);
// 0x000002C1 System.Boolean AddItemFavB/<tmp>d__135::MoveNext()
extern void U3CtmpU3Ed__135_MoveNext_mEF8D965784FA3FA3821C0DA0F5826649411432FB (void);
// 0x000002C2 System.Object AddItemFavB/<tmp>d__135::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtmpU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9363254BF1666CFC6D4530829C09BC31CF242E00 (void);
// 0x000002C3 System.Void AddItemFavB/<tmp>d__135::System.Collections.IEnumerator.Reset()
extern void U3CtmpU3Ed__135_System_Collections_IEnumerator_Reset_m4045FFCC29548CDAAD61AF52B90AEB91B3090987 (void);
// 0x000002C4 System.Object AddItemFavB/<tmp>d__135::System.Collections.IEnumerator.get_Current()
extern void U3CtmpU3Ed__135_System_Collections_IEnumerator_get_Current_mFE3562A87D8848233B9A20676B6178F80562E0A9 (void);
// 0x000002C5 System.Void AddItemSingle::Start()
extern void AddItemSingle_Start_m7C29FE4ABD449EBF98774BB07E6FC15392BD6A01 (void);
// 0x000002C6 System.Collections.IEnumerator AddItemSingle::ButHomeRoutine()
extern void AddItemSingle_ButHomeRoutine_m3A610393D49D4219306D680960B890A93A91EB14 (void);
// 0x000002C7 System.Collections.IEnumerator AddItemSingle::ButSignOutRoutine()
extern void AddItemSingle_ButSignOutRoutine_m512500DFBFFC99B2ECDA6B0E017427098D8440F3 (void);
// 0x000002C8 System.Collections.IEnumerator AddItemSingle::CleanTex()
extern void AddItemSingle_CleanTex_m8F1CD069B973B2F305B451EBC3092A3272A2FE1A (void);
// 0x000002C9 System.Void AddItemSingle::OnDisable()
extern void AddItemSingle_OnDisable_mA1413855702E0311F9822AFA851E75BE7D798FF6 (void);
// 0x000002CA System.Collections.IEnumerator AddItemSingle::ImageAddingProcess(System.String)
extern void AddItemSingle_ImageAddingProcess_mE0C64BBDA23A454F59F6386BCD424777079E19AE (void);
// 0x000002CB System.Void AddItemSingle::InitPoPProduct(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void AddItemSingle_InitPoPProduct_mFEB56EAE900D84C21517CD738EE7F9E437A4EC2B (void);
// 0x000002CC System.Void AddItemSingle::Update()
extern void AddItemSingle_Update_m9E4EDCA3CDC3092CEF03559ABCCC1D4DDE70D779 (void);
// 0x000002CD System.Void AddItemSingle::.ctor()
extern void AddItemSingle__ctor_m6C7D70C1BB1A56C51791AA8C081CA30BAC2B6D73 (void);
// 0x000002CE System.Void AddItemSingle::<Start>b__27_0()
extern void AddItemSingle_U3CStartU3Eb__27_0_m320F1D837FDB0993585F54FBEE60486E106C47E4 (void);
// 0x000002CF System.Void AddItemSingle::<Start>b__27_1()
extern void AddItemSingle_U3CStartU3Eb__27_1_m58A40C02BF1EDD867CECF3998ADB67E23739EFD5 (void);
// 0x000002D0 System.Boolean AddItemSingle::<ButHomeRoutine>b__28_0()
extern void AddItemSingle_U3CButHomeRoutineU3Eb__28_0_m0421464A105FB4632D590F221E4A092B9EFABA41 (void);
// 0x000002D1 System.Boolean AddItemSingle::<ButSignOutRoutine>b__29_0()
extern void AddItemSingle_U3CButSignOutRoutineU3Eb__29_0_mECADDAC0D393D555328D4CEB84A4AA54BA90FB2B (void);
// 0x000002D2 System.Void AddItemSingle::<ImageAddingProcess>b__32_0(System.String)
extern void AddItemSingle_U3CImageAddingProcessU3Eb__32_0_mE5C01CEF59A35D36831ACDE62AB012B49058645C (void);
// 0x000002D3 System.Void AddItemSingle::<ImageAddingProcess>b__32_1()
extern void AddItemSingle_U3CImageAddingProcessU3Eb__32_1_m6FBB2D3F4AF2D56D3F48FD456C2FF6E4D23A0150 (void);
// 0x000002D4 System.Boolean AddItemSingle::<ImageAddingProcess>b__32_2()
extern void AddItemSingle_U3CImageAddingProcessU3Eb__32_2_m773DFB3E50C1620401ACB394E2E213DE9E0F4E23 (void);
// 0x000002D5 System.Void AddItemSingle/<ButHomeRoutine>d__28::.ctor(System.Int32)
extern void U3CButHomeRoutineU3Ed__28__ctor_mB4504AB9D5F362706B1B45DF56DE52D86107543F (void);
// 0x000002D6 System.Void AddItemSingle/<ButHomeRoutine>d__28::System.IDisposable.Dispose()
extern void U3CButHomeRoutineU3Ed__28_System_IDisposable_Dispose_mF7FA8E483FB630014A420E312019C99A4C91630A (void);
// 0x000002D7 System.Boolean AddItemSingle/<ButHomeRoutine>d__28::MoveNext()
extern void U3CButHomeRoutineU3Ed__28_MoveNext_m76E41C5A986184F13E2D2B09FB6681D32332C335 (void);
// 0x000002D8 System.Object AddItemSingle/<ButHomeRoutine>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButHomeRoutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CB4964F1334604A1DE3B425844529B9333DA1F8 (void);
// 0x000002D9 System.Void AddItemSingle/<ButHomeRoutine>d__28::System.Collections.IEnumerator.Reset()
extern void U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_Reset_m0A2FE0CD2B6E84F803236ADE6B38B5E67E80858D (void);
// 0x000002DA System.Object AddItemSingle/<ButHomeRoutine>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_get_Current_mFE4C850F5F097C4BDE565B8F00258284878BBADB (void);
// 0x000002DB System.Void AddItemSingle/<ButSignOutRoutine>d__29::.ctor(System.Int32)
extern void U3CButSignOutRoutineU3Ed__29__ctor_mE815D9176BA4FE8E1196967DE1D6D0E66D3AB8ED (void);
// 0x000002DC System.Void AddItemSingle/<ButSignOutRoutine>d__29::System.IDisposable.Dispose()
extern void U3CButSignOutRoutineU3Ed__29_System_IDisposable_Dispose_mD297951099D5E22898EB1BF799A729585C7122E8 (void);
// 0x000002DD System.Boolean AddItemSingle/<ButSignOutRoutine>d__29::MoveNext()
extern void U3CButSignOutRoutineU3Ed__29_MoveNext_mFB181DC460EA2706C60FD33E9C2237AF1C32E930 (void);
// 0x000002DE System.Object AddItemSingle/<ButSignOutRoutine>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CButSignOutRoutineU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB6552AC06A51E5E351C52D5EA3F576AC9DF591A (void);
// 0x000002DF System.Void AddItemSingle/<ButSignOutRoutine>d__29::System.Collections.IEnumerator.Reset()
extern void U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_Reset_m9AAD08A86CC8BF57733370B1172044DAA919B197 (void);
// 0x000002E0 System.Object AddItemSingle/<ButSignOutRoutine>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_get_Current_m3F0632BD67CBA8E8A98ECCAD924769029E3994C8 (void);
// 0x000002E1 System.Void AddItemSingle/<CleanTex>d__30::.ctor(System.Int32)
extern void U3CCleanTexU3Ed__30__ctor_m0519A26FB715C19DB3B1BC92370C8546FFC96ADC (void);
// 0x000002E2 System.Void AddItemSingle/<CleanTex>d__30::System.IDisposable.Dispose()
extern void U3CCleanTexU3Ed__30_System_IDisposable_Dispose_mF5AE38C1BF625A1711988EA9BDB0257714C52C7C (void);
// 0x000002E3 System.Boolean AddItemSingle/<CleanTex>d__30::MoveNext()
extern void U3CCleanTexU3Ed__30_MoveNext_m645EE34876427ABCD7BF753E92AFAA6E46A6FEB5 (void);
// 0x000002E4 System.Object AddItemSingle/<CleanTex>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCleanTexU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4518055E3B70544495786AD3A5BB96DF4AA684C (void);
// 0x000002E5 System.Void AddItemSingle/<CleanTex>d__30::System.Collections.IEnumerator.Reset()
extern void U3CCleanTexU3Ed__30_System_Collections_IEnumerator_Reset_m569B1DADFD37C2B6E6EB20CDE72DA244FB9C72D9 (void);
// 0x000002E6 System.Object AddItemSingle/<CleanTex>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CCleanTexU3Ed__30_System_Collections_IEnumerator_get_Current_mECC037CFDCC6AC9FF87C1D02AAA62AA5D30465FD (void);
// 0x000002E7 System.Void AddItemSingle/<ImageAddingProcess>d__32::.ctor(System.Int32)
extern void U3CImageAddingProcessU3Ed__32__ctor_m854CE612288CB526EA88059B6A9075B463749D16 (void);
// 0x000002E8 System.Void AddItemSingle/<ImageAddingProcess>d__32::System.IDisposable.Dispose()
extern void U3CImageAddingProcessU3Ed__32_System_IDisposable_Dispose_mFF956FF062FDF19085C086812D7323C48DD38F74 (void);
// 0x000002E9 System.Boolean AddItemSingle/<ImageAddingProcess>d__32::MoveNext()
extern void U3CImageAddingProcessU3Ed__32_MoveNext_m280A77ED3E03D308DE0FBA73FE227850684B537C (void);
// 0x000002EA System.Object AddItemSingle/<ImageAddingProcess>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageAddingProcessU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0F9BCD72DD34F2D3C1E470C814BDEF4ABDD0018 (void);
// 0x000002EB System.Void AddItemSingle/<ImageAddingProcess>d__32::System.Collections.IEnumerator.Reset()
extern void U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_Reset_mDD12A1D00339C63FCE4232072A6A0140F2415C2E (void);
// 0x000002EC System.Object AddItemSingle/<ImageAddingProcess>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_get_Current_m4BA238ED68F161B93A9D59108075FB6AFC756CCE (void);
// 0x000002ED System.Void AddItemSingle/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m0612D1144698CE2522806F886E49F40A1A464A81 (void);
// 0x000002EE System.Void AddItemSingle/<>c__DisplayClass33_0::<InitPoPProduct>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__0_m57DA530FF6375A1BAA1F59DE421BD7A0EECCE6AC (void);
// 0x000002EF System.Void AddItemSingle/<>c__DisplayClass33_0::<InitPoPProduct>b__1()
extern void U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__1_m1FFA39C50375F9F577651AB200FB3BB8902F7986 (void);
// 0x000002F0 System.Collections.IEnumerator AddItemSingle/<>c__DisplayClass33_0::<InitPoPProduct>g__ViewLoad|2(System.String)
extern void U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eg__ViewLoadU7C2_mDE7ABBD9C4B3FEE0C399BE88F28C36D50796E910 (void);
// 0x000002F1 System.Boolean AddItemSingle/<>c__DisplayClass33_0::<InitPoPProduct>b__5()
extern void U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__5_mFD6A4DA88E5E6FD3B2D4DA714F304E0743BBB764 (void);
// 0x000002F2 System.Void AddItemSingle/<>c__DisplayClass33_0::<InitPoPProduct>b__3()
extern void U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__3_m29FB14662D7CDE3126944A612760E3881692269D (void);
// 0x000002F3 System.Void AddItemSingle/<>c__DisplayClass33_0::<InitPoPProduct>b__4()
extern void U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__4_mEE05507E522456AD1A80074B626DBD1F7C7577FD (void);
// 0x000002F4 System.Void AddItemSingle/<>c__DisplayClass33_0/<<InitPoPProduct>g__ViewLoad|2>d::.ctor(System.Int32)
extern void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed__ctor_mFFC5CDFA51BD0578917E8B7D206E899CCE14443E (void);
// 0x000002F5 System.Void AddItemSingle/<>c__DisplayClass33_0/<<InitPoPProduct>g__ViewLoad|2>d::System.IDisposable.Dispose()
extern void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_IDisposable_Dispose_mF6EF7AE7E4453DA1F88CFDC0B67151108164418E (void);
// 0x000002F6 System.Boolean AddItemSingle/<>c__DisplayClass33_0/<<InitPoPProduct>g__ViewLoad|2>d::MoveNext()
extern void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_MoveNext_mF768BB105B727E5A4CD93C6C25163F08C6C02B9F (void);
// 0x000002F7 System.Object AddItemSingle/<>c__DisplayClass33_0/<<InitPoPProduct>g__ViewLoad|2>d::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F167D0D324A297085FF7718472A0DA97810714E (void);
// 0x000002F8 System.Void AddItemSingle/<>c__DisplayClass33_0/<<InitPoPProduct>g__ViewLoad|2>d::System.Collections.IEnumerator.Reset()
extern void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_Reset_m51DA00B6F13F0F29EA10E6464B2994FC00F3A643 (void);
// 0x000002F9 System.Object AddItemSingle/<>c__DisplayClass33_0/<<InitPoPProduct>g__ViewLoad|2>d::System.Collections.IEnumerator.get_Current()
extern void U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_get_Current_mD147C9A9E2ADE062EE4942F87F7BB7448FBCA02C (void);
// 0x000002FA System.Void AddItemSingle/<>c::.cctor()
extern void U3CU3Ec__cctor_m50565980B3965B7F7C2DF68E957691AE32088475 (void);
// 0x000002FB System.Void AddItemSingle/<>c::.ctor()
extern void U3CU3Ec__ctor_mEAE64C8683B957A7D01117A6899C3883BC3E5753 (void);
// 0x000002FC System.Void AddItemSingle/<>c::<InitPoPProduct>b__33_7(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CInitPoPProductU3Eb__33_7_m8FC8BDC1E45B5F626257CF1FE2D123211E9DA009 (void);
// 0x000002FD System.Void AddItemSingle/<>c::<InitPoPProduct>b__33_6(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CInitPoPProductU3Eb__33_6_m86FC5D2852525D9C829F7402E646973D6EB4CF4A (void);
// 0x000002FE System.Void AppManager::Awake()
extern void AppManager_Awake_m35745A5427AB4F00226708C1ACA35EB14B3DA4DE (void);
// 0x000002FF System.Void AppManager::ChangeScene(System.String)
extern void AppManager_ChangeScene_m211A3C5332E6B6D757DCC0CAC7520BB7F3E047B7 (void);
// 0x00000300 System.Void AppManager::.ctor()
extern void AppManager__ctor_mC3FF75565A6362EEBDB47070724D891877AE0414 (void);
// 0x00000301 System.Void ArUI::Start()
extern void ArUI_Start_m5C1693987688735C0ECC8F023FFA3FCA9A08DC9C (void);
// 0x00000302 System.Void ArUI::Update()
extern void ArUI_Update_m401347E473630CCB2F8FA9B4E63D880C83C5879C (void);
// 0x00000303 System.Void ArUI::.ctor()
extern void ArUI__ctor_m1E8D1D5C1A2BE44DE233095C44802530A32D473C (void);
// 0x00000304 System.Void AuthLogIn::Start()
extern void AuthLogIn_Start_mCCDAB431452883F34D5762E8C8BE5ACC229AB45A (void);
// 0x00000305 System.Void AuthLogIn::.ctor()
extern void AuthLogIn__ctor_mAB140E1C7504158E901FE1FD0F71499DB47FF567 (void);
// 0x00000306 System.Void AuthLogIn::<Start>b__5_0(System.String)
extern void AuthLogIn_U3CStartU3Eb__5_0_mD050B6932D242E0CC487AA1625BC28B7F8973C25 (void);
// 0x00000307 System.Void AuthLogIn::<Start>b__5_1(System.String)
extern void AuthLogIn_U3CStartU3Eb__5_1_mA894BEB560F0D5DE6DC808C3EBDC62F0C38DD8F1 (void);
// 0x00000308 System.Void AuthUIManager::Awake()
extern void AuthUIManager_Awake_mD990CE815FBB3A184C5DB3FD903ABD6312109DCB (void);
// 0x00000309 System.Void AuthUIManager::ClearUI()
extern void AuthUIManager_ClearUI_m3144B66FB67DD933439BD02485B5C373B1AAAEBD (void);
// 0x0000030A System.Void AuthUIManager::LoginScreen()
extern void AuthUIManager_LoginScreen_mDE39A1393B6AC2A27081C5CB6B594E015F8C1F9A (void);
// 0x0000030B System.Void AuthUIManager::RegisterScreen()
extern void AuthUIManager_RegisterScreen_mF56965E534E0CAA79FB6470E81D9E87D74875F4F (void);
// 0x0000030C System.Void AuthUIManager::.ctor()
extern void AuthUIManager__ctor_mBD40733306150C7E895EC0515DC6611365B29CE0 (void);
// 0x0000030D UnityEngine.Transform BoneController::get_skeletonRoot()
extern void BoneController_get_skeletonRoot_m5B82500607091C4733CDA82A10439B15952C2209 (void);
// 0x0000030E System.Void BoneController::set_skeletonRoot(UnityEngine.Transform)
extern void BoneController_set_skeletonRoot_m238DE02D1A37978A54902DCBA560E4ABBF54588C (void);
// 0x0000030F System.Void BoneController::InitializeSkeletonJoints()
extern void BoneController_InitializeSkeletonJoints_m4C6352B4B25AC75A6A0FBD16E1A9BAE78A25DC3F (void);
// 0x00000310 System.Void BoneController::ApplyBodyPose(UnityEngine.XR.ARFoundation.ARHumanBody)
extern void BoneController_ApplyBodyPose_m882A8342657FF17B71197534E2384CC1D8ED56CB (void);
// 0x00000311 System.Void BoneController::ApplyBodyPoseLeg(UnityEngine.XR.ARFoundation.ARHumanBody)
extern void BoneController_ApplyBodyPoseLeg_m40B592C09BD9E928AE268E9DA85261A609089DB9 (void);
// 0x00000312 System.Void BoneController::ProcessJoint(UnityEngine.Transform)
extern void BoneController_ProcessJoint_mE568E4706A98EB4CB10CF23A80732016F2B1F78F (void);
// 0x00000313 System.Int32 BoneController::GetJointIndex(System.String)
extern void BoneController_GetJointIndex_m559746F37FCE21E55B6A3873BB60DDDD7B132C29 (void);
// 0x00000314 System.Void BoneController::.ctor()
extern void BoneController__ctor_m03D53E58E7511A5F9332BCDCB403C3DC344F17F3 (void);
// 0x00000315 UnityEngine.Color ColorExtensions::ParseColor(System.String)
extern void ColorExtensions_ParseColor_m8AF37B4F2A3A2747F9B4C6734B82E33819D8FB04 (void);
// 0x00000316 System.Void CreateAcc::Start()
extern void CreateAcc_Start_mC3CA1FC10BA390F09CCAC85BC9AB73EE5FB8F145 (void);
// 0x00000317 System.Void CreateAcc::Update()
extern void CreateAcc_Update_m111D838F8FBBFD47317DED27E9402ADC513A675A (void);
// 0x00000318 System.Void CreateAcc::.ctor()
extern void CreateAcc__ctor_m91C1A7EE03DF89FACA5618F8D427F5223688D064 (void);
// 0x00000319 System.Void CreateAcc::<Start>b__14_0(System.String)
extern void CreateAcc_U3CStartU3Eb__14_0_m8493AE06DE46BB2D14988EFCCED4A6F704ED723A (void);
// 0x0000031A System.Void CreateAcc::<Start>b__14_1(System.String)
extern void CreateAcc_U3CStartU3Eb__14_1_m279DBDEDDFFE381FDAD7941FCAE37CB8C80CEE77 (void);
// 0x0000031B System.Void CreateAcc::<Start>b__14_2(System.String)
extern void CreateAcc_U3CStartU3Eb__14_2_m4C82B7B7FFC9F7DE9D05BBC79E8094FEE9B59881 (void);
// 0x0000031C System.Void CreateAcc::<Start>b__14_3(System.Int32)
extern void CreateAcc_U3CStartU3Eb__14_3_m0EDEE6B256DBFDCE07F6557CCB638B68C1C4F081 (void);
// 0x0000031D System.Void CreateAcc::<Start>b__14_4(System.Int32)
extern void CreateAcc_U3CStartU3Eb__14_4_mFCD9F25CEA26FAB2B305E94A54D9DF59591EF696 (void);
// 0x0000031E System.Void CreateAcc::<Start>b__14_5(System.Int32)
extern void CreateAcc_U3CStartU3Eb__14_5_m1AEDA456989B62CAAD3DC6FD724F8F321182ECB4 (void);
// 0x0000031F System.Void CreateAcc::<Start>b__14_6()
extern void CreateAcc_U3CStartU3Eb__14_6_mADFF5EB61973959631C2F5FD92E7D65D5D99B8F3 (void);
// 0x00000320 System.Void DropdownFilOverride::Start()
extern void DropdownFilOverride_Start_m693F77E324752D47FD3CBB9635250E490D1508C8 (void);
// 0x00000321 System.Void DropdownFilOverride::dropFil_element()
extern void DropdownFilOverride_dropFil_element_mC1E99AEBCDB4B3996C62217FF11A9A719FB09A11 (void);
// 0x00000322 System.Void DropdownFilOverride::Update()
extern void DropdownFilOverride_Update_m8DD4A01B9A76CAB644DAF54C5D47E81400F6CABE (void);
// 0x00000323 System.Void DropdownFilOverride::.ctor()
extern void DropdownFilOverride__ctor_m92B8221BF5E5BD84A0675E87501A01B80349FEED (void);
// 0x00000324 System.Void DropdownOverride::.ctor()
extern void DropdownOverride__ctor_mAD31A876B28DEB8E0E207F3D87336E12703CD24D (void);
// 0x00000325 System.Void DrpDwn::Start()
extern void DrpDwn_Start_mB59FEF775377D16CA9C4E1F6C0600BB69613F4A0 (void);
// 0x00000326 System.Collections.IEnumerator DrpDwn::UpdateInfo()
extern void DrpDwn_UpdateInfo_m1233C2A1B3EAD9AACFA1A17F6ADE9950491F9A2E (void);
// 0x00000327 System.Void DrpDwn::Update()
extern void DrpDwn_Update_mA1A8CBD7317E058FD9072484A8AE865810D0F4DD (void);
// 0x00000328 System.Void DrpDwn::.ctor()
extern void DrpDwn__ctor_m3968EB3B7139B3763E01BFF3D06140AEC7468D7B (void);
// 0x00000329 System.Void DrpDwn::<Start>b__16_0(System.Int32)
extern void DrpDwn_U3CStartU3Eb__16_0_mD5E5F262F4764E4BDD121F758249F5B07B318230 (void);
// 0x0000032A System.Void DrpDwn::<Start>b__16_1(System.Int32)
extern void DrpDwn_U3CStartU3Eb__16_1_m4C1733F6C750ACFCA947DDDF8E0C70C3F3197A98 (void);
// 0x0000032B System.Void DrpDwn::<Start>b__16_2()
extern void DrpDwn_U3CStartU3Eb__16_2_mAA43DE9BCFE604E3E8ABAF9F8B3D390205896289 (void);
// 0x0000032C System.Void DrpDwn/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m708B6233367F4C12D335BE051D75F9ADE1D6227D (void);
// 0x0000032D System.Boolean DrpDwn/<>c__DisplayClass17_0::<UpdateInfo>b__1()
extern void U3CU3Ec__DisplayClass17_0_U3CUpdateInfoU3Eb__1_m5BAFBB114542688082C094F29F592A23812053EA (void);
// 0x0000032E System.Void DrpDwn/<>c::.cctor()
extern void U3CU3Ec__cctor_m9B85CF271DF6B6AA8FAF16B1078DAE6617C47D5A (void);
// 0x0000032F System.Void DrpDwn/<>c::.ctor()
extern void U3CU3Ec__ctor_m9AB5A708036223B5B520102E9D70EC9754E03FAF (void);
// 0x00000330 System.Void DrpDwn/<>c::<UpdateInfo>b__17_0(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CUpdateInfoU3Eb__17_0_m34DC8AF7991BB58BD0F6BA09B9F19D5BBC9EC2AB (void);
// 0x00000331 System.Void DrpDwn/<UpdateInfo>d__17::.ctor(System.Int32)
extern void U3CUpdateInfoU3Ed__17__ctor_m3FEF25B5AD5BC513EA9D5F77465BD37D6D43EB50 (void);
// 0x00000332 System.Void DrpDwn/<UpdateInfo>d__17::System.IDisposable.Dispose()
extern void U3CUpdateInfoU3Ed__17_System_IDisposable_Dispose_m16C5556EAC8EDC7494F39C587FD33B54F4AAD8B7 (void);
// 0x00000333 System.Boolean DrpDwn/<UpdateInfo>d__17::MoveNext()
extern void U3CUpdateInfoU3Ed__17_MoveNext_mAE7E97DDB0C937FACBD071F4573BE308BE3946A7 (void);
// 0x00000334 System.Object DrpDwn/<UpdateInfo>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateInfoU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB4C894555B34B6737C66E1D17ECBE9DB1C7927F (void);
// 0x00000335 System.Void DrpDwn/<UpdateInfo>d__17::System.Collections.IEnumerator.Reset()
extern void U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_Reset_m253F46F270B3551A1D33007329863296A8A95CAC (void);
// 0x00000336 System.Object DrpDwn/<UpdateInfo>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_get_Current_mA27DF123CD6808FFCFF01A6410EDF7D56732C434 (void);
// 0x00000337 System.Void DrpDwnReg::Start()
extern void DrpDwnReg_Start_mFEF5C8C42821D9A3773FFED5D26D1DE623D08972 (void);
// 0x00000338 System.Void DrpDwnReg::Update()
extern void DrpDwnReg_Update_mBD537603E6CF02FCC872ACF27866355AF8B92C4C (void);
// 0x00000339 System.Void DrpDwnReg::.ctor()
extern void DrpDwnReg__ctor_mB4C7C9C44BB384B2F51A9FEB729D628D3CF4EAA0 (void);
// 0x0000033A System.Void DrpDwnReg::<Start>b__10_0(System.Int32)
extern void DrpDwnReg_U3CStartU3Eb__10_0_m37F2FF9FA3B44096345696AB66223D8869443F40 (void);
// 0x0000033B System.Void FirebaseLoad::Awake()
extern void FirebaseLoad_Awake_mC63CF9108E4161DE0BB12593F7087BCA047B7097 (void);
// 0x0000033C System.Void FirebaseLoad::Start()
extern void FirebaseLoad_Start_m0088E3710F313558FAFB3467B7708B3AB0C3CCE7 (void);
// 0x0000033D System.Void FirebaseLoad::OnDisable()
extern void FirebaseLoad_OnDisable_m77C2F7C466551479EA70AF45BC7257CB24C87F9C (void);
// 0x0000033E System.Void FirebaseLoad::ARDocumentsBatch(Firebase.Firestore.DocumentSnapshot,System.Int32)
extern void FirebaseLoad_ARDocumentsBatch_m35E94991AB7C239E5A7BB71365565F95739CECDA (void);
// 0x0000033F System.Void FirebaseLoad::ARDocumentsFav()
extern void FirebaseLoad_ARDocumentsFav_mF54A7E214DDFC9BAD4729EF83F21C888270F499B (void);
// 0x00000340 System.Collections.IEnumerator FirebaseLoad::ARDocsFV()
extern void FirebaseLoad_ARDocsFV_m723E735998B8B4D20AAA96EB69FAC26E117084B5 (void);
// 0x00000341 System.Void FirebaseLoad::ARDocumentsFiltered(Firebase.Firestore.DocumentSnapshot,System.Int32,System.String)
extern void FirebaseLoad_ARDocumentsFiltered_mC55EBF1FE6F06BDF5CB12E5E025CDDAB2E00C028 (void);
// 0x00000342 System.Void FirebaseLoad::ARDocuments(Firebase.Firestore.DocumentSnapshot)
extern void FirebaseLoad_ARDocuments_mAA4A6E5B0D5D927755DD7E36822869D02F1BCD3B (void);
// 0x00000343 System.Collections.IEnumerator FirebaseLoad::ARDoc(System.Int32,System.String,Firebase.Firestore.DocumentSnapshot)
extern void FirebaseLoad_ARDoc_mE3C0EA4A11B5C67EF5AF8BAFD0D5BEA999FBB461 (void);
// 0x00000344 System.Void FirebaseLoad::.ctor()
extern void FirebaseLoad__ctor_m240A0324125E79A183E90EB3BE3108F495A660E1 (void);
// 0x00000345 System.Void FirebaseLoad/OnQueryfinished::.ctor(System.Object,System.IntPtr)
extern void OnQueryfinished__ctor_mB70BB12076A977EBC8CB0B9BC4E386D731B510ED (void);
// 0x00000346 System.Void FirebaseLoad/OnQueryfinished::Invoke(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Int32,System.Int32,System.Collections.Generic.List`1<System.String>,Firebase.Firestore.DocumentSnapshot,System.Boolean,System.Collections.Generic.List`1<System.String>)
extern void OnQueryfinished_Invoke_m68ACCBE29D78BF8DD1DAA66D9E12C0BA9202329F (void);
// 0x00000347 System.IAsyncResult FirebaseLoad/OnQueryfinished::BeginInvoke(System.Collections.Generic.Dictionary`2<System.ValueTuple`2<System.String,System.String>,System.Object>,System.Int32,System.Int32,System.Collections.Generic.List`1<System.String>,Firebase.Firestore.DocumentSnapshot,System.Boolean,System.Collections.Generic.List`1<System.String>,System.AsyncCallback,System.Object)
extern void OnQueryfinished_BeginInvoke_mAE3CEDC4B64439CA659F52C765AECF4757EDBED5 (void);
// 0x00000348 System.Void FirebaseLoad/OnQueryfinished::EndInvoke(System.IAsyncResult)
extern void OnQueryfinished_EndInvoke_mA343E71E3956036C3A5ED5161A2B1AE839810A9B (void);
// 0x00000349 System.Void FirebaseLoad/OnInitPagingRequested::.ctor(System.Object,System.IntPtr)
extern void OnInitPagingRequested__ctor_mA397F96C548C4D24388D301EB67E28FF9337F9D1 (void);
// 0x0000034A System.Void FirebaseLoad/OnInitPagingRequested::Invoke(System.Int32,System.Int32,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void OnInitPagingRequested_Invoke_m6134C247A7A3F7478EFA909EB61DCD78143A0902 (void);
// 0x0000034B System.IAsyncResult FirebaseLoad/OnInitPagingRequested::BeginInvoke(System.Int32,System.Int32,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.AsyncCallback,System.Object)
extern void OnInitPagingRequested_BeginInvoke_m4EA59ABA2CCFCB9665711A7E402E229D04E9951D (void);
// 0x0000034C System.Void FirebaseLoad/OnInitPagingRequested::EndInvoke(System.IAsyncResult)
extern void OnInitPagingRequested_EndInvoke_m0378A727CED84A656F27F68F8CA1899614F03625 (void);
// 0x0000034D System.Void FirebaseLoad/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_mEFF7B45AAFB9ED8C5F039C28FAC844A3EC0BD1BA (void);
// 0x0000034E System.Void FirebaseLoad/<>c__DisplayClass30_0::<ARDocsFV>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__0_m2540B2DCAB7CCDA30F2C8E7E0E74AA0B363F8153 (void);
// 0x0000034F System.Boolean FirebaseLoad/<>c__DisplayClass30_0::<ARDocsFV>b__1()
extern void U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__1_m3C6454FC7CE2D2A40EB8EEF60E96461FF4D03F5D (void);
// 0x00000350 System.Void FirebaseLoad/<>c__DisplayClass30_0::<ARDocsFV>b__2(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__2_m6CECE7F640BC3B169C20DE542025D6D8BD03F767 (void);
// 0x00000351 System.Boolean FirebaseLoad/<>c__DisplayClass30_0::<ARDocsFV>b__3()
extern void U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__3_mFA6D63BE477DD57E38C2838458576A5472D17578 (void);
// 0x00000352 System.Void FirebaseLoad/<>c__DisplayClass30_0::<ARDocsFV>b__4(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__4_m1A5A93ED6712814514642B7028A023137CF513E6 (void);
// 0x00000353 System.Void FirebaseLoad/<>c__DisplayClass30_1::.ctor()
extern void U3CU3Ec__DisplayClass30_1__ctor_m0582C841C43C42790BFDE3388C7E66EC3EB85624 (void);
// 0x00000354 System.Boolean FirebaseLoad/<>c__DisplayClass30_1::<ARDocsFV>b__5()
extern void U3CU3Ec__DisplayClass30_1_U3CARDocsFVU3Eb__5_m5A64AC4EF00D12AB72947375D358420FCE34CC1B (void);
// 0x00000355 System.Void FirebaseLoad/<ARDocsFV>d__30::.ctor(System.Int32)
extern void U3CARDocsFVU3Ed__30__ctor_m75CDDE36D3FFCC64B53247BA6FFF09812B19B611 (void);
// 0x00000356 System.Void FirebaseLoad/<ARDocsFV>d__30::System.IDisposable.Dispose()
extern void U3CARDocsFVU3Ed__30_System_IDisposable_Dispose_mCA30BA2BD4B04206E965F6EB72F13B58B4774741 (void);
// 0x00000357 System.Boolean FirebaseLoad/<ARDocsFV>d__30::MoveNext()
extern void U3CARDocsFVU3Ed__30_MoveNext_mD4D17D5E59DB6B735841D339B3127145DD8E04A8 (void);
// 0x00000358 System.Void FirebaseLoad/<ARDocsFV>d__30::<>m__Finally1()
extern void U3CARDocsFVU3Ed__30_U3CU3Em__Finally1_mFF6D0D30C770C2B83F36E70D3470F18650F3F97F (void);
// 0x00000359 System.Object FirebaseLoad/<ARDocsFV>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CARDocsFVU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5700A999E0CADC5394B66BAAEC398D681B28FDC0 (void);
// 0x0000035A System.Void FirebaseLoad/<ARDocsFV>d__30::System.Collections.IEnumerator.Reset()
extern void U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_Reset_m133F4D7C73219BD846D577CA416A3FD3E71F788B (void);
// 0x0000035B System.Object FirebaseLoad/<ARDocsFV>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_get_Current_mA1F3A732F1D8F4FA9FD6BF4BBC3ED3E58DED5292 (void);
// 0x0000035C System.Void FirebaseLoad/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m17DF0A022A73DF5C8D1EFA526CA38A6BA3FAE46D (void);
// 0x0000035D System.Void FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__0_m604B7351582E22966F31C7A27ABF3AAD3B0A6E3C (void);
// 0x0000035E System.Boolean FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__1()
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__1_m0CB464F55F50FFEBF4234D90B91BDA0DE9DCADA0 (void);
// 0x0000035F System.Void FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__2(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__2_m85182A04FCB7D9BF944D5935CC6EBF6B26B92572 (void);
// 0x00000360 System.Boolean FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__3()
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__3_mD64FED2F46D5B02CF2332227F9CF217391F3E85B (void);
// 0x00000361 System.Void FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__4(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__4_m848D6C51B44A4A8474FA82A31ECAF51841B92892 (void);
// 0x00000362 System.Void FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__6(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__6_m7AEA77BBF2AC33DD9DD758A5DE5C2193BA30A91E (void);
// 0x00000363 System.Void FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__8(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__8_m3CF03E60DE4E44FF3197D25B53847264ACC94856 (void);
// 0x00000364 System.Void FirebaseLoad/<>c__DisplayClass33_0::<ARDoc>b__10(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__10_mA38F9AFE9B2156854A56936C7EB9D56E7AE771EE (void);
// 0x00000365 System.Void FirebaseLoad/<>c__DisplayClass33_1::.ctor()
extern void U3CU3Ec__DisplayClass33_1__ctor_mBD9D37F7AE2B37ED2150AFED8A3F5082812D4682 (void);
// 0x00000366 System.Boolean FirebaseLoad/<>c__DisplayClass33_1::<ARDoc>b__5()
extern void U3CU3Ec__DisplayClass33_1_U3CARDocU3Eb__5_mD45E959A29F4180227789559CF0FB2EE6D30722F (void);
// 0x00000367 System.Void FirebaseLoad/<>c__DisplayClass33_2::.ctor()
extern void U3CU3Ec__DisplayClass33_2__ctor_mF1F9382B63CB796BAC1AFF5E06435CD3100E5350 (void);
// 0x00000368 System.Boolean FirebaseLoad/<>c__DisplayClass33_2::<ARDoc>b__7()
extern void U3CU3Ec__DisplayClass33_2_U3CARDocU3Eb__7_mA19D7EBA9D984427C8DF357D712F52AA0E195B45 (void);
// 0x00000369 System.Void FirebaseLoad/<>c__DisplayClass33_3::.ctor()
extern void U3CU3Ec__DisplayClass33_3__ctor_m8B0B87D4BD0635AE7EDE982B3505CE3B59928398 (void);
// 0x0000036A System.Boolean FirebaseLoad/<>c__DisplayClass33_3::<ARDoc>b__9()
extern void U3CU3Ec__DisplayClass33_3_U3CARDocU3Eb__9_mE4025D9CAB06A18151493AFF0484393BB599C4FC (void);
// 0x0000036B System.Void FirebaseLoad/<>c__DisplayClass33_4::.ctor()
extern void U3CU3Ec__DisplayClass33_4__ctor_mA36D32BDDF0E5996702C83F124D5BB37504A8658 (void);
// 0x0000036C System.Boolean FirebaseLoad/<>c__DisplayClass33_4::<ARDoc>b__11()
extern void U3CU3Ec__DisplayClass33_4_U3CARDocU3Eb__11_m82E32590913D04D56076B6A8E023F640A18F6071 (void);
// 0x0000036D System.Void FirebaseLoad/<ARDoc>d__33::.ctor(System.Int32)
extern void U3CARDocU3Ed__33__ctor_m960E0A911B1A28A79ACDB501FAC11582CDC61C74 (void);
// 0x0000036E System.Void FirebaseLoad/<ARDoc>d__33::System.IDisposable.Dispose()
extern void U3CARDocU3Ed__33_System_IDisposable_Dispose_m1F7771C8269333EDF80C8A0A9BC6900F7026A6B1 (void);
// 0x0000036F System.Boolean FirebaseLoad/<ARDoc>d__33::MoveNext()
extern void U3CARDocU3Ed__33_MoveNext_mE1547D2BD77420BEBEAABE476FCFD1F8C52437A8 (void);
// 0x00000370 System.Void FirebaseLoad/<ARDoc>d__33::<>m__Finally1()
extern void U3CARDocU3Ed__33_U3CU3Em__Finally1_mA8EF6ABAB6E2CB04BC10F5EA5605EE8365EA4878 (void);
// 0x00000371 System.Object FirebaseLoad/<ARDoc>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CARDocU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A477DE8915FCE6C5B41631BBF0A1149DF46AA5E (void);
// 0x00000372 System.Void FirebaseLoad/<ARDoc>d__33::System.Collections.IEnumerator.Reset()
extern void U3CARDocU3Ed__33_System_Collections_IEnumerator_Reset_m829D659EDD2CDD8D554D68CA96FFBFB260607642 (void);
// 0x00000373 System.Object FirebaseLoad/<ARDoc>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CARDocU3Ed__33_System_Collections_IEnumerator_get_Current_m488B0F68F860C0A9E1F27F591DD7E840B49E9DE5 (void);
// 0x00000374 System.Void FirebaseLoadProduct::Start()
extern void FirebaseLoadProduct_Start_m867F13B7891AFD714353472D68564F30E5131C3D (void);
// 0x00000375 System.Void FirebaseLoadProduct::OnDisable()
extern void FirebaseLoadProduct_OnDisable_mCB0C639E1FED4D4F21C638E85BE0FD2E3A3F8D86 (void);
// 0x00000376 System.Void FirebaseLoadProduct::SingleDocument(System.String)
extern void FirebaseLoadProduct_SingleDocument_m135FD585173A4DD4EAC0A592A06C50D41F1CB278 (void);
// 0x00000377 System.Collections.IEnumerator FirebaseLoadProduct::SDoc(System.String)
extern void FirebaseLoadProduct_SDoc_m3568BA13939C82452402248225F7027FAC287E6D (void);
// 0x00000378 System.Void FirebaseLoadProduct::Update()
extern void FirebaseLoadProduct_Update_m2176533BD75B69ED011965BFCE2B9C7E7DA5F146 (void);
// 0x00000379 System.Void FirebaseLoadProduct::.ctor()
extern void FirebaseLoadProduct__ctor_m2AD37FCFCB654C853CBDDBDDDC855DA59DFE3439 (void);
// 0x0000037A System.Void FirebaseLoadProduct/OnSingleQueryfinished::.ctor(System.Object,System.IntPtr)
extern void OnSingleQueryfinished__ctor_m73FDE9CF1F76950F048FF400A1677A58A6114CE6 (void);
// 0x0000037B System.Void FirebaseLoadProduct/OnSingleQueryfinished::Invoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void OnSingleQueryfinished_Invoke_mA08C46CCB92EE2A0829EB5B73B434C8F459BF122 (void);
// 0x0000037C System.IAsyncResult FirebaseLoadProduct/OnSingleQueryfinished::BeginInvoke(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.AsyncCallback,System.Object)
extern void OnSingleQueryfinished_BeginInvoke_m9F6792FF057D653E066D601F1F504D2443F4A112 (void);
// 0x0000037D System.Void FirebaseLoadProduct/OnSingleQueryfinished::EndInvoke(System.IAsyncResult)
extern void OnSingleQueryfinished_EndInvoke_m14B091EC4A2378F13A2DA9AB7EA0DFF40B96ED0C (void);
// 0x0000037E System.Void FirebaseLoadProduct/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m00F5C4A61FE968E1413D8F898E78D3EC1265CC01 (void);
// 0x0000037F System.Void FirebaseLoadProduct/<>c__DisplayClass6_0::<SDoc>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass6_0_U3CSDocU3Eb__0_m11CF0C98D671A9EFB5F3D1A42AF11114ABD35F38 (void);
// 0x00000380 System.Boolean FirebaseLoadProduct/<>c__DisplayClass6_0::<SDoc>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CSDocU3Eb__1_m3A6322470C611410B29B234B2DA3D69A3258203B (void);
// 0x00000381 System.Void FirebaseLoadProduct/<SDoc>d__6::.ctor(System.Int32)
extern void U3CSDocU3Ed__6__ctor_m11F634D3BA0FE2F0429A352773685482007C1CA9 (void);
// 0x00000382 System.Void FirebaseLoadProduct/<SDoc>d__6::System.IDisposable.Dispose()
extern void U3CSDocU3Ed__6_System_IDisposable_Dispose_m45D6F15DC5EFC39587FBFEC0379E79580C78CB1A (void);
// 0x00000383 System.Boolean FirebaseLoadProduct/<SDoc>d__6::MoveNext()
extern void U3CSDocU3Ed__6_MoveNext_mDA232EBE086D4D278AF2E7E48620235A0BCE041D (void);
// 0x00000384 System.Object FirebaseLoadProduct/<SDoc>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSDocU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FDA9AE366B9131C3E4028C87E0F19AC0E02DFB5 (void);
// 0x00000385 System.Void FirebaseLoadProduct/<SDoc>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSDocU3Ed__6_System_Collections_IEnumerator_Reset_m307C49A52A70B1F44B5E4422D634A0CF460E5285 (void);
// 0x00000386 System.Object FirebaseLoadProduct/<SDoc>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSDocU3Ed__6_System_Collections_IEnumerator_get_Current_m19E7A850C40FA8D83DB0085945AB5D14CDF3F34B (void);
// 0x00000387 System.Void FirebaseManager::Awake()
extern void FirebaseManager_Awake_m0FB28B77E6B0665F486CD8B13EDAD174FDC911CA (void);
// 0x00000388 System.Void FirebaseManager::Start()
extern void FirebaseManager_Start_mC613ACB1AF5C9327DFD21634676C02D3991E5571 (void);
// 0x00000389 System.Collections.IEnumerator FirebaseManager::CheckAndFixDependencies()
extern void FirebaseManager_CheckAndFixDependencies_mCDC8E2754EC8ECC9B3E0DC26278D64E9077899C1 (void);
// 0x0000038A System.Void FirebaseManager::InitializeFirebase()
extern void FirebaseManager_InitializeFirebase_m0AB60DA55A5299AEFEEB03BAA7AAAA2CEDB11E90 (void);
// 0x0000038B System.Collections.IEnumerator FirebaseManager::checkAutoLogin()
extern void FirebaseManager_checkAutoLogin_mF4122E6817DA0E8B51DB444A81149C836D75F1CE (void);
// 0x0000038C System.Void FirebaseManager::AutoLogin()
extern void FirebaseManager_AutoLogin_m305C571F91FA74478971350489E6A8EEADCEF97A (void);
// 0x0000038D System.Void FirebaseManager::AuthStateChanged(System.Object,System.EventArgs)
extern void FirebaseManager_AuthStateChanged_m918C661027778087914DAF004BDD83A730CD7F3B (void);
// 0x0000038E System.Void FirebaseManager::ClearOutputs()
extern void FirebaseManager_ClearOutputs_mDE479EB062FCBBC65991145EA585E789EA15D00B (void);
// 0x0000038F System.Void FirebaseManager::LoginButton()
extern void FirebaseManager_LoginButton_m6641E5EC1D5B8447CBB05DA37E7C3DDADCED23C7 (void);
// 0x00000390 System.Void FirebaseManager::LogOut()
extern void FirebaseManager_LogOut_mA9A19730AF4F90ED134AA731AC1EA88AA33FFB5F (void);
// 0x00000391 System.Void FirebaseManager::RegisterButton()
extern void FirebaseManager_RegisterButton_mB929DC64C8D07811786BB11AA419EC7D90C99473 (void);
// 0x00000392 System.Collections.IEnumerator FirebaseManager::LoginLogic(System.String,System.String)
extern void FirebaseManager_LoginLogic_m57B61E650C749A3D740CE8E03A35F1F099EF9A4F (void);
// 0x00000393 System.Collections.IEnumerator FirebaseManager::RegisterLogic(System.String,System.String,System.String,System.Int32,System.Int32,System.Boolean)
extern void FirebaseManager_RegisterLogic_m30CBFD9C2A3A498E6A7427FC73DA16B03DDB80E6 (void);
// 0x00000394 System.Void FirebaseManager::.ctor()
extern void FirebaseManager__ctor_mB894832C008369C3C25D3B0DF9CDF84481C89506 (void);
// 0x00000395 System.Void FirebaseManager/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m4A4880E34501BD9BAA4AC205BBB9CA8A34FDBBE7 (void);
// 0x00000396 System.Boolean FirebaseManager/<>c__DisplayClass20_0::<CheckAndFixDependencies>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CCheckAndFixDependenciesU3Eb__0_m96829A9A6B434C1D4590E6B410FE2254181BAF8E (void);
// 0x00000397 System.Void FirebaseManager/<CheckAndFixDependencies>d__20::.ctor(System.Int32)
extern void U3CCheckAndFixDependenciesU3Ed__20__ctor_mE513235E9CEFAD28389F4BD273B0234A2546E9A6 (void);
// 0x00000398 System.Void FirebaseManager/<CheckAndFixDependencies>d__20::System.IDisposable.Dispose()
extern void U3CCheckAndFixDependenciesU3Ed__20_System_IDisposable_Dispose_m50897C509C5058F4E638DB16A8BCD88C2262F6AF (void);
// 0x00000399 System.Boolean FirebaseManager/<CheckAndFixDependencies>d__20::MoveNext()
extern void U3CCheckAndFixDependenciesU3Ed__20_MoveNext_mF3BDC7CFC43BBF25CA6D1DBCD85ECBF70F984389 (void);
// 0x0000039A System.Object FirebaseManager/<CheckAndFixDependencies>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAndFixDependenciesU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66C8BEDCAFFA24358C0D1DCA597C7DD35CF55F50 (void);
// 0x0000039B System.Void FirebaseManager/<CheckAndFixDependencies>d__20::System.Collections.IEnumerator.Reset()
extern void U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_Reset_m0ADA09A0EE9657765B6A84B223956B114B8A8479 (void);
// 0x0000039C System.Object FirebaseManager/<CheckAndFixDependencies>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_get_Current_m1FC420C47DEF8F1B4127A3E12EB95BB1D8AD6AB5 (void);
// 0x0000039D System.Void FirebaseManager/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mA7C9228D96EBF0E9677310B79C7DE350B63F7C08 (void);
// 0x0000039E System.Boolean FirebaseManager/<>c__DisplayClass22_0::<checkAutoLogin>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CcheckAutoLoginU3Eb__0_m29841C8B6A8500AFAC2AB94B5B286237D1817ED6 (void);
// 0x0000039F System.Void FirebaseManager/<checkAutoLogin>d__22::.ctor(System.Int32)
extern void U3CcheckAutoLoginU3Ed__22__ctor_m872281A15157F52029A9E8A929C8AC92CBA9C840 (void);
// 0x000003A0 System.Void FirebaseManager/<checkAutoLogin>d__22::System.IDisposable.Dispose()
extern void U3CcheckAutoLoginU3Ed__22_System_IDisposable_Dispose_mF912F49AC2DCF9900F49A2BBCF2FA99834A0F8B2 (void);
// 0x000003A1 System.Boolean FirebaseManager/<checkAutoLogin>d__22::MoveNext()
extern void U3CcheckAutoLoginU3Ed__22_MoveNext_m5273699201C45721E6EF9FE67635E488BFA4E007 (void);
// 0x000003A2 System.Object FirebaseManager/<checkAutoLogin>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CcheckAutoLoginU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99FE1B44D48593D6CA001BBDCD7F9857C6665444 (void);
// 0x000003A3 System.Void FirebaseManager/<checkAutoLogin>d__22::System.Collections.IEnumerator.Reset()
extern void U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_Reset_mDBD4A544E27C49C79F8AD57FC51E2F9818F0AB0B (void);
// 0x000003A4 System.Object FirebaseManager/<checkAutoLogin>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_get_Current_mB0390E85E68A1251F204074331BB25608E431ACF (void);
// 0x000003A5 System.Void FirebaseManager/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_m1A677314734F28E27D7AA62FB1767E6834E81BCE (void);
// 0x000003A6 System.Boolean FirebaseManager/<>c__DisplayClass29_0::<LoginLogic>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CLoginLogicU3Eb__0_m1639EE91752399394EFF76E024753335C1EFAE74 (void);
// 0x000003A7 System.Void FirebaseManager/<LoginLogic>d__29::.ctor(System.Int32)
extern void U3CLoginLogicU3Ed__29__ctor_m237FCABA55986243AE341530B887A7B9CFECF3B2 (void);
// 0x000003A8 System.Void FirebaseManager/<LoginLogic>d__29::System.IDisposable.Dispose()
extern void U3CLoginLogicU3Ed__29_System_IDisposable_Dispose_m56262A632D7BBE88A4049CDE4E61400A42DE5411 (void);
// 0x000003A9 System.Boolean FirebaseManager/<LoginLogic>d__29::MoveNext()
extern void U3CLoginLogicU3Ed__29_MoveNext_m6475088819A91587313B4114925295334B950159 (void);
// 0x000003AA System.Object FirebaseManager/<LoginLogic>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoginLogicU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B518C7141CFA0652289ECD3B525D0285655B7D1 (void);
// 0x000003AB System.Void FirebaseManager/<LoginLogic>d__29::System.Collections.IEnumerator.Reset()
extern void U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_Reset_m7A22EA515F8C3136ED8677F16282E19F0D98DDD8 (void);
// 0x000003AC System.Object FirebaseManager/<LoginLogic>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_get_Current_mCBABEFDD04C2BC6495F7DB8329606E5E19172F38 (void);
// 0x000003AD System.Void FirebaseManager/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m6C802D47CB88DAADF4DB64FB2E8C64D8C9F9F277 (void);
// 0x000003AE System.Boolean FirebaseManager/<>c__DisplayClass30_0::<RegisterLogic>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CRegisterLogicU3Eb__0_m3C55808E777E37F150CBD289C805DFCF193BC721 (void);
// 0x000003AF System.Void FirebaseManager/<>c__DisplayClass30_1::.ctor()
extern void U3CU3Ec__DisplayClass30_1__ctor_m0ADE6D59F393F35F1AEEDC213FE8DB5B26FA70B1 (void);
// 0x000003B0 System.Boolean FirebaseManager/<>c__DisplayClass30_1::<RegisterLogic>b__1()
extern void U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__1_mA079161ABF2C1BBEB82F33390A22D0358234903A (void);
// 0x000003B1 System.Boolean FirebaseManager/<>c__DisplayClass30_1::<RegisterLogic>b__3()
extern void U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__3_m31A0438D67318B88950BF6ABE708899B381CD0FE (void);
// 0x000003B2 System.Boolean FirebaseManager/<>c__DisplayClass30_1::<RegisterLogic>b__5()
extern void U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__5_m9708FDEC75EA78EE9250962108F2C1B7A7F3E863 (void);
// 0x000003B3 System.Boolean FirebaseManager/<>c__DisplayClass30_1::<RegisterLogic>b__7()
extern void U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__7_mBCBE396AF0302D04D10B98DA79366C4D70ED0D94 (void);
// 0x000003B4 System.Void FirebaseManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m9C76236275D1302561C5F35B08586CB778EFA06E (void);
// 0x000003B5 System.Void FirebaseManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m9539F2699E040A5A97D5CBB90137A1FD789DC372 (void);
// 0x000003B6 System.Void FirebaseManager/<>c::<RegisterLogic>b__30_2(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CRegisterLogicU3Eb__30_2_mAA423066A61A9D823ED193B857BF102C12D79180 (void);
// 0x000003B7 System.Void FirebaseManager/<>c::<RegisterLogic>b__30_4(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CRegisterLogicU3Eb__30_4_m11364D72F993ECBCC265EAFB8DD5C5F8423C8B24 (void);
// 0x000003B8 System.Void FirebaseManager/<>c::<RegisterLogic>b__30_6(System.Threading.Tasks.Task)
extern void U3CU3Ec_U3CRegisterLogicU3Eb__30_6_mAC7BD6AE84AFAAC6C003ECA6F32EA9BC6A01C376 (void);
// 0x000003B9 System.Void FirebaseManager/<RegisterLogic>d__30::.ctor(System.Int32)
extern void U3CRegisterLogicU3Ed__30__ctor_mB79F27786D67ED3D68638817B1BD0B2089F1FDBD (void);
// 0x000003BA System.Void FirebaseManager/<RegisterLogic>d__30::System.IDisposable.Dispose()
extern void U3CRegisterLogicU3Ed__30_System_IDisposable_Dispose_mB5B9C8F6193294A31E5211AADE5344B5C0F68D14 (void);
// 0x000003BB System.Boolean FirebaseManager/<RegisterLogic>d__30::MoveNext()
extern void U3CRegisterLogicU3Ed__30_MoveNext_m767EA298C57C6F4DECA1FA47C80403272DE210AE (void);
// 0x000003BC System.Object FirebaseManager/<RegisterLogic>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRegisterLogicU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABB3EA176CD17130B14FB881A35DD2D15C6C4A6F (void);
// 0x000003BD System.Void FirebaseManager/<RegisterLogic>d__30::System.Collections.IEnumerator.Reset()
extern void U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_Reset_m777E21375ABD9F78DD640A7188273C00A51F78AB (void);
// 0x000003BE System.Object FirebaseManager/<RegisterLogic>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_get_Current_mBBB8E1D2A536AFC119B513D81CE9ED58929E784D (void);
// 0x000003BF System.Void GarmPop::Start()
extern void GarmPop_Start_m0DE910BD19A7F100C2619D3E41145D23E098EA20 (void);
// 0x000003C0 System.Void GarmPop::LayoutFix(System.Int32)
extern void GarmPop_LayoutFix_m755DFC9DE0CEABEDC13F87BDB8373A2A07CE1F5A (void);
// 0x000003C1 System.Void GarmPop::AddGarms(System.Int32)
extern void GarmPop_AddGarms_m64A1500BD8163E0AD7C48858CC08E7887A4F758A (void);
// 0x000003C2 System.Void GarmPop::InitItem(UnityEngine.GameObject,System.String)
extern void GarmPop_InitItem_mC1162B371CDAFB78A8797D849850444106D108E3 (void);
// 0x000003C3 System.Collections.IEnumerator GarmPop::DownloadImage(System.String,System.Action`1<UnityEngine.Texture>)
extern void GarmPop_DownloadImage_m84173F9995B260824C08FC1C11F8B4C8035E3AC2 (void);
// 0x000003C4 System.Void GarmPop::MakeItem(System.String)
extern void GarmPop_MakeItem_m107802F68967B6A1F2F311F3E93D51565B453E2E (void);
// 0x000003C5 System.Single GarmPop::rectcalc(System.Int32)
extern void GarmPop_rectcalc_m65136061B74BDDD46E9653306C984B4EBA0AFDCF (void);
// 0x000003C6 System.Void GarmPop::.ctor()
extern void GarmPop__ctor_mF01AD224772100422C0855EBA7C68DA76AB0F679 (void);
// 0x000003C7 System.Void GarmPop::.cctor()
extern void GarmPop__cctor_mA5575E55296E1D9907C3360F34F27924C95B7E6E (void);
// 0x000003C8 System.Void GarmPop/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mACAD90F33E52B3CFFE4B4087C216FCD4F5D9A0ED (void);
// 0x000003C9 System.Void GarmPop/<>c__DisplayClass26_0::<InitItem>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CInitItemU3Eb__0_m3E1B53C0666146FAD3CD3E41E422F81D883CFA0F (void);
// 0x000003CA System.Void GarmPop/<>c__DisplayClass26_0::<InitItem>b__1()
extern void U3CU3Ec__DisplayClass26_0_U3CInitItemU3Eb__1_m2057C8EB951C84CDCD550D7A51BA7D7BCC932F2E (void);
// 0x000003CB System.Void GarmPop/<DownloadImage>d__27::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__27__ctor_m4A14D0747572B8EE136B45D12D121AFE2162C2C8 (void);
// 0x000003CC System.Void GarmPop/<DownloadImage>d__27::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__27_System_IDisposable_Dispose_mF6799C18193605A8BE990E1BEFAF3A3A920D975F (void);
// 0x000003CD System.Boolean GarmPop/<DownloadImage>d__27::MoveNext()
extern void U3CDownloadImageU3Ed__27_MoveNext_mFF70143CCBC387DA9226CCD6145738B29EA71A53 (void);
// 0x000003CE System.Object GarmPop/<DownloadImage>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2B5B8ABD582D8F5B844A24F13A3C5B25B52AE1D (void);
// 0x000003CF System.Void GarmPop/<DownloadImage>d__27::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_Reset_m77E7F96BBC7786A52B73A5EFCA9BEC65D739CC29 (void);
// 0x000003D0 System.Object GarmPop/<DownloadImage>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_get_Current_m44C8F7E23295F58BC5D19979F36A056B1A68C76D (void);
// 0x000003D1 UnityEngine.XR.ARFoundation.ARHumanBodyManager HumanBodyTracker::get_humanBodyManager()
extern void HumanBodyTracker_get_humanBodyManager_m68D297EB7BABF50F4C2DE39F88B5B90C5E4191AB (void);
// 0x000003D2 System.Void HumanBodyTracker::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTracker_set_humanBodyManager_m76D4425C7F0F4E4CC53FF83A9FE884B61F7EBEFF (void);
// 0x000003D3 System.Void HumanBodyTracker::OnEnable()
extern void HumanBodyTracker_OnEnable_m0470A62AD963F51487CEA9B49412304C19CE36A1 (void);
// 0x000003D4 System.Void HumanBodyTracker::OnDisable()
extern void HumanBodyTracker_OnDisable_m91B4B8B572BEB519C241D248A7FBA817BEA3A7B2 (void);
// 0x000003D5 System.Collections.IEnumerator HumanBodyTracker::InitAr()
extern void HumanBodyTracker_InitAr_mBEB9EA30F3D57CBB6B997C95EE4E5860A59065DF (void);
// 0x000003D6 System.Collections.IEnumerator HumanBodyTracker::GetAssetBundle(System.String)
extern void HumanBodyTracker_GetAssetBundle_m88CEBCF31113436C0DEEBE6E8FB712F4F4E7871D (void);
// 0x000003D7 System.Void HumanBodyTracker::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTracker_OnHumanBodiesChanged_mD2D373BEF77C090B89BE48A9459D87953676E1DC (void);
// 0x000003D8 System.Void HumanBodyTracker::.ctor()
extern void HumanBodyTracker__ctor_m7F9563662EA3EB1910A498B7E962391A67398CA9 (void);
// 0x000003D9 System.Void HumanBodyTracker::<InitAr>b__14_1(System.Threading.Tasks.Task`1<System.Uri>)
extern void HumanBodyTracker_U3CInitArU3Eb__14_1_m4DE5C014E90FB1395DECCE1D36220252B78B7BF4 (void);
// 0x000003DA System.Void HumanBodyTracker/<>c::.cctor()
extern void U3CU3Ec__cctor_m297B92DA3C960DC4DD545922EC608164386C9E41 (void);
// 0x000003DB System.Void HumanBodyTracker/<>c::.ctor()
extern void U3CU3Ec__ctor_mCA14709230C9399C4A6DE985C838912E9C05AB9A (void);
// 0x000003DC System.Boolean HumanBodyTracker/<>c::<InitAr>b__14_0()
extern void U3CU3Ec_U3CInitArU3Eb__14_0_m4A89FBCBAE8C7D6919F36ED978B700EF4EF8AD0C (void);
// 0x000003DD System.Boolean HumanBodyTracker/<>c::<GetAssetBundle>b__15_0()
extern void U3CU3Ec_U3CGetAssetBundleU3Eb__15_0_mAB67F2571E0C76246D491A1B91C3F17000FA172E (void);
// 0x000003DE System.Void HumanBodyTracker/<InitAr>d__14::.ctor(System.Int32)
extern void U3CInitArU3Ed__14__ctor_m0BCE60FC071A5BB60B68A3F58582F4072B03F4A7 (void);
// 0x000003DF System.Void HumanBodyTracker/<InitAr>d__14::System.IDisposable.Dispose()
extern void U3CInitArU3Ed__14_System_IDisposable_Dispose_m2D6CADDAA2F5A47F381E9C8197F4E3DE4998CC30 (void);
// 0x000003E0 System.Boolean HumanBodyTracker/<InitAr>d__14::MoveNext()
extern void U3CInitArU3Ed__14_MoveNext_m7CFF5AFCEB960093E9D44339E761080076E039E8 (void);
// 0x000003E1 System.Object HumanBodyTracker/<InitAr>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitArU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5ECF5BB94B05D7C55A84197D235473F98DFAD1F (void);
// 0x000003E2 System.Void HumanBodyTracker/<InitAr>d__14::System.Collections.IEnumerator.Reset()
extern void U3CInitArU3Ed__14_System_Collections_IEnumerator_Reset_mDAADBFE05CAAB470F57C48D2380BD54640B285A9 (void);
// 0x000003E3 System.Object HumanBodyTracker/<InitAr>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CInitArU3Ed__14_System_Collections_IEnumerator_get_Current_mB4952B1DD52B1A6B63B24723EA9B6C6E5704EE9E (void);
// 0x000003E4 System.Void HumanBodyTracker/<GetAssetBundle>d__15::.ctor(System.Int32)
extern void U3CGetAssetBundleU3Ed__15__ctor_m401E6206298334CA59DD8FAD9E64B78A6D1DE8FF (void);
// 0x000003E5 System.Void HumanBodyTracker/<GetAssetBundle>d__15::System.IDisposable.Dispose()
extern void U3CGetAssetBundleU3Ed__15_System_IDisposable_Dispose_m0F4FD2EE83D390DD3506608D143E0521AE2B902C (void);
// 0x000003E6 System.Boolean HumanBodyTracker/<GetAssetBundle>d__15::MoveNext()
extern void U3CGetAssetBundleU3Ed__15_MoveNext_m6F83A92E40318755E7BD6CEA642F559D1948FA11 (void);
// 0x000003E7 System.Object HumanBodyTracker/<GetAssetBundle>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetAssetBundleU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m281ECDE9CA2D3588CE643FDC7865D477D53C70FA (void);
// 0x000003E8 System.Void HumanBodyTracker/<GetAssetBundle>d__15::System.Collections.IEnumerator.Reset()
extern void U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_Reset_mC0ADC36AB218452B7F2409B765C99D0D82E8EB8F (void);
// 0x000003E9 System.Object HumanBodyTracker/<GetAssetBundle>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_get_Current_m6419ED791E01A27B653D693CD250F4BAE9699232 (void);
// 0x000003EA UnityEngine.XR.ARFoundation.ARHumanBodyManager HumanBodyTrackerAvatar::get_humanBodyManager()
extern void HumanBodyTrackerAvatar_get_humanBodyManager_m227A5F2BD00A31243ED33B2DBEAA7EFDDD36AFD4 (void);
// 0x000003EB System.Void HumanBodyTrackerAvatar::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTrackerAvatar_set_humanBodyManager_m9F8CAE6E17E31F4F45642D175CD16EEB345EECFD (void);
// 0x000003EC System.Void HumanBodyTrackerAvatar::OnEnable()
extern void HumanBodyTrackerAvatar_OnEnable_mB4661B9274890152143D6FDB208767E919D46819 (void);
// 0x000003ED System.Void HumanBodyTrackerAvatar::OnDisable()
extern void HumanBodyTrackerAvatar_OnDisable_m226CE3EB7DE70BCD1676109D0748975CADF990A3 (void);
// 0x000003EE System.Collections.IEnumerator HumanBodyTrackerAvatar::InitAr()
extern void HumanBodyTrackerAvatar_InitAr_mBB6CB82039FBE01973C0A9FEDD0727EAEE8430DD (void);
// 0x000003EF System.Void HumanBodyTrackerAvatar::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTrackerAvatar_OnHumanBodiesChanged_m00462AE6E5C86639C597E26EADDF44727D583378 (void);
// 0x000003F0 System.Void HumanBodyTrackerAvatar::.ctor()
extern void HumanBodyTrackerAvatar__ctor_m9B0766C27B46E2D0488CDB200C0F9C8494EEB97B (void);
// 0x000003F1 System.Void HumanBodyTrackerAvatar/<InitAr>d__17::.ctor(System.Int32)
extern void U3CInitArU3Ed__17__ctor_m5D62044BEC329579BDD4C8F4BAE9B64F1A9AF6A8 (void);
// 0x000003F2 System.Void HumanBodyTrackerAvatar/<InitAr>d__17::System.IDisposable.Dispose()
extern void U3CInitArU3Ed__17_System_IDisposable_Dispose_mC04A34D6A9AA9D0ADE679858B6A0CF8EDE9098C2 (void);
// 0x000003F3 System.Boolean HumanBodyTrackerAvatar/<InitAr>d__17::MoveNext()
extern void U3CInitArU3Ed__17_MoveNext_m6246767278CB6B2BE90239267132B2D57890A426 (void);
// 0x000003F4 System.Object HumanBodyTrackerAvatar/<InitAr>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitArU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD40A5A4069316DB30BB2F1611247BDBD1F5345F3 (void);
// 0x000003F5 System.Void HumanBodyTrackerAvatar/<InitAr>d__17::System.Collections.IEnumerator.Reset()
extern void U3CInitArU3Ed__17_System_Collections_IEnumerator_Reset_m95F44591F726BD047A6BB8920E7B62C2CCCDD776 (void);
// 0x000003F6 System.Object HumanBodyTrackerAvatar/<InitAr>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CInitArU3Ed__17_System_Collections_IEnumerator_get_Current_m6017680888DB02B9B20F061F4B8C9EC90E1562D8 (void);
// 0x000003F7 UnityEngine.XR.ARFoundation.ARHumanBodyManager HumanBodyTrackerGarment::get_humanBodyManager()
extern void HumanBodyTrackerGarment_get_humanBodyManager_m8BE7C6C6B20F03918528CCFE520BA1D4D5C34BC5 (void);
// 0x000003F8 System.Void HumanBodyTrackerGarment::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTrackerGarment_set_humanBodyManager_m17D685BCEBC1CBC52A8A3744134468CDA6C09D14 (void);
// 0x000003F9 System.Void HumanBodyTrackerGarment::OnEnable()
extern void HumanBodyTrackerGarment_OnEnable_m48C580FE26248CEA6C39C7530DBC79D0156A369B (void);
// 0x000003FA System.Void HumanBodyTrackerGarment::OnDisable()
extern void HumanBodyTrackerGarment_OnDisable_m0509B03FC25B0B111AF90C18BC733471689A10EC (void);
// 0x000003FB System.Collections.IEnumerator HumanBodyTrackerGarment::AvailableColors()
extern void HumanBodyTrackerGarment_AvailableColors_mC1C04209D418DE6B950FB40DC296DB41724E6CF4 (void);
// 0x000003FC System.Void HumanBodyTrackerGarment::AddDrpOpts()
extern void HumanBodyTrackerGarment_AddDrpOpts_mC0F86E2DFB87FC4FD16C7EF8FD17A7CF6EAF68C0 (void);
// 0x000003FD System.Collections.IEnumerator HumanBodyTrackerGarment::InitAr()
extern void HumanBodyTrackerGarment_InitAr_mD74EBB8D82EB0E679796BC4561B5C1AD66CAEA99 (void);
// 0x000003FE System.Collections.IEnumerator HumanBodyTrackerGarment::GetAssetBundleInit(System.String,System.String)
extern void HumanBodyTrackerGarment_GetAssetBundleInit_mC8408C8F96732956774018F9279CF136A3DB118B (void);
// 0x000003FF System.Collections.IEnumerator HumanBodyTrackerGarment::RespawnColor()
extern void HumanBodyTrackerGarment_RespawnColor_m1AF0CD10B47DBF05E07FB6CB2DD5E7EFC91C86B7 (void);
// 0x00000400 System.Collections.IEnumerator HumanBodyTrackerGarment::RespawnSize(System.String)
extern void HumanBodyTrackerGarment_RespawnSize_m187FE19EF6C2040513748D61E17797E13C3B8A34 (void);
// 0x00000401 System.Collections.IEnumerator HumanBodyTrackerGarment::GetAssetBundleExtra(System.String,System.String)
extern void HumanBodyTrackerGarment_GetAssetBundleExtra_m6BA170E938C7A8E18D07F75EBFFE195A7D0AA8C4 (void);
// 0x00000402 System.Void HumanBodyTrackerGarment::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTrackerGarment_OnHumanBodiesChanged_m49B3939B73B014EC3DFCE6D9C65DB9904356474A (void);
// 0x00000403 System.Void HumanBodyTrackerGarment::.ctor()
extern void HumanBodyTrackerGarment__ctor_m907A4042F0BB524B537C6E7ADE036755F96E5900 (void);
// 0x00000404 System.Void HumanBodyTrackerGarment::<InitAr>b__41_0(System.Threading.Tasks.Task`1<System.Uri>)
extern void HumanBodyTrackerGarment_U3CInitArU3Eb__41_0_m77EFB7D5E455F11427FFE4DEB5A5E7DFCE0DB687 (void);
// 0x00000405 System.Void HumanBodyTrackerGarment::<InitAr>b__41_1()
extern void HumanBodyTrackerGarment_U3CInitArU3Eb__41_1_mC654DD0D05916ACE0CFBF0C58EFCAC4876750B07 (void);
// 0x00000406 System.Void HumanBodyTrackerGarment::<InitAr>b__41_2(System.Int32)
extern void HumanBodyTrackerGarment_U3CInitArU3Eb__41_2_m9D21EB7E7EC5693B339E241A00BB7A0FA5427BA9 (void);
// 0x00000407 System.Void HumanBodyTrackerGarment/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_mAA8E1551EED6CCA554DA6F0E6C0D15911BB8FF2F (void);
// 0x00000408 System.Void HumanBodyTrackerGarment/<>c__DisplayClass39_0::<AvailableColors>b__0(System.Threading.Tasks.Task`1<Firebase.Firestore.DocumentSnapshot>)
extern void U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__0_m8AC25BEF4E302711FC9B8265ECA0D382AE2CBF21 (void);
// 0x00000409 System.Boolean HumanBodyTrackerGarment/<>c__DisplayClass39_0::<AvailableColors>b__1()
extern void U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__1_m8948804688DAE76538F7DF81B055D1C0CA693B1C (void);
// 0x0000040A System.Void HumanBodyTrackerGarment/<>c__DisplayClass39_0::<AvailableColors>b__2(System.Threading.Tasks.Task`1<Firebase.Firestore.QuerySnapshot>)
extern void U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__2_m65A126E37746EB32290BC85B114FA3DEC5B4B19B (void);
// 0x0000040B System.Boolean HumanBodyTrackerGarment/<>c__DisplayClass39_0::<AvailableColors>b__3()
extern void U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__3_m014928A1436DE83CF25660B0F4430ED2C00C8E7F (void);
// 0x0000040C System.Void HumanBodyTrackerGarment/<AvailableColors>d__39::.ctor(System.Int32)
extern void U3CAvailableColorsU3Ed__39__ctor_m0E7ED7000437E952D8E68D1C62B1316AAA31A049 (void);
// 0x0000040D System.Void HumanBodyTrackerGarment/<AvailableColors>d__39::System.IDisposable.Dispose()
extern void U3CAvailableColorsU3Ed__39_System_IDisposable_Dispose_mDBC0CA875800EF0E74C0C02CEC5C5D38966EACC8 (void);
// 0x0000040E System.Boolean HumanBodyTrackerGarment/<AvailableColors>d__39::MoveNext()
extern void U3CAvailableColorsU3Ed__39_MoveNext_mBBD9997A9EDCE9D5A34E38DBA2B9F97E6D3FF101 (void);
// 0x0000040F System.Object HumanBodyTrackerGarment/<AvailableColors>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAvailableColorsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m71E0D4E1D33EB11C00D58C59F1CB4EF672574BF8 (void);
// 0x00000410 System.Void HumanBodyTrackerGarment/<AvailableColors>d__39::System.Collections.IEnumerator.Reset()
extern void U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_Reset_mB982CFD7B68F4859E7412FE32A24554DDF8B9E34 (void);
// 0x00000411 System.Object HumanBodyTrackerGarment/<AvailableColors>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_get_Current_m83710E3C8D34400DB97D8B5169AAE0BDFD131444 (void);
// 0x00000412 System.Void HumanBodyTrackerGarment/<InitAr>d__41::.ctor(System.Int32)
extern void U3CInitArU3Ed__41__ctor_m1D5A3ACACB83EE5605FF4AC082EB241A1460BCDF (void);
// 0x00000413 System.Void HumanBodyTrackerGarment/<InitAr>d__41::System.IDisposable.Dispose()
extern void U3CInitArU3Ed__41_System_IDisposable_Dispose_mCB2CE8CBF5674FD98B569F02FE5AC26555DE020C (void);
// 0x00000414 System.Boolean HumanBodyTrackerGarment/<InitAr>d__41::MoveNext()
extern void U3CInitArU3Ed__41_MoveNext_m519C71A8B6D062CC36C72A6D459D651800501EE2 (void);
// 0x00000415 System.Object HumanBodyTrackerGarment/<InitAr>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitArU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC0533830866A8CF98F479EF5B8106A8B635384 (void);
// 0x00000416 System.Void HumanBodyTrackerGarment/<InitAr>d__41::System.Collections.IEnumerator.Reset()
extern void U3CInitArU3Ed__41_System_Collections_IEnumerator_Reset_m67F2B799690EB2F34910D863BD4CB2C83F3E1809 (void);
// 0x00000417 System.Object HumanBodyTrackerGarment/<InitAr>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CInitArU3Ed__41_System_Collections_IEnumerator_get_Current_m29DD6B0CAAAF87F7F1322A81E39F4F0788B86053 (void);
// 0x00000418 System.Void HumanBodyTrackerGarment/<GetAssetBundleInit>d__42::.ctor(System.Int32)
extern void U3CGetAssetBundleInitU3Ed__42__ctor_m3BD2C502FFA7F018FF2A9E5556E12D28D147BDC5 (void);
// 0x00000419 System.Void HumanBodyTrackerGarment/<GetAssetBundleInit>d__42::System.IDisposable.Dispose()
extern void U3CGetAssetBundleInitU3Ed__42_System_IDisposable_Dispose_mA97EB664D7C245AA9DA4CBD3507F5603E8464FDB (void);
// 0x0000041A System.Boolean HumanBodyTrackerGarment/<GetAssetBundleInit>d__42::MoveNext()
extern void U3CGetAssetBundleInitU3Ed__42_MoveNext_m5EBF5551D9F559854F13B35A96EDEEBDCFEBDE72 (void);
// 0x0000041B System.Object HumanBodyTrackerGarment/<GetAssetBundleInit>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetAssetBundleInitU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F3D0239193B5CBCC7AADCA905298F2B85B3870D (void);
// 0x0000041C System.Void HumanBodyTrackerGarment/<GetAssetBundleInit>d__42::System.Collections.IEnumerator.Reset()
extern void U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_Reset_m9F64FD56FD033B71B1AEDF751D313A81230B2FC7 (void);
// 0x0000041D System.Object HumanBodyTrackerGarment/<GetAssetBundleInit>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_get_Current_mEE72D133A4AD3A0E74B5164CB3F09F72E28D87AA (void);
// 0x0000041E System.Void HumanBodyTrackerGarment/<>c__DisplayClass43_0::.ctor()
extern void U3CU3Ec__DisplayClass43_0__ctor_m1B51A327FD403F7A5A6FFCBB1778A99F86C49135 (void);
// 0x0000041F System.Void HumanBodyTrackerGarment/<>c__DisplayClass43_0::<RespawnColor>b__0(System.Threading.Tasks.Task`1<System.Uri>)
extern void U3CU3Ec__DisplayClass43_0_U3CRespawnColorU3Eb__0_mEE986E8C5469587E1E3A2E32C0ED5D7F094542E6 (void);
// 0x00000420 System.Void HumanBodyTrackerGarment/<RespawnColor>d__43::.ctor(System.Int32)
extern void U3CRespawnColorU3Ed__43__ctor_mCDD3B2C9874ADE71189C1A2ACBDAB227D2455955 (void);
// 0x00000421 System.Void HumanBodyTrackerGarment/<RespawnColor>d__43::System.IDisposable.Dispose()
extern void U3CRespawnColorU3Ed__43_System_IDisposable_Dispose_m7BBE4857CF406984A98BD1C95C6F40D8DEF5A516 (void);
// 0x00000422 System.Boolean HumanBodyTrackerGarment/<RespawnColor>d__43::MoveNext()
extern void U3CRespawnColorU3Ed__43_MoveNext_mDE8B95E357B923EEE1CEB004FD055AFCCAD7B063 (void);
// 0x00000423 System.Object HumanBodyTrackerGarment/<RespawnColor>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnColorU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D63008CE9C30952B4DEB9402BB40C2C291BF02 (void);
// 0x00000424 System.Void HumanBodyTrackerGarment/<RespawnColor>d__43::System.Collections.IEnumerator.Reset()
extern void U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_Reset_m7FBE3DC6DA7C4020EE8EE7D13954C9FCAA874F81 (void);
// 0x00000425 System.Object HumanBodyTrackerGarment/<RespawnColor>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_get_Current_m5EDFFCF83AF6C196734AAAA90F2CF68397620C8E (void);
// 0x00000426 System.Void HumanBodyTrackerGarment/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m291704CF43BB311CDCA3CAB263DA8F9BE4DC740A (void);
// 0x00000427 System.Void HumanBodyTrackerGarment/<>c__DisplayClass44_0::<RespawnSize>b__0(System.Threading.Tasks.Task`1<System.Uri>)
extern void U3CU3Ec__DisplayClass44_0_U3CRespawnSizeU3Eb__0_m9F32DAFB5220B8E308F010933309D11F4C39FF03 (void);
// 0x00000428 System.Void HumanBodyTrackerGarment/<RespawnSize>d__44::.ctor(System.Int32)
extern void U3CRespawnSizeU3Ed__44__ctor_mAC3B30605681C825B91BBA89893EF6CB1A3D96E8 (void);
// 0x00000429 System.Void HumanBodyTrackerGarment/<RespawnSize>d__44::System.IDisposable.Dispose()
extern void U3CRespawnSizeU3Ed__44_System_IDisposable_Dispose_mA0ECE09AB7D10863359BF0B7026FC47BEF15FF48 (void);
// 0x0000042A System.Boolean HumanBodyTrackerGarment/<RespawnSize>d__44::MoveNext()
extern void U3CRespawnSizeU3Ed__44_MoveNext_m61E7D93893C4418DEAE35D289E2DB95A97421556 (void);
// 0x0000042B System.Object HumanBodyTrackerGarment/<RespawnSize>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRespawnSizeU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34077D9F5C1F11EE301947D160A382D1349E45EA (void);
// 0x0000042C System.Void HumanBodyTrackerGarment/<RespawnSize>d__44::System.Collections.IEnumerator.Reset()
extern void U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_Reset_m3FDD44A4DF8529AECBA79B66DEF8E53C8AC5356E (void);
// 0x0000042D System.Object HumanBodyTrackerGarment/<RespawnSize>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_get_Current_m7F780518B35ADEDB939AE4E7A59237B3BF7F42BC (void);
// 0x0000042E System.Void HumanBodyTrackerGarment/<GetAssetBundleExtra>d__45::.ctor(System.Int32)
extern void U3CGetAssetBundleExtraU3Ed__45__ctor_mF7B098B86805EA416C3462209C68E05D57BC41D6 (void);
// 0x0000042F System.Void HumanBodyTrackerGarment/<GetAssetBundleExtra>d__45::System.IDisposable.Dispose()
extern void U3CGetAssetBundleExtraU3Ed__45_System_IDisposable_Dispose_mB00EE4A9B81482B388F6A20A290B0D0752167601 (void);
// 0x00000430 System.Boolean HumanBodyTrackerGarment/<GetAssetBundleExtra>d__45::MoveNext()
extern void U3CGetAssetBundleExtraU3Ed__45_MoveNext_m3E3AFB36716755B91F6482393950C5EA446FAFF8 (void);
// 0x00000431 System.Object HumanBodyTrackerGarment/<GetAssetBundleExtra>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetAssetBundleExtraU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA81081F88F877593B79FB77044DF848A78B7CBF (void);
// 0x00000432 System.Void HumanBodyTrackerGarment/<GetAssetBundleExtra>d__45::System.Collections.IEnumerator.Reset()
extern void U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_Reset_mAA2607639C0DF413DF98F03E05E7B59C31B83CCC (void);
// 0x00000433 System.Object HumanBodyTrackerGarment/<GetAssetBundleExtra>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_get_Current_m9E60F5E5C1D05D243287FD86E4ADE78EE4ABAE36 (void);
// 0x00000434 System.Void Init::Start()
extern void Init_Start_m82D7ED5811F1B9A02537C9648565249359023DC3 (void);
// 0x00000435 System.Void Init::.ctor()
extern void Init__ctor_m3AB34A4D9E5A7311947DBDEF3D59B679BC6A3205 (void);
// 0x00000436 System.Void Init::.cctor()
extern void Init__cctor_m0FDFEFC46A4A53960685F898959F9B39EF4755FF (void);
// 0x00000437 System.Void InitList::Awake()
extern void InitList_Awake_m5D8E9F4A5364380CC2D05910774C1A426550A4B4 (void);
// 0x00000438 System.Void InitList::SendRequest()
extern void InitList_SendRequest_mDCB17F399B1B450B88D03071CBBB291575729F24 (void);
// 0x00000439 System.Void InitList::Update()
extern void InitList_Update_m9461782FD2D6039D4B3AA9943EE1E794726796B7 (void);
// 0x0000043A System.Void InitList::.ctor()
extern void InitList__ctor_m7F05BC830284FDE397311D04A9FE0C28E2D40AF8 (void);
// 0x0000043B System.Void InitList/OnInitPagingRequested::.ctor(System.Object,System.IntPtr)
extern void OnInitPagingRequested__ctor_m174AE0E417A1DAA491FEEF58FC6D65EFB4EF2CF6 (void);
// 0x0000043C System.Void InitList/OnInitPagingRequested::Invoke(System.Int32,System.Int32,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void OnInitPagingRequested_Invoke_mEC89E3110366A23E02ADEF22966B545563112775 (void);
// 0x0000043D System.IAsyncResult InitList/OnInitPagingRequested::BeginInvoke(System.Int32,System.Int32,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.AsyncCallback,System.Object)
extern void OnInitPagingRequested_BeginInvoke_mC7C2BB6D7988C4555D9224C21430428D96D18969 (void);
// 0x0000043E System.Void InitList/OnInitPagingRequested::EndInvoke(System.IAsyncResult)
extern void OnInitPagingRequested_EndInvoke_m66E6C1DDF872ECD5D22BEFC8A5DEC9EB18760E1C (void);
// 0x0000043F System.Void InitList/OnInitSingleDocRequested::.ctor(System.Object,System.IntPtr)
extern void OnInitSingleDocRequested__ctor_m45036B08EF5BB946377F2C2FF20C6037045FDD96 (void);
// 0x00000440 System.Void InitList/OnInitSingleDocRequested::Invoke(System.String)
extern void OnInitSingleDocRequested_Invoke_m385A4B07A171F42F0250EAD5378870A12F73D9E0 (void);
// 0x00000441 System.IAsyncResult InitList/OnInitSingleDocRequested::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnInitSingleDocRequested_BeginInvoke_mEA36DB8A9049573764E48ADA99A357B6E98A670D (void);
// 0x00000442 System.Void InitList/OnInitSingleDocRequested::EndInvoke(System.IAsyncResult)
extern void OnInitSingleDocRequested_EndInvoke_mA90B5F064100B11C8D5D4D2B74C5152F06C3F348 (void);
// 0x00000443 System.Void InitProduct::Start()
extern void InitProduct_Start_m0961301980C1FA5077F5C0BD752DC754C6E5E8DB (void);
// 0x00000444 System.Void InitProduct::Update()
extern void InitProduct_Update_m177F680AE737F4B05AADED470269A4572D60C1DF (void);
// 0x00000445 System.Void InitProduct::.ctor()
extern void InitProduct__ctor_m39256C3BC9A7F1EF54BAF5AE59A8B9F11E8FB310 (void);
// 0x00000446 System.Void InitProduct/OnInitSingleDocRequested::.ctor(System.Object,System.IntPtr)
extern void OnInitSingleDocRequested__ctor_m8060CA3A2D3C916F09623A8BE892A9EE5908DA4E (void);
// 0x00000447 System.Void InitProduct/OnInitSingleDocRequested::Invoke(System.String)
extern void OnInitSingleDocRequested_Invoke_m722361E55E140E04470AC4E1ED9429F242460592 (void);
// 0x00000448 System.IAsyncResult InitProduct/OnInitSingleDocRequested::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnInitSingleDocRequested_BeginInvoke_m53BF7CB569BCE1B528AF19BA04A46123A2C3F2A4 (void);
// 0x00000449 System.Void InitProduct/OnInitSingleDocRequested::EndInvoke(System.IAsyncResult)
extern void OnInitSingleDocRequested_EndInvoke_m4D051E2B60217F1CE5BA71A3DE61E903798EDF2C (void);
// 0x0000044A System.Void MainManager::Awake()
extern void MainManager_Awake_m00A3A3401B125A3CE7EA546FC61FB4D8FD1E0B79 (void);
// 0x0000044B System.Void MainManager::.ctor()
extern void MainManager__ctor_m90DF2FBE6170D98DF8F9C03C44FB914322C4D10F (void);
// 0x0000044C System.Void NewBehaviourScript::Start()
extern void NewBehaviourScript_Start_m783F84A617DADC4574B0BF1524481E6B96C65661 (void);
// 0x0000044D System.Void NewBehaviourScript::Update()
extern void NewBehaviourScript_Update_m411C4D5C2D993FD70092FDA0FE2AC4786F8AC001 (void);
// 0x0000044E System.Void NewBehaviourScript::.ctor()
extern void NewBehaviourScript__ctor_m437970EA37D66BDF32972F4CC0F65B95E5961FAA (void);
// 0x0000044F System.Void Recommendation?nputClass::.ctor()
extern void RecommendationU399nputClass__ctor_mAA6A161CB7EFED2B28A7F6FDC3648252A6A74AE2 (void);
// 0x00000450 System.Void SceneBack::ChangeScene()
extern void SceneBack_ChangeScene_m078EB208872E0D7D66B18011BD5F23967397A77C (void);
// 0x00000451 System.Void SceneBack::Update()
extern void SceneBack_Update_m3207CCCDC04845C8B1274F238750A96EEBD9D15B (void);
// 0x00000452 System.Void SceneBack::.ctor()
extern void SceneBack__ctor_m4E9E84576134D36D0C21F296D2782745435A445E (void);
// 0x00000453 System.Void SceneChanger::ChangeScene(System.String)
extern void SceneChanger_ChangeScene_m00182E4E31B32E2E0E818E34FEEF207B6CF9C790 (void);
// 0x00000454 System.Collections.IEnumerator SceneChanger::Logout()
extern void SceneChanger_Logout_mCC126448679F9759D5D0EC6DACB7AC41863FCEE0 (void);
// 0x00000455 System.Void SceneChanger::Exit()
extern void SceneChanger_Exit_m7367524BED2B13B8A53A9698F3AA3A9B2D31CCD3 (void);
// 0x00000456 System.Void SceneChanger::.ctor()
extern void SceneChanger__ctor_m11AE9A596EFE92EE1AA22BD7A48AB0C1D758AB1D (void);
// 0x00000457 System.Void SceneChanger/<Logout>d__2::.ctor(System.Int32)
extern void U3CLogoutU3Ed__2__ctor_m8FF31D23636AB1BA8FE7C518DD99F955F57483BC (void);
// 0x00000458 System.Void SceneChanger/<Logout>d__2::System.IDisposable.Dispose()
extern void U3CLogoutU3Ed__2_System_IDisposable_Dispose_m19E8D33544E5D32EBA7F3CD5842047218D244A58 (void);
// 0x00000459 System.Boolean SceneChanger/<Logout>d__2::MoveNext()
extern void U3CLogoutU3Ed__2_MoveNext_mCB5AB6C5BDF5457A6BCBCD0CDE05D2396413C9A0 (void);
// 0x0000045A System.Object SceneChanger/<Logout>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLogoutU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCABAF6E94019D40B2B6971BD8E7801A981335B65 (void);
// 0x0000045B System.Void SceneChanger/<Logout>d__2::System.Collections.IEnumerator.Reset()
extern void U3CLogoutU3Ed__2_System_Collections_IEnumerator_Reset_m50F33DB45F147049E257D2706FA09BC1E33CD0DF (void);
// 0x0000045C System.Object SceneChanger/<Logout>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CLogoutU3Ed__2_System_Collections_IEnumerator_get_Current_m5195917ABAB3B2D446AF9DAA5BB46F03FD156627 (void);
// 0x0000045D System.Void SceneChangerAcc::Start()
extern void SceneChangerAcc_Start_m45FC877D6F266DBE50B32B34546413CD63AED210 (void);
// 0x0000045E System.Void SceneChangerAcc::ChangeScene(System.String)
extern void SceneChangerAcc_ChangeScene_m115ED3A6CE1380F8117FDE99C2AEB510044A425B (void);
// 0x0000045F System.Void SceneChangerAcc::Update()
extern void SceneChangerAcc_Update_mE70D8FFE07655C48D8749272AED23790A0F51A42 (void);
// 0x00000460 System.Void SceneChangerAcc::.ctor()
extern void SceneChangerAcc__ctor_m9C043D7E473859C027B66B06A6BF6B693772DB71 (void);
// 0x00000461 System.Void ScenechangeProduct::ChangeScene()
extern void ScenechangeProduct_ChangeScene_m7EBD7B58B2D6E6CF5894956DE0E26062E94FAFD9 (void);
// 0x00000462 System.Void ScenechangeProduct::Exit()
extern void ScenechangeProduct_Exit_m7F955F33026B573F9D2A43777CFBCDDD095345CB (void);
// 0x00000463 System.Void ScenechangeProduct::.ctor()
extern void ScenechangeProduct__ctor_m58B7161E686AD417F35688F2DD03F82B0AE15D9D (void);
// 0x00000464 System.Void TextureManager::Awake()
extern void TextureManager_Awake_m138AFFD0D12AFCA0FB33B93CB1774401EDAC1E1C (void);
// 0x00000465 System.Void TextureManager::DestroyTextureOnReceived()
extern void TextureManager_DestroyTextureOnReceived_m8AB3E19012E7C088BC325B995DD1969627B4697C (void);
// 0x00000466 System.Collections.IEnumerator TextureManager::texHandle()
extern void TextureManager_texHandle_m439B211B1DEF03FA31002E8E564A3DF8BF6B0049 (void);
// 0x00000467 System.Void TextureManager::.ctor()
extern void TextureManager__ctor_m13A2832651A1209C03473A832C950382D6E13B6B (void);
// 0x00000468 System.Void TextureManager/<texHandle>d__4::.ctor(System.Int32)
extern void U3CtexHandleU3Ed__4__ctor_m78A498D373C0F1D47DF31B65F65830A922CCAA04 (void);
// 0x00000469 System.Void TextureManager/<texHandle>d__4::System.IDisposable.Dispose()
extern void U3CtexHandleU3Ed__4_System_IDisposable_Dispose_mBC6A134D53DCB893C0332E66F377E631EC743E83 (void);
// 0x0000046A System.Boolean TextureManager/<texHandle>d__4::MoveNext()
extern void U3CtexHandleU3Ed__4_MoveNext_m30C6B218F37F5CCC606487CCD8493ED40678B6FA (void);
// 0x0000046B System.Object TextureManager/<texHandle>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtexHandleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61A1F42F4519A4336BDE57BD8AF01BDB6A3EAD09 (void);
// 0x0000046C System.Void TextureManager/<texHandle>d__4::System.Collections.IEnumerator.Reset()
extern void U3CtexHandleU3Ed__4_System_Collections_IEnumerator_Reset_m5A9853940637A064502F5F4897D24B489826CBD1 (void);
// 0x0000046D System.Object TextureManager/<texHandle>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CtexHandleU3Ed__4_System_Collections_IEnumerator_get_Current_m8473DEAF348680FBE1E0506F50C6FBCD80D6AB7E (void);
// 0x0000046E System.Void ToggleNavscroll::Start()
extern void ToggleNavscroll_Start_m4B70016B5078B069595B196605124CAD5DD9CD8B (void);
// 0x0000046F System.Void ToggleNavscroll::Update()
extern void ToggleNavscroll_Update_mBB2B3D05594DB0F354A01E449CF6F2591FF5A1BE (void);
// 0x00000470 System.Void ToggleNavscroll::SliderValueUnitChanged(System.Single)
extern void ToggleNavscroll_SliderValueUnitChanged_mDC94EE14C9040F63643B8A4CF124291E2CF81687 (void);
// 0x00000471 System.Void ToggleNavscroll::SliderValueGarmsChanged(System.Single)
extern void ToggleNavscroll_SliderValueGarmsChanged_m9DE84AF4273CBD260497911C32FC72CB14B341BB (void);
// 0x00000472 System.Void ToggleNavscroll::.ctor()
extern void ToggleNavscroll__ctor_mA4725E55E08D156BDA0E6767D26ECFD2F78EA28A (void);
// 0x00000473 System.Void NavigationDrawer.UI.FilterPanel::Awake()
extern void FilterPanel_Awake_m9DEA31E67384D6308A58310ECF32E6B34CE75893 (void);
// 0x00000474 System.Void NavigationDrawer.UI.FilterPanel::Start()
extern void FilterPanel_Start_mF768AAE3006D20FAB068864166B723BA64845E21 (void);
// 0x00000475 System.Void NavigationDrawer.UI.FilterPanel::OnDisable()
extern void FilterPanel_OnDisable_m9C36B4FC7746807E54C4ABBBA3ABB6A6947F8B62 (void);
// 0x00000476 System.Void NavigationDrawer.UI.FilterPanel::Update()
extern void FilterPanel_Update_mFE760ADC827264ECEF30E3F8F5ABDC5B1DE459C4 (void);
// 0x00000477 System.Void NavigationDrawer.UI.FilterPanel::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void FilterPanel_OnBeginDrag_mE3E593FE68E710849547E64B7F3D24239234761B (void);
// 0x00000478 System.Void NavigationDrawer.UI.FilterPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void FilterPanel_OnDrag_mD41C76416DEFD98D15ADBD27036AD2126D616564 (void);
// 0x00000479 System.Void NavigationDrawer.UI.FilterPanel::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void FilterPanel_OnEndDrag_m42043EB6FB099EC59476B79B6218E2ED2BA3869C (void);
// 0x0000047A System.Void NavigationDrawer.UI.FilterPanel::BackgroundTap()
extern void FilterPanel_BackgroundTap_m87B2BB83F18B3795A95FD0C8EAEF1DF341AFB9AC (void);
// 0x0000047B System.Void NavigationDrawer.UI.FilterPanel::Open()
extern void FilterPanel_Open_m15BC74945C46E1C553F17E7BDD0DAF3B7402D5C8 (void);
// 0x0000047C System.Void NavigationDrawer.UI.FilterPanel::Close()
extern void FilterPanel_Close_m08A076793232AC2FCC76855A5408473A21EBC0E7 (void);
// 0x0000047D System.Single NavigationDrawer.UI.FilterPanel::QuintOut(System.Single,System.Single,System.Single,System.Single)
extern void FilterPanel_QuintOut_m54EE377566DF16E3E956B8019C7639F7005FFF3A (void);
// 0x0000047E System.Void NavigationDrawer.UI.FilterPanel::RefreshBackgroundSize()
extern void FilterPanel_RefreshBackgroundSize_m41191343BA2532F60F5F25E0D7453B4F541EE467 (void);
// 0x0000047F UnityEngine.Vector2 NavigationDrawer.UI.FilterPanel::QuintOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void FilterPanel_QuintOut_m3587ACE048C39E1BAA08901C095712793F9EAC53 (void);
// 0x00000480 System.Void NavigationDrawer.UI.FilterPanel::.ctor()
extern void FilterPanel__ctor_m7264600D753412C082348227A837D96C7B608AE0 (void);
// 0x00000481 System.Void NavigationDrawer.UI.NavDrawerPanel::Awake()
extern void NavDrawerPanel_Awake_m142F9D46280D952C31513C6D85033C5B56EF1B4B (void);
// 0x00000482 System.Void NavigationDrawer.UI.NavDrawerPanel::Start()
extern void NavDrawerPanel_Start_mBFC84C19E44C531FCCAF123CD90C8E433DCD458E (void);
// 0x00000483 System.Void NavigationDrawer.UI.NavDrawerPanel::OnDisable()
extern void NavDrawerPanel_OnDisable_m580657011A7CFCE920CF4B0DEE10DCFCE4831825 (void);
// 0x00000484 System.Void NavigationDrawer.UI.NavDrawerPanel::Update()
extern void NavDrawerPanel_Update_mEE1F1C289B8B12CDBBC7BB3AD7D049B8612D3F18 (void);
// 0x00000485 System.Void NavigationDrawer.UI.NavDrawerPanel::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void NavDrawerPanel_OnBeginDrag_mE587FF4D91AC4B2E05DBFAFA78ECEBC1C5E0AA0E (void);
// 0x00000486 System.Void NavigationDrawer.UI.NavDrawerPanel::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void NavDrawerPanel_OnDrag_m572E807F063DAC152E9675D9A8E76EEA6FE2A395 (void);
// 0x00000487 System.Void NavigationDrawer.UI.NavDrawerPanel::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void NavDrawerPanel_OnEndDrag_m59621F62C24910CFF50241C6BA929B1E16BFC3AC (void);
// 0x00000488 System.Void NavigationDrawer.UI.NavDrawerPanel::BackgroundTap()
extern void NavDrawerPanel_BackgroundTap_m3FE807F59D23ECC7BABC8D922D889523B073085C (void);
// 0x00000489 System.Void NavigationDrawer.UI.NavDrawerPanel::Open()
extern void NavDrawerPanel_Open_mD120F533906337EC51949E841E640C71905FBC91 (void);
// 0x0000048A System.Void NavigationDrawer.UI.NavDrawerPanel::Close()
extern void NavDrawerPanel_Close_m55CF6A7333896CEE7768546DD40906857FB5688F (void);
// 0x0000048B System.Single NavigationDrawer.UI.NavDrawerPanel::QuintOut(System.Single,System.Single,System.Single,System.Single)
extern void NavDrawerPanel_QuintOut_m5E9C6B16408B098B7CF49752593C21A66DD8A287 (void);
// 0x0000048C System.Void NavigationDrawer.UI.NavDrawerPanel::RefreshBackgroundSize()
extern void NavDrawerPanel_RefreshBackgroundSize_m6C20CF27428601A3D83B7DB8CF591EAFFB557211 (void);
// 0x0000048D UnityEngine.Vector2 NavigationDrawer.UI.NavDrawerPanel::QuintOut(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Single)
extern void NavDrawerPanel_QuintOut_m33B001BD252F490CBEF984FB435A7F40CF36D42C (void);
// 0x0000048E System.Void NavigationDrawer.UI.NavDrawerPanel::.ctor()
extern void NavDrawerPanel__ctor_mA0954D43D05F7BA7A2BA673D86050791EB984B58 (void);
// 0x0000048F UnityEngine.UI.AutoExpandGridLayoutGroup/Corner UnityEngine.UI.AutoExpandGridLayoutGroup::get_startCorner()
extern void AutoExpandGridLayoutGroup_get_startCorner_m46D6D0F268B8B31FB6F0EA597D4A10095737F9A5 (void);
// 0x00000490 System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::set_startCorner(UnityEngine.UI.AutoExpandGridLayoutGroup/Corner)
extern void AutoExpandGridLayoutGroup_set_startCorner_mEC3FFE515D144056CD8FD3886E23009D2D15B575 (void);
// 0x00000491 UnityEngine.UI.AutoExpandGridLayoutGroup/Axis UnityEngine.UI.AutoExpandGridLayoutGroup::get_startAxis()
extern void AutoExpandGridLayoutGroup_get_startAxis_mE1B2B8087318C6776BC89D69D88A9A9FF94DCF04 (void);
// 0x00000492 System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::set_startAxis(UnityEngine.UI.AutoExpandGridLayoutGroup/Axis)
extern void AutoExpandGridLayoutGroup_set_startAxis_mA139CE21A6900EE7161500314DC08B8C84B2EBE5 (void);
// 0x00000493 UnityEngine.Vector2 UnityEngine.UI.AutoExpandGridLayoutGroup::get_cellSize()
extern void AutoExpandGridLayoutGroup_get_cellSize_m2788A6663ED4EF152CEF9046D551B6B2F1623973 (void);
// 0x00000494 System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern void AutoExpandGridLayoutGroup_set_cellSize_mC139347895A5DE01376B751F480B9F02A215BC6B (void);
// 0x00000495 UnityEngine.Vector2 UnityEngine.UI.AutoExpandGridLayoutGroup::get_spacing()
extern void AutoExpandGridLayoutGroup_get_spacing_m0A1D996BB7D1E4CBF52E70D2B70D531A6EABB39D (void);
// 0x00000496 System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern void AutoExpandGridLayoutGroup_set_spacing_mA9AECC549CCF3839154323BB74F4112A466FC90D (void);
// 0x00000497 UnityEngine.UI.AutoExpandGridLayoutGroup/Constraint UnityEngine.UI.AutoExpandGridLayoutGroup::get_constraint()
extern void AutoExpandGridLayoutGroup_get_constraint_m28D8766421877EBFD3A6EBB5202BA0FF1F8FF870 (void);
// 0x00000498 System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::set_constraint(UnityEngine.UI.AutoExpandGridLayoutGroup/Constraint)
extern void AutoExpandGridLayoutGroup_set_constraint_m767D1DEE4CF14E5C66CC89634546EA9EA3337E23 (void);
// 0x00000499 System.Int32 UnityEngine.UI.AutoExpandGridLayoutGroup::get_constraintCount()
extern void AutoExpandGridLayoutGroup_get_constraintCount_m98B8F125C072A2CD82FAA5917BA49BCE4523AAF2 (void);
// 0x0000049A System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::set_constraintCount(System.Int32)
extern void AutoExpandGridLayoutGroup_set_constraintCount_mE168C2F30AAA6178FE483B6AAE459BD5E01A72AF (void);
// 0x0000049B System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::.ctor()
extern void AutoExpandGridLayoutGroup__ctor_mCA3A99E5905C5C7CE96F808296306538EC4180C2 (void);
// 0x0000049C System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::CalculateLayoutInputHorizontal()
extern void AutoExpandGridLayoutGroup_CalculateLayoutInputHorizontal_m352D680BD0153EF702DE99D39ADDF3CD89B439EA (void);
// 0x0000049D System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::CalculateLayoutInputVertical()
extern void AutoExpandGridLayoutGroup_CalculateLayoutInputVertical_m748DE8A07A1B08136F208F60EA972A3A35740DFF (void);
// 0x0000049E System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::SetLayoutHorizontal()
extern void AutoExpandGridLayoutGroup_SetLayoutHorizontal_mF8E221D3640998C160D65C7A8E2F64C3FA9420FE (void);
// 0x0000049F System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::SetLayoutVertical()
extern void AutoExpandGridLayoutGroup_SetLayoutVertical_m6039ED454960261B750BAF5FA267769566E3A68A (void);
// 0x000004A0 System.Void UnityEngine.UI.AutoExpandGridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern void AutoExpandGridLayoutGroup_SetCellsAlongAxis_m0598864CE36606EB056680242018598168DB7E17 (void);
// 0x000004A1 System.Void Obi.ObiCharacter::Start()
extern void ObiCharacter_Start_m9AF9959BA6BB669EC44CC6A19F2C4921A8D3EC12 (void);
// 0x000004A2 System.Void Obi.ObiCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ObiCharacter_Move_mE36529E342431BC2E3F82ED9221503AF7F957934 (void);
// 0x000004A3 System.Void Obi.ObiCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ObiCharacter_ScaleCapsuleForCrouching_mBEE0D314BF3D424DEC1B2D4249AC6F1F1B577A51 (void);
// 0x000004A4 System.Void Obi.ObiCharacter::PreventStandingInLowHeadroom()
extern void ObiCharacter_PreventStandingInLowHeadroom_mE8D5660997F99D5B260E7CCC9BF758FFF4FDFEDD (void);
// 0x000004A5 System.Void Obi.ObiCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ObiCharacter_UpdateAnimator_m0133BC89517D199811A84D3308D99A39508A6163 (void);
// 0x000004A6 System.Void Obi.ObiCharacter::HandleAirborneMovement()
extern void ObiCharacter_HandleAirborneMovement_m2237FCB54D63F3C66E8DC444AE9485868A07E604 (void);
// 0x000004A7 System.Void Obi.ObiCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ObiCharacter_HandleGroundedMovement_m665A36CA6A0EAD5D0C62AA11737F2A6308EEEDC6 (void);
// 0x000004A8 System.Void Obi.ObiCharacter::ApplyExtraTurnRotation()
extern void ObiCharacter_ApplyExtraTurnRotation_m0D9CC7D6882FDB7888330B062F69EE0FA0667BF5 (void);
// 0x000004A9 System.Void Obi.ObiCharacter::OnAnimatorMove()
extern void ObiCharacter_OnAnimatorMove_mE0C7A78FB55C063DF34E2BC3FF6F84125B63AB6A (void);
// 0x000004AA System.Void Obi.ObiCharacter::CheckGroundStatus()
extern void ObiCharacter_CheckGroundStatus_m21B58AEB9AB71B4D7885687B1C5979A9CB9CF45E (void);
// 0x000004AB System.Void Obi.ObiCharacter::.ctor()
extern void ObiCharacter__ctor_m16255C98358F19058CDB9991C96F04FE19EA7A93 (void);
// 0x000004AC System.Void Obi.SampleCharacterController::Start()
extern void SampleCharacterController_Start_mE5EAF1421DBAD9FFAC2D550E04332581CDFFE4BC (void);
// 0x000004AD System.Void Obi.SampleCharacterController::FixedUpdate()
extern void SampleCharacterController_FixedUpdate_m1AC87DDCCC039856B18C977E02E0CD2D80F1A524 (void);
// 0x000004AE System.Void Obi.SampleCharacterController::.ctor()
extern void SampleCharacterController__ctor_m8605568614106AD773DE856FE8AC1968C23A4262 (void);
// 0x000004AF System.Void Obi.ColorFromPhase::Awake()
extern void ColorFromPhase_Awake_m8EE051CBD4CAF6406768C2D6941DCABCD6CFD2B9 (void);
// 0x000004B0 System.Void Obi.ColorFromPhase::LateUpdate()
extern void ColorFromPhase_LateUpdate_m56F07E6A796032EB6DD06CE10C57090983DAA1AB (void);
// 0x000004B1 System.Void Obi.ColorFromPhase::.ctor()
extern void ColorFromPhase__ctor_mECC2D22AC69AC7C21BC64BF585AB14C65E7A2415 (void);
// 0x000004B2 System.Void Obi.ColorFromVelocity::Awake()
extern void ColorFromVelocity_Awake_mBA50845A66FAE55EB4F4637C1BF190EE329FAEB5 (void);
// 0x000004B3 System.Void Obi.ColorFromVelocity::OnEnable()
extern void ColorFromVelocity_OnEnable_m60CE9B091B8725925F60E867BE08F1F137E2AB9D (void);
// 0x000004B4 System.Void Obi.ColorFromVelocity::LateUpdate()
extern void ColorFromVelocity_LateUpdate_m4A026CE0BCACBCA70F219D29A3EEFA7EDE33C2A3 (void);
// 0x000004B5 System.Void Obi.ColorFromVelocity::.ctor()
extern void ColorFromVelocity__ctor_mD26A1B2F148FC8BF6F57EDEA8179E0B2D8CD8FFC (void);
// 0x000004B6 System.Void Obi.ColorRandomizer::Start()
extern void ColorRandomizer_Start_mF174C12B77903483554BBCE5D4A4D82118DD4651 (void);
// 0x000004B7 System.Void Obi.ColorRandomizer::.ctor()
extern void ColorRandomizer__ctor_mF1141F2D29657D595C863D186B46C6B3CD672A85 (void);
// 0x000004B8 System.Void Obi.LookAroundCamera::Awake()
extern void LookAroundCamera_Awake_mE90B4191F1288756CA34EEC0C21C1998251B0E8E (void);
// 0x000004B9 System.Void Obi.LookAroundCamera::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LookAroundCamera_LookAt_mEF29B91315C1AD0156E505C73EF9931CDD6E7718 (void);
// 0x000004BA System.Void Obi.LookAroundCamera::UpdateShot()
extern void LookAroundCamera_UpdateShot_m8C764716D01D701607A13430043C9E5B87A50597 (void);
// 0x000004BB System.Void Obi.LookAroundCamera::LateUpdate()
extern void LookAroundCamera_LateUpdate_m18FD061FDD268DE063DEC79BAD72F08432F1A7A8 (void);
// 0x000004BC System.Void Obi.LookAroundCamera::.ctor()
extern void LookAroundCamera__ctor_m52D5B9733D32E65A768D29868D4B07EB63988D12 (void);
// 0x000004BD System.Void Obi.LookAroundCamera/CameraShot::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179 (void);
// 0x000004BE System.Void Obi.MoveAndRotate::Start()
extern void MoveAndRotate_Start_m81492AAA391819DF463B1781F3F647B8F50E8E1D (void);
// 0x000004BF System.Void Obi.MoveAndRotate::FixedUpdate()
extern void MoveAndRotate_FixedUpdate_mC9020498CC29B200215037EB0EA7508367CCD8BD (void);
// 0x000004C0 System.Void Obi.MoveAndRotate::.ctor()
extern void MoveAndRotate__ctor_mC46EA5D888532267ADC051A68F468CD7FD5D2130 (void);
// 0x000004C1 System.Void Obi.MoveAndRotate/Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_m5653715EE0EAB2271B055D2729CCF8046D2CE78B (void);
// 0x000004C2 System.Void ETryOn.CreateObiComponents::Start()
extern void CreateObiComponents_Start_mB30E01E05FEFA7BA1C6C6AEE8186A1CD2FB5A577 (void);
// 0x000004C3 System.Void ETryOn.CreateObiComponents::CreateComponents(UnityEngine.GameObject)
extern void CreateObiComponents_CreateComponents_m819F1F20C2601A5E5C8AAFCAEA2FAD334F569908 (void);
// 0x000004C4 System.Void ETryOn.CreateObiComponents::.ctor()
extern void CreateObiComponents__ctor_m7C6D26F25B3FE39A9B4269783607CF9F28029A80 (void);
// 0x000004C5 System.Void ETryOn.LoadAssetBundle::Start()
extern void LoadAssetBundle_Start_m105029648FC51AA46E2286E4F24DD78186AFB8C1 (void);
// 0x000004C6 System.Void ETryOn.LoadAssetBundle::.ctor()
extern void LoadAssetBundle__ctor_m30813C43DD742266B3F74AE6BE4307BAD268781A (void);
// 0x000004C7 System.Void ETryOn.PartialObi::add_OnObiAdded(ETryOn.PartialObi/ObiAdded)
extern void PartialObi_add_OnObiAdded_m5F1F706772F4FEB073C6EC4657E856AC8F46924F (void);
// 0x000004C8 System.Void ETryOn.PartialObi::remove_OnObiAdded(ETryOn.PartialObi/ObiAdded)
extern void PartialObi_remove_OnObiAdded_m47896DA4AC0060E8BA14323E36446E7DCC56A7C3 (void);
// 0x000004C9 System.Void ETryOn.PartialObi::Awake()
extern void PartialObi_Awake_mA104E99467750FB09279C3714E3A309D8B14C95B (void);
// 0x000004CA System.Collections.IEnumerator ETryOn.PartialObi::AttachObi()
extern void PartialObi_AttachObi_mA2D9B03ED5F9CAEDD8C47C859101CAF89F2CF059 (void);
// 0x000004CB UnityEngine.Texture2D ETryOn.PartialObi::LoadParamImg(System.String)
extern void PartialObi_LoadParamImg_m4C043D2912C9718F6C849C91898E28B0D0EAA184 (void);
// 0x000004CC UnityEngine.Texture2D ETryOn.PartialObi::duplicateTexture(UnityEngine.Texture2D)
extern void PartialObi_duplicateTexture_mAFE5A422AB2CA8A0F5312FB0405F4F8FA6A39874 (void);
// 0x000004CD Obi.ObiSkinnedClothBlueprint ETryOn.PartialObi::ReadObiBlueprint(System.String)
extern void PartialObi_ReadObiBlueprint_mD1D999630BD5DD7695324E6EB3E55B18605DF324 (void);
// 0x000004CE System.Collections.IEnumerator ETryOn.PartialObi::CreateObi()
extern void PartialObi_CreateObi_m7B19FEF5D807AC60C14A480B266EC3FA4571E603 (void);
// 0x000004CF System.Void ETryOn.PartialObi::.ctor()
extern void PartialObi__ctor_m34B2CB15C30118F75A67EB15FF3624303185EF9E (void);
// 0x000004D0 System.Void ETryOn.PartialObi/ObiAdded::.ctor(System.Object,System.IntPtr)
extern void ObiAdded__ctor_mE4D67E0C5E2CA65A2A3F9A64C83858FD8DA81C40 (void);
// 0x000004D1 System.Void ETryOn.PartialObi/ObiAdded::Invoke()
extern void ObiAdded_Invoke_mCAC30857B95B90A2EAD243A2B66E8066CE60AF38 (void);
// 0x000004D2 System.IAsyncResult ETryOn.PartialObi/ObiAdded::BeginInvoke(System.AsyncCallback,System.Object)
extern void ObiAdded_BeginInvoke_m605DB4E3C85ADB53055741F355481010A855AD96 (void);
// 0x000004D3 System.Void ETryOn.PartialObi/ObiAdded::EndInvoke(System.IAsyncResult)
extern void ObiAdded_EndInvoke_m5D67F76C844AAD6FA5656AA61BC175AE4933479C (void);
// 0x000004D4 System.Void ETryOn.PartialObi/<AttachObi>d__12::.ctor(System.Int32)
extern void U3CAttachObiU3Ed__12__ctor_m323E149194970DEB7C74D42B50BCD92DDFA10E58 (void);
// 0x000004D5 System.Void ETryOn.PartialObi/<AttachObi>d__12::System.IDisposable.Dispose()
extern void U3CAttachObiU3Ed__12_System_IDisposable_Dispose_m606C4A0C39F85E47234D008EA5C15466EE21E6B9 (void);
// 0x000004D6 System.Boolean ETryOn.PartialObi/<AttachObi>d__12::MoveNext()
extern void U3CAttachObiU3Ed__12_MoveNext_mD96F9D75DC8B4705327562DEF0AC62DB11D072F1 (void);
// 0x000004D7 System.Object ETryOn.PartialObi/<AttachObi>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9F06E12394352EAA6288915A8E1A9FD2A9020E9 (void);
// 0x000004D8 System.Void ETryOn.PartialObi/<AttachObi>d__12::System.Collections.IEnumerator.Reset()
extern void U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_m7C8BFAF6A415731CB8B4DF5702063CF293F51321 (void);
// 0x000004D9 System.Object ETryOn.PartialObi/<AttachObi>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mB54B79F9A99CD527CA0F69AA8F4D51E10E398815 (void);
// 0x000004DA System.Void ETryOn.PartialObi/<CreateObi>d__16::.ctor(System.Int32)
extern void U3CCreateObiU3Ed__16__ctor_m62416639B5CB564A96F41F34AFF4479F4EC0F737 (void);
// 0x000004DB System.Void ETryOn.PartialObi/<CreateObi>d__16::System.IDisposable.Dispose()
extern void U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m20CA1D1DC9156B9D0BA1C9B45D02B4276BD0227B (void);
// 0x000004DC System.Boolean ETryOn.PartialObi/<CreateObi>d__16::MoveNext()
extern void U3CCreateObiU3Ed__16_MoveNext_m7C4447AD6A6A0AA0E69502745AB918001B3212B0 (void);
// 0x000004DD System.Void ETryOn.PartialObi/<CreateObi>d__16::<>m__Finally1()
extern void U3CCreateObiU3Ed__16_U3CU3Em__Finally1_mD641B50A2E250A3068F4F1D685DB5FCC77D41E2B (void);
// 0x000004DE System.Object ETryOn.PartialObi/<CreateObi>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1153171D17D5A85E9D1089961F56E8C35031B60E (void);
// 0x000004DF System.Void ETryOn.PartialObi/<CreateObi>d__16::System.Collections.IEnumerator.Reset()
extern void U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDECD8FBF73AC546C48D6663310A224A229D2A20E (void);
// 0x000004E0 System.Object ETryOn.PartialObi/<CreateObi>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m20F4A3067E9EE93EBE98BBB16E3A8C3BED678928 (void);
// 0x000004E1 System.Void ETryOn.SetUpObi::add_OnObiAdded(ETryOn.SetUpObi/ObiAdded)
extern void SetUpObi_add_OnObiAdded_m21C4DFFB388835AAF49AD553672C78EF8083F487 (void);
// 0x000004E2 System.Void ETryOn.SetUpObi::remove_OnObiAdded(ETryOn.SetUpObi/ObiAdded)
extern void SetUpObi_remove_OnObiAdded_mD59D6B40F02F799BD8BACCE38AD4BC2D703FF9D7 (void);
// 0x000004E3 System.Void ETryOn.SetUpObi::Awake()
extern void SetUpObi_Awake_m5D93E087280265178A9FF13232EFE89C431DADAD (void);
// 0x000004E4 System.Collections.IEnumerator ETryOn.SetUpObi::AttachObi()
extern void SetUpObi_AttachObi_mDEA5E44A4A4BD87B69355AC66EC89C5E98AF6099 (void);
// 0x000004E5 UnityEngine.Texture2D ETryOn.SetUpObi::LoadParamImg(System.String)
extern void SetUpObi_LoadParamImg_m9909A680D7CCE7335CE8666AA9F3D7846A6E2C77 (void);
// 0x000004E6 UnityEngine.Texture2D ETryOn.SetUpObi::duplicateTexture(UnityEngine.Texture2D)
extern void SetUpObi_duplicateTexture_mA1C0E528DE63E3E92162504AE5AD53EA8D7C70B9 (void);
// 0x000004E7 Obi.ObiSkinnedClothBlueprint ETryOn.SetUpObi::ReadObiBlueprint(System.String)
extern void SetUpObi_ReadObiBlueprint_m9D40CA80D5A17E67D07AB676B09936C5EE274B75 (void);
// 0x000004E8 System.Collections.IEnumerator ETryOn.SetUpObi::CreateObi()
extern void SetUpObi_CreateObi_m7C3477A5C3F0BB4E8FB587E629AC7F3D5E072425 (void);
// 0x000004E9 System.Void ETryOn.SetUpObi::.ctor()
extern void SetUpObi__ctor_m30344D85BBCF01378960D02915525AF8C275054D (void);
// 0x000004EA System.Void ETryOn.SetUpObi/ObiAdded::.ctor(System.Object,System.IntPtr)
extern void ObiAdded__ctor_m701E89814A33CA5F5BE7E9411DDD5FD4386DDB8D (void);
// 0x000004EB System.Void ETryOn.SetUpObi/ObiAdded::Invoke()
extern void ObiAdded_Invoke_mC429DB252994488CD5076D289BB53EB5746C49C6 (void);
// 0x000004EC System.IAsyncResult ETryOn.SetUpObi/ObiAdded::BeginInvoke(System.AsyncCallback,System.Object)
extern void ObiAdded_BeginInvoke_m87B55DAB46FC383D454FB03FC3DCEADA873C78C3 (void);
// 0x000004ED System.Void ETryOn.SetUpObi/ObiAdded::EndInvoke(System.IAsyncResult)
extern void ObiAdded_EndInvoke_mE333A9CF196776BD8ED733669CC4E3114CAAA3FA (void);
// 0x000004EE System.Void ETryOn.SetUpObi/<AttachObi>d__12::.ctor(System.Int32)
extern void U3CAttachObiU3Ed__12__ctor_mB9723999E31B7F5F4B26C52846EE0EF7F89C03D8 (void);
// 0x000004EF System.Void ETryOn.SetUpObi/<AttachObi>d__12::System.IDisposable.Dispose()
extern void U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mEB86C2EEC5E55A40B077EAE0B7CD5291FBBE4FDB (void);
// 0x000004F0 System.Boolean ETryOn.SetUpObi/<AttachObi>d__12::MoveNext()
extern void U3CAttachObiU3Ed__12_MoveNext_m4329210E28D2ABB6E013515414F1BF9DB5B870F0 (void);
// 0x000004F1 System.Object ETryOn.SetUpObi/<AttachObi>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m124A441CC082D84EB1C7E8C4D09DD36E4BAA1DC4 (void);
// 0x000004F2 System.Void ETryOn.SetUpObi/<AttachObi>d__12::System.Collections.IEnumerator.Reset()
extern void U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mF88ACCD9C8DB2BE87221C306721BE2C8E795FD47 (void);
// 0x000004F3 System.Object ETryOn.SetUpObi/<AttachObi>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mFDF31BB57526DB3E034C630C43113FEBAE74DA2C (void);
// 0x000004F4 System.Void ETryOn.SetUpObi/<CreateObi>d__16::.ctor(System.Int32)
extern void U3CCreateObiU3Ed__16__ctor_m484B3307000BF1D629371B445312F411B8975D51 (void);
// 0x000004F5 System.Void ETryOn.SetUpObi/<CreateObi>d__16::System.IDisposable.Dispose()
extern void U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m884A0B4D81853BF2D53AA1F505BE7B082C5B492E (void);
// 0x000004F6 System.Boolean ETryOn.SetUpObi/<CreateObi>d__16::MoveNext()
extern void U3CCreateObiU3Ed__16_MoveNext_m56DC26122EAF57C1DB79429DAC6017DE9A4871BA (void);
// 0x000004F7 System.Void ETryOn.SetUpObi/<CreateObi>d__16::<>m__Finally1()
extern void U3CCreateObiU3Ed__16_U3CU3Em__Finally1_m7F08DE8D47D1DE526A71C0A416C945CA4B06A59B (void);
// 0x000004F8 System.Object ETryOn.SetUpObi/<CreateObi>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BFCD23E4DD867743952EEC697B33C6095E83FCB (void);
// 0x000004F9 System.Void ETryOn.SetUpObi/<CreateObi>d__16::System.Collections.IEnumerator.Reset()
extern void U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mD9BB880CE88988B91F124368A497E88A3A05AE21 (void);
// 0x000004FA System.Object ETryOn.SetUpObi/<CreateObi>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m6C0A0740DA6B6C547A0784E83EE609B703F35B15 (void);
// 0x000004FB System.Void ETryOn.StitchController::Start()
extern void StitchController_Start_m8482751452DA75C5ED096FCD44A43C40FE8433BF (void);
// 0x000004FC System.Boolean ETryOn.StitchController::FastApproximately(System.Single,System.Single,System.Single)
extern void StitchController_FastApproximately_m1219C630F18967C408B77FC08F724992EEBEA05D (void);
// 0x000004FD System.Void ETryOn.StitchController::StitchPairs(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,Obi.ObiStitcher)
extern void StitchController_StitchPairs_m8E32FA7C7CD918A60CD7A145389EAA96B9C54D5C (void);
// 0x000004FE System.Void ETryOn.StitchController::.ctor()
extern void StitchController__ctor_mEC86ED3793A97122797850E96417ACF769806D5F (void);
// 0x000004FF System.Void ETryOn.UIController::Start()
extern void UIController_Start_m97BEE4041FBDC22BEF794F04D4E86829133E9EB1 (void);
// 0x00000500 System.Void ETryOn.UIController::OpenDemoScene()
extern void UIController_OpenDemoScene_m850F0C18FBBDDAC05A764AE735FE11E20C6EEE9F (void);
// 0x00000501 System.Void ETryOn.UIController::ResetTracking()
extern void UIController_ResetTracking_mFA9E3CDD16C75D98DF177166D8890A6C69F687AE (void);
// 0x00000502 System.Void ETryOn.UIController::ShowGarment()
extern void UIController_ShowGarment_mAAF5DC9E3234DD2CC0AC3028CB38EF2EE8F2B4C7 (void);
// 0x00000503 System.Collections.IEnumerator ETryOn.UIController::StartBodyTracking()
extern void UIController_StartBodyTracking_mB5FBB5B789AC591BEE67379C313B08C96B65AA0F (void);
// 0x00000504 System.Void ETryOn.UIController::ReturnToProduct()
extern void UIController_ReturnToProduct_m4D1F7C0D2B18CBF63F16D5E8998A676A1BC05D5E (void);
// 0x00000505 System.Void ETryOn.UIController::ResetScene()
extern void UIController_ResetScene_mF9DE3599912B251EF247731B2B65CFAD66FFD26D (void);
// 0x00000506 System.Void ETryOn.UIController::.ctor()
extern void UIController__ctor_mECCAE8B658781598BF9F78855801DA15FFD7CA24 (void);
// 0x00000507 System.Void ETryOn.UIController/<StartBodyTracking>d__11::.ctor(System.Int32)
extern void U3CStartBodyTrackingU3Ed__11__ctor_m7D47E8B6DA6315A081D9E41F1EACCA21631D6B63 (void);
// 0x00000508 System.Void ETryOn.UIController/<StartBodyTracking>d__11::System.IDisposable.Dispose()
extern void U3CStartBodyTrackingU3Ed__11_System_IDisposable_Dispose_m0E93CE83F9A57AE50F030818B6979783C7514008 (void);
// 0x00000509 System.Boolean ETryOn.UIController/<StartBodyTracking>d__11::MoveNext()
extern void U3CStartBodyTrackingU3Ed__11_MoveNext_mCCB924FAC1983E7435E1AC28AAC53C8C383143C2 (void);
// 0x0000050A System.Object ETryOn.UIController/<StartBodyTracking>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartBodyTrackingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94FE82124B37C2D7ECD1A80707193342445FF4E9 (void);
// 0x0000050B System.Void ETryOn.UIController/<StartBodyTracking>d__11::System.Collections.IEnumerator.Reset()
extern void U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_Reset_mF038F71D5EF0A73A63C671C81B716DE08772EC01 (void);
// 0x0000050C System.Object ETryOn.UIController/<StartBodyTracking>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_get_Current_mCAF09EA58826F4E0C4B7679D5070E4FE354EDF97 (void);
// 0x0000050D System.Void ETryOn.WpiObiNoBlueprint::Start()
extern void WpiObiNoBlueprint_Start_mD6F6190E00C3014C8A4753D3BD30060A05EA1260 (void);
// 0x0000050E System.Collections.IEnumerator ETryOn.WpiObiNoBlueprint::CreateObi()
extern void WpiObiNoBlueprint_CreateObi_m81599B62AE6C534CFBE93954E065DB7915AB8D32 (void);
// 0x0000050F System.Void ETryOn.WpiObiNoBlueprint::.ctor()
extern void WpiObiNoBlueprint__ctor_m96F210A77C0F84446458BC6095B5DC8751F10D18 (void);
// 0x00000510 System.Void ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::.ctor(System.Int32)
extern void U3CCreateObiU3Ed__4__ctor_m6B7FE0270BDD042C4CF7FEE7E23DA518AFCD26BD (void);
// 0x00000511 System.Void ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::System.IDisposable.Dispose()
extern void U3CCreateObiU3Ed__4_System_IDisposable_Dispose_mF66738AD01D56578BBD2552D07182E3D03750B72 (void);
// 0x00000512 System.Boolean ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::MoveNext()
extern void U3CCreateObiU3Ed__4_MoveNext_m1E7B59A185BD763CA55093BBE12B63219A6DD5D3 (void);
// 0x00000513 System.Void ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::<>m__Finally1()
extern void U3CCreateObiU3Ed__4_U3CU3Em__Finally1_m4597905C4F2FF129447BC7DC1AA0359996771D61 (void);
// 0x00000514 System.Object ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateObiU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5AC2783D38C1D425E7FC54C3BA6D510C3F88713 (void);
// 0x00000515 System.Void ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCreateObiU3Ed__4_System_Collections_IEnumerator_Reset_mD360873099FC44F0F1ED6DEFC12B6BB0A78DD40A (void);
// 0x00000516 System.Object ETryOn.WpiObiNoBlueprint/<CreateObi>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCreateObiU3Ed__4_System_Collections_IEnumerator_get_Current_mBD9F9895E725E1403A8F438CA60098BC62E5B8D7 (void);
// 0x00000517 System.Void ETryOn.setupsingle::add_OnObiAdded(ETryOn.setupsingle/ObiAdded)
extern void setupsingle_add_OnObiAdded_mF7DD3C8BDBB033D6743D82515CFB9EC100D5315F (void);
// 0x00000518 System.Void ETryOn.setupsingle::remove_OnObiAdded(ETryOn.setupsingle/ObiAdded)
extern void setupsingle_remove_OnObiAdded_mA052507D0A9DCAAE4F0CFAC9737E7BD2E5B3EA2A (void);
// 0x00000519 System.Void ETryOn.setupsingle::Awake()
extern void setupsingle_Awake_m2F770577B5CD70B5D8817785BED47DA9D16A0391 (void);
// 0x0000051A System.Collections.IEnumerator ETryOn.setupsingle::AttachObi()
extern void setupsingle_AttachObi_mB362DE10565BBEF3BC97834455F2389F77372CBC (void);
// 0x0000051B UnityEngine.Texture2D ETryOn.setupsingle::LoadParamImg(System.String)
extern void setupsingle_LoadParamImg_mAB04812AB94D3BAF467FAF80EDBD7FAFC161E9A6 (void);
// 0x0000051C UnityEngine.Texture2D ETryOn.setupsingle::duplicateTexture(UnityEngine.Texture2D)
extern void setupsingle_duplicateTexture_mCC15C8482FB793AA2ED4D681FE188A9988BFA654 (void);
// 0x0000051D Obi.ObiSkinnedClothBlueprint ETryOn.setupsingle::ReadObiBlueprint(System.String)
extern void setupsingle_ReadObiBlueprint_mD984FCC608833B7938D43C384D8463B94C6AE532 (void);
// 0x0000051E System.Collections.IEnumerator ETryOn.setupsingle::CreateObi()
extern void setupsingle_CreateObi_m3ED0DF41000191CB095295ED8735804C08B040FF (void);
// 0x0000051F System.Void ETryOn.setupsingle::.ctor()
extern void setupsingle__ctor_m82862E0D77620903AA2AD71E08E4D1503B08EF25 (void);
// 0x00000520 System.Void ETryOn.setupsingle/ObiAdded::.ctor(System.Object,System.IntPtr)
extern void ObiAdded__ctor_mAD4D9F7C90A3E40795A330EB103E80DD5C9EB96F (void);
// 0x00000521 System.Void ETryOn.setupsingle/ObiAdded::Invoke()
extern void ObiAdded_Invoke_m3F69C91AD92E885602B35B16E5BA391B5073F2F8 (void);
// 0x00000522 System.IAsyncResult ETryOn.setupsingle/ObiAdded::BeginInvoke(System.AsyncCallback,System.Object)
extern void ObiAdded_BeginInvoke_m1CB32D8C62387BE7FB364148617871BB52E641E8 (void);
// 0x00000523 System.Void ETryOn.setupsingle/ObiAdded::EndInvoke(System.IAsyncResult)
extern void ObiAdded_EndInvoke_m8537B9566CD0DEAE791714098B81A8AD095511F2 (void);
// 0x00000524 System.Void ETryOn.setupsingle/<AttachObi>d__12::.ctor(System.Int32)
extern void U3CAttachObiU3Ed__12__ctor_m54F90B5E1243762481D29EC8164CB32CA8BFC633 (void);
// 0x00000525 System.Void ETryOn.setupsingle/<AttachObi>d__12::System.IDisposable.Dispose()
extern void U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mFF2C2A49D0534860E17ADBCABCD20753A4FB0C90 (void);
// 0x00000526 System.Boolean ETryOn.setupsingle/<AttachObi>d__12::MoveNext()
extern void U3CAttachObiU3Ed__12_MoveNext_m88317582EAB0AEB2C048D023AB82B8BCAE142019 (void);
// 0x00000527 System.Object ETryOn.setupsingle/<AttachObi>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0DF70F72243D4FF5714E4EFDC1FAA0ABE2CE5E6 (void);
// 0x00000528 System.Void ETryOn.setupsingle/<AttachObi>d__12::System.Collections.IEnumerator.Reset()
extern void U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mDCFE47940ECB0E6F4067A64FF563DCC8FF30CD52 (void);
// 0x00000529 System.Object ETryOn.setupsingle/<AttachObi>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_m6E8969EF8A777A33E45811966F96C714B26D25D4 (void);
// 0x0000052A System.Void ETryOn.setupsingle/<CreateObi>d__16::.ctor(System.Int32)
extern void U3CCreateObiU3Ed__16__ctor_m122099FC8C75500A0683EC9FC6C4C74DB6309A07 (void);
// 0x0000052B System.Void ETryOn.setupsingle/<CreateObi>d__16::System.IDisposable.Dispose()
extern void U3CCreateObiU3Ed__16_System_IDisposable_Dispose_mC60A3B6541E7D65C9CEDC54A79114FAC9CED9FAC (void);
// 0x0000052C System.Boolean ETryOn.setupsingle/<CreateObi>d__16::MoveNext()
extern void U3CCreateObiU3Ed__16_MoveNext_m371F273301ABC6704E292A415A54029F091C754D (void);
// 0x0000052D System.Void ETryOn.setupsingle/<CreateObi>d__16::<>m__Finally1()
extern void U3CCreateObiU3Ed__16_U3CU3Em__Finally1_m5790B4F4732FBC9B264C3205E266CB0DA3DE28AC (void);
// 0x0000052E System.Object ETryOn.setupsingle/<CreateObi>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA77337728FC469EBCAE52E998D106954DF94E715 (void);
// 0x0000052F System.Void ETryOn.setupsingle/<CreateObi>d__16::System.Collections.IEnumerator.Reset()
extern void U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDFE053E3602BE312CEF0388C87F276ED43D09CC3 (void);
// 0x00000530 System.Object ETryOn.setupsingle/<CreateObi>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m793AE57000E8558A92A7B3C3869D377791749309 (void);
static Il2CppMethodPointer s_methodPointers[1328] = 
{
	SimpleExample_Start_m095C1C3007F35B16A5B101B8BEAF4D3CE4165302,
	SimpleExample__ctor_m34DF183D0053400BADD5B81F25453AC415879D78,
	PlaceholdersExample_Start_m06206F1262FE27A3553B9322D63E399DC06DB4FE,
	PlaceholdersExample__ctor_m09120A410A7AE704237E4EBB5C1A252419A1FE5B,
	FadingExample_Start_mA6BD0D92AB1033B0A733F781232555BA21B5E511,
	FadingExample__ctor_mA44FD6768BFE69259FBF1AB82EBDBC6AC78C9527,
	CallbacksExample_Start_mAF647686E7E912DE8FD6665B299A0E356877951C,
	CallbacksExample__ctor_mA55C1F1043BD03F79C682F7803454B78599CB4CC,
	CallbacksExample_U3CStartU3Eb__5_0_mA8EA2B84DA44FCE5D20EDCD476540D9FE79D7E66,
	CallbacksExample_U3CStartU3Eb__5_1_mDAD7A8FE7ACACAB80270E91FFF0AFE80BA923822,
	CallbacksExample_U3CStartU3Eb__5_2_m1EA55EE03648F140001BB6C0C74318DB9191BCA6,
	CallbacksExample_U3CStartU3Eb__5_3_m3C58176A7C17724C2C8DC3C45206A46D663235F8,
	CallbacksExample_U3CStartU3Eb__5_4_m73A75D4D2C5FB9CE958AA0531FB9301B18BC6254,
	U3CU3Ec__cctor_m28E151311BFE4B20F82AE6A27D4BF48C02E9854B,
	U3CU3Ec__ctor_m1E1050F7832E1CC337C5DB3E1EBD61617A022BDA,
	U3CU3Ec_U3CStartU3Eb__5_5_m1672B2E8407D6CB7B606DF4A1B72041EC50B663C,
	TextureExamlpe_Start_mC53A61BFA69D8A5A18CE61FF569039130C5FED53,
	TextureExamlpe__ctor_m654FA6145B22B204670127F3A3C7BD1AA855BB5D,
	Davinci_get_m4F17617DB7DF59BC86508BB1EC91CECE84BD899F,
	Davinci_load_m1855BC2886BF13307026820FBFA198DA1CD356D0,
	Davinci_setFadeTime_m5040EFDA4781C5E8F38358595F4C868B37BFCCED,
	Davinci_into_m54D250C70A9EE1631871D16D42FA901DB69CD037,
	Davinci_into_mFF34CB96C053C0F7DFCEB90F0F02BE005DC72EE5,
	Davinci_into_m0FF5BD988336147F9750086064B9EB2685B53BA8,
	Davinci_withStartAction_mB2FEBD4785FDFD6018D883B5FF31F434CE64613A,
	Davinci_withDownloadedAction_mFFE28047FE6DB6E2416D6FA54B21D9CCF4FB58BD,
	Davinci_withDownloadProgressChangedAction_m4F5ABF6E286A0672CBA4AA239FB8D716D78D9C06,
	Davinci_withLoadedAction_m5AE2C36892EFAC76035654EBE6302C3618897BB7,
	Davinci_withErrorAction_mB01E04C0D86B35617928D2BFB04C287F2B70AB8A,
	Davinci_withEndAction_m1297B51CB93030115FC44C728AE517C92CAB6BFD,
	Davinci_setEnableLog_m4A1CD38507EC33AA3486B591583F1C99B993A60F,
	Davinci_setLoadingPlaceholder_mA3E1CAFDF7305AED0AD6CD0DFAFAD9D1C612E451,
	Davinci_setErrorPlaceholder_mE3635F64EEA1E5957991484B500AFB3720D1340C,
	Davinci_setCached_m230AD2E40E65EABFAF235951A3277376C27D1B90,
	Davinci_start_mD79F1F0AC06ED947D37D3F79FE5D3AA2FF4E7139,
	Davinci_Downloader_m15650852A45FBDAF97CC882F463B75CE7647E772,
	Davinci_loadSpriteToImage_mDF18A2121BFDC3EE5C258EE40614037ACF001168,
	Davinci_SetLoadingImage_mCF973D3BC2858C72D7A2C897DFC7D3F1FFD4C905,
	Davinci_ImageLoader_m556EEE7C8C84B797F2FEE75F6140CC9EB92900DD,
	Davinci_CreateMD5_m5B60E0C8CE93A58FBE0EFD5CEB7D02986AAA6DEC,
	Davinci_error_m2B026CE7554656E9A34D7C6C63400D2DE740910B,
	Davinci_finish_m962F2234E00E3216668D7001C91161930EE0BD59,
	Davinci_destroyer_m4D968D2923B8A98CF2D5FD16551FB44E74F0D622,
	Davinci_ClearCache_m7AFE4493AD866F6C5F39A9B8C5BA87AF09D1E28C,
	Davinci_ClearAllCachedFiles_m6C5942ABE7FE281DF9CACC74908064E2A5DD97A1,
	Davinci__ctor_m6D6A5E4AA5577B2B6CB261467D744C8BCAFF4B18,
	Davinci__cctor_mCBF6FE1F73771438A9ABC324AFD709A37AD7DFFE,
	Davinci_U3CstartU3Eb__37_0_m8B355C931283442F82EA487DF964E84991D80035,
	U3CDownloaderU3Ed__38__ctor_mC0933E4939833D0C437D034A5542BAB979F0D293,
	U3CDownloaderU3Ed__38_System_IDisposable_Dispose_m7BDA6A678D4C61D5C65745A335E01A40C9377321,
	U3CDownloaderU3Ed__38_MoveNext_mFBA98E3B30FB60E1F0E822DA8CE6397B6A0A6658,
	U3CDownloaderU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFF5289C7B9DB78D33EA79C46839A9D83A623BBEA,
	U3CDownloaderU3Ed__38_System_Collections_IEnumerator_Reset_mF1D52F18D10CB6C87FB5481732F1A1EFEFC988EF,
	U3CDownloaderU3Ed__38_System_Collections_IEnumerator_get_Current_mC9A6BA27CA0A438B9DFEAA6658C83E8F659DE96E,
	U3CImageLoaderU3Ed__41__ctor_mB990C3A44F2C25ED035517217354815A4702FF57,
	U3CImageLoaderU3Ed__41_System_IDisposable_Dispose_m4E0B05BF9B4A31543CE38260AAB6B24316A762C8,
	U3CImageLoaderU3Ed__41_MoveNext_m253F0A9FA0F0A676D5FC59785B2266C4DE894301,
	U3CImageLoaderU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB39260EC0C134F7CD19D5DCF10ED803C9ACBCDCC,
	U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_Reset_mA80C9BF77968EC1F27A49592D8F2FCDC749101CB,
	U3CImageLoaderU3Ed__41_System_Collections_IEnumerator_get_Current_mAB3334FDDC841DB00E54398A582A59001D2FAE97,
	CreateObiBP_Start_mA9D0607E12C9870E26D798D69B3E4541F3E7E27C,
	CreateObiBP_AttachOBI_mE908C1B923E8EBC3C69393E6469B518FD34926EF,
	CreateObiBP_LoadParamImg_m9E2016C240982AAF2AC338DD19EB04BC98742B31,
	CreateObiBP_duplicateTexture_mB21ED54C6170AE667A2606A0A4B9EEEFDEEC1F7D,
	CreateObiBP_ReadObiBlueprint_m441101C063382B0E71FF62AF66A42EEC7ABE5615,
	CreateObiBP_CreateOBI_m0DFBBACA70566C2FC62BF6D5C649921563471BAE,
	CreateObiBP__ctor_mEDBF209C5AC411E024E792A4D805E142E4CE57E9,
	OnObiAdded__ctor_m68089FFA3218E04604FEE8EB3DB217EEAE7675EB,
	OnObiAdded_Invoke_m365B4AE77F874ED89FF7B17CAEEAC1C86F654724,
	OnObiAdded_BeginInvoke_mA4087AF86CF308000D8B9E31F6155C3B6B02EEA0,
	OnObiAdded_EndInvoke_m46CE6F2DE8C64F62DD2EEBF545BA6C59DA325E3F,
	U3CAttachOBIU3Ed__10__ctor_m430968C5EFD2BA0C9B96735FA7AC90263C382180,
	U3CAttachOBIU3Ed__10_System_IDisposable_Dispose_mB2D097C019144FCC309F6414F7076DB3E6772006,
	U3CAttachOBIU3Ed__10_MoveNext_m59C23EF4B0FD0BFE71F5FD12F8F52BE6351FE553,
	U3CAttachOBIU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA42B13CC11372A044675C07BCDC32019A08A352E,
	U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_Reset_mC3C7E467B258179D51349B0F83385F1AEAECBFDE,
	U3CAttachOBIU3Ed__10_System_Collections_IEnumerator_get_Current_m1F939691B3EADD74CDA34716DBA3FC4374AC9B15,
	U3CCreateOBIU3Ed__14__ctor_m8DD675C517F191749940BE2ED1FE999EDF7E465E,
	U3CCreateOBIU3Ed__14_System_IDisposable_Dispose_mA7F079A16092731E506A9DC1FCF58913EA7C9EEC,
	U3CCreateOBIU3Ed__14_MoveNext_mE4D02B8A76E4EE639E048716FCB8A51E65CCFBA3,
	U3CCreateOBIU3Ed__14_U3CU3Em__Finally1_mFC2D79C4FB1C8D47AA49BFED56A8CD55D16AD833,
	U3CCreateOBIU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8F4E6D01EEB24AB539251D4E4711125E71EC0A4,
	U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_Reset_m94B2BAABBBE38D5B38A23B14F1849F43283A7C52,
	U3CCreateOBIU3Ed__14_System_Collections_IEnumerator_get_Current_mE32BA430181E59F701837DD6862D39C8B836F94D,
	EventStitchDS_Start_mE706C227160B939141B992D3976BD9FFD945D30D,
	EventStitchDS_Smethod_m34F1103586873E5384DFC146E6CA01443BF76508,
	EventStitchDS_FastApproximately_mB240F5F641695C64BA6EE5C4989716BDF8695A60,
	EventStitchDS_StitchPairs_mE5F7B5FC223F878A44F0E015D3ECC6C755126EB9,
	EventStitchDS__ctor_mECACDB6336EB1589FE9A9A8C98023D460B0EC080,
	OnStitchfinished__ctor_m9F960B86B2ADF3E446B48924E920BE32BF672D10,
	OnStitchfinished_Invoke_mDBBBFCD4534B55D451284D53F9555148CE50161F,
	OnStitchfinished_BeginInvoke_m813CBF8561714189C22201738A00E55F0FE1B851,
	OnStitchfinished_EndInvoke_m54080B43C21BE5BC218B826FBBB773B6811CF521,
	SpawnObject_Awake_m8488C00D42B5DEAF841E22C153B011FB324A94B9,
	SpawnObject_RespawnSize_mE0E3232D9F0388D51F1DB659898D20142FCE3996,
	SpawnObject_RespawnColor_mD3B295CF8C5D23AC4BDC05BCCEC5D78D1528CE3F,
	SpawnObject_AddDrpOpts_mCEF87B38F8E0C9E698CC9137B6ABB689966166D0,
	SpawnObject_AvailableColors_m0D8D4CD97205390CCA950E81B8838C974B4813FD,
	SpawnObject_OnDisable_m39C68380D05C4977A59C1B0A24ED5D658EE2D909,
	SpawnObject_Update_mBF48C6ACB7DEAD5CA8055E7CE0F3D18558E28712,
	SpawnObject_GetAssetBundleExtra_m59ECC3DE93963BD0696FB9CA50F2CA57EF40B5EB,
	SpawnObject_OnPositionContent_m52D3CE7C460401F9F2CF9E6737906F39E728D157,
	SpawnObject_DisablePlanes_mAA601812A7F841BA41FEC6E38CDE5BC5C2B6F8D5,
	SpawnObject__ctor_m5CDF660BE739E940121CC6E870A389E7F74045EE,
	SpawnObject_U3CAwakeU3Eb__44_0_mDC6AF6032E050DE844017BEE165EAAFBA04F7BD1,
	SpawnObject_U3CAwakeU3Eb__44_1_mB8992DB84930877FD37617539617E9439AED0E99,
	SpawnObject_U3CAwakeU3Eb__44_2_m0C49CFD50E46835A000B10F07D526ACDD648838A,
	SpawnObject_U3CAwakeU3Eb__44_3_m7649A734B850981A28B2DEF385131CCEA6981494,
	SpawnObject_U3CAwakeU3Eb__44_4_mB908BB1BE2E3A1BDF9E0AD81FED28253716603CF,
	SpawnObject_U3CAwakeU3Eb__44_5_m50A712DD6056870287C4A7DB9E09435FDD1993E3,
	SpawnObject_U3CAwakeU3Eb__44_6_m7AD4F82BDD6442A8837A750FEF0FEB560EFADB92,
	U3CU3Ec__DisplayClass45_0__ctor_mA8A9A7C0A564846399D4E4A38FAD59BE1ED09953,
	U3CU3Ec__DisplayClass45_0_U3CRespawnSizeU3Eb__0_m21BB8776E520E12F271E1D648CABB7720D8D97BA,
	U3CRespawnSizeU3Ed__45__ctor_m88BFF80AEC54A20B5231575F9D453F32FCD3B8A9,
	U3CRespawnSizeU3Ed__45_System_IDisposable_Dispose_m88C9FEB4B8FB6C1BB4F83458A6AE573C44B74FDB,
	U3CRespawnSizeU3Ed__45_MoveNext_m3DE87B22133E8DE0817D55E582149677CDC492C2,
	U3CRespawnSizeU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m39AC1153721E7A5AE59D1A77B9900B3A6C5E5EEE,
	U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_Reset_m974E70A42014423F08D1565E86B4D39C7E268EFF,
	U3CRespawnSizeU3Ed__45_System_Collections_IEnumerator_get_Current_mA773B08876925350D43ECD146FEE0D954D30146C,
	U3CU3Ec__DisplayClass46_0__ctor_mDA8B629C905F38F2351B9B8943480D6F3ED1246E,
	U3CU3Ec__DisplayClass46_0_U3CRespawnColorU3Eb__0_m6FE7A7F6CA4F78C007ED4BE952ABE23CB0C291A0,
	U3CRespawnColorU3Ed__46__ctor_mD8855F765A5C43BEEF30F012AEB49BD1F2A748DE,
	U3CRespawnColorU3Ed__46_System_IDisposable_Dispose_m249A9D780D8D886614FDED6E01EB42EBEB4A6487,
	U3CRespawnColorU3Ed__46_MoveNext_mD696CA6E0B6E13FBA5223813E47E70C045FBF80D,
	U3CRespawnColorU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBD97D6E8AAABA5C750DC8CB00D5479AC89A5BFA7,
	U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_Reset_m08FE605DE550D0A3942DF553EFA3454649868582,
	U3CRespawnColorU3Ed__46_System_Collections_IEnumerator_get_Current_m5376A4C8A91D27D8E3BE69DBF1A0301865167299,
	U3CU3Ec__DisplayClass48_0__ctor_m29D5138CDFFFE502D3285A1C90AAB1DF8303A1D5,
	U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__0_m79CD505B22CB8A120ECDE7F0289DD7C129451A5F,
	U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__1_m14F1D91C701A53365E7AF7916F198AC99A8A2C96,
	U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__2_m3A1394D1DB4454CBC9268EFCB9DDF8A2C7D94829,
	U3CU3Ec__DisplayClass48_0_U3CAvailableColorsU3Eb__3_mAD73710125C7210312A4ABD748968210C4E1EC72,
	U3CAvailableColorsU3Ed__48__ctor_m9ACE7EA35939C6AF56A3F09CAF876C1ADB3E0CE6,
	U3CAvailableColorsU3Ed__48_System_IDisposable_Dispose_m55EA1B9E30947A469F5A1B916B43EA3FD3E0AC00,
	U3CAvailableColorsU3Ed__48_MoveNext_m59D8B2E33BC8FED66334A8A7639968EE2A109229,
	U3CAvailableColorsU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBE25815C4D63837B720D40C07AE702CCB69C1D38,
	U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_Reset_m22EE8729D104DD16C47EA3D39D7EFD808EAFD7D4,
	U3CAvailableColorsU3Ed__48_System_Collections_IEnumerator_get_Current_m6B887B061C6F11CF5DCDBB3ED3DDFF453CE6EA09,
	U3CGetAssetBundleExtraU3Ed__51__ctor_mE87227D1EC63AF5A16DAFC6449A71A27698EA652,
	U3CGetAssetBundleExtraU3Ed__51_System_IDisposable_Dispose_m67B570BAA9827F4884D1A27EF57D8BCEBC9D7650,
	U3CGetAssetBundleExtraU3Ed__51_MoveNext_m3C109060CED75B272A76DA9ED371353C830E6958,
	U3CGetAssetBundleExtraU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m811274D9475214B976094D0D0AA90CBADAE30578,
	U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_Reset_m1FC9B030B7BF99AA20630D8D4E082DD182443C29,
	U3CGetAssetBundleExtraU3Ed__51_System_Collections_IEnumerator_get_Current_m2AA89470EDD5DAE93E38D463BE1C4D2785AA4106,
	U3COnPositionContentU3Ed__52__ctor_mFEC8E57EA5F72169E05AAAFB7FFFB7D33B910830,
	U3COnPositionContentU3Ed__52_System_IDisposable_Dispose_m6AC2F351F789E1DA2154C3D796F41C90A7CED279,
	U3COnPositionContentU3Ed__52_MoveNext_mB98BA0202DD75E80C70217D6AD3E6A6B01D3BB65,
	U3COnPositionContentU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B864B6D262CCF48697FACB821EF52279A3002AA,
	U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_Reset_m1C71BEAB3372161324E40787AEFDA57E524D6F0F,
	U3COnPositionContentU3Ed__52_System_Collections_IEnumerator_get_Current_m67A830885BDDC192E16E597A6ED50A3E3C92A416,
	changeobi_Start_mCD34167D84D85AD495AB7B3DB2EB00F4D3F06DFF,
	changeobi_AttachOBI_m0BD1EEB038499BD33DD5DC25A1CCF287F0FC81F0,
	changeobi_LoadParamImg_m09C2F6FC69196B1DC5AAFCE8C813AB7F396BF840,
	changeobi_duplicateTexture_mDEC598CA197414A67BAC47DDB36779E20069622C,
	changeobi_ReadObiBlueprint_m2B40A5856AE6265FF25DF55D36ECA03EB684F1A1,
	changeobi__ctor_mE69BAFEE6414E3E0F0C46F2F7FA2EFE72C79AC67,
	OnObiAdded__ctor_m6F885CF87CB3C67751F7CFCCEF93041B7B988986,
	OnObiAdded_Invoke_m113AFEC3143E467B194018A6FDBCEFE83CC3621E,
	OnObiAdded_BeginInvoke_m6AE43FFD84BDDD00EC3D50B0251380D4D29EBEA5,
	OnObiAdded_EndInvoke_mF4A074F28AE3D865DEB003F7ABEFFC27DC8E1FDB,
	AutoCannon_Awake_m0B3F9E1A76494A9B990C2592355BCA021182410C,
	AutoCannon_Update_m29856A10678515925F64F8F37BFC519CB2FC5B3F,
	AutoCannon_Shoot_mF63F6DA0A7FB8DA424F38A5853EAA1A6B2255444,
	AutoCannon_Launch_mE945B7E518A8168885A2323DB34FAD265EC3BD6A,
	AutoCannon__ctor_m423E23A23D43AD26C4C240167226FDD9FDEC5FDA,
	U3CShootU3Ed__11__ctor_m884BF51AF4DF8056D4965C028CF7CFF29DC42E43,
	U3CShootU3Ed__11_System_IDisposable_Dispose_mE4F09F48E287A53FF7E525CD9413A83AC867F28A,
	U3CShootU3Ed__11_MoveNext_m4309144FBA4D6A962B4D5BEC3214C4D2CDDB9B87,
	U3CShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C060564EA19770828A39D2CF575AE4E6387CF6C,
	U3CShootU3Ed__11_System_Collections_IEnumerator_Reset_m95B67AE27CE9C309D876182ADFCB7EB45FAB2253,
	U3CShootU3Ed__11_System_Collections_IEnumerator_get_Current_mD7F7BAED04E09C7275269837A41C3C5ACEE25825,
	BoatController_Update_mDCE9F01212A47882B1989C9AEE0F16FE4E01E22D,
	BoatController_Respawn_mA6571213D913950C16566945D1A107A4A25CFA85,
	BoatController__ctor_m4FA5C465BE85611BC18F8A8764479238D10447A1,
	BoatFinishLine_OnTriggerEnter_mEB3243D3FA9435FAEF7C2E53DE4B6C64A61B7197,
	BoatFinishLine__ctor_m9A36971472140FF28E16AF892B672F4C2377C1F6,
	BoatGameController_Die_m364402719425090B66310A9A1CDC13781A725C18,
	BoatGameController_Finish_m7EB191C5A00772C7894A8EEE18EF545657763616,
	BoatGameController_Update_mE44F8247EA88E1822BDE86E3A5F1511CF93FD200,
	BoatGameController__ctor_mB8D2B757DF9876305711829C92335AA506F52A58,
	CannonBall_Awake_mDF03DD952ABB744D517E5810E3BA6F51422BBFD9,
	CannonBall_OnTriggerEnter_m91362C26D92B656EE7169489715D45F06799EF89,
	CannonBall__ctor_mBE2D057682E5E5E02573C5DB6FAC7AAE229C2495,
	ClothTornEvent__ctor_m72D4C4974CB71597C3A89C0452D246AE184BEB38,
	Mine_Awake_m58C8F5FDAED78C9F8B65532C6D9EFE9F544E6212,
	Mine_Update_m79DE943463F60EF84971D18A447EBCB2A4410A12,
	Mine_OnCollisionEnter_m690D60F3CF73D839847FD50936EFF2A12EDB8251,
	Mine__ctor_m4764AA96D27727EB5A0D1BFE0C8DC7A7A92DA5E6,
	RaycastLasers_Update_m21105C5A886E47994304701EAFD2EF7535169044,
	RaycastLasers__ctor_m7FCFEF7FAABF8B06CBD65E950736359950554A2B,
	Rudder_Awake_mC1C287C9FB4AC93C9A939AFA62E063D6F5F31982,
	Rudder_FixedUpdate_m9D7C8E45614E9C3DAA0D7AE6821248D1F921DCBB,
	Rudder__ctor_mD7D5560A62B2EFF3A53FDD201B82E30AEC667BD1,
	SackGenerator_GenerateSheetMesh_m01FD9E7358BFAE145976E183AEF2E822112AD881,
	SackGenerator_GenerateSack_m5749634500E10A9C49CC02D5B461D5CE4EC62F74,
	SackGenerator_Update_m61EB7A1965BBC01ED18EF37657A3F0A90AA25D51,
	SackGenerator__ctor_m840AC8E93954A7C871F341081AE309B9052DE168,
	U3CGenerateSackU3Ed__5__ctor_m60AC74DB36D70827F636F2922CB635E9B7F977F5,
	U3CGenerateSackU3Ed__5_System_IDisposable_Dispose_mC3E89951FB26F5B4CE2B7E27251604B8F4376658,
	U3CGenerateSackU3Ed__5_MoveNext_mB6D8C3958242578BD39A2524139636E33D460A3F,
	U3CGenerateSackU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m748A15AB6A05CC78B95300CA85662577A2EA5F19,
	U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_Reset_mD87A155F07288E5B5248CE7AA284740859004F3F,
	U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_get_Current_m61246B7B8E9A24805CF9F6D7ABA51935CC7BA088,
	StretchToColors_Start_m7C77408010D4ED8A560CF9C51C6B8A606EAA1561,
	StretchToColors_OnDestroy_mEC83FB00FC818CF84333AA4757FEEB4D4BAA71B0,
	StretchToColors_Cloth_OnEndStep_mCCA1EB30709882BF5ACBEE581725AAA3359BB9B3,
	StretchToColors__ctor_m2620D123D4418AF0F12B010509CBD374A52268E8,
	WaterTrigger__ctor_mDF14818F4DDDFA48CE03DD4ED16387C76A0D37F4,
	ActorCOMTransform_Update_m42CC68376DF28C96387BACFC64C48515AE9CE469,
	ActorCOMTransform__ctor_mB7B969737BB7416B0E66C1CD4B8FDF1E8A7186CD,
	ActorSpawner_Update_mBB452D66B51B50FE636D74E7FF957A10A1FE2A07,
	ActorSpawner__ctor_m48C1E536BA52A4F4EDE74F5B90749B4619F13C86,
	AddRandomVelocity_Update_m6584A3F85D2E61F6D420B0B911B3C61637008449,
	AddRandomVelocity__ctor_mC1CFE0D59EF94BA30BA6AAB81CA6FF0899F84DDF,
	Blinker_Awake_mC92D3CBC36B46F324C8197377BC40A434109C876,
	Blinker_Blink_m4082F3A19006F464F4C14105BED460D5A1A0E273,
	Blinker_LateUpdate_mEE40A5EDA3EAFBB9CAD39BC413CB921E15CF6CCB,
	Blinker__ctor_mAE1E307D31F5C3BB83001FBC5B2D61F827A69F3C,
	ColliderHighlighter_Awake_m5920B8F626704DB40821E9BB9A85FDA1E0BFE4B3,
	ColliderHighlighter_OnEnable_m6F1425D169D88CC4D90E0A8F144A05C55F7E03B6,
	ColliderHighlighter_OnDisable_m2780DDE2CFE790FF4B04AD45654BDB74DD752893,
	ColliderHighlighter_Solver_OnCollision_m8A90B739BAB8461A0179C2B4CD4EE87AB3DE8D30,
	ColliderHighlighter__ctor_mBE8B58BC252B389A907738D87DAD06F094A62587,
	CollisionEventHandler_Awake_m744FDE1AB7D3F6777399B5108896E5E2CC61FE93,
	CollisionEventHandler_OnEnable_m2D3F33422D785CEDFCDCF1012A0B37B5F3ACB9F6,
	CollisionEventHandler_OnDisable_m75B03CB65B3D0226C04FF77A99D488596144BC32,
	CollisionEventHandler_Solver_OnCollision_m484E685CBD1E502504C567FD2D5CCB6F0860D616,
	CollisionEventHandler_OnDrawGizmos_m7939D47B098838344A41DD08F547354E268E07A8,
	CollisionEventHandler__ctor_m92805BEBFF6E7114772EF8DFF4D5C244467A479B,
	DebugParticleFrames_Awake_mA5C0197560CB8F73D7472974A693020EA29B28B3,
	DebugParticleFrames_OnDrawGizmos_m51378254E7930D9050D3355F810A3BC15A4CBBED,
	DebugParticleFrames__ctor_mBE2F687FB03E680B1957BB4D2C9F19E43F3CF260,
	ExtrapolationCamera_Start_mCB1FD551CBFB047EEA0FA534CE64CA969640D51E,
	ExtrapolationCamera_FixedUpdate_m622D9ECE2C7F91E1C5A70068B94B7C3E03AF4523,
	ExtrapolationCamera_LateUpdate_mB90259A761A12F0D8301E75BC381A6C0B32B443D,
	ExtrapolationCamera_Teleport_m8A6D37587FD1DF2518851886F5BE70B897DE0B67,
	ExtrapolationCamera__ctor_mA4E8D57C9E25BC7F9DA5676D79A7FC5505073032,
	FPSDisplay_get_CurrentFPS_mA9019EF8EAA0384D37F2DE714D84E5413E783C72,
	FPSDisplay_get_FPSMedian_m6ACF1BBC790E915C8CE3D94C797CE4A4E8F116DD,
	FPSDisplay_get_FPSAverage_m52F2116EA4DD6DA75E483FE24D21321CF9F2D4C1,
	FPSDisplay_Start_mF1CE43CCE2AAB57ECD8D8B623B7E7358AB163C55,
	FPSDisplay_Update_m8740416265A05D0C56CA05EC0DE3A12E5132B028,
	FPSDisplay_ResetMedianAndAverage_m82840B2C8D0189EA211AE4B14A8C94B22D932F70,
	FPSDisplay__ctor_mC4AD184EF09FAF5C43D89D0F7C211B2754D95B18,
	FirstPersonLauncher_Update_m90772B87D990D9020988335EBC4E68DD68D177F5,
	FirstPersonLauncher__ctor_mEE941D947823862AE74E5FA285B42A02799BC5C9,
	ObiActorTeleport_Teleport_m83F48614A9D7AE20261E92546E28B7C65C26DA1D,
	ObiActorTeleport__ctor_mCC83730596EA6276332F31F582F5C8D1F08EE019,
	ObiParticleCounter_Awake_m42D6C0254A2B27439769A96FE50705D33EC62B98,
	ObiParticleCounter_OnEnable_mA9798F6382D0620125A95B047C974EBA660926CB,
	ObiParticleCounter_OnDisable_m3224433810D75B192BD03D399ADE656864BBD594,
	ObiParticleCounter_Solver_OnCollision_mC585B439D90C23BA5FE74E65D3195651DCAEF392,
	ObiParticleCounter__ctor_m3107B0DABE8BEA1C8EB84C5A8EEEDB3C3E4B1B52,
	ObjectDragger_OnMouseDown_mC96AA07AABD47631E097AEA08E9A81AE1506EEEE,
	ObjectDragger_OnMouseDrag_m7E0B861E5546C709DED1ACF6F33ED31D6004FA6E,
	ObjectDragger__ctor_m97083AF9B77C7E2554A3D0701C6FAEE5D2282A6C,
	ObjectLimit_Update_m6689D4679CA59887B038EE4BD3F2294753C57E38,
	ObjectLimit__ctor_m8F9F789DA4B949FECADA677D21A26E893A0A5F41,
	SlowmoToggler_Slowmo_mF683360609D33F0B08F26A1019AAAD2BA21C8404,
	SlowmoToggler__ctor_m17E4BB8E004DE90384ADF6074E74BC7C383E7001,
	WorldSpaceGravity_Awake_m20A0105B37EEF41CE69A528E59C13099276E4F2B,
	WorldSpaceGravity_Update_m9BFD6836B3B4FD9F671F9CC77622279143C1F72C,
	WorldSpaceGravity__ctor_m8A54BF21A4069DBE4B27EA67FDABDC1C26766539,
	AddCat_Start_mDA135BBA22B0266142900DCF8DA177E00A87F565,
	AddCat_AddCats_mC9B200DDB37189AAA7E2FBDE470ACC1B7B50F654,
	AddCat_InitItem_mA181501CA9E0B52243C8E766DE44707295D7C19E,
	AddCat_MakeItem_mC5AA410D891F645085AFD61F7B014A700C3E56A2,
	AddCat__ctor_m6417B73991CDD9C134D23D047B81372C070C1598,
	U3CU3Ec__DisplayClass16_0__ctor_m115880E935FB7D335129E39E720465FF16A58570,
	U3CU3Ec__DisplayClass16_0_U3CInitItemU3Eb__0_m2895CBAC8E8C9A9ACB39231D10DC6649EADEB4C1,
	AddItemComp_Start_m2378DEEE58C3A77F9AA3989853B870A86AAFF92B,
	AddItemComp_SetPrefs_m930EF2A4A0384D344FBA9D9C5EACCB14125E2FB0,
	AddItemComp_ButNextRoutine_m491D1048FB724E16FD51D5C8FA3B6E4864E645F5,
	AddItemComp_DelItemListWait_m304E9147CFA670BF3F714A6326A4C0D40B9AEFC4,
	AddItemComp_ButHomeRoutine_m12A488E1C2E634DBFD20C4DF299FF5B998369AB2,
	AddItemComp_ManagerReset_m7C0F20A88B512978DF5EF6A53D0CECB0A17C4D01,
	AddItemComp_ButSignOutRoutine_mAD978974F12CC9E41D44B7FB3E307E70173CD761,
	AddItemComp_ButPrevRoutine_mB7E7EF57C738F6AECF963DB4FADFAA20121C4228,
	AddItemComp_IsFinishedDownloading_mA3D8DE7943509E57F7A0A2E8DD12BA0E8251E83E,
	AddItemComp_FilRecRoutine_m9994A64E295BBD9D52D828B6184F2CAC33032C0E,
	AddItemComp_FilTypeRoutine_mDAAE833DE05D0C9210A61E10C69BD790D1951783,
	AddItemComp_FilSrcRoutine_m7A9182A31CA54285EF9BFF3C760E3A76CDED3C90,
	AddItemComp_FilUpdRoutine_m4ED3B5CE0BA022BEC13C4AA0DBA0393E0C0AB507,
	AddItemComp_OnDisable_m19DF731B0B194146D181F3D5DE9A4309F58AE3E9,
	AddItemComp_StopDownloading_mFE88CA2D4154EE4AC05F5C98970D9E1EE51B4B99,
	AddItemComp_Update_mB971A2D24EAE16CABF31D48E2A35D91030288680,
	AddItemComp_ResendRepeat_mF9585698444E6288F3D0BFEDE9E6212E7240CD06,
	AddItemComp_ResetMan_mC801238665C33571644CFAAAB85479847A165D1A,
	AddItemComp_scrollrectCallBack_m067C064D782129521DF003F0AD4F01663EDB5664,
	AddItemComp_tmpScroll_m86AE102F3384CE2C55BA0B5F9D750F846FD95D9F,
	AddItemComp_BeginGarmentCall_m087BB0472C9CCF6A072EFEFC1C2593AB99A16164,
	AddItemComp_AddImages_m23FB81BA2924B6546C402D2F80B29DAECE377F5D,
	AddItemComp_ToggleLoading_m3C80531EDA22129E7716FE071C066B9B0FFD9D4D,
	AddItemComp_ImageTexHandler_mAFE4B133389E581FD3A8C6CF2141FB043A1932BA,
	AddItemComp_ImageAddingProcess_m7A5ADBEF5D1CC2AE157522E14FD7D6F104BFE1A6,
	AddItemComp_ScaleTexture_mACBB04DE9E10947DD95E02B7AE4C6697338B67A5,
	AddItemComp_FbaseQueries_mC730E8CD3FF08272E05325E76D173716C0A20A70,
	AddItemComp_LayoutFix_m3B45FC03118B2EE7071CAE387DFE3B91D689A41C,
	AddItemComp_MakeGarms_m32492B4F4016722F296A65C8146C3B286D473052,
	AddItemComp_AddGarms_m9A28486C6857D4EDCB3B9E8F2C5E1EBB793738D3,
	AddItemComp_UpdateGarms_m7B70106F734C08C934EDE76E6561A0B9B3A5D011,
	AddItemComp_DelItemList_m2A5233C3B457722A0BADAD248EA04A4073975266,
	AddItemComp_ToProduct_mC0687748A4043499BEEC4066AB0A9578BEF8AE87,
	AddItemComp_InitItem_mE2CA196FA40E4EB2E4DADC83BC7E1AD0A8EFA93D,
	AddItemComp_ImportImage_m193885C921D7C5A2ACD4867D1802C6551B6D375D,
	AddItemComp_tmp_m574D1139763D97D78638D7B17E389CC5E75A734D,
	AddItemComp_MakeItem_mBAFE3C5FE3E9CE73029DB9EBDCB504C9B3A07F3C,
	AddItemComp_clearManager_mF27F2080DB2BB8371CDF6A3F2287BF1C77FA4A51,
	AddItemComp_filterlist_m84E3172E7E2EF77D1576A499BC395771B1DCF056,
	AddItemComp_rectcalc_m7808BA0EE83F3CDD43DCE023BE2A625C185E9910,
	AddItemComp__ctor_mEAC8A40C02E96CE194488F987740DD60B45B727E,
	AddItemComp__cctor_mA249536321F3E5E026EB6FA6CC1A9022A3BD8C46,
	AddItemComp_U3CStartU3Eb__102_0_m2096C692CD9442FC46902A0F16DC4328A918C7D6,
	AddItemComp_U3CStartU3Eb__102_1_mC08704029D6F7E6D445DCC23E94F12A359980080,
	AddItemComp_U3CStartU3Eb__102_3_mB54E039BC3C1F73EEA1FAB29614E7F3ECD241CC9,
	AddItemComp_U3CStartU3Eb__102_4_m3E9597F4595EC78C0B9618C3F09A04B8BA85C040,
	AddItemComp_U3CStartU3Eb__102_5_m38832F72F9C87F8CC9D1500BA5733AFE8A343B35,
	AddItemComp_U3CStartU3Eb__102_6_m597E16370DAD0288AD7EED4CC61FB32F1C99F481,
	AddItemComp_U3CStartU3Eb__102_7_m65B2483950F056B5C1287834E018A66BD1DD4B43,
	AddItemComp_U3CStartU3Eb__102_8_mA016DA8AAEB55699B162DD1DF66FD216946D823F,
	AddItemComp_U3CStartU3Eb__102_9_mE809BBAC55ECD75FFA6A593D4143EFB84ED9C9E5,
	AddItemComp_U3CButNextRoutineU3Eb__104_0_mBF7052D323E37B73886148348762818ACE5C002E,
	AddItemComp_U3CButHomeRoutineU3Eb__106_0_m5C45AB4CC871CC952A74FDD92A52CC19E58F8072,
	AddItemComp_U3CButSignOutRoutineU3Eb__108_0_mA7DC2D1B6D727E144DEAB10191F7F5E6F68A96A6,
	AddItemComp_U3CButPrevRoutineU3Eb__109_0_m5803B19502851521C955B2930AF40EB36847F124,
	AddItemComp_U3CFilRecRoutineU3Eb__111_0_m0F1EBEF4BE8FA985651D2F375CBD989D0EEA42F3,
	AddItemComp_U3CFilTypeRoutineU3Eb__112_0_m99DB59CD2666C0D6462C28AFD55D09231AAC0741,
	AddItemComp_U3CFilSrcRoutineU3Eb__113_0_m30BA0187BE2962F46D35967E21A811E8E14F5922,
	AddItemComp_U3CFilUpdRoutineU3Eb__114_0_m5D40CD366F70EA64C5CF7F0FBF39CA0F11B29FCC,
	AddItemComp_U3CStopDownloadingU3Eb__116_0_mA97AEED6FC22F8068EB6F9021BCCD84D39A30E57,
	AddItemComp_U3CtmpScrollU3Eb__121_0_m025E168AD6D3C68E5476FB87A5F4B7E902C67EC7,
	AddItemComp_U3CImageAddingProcessU3Eb__126_0_m440ADB857BC9E9AEAA14DBCF5C2B78C6FA04D3B5,
	AddItemComp_U3CToProductU3Eb__134_0_mE07CC29E524AD36543895E68382EF7AA4D07132A,
	OnReadyToReceive__ctor_mF3A576F94D54B15D64697B37BF25728303239D59,
	OnReadyToReceive_Invoke_m6EC5E059540BC5C88493C14066A7B4FEFA0EE300,
	OnReadyToReceive_BeginInvoke_m20BB74F40726337268C02A96CA6539DBC2360DCB,
	OnReadyToReceive_EndInvoke_m873E947BD439CD2FF2FC3BA03D0DA5F541E1AF8D,
	OnReadyToReceiveBatch__ctor_m00C1EA99DEDA8EB19DDDA0516A67359D6AD1D2D3,
	OnReadyToReceiveBatch_Invoke_mB38BEB55A0582959CE3F6024935165254F8A8784,
	OnReadyToReceiveBatch_BeginInvoke_mFB82CE2E3DA974D75607AFE17BEA33C3FDD2F4EA,
	OnReadyToReceiveBatch_EndInvoke_mF47E310BB5333982EFECB732BECF9E4C899F4BDE,
	OnCloseNav__ctor_m35F6534D1999A8BD8268527FD478407DCC5F3FD4,
	OnCloseNav_Invoke_mECB527F7FB2F00BD9627BF529DDDAF19187B4C86,
	OnCloseNav_BeginInvoke_mBA336430E2A9B372EAA68D34221F5FDD807C9BBB,
	OnCloseNav_EndInvoke_mA07F88C703000C6BC34BEC45178AFC80230602A2,
	OnCloseFil__ctor_m1D73DB60FF63E9F8FB1B08CC5325EFBAA3A50E6B,
	OnCloseFil_Invoke_mBBDE8B377170FAF6F5DF388A77E180CF3F266201,
	OnCloseFil_BeginInvoke_m2CE499C0E0715876552AB71AE5A957FAF3DFE4C9,
	OnCloseFil_EndInvoke_mF8BA63B13E89747BA27A38F3D168A87CA89F7DCB,
	U3CU3Ec__cctor_m583470F1CA6B150D1A783A354C2D9F14C653B89F,
	U3CU3Ec__ctor_m845929B7233547C64476F81B0A950327499363C3,
	U3CU3Ec_U3CStartU3Eb__102_2_m91173F44425DEE3A805AA673005927559D0D8327,
	U3CU3Ec_U3CFilRecRoutineU3Eb__111_1_m216D843F76FC97380F7901E38F3AA443D14611A1,
	U3CU3Ec_U3CFilUpdRoutineU3Eb__114_1_m7E0311EEAFF9F13C99577F71BB1C38CA7D6AC1BD,
	U3CU3Ec_U3CInitItemU3Eb__135_4_m32A40B9D8EA231CFFD1DE26BB87B917868FA5FA1,
	U3CU3Ec_U3CInitItemU3Eb__135_3_m74B2E2BB3C8AE4C43A338C61D6CB89EBAA8F23AD,
	U3CSetPrefsU3Ed__103__ctor_m8FB32A3EA183CB8090C3DBB50B0109656692BBAA,
	U3CSetPrefsU3Ed__103_System_IDisposable_Dispose_mB4BEB73E7A346D24D2DEF57F701A013AAC8B83EE,
	U3CSetPrefsU3Ed__103_MoveNext_mFE0F8D59C1D006153BD286BD482A35B1FA2177D1,
	U3CSetPrefsU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093F4D0EFF81DDC45B7624C209E883E5E07B08E4,
	U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_Reset_mB60350DCF81DA37ECE9FFA39FB691C9836E3E285,
	U3CSetPrefsU3Ed__103_System_Collections_IEnumerator_get_Current_mCDD4AC07D175D9271B089B5B72A7BAEC106B0F1F,
	U3CButNextRoutineU3Ed__104__ctor_m0085365472595061903314EC1E4FFF79AC9C485F,
	U3CButNextRoutineU3Ed__104_System_IDisposable_Dispose_m23377156CD91B1D2CFE09DE547413B94CD1B85E3,
	U3CButNextRoutineU3Ed__104_MoveNext_m0CA753EBD36434BA824FF41C31ABE77B4A2E680D,
	U3CButNextRoutineU3Ed__104_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2CC7DBC65BBF1EE481C3463FCC966E79EBAB87A2,
	U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_Reset_mDBC944F58A8A27B4EED103ACD698716C6F694C39,
	U3CButNextRoutineU3Ed__104_System_Collections_IEnumerator_get_Current_m9EF10872F1E79554771AD85FBB5C87D3E006067B,
	U3CDelItemListWaitU3Ed__105__ctor_mE56DCC6198C2DA9F89186F244E620BE6A4832C24,
	U3CDelItemListWaitU3Ed__105_System_IDisposable_Dispose_mAFF526E03E52DD0CF05DD04626EB5F6DA87EB364,
	U3CDelItemListWaitU3Ed__105_MoveNext_m389C02AFD8D8FE73EAD4E12066214CC536F8183F,
	U3CDelItemListWaitU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75D67143542BF51250E417F47999162F3082217D,
	U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_Reset_m10DD860E4ED3C27E7B607E6F22AC712A8DED3025,
	U3CDelItemListWaitU3Ed__105_System_Collections_IEnumerator_get_Current_mCF63A193F61F5E9CAEE380812754088DF047E6DE,
	U3CButHomeRoutineU3Ed__106__ctor_mF0F347434E4D78C82D94BBCBC18072E688202C2B,
	U3CButHomeRoutineU3Ed__106_System_IDisposable_Dispose_m9B496850533015E4F608FC096B9B500AC23026F2,
	U3CButHomeRoutineU3Ed__106_MoveNext_mD76BAC02F8E6E099D361501E55F0EDC4D334887B,
	U3CButHomeRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDEDE7FE05FB39A15955781EDD78073457E2CA83,
	U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_Reset_m3BB033F0512E738AEA51BFFF949335B96A414367,
	U3CButHomeRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m4FCD177E7D65FE61EA91F37BBA0049937F2B08BA,
	U3CButSignOutRoutineU3Ed__108__ctor_mF79A9854726851E2C5773958F6873EBD682799D5,
	U3CButSignOutRoutineU3Ed__108_System_IDisposable_Dispose_m8C68B05A8AE1C61C64F9712905CC74FECD3E690D,
	U3CButSignOutRoutineU3Ed__108_MoveNext_mE5D8B4BF0ABFB884CA194B1DB5A19DA2CCDEDCA8,
	U3CButSignOutRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m738D1C6B18D3CAA63D518C2B7C0D5DEE4FD8D948,
	U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_Reset_mB3CE8FBA1B3287E592432EDC4554176C8B3E6690,
	U3CButSignOutRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_mEE50C74269A8013E6987EB9EE0F4249E263BDE85,
	U3CButPrevRoutineU3Ed__109__ctor_m4A20BECF42F8B7F4C026E469599A2F8860953006,
	U3CButPrevRoutineU3Ed__109_System_IDisposable_Dispose_mB8627B57D8EDF2A14E07394DE8C70C5C8C559B13,
	U3CButPrevRoutineU3Ed__109_MoveNext_m13B609F386F42BBE73F68A4B5820C6BFDFA77636,
	U3CButPrevRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFCB870E8A5E90D88232266CDBB7CE407C60B363,
	U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m521533485C83DBEC45F0E4E4609BE8269DEA0434,
	U3CButPrevRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_m9799E1C63B54F2CD96262A73C227B1F136A9D3D5,
	U3CFilRecRoutineU3Ed__111__ctor_m9F1A768D3DD7CED2C75103495721D60FDA5281DA,
	U3CFilRecRoutineU3Ed__111_System_IDisposable_Dispose_mE9A6542D040200FB09383043C06B14A0C59EFFCD,
	U3CFilRecRoutineU3Ed__111_MoveNext_mD5B25DCC95CC845DCE1950796C70AD520DFFF718,
	U3CFilRecRoutineU3Ed__111_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE2C803E973C3986774B9663A022FB34250DB0C2F,
	U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_Reset_m8C8316FC76E78AD0D70ABDF37C157B0874E4B59A,
	U3CFilRecRoutineU3Ed__111_System_Collections_IEnumerator_get_Current_mCA6F124A8453688732770F731E9794591A34EE0F,
	U3CFilTypeRoutineU3Ed__112__ctor_m3541807D0A5921CF9362E45C07E1B78D1F5628D5,
	U3CFilTypeRoutineU3Ed__112_System_IDisposable_Dispose_m7BB15FC367573484AAAC7342C6F8E7C805787EB7,
	U3CFilTypeRoutineU3Ed__112_MoveNext_mAEFC1ABC86F3BBAE9138A8564976A86F6B185C99,
	U3CFilTypeRoutineU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD81030DA4DF6EABEF2C3440166062FBFDC1EC41E,
	U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_Reset_m01D8D7C4430B1CB6B9D833F137FCB920DA0C41C5,
	U3CFilTypeRoutineU3Ed__112_System_Collections_IEnumerator_get_Current_m1CED9C1E2ABEF0F14A0E4EFDE9B9E89C79B32D67,
	U3CFilSrcRoutineU3Ed__113__ctor_m4425196A106EDB43B1640CE7D4F9F6769C0A1CA7,
	U3CFilSrcRoutineU3Ed__113_System_IDisposable_Dispose_m36F97F0CD2C466182E98033B9D28AAB0BB634CE2,
	U3CFilSrcRoutineU3Ed__113_MoveNext_mEFF05ED821D7AC181DC13B375212EE2789450FBB,
	U3CFilSrcRoutineU3Ed__113_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4503567FA847BD7C5351F587C42DC420F4C1417E,
	U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_Reset_m570D349FC4ACAF60F6A794BA378A3E009A92290D,
	U3CFilSrcRoutineU3Ed__113_System_Collections_IEnumerator_get_Current_m5FD53CA1FDE0E65DB09E071A5AC660772BDD5AA8,
	U3CU3Ec__DisplayClass114_0__ctor_mC265B3C8289A919A3AA1A4389CD06BEC1385B1EA,
	U3CU3Ec__DisplayClass114_0_U3CFilUpdRoutineU3Eb__2_m8ADC6A1CBAA29A4B3FD4BCCB4B2EFA921B8B6C4B,
	U3CFilUpdRoutineU3Ed__114__ctor_m7E9325EBC3194C561289526D7988BFC13917F6BC,
	U3CFilUpdRoutineU3Ed__114_System_IDisposable_Dispose_mF09C79B7A5344B289C5788B9EEA124E6960B4FA6,
	U3CFilUpdRoutineU3Ed__114_MoveNext_mDAF940D785828D3150C3F3077C0CC26B42723323,
	U3CFilUpdRoutineU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D53B015E411EAB5F586060E9184710FC2B3CEBC,
	U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_Reset_m4A15596167AA539664B1C694F372712A981963CF,
	U3CFilUpdRoutineU3Ed__114_System_Collections_IEnumerator_get_Current_mC7DDE1817B9D41C593B61E33C0F6E76DE1E213FC,
	U3CStopDownloadingU3Ed__116__ctor_mB8BC0F77581ED0D22C2DC3707858178F36FA1E1C,
	U3CStopDownloadingU3Ed__116_System_IDisposable_Dispose_m9A2B019169627DC744778F6A581A1341FBBBF5BB,
	U3CStopDownloadingU3Ed__116_MoveNext_mFFED1CCD93396F904CE1644D742E1FD063873A94,
	U3CStopDownloadingU3Ed__116_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6EFAF6C8391D7EF16F8A34DA187B9F19859F769A,
	U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_Reset_mC9B5D5C32A29DA3C1F6822956056ADC748174746,
	U3CStopDownloadingU3Ed__116_System_Collections_IEnumerator_get_Current_mB8DD0110D702CB79F468A1D31EDE5C4DBA965AEA,
	U3CResendRepeatU3Ed__118__ctor_mFBD5278226634B801677A62FE848BDDBC9D38DA9,
	U3CResendRepeatU3Ed__118_System_IDisposable_Dispose_m5C96DFF10E9036353B869A60C5B1DC6C77103A8C,
	U3CResendRepeatU3Ed__118_MoveNext_m29F74973751C2D1DA955DB9ED0EC5AEC0BB47A41,
	U3CResendRepeatU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2D4886779C7D6108537AFC05D1F84B5960FB557B,
	U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_Reset_m005EF473A7FE5497F43D0746E0ECCC6C416EB77D,
	U3CResendRepeatU3Ed__118_System_Collections_IEnumerator_get_Current_m23FB6E14BEDD84646381D9C2756B6A1DD077C340,
	U3CtmpScrollU3Ed__121__ctor_mC57CFA92A60222C607BBFE9DD9070ECE9FC02308,
	U3CtmpScrollU3Ed__121_System_IDisposable_Dispose_m058B5042FFD6D072E890FD42FDC4685BF9D90978,
	U3CtmpScrollU3Ed__121_MoveNext_mA4FCA7D434AE4CA87A5DCCC7024D046230359304,
	U3CtmpScrollU3Ed__121_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14121AE1F25F2547E709F627B873F6732220D5AA,
	U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_Reset_m8553DC27B6409578E06916683E3CEC4170FDAAB0,
	U3CtmpScrollU3Ed__121_System_Collections_IEnumerator_get_Current_m1CCB279ED7CB5FD1F214F85541F1BE979D762957,
	U3CBeginGarmentCallU3Ed__122__ctor_mF1D34C8ED14D2838ACB9179BFF9B8E25060E347E,
	U3CBeginGarmentCallU3Ed__122_System_IDisposable_Dispose_mC2A7529A5F4F8E37EF2543B0160108DAB4BE5EED,
	U3CBeginGarmentCallU3Ed__122_MoveNext_mE2E2CBD84E4257A5F907E93274DD9149C1780346,
	U3CBeginGarmentCallU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC18C03DECED7724F0A8C879A81A799DE616B8884,
	U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_Reset_m33B50615A9C70037B8F1BFAB67DAB62019C6611C,
	U3CBeginGarmentCallU3Ed__122_System_Collections_IEnumerator_get_Current_mD4DB387972B3BB6B64DE01FDBF515D72DDFCD398,
	U3CImageTexHandlerU3Ed__125__ctor_m4DC549290A14DEE1FD6853C90B3D95424FE0BA2A,
	U3CImageTexHandlerU3Ed__125_System_IDisposable_Dispose_mB1A2DA2D622318EF7C7882E054DA421CE22421EC,
	U3CImageTexHandlerU3Ed__125_MoveNext_mE79793F16545C99A58F5E21E1A692A9152904E48,
	U3CImageTexHandlerU3Ed__125_U3CU3Em__Finally1_m83D596986D462FA9C5450C18E0D5B41532773466,
	U3CImageTexHandlerU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54E7FF932D9D77128ED4C58634C42ADBF5010C96,
	U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_Reset_mCF9361949B92FA15B21927BD9391E205FBC8A0F6,
	U3CImageTexHandlerU3Ed__125_System_Collections_IEnumerator_get_Current_m4FE98BC68F8422625EC9F9C5B37277F30787E340,
	U3CImageAddingProcessU3Ed__126__ctor_m8FA7206E28AE6338E7F743FD336B4F1EA75A6124,
	U3CImageAddingProcessU3Ed__126_System_IDisposable_Dispose_m49B8A7C618AE71A995D3411AC554ACD5F2232700,
	U3CImageAddingProcessU3Ed__126_MoveNext_m125B68955309B84358A3FFB4A0B5ED4DDF98EC75,
	U3CImageAddingProcessU3Ed__126_U3CU3Em__Finally1_mA9C9EEF03D1989C147C2B1FB2113BB412F86F093,
	U3CImageAddingProcessU3Ed__126_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC465F56FE788453CE681703A3E164BF809528187,
	U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_Reset_m0377E1B1D0E57425A4B765F434124AA99B952FF8,
	U3CImageAddingProcessU3Ed__126_System_Collections_IEnumerator_get_Current_m8444CD45133F500A05CCD3393B8594E45FBB4241,
	U3CU3Ec__DisplayClass128_0__ctor_mEE8BCEB6FC5CB5B7C3E8D75ABA3DCA5722190F9B,
	U3CU3Ec__DisplayClass128_0_U3CFbaseQueriesU3Eb__0_m0E40C5FB38920F733F5D0AA17DF1B19148ECBD9C,
	U3CU3Ec__DisplayClass128_0_U3CFbaseQueriesU3Eb__1_mE27DC9579501583B41963D59B5A49E077845DEFD,
	U3CFbaseQueriesU3Ed__128__ctor_m25DB5088A39499E23E6A2A1196C7B42564F860F3,
	U3CFbaseQueriesU3Ed__128_System_IDisposable_Dispose_m6D6B952EC69EAE27B82D5C14AE57A6B4B87A01B5,
	U3CFbaseQueriesU3Ed__128_MoveNext_m9EA4677B133AE5DA0CEDAC31152FDBD1AB6D0AD6,
	U3CFbaseQueriesU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7808CAD7B133848AAF612E1D96BF5F2E0E492AF9,
	U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_Reset_m78FBE24F99396A51AB662A48447758AB67DE39A6,
	U3CFbaseQueriesU3Ed__128_System_Collections_IEnumerator_get_Current_mA5F4F972647F1DC794480D6E4908D4F4550E20F7,
	U3CToProductU3Ed__134__ctor_m9900F913A83872687C721DCB5D9B714B700A1079,
	U3CToProductU3Ed__134_System_IDisposable_Dispose_m397680E3FE53C7724B8685B74462B56C920316F3,
	U3CToProductU3Ed__134_MoveNext_mCB9E22E732BC00D543B7DFF6E510F4087AA06526,
	U3CToProductU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m246861321A3C383AABF3B464F205A15A62B12B00,
	U3CToProductU3Ed__134_System_Collections_IEnumerator_Reset_mBB707E3B20E4A99F1327035DA657A128F51AB0CF,
	U3CToProductU3Ed__134_System_Collections_IEnumerator_get_Current_mA0865F6EACB8D266C253B56FFFF15B13AD085323,
	U3CU3Ec__DisplayClass135_0__ctor_m7A11B18C3DEE1E41289EB635D87F47EC67F3E2D5,
	U3CU3Ec__DisplayClass135_0_U3CInitItemU3Eb__0_mB4957E07DFDC12C7299A72F209A06CB4FE2B6692,
	U3CU3Ec__DisplayClass135_0_U3CInitItemU3Eb__1_m448D902C83B9071956EEEBAE636C66DCBD5DFDBD,
	U3CU3Ec__DisplayClass135_0_U3CInitItemU3Eb__2_mE0C7A6459A1661A0768A40CF27C5B8E73C9133AA,
	U3CU3Ec__DisplayClass136_0__ctor_m6C6649C0EE977BC5F35B1E1BEBF1A85C08E86607,
	U3CU3Ec__DisplayClass136_0_U3CImportImageU3Eb__0_mEAEFB4E0782953DA9861D5C4F0829E5FA8357D6D,
	U3CU3Ec__DisplayClass136_0_U3CImportImageU3Eb__1_m8AC02C432ECD8A736777DF3E14890CEFAA0C5FD7,
	U3CImportImageU3Ed__136__ctor_m18CB1E44865D2185DC631E553BBE9CB761AD5195,
	U3CImportImageU3Ed__136_System_IDisposable_Dispose_m9BBAB55890CF9448000562B794994367EAFD12E8,
	U3CImportImageU3Ed__136_MoveNext_m35708D5974A45371818D53323B63F1FDA0A1E600,
	U3CImportImageU3Ed__136_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m43399C0073CD2064ABA95AC6724AA31CCDA971D2,
	U3CImportImageU3Ed__136_System_Collections_IEnumerator_Reset_m8B0A961330D62896C46FA5268C1506A17F5F32CD,
	U3CImportImageU3Ed__136_System_Collections_IEnumerator_get_Current_mDAD3A362FA8B96F11901C0389A2E0F1AE02C5D26,
	U3CtmpU3Ed__137__ctor_m64A892AAFE35BB0DB81EBC891907BAD699F14A11,
	U3CtmpU3Ed__137_System_IDisposable_Dispose_m038DB0E832F38E7D0A7438FBFD21621EE4D66A9F,
	U3CtmpU3Ed__137_MoveNext_m575DE591C2B0E4D9109362A4BD00CD5F90BD1F58,
	U3CtmpU3Ed__137_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FECF7697F97244EE9AC8637AA540E38264CF7FF,
	U3CtmpU3Ed__137_System_Collections_IEnumerator_Reset_m8771E3D830378C9D1BD0A73DB43CA451AA8718CD,
	U3CtmpU3Ed__137_System_Collections_IEnumerator_get_Current_m7FC964576C9960A08657AB667982AC5291A27D51,
	AddItemFav_Start_m22EB63A4BB574A0F65F624BB93C171FBAC5C0C65,
	AddItemFav_Update_m8FAB638DFB4FCD4B633D22F644552AB3049A2534,
	AddItemFav_Awake_m7A464B617259F8C2A4A65959323B0CA4FAC3EA54,
	AddItemFav_Initpop_m7D6D7D6524CBD5A289DFCEBAE31E8E2E3EC91062,
	AddItemFav_MakeItem_m5DC8CC01221D1884BF524D7604B68BF667E3223B,
	AddItemFav_InitItem_mC97C379AA61BE42FC3C5624675C5B870839350A1,
	AddItemFav__ctor_m72EE683B90BCF8352BF8308293E98F558FCDBDCC,
	U3CU3Ec__DisplayClass19_0__ctor_m8140DC239D6578274C15E3B32239EE9B50B58626,
	U3CU3Ec__DisplayClass19_0_U3CInitItemU3Eb__0_m1EC3CF5C1F017591611B1691ECA7EE5D262B6D8C,
	U3CU3Ec__DisplayClass19_0_U3CInitItemU3Eb__1_m19276384489C4DD6C4752C783469686CF1F1CBFC,
	AddItemFavB_Start_m30BA9031B6C52DAA3F976BA8C1F0F498A10D98B9,
	AddItemFavB_SetPrefs_mB24E9E1899BEF44D6C33A153F63B240862831A89,
	AddItemFavB_ButNextRoutine_mC8332E00005A39E745D65C9ABCE09820CDED8AF2,
	AddItemFavB_DelItemListWait_m34607DECD00D5A8203DD0C9B94A136F0D8C07A43,
	AddItemFavB_ButHomeRoutine_mBDA4FBC56EFB11901F98BF56D39FD62DF1BF5DB9,
	AddItemFavB_ManagerReset_m518ADAC3030F842B494901104A78D75FEEC6E062,
	AddItemFavB_ButSignOutRoutine_mDB66FA14184594255315C663B57AEDC770551A90,
	AddItemFavB_ButPrevRoutine_m179C7214D4B09084473108EF4B89F611FFC104B8,
	AddItemFavB_IsFinishedDownloading_m06E39FC6BFB1E80640132961728031376F4E8D4B,
	AddItemFavB_FilSrcRoutine_m7F3CFD187CE0378C12B5016ED5E472362C740472,
	AddItemFavB_FilUpdRoutine_mAB5BA98A6452BA0212A652CFFC4BFB301782F528,
	AddItemFavB_checkButtons_m050B20A2818F1A9D7C56A4C2E02F0586D00270FF,
	AddItemFavB_OnDisable_m5D0489A9423F098B5D20CAE83F544BC12AEBE17D,
	AddItemFavB_StopDownloading_m3811835D137C6DF0D3E289687A00193434FF42ED,
	AddItemFavB_Update_mAE6E83DA4BA2CBE328BA448DDD7C9C17F4B8DA8C,
	AddItemFavB_ResendRepeat_m33ACCF6A21E700039301FDCB1F057F8DC0E646B3,
	AddItemFavB_ResetMan_m77535E95658CFD87308B1229EB656F72C8F507F3,
	AddItemFavB_scrollrectCallBack_mC113B0C6941FD75B3FDB7325E33D36B172F4CFB4,
	AddItemFavB_tmpScroll_m7C77D6633C3C6234DCEA3B50B9ADAB6145D6D743,
	AddItemFavB_BeginGarmentCall_mFD0B654FDB658DCFA40C758EADA73B06943ADF53,
	AddItemFavB_showGarms_mE0E7654AEF7B4250CEDD1157ABB6A3AE8716FB13,
	AddItemFavB_AddImages_mB021CB8F15D14E6263F31924E8457A15910962C1,
	AddItemFavB_ToggleButtons_m7E924CF0F1FC1782D62FCF4794FEFC27292D6858,
	AddItemFavB_ImageTexHandler_m6CC6A14F463CAA419779D706DCE0EF37BAA578DF,
	AddItemFavB_ImageAddingProcess_m15B9C3BB4D6B081000AB2D595049CA71D5B32FFB,
	AddItemFavB_ScaleTexture_mD3CAB23F54712E100F862E0FB36031E975029054,
	AddItemFavB_FbaseQueries_m4D8C6B4A237936B4ED4475DC22E360BB5364D55F,
	AddItemFavB_LayoutFix_m2E7B286217AE30C7881433ED82ABCA883914AD8F,
	AddItemFavB_ToggleLoading_m4781BB2B35354DE33C8EF0848B10CB5500D57F5E,
	AddItemFavB_ToProduct_mA72463E586F425D9C1E93F6C8FAAEF78CC08D407,
	AddItemFavB_MakeGarms_m20D2E830759AEEE45756DA478500F246CAB55334,
	AddItemFavB_AddGarms_mE48544A93022326A75B151244AA0E4DFDB96A97F,
	AddItemFavB_UpdateGarms_m97B0E574EA1F8173BF7B1E5992FB3D19AF2BC8E0,
	AddItemFavB_DelItemList_mBA5C925C6F8C778173B03A582DE3350FFC965C04,
	AddItemFavB_InitItem_mBEE11E74364AB4014861A630B6D21D63AE0FDE50,
	AddItemFavB_ImportImage_mD2F4F737D7C782E165170EED54985C283FF55D13,
	AddItemFavB_tmp_m7751B13DC3FCE7A89EE6396F031D7585480E7509,
	AddItemFavB_MakeItem_mAB1CAE7FED229C3F01A0D7F18ABE4C048311CB82,
	AddItemFavB_clearManager_mEE1E3A7A10502A7298977E10FD8506C66464C026,
	AddItemFavB_filterlist_mACA526106C7784AFFDB5ECE71A171E9BBE1EC73A,
	AddItemFavB_rectcalc_mC02D85956B8D0AA82C6408244D4F469585EAA53B,
	AddItemFavB__ctor_m5FF4671857685C3AB58859505C9FAFC9FEF92C2D,
	AddItemFavB__cctor_m301EFC4D073F41B37BE3328B72E210069BC9792F,
	AddItemFavB_U3CStartU3Eb__99_0_mA8D5B273FC5BA5D94E2BF1ED4497CE6A3DAE824D,
	AddItemFavB_U3CStartU3Eb__99_1_m3EF20EAD20C93BB7BE66740C163360D43939729B,
	AddItemFavB_U3CStartU3Eb__99_2_mC9A25F8B804079C5FFB41CD8B5870F3AAAAD74DD,
	AddItemFavB_U3CStartU3Eb__99_3_mE3A548D498C7971A9E09A67EB2D3A53490C1A218,
	AddItemFavB_U3CStartU3Eb__99_4_mE059827DCAFF42E96F366E0D5A6ABE6DE6218C4E,
	AddItemFavB_U3CStartU3Eb__99_5_mCCE5DFBA80917F86A0D33F8F95A90EE68FBD7B8C,
	AddItemFavB_U3CStartU3Eb__99_6_m74B31F18DD846810AA503A5E18DFC3338A1EA903,
	AddItemFavB_U3CStartU3Eb__99_7_mEF9860D29D5CE0D20BEA43B5CD887B4C52EFE442,
	AddItemFavB_U3CStartU3Eb__99_8_m79681213EFBBA835E69B65411256690670A91739,
	AddItemFavB_U3CStartU3Eb__99_9_m555BE782F45EBF179B9BABE54B16492B6CEB8812,
	AddItemFavB_U3CButNextRoutineU3Eb__101_0_mF0329110041A2D13DD902A94C366BF88A5BB1153,
	AddItemFavB_U3CButHomeRoutineU3Eb__103_0_m3DD2F91A3E0847798137288B2BD5200AD3743A73,
	AddItemFavB_U3CButSignOutRoutineU3Eb__105_0_mBAC844154801247AF08098C35F5FF884679820A7,
	AddItemFavB_U3CButPrevRoutineU3Eb__106_0_m9C4A5DA6DE7430FFD2CA7B92EF9FF19A2390BCD3,
	AddItemFavB_U3CFilSrcRoutineU3Eb__108_0_mA644BDEF06923340BC7780C8B217A4A256DDB23A,
	AddItemFavB_U3CFilUpdRoutineU3Eb__109_0_m49BB9EC280F925B74A31AA25ECC4574E69B5F686,
	AddItemFavB_U3CStopDownloadingU3Eb__112_0_mB21FEE923B57F6D9C1FFA163A7A72EACE957ECD2,
	AddItemFavB_U3CtmpScrollU3Eb__117_0_m40BA1F8A9DBBFF062BFA53257DC02DFE9CD623EB,
	AddItemFavB_U3CImageAddingProcessU3Eb__123_0_mFF88D401B07CC1CDC0FD84D028C5E4A0AD0EB31A,
	AddItemFavB_U3CToProductU3Eb__128_0_m12AB57D8327CC94A4ACF2D61557E599DAC507D78,
	OnReadyToReceive__ctor_m5BA976264F676FA37C832C0B713BD32E072450BB,
	OnReadyToReceive_Invoke_m90F135426AF33FC5EED6A904D820EF832F23EAF2,
	OnReadyToReceive_BeginInvoke_mA687EEAF7934ACFBD772E83CC7F172530FE1AA84,
	OnReadyToReceive_EndInvoke_m8F590E3B448742A7B303D44017DD7CFBA862A184,
	OnReadyToReceiveBatch__ctor_m6B3F983EB4FD08E5D9186A9585FE6FCDFED7E576,
	OnReadyToReceiveBatch_Invoke_m9CF9203113AA748372561176107D1359DAEBFE09,
	OnReadyToReceiveBatch_BeginInvoke_mAA642CB130478721475B2B8DFC260CB509F5F231,
	OnReadyToReceiveBatch_EndInvoke_m963FA6AD9BC0E1E8E4B9827FEAD11400DF271424,
	OnReadyToReceiveFav__ctor_mF5DF7BE191A2F4B9451957580AB190B6E9EAD4B7,
	OnReadyToReceiveFav_Invoke_m32672FE23775F12D8B53314CF9A3F003A7473916,
	OnReadyToReceiveFav_BeginInvoke_m8BC7E46D0031197EC55CFF49C47DC0940F5FAD7B,
	OnReadyToReceiveFav_EndInvoke_mEF014CCCAECF737CAB55FE1A9DC7E56468A678DD,
	OnCloseNav__ctor_mCA5E5AA84D53AC37CC5851F51DEE4451A2529C0A,
	OnCloseNav_Invoke_m9C60BDB40A53E56EABB5EA7E6F6D5ED5D8BC5EFA,
	OnCloseNav_BeginInvoke_m8882779DBA8E58C3ADAB2D3EE8E9D788C67F7C5B,
	OnCloseNav_EndInvoke_m84892566A5E732C0DEC94251A753F5B11261EC51,
	OnCloseFil__ctor_mDC82FB5AD1A8D0AF64875DADE4F5557266634C08,
	OnCloseFil_Invoke_m861225219A4A40363D3A3C6FA2A801626E9A3C98,
	OnCloseFil_BeginInvoke_mACF38DAADA380B37FF8D3CD830AAE5AD0C0CB938,
	OnCloseFil_EndInvoke_m2A4D71A9AF5F5C3477DAA504AE8E2E713A46B21D,
	U3CSetPrefsU3Ed__100__ctor_m76E7D60293C830A2EB67DE51B7781AE61C4BE76B,
	U3CSetPrefsU3Ed__100_System_IDisposable_Dispose_m7A07429A4ACA08DAF578053504AAAEDD40BE443E,
	U3CSetPrefsU3Ed__100_MoveNext_m77E2F4A8E1593B420D3F219C4583032C8A611740,
	U3CSetPrefsU3Ed__100_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC09ECBE0E3957BD7BE662F0A0D8F1423E702C59D,
	U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_Reset_mBF33BE7CA5F9C5F1DA7EF54857F51C9E81D9ABEF,
	U3CSetPrefsU3Ed__100_System_Collections_IEnumerator_get_Current_m7891B9920CFC41570FFE0F5564EFA91AF3E18D7E,
	U3CButNextRoutineU3Ed__101__ctor_m770F9523CADF1BEF7FF7C16013118EF432B0C662,
	U3CButNextRoutineU3Ed__101_System_IDisposable_Dispose_m8A144B4DA6125320A42AF0152CB56D7085711F64,
	U3CButNextRoutineU3Ed__101_MoveNext_m341587A342195F33ECC1803E9343ED29911C21A6,
	U3CButNextRoutineU3Ed__101_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE566BDB6564C589FD9A492BDAF211E8BE551926A,
	U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_Reset_m0ECC8CDBA0AB2C92963F60C9C5ACF1D68091BCF3,
	U3CButNextRoutineU3Ed__101_System_Collections_IEnumerator_get_Current_m88FF3464C964E918C4D1B4563158F2E2742BF34B,
	U3CDelItemListWaitU3Ed__102__ctor_mE4FD584463368E858E6500444E13560D39A8E7E4,
	U3CDelItemListWaitU3Ed__102_System_IDisposable_Dispose_m84599E19DEB6523032C1B366CE872B1CAD3F029D,
	U3CDelItemListWaitU3Ed__102_MoveNext_m1AA21BCB909060ED7D3F10A742908DFF9FBC8984,
	U3CDelItemListWaitU3Ed__102_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2E1971853F5E1897DE572CFE2A1F5CE93DCC2E11,
	U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_Reset_m9AC6364FB08D83A09BA4918A401DADF38A842113,
	U3CDelItemListWaitU3Ed__102_System_Collections_IEnumerator_get_Current_m9A7B9240C6F6E8EFA321E3470395DD66C24C5F9D,
	U3CButHomeRoutineU3Ed__103__ctor_m8833F61EF7E0A7572E454A323DF97D20BA2588B1,
	U3CButHomeRoutineU3Ed__103_System_IDisposable_Dispose_m7EEEB03EB525F93260C6A019B8A907A9FEBB3F8F,
	U3CButHomeRoutineU3Ed__103_MoveNext_mFCFE8A14A87214506B84399E3DD0189891AA4727,
	U3CButHomeRoutineU3Ed__103_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1C82185097BE3FEAB452F9C1ABD34D4DCBFF679C,
	U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_Reset_mE8665766A9A6D613DAE425A1054D00FE793FA5EC,
	U3CButHomeRoutineU3Ed__103_System_Collections_IEnumerator_get_Current_mD32884E4815C7AEA078E5202A3399B8999BB570E,
	U3CButSignOutRoutineU3Ed__105__ctor_m007F2C5053AA977BE6D4BA0F4B26EF8168F62BCF,
	U3CButSignOutRoutineU3Ed__105_System_IDisposable_Dispose_mCDB3459813B11DBED7B42C011506ACB7F3FA7FDD,
	U3CButSignOutRoutineU3Ed__105_MoveNext_m274820FDF5CFE17304F46A2B3ACDA8B6A698D630,
	U3CButSignOutRoutineU3Ed__105_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDC0D8BF7401FD148B2B40B4D9BF77090FDEE7406,
	U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_Reset_m1BEDE53990B83198CB62F9F2246E3AB05D7286F8,
	U3CButSignOutRoutineU3Ed__105_System_Collections_IEnumerator_get_Current_m4768AB7E02D5CD67EEF47DCFE270C258F0454E7B,
	U3CButPrevRoutineU3Ed__106__ctor_mD5DB2A5A9B55B135C05747812341B7DEF3C10503,
	U3CButPrevRoutineU3Ed__106_System_IDisposable_Dispose_m41E2986903131B9738B1D226DE03735A5E5A8393,
	U3CButPrevRoutineU3Ed__106_MoveNext_m247438388020CEDF9185F8B4940F7570C01AC05F,
	U3CButPrevRoutineU3Ed__106_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m677A17883231084C84F5D047340EEBA04A75E6CD,
	U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_Reset_mF8889B67CDD20BE3EC695C1369D51E699AF71F59,
	U3CButPrevRoutineU3Ed__106_System_Collections_IEnumerator_get_Current_m6A78883AB5236917A0C8EEECDB55B0650B83138F,
	U3CFilSrcRoutineU3Ed__108__ctor_m4A95C6E500D70B86FD6E61E285BA194978AACC8C,
	U3CFilSrcRoutineU3Ed__108_System_IDisposable_Dispose_m02E3089371909C6F27EE212E7AEFC5AD54F45A63,
	U3CFilSrcRoutineU3Ed__108_MoveNext_m0880302B0761556C023C4B432F0574F69B3BD192,
	U3CFilSrcRoutineU3Ed__108_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF2BEEC524BF85436365222AE9DCE459A093759C,
	U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_Reset_m1C0E3E34FE3BFF6CE43A3C4711E7498BCB8026C4,
	U3CFilSrcRoutineU3Ed__108_System_Collections_IEnumerator_get_Current_m6C97BB788CFF7957C613C778C2B2224FEB864D37,
	U3CU3Ec__DisplayClass109_0__ctor_m126D2FBAB6967FAF17D28289325A15D4D9AAF168,
	U3CU3Ec__DisplayClass109_0_U3CFilUpdRoutineU3Eb__2_m3E356654F5F2FC6E596A6A43298AB39192116E2C,
	U3CU3Ec__cctor_m8FE969DD5E66643884520973EBC958190F0DD83E,
	U3CU3Ec__ctor_m8013E921369BD9D2C9918D14FC091419CA571136,
	U3CU3Ec_U3CFilUpdRoutineU3Eb__109_1_m5C235F8BC6171A3E85E3AFE899B89BDDABF9B169,
	U3CU3Ec_U3CInitItemU3Eb__133_2_mD29ABCE4171D0F18D7C10242167AEA4E680BC84A,
	U3CFilUpdRoutineU3Ed__109__ctor_m996A8F53737D39937922BAD80D230D2CDA6C1D9E,
	U3CFilUpdRoutineU3Ed__109_System_IDisposable_Dispose_m076118330E20CBC1EB2648A81B6B1C1E97160BB6,
	U3CFilUpdRoutineU3Ed__109_MoveNext_m16D414225DF8EFB45108BFF12B2757C6F99906AD,
	U3CFilUpdRoutineU3Ed__109_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m851CADA05D65739DDA283DC975A580F7DDD0CA2D,
	U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_Reset_m4E5E5700E9A4F96411B9DD5C3F3E0C1D318277B1,
	U3CFilUpdRoutineU3Ed__109_System_Collections_IEnumerator_get_Current_mAB074BD2F4B1C09BDD6D48EFFC3AF7E73D1655A5,
	U3CStopDownloadingU3Ed__112__ctor_mF6C2EC9ACE60A4839D8D46C1C43BC6281C03AEF8,
	U3CStopDownloadingU3Ed__112_System_IDisposable_Dispose_m0917D6FA891909094BE58CCE929228AB202B62FD,
	U3CStopDownloadingU3Ed__112_MoveNext_m290FD4A7C0D0E1A5B257A3FA63EAEA6153E8734B,
	U3CStopDownloadingU3Ed__112_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5DD6FFCB5D8649CB1E542FD833A680725C891CD6,
	U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_Reset_mA01D081F3449DBD67B05E87BB4B63E3333994878,
	U3CStopDownloadingU3Ed__112_System_Collections_IEnumerator_get_Current_m6434132FC738B3A11CFAACD960F507329B563756,
	U3CResendRepeatU3Ed__114__ctor_m3F5BFEDD90E83C5794F3D831B106A8B58CDC04B8,
	U3CResendRepeatU3Ed__114_System_IDisposable_Dispose_mC1A4224C9CD3D4C57EA721AF4825AE14184C37B9,
	U3CResendRepeatU3Ed__114_MoveNext_mC59492B45F4F6D622F755A54067DEEA42483C77A,
	U3CResendRepeatU3Ed__114_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m719B58A1C0991C1BA3B02879D4A1A20254AAB6C9,
	U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_Reset_m5FCD37DD1EE430EDECE9ED6DAB1B6A56E04C2392,
	U3CResendRepeatU3Ed__114_System_Collections_IEnumerator_get_Current_mA40A27CD9848559171D6093EFB1937A3DCE29D7A,
	U3CtmpScrollU3Ed__117__ctor_m7C268731D5F8756B86633FC062CCC952B6740FE5,
	U3CtmpScrollU3Ed__117_System_IDisposable_Dispose_m831B53BB37B2E9240405FA9BE9F1076F02087F06,
	U3CtmpScrollU3Ed__117_MoveNext_m228C64A953EF790656960D283398B6697E28244F,
	U3CtmpScrollU3Ed__117_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m224B1BD9D4CD4FB193F8A17EC08A02C6E8ECE348,
	U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_Reset_mD408582C33E6F0343EA87EC85DC6FD882F3B218A,
	U3CtmpScrollU3Ed__117_System_Collections_IEnumerator_get_Current_m7601D10039050BAF1A5EC3F95CF7F794E28876C9,
	U3CBeginGarmentCallU3Ed__118__ctor_mFF9ECD2897932FB44B8FDED5ED73D79BD14E8E8D,
	U3CBeginGarmentCallU3Ed__118_System_IDisposable_Dispose_m4F58A0CDBC68DEF08EE185191E23BE180CD4AD34,
	U3CBeginGarmentCallU3Ed__118_MoveNext_m0D9996738D698B22F8B4CDCA1BEE7993AA234E32,
	U3CBeginGarmentCallU3Ed__118_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27291F3DF06BA60F244574ABBB6F46214B0DA02F,
	U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_Reset_m195F2702399FBC0ADC51B2205A1029553D4E87ED,
	U3CBeginGarmentCallU3Ed__118_System_Collections_IEnumerator_get_Current_mFCDE05B088B0E75E0D13F80DFA5BACD20AF5FF74,
	U3CImageTexHandlerU3Ed__122__ctor_mF7419C5E9365A97FB7C1DA3B0424FDDD07887E25,
	U3CImageTexHandlerU3Ed__122_System_IDisposable_Dispose_m9AA69120AE92EE8525CD5258A84ECCB851F6A777,
	U3CImageTexHandlerU3Ed__122_MoveNext_m9E40E4D360125DCD7FB232BE4C4CA9F37A02C024,
	U3CImageTexHandlerU3Ed__122_U3CU3Em__Finally1_mB71D86C4AC6620BF6537AE82090643CEA2A9AC31,
	U3CImageTexHandlerU3Ed__122_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m91B0112E2D4FBDA7C9BB2CE5BB6011D358ED2A80,
	U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_Reset_m5BF7D635AFBE4EB47303002CB2C6AF54DB25E364,
	U3CImageTexHandlerU3Ed__122_System_Collections_IEnumerator_get_Current_mA1A91EE97DA546664D9806E5363E4A5D640B1F87,
	U3CImageAddingProcessU3Ed__123__ctor_mD95084F0EADFB19F6539589548840D6AD193B303,
	U3CImageAddingProcessU3Ed__123_System_IDisposable_Dispose_m575C4A8E4B54ABC855E1869356DBE5701BBCFC6B,
	U3CImageAddingProcessU3Ed__123_MoveNext_m3289B02398939735D81DB6A4560AF79314D939DB,
	U3CImageAddingProcessU3Ed__123_U3CU3Em__Finally1_m63622B0D3CDA33D0334A4B188FC72E65F21C2A6C,
	U3CImageAddingProcessU3Ed__123_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8059792712242D80BA76F0E9D00EC3DE621E69E1,
	U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_Reset_m13A4AEA4FB51F19C57232D20FE71D9D0E95F965D,
	U3CImageAddingProcessU3Ed__123_System_Collections_IEnumerator_get_Current_mB06D6DA393684E65C8B457E41A43D251E15063C5,
	U3CU3Ec__DisplayClass125_0__ctor_m9E145E69AC81B1E08A8B1FB9D0C79EE192BA705F,
	U3CU3Ec__DisplayClass125_0_U3CFbaseQueriesU3Eb__0_m0B528E761873CBBDCB12BE7EB5C69BCB1E43A144,
	U3CU3Ec__DisplayClass125_0_U3CFbaseQueriesU3Eb__1_m1054EF8816FAECBEC57675EF1DC2E4C94DBC176B,
	U3CFbaseQueriesU3Ed__125__ctor_m0021C3BB0BEFE90B6EC45561DF67D8B90CC2F05D,
	U3CFbaseQueriesU3Ed__125_System_IDisposable_Dispose_m16D84B535CF2EE6EBC48C2BEE44FACBB6172BD57,
	U3CFbaseQueriesU3Ed__125_MoveNext_m19C45FBD2C092D6BC734CB3777AF7DDE0B8185B0,
	U3CFbaseQueriesU3Ed__125_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m482847F1098B4057D8AE595F730B5DB8CBD9CA56,
	U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_Reset_m5A083EF573E4B42EF6EABF3A0F000DB6A9D1970C,
	U3CFbaseQueriesU3Ed__125_System_Collections_IEnumerator_get_Current_mA7E00797E8DF86728C49DC78C62E20956683FAD6,
	U3CToProductU3Ed__128__ctor_m6A9A821B8BABFB4F8CE697657BB2FA1225B4082E,
	U3CToProductU3Ed__128_System_IDisposable_Dispose_mAA22CFB62E9C7491744FC997FF1567091C6A754A,
	U3CToProductU3Ed__128_MoveNext_m5FE41A55947388389BF818B677EFFF5AE0310CF0,
	U3CToProductU3Ed__128_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2014580B242F8751BBB63797DD261AC125C09DC,
	U3CToProductU3Ed__128_System_Collections_IEnumerator_Reset_m3C019D7B395DA65959C6C26F5CDD692641DC1156,
	U3CToProductU3Ed__128_System_Collections_IEnumerator_get_Current_m866CBF81E26395A49826034FB021CE050BDCCF03,
	U3CU3Ec__DisplayClass133_0__ctor_m91FE31B5AD72669CAF5C7A22859E7CC11F145FE7,
	U3CU3Ec__DisplayClass133_0_U3CInitItemU3Eb__0_m489DA734BD2163B21D4FF135838CC1698395A9CB,
	U3CU3Ec__DisplayClass133_0_U3CInitItemU3Eb__1_m554FC57D514FD3E17EA02ABDE60ECF78D7B9A31A,
	U3CU3Ec__DisplayClass134_0__ctor_m8790328CD4DFF3C1F8520503D4261E1C6E7FA64D,
	U3CU3Ec__DisplayClass134_0_U3CImportImageU3Eb__0_mFF6E310A5A142A5EA23C1C9B2531D66D200465AC,
	U3CU3Ec__DisplayClass134_0_U3CImportImageU3Eb__1_mDDE969D220BD447748AE16C11405C4BF2B7C868A,
	U3CImportImageU3Ed__134__ctor_mE4C6E2BC19C6F1318CB4F0BAB54870E62A778064,
	U3CImportImageU3Ed__134_System_IDisposable_Dispose_m799A659DD685A41779A1E5664864393915A0AF8C,
	U3CImportImageU3Ed__134_MoveNext_m213D02F07B8B79C81B23D28B4F87CEDE2CA3DBA2,
	U3CImportImageU3Ed__134_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96D8D603CFA9A391F5A246114AEFEEDF6664C9F8,
	U3CImportImageU3Ed__134_System_Collections_IEnumerator_Reset_m0005C3A33E2057DF0A8C59A47855A44B385D0865,
	U3CImportImageU3Ed__134_System_Collections_IEnumerator_get_Current_m2B7726DF26F380FDA790258265D8E5E2831759D8,
	U3CtmpU3Ed__135__ctor_mE8EA412BD2B6E55EA20FCA793A6446F4CF18BAE9,
	U3CtmpU3Ed__135_System_IDisposable_Dispose_m800554F20A5F13ABE7D22BAE6F37B0FF286CAF32,
	U3CtmpU3Ed__135_MoveNext_mEF8D965784FA3FA3821C0DA0F5826649411432FB,
	U3CtmpU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9363254BF1666CFC6D4530829C09BC31CF242E00,
	U3CtmpU3Ed__135_System_Collections_IEnumerator_Reset_m4045FFCC29548CDAAD61AF52B90AEB91B3090987,
	U3CtmpU3Ed__135_System_Collections_IEnumerator_get_Current_mFE3562A87D8848233B9A20676B6178F80562E0A9,
	AddItemSingle_Start_m7C29FE4ABD449EBF98774BB07E6FC15392BD6A01,
	AddItemSingle_ButHomeRoutine_m3A610393D49D4219306D680960B890A93A91EB14,
	AddItemSingle_ButSignOutRoutine_m512500DFBFFC99B2ECDA6B0E017427098D8440F3,
	AddItemSingle_CleanTex_m8F1CD069B973B2F305B451EBC3092A3272A2FE1A,
	AddItemSingle_OnDisable_mA1413855702E0311F9822AFA851E75BE7D798FF6,
	AddItemSingle_ImageAddingProcess_mE0C64BBDA23A454F59F6386BCD424777079E19AE,
	AddItemSingle_InitPoPProduct_mFEB56EAE900D84C21517CD738EE7F9E437A4EC2B,
	AddItemSingle_Update_m9E4EDCA3CDC3092CEF03559ABCCC1D4DDE70D779,
	AddItemSingle__ctor_m6C7D70C1BB1A56C51791AA8C081CA30BAC2B6D73,
	AddItemSingle_U3CStartU3Eb__27_0_m320F1D837FDB0993585F54FBEE60486E106C47E4,
	AddItemSingle_U3CStartU3Eb__27_1_m58A40C02BF1EDD867CECF3998ADB67E23739EFD5,
	AddItemSingle_U3CButHomeRoutineU3Eb__28_0_m0421464A105FB4632D590F221E4A092B9EFABA41,
	AddItemSingle_U3CButSignOutRoutineU3Eb__29_0_mECADDAC0D393D555328D4CEB84A4AA54BA90FB2B,
	AddItemSingle_U3CImageAddingProcessU3Eb__32_0_mE5C01CEF59A35D36831ACDE62AB012B49058645C,
	AddItemSingle_U3CImageAddingProcessU3Eb__32_1_m6FBB2D3F4AF2D56D3F48FD456C2FF6E4D23A0150,
	AddItemSingle_U3CImageAddingProcessU3Eb__32_2_m773DFB3E50C1620401ACB394E2E213DE9E0F4E23,
	U3CButHomeRoutineU3Ed__28__ctor_mB4504AB9D5F362706B1B45DF56DE52D86107543F,
	U3CButHomeRoutineU3Ed__28_System_IDisposable_Dispose_mF7FA8E483FB630014A420E312019C99A4C91630A,
	U3CButHomeRoutineU3Ed__28_MoveNext_m76E41C5A986184F13E2D2B09FB6681D32332C335,
	U3CButHomeRoutineU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CB4964F1334604A1DE3B425844529B9333DA1F8,
	U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_Reset_m0A2FE0CD2B6E84F803236ADE6B38B5E67E80858D,
	U3CButHomeRoutineU3Ed__28_System_Collections_IEnumerator_get_Current_mFE4C850F5F097C4BDE565B8F00258284878BBADB,
	U3CButSignOutRoutineU3Ed__29__ctor_mE815D9176BA4FE8E1196967DE1D6D0E66D3AB8ED,
	U3CButSignOutRoutineU3Ed__29_System_IDisposable_Dispose_mD297951099D5E22898EB1BF799A729585C7122E8,
	U3CButSignOutRoutineU3Ed__29_MoveNext_mFB181DC460EA2706C60FD33E9C2237AF1C32E930,
	U3CButSignOutRoutineU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB6552AC06A51E5E351C52D5EA3F576AC9DF591A,
	U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_Reset_m9AAD08A86CC8BF57733370B1172044DAA919B197,
	U3CButSignOutRoutineU3Ed__29_System_Collections_IEnumerator_get_Current_m3F0632BD67CBA8E8A98ECCAD924769029E3994C8,
	U3CCleanTexU3Ed__30__ctor_m0519A26FB715C19DB3B1BC92370C8546FFC96ADC,
	U3CCleanTexU3Ed__30_System_IDisposable_Dispose_mF5AE38C1BF625A1711988EA9BDB0257714C52C7C,
	U3CCleanTexU3Ed__30_MoveNext_m645EE34876427ABCD7BF753E92AFAA6E46A6FEB5,
	U3CCleanTexU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD4518055E3B70544495786AD3A5BB96DF4AA684C,
	U3CCleanTexU3Ed__30_System_Collections_IEnumerator_Reset_m569B1DADFD37C2B6E6EB20CDE72DA244FB9C72D9,
	U3CCleanTexU3Ed__30_System_Collections_IEnumerator_get_Current_mECC037CFDCC6AC9FF87C1D02AAA62AA5D30465FD,
	U3CImageAddingProcessU3Ed__32__ctor_m854CE612288CB526EA88059B6A9075B463749D16,
	U3CImageAddingProcessU3Ed__32_System_IDisposable_Dispose_mFF956FF062FDF19085C086812D7323C48DD38F74,
	U3CImageAddingProcessU3Ed__32_MoveNext_m280A77ED3E03D308DE0FBA73FE227850684B537C,
	U3CImageAddingProcessU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0F9BCD72DD34F2D3C1E470C814BDEF4ABDD0018,
	U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_Reset_mDD12A1D00339C63FCE4232072A6A0140F2415C2E,
	U3CImageAddingProcessU3Ed__32_System_Collections_IEnumerator_get_Current_m4BA238ED68F161B93A9D59108075FB6AFC756CCE,
	U3CU3Ec__DisplayClass33_0__ctor_m0612D1144698CE2522806F886E49F40A1A464A81,
	U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__0_m57DA530FF6375A1BAA1F59DE421BD7A0EECCE6AC,
	U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__1_m1FFA39C50375F9F577651AB200FB3BB8902F7986,
	U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eg__ViewLoadU7C2_mDE7ABBD9C4B3FEE0C399BE88F28C36D50796E910,
	U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__5_mFD6A4DA88E5E6FD3B2D4DA714F304E0743BBB764,
	U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__3_m29FB14662D7CDE3126944A612760E3881692269D,
	U3CU3Ec__DisplayClass33_0_U3CInitPoPProductU3Eb__4_mEE05507E522456AD1A80074B626DBD1F7C7577FD,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed__ctor_mFFC5CDFA51BD0578917E8B7D206E899CCE14443E,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_IDisposable_Dispose_mF6EF7AE7E4453DA1F88CFDC0B67151108164418E,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_MoveNext_mF768BB105B727E5A4CD93C6C25163F08C6C02B9F,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F167D0D324A297085FF7718472A0DA97810714E,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_Reset_m51DA00B6F13F0F29EA10E6464B2994FC00F3A643,
	U3CU3CInitPoPProductU3Eg__ViewLoadU7C2U3Ed_System_Collections_IEnumerator_get_Current_mD147C9A9E2ADE062EE4942F87F7BB7448FBCA02C,
	U3CU3Ec__cctor_m50565980B3965B7F7C2DF68E957691AE32088475,
	U3CU3Ec__ctor_mEAE64C8683B957A7D01117A6899C3883BC3E5753,
	U3CU3Ec_U3CInitPoPProductU3Eb__33_7_m8FC8BDC1E45B5F626257CF1FE2D123211E9DA009,
	U3CU3Ec_U3CInitPoPProductU3Eb__33_6_m86FC5D2852525D9C829F7402E646973D6EB4CF4A,
	AppManager_Awake_m35745A5427AB4F00226708C1ACA35EB14B3DA4DE,
	AppManager_ChangeScene_m211A3C5332E6B6D757DCC0CAC7520BB7F3E047B7,
	AppManager__ctor_mC3FF75565A6362EEBDB47070724D891877AE0414,
	ArUI_Start_m5C1693987688735C0ECC8F023FFA3FCA9A08DC9C,
	ArUI_Update_m401347E473630CCB2F8FA9B4E63D880C83C5879C,
	ArUI__ctor_m1E8D1D5C1A2BE44DE233095C44802530A32D473C,
	AuthLogIn_Start_mCCDAB431452883F34D5762E8C8BE5ACC229AB45A,
	AuthLogIn__ctor_mAB140E1C7504158E901FE1FD0F71499DB47FF567,
	AuthLogIn_U3CStartU3Eb__5_0_mD050B6932D242E0CC487AA1625BC28B7F8973C25,
	AuthLogIn_U3CStartU3Eb__5_1_mA894BEB560F0D5DE6DC808C3EBDC62F0C38DD8F1,
	AuthUIManager_Awake_mD990CE815FBB3A184C5DB3FD903ABD6312109DCB,
	AuthUIManager_ClearUI_m3144B66FB67DD933439BD02485B5C373B1AAAEBD,
	AuthUIManager_LoginScreen_mDE39A1393B6AC2A27081C5CB6B594E015F8C1F9A,
	AuthUIManager_RegisterScreen_mF56965E534E0CAA79FB6470E81D9E87D74875F4F,
	AuthUIManager__ctor_mBD40733306150C7E895EC0515DC6611365B29CE0,
	BoneController_get_skeletonRoot_m5B82500607091C4733CDA82A10439B15952C2209,
	BoneController_set_skeletonRoot_m238DE02D1A37978A54902DCBA560E4ABBF54588C,
	BoneController_InitializeSkeletonJoints_m4C6352B4B25AC75A6A0FBD16E1A9BAE78A25DC3F,
	BoneController_ApplyBodyPose_m882A8342657FF17B71197534E2384CC1D8ED56CB,
	BoneController_ApplyBodyPoseLeg_m40B592C09BD9E928AE268E9DA85261A609089DB9,
	BoneController_ProcessJoint_mE568E4706A98EB4CB10CF23A80732016F2B1F78F,
	BoneController_GetJointIndex_m559746F37FCE21E55B6A3873BB60DDDD7B132C29,
	BoneController__ctor_m03D53E58E7511A5F9332BCDCB403C3DC344F17F3,
	ColorExtensions_ParseColor_m8AF37B4F2A3A2747F9B4C6734B82E33819D8FB04,
	CreateAcc_Start_mC3CA1FC10BA390F09CCAC85BC9AB73EE5FB8F145,
	CreateAcc_Update_m111D838F8FBBFD47317DED27E9402ADC513A675A,
	CreateAcc__ctor_m91C1A7EE03DF89FACA5618F8D427F5223688D064,
	CreateAcc_U3CStartU3Eb__14_0_m8493AE06DE46BB2D14988EFCCED4A6F704ED723A,
	CreateAcc_U3CStartU3Eb__14_1_m279DBDEDDFFE381FDAD7941FCAE37CB8C80CEE77,
	CreateAcc_U3CStartU3Eb__14_2_m4C82B7B7FFC9F7DE9D05BBC79E8094FEE9B59881,
	CreateAcc_U3CStartU3Eb__14_3_m0EDEE6B256DBFDCE07F6557CCB638B68C1C4F081,
	CreateAcc_U3CStartU3Eb__14_4_mFCD9F25CEA26FAB2B305E94A54D9DF59591EF696,
	CreateAcc_U3CStartU3Eb__14_5_m1AEDA456989B62CAAD3DC6FD724F8F321182ECB4,
	CreateAcc_U3CStartU3Eb__14_6_mADFF5EB61973959631C2F5FD92E7D65D5D99B8F3,
	DropdownFilOverride_Start_m693F77E324752D47FD3CBB9635250E490D1508C8,
	DropdownFilOverride_dropFil_element_mC1E99AEBCDB4B3996C62217FF11A9A719FB09A11,
	DropdownFilOverride_Update_m8DD4A01B9A76CAB644DAF54C5D47E81400F6CABE,
	DropdownFilOverride__ctor_m92B8221BF5E5BD84A0675E87501A01B80349FEED,
	DropdownOverride__ctor_mAD31A876B28DEB8E0E207F3D87336E12703CD24D,
	DrpDwn_Start_mB59FEF775377D16CA9C4E1F6C0600BB69613F4A0,
	DrpDwn_UpdateInfo_m1233C2A1B3EAD9AACFA1A17F6ADE9950491F9A2E,
	DrpDwn_Update_mA1A8CBD7317E058FD9072484A8AE865810D0F4DD,
	DrpDwn__ctor_m3968EB3B7139B3763E01BFF3D06140AEC7468D7B,
	DrpDwn_U3CStartU3Eb__16_0_mD5E5F262F4764E4BDD121F758249F5B07B318230,
	DrpDwn_U3CStartU3Eb__16_1_m4C1733F6C750ACFCA947DDDF8E0C70C3F3197A98,
	DrpDwn_U3CStartU3Eb__16_2_mAA43DE9BCFE604E3E8ABAF9F8B3D390205896289,
	U3CU3Ec__DisplayClass17_0__ctor_m708B6233367F4C12D335BE051D75F9ADE1D6227D,
	U3CU3Ec__DisplayClass17_0_U3CUpdateInfoU3Eb__1_m5BAFBB114542688082C094F29F592A23812053EA,
	U3CU3Ec__cctor_m9B85CF271DF6B6AA8FAF16B1078DAE6617C47D5A,
	U3CU3Ec__ctor_m9AB5A708036223B5B520102E9D70EC9754E03FAF,
	U3CU3Ec_U3CUpdateInfoU3Eb__17_0_m34DC8AF7991BB58BD0F6BA09B9F19D5BBC9EC2AB,
	U3CUpdateInfoU3Ed__17__ctor_m3FEF25B5AD5BC513EA9D5F77465BD37D6D43EB50,
	U3CUpdateInfoU3Ed__17_System_IDisposable_Dispose_m16C5556EAC8EDC7494F39C587FD33B54F4AAD8B7,
	U3CUpdateInfoU3Ed__17_MoveNext_mAE7E97DDB0C937FACBD071F4573BE308BE3946A7,
	U3CUpdateInfoU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB4C894555B34B6737C66E1D17ECBE9DB1C7927F,
	U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_Reset_m253F46F270B3551A1D33007329863296A8A95CAC,
	U3CUpdateInfoU3Ed__17_System_Collections_IEnumerator_get_Current_mA27DF123CD6808FFCFF01A6410EDF7D56732C434,
	DrpDwnReg_Start_mFEF5C8C42821D9A3773FFED5D26D1DE623D08972,
	DrpDwnReg_Update_mBD537603E6CF02FCC872ACF27866355AF8B92C4C,
	DrpDwnReg__ctor_mB4C7C9C44BB384B2F51A9FEB729D628D3CF4EAA0,
	DrpDwnReg_U3CStartU3Eb__10_0_m37F2FF9FA3B44096345696AB66223D8869443F40,
	FirebaseLoad_Awake_mC63CF9108E4161DE0BB12593F7087BCA047B7097,
	FirebaseLoad_Start_m0088E3710F313558FAFB3467B7708B3AB0C3CCE7,
	FirebaseLoad_OnDisable_m77C2F7C466551479EA70AF45BC7257CB24C87F9C,
	FirebaseLoad_ARDocumentsBatch_m35E94991AB7C239E5A7BB71365565F95739CECDA,
	FirebaseLoad_ARDocumentsFav_mF54A7E214DDFC9BAD4729EF83F21C888270F499B,
	FirebaseLoad_ARDocsFV_m723E735998B8B4D20AAA96EB69FAC26E117084B5,
	FirebaseLoad_ARDocumentsFiltered_mC55EBF1FE6F06BDF5CB12E5E025CDDAB2E00C028,
	FirebaseLoad_ARDocuments_mAA4A6E5B0D5D927755DD7E36822869D02F1BCD3B,
	FirebaseLoad_ARDoc_mE3C0EA4A11B5C67EF5AF8BAFD0D5BEA999FBB461,
	FirebaseLoad__ctor_m240A0324125E79A183E90EB3BE3108F495A660E1,
	OnQueryfinished__ctor_mB70BB12076A977EBC8CB0B9BC4E386D731B510ED,
	OnQueryfinished_Invoke_m68ACCBE29D78BF8DD1DAA66D9E12C0BA9202329F,
	OnQueryfinished_BeginInvoke_mAE3CEDC4B64439CA659F52C765AECF4757EDBED5,
	OnQueryfinished_EndInvoke_mA343E71E3956036C3A5ED5161A2B1AE839810A9B,
	OnInitPagingRequested__ctor_mA397F96C548C4D24388D301EB67E28FF9337F9D1,
	OnInitPagingRequested_Invoke_m6134C247A7A3F7478EFA909EB61DCD78143A0902,
	OnInitPagingRequested_BeginInvoke_m4EA59ABA2CCFCB9665711A7E402E229D04E9951D,
	OnInitPagingRequested_EndInvoke_m0378A727CED84A656F27F68F8CA1899614F03625,
	U3CU3Ec__DisplayClass30_0__ctor_mEFF7B45AAFB9ED8C5F039C28FAC844A3EC0BD1BA,
	U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__0_m2540B2DCAB7CCDA30F2C8E7E0E74AA0B363F8153,
	U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__1_m3C6454FC7CE2D2A40EB8EEF60E96461FF4D03F5D,
	U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__2_m6CECE7F640BC3B169C20DE542025D6D8BD03F767,
	U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__3_mFA6D63BE477DD57E38C2838458576A5472D17578,
	U3CU3Ec__DisplayClass30_0_U3CARDocsFVU3Eb__4_m1A5A93ED6712814514642B7028A023137CF513E6,
	U3CU3Ec__DisplayClass30_1__ctor_m0582C841C43C42790BFDE3388C7E66EC3EB85624,
	U3CU3Ec__DisplayClass30_1_U3CARDocsFVU3Eb__5_m5A64AC4EF00D12AB72947375D358420FCE34CC1B,
	U3CARDocsFVU3Ed__30__ctor_m75CDDE36D3FFCC64B53247BA6FFF09812B19B611,
	U3CARDocsFVU3Ed__30_System_IDisposable_Dispose_mCA30BA2BD4B04206E965F6EB72F13B58B4774741,
	U3CARDocsFVU3Ed__30_MoveNext_mD4D17D5E59DB6B735841D339B3127145DD8E04A8,
	U3CARDocsFVU3Ed__30_U3CU3Em__Finally1_mFF6D0D30C770C2B83F36E70D3470F18650F3F97F,
	U3CARDocsFVU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5700A999E0CADC5394B66BAAEC398D681B28FDC0,
	U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_Reset_m133F4D7C73219BD846D577CA416A3FD3E71F788B,
	U3CARDocsFVU3Ed__30_System_Collections_IEnumerator_get_Current_mA1F3A732F1D8F4FA9FD6BF4BBC3ED3E58DED5292,
	U3CU3Ec__DisplayClass33_0__ctor_m17DF0A022A73DF5C8D1EFA526CA38A6BA3FAE46D,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__0_m604B7351582E22966F31C7A27ABF3AAD3B0A6E3C,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__1_m0CB464F55F50FFEBF4234D90B91BDA0DE9DCADA0,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__2_m85182A04FCB7D9BF944D5935CC6EBF6B26B92572,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__3_mD64FED2F46D5B02CF2332227F9CF217391F3E85B,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__4_m848D6C51B44A4A8474FA82A31ECAF51841B92892,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__6_m7AEA77BBF2AC33DD9DD758A5DE5C2193BA30A91E,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__8_m3CF03E60DE4E44FF3197D25B53847264ACC94856,
	U3CU3Ec__DisplayClass33_0_U3CARDocU3Eb__10_mA38F9AFE9B2156854A56936C7EB9D56E7AE771EE,
	U3CU3Ec__DisplayClass33_1__ctor_mBD9D37F7AE2B37ED2150AFED8A3F5082812D4682,
	U3CU3Ec__DisplayClass33_1_U3CARDocU3Eb__5_mD45E959A29F4180227789559CF0FB2EE6D30722F,
	U3CU3Ec__DisplayClass33_2__ctor_mF1F9382B63CB796BAC1AFF5E06435CD3100E5350,
	U3CU3Ec__DisplayClass33_2_U3CARDocU3Eb__7_mA19D7EBA9D984427C8DF357D712F52AA0E195B45,
	U3CU3Ec__DisplayClass33_3__ctor_m8B0B87D4BD0635AE7EDE982B3505CE3B59928398,
	U3CU3Ec__DisplayClass33_3_U3CARDocU3Eb__9_mE4025D9CAB06A18151493AFF0484393BB599C4FC,
	U3CU3Ec__DisplayClass33_4__ctor_mA36D32BDDF0E5996702C83F124D5BB37504A8658,
	U3CU3Ec__DisplayClass33_4_U3CARDocU3Eb__11_m82E32590913D04D56076B6A8E023F640A18F6071,
	U3CARDocU3Ed__33__ctor_m960E0A911B1A28A79ACDB501FAC11582CDC61C74,
	U3CARDocU3Ed__33_System_IDisposable_Dispose_m1F7771C8269333EDF80C8A0A9BC6900F7026A6B1,
	U3CARDocU3Ed__33_MoveNext_mE1547D2BD77420BEBEAABE476FCFD1F8C52437A8,
	U3CARDocU3Ed__33_U3CU3Em__Finally1_mA8EF6ABAB6E2CB04BC10F5EA5605EE8365EA4878,
	U3CARDocU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A477DE8915FCE6C5B41631BBF0A1149DF46AA5E,
	U3CARDocU3Ed__33_System_Collections_IEnumerator_Reset_m829D659EDD2CDD8D554D68CA96FFBFB260607642,
	U3CARDocU3Ed__33_System_Collections_IEnumerator_get_Current_m488B0F68F860C0A9E1F27F591DD7E840B49E9DE5,
	FirebaseLoadProduct_Start_m867F13B7891AFD714353472D68564F30E5131C3D,
	FirebaseLoadProduct_OnDisable_mCB0C639E1FED4D4F21C638E85BE0FD2E3A3F8D86,
	FirebaseLoadProduct_SingleDocument_m135FD585173A4DD4EAC0A592A06C50D41F1CB278,
	FirebaseLoadProduct_SDoc_m3568BA13939C82452402248225F7027FAC287E6D,
	FirebaseLoadProduct_Update_m2176533BD75B69ED011965BFCE2B9C7E7DA5F146,
	FirebaseLoadProduct__ctor_m2AD37FCFCB654C853CBDDBDDDC855DA59DFE3439,
	OnSingleQueryfinished__ctor_m73FDE9CF1F76950F048FF400A1677A58A6114CE6,
	OnSingleQueryfinished_Invoke_mA08C46CCB92EE2A0829EB5B73B434C8F459BF122,
	OnSingleQueryfinished_BeginInvoke_m9F6792FF057D653E066D601F1F504D2443F4A112,
	OnSingleQueryfinished_EndInvoke_m14B091EC4A2378F13A2DA9AB7EA0DFF40B96ED0C,
	U3CU3Ec__DisplayClass6_0__ctor_m00F5C4A61FE968E1413D8F898E78D3EC1265CC01,
	U3CU3Ec__DisplayClass6_0_U3CSDocU3Eb__0_m11CF0C98D671A9EFB5F3D1A42AF11114ABD35F38,
	U3CU3Ec__DisplayClass6_0_U3CSDocU3Eb__1_m3A6322470C611410B29B234B2DA3D69A3258203B,
	U3CSDocU3Ed__6__ctor_m11F634D3BA0FE2F0429A352773685482007C1CA9,
	U3CSDocU3Ed__6_System_IDisposable_Dispose_m45D6F15DC5EFC39587FBFEC0379E79580C78CB1A,
	U3CSDocU3Ed__6_MoveNext_mDA232EBE086D4D278AF2E7E48620235A0BCE041D,
	U3CSDocU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FDA9AE366B9131C3E4028C87E0F19AC0E02DFB5,
	U3CSDocU3Ed__6_System_Collections_IEnumerator_Reset_m307C49A52A70B1F44B5E4422D634A0CF460E5285,
	U3CSDocU3Ed__6_System_Collections_IEnumerator_get_Current_m19E7A850C40FA8D83DB0085945AB5D14CDF3F34B,
	FirebaseManager_Awake_m0FB28B77E6B0665F486CD8B13EDAD174FDC911CA,
	FirebaseManager_Start_mC613ACB1AF5C9327DFD21634676C02D3991E5571,
	FirebaseManager_CheckAndFixDependencies_mCDC8E2754EC8ECC9B3E0DC26278D64E9077899C1,
	FirebaseManager_InitializeFirebase_m0AB60DA55A5299AEFEEB03BAA7AAAA2CEDB11E90,
	FirebaseManager_checkAutoLogin_mF4122E6817DA0E8B51DB444A81149C836D75F1CE,
	FirebaseManager_AutoLogin_m305C571F91FA74478971350489E6A8EEADCEF97A,
	FirebaseManager_AuthStateChanged_m918C661027778087914DAF004BDD83A730CD7F3B,
	FirebaseManager_ClearOutputs_mDE479EB062FCBBC65991145EA585E789EA15D00B,
	FirebaseManager_LoginButton_m6641E5EC1D5B8447CBB05DA37E7C3DDADCED23C7,
	FirebaseManager_LogOut_mA9A19730AF4F90ED134AA731AC1EA88AA33FFB5F,
	FirebaseManager_RegisterButton_mB929DC64C8D07811786BB11AA419EC7D90C99473,
	FirebaseManager_LoginLogic_m57B61E650C749A3D740CE8E03A35F1F099EF9A4F,
	FirebaseManager_RegisterLogic_m30CBFD9C2A3A498E6A7427FC73DA16B03DDB80E6,
	FirebaseManager__ctor_mB894832C008369C3C25D3B0DF9CDF84481C89506,
	U3CU3Ec__DisplayClass20_0__ctor_m4A4880E34501BD9BAA4AC205BBB9CA8A34FDBBE7,
	U3CU3Ec__DisplayClass20_0_U3CCheckAndFixDependenciesU3Eb__0_m96829A9A6B434C1D4590E6B410FE2254181BAF8E,
	U3CCheckAndFixDependenciesU3Ed__20__ctor_mE513235E9CEFAD28389F4BD273B0234A2546E9A6,
	U3CCheckAndFixDependenciesU3Ed__20_System_IDisposable_Dispose_m50897C509C5058F4E638DB16A8BCD88C2262F6AF,
	U3CCheckAndFixDependenciesU3Ed__20_MoveNext_mF3BDC7CFC43BBF25CA6D1DBCD85ECBF70F984389,
	U3CCheckAndFixDependenciesU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66C8BEDCAFFA24358C0D1DCA597C7DD35CF55F50,
	U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_Reset_m0ADA09A0EE9657765B6A84B223956B114B8A8479,
	U3CCheckAndFixDependenciesU3Ed__20_System_Collections_IEnumerator_get_Current_m1FC420C47DEF8F1B4127A3E12EB95BB1D8AD6AB5,
	U3CU3Ec__DisplayClass22_0__ctor_mA7C9228D96EBF0E9677310B79C7DE350B63F7C08,
	U3CU3Ec__DisplayClass22_0_U3CcheckAutoLoginU3Eb__0_m29841C8B6A8500AFAC2AB94B5B286237D1817ED6,
	U3CcheckAutoLoginU3Ed__22__ctor_m872281A15157F52029A9E8A929C8AC92CBA9C840,
	U3CcheckAutoLoginU3Ed__22_System_IDisposable_Dispose_mF912F49AC2DCF9900F49A2BBCF2FA99834A0F8B2,
	U3CcheckAutoLoginU3Ed__22_MoveNext_m5273699201C45721E6EF9FE67635E488BFA4E007,
	U3CcheckAutoLoginU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99FE1B44D48593D6CA001BBDCD7F9857C6665444,
	U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_Reset_mDBD4A544E27C49C79F8AD57FC51E2F9818F0AB0B,
	U3CcheckAutoLoginU3Ed__22_System_Collections_IEnumerator_get_Current_mB0390E85E68A1251F204074331BB25608E431ACF,
	U3CU3Ec__DisplayClass29_0__ctor_m1A677314734F28E27D7AA62FB1767E6834E81BCE,
	U3CU3Ec__DisplayClass29_0_U3CLoginLogicU3Eb__0_m1639EE91752399394EFF76E024753335C1EFAE74,
	U3CLoginLogicU3Ed__29__ctor_m237FCABA55986243AE341530B887A7B9CFECF3B2,
	U3CLoginLogicU3Ed__29_System_IDisposable_Dispose_m56262A632D7BBE88A4049CDE4E61400A42DE5411,
	U3CLoginLogicU3Ed__29_MoveNext_m6475088819A91587313B4114925295334B950159,
	U3CLoginLogicU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9B518C7141CFA0652289ECD3B525D0285655B7D1,
	U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_Reset_m7A22EA515F8C3136ED8677F16282E19F0D98DDD8,
	U3CLoginLogicU3Ed__29_System_Collections_IEnumerator_get_Current_mCBABEFDD04C2BC6495F7DB8329606E5E19172F38,
	U3CU3Ec__DisplayClass30_0__ctor_m6C802D47CB88DAADF4DB64FB2E8C64D8C9F9F277,
	U3CU3Ec__DisplayClass30_0_U3CRegisterLogicU3Eb__0_m3C55808E777E37F150CBD289C805DFCF193BC721,
	U3CU3Ec__DisplayClass30_1__ctor_m0ADE6D59F393F35F1AEEDC213FE8DB5B26FA70B1,
	U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__1_mA079161ABF2C1BBEB82F33390A22D0358234903A,
	U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__3_m31A0438D67318B88950BF6ABE708899B381CD0FE,
	U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__5_m9708FDEC75EA78EE9250962108F2C1B7A7F3E863,
	U3CU3Ec__DisplayClass30_1_U3CRegisterLogicU3Eb__7_mBCBE396AF0302D04D10B98DA79366C4D70ED0D94,
	U3CU3Ec__cctor_m9C76236275D1302561C5F35B08586CB778EFA06E,
	U3CU3Ec__ctor_m9539F2699E040A5A97D5CBB90137A1FD789DC372,
	U3CU3Ec_U3CRegisterLogicU3Eb__30_2_mAA423066A61A9D823ED193B857BF102C12D79180,
	U3CU3Ec_U3CRegisterLogicU3Eb__30_4_m11364D72F993ECBCC265EAFB8DD5C5F8423C8B24,
	U3CU3Ec_U3CRegisterLogicU3Eb__30_6_mAC7BD6AE84AFAAC6C003ECA6F32EA9BC6A01C376,
	U3CRegisterLogicU3Ed__30__ctor_mB79F27786D67ED3D68638817B1BD0B2089F1FDBD,
	U3CRegisterLogicU3Ed__30_System_IDisposable_Dispose_mB5B9C8F6193294A31E5211AADE5344B5C0F68D14,
	U3CRegisterLogicU3Ed__30_MoveNext_m767EA298C57C6F4DECA1FA47C80403272DE210AE,
	U3CRegisterLogicU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mABB3EA176CD17130B14FB881A35DD2D15C6C4A6F,
	U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_Reset_m777E21375ABD9F78DD640A7188273C00A51F78AB,
	U3CRegisterLogicU3Ed__30_System_Collections_IEnumerator_get_Current_mBBB8E1D2A536AFC119B513D81CE9ED58929E784D,
	GarmPop_Start_m0DE910BD19A7F100C2619D3E41145D23E098EA20,
	GarmPop_LayoutFix_m755DFC9DE0CEABEDC13F87BDB8373A2A07CE1F5A,
	GarmPop_AddGarms_m64A1500BD8163E0AD7C48858CC08E7887A4F758A,
	GarmPop_InitItem_mC1162B371CDAFB78A8797D849850444106D108E3,
	GarmPop_DownloadImage_m84173F9995B260824C08FC1C11F8B4C8035E3AC2,
	GarmPop_MakeItem_m107802F68967B6A1F2F311F3E93D51565B453E2E,
	GarmPop_rectcalc_m65136061B74BDDD46E9653306C984B4EBA0AFDCF,
	GarmPop__ctor_mF01AD224772100422C0855EBA7C68DA76AB0F679,
	GarmPop__cctor_mA5575E55296E1D9907C3360F34F27924C95B7E6E,
	U3CU3Ec__DisplayClass26_0__ctor_mACAD90F33E52B3CFFE4B4087C216FCD4F5D9A0ED,
	U3CU3Ec__DisplayClass26_0_U3CInitItemU3Eb__0_m3E1B53C0666146FAD3CD3E41E422F81D883CFA0F,
	U3CU3Ec__DisplayClass26_0_U3CInitItemU3Eb__1_m2057C8EB951C84CDCD550D7A51BA7D7BCC932F2E,
	U3CDownloadImageU3Ed__27__ctor_m4A14D0747572B8EE136B45D12D121AFE2162C2C8,
	U3CDownloadImageU3Ed__27_System_IDisposable_Dispose_mF6799C18193605A8BE990E1BEFAF3A3A920D975F,
	U3CDownloadImageU3Ed__27_MoveNext_mFF70143CCBC387DA9226CCD6145738B29EA71A53,
	U3CDownloadImageU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2B5B8ABD582D8F5B844A24F13A3C5B25B52AE1D,
	U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_Reset_m77E7F96BBC7786A52B73A5EFCA9BEC65D739CC29,
	U3CDownloadImageU3Ed__27_System_Collections_IEnumerator_get_Current_m44C8F7E23295F58BC5D19979F36A056B1A68C76D,
	HumanBodyTracker_get_humanBodyManager_m68D297EB7BABF50F4C2DE39F88B5B90C5E4191AB,
	HumanBodyTracker_set_humanBodyManager_m76D4425C7F0F4E4CC53FF83A9FE884B61F7EBEFF,
	HumanBodyTracker_OnEnable_m0470A62AD963F51487CEA9B49412304C19CE36A1,
	HumanBodyTracker_OnDisable_m91B4B8B572BEB519C241D248A7FBA817BEA3A7B2,
	HumanBodyTracker_InitAr_mBEB9EA30F3D57CBB6B997C95EE4E5860A59065DF,
	HumanBodyTracker_GetAssetBundle_m88CEBCF31113436C0DEEBE6E8FB712F4F4E7871D,
	HumanBodyTracker_OnHumanBodiesChanged_mD2D373BEF77C090B89BE48A9459D87953676E1DC,
	HumanBodyTracker__ctor_m7F9563662EA3EB1910A498B7E962391A67398CA9,
	HumanBodyTracker_U3CInitArU3Eb__14_1_m4DE5C014E90FB1395DECCE1D36220252B78B7BF4,
	U3CU3Ec__cctor_m297B92DA3C960DC4DD545922EC608164386C9E41,
	U3CU3Ec__ctor_mCA14709230C9399C4A6DE985C838912E9C05AB9A,
	U3CU3Ec_U3CInitArU3Eb__14_0_m4A89FBCBAE8C7D6919F36ED978B700EF4EF8AD0C,
	U3CU3Ec_U3CGetAssetBundleU3Eb__15_0_mAB67F2571E0C76246D491A1B91C3F17000FA172E,
	U3CInitArU3Ed__14__ctor_m0BCE60FC071A5BB60B68A3F58582F4072B03F4A7,
	U3CInitArU3Ed__14_System_IDisposable_Dispose_m2D6CADDAA2F5A47F381E9C8197F4E3DE4998CC30,
	U3CInitArU3Ed__14_MoveNext_m7CFF5AFCEB960093E9D44339E761080076E039E8,
	U3CInitArU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5ECF5BB94B05D7C55A84197D235473F98DFAD1F,
	U3CInitArU3Ed__14_System_Collections_IEnumerator_Reset_mDAADBFE05CAAB470F57C48D2380BD54640B285A9,
	U3CInitArU3Ed__14_System_Collections_IEnumerator_get_Current_mB4952B1DD52B1A6B63B24723EA9B6C6E5704EE9E,
	U3CGetAssetBundleU3Ed__15__ctor_m401E6206298334CA59DD8FAD9E64B78A6D1DE8FF,
	U3CGetAssetBundleU3Ed__15_System_IDisposable_Dispose_m0F4FD2EE83D390DD3506608D143E0521AE2B902C,
	U3CGetAssetBundleU3Ed__15_MoveNext_m6F83A92E40318755E7BD6CEA642F559D1948FA11,
	U3CGetAssetBundleU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m281ECDE9CA2D3588CE643FDC7865D477D53C70FA,
	U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_Reset_mC0ADC36AB218452B7F2409B765C99D0D82E8EB8F,
	U3CGetAssetBundleU3Ed__15_System_Collections_IEnumerator_get_Current_m6419ED791E01A27B653D693CD250F4BAE9699232,
	HumanBodyTrackerAvatar_get_humanBodyManager_m227A5F2BD00A31243ED33B2DBEAA7EFDDD36AFD4,
	HumanBodyTrackerAvatar_set_humanBodyManager_m9F8CAE6E17E31F4F45642D175CD16EEB345EECFD,
	HumanBodyTrackerAvatar_OnEnable_mB4661B9274890152143D6FDB208767E919D46819,
	HumanBodyTrackerAvatar_OnDisable_m226CE3EB7DE70BCD1676109D0748975CADF990A3,
	HumanBodyTrackerAvatar_InitAr_mBB6CB82039FBE01973C0A9FEDD0727EAEE8430DD,
	HumanBodyTrackerAvatar_OnHumanBodiesChanged_m00462AE6E5C86639C597E26EADDF44727D583378,
	HumanBodyTrackerAvatar__ctor_m9B0766C27B46E2D0488CDB200C0F9C8494EEB97B,
	U3CInitArU3Ed__17__ctor_m5D62044BEC329579BDD4C8F4BAE9B64F1A9AF6A8,
	U3CInitArU3Ed__17_System_IDisposable_Dispose_mC04A34D6A9AA9D0ADE679858B6A0CF8EDE9098C2,
	U3CInitArU3Ed__17_MoveNext_m6246767278CB6B2BE90239267132B2D57890A426,
	U3CInitArU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD40A5A4069316DB30BB2F1611247BDBD1F5345F3,
	U3CInitArU3Ed__17_System_Collections_IEnumerator_Reset_m95F44591F726BD047A6BB8920E7B62C2CCCDD776,
	U3CInitArU3Ed__17_System_Collections_IEnumerator_get_Current_m6017680888DB02B9B20F061F4B8C9EC90E1562D8,
	HumanBodyTrackerGarment_get_humanBodyManager_m8BE7C6C6B20F03918528CCFE520BA1D4D5C34BC5,
	HumanBodyTrackerGarment_set_humanBodyManager_m17D685BCEBC1CBC52A8A3744134468CDA6C09D14,
	HumanBodyTrackerGarment_OnEnable_m48C580FE26248CEA6C39C7530DBC79D0156A369B,
	HumanBodyTrackerGarment_OnDisable_m0509B03FC25B0B111AF90C18BC733471689A10EC,
	HumanBodyTrackerGarment_AvailableColors_mC1C04209D418DE6B950FB40DC296DB41724E6CF4,
	HumanBodyTrackerGarment_AddDrpOpts_mC0F86E2DFB87FC4FD16C7EF8FD17A7CF6EAF68C0,
	HumanBodyTrackerGarment_InitAr_mD74EBB8D82EB0E679796BC4561B5C1AD66CAEA99,
	HumanBodyTrackerGarment_GetAssetBundleInit_mC8408C8F96732956774018F9279CF136A3DB118B,
	HumanBodyTrackerGarment_RespawnColor_m1AF0CD10B47DBF05E07FB6CB2DD5E7EFC91C86B7,
	HumanBodyTrackerGarment_RespawnSize_m187FE19EF6C2040513748D61E17797E13C3B8A34,
	HumanBodyTrackerGarment_GetAssetBundleExtra_m6BA170E938C7A8E18D07F75EBFFE195A7D0AA8C4,
	HumanBodyTrackerGarment_OnHumanBodiesChanged_m49B3939B73B014EC3DFCE6D9C65DB9904356474A,
	HumanBodyTrackerGarment__ctor_m907A4042F0BB524B537C6E7ADE036755F96E5900,
	HumanBodyTrackerGarment_U3CInitArU3Eb__41_0_m77EFB7D5E455F11427FFE4DEB5A5E7DFCE0DB687,
	HumanBodyTrackerGarment_U3CInitArU3Eb__41_1_mC654DD0D05916ACE0CFBF0C58EFCAC4876750B07,
	HumanBodyTrackerGarment_U3CInitArU3Eb__41_2_m9D21EB7E7EC5693B339E241A00BB7A0FA5427BA9,
	U3CU3Ec__DisplayClass39_0__ctor_mAA8E1551EED6CCA554DA6F0E6C0D15911BB8FF2F,
	U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__0_m8AC25BEF4E302711FC9B8265ECA0D382AE2CBF21,
	U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__1_m8948804688DAE76538F7DF81B055D1C0CA693B1C,
	U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__2_m65A126E37746EB32290BC85B114FA3DEC5B4B19B,
	U3CU3Ec__DisplayClass39_0_U3CAvailableColorsU3Eb__3_m014928A1436DE83CF25660B0F4430ED2C00C8E7F,
	U3CAvailableColorsU3Ed__39__ctor_m0E7ED7000437E952D8E68D1C62B1316AAA31A049,
	U3CAvailableColorsU3Ed__39_System_IDisposable_Dispose_mDBC0CA875800EF0E74C0C02CEC5C5D38966EACC8,
	U3CAvailableColorsU3Ed__39_MoveNext_mBBD9997A9EDCE9D5A34E38DBA2B9F97E6D3FF101,
	U3CAvailableColorsU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m71E0D4E1D33EB11C00D58C59F1CB4EF672574BF8,
	U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_Reset_mB982CFD7B68F4859E7412FE32A24554DDF8B9E34,
	U3CAvailableColorsU3Ed__39_System_Collections_IEnumerator_get_Current_m83710E3C8D34400DB97D8B5169AAE0BDFD131444,
	U3CInitArU3Ed__41__ctor_m1D5A3ACACB83EE5605FF4AC082EB241A1460BCDF,
	U3CInitArU3Ed__41_System_IDisposable_Dispose_mCB2CE8CBF5674FD98B569F02FE5AC26555DE020C,
	U3CInitArU3Ed__41_MoveNext_m519C71A8B6D062CC36C72A6D459D651800501EE2,
	U3CInitArU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3BC0533830866A8CF98F479EF5B8106A8B635384,
	U3CInitArU3Ed__41_System_Collections_IEnumerator_Reset_m67F2B799690EB2F34910D863BD4CB2C83F3E1809,
	U3CInitArU3Ed__41_System_Collections_IEnumerator_get_Current_m29DD6B0CAAAF87F7F1322A81E39F4F0788B86053,
	U3CGetAssetBundleInitU3Ed__42__ctor_m3BD2C502FFA7F018FF2A9E5556E12D28D147BDC5,
	U3CGetAssetBundleInitU3Ed__42_System_IDisposable_Dispose_mA97EB664D7C245AA9DA4CBD3507F5603E8464FDB,
	U3CGetAssetBundleInitU3Ed__42_MoveNext_m5EBF5551D9F559854F13B35A96EDEEBDCFEBDE72,
	U3CGetAssetBundleInitU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F3D0239193B5CBCC7AADCA905298F2B85B3870D,
	U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_Reset_m9F64FD56FD033B71B1AEDF751D313A81230B2FC7,
	U3CGetAssetBundleInitU3Ed__42_System_Collections_IEnumerator_get_Current_mEE72D133A4AD3A0E74B5164CB3F09F72E28D87AA,
	U3CU3Ec__DisplayClass43_0__ctor_m1B51A327FD403F7A5A6FFCBB1778A99F86C49135,
	U3CU3Ec__DisplayClass43_0_U3CRespawnColorU3Eb__0_mEE986E8C5469587E1E3A2E32C0ED5D7F094542E6,
	U3CRespawnColorU3Ed__43__ctor_mCDD3B2C9874ADE71189C1A2ACBDAB227D2455955,
	U3CRespawnColorU3Ed__43_System_IDisposable_Dispose_m7BBE4857CF406984A98BD1C95C6F40D8DEF5A516,
	U3CRespawnColorU3Ed__43_MoveNext_mDE8B95E357B923EEE1CEB004FD055AFCCAD7B063,
	U3CRespawnColorU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04D63008CE9C30952B4DEB9402BB40C2C291BF02,
	U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_Reset_m7FBE3DC6DA7C4020EE8EE7D13954C9FCAA874F81,
	U3CRespawnColorU3Ed__43_System_Collections_IEnumerator_get_Current_m5EDFFCF83AF6C196734AAAA90F2CF68397620C8E,
	U3CU3Ec__DisplayClass44_0__ctor_m291704CF43BB311CDCA3CAB263DA8F9BE4DC740A,
	U3CU3Ec__DisplayClass44_0_U3CRespawnSizeU3Eb__0_m9F32DAFB5220B8E308F010933309D11F4C39FF03,
	U3CRespawnSizeU3Ed__44__ctor_mAC3B30605681C825B91BBA89893EF6CB1A3D96E8,
	U3CRespawnSizeU3Ed__44_System_IDisposable_Dispose_mA0ECE09AB7D10863359BF0B7026FC47BEF15FF48,
	U3CRespawnSizeU3Ed__44_MoveNext_m61E7D93893C4418DEAE35D289E2DB95A97421556,
	U3CRespawnSizeU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34077D9F5C1F11EE301947D160A382D1349E45EA,
	U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_Reset_m3FDD44A4DF8529AECBA79B66DEF8E53C8AC5356E,
	U3CRespawnSizeU3Ed__44_System_Collections_IEnumerator_get_Current_m7F780518B35ADEDB939AE4E7A59237B3BF7F42BC,
	U3CGetAssetBundleExtraU3Ed__45__ctor_mF7B098B86805EA416C3462209C68E05D57BC41D6,
	U3CGetAssetBundleExtraU3Ed__45_System_IDisposable_Dispose_mB00EE4A9B81482B388F6A20A290B0D0752167601,
	U3CGetAssetBundleExtraU3Ed__45_MoveNext_m3E3AFB36716755B91F6482393950C5EA446FAFF8,
	U3CGetAssetBundleExtraU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCA81081F88F877593B79FB77044DF848A78B7CBF,
	U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_Reset_mAA2607639C0DF413DF98F03E05E7B59C31B83CCC,
	U3CGetAssetBundleExtraU3Ed__45_System_Collections_IEnumerator_get_Current_m9E60F5E5C1D05D243287FD86E4ADE78EE4ABAE36,
	Init_Start_m82D7ED5811F1B9A02537C9648565249359023DC3,
	Init__ctor_m3AB34A4D9E5A7311947DBDEF3D59B679BC6A3205,
	Init__cctor_m0FDFEFC46A4A53960685F898959F9B39EF4755FF,
	InitList_Awake_m5D8E9F4A5364380CC2D05910774C1A426550A4B4,
	InitList_SendRequest_mDCB17F399B1B450B88D03071CBBB291575729F24,
	InitList_Update_m9461782FD2D6039D4B3AA9943EE1E794726796B7,
	InitList__ctor_m7F05BC830284FDE397311D04A9FE0C28E2D40AF8,
	OnInitPagingRequested__ctor_m174AE0E417A1DAA491FEEF58FC6D65EFB4EF2CF6,
	OnInitPagingRequested_Invoke_mEC89E3110366A23E02ADEF22966B545563112775,
	OnInitPagingRequested_BeginInvoke_mC7C2BB6D7988C4555D9224C21430428D96D18969,
	OnInitPagingRequested_EndInvoke_m66E6C1DDF872ECD5D22BEFC8A5DEC9EB18760E1C,
	OnInitSingleDocRequested__ctor_m45036B08EF5BB946377F2C2FF20C6037045FDD96,
	OnInitSingleDocRequested_Invoke_m385A4B07A171F42F0250EAD5378870A12F73D9E0,
	OnInitSingleDocRequested_BeginInvoke_mEA36DB8A9049573764E48ADA99A357B6E98A670D,
	OnInitSingleDocRequested_EndInvoke_mA90B5F064100B11C8D5D4D2B74C5152F06C3F348,
	InitProduct_Start_m0961301980C1FA5077F5C0BD752DC754C6E5E8DB,
	InitProduct_Update_m177F680AE737F4B05AADED470269A4572D60C1DF,
	InitProduct__ctor_m39256C3BC9A7F1EF54BAF5AE59A8B9F11E8FB310,
	OnInitSingleDocRequested__ctor_m8060CA3A2D3C916F09623A8BE892A9EE5908DA4E,
	OnInitSingleDocRequested_Invoke_m722361E55E140E04470AC4E1ED9429F242460592,
	OnInitSingleDocRequested_BeginInvoke_m53BF7CB569BCE1B528AF19BA04A46123A2C3F2A4,
	OnInitSingleDocRequested_EndInvoke_m4D051E2B60217F1CE5BA71A3DE61E903798EDF2C,
	MainManager_Awake_m00A3A3401B125A3CE7EA546FC61FB4D8FD1E0B79,
	MainManager__ctor_m90DF2FBE6170D98DF8F9C03C44FB914322C4D10F,
	NewBehaviourScript_Start_m783F84A617DADC4574B0BF1524481E6B96C65661,
	NewBehaviourScript_Update_m411C4D5C2D993FD70092FDA0FE2AC4786F8AC001,
	NewBehaviourScript__ctor_m437970EA37D66BDF32972F4CC0F65B95E5961FAA,
	RecommendationU399nputClass__ctor_mAA6A161CB7EFED2B28A7F6FDC3648252A6A74AE2,
	SceneBack_ChangeScene_m078EB208872E0D7D66B18011BD5F23967397A77C,
	SceneBack_Update_m3207CCCDC04845C8B1274F238750A96EEBD9D15B,
	SceneBack__ctor_m4E9E84576134D36D0C21F296D2782745435A445E,
	SceneChanger_ChangeScene_m00182E4E31B32E2E0E818E34FEEF207B6CF9C790,
	SceneChanger_Logout_mCC126448679F9759D5D0EC6DACB7AC41863FCEE0,
	SceneChanger_Exit_m7367524BED2B13B8A53A9698F3AA3A9B2D31CCD3,
	SceneChanger__ctor_m11AE9A596EFE92EE1AA22BD7A48AB0C1D758AB1D,
	U3CLogoutU3Ed__2__ctor_m8FF31D23636AB1BA8FE7C518DD99F955F57483BC,
	U3CLogoutU3Ed__2_System_IDisposable_Dispose_m19E8D33544E5D32EBA7F3CD5842047218D244A58,
	U3CLogoutU3Ed__2_MoveNext_mCB5AB6C5BDF5457A6BCBCD0CDE05D2396413C9A0,
	U3CLogoutU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCABAF6E94019D40B2B6971BD8E7801A981335B65,
	U3CLogoutU3Ed__2_System_Collections_IEnumerator_Reset_m50F33DB45F147049E257D2706FA09BC1E33CD0DF,
	U3CLogoutU3Ed__2_System_Collections_IEnumerator_get_Current_m5195917ABAB3B2D446AF9DAA5BB46F03FD156627,
	SceneChangerAcc_Start_m45FC877D6F266DBE50B32B34546413CD63AED210,
	SceneChangerAcc_ChangeScene_m115ED3A6CE1380F8117FDE99C2AEB510044A425B,
	SceneChangerAcc_Update_mE70D8FFE07655C48D8749272AED23790A0F51A42,
	SceneChangerAcc__ctor_m9C043D7E473859C027B66B06A6BF6B693772DB71,
	ScenechangeProduct_ChangeScene_m7EBD7B58B2D6E6CF5894956DE0E26062E94FAFD9,
	ScenechangeProduct_Exit_m7F955F33026B573F9D2A43777CFBCDDD095345CB,
	ScenechangeProduct__ctor_m58B7161E686AD417F35688F2DD03F82B0AE15D9D,
	TextureManager_Awake_m138AFFD0D12AFCA0FB33B93CB1774401EDAC1E1C,
	TextureManager_DestroyTextureOnReceived_m8AB3E19012E7C088BC325B995DD1969627B4697C,
	TextureManager_texHandle_m439B211B1DEF03FA31002E8E564A3DF8BF6B0049,
	TextureManager__ctor_m13A2832651A1209C03473A832C950382D6E13B6B,
	U3CtexHandleU3Ed__4__ctor_m78A498D373C0F1D47DF31B65F65830A922CCAA04,
	U3CtexHandleU3Ed__4_System_IDisposable_Dispose_mBC6A134D53DCB893C0332E66F377E631EC743E83,
	U3CtexHandleU3Ed__4_MoveNext_m30C6B218F37F5CCC606487CCD8493ED40678B6FA,
	U3CtexHandleU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61A1F42F4519A4336BDE57BD8AF01BDB6A3EAD09,
	U3CtexHandleU3Ed__4_System_Collections_IEnumerator_Reset_m5A9853940637A064502F5F4897D24B489826CBD1,
	U3CtexHandleU3Ed__4_System_Collections_IEnumerator_get_Current_m8473DEAF348680FBE1E0506F50C6FBCD80D6AB7E,
	ToggleNavscroll_Start_m4B70016B5078B069595B196605124CAD5DD9CD8B,
	ToggleNavscroll_Update_mBB2B3D05594DB0F354A01E449CF6F2591FF5A1BE,
	ToggleNavscroll_SliderValueUnitChanged_mDC94EE14C9040F63643B8A4CF124291E2CF81687,
	ToggleNavscroll_SliderValueGarmsChanged_m9DE84AF4273CBD260497911C32FC72CB14B341BB,
	ToggleNavscroll__ctor_mA4725E55E08D156BDA0E6767D26ECFD2F78EA28A,
	FilterPanel_Awake_m9DEA31E67384D6308A58310ECF32E6B34CE75893,
	FilterPanel_Start_mF768AAE3006D20FAB068864166B723BA64845E21,
	FilterPanel_OnDisable_m9C36B4FC7746807E54C4ABBBA3ABB6A6947F8B62,
	FilterPanel_Update_mFE760ADC827264ECEF30E3F8F5ABDC5B1DE459C4,
	FilterPanel_OnBeginDrag_mE3E593FE68E710849547E64B7F3D24239234761B,
	FilterPanel_OnDrag_mD41C76416DEFD98D15ADBD27036AD2126D616564,
	FilterPanel_OnEndDrag_m42043EB6FB099EC59476B79B6218E2ED2BA3869C,
	FilterPanel_BackgroundTap_m87B2BB83F18B3795A95FD0C8EAEF1DF341AFB9AC,
	FilterPanel_Open_m15BC74945C46E1C553F17E7BDD0DAF3B7402D5C8,
	FilterPanel_Close_m08A076793232AC2FCC76855A5408473A21EBC0E7,
	FilterPanel_QuintOut_m54EE377566DF16E3E956B8019C7639F7005FFF3A,
	FilterPanel_RefreshBackgroundSize_m41191343BA2532F60F5F25E0D7453B4F541EE467,
	FilterPanel_QuintOut_m3587ACE048C39E1BAA08901C095712793F9EAC53,
	FilterPanel__ctor_m7264600D753412C082348227A837D96C7B608AE0,
	NavDrawerPanel_Awake_m142F9D46280D952C31513C6D85033C5B56EF1B4B,
	NavDrawerPanel_Start_mBFC84C19E44C531FCCAF123CD90C8E433DCD458E,
	NavDrawerPanel_OnDisable_m580657011A7CFCE920CF4B0DEE10DCFCE4831825,
	NavDrawerPanel_Update_mEE1F1C289B8B12CDBBC7BB3AD7D049B8612D3F18,
	NavDrawerPanel_OnBeginDrag_mE587FF4D91AC4B2E05DBFAFA78ECEBC1C5E0AA0E,
	NavDrawerPanel_OnDrag_m572E807F063DAC152E9675D9A8E76EEA6FE2A395,
	NavDrawerPanel_OnEndDrag_m59621F62C24910CFF50241C6BA929B1E16BFC3AC,
	NavDrawerPanel_BackgroundTap_m3FE807F59D23ECC7BABC8D922D889523B073085C,
	NavDrawerPanel_Open_mD120F533906337EC51949E841E640C71905FBC91,
	NavDrawerPanel_Close_m55CF6A7333896CEE7768546DD40906857FB5688F,
	NavDrawerPanel_QuintOut_m5E9C6B16408B098B7CF49752593C21A66DD8A287,
	NavDrawerPanel_RefreshBackgroundSize_m6C20CF27428601A3D83B7DB8CF591EAFFB557211,
	NavDrawerPanel_QuintOut_m33B001BD252F490CBEF984FB435A7F40CF36D42C,
	NavDrawerPanel__ctor_mA0954D43D05F7BA7A2BA673D86050791EB984B58,
	AutoExpandGridLayoutGroup_get_startCorner_m46D6D0F268B8B31FB6F0EA597D4A10095737F9A5,
	AutoExpandGridLayoutGroup_set_startCorner_mEC3FFE515D144056CD8FD3886E23009D2D15B575,
	AutoExpandGridLayoutGroup_get_startAxis_mE1B2B8087318C6776BC89D69D88A9A9FF94DCF04,
	AutoExpandGridLayoutGroup_set_startAxis_mA139CE21A6900EE7161500314DC08B8C84B2EBE5,
	AutoExpandGridLayoutGroup_get_cellSize_m2788A6663ED4EF152CEF9046D551B6B2F1623973,
	AutoExpandGridLayoutGroup_set_cellSize_mC139347895A5DE01376B751F480B9F02A215BC6B,
	AutoExpandGridLayoutGroup_get_spacing_m0A1D996BB7D1E4CBF52E70D2B70D531A6EABB39D,
	AutoExpandGridLayoutGroup_set_spacing_mA9AECC549CCF3839154323BB74F4112A466FC90D,
	AutoExpandGridLayoutGroup_get_constraint_m28D8766421877EBFD3A6EBB5202BA0FF1F8FF870,
	AutoExpandGridLayoutGroup_set_constraint_m767D1DEE4CF14E5C66CC89634546EA9EA3337E23,
	AutoExpandGridLayoutGroup_get_constraintCount_m98B8F125C072A2CD82FAA5917BA49BCE4523AAF2,
	AutoExpandGridLayoutGroup_set_constraintCount_mE168C2F30AAA6178FE483B6AAE459BD5E01A72AF,
	AutoExpandGridLayoutGroup__ctor_mCA3A99E5905C5C7CE96F808296306538EC4180C2,
	AutoExpandGridLayoutGroup_CalculateLayoutInputHorizontal_m352D680BD0153EF702DE99D39ADDF3CD89B439EA,
	AutoExpandGridLayoutGroup_CalculateLayoutInputVertical_m748DE8A07A1B08136F208F60EA972A3A35740DFF,
	AutoExpandGridLayoutGroup_SetLayoutHorizontal_mF8E221D3640998C160D65C7A8E2F64C3FA9420FE,
	AutoExpandGridLayoutGroup_SetLayoutVertical_m6039ED454960261B750BAF5FA267769566E3A68A,
	AutoExpandGridLayoutGroup_SetCellsAlongAxis_m0598864CE36606EB056680242018598168DB7E17,
	ObiCharacter_Start_m9AF9959BA6BB669EC44CC6A19F2C4921A8D3EC12,
	ObiCharacter_Move_mE36529E342431BC2E3F82ED9221503AF7F957934,
	ObiCharacter_ScaleCapsuleForCrouching_mBEE0D314BF3D424DEC1B2D4249AC6F1F1B577A51,
	ObiCharacter_PreventStandingInLowHeadroom_mE8D5660997F99D5B260E7CCC9BF758FFF4FDFEDD,
	ObiCharacter_UpdateAnimator_m0133BC89517D199811A84D3308D99A39508A6163,
	ObiCharacter_HandleAirborneMovement_m2237FCB54D63F3C66E8DC444AE9485868A07E604,
	ObiCharacter_HandleGroundedMovement_m665A36CA6A0EAD5D0C62AA11737F2A6308EEEDC6,
	ObiCharacter_ApplyExtraTurnRotation_m0D9CC7D6882FDB7888330B062F69EE0FA0667BF5,
	ObiCharacter_OnAnimatorMove_mE0C7A78FB55C063DF34E2BC3FF6F84125B63AB6A,
	ObiCharacter_CheckGroundStatus_m21B58AEB9AB71B4D7885687B1C5979A9CB9CF45E,
	ObiCharacter__ctor_m16255C98358F19058CDB9991C96F04FE19EA7A93,
	SampleCharacterController_Start_mE5EAF1421DBAD9FFAC2D550E04332581CDFFE4BC,
	SampleCharacterController_FixedUpdate_m1AC87DDCCC039856B18C977E02E0CD2D80F1A524,
	SampleCharacterController__ctor_m8605568614106AD773DE856FE8AC1968C23A4262,
	ColorFromPhase_Awake_m8EE051CBD4CAF6406768C2D6941DCABCD6CFD2B9,
	ColorFromPhase_LateUpdate_m56F07E6A796032EB6DD06CE10C57090983DAA1AB,
	ColorFromPhase__ctor_mECC2D22AC69AC7C21BC64BF585AB14C65E7A2415,
	ColorFromVelocity_Awake_mBA50845A66FAE55EB4F4637C1BF190EE329FAEB5,
	ColorFromVelocity_OnEnable_m60CE9B091B8725925F60E867BE08F1F137E2AB9D,
	ColorFromVelocity_LateUpdate_m4A026CE0BCACBCA70F219D29A3EEFA7EDE33C2A3,
	ColorFromVelocity__ctor_mD26A1B2F148FC8BF6F57EDEA8179E0B2D8CD8FFC,
	ColorRandomizer_Start_mF174C12B77903483554BBCE5D4A4D82118DD4651,
	ColorRandomizer__ctor_mF1141F2D29657D595C863D186B46C6B3CD672A85,
	LookAroundCamera_Awake_mE90B4191F1288756CA34EEC0C21C1998251B0E8E,
	LookAroundCamera_LookAt_mEF29B91315C1AD0156E505C73EF9931CDD6E7718,
	LookAroundCamera_UpdateShot_m8C764716D01D701607A13430043C9E5B87A50597,
	LookAroundCamera_LateUpdate_m18FD061FDD268DE063DEC79BAD72F08432F1A7A8,
	LookAroundCamera__ctor_m52D5B9733D32E65A768D29868D4B07EB63988D12,
	CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179,
	MoveAndRotate_Start_m81492AAA391819DF463B1781F3F647B8F50E8E1D,
	MoveAndRotate_FixedUpdate_mC9020498CC29B200215037EB0EA7508367CCD8BD,
	MoveAndRotate__ctor_mC46EA5D888532267ADC051A68F468CD7FD5D2130,
	Vector3andSpace__ctor_m5653715EE0EAB2271B055D2729CCF8046D2CE78B,
	CreateObiComponents_Start_mB30E01E05FEFA7BA1C6C6AEE8186A1CD2FB5A577,
	CreateObiComponents_CreateComponents_m819F1F20C2601A5E5C8AAFCAEA2FAD334F569908,
	CreateObiComponents__ctor_m7C6D26F25B3FE39A9B4269783607CF9F28029A80,
	LoadAssetBundle_Start_m105029648FC51AA46E2286E4F24DD78186AFB8C1,
	LoadAssetBundle__ctor_m30813C43DD742266B3F74AE6BE4307BAD268781A,
	PartialObi_add_OnObiAdded_m5F1F706772F4FEB073C6EC4657E856AC8F46924F,
	PartialObi_remove_OnObiAdded_m47896DA4AC0060E8BA14323E36446E7DCC56A7C3,
	PartialObi_Awake_mA104E99467750FB09279C3714E3A309D8B14C95B,
	PartialObi_AttachObi_mA2D9B03ED5F9CAEDD8C47C859101CAF89F2CF059,
	PartialObi_LoadParamImg_m4C043D2912C9718F6C849C91898E28B0D0EAA184,
	PartialObi_duplicateTexture_mAFE5A422AB2CA8A0F5312FB0405F4F8FA6A39874,
	PartialObi_ReadObiBlueprint_mD1D999630BD5DD7695324E6EB3E55B18605DF324,
	PartialObi_CreateObi_m7B19FEF5D807AC60C14A480B266EC3FA4571E603,
	PartialObi__ctor_m34B2CB15C30118F75A67EB15FF3624303185EF9E,
	ObiAdded__ctor_mE4D67E0C5E2CA65A2A3F9A64C83858FD8DA81C40,
	ObiAdded_Invoke_mCAC30857B95B90A2EAD243A2B66E8066CE60AF38,
	ObiAdded_BeginInvoke_m605DB4E3C85ADB53055741F355481010A855AD96,
	ObiAdded_EndInvoke_m5D67F76C844AAD6FA5656AA61BC175AE4933479C,
	U3CAttachObiU3Ed__12__ctor_m323E149194970DEB7C74D42B50BCD92DDFA10E58,
	U3CAttachObiU3Ed__12_System_IDisposable_Dispose_m606C4A0C39F85E47234D008EA5C15466EE21E6B9,
	U3CAttachObiU3Ed__12_MoveNext_mD96F9D75DC8B4705327562DEF0AC62DB11D072F1,
	U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9F06E12394352EAA6288915A8E1A9FD2A9020E9,
	U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_m7C8BFAF6A415731CB8B4DF5702063CF293F51321,
	U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mB54B79F9A99CD527CA0F69AA8F4D51E10E398815,
	U3CCreateObiU3Ed__16__ctor_m62416639B5CB564A96F41F34AFF4479F4EC0F737,
	U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m20CA1D1DC9156B9D0BA1C9B45D02B4276BD0227B,
	U3CCreateObiU3Ed__16_MoveNext_m7C4447AD6A6A0AA0E69502745AB918001B3212B0,
	U3CCreateObiU3Ed__16_U3CU3Em__Finally1_mD641B50A2E250A3068F4F1D685DB5FCC77D41E2B,
	U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1153171D17D5A85E9D1089961F56E8C35031B60E,
	U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDECD8FBF73AC546C48D6663310A224A229D2A20E,
	U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m20F4A3067E9EE93EBE98BBB16E3A8C3BED678928,
	SetUpObi_add_OnObiAdded_m21C4DFFB388835AAF49AD553672C78EF8083F487,
	SetUpObi_remove_OnObiAdded_mD59D6B40F02F799BD8BACCE38AD4BC2D703FF9D7,
	SetUpObi_Awake_m5D93E087280265178A9FF13232EFE89C431DADAD,
	SetUpObi_AttachObi_mDEA5E44A4A4BD87B69355AC66EC89C5E98AF6099,
	SetUpObi_LoadParamImg_m9909A680D7CCE7335CE8666AA9F3D7846A6E2C77,
	SetUpObi_duplicateTexture_mA1C0E528DE63E3E92162504AE5AD53EA8D7C70B9,
	SetUpObi_ReadObiBlueprint_m9D40CA80D5A17E67D07AB676B09936C5EE274B75,
	SetUpObi_CreateObi_m7C3477A5C3F0BB4E8FB587E629AC7F3D5E072425,
	SetUpObi__ctor_m30344D85BBCF01378960D02915525AF8C275054D,
	ObiAdded__ctor_m701E89814A33CA5F5BE7E9411DDD5FD4386DDB8D,
	ObiAdded_Invoke_mC429DB252994488CD5076D289BB53EB5746C49C6,
	ObiAdded_BeginInvoke_m87B55DAB46FC383D454FB03FC3DCEADA873C78C3,
	ObiAdded_EndInvoke_mE333A9CF196776BD8ED733669CC4E3114CAAA3FA,
	U3CAttachObiU3Ed__12__ctor_mB9723999E31B7F5F4B26C52846EE0EF7F89C03D8,
	U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mEB86C2EEC5E55A40B077EAE0B7CD5291FBBE4FDB,
	U3CAttachObiU3Ed__12_MoveNext_m4329210E28D2ABB6E013515414F1BF9DB5B870F0,
	U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m124A441CC082D84EB1C7E8C4D09DD36E4BAA1DC4,
	U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mF88ACCD9C8DB2BE87221C306721BE2C8E795FD47,
	U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_mFDF31BB57526DB3E034C630C43113FEBAE74DA2C,
	U3CCreateObiU3Ed__16__ctor_m484B3307000BF1D629371B445312F411B8975D51,
	U3CCreateObiU3Ed__16_System_IDisposable_Dispose_m884A0B4D81853BF2D53AA1F505BE7B082C5B492E,
	U3CCreateObiU3Ed__16_MoveNext_m56DC26122EAF57C1DB79429DAC6017DE9A4871BA,
	U3CCreateObiU3Ed__16_U3CU3Em__Finally1_m7F08DE8D47D1DE526A71C0A416C945CA4B06A59B,
	U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BFCD23E4DD867743952EEC697B33C6095E83FCB,
	U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mD9BB880CE88988B91F124368A497E88A3A05AE21,
	U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m6C0A0740DA6B6C547A0784E83EE609B703F35B15,
	StitchController_Start_m8482751452DA75C5ED096FCD44A43C40FE8433BF,
	StitchController_FastApproximately_m1219C630F18967C408B77FC08F724992EEBEA05D,
	StitchController_StitchPairs_m8E32FA7C7CD918A60CD7A145389EAA96B9C54D5C,
	StitchController__ctor_mEC86ED3793A97122797850E96417ACF769806D5F,
	UIController_Start_m97BEE4041FBDC22BEF794F04D4E86829133E9EB1,
	UIController_OpenDemoScene_m850F0C18FBBDDAC05A764AE735FE11E20C6EEE9F,
	UIController_ResetTracking_mFA9E3CDD16C75D98DF177166D8890A6C69F687AE,
	UIController_ShowGarment_mAAF5DC9E3234DD2CC0AC3028CB38EF2EE8F2B4C7,
	UIController_StartBodyTracking_mB5FBB5B789AC591BEE67379C313B08C96B65AA0F,
	UIController_ReturnToProduct_m4D1F7C0D2B18CBF63F16D5E8998A676A1BC05D5E,
	UIController_ResetScene_mF9DE3599912B251EF247731B2B65CFAD66FFD26D,
	UIController__ctor_mECCAE8B658781598BF9F78855801DA15FFD7CA24,
	U3CStartBodyTrackingU3Ed__11__ctor_m7D47E8B6DA6315A081D9E41F1EACCA21631D6B63,
	U3CStartBodyTrackingU3Ed__11_System_IDisposable_Dispose_m0E93CE83F9A57AE50F030818B6979783C7514008,
	U3CStartBodyTrackingU3Ed__11_MoveNext_mCCB924FAC1983E7435E1AC28AAC53C8C383143C2,
	U3CStartBodyTrackingU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94FE82124B37C2D7ECD1A80707193342445FF4E9,
	U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_Reset_mF038F71D5EF0A73A63C671C81B716DE08772EC01,
	U3CStartBodyTrackingU3Ed__11_System_Collections_IEnumerator_get_Current_mCAF09EA58826F4E0C4B7679D5070E4FE354EDF97,
	WpiObiNoBlueprint_Start_mD6F6190E00C3014C8A4753D3BD30060A05EA1260,
	WpiObiNoBlueprint_CreateObi_m81599B62AE6C534CFBE93954E065DB7915AB8D32,
	WpiObiNoBlueprint__ctor_m96F210A77C0F84446458BC6095B5DC8751F10D18,
	U3CCreateObiU3Ed__4__ctor_m6B7FE0270BDD042C4CF7FEE7E23DA518AFCD26BD,
	U3CCreateObiU3Ed__4_System_IDisposable_Dispose_mF66738AD01D56578BBD2552D07182E3D03750B72,
	U3CCreateObiU3Ed__4_MoveNext_m1E7B59A185BD763CA55093BBE12B63219A6DD5D3,
	U3CCreateObiU3Ed__4_U3CU3Em__Finally1_m4597905C4F2FF129447BC7DC1AA0359996771D61,
	U3CCreateObiU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA5AC2783D38C1D425E7FC54C3BA6D510C3F88713,
	U3CCreateObiU3Ed__4_System_Collections_IEnumerator_Reset_mD360873099FC44F0F1ED6DEFC12B6BB0A78DD40A,
	U3CCreateObiU3Ed__4_System_Collections_IEnumerator_get_Current_mBD9F9895E725E1403A8F438CA60098BC62E5B8D7,
	setupsingle_add_OnObiAdded_mF7DD3C8BDBB033D6743D82515CFB9EC100D5315F,
	setupsingle_remove_OnObiAdded_mA052507D0A9DCAAE4F0CFAC9737E7BD2E5B3EA2A,
	setupsingle_Awake_m2F770577B5CD70B5D8817785BED47DA9D16A0391,
	setupsingle_AttachObi_mB362DE10565BBEF3BC97834455F2389F77372CBC,
	setupsingle_LoadParamImg_mAB04812AB94D3BAF467FAF80EDBD7FAFC161E9A6,
	setupsingle_duplicateTexture_mCC15C8482FB793AA2ED4D681FE188A9988BFA654,
	setupsingle_ReadObiBlueprint_mD984FCC608833B7938D43C384D8463B94C6AE532,
	setupsingle_CreateObi_m3ED0DF41000191CB095295ED8735804C08B040FF,
	setupsingle__ctor_m82862E0D77620903AA2AD71E08E4D1503B08EF25,
	ObiAdded__ctor_mAD4D9F7C90A3E40795A330EB103E80DD5C9EB96F,
	ObiAdded_Invoke_m3F69C91AD92E885602B35B16E5BA391B5073F2F8,
	ObiAdded_BeginInvoke_m1CB32D8C62387BE7FB364148617871BB52E641E8,
	ObiAdded_EndInvoke_m8537B9566CD0DEAE791714098B81A8AD095511F2,
	U3CAttachObiU3Ed__12__ctor_m54F90B5E1243762481D29EC8164CB32CA8BFC633,
	U3CAttachObiU3Ed__12_System_IDisposable_Dispose_mFF2C2A49D0534860E17ADBCABCD20753A4FB0C90,
	U3CAttachObiU3Ed__12_MoveNext_m88317582EAB0AEB2C048D023AB82B8BCAE142019,
	U3CAttachObiU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE0DF70F72243D4FF5714E4EFDC1FAA0ABE2CE5E6,
	U3CAttachObiU3Ed__12_System_Collections_IEnumerator_Reset_mDCFE47940ECB0E6F4067A64FF563DCC8FF30CD52,
	U3CAttachObiU3Ed__12_System_Collections_IEnumerator_get_Current_m6E8969EF8A777A33E45811966F96C714B26D25D4,
	U3CCreateObiU3Ed__16__ctor_m122099FC8C75500A0683EC9FC6C4C74DB6309A07,
	U3CCreateObiU3Ed__16_System_IDisposable_Dispose_mC60A3B6541E7D65C9CEDC54A79114FAC9CED9FAC,
	U3CCreateObiU3Ed__16_MoveNext_m371F273301ABC6704E292A415A54029F091C754D,
	U3CCreateObiU3Ed__16_U3CU3Em__Finally1_m5790B4F4732FBC9B264C3205E266CB0DA3DE28AC,
	U3CCreateObiU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA77337728FC469EBCAE52E998D106954DF94E715,
	U3CCreateObiU3Ed__16_System_Collections_IEnumerator_Reset_mDFE053E3602BE312CEF0388C87F276ED43D09CC3,
	U3CCreateObiU3Ed__16_System_Collections_IEnumerator_get_Current_m793AE57000E8558A92A7B3C3869D377791749309,
};
extern void CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x060004BD, CameraShot__ctor_mD9736DEA6A3F14DF47920666D00E1ABA5899E179_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1328] = 
{
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3021,
	3780,
	3780,
	3038,
	6299,
	3780,
	3780,
	3780,
	3780,
	6278,
	2196,
	2199,
	2196,
	2196,
	2196,
	2196,
	2196,
	2196,
	2196,
	2196,
	2196,
	2198,
	2196,
	2196,
	2198,
	3780,
	3723,
	3780,
	3780,
	2196,
	6113,
	3038,
	3780,
	3780,
	6212,
	6299,
	3780,
	6299,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3723,
	2196,
	2196,
	2196,
	3723,
	3780,
	1614,
	3780,
	1103,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3780,
	3780,
	4957,
	582,
	3780,
	1614,
	3780,
	1103,
	3038,
	3780,
	2196,
	3723,
	3780,
	3723,
	3780,
	3780,
	1103,
	3723,
	3780,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3021,
	3780,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3038,
	3749,
	3038,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	2196,
	2196,
	2196,
	3780,
	1614,
	3780,
	1103,
	3038,
	3780,
	3780,
	3723,
	3038,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	1662,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3723,
	3723,
	3780,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	1623,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	1617,
	3780,
	3780,
	3780,
	3780,
	1617,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	1662,
	3780,
	3753,
	3753,
	3753,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	1617,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3060,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	889,
	1621,
	3780,
	3780,
	3780,
	3780,
	1091,
	3723,
	1103,
	3723,
	3780,
	3723,
	3723,
	3749,
	2199,
	2192,
	1094,
	1091,
	3780,
	2196,
	3780,
	3723,
	3780,
	3084,
	3723,
	3723,
	3038,
	3060,
	706,
	2196,
	702,
	3723,
	1502,
	260,
	85,
	3780,
	1617,
	431,
	580,
	1103,
	3723,
	580,
	3780,
	3038,
	2708,
	3780,
	6299,
	3021,
	3062,
	3021,
	3021,
	3780,
	3021,
	3780,
	3780,
	3780,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3780,
	3749,
	1614,
	3038,
	707,
	3038,
	1614,
	1612,
	424,
	3038,
	1614,
	3780,
	1103,
	3038,
	1614,
	3780,
	1103,
	3038,
	6299,
	3780,
	3021,
	3749,
	3038,
	3038,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3780,
	3038,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	2570,
	3780,
	3780,
	3780,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3780,
	3780,
	3038,
	1617,
	3780,
	3780,
	3780,
	3780,
	3780,
	1091,
	3723,
	1103,
	3723,
	3780,
	3723,
	3723,
	3749,
	1091,
	1091,
	3021,
	3780,
	2196,
	3780,
	3723,
	3780,
	3084,
	3723,
	3723,
	1617,
	3038,
	3060,
	706,
	2196,
	702,
	3723,
	1502,
	3060,
	707,
	579,
	85,
	3780,
	1617,
	888,
	1103,
	3723,
	888,
	3780,
	3038,
	2708,
	3780,
	6299,
	3021,
	3780,
	3780,
	3021,
	3021,
	3780,
	3021,
	3780,
	3780,
	3780,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3749,
	3780,
	3749,
	1614,
	3038,
	707,
	3038,
	1614,
	1612,
	424,
	3038,
	1614,
	3780,
	1103,
	3038,
	1614,
	3780,
	1103,
	3038,
	1614,
	3780,
	1103,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3749,
	6299,
	3780,
	3038,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3780,
	3038,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3780,
	3780,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3723,
	3723,
	3723,
	3780,
	2196,
	3038,
	3780,
	3780,
	3780,
	3780,
	3749,
	3749,
	3038,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3780,
	2196,
	3749,
	3780,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	6299,
	3780,
	3038,
	3038,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3723,
	3038,
	3780,
	3038,
	3038,
	3038,
	2075,
	3780,
	5972,
	3780,
	3780,
	3780,
	3038,
	3038,
	3038,
	3021,
	3021,
	3021,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3723,
	3780,
	3780,
	3021,
	3021,
	3780,
	3780,
	3749,
	6299,
	3780,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3780,
	3021,
	3780,
	3780,
	3780,
	1612,
	3780,
	3723,
	881,
	3038,
	693,
	3780,
	1614,
	85,
	19,
	3038,
	1614,
	550,
	110,
	3038,
	3780,
	3038,
	3749,
	3038,
	3749,
	3038,
	3780,
	3749,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3780,
	3038,
	3749,
	3038,
	3749,
	3038,
	3038,
	3038,
	3038,
	3780,
	3749,
	3780,
	3749,
	3780,
	3749,
	3780,
	3749,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3780,
	3780,
	3038,
	2196,
	3780,
	3780,
	1614,
	3038,
	707,
	3038,
	3780,
	3038,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3723,
	3780,
	3723,
	3780,
	1617,
	3780,
	3780,
	3780,
	3780,
	1103,
	117,
	3780,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3749,
	3780,
	3749,
	3749,
	3749,
	3749,
	6299,
	3780,
	3038,
	3038,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3021,
	3021,
	1617,
	1103,
	3038,
	2708,
	3780,
	6299,
	3780,
	3780,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3723,
	3038,
	3780,
	3780,
	3723,
	2196,
	2937,
	3780,
	3038,
	6299,
	3780,
	3749,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3723,
	3038,
	3780,
	3780,
	3723,
	2937,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3723,
	3038,
	3780,
	3780,
	3723,
	3780,
	3723,
	1103,
	3723,
	2196,
	1103,
	2937,
	3780,
	3038,
	3780,
	3021,
	3780,
	3038,
	3749,
	3038,
	3749,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	6299,
	3780,
	3780,
	3780,
	3780,
	1614,
	550,
	110,
	3038,
	1614,
	3038,
	707,
	3038,
	3780,
	3780,
	3780,
	1614,
	3038,
	707,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3723,
	3780,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3038,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	6299,
	3723,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3780,
	3062,
	3062,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	3038,
	3780,
	3780,
	3780,
	493,
	3780,
	495,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3038,
	3038,
	3780,
	3780,
	3780,
	493,
	3780,
	495,
	3780,
	3705,
	3021,
	3705,
	3021,
	3774,
	3084,
	3774,
	3084,
	3705,
	3021,
	3705,
	3021,
	3780,
	3780,
	3780,
	3780,
	3780,
	3021,
	3780,
	935,
	3060,
	3780,
	3086,
	3780,
	1643,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	3780,
	1664,
	3780,
	3780,
	3780,
	613,
	3780,
	3780,
	3780,
	3780,
	3780,
	3038,
	3780,
	3780,
	3780,
	6212,
	6212,
	3780,
	3723,
	2196,
	2196,
	2196,
	3723,
	3780,
	1614,
	3780,
	1103,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	6212,
	6212,
	3780,
	3723,
	2196,
	2196,
	2196,
	3723,
	3780,
	1614,
	3780,
	1103,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	3780,
	4957,
	888,
	3780,
	3780,
	3780,
	3780,
	3780,
	3723,
	3780,
	3780,
	3780,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3780,
	3723,
	3780,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
	6212,
	6212,
	3780,
	3723,
	2196,
	2196,
	2196,
	3723,
	3780,
	1614,
	3780,
	1103,
	3038,
	3021,
	3780,
	3749,
	3723,
	3780,
	3723,
	3021,
	3780,
	3749,
	3780,
	3723,
	3780,
	3723,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1328,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
