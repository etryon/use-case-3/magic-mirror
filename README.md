# Magic Mirror App

This repository stores the Magic Mirror App developed for Use Case 3 of the eTryOn project.

## Development

In order to execute the code, the Obi physics library needs to be imported in the project, as well as the google plist file for the firebase and the firebase storage location URLs. The credentials for the recommendation function are also required. The code can be then executed from the LogInScene.

## Build

The application needs to be built for *iOS* in order to run properly. For that, XCode must be installed and proper Apple Developer identifiers included, for signing the app's certificate. A physical device with ARkit capabilities is required to take advantage of the application's AR capabilities.
