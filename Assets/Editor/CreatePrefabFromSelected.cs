using UnityEditor;
using UnityEngine;

/// <summary>
/// Creates a prefab from a selected game object.
/// </summary>
class CreatePrefabFromSelected
{
	const string menuName = "GameObject/Create Prefab From Selected";
	
	/// <summary>
	/// Adds a menu named "Create Prefab From Selected" to the GameObject menu.
	/// </summary>
	[MenuItem(menuName)]
	static void CreatePrefabMenu ()
	{
		var go = Selection.activeGameObject;
		var prefab = EditorUtility.CreateEmptyPrefab("Assets/" + go.name + ".prefab");
		EditorUtility.ReplacePrefab(go, prefab);
		AssetDatabase.Refresh();
	}
	
	/// <summary>
	/// Validates the menu.
	
	/// </summary>
	/// <returns>True if the menu item is valid.</returns>
	[MenuItem(menuName, true)]
	static bool ValidateCreatePrefabMenu ()
	{
		return Selection.activeGameObject != null;
	}
}
