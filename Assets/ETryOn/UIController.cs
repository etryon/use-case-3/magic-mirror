using Obi;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using System.Collections;


namespace ETryOn
{
    public class UIController : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("GameObject with the Garment armature inside")]
        private Transform garment;

        private bool _isShowingGarment;
        private bool _isSimulating;
        private bool _isBodyTracking;
        private bool _isAnimating;
        private GameObject skeleton;


        private ARHumanBodyManager _arHumanBodyManager;

        private void Start()
        {
            //_isAnimating = true;
            _arHumanBodyManager = FindObjectOfType<ARHumanBodyManager>();

            if (_arHumanBodyManager)
            {
                _isBodyTracking = false;
                _arHumanBodyManager.SetTrackablesActive(_isBodyTracking);
                _arHumanBodyManager.enabled = _isBodyTracking;
                StartCoroutine(StartBodyTracking());
            }

        }
        public void OpenDemoScene()
        {
            SceneManager.LoadScene("Demo_v2", LoadSceneMode.Single);
        }
        
        public void ResetTracking()
        {
            StopCoroutine(StartBodyTracking());
            _isBodyTracking = !_isBodyTracking;
            _arHumanBodyManager.SetTrackablesActive(_isBodyTracking);
            _arHumanBodyManager.enabled = _isBodyTracking;
            StartCoroutine(StartBodyTracking());

        }

        public void ShowGarment()
        {
            _isShowingGarment = !_isShowingGarment;
            foreach (Transform garmentPart in garment)
            {
                Renderer garmentRenderer = garmentPart.GetComponent<Renderer>();
                if (garmentRenderer)
                {
                    garmentRenderer.enabled = _isShowingGarment;
                }
            }
        }
        
        IEnumerator StartBodyTracking()
        {
            yield return new WaitForSeconds(1);
            //Destroy(garment.gameObject);

            _isBodyTracking = !_isBodyTracking;
            _arHumanBodyManager.SetTrackablesActive(_isBodyTracking);
            _arHumanBodyManager.enabled = _isBodyTracking;
            
        }

        public void ReturnToProduct()
        {
            
            SceneManager.LoadScene("ProductScene");
            
        }

        public void ResetScene()
        {
            AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);


        }
    }
}