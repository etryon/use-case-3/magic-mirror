using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;
using System.Threading.Tasks;

using UnityEngine.Networking;
using System.Collections;
using Obi;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;
using Firebase.Storage;

using UnityEngine.UI;

public class SpawnObject : MonoBehaviour
{
    private Transform oTransform;
    GameObject spawnedObject;
    ARSessionOrigin m_SessionOrigin;
    public GameObject arCam;
    Material matobj;
    public GameObject left;
    public GameObject right;
    AssetBundle bundle;
    public GameObject plus;
    public GameObject minus;
    public Button cycle;
    
    float distance = 2;
    private Button leftbut, rightbut, plusbut, minusbut;
    Vector3 cameraPoint;
    GameObject hbtObj;        
    Vector3 pos;
    public TMPro.TMP_Text textTitle,colorText;
    public FirebaseAuth auth;
    public GameObject loadingScr;
    Dictionary<string, object> userSnap = new Dictionary<string, object>();
    private List<string> colsList = new List<string>();
    
    string itemcol;
    
    
    bool contentPlaced, canCycle;
    private int start, next;
    private string uSize, uGender;
    private string groupGarm, genderGarm;
    public TMPro.TMP_Dropdown sizeDropdown;
    private string s;
    private string sShown = "Size";
    private int curDrp = -1;
    Dictionary<string, GameObject> garmDict = new Dictionary<string, GameObject>();



    List<string> sizeOptionsF = new List<string> { "Size","XS", "S", "M", "L", "XL"};
    Dictionary<int, string> sizeOptsF = new Dictionary<int, string>{ { 0, "m" } ,{ 1, "xs" } , { 2, "s" }, { 3, "m" }, { 4, "l" }, { 5, "xl" }};
     List<string> sizeOptionsM = new List<string> { "Size", "S", "M", "L", "XL","XXL"};
    Dictionary<int, string> sizeOptsM = new Dictionary<int, string>{ { 0, "m" } , { 1, "s" }, { 2, "m" }, { 3, "l" }, { 4, "xl" } ,{ 5, "xxl" }};
    List<string> options;
    void Awake()
    {
        
        loadingScr.SetActive(true);
        StartCoroutine(AvailableColors());
        
        textTitle.text = MainManager.Instance.title;
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;

        StorageReference storageRef =
        storage.GetReferenceFromUrl("");
        
        storageRef.Child($"{MainManager.Instance.fullPath}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
        if (!task.IsFaulted && !task.IsCanceled) 
        {
            Debug.Log("Download URL: " + task.Result);
            StartCoroutine(GetAssetBundleExtra(Convert.ToString(task.Result), MainManager.Instance.fullPath));
        }
        
        else
        {
            loadingScr.SetActive(false);
            Debug.Log(task.Exception);
        }
        
        
        });
        oTransform = transform;
        m_SessionOrigin = GetComponent<ARSessionOrigin>();
        contentPlaced = false;
        
        leftbut = left.GetComponent<Button>();
        rightbut = right.GetComponent<Button>();

        plusbut = plus.GetComponent<Button>();
        minusbut = minus.GetComponent<Button>();

        leftbut.onClick.AddListener(() =>
        {
            if (spawnedObject)
            {
                
                
                

                
                
                

                Quaternion newpose = spawnedObject.transform.rotation;
                newpose = newpose * Quaternion.Euler(0, 45,0);
                spawnedObject.transform.rotation = newpose;
            }
        });

        rightbut.onClick.AddListener(() =>
        {
            if (spawnedObject)
            {
                Quaternion newpose = spawnedObject.transform.rotation;
                newpose = newpose * Quaternion.Euler(0, -45,0);
                spawnedObject.transform.rotation = newpose;
            }
        });

        
        plusbut.onClick.AddListener(() =>
        {
            if (distance > 0.5f)
                distance = distance -0.25f;
        });

        minusbut.onClick.AddListener(() =>
        {
            if (distance < 4f)
                distance = distance + 0.25f;
        });

        cycle.onClick.AddListener(() =>
        {

            
            if (canCycle == true && contentPlaced == true)
            {
                canCycle = false;
                StartCoroutine(RespawnColor());
            }
                    
        });

        sizeDropdown.onValueChanged.AddListener((int arg0)=>
        {
            
            
            if (canCycle == true && contentPlaced == true)
            {
                canCycle = false;
                if (genderGarm == "m")
                {
                    s = sizeOptsM[arg0];
                    sShown = sizeOptionsM[arg0];
                }
                else if (genderGarm == "w")
                {
                    s = sizeOptsF[arg0];
                    sShown = sizeOptionsF[arg0];
                }
                StartCoroutine(RespawnSize(s));
            }
            
                    
        });
        

    }

    IEnumerator RespawnSize(string s)
    {
        
        
        canCycle = false;
        sizeDropdown.ClearOptions();

        if(start + next > colsList.Count - 1)
        {
            start = 0;
            next = 0;
            
        }
        
        uSize = s.ToLower();
        
        string urlCol = $"{genderGarm.ToLower()}_{s.ToLower()}_{groupGarm}_{colsList[start + next]}";

        Destroy(spawnedObject);
        contentPlaced = false;
        yield return null;




        textTitle.text = MainManager.Instance.title;
        loadingScr.SetActive(true);
        if (garmDict.ContainsKey(urlCol))
        {
            hbtObj = garmDict[urlCol];
            ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
            olfu.substeps = MainManager.Instance.qty;
            
            spawnedObject = Instantiate(hbtObj,cameraPoint, Quaternion.identity);

            spawnedObject.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
            contentPlaced = true;
            canCycle = true;
            AddDrpOpts();
            loadingScr.SetActive(false);
        }
        else
        {
            FirebaseStorage storage = FirebaseStorage.DefaultInstance;

            StorageReference storageRef =
            storage.GetReferenceFromUrl("");
            
           
            
            storageRef.Child($"{urlCol}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
            if (!task.IsFaulted && !task.IsCanceled) 
            {
                Debug.Log("Download URL: " + task.Result);
                StartCoroutine(GetAssetBundleExtra(Convert.ToString(task.Result), urlCol));
            }
            
            else
            {
                loadingScr.SetActive(false);
                Debug.Log(task.Exception);
            }
            });
        }
    
        
        
    }


    IEnumerator RespawnColor()
    {
        
        canCycle = false;
        sizeDropdown.ClearOptions();


        next += 1;
       
        if(start + next > colsList.Count - 1)
        {
            start = 0;
            next = 0;
        }
        
        string urlCol = $"{genderGarm.ToLower()}_{uSize.ToLower()}_{groupGarm}_{colsList[start + next]}";

        Destroy(spawnedObject);
        contentPlaced = false;
        yield return null;





        loadingScr.SetActive(true);
    
        textTitle.text = MainManager.Instance.title;


        if (garmDict.ContainsKey(urlCol))
        {
            hbtObj = garmDict[urlCol];
            ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
            olfu.substeps = MainManager.Instance.qty;
            
            spawnedObject = Instantiate(hbtObj,cameraPoint, Quaternion.identity);

            spawnedObject.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
            contentPlaced = true;
            canCycle = true;
            AddDrpOpts();
            loadingScr.SetActive(false);
        }
        else
        {
            FirebaseStorage storage = FirebaseStorage.DefaultInstance;

            StorageReference storageRef =
            storage.GetReferenceFromUrl("");
            
            storageRef.Child($"{urlCol}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
            if (!task.IsFaulted && !task.IsCanceled) 
            {
                Debug.Log("Download URL: " + task.Result);
                StartCoroutine(GetAssetBundleExtra(Convert.ToString(task.Result), urlCol));
            }
            
            else
            {
                loadingScr.SetActive(false);
                Debug.Log(task.Exception);
            }
            });
        }
    }
    void AddDrpOpts()
    {
        if (genderGarm == "m")
        {
            sizeDropdown.ClearOptions();
            sizeDropdown.AddOptions(sizeOptionsM);
            
        }
        else if (genderGarm == "w")
        {
            sizeDropdown.ClearOptions();
            sizeDropdown.AddOptions(sizeOptionsF);
            
        }
        
        sizeDropdown.captionText.text = sShown;
        
        
    }

    IEnumerator AvailableColors()
    {
        canCycle = false;
        
        
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        CollectionReference itemsRef = db.Collection("garments");
        
        string[] textSplit = MainManager.Instance.fullPath.Split('_');
        genderGarm = textSplit[0];
        string sizeGarm = textSplit[1];
        groupGarm = textSplit[2];
        string colorGarm = textSplit[3];
        Debug.Log($"The gender garm is{genderGarm}");

        AddDrpOpts();
        

        sizeDropdown.gameObject.SetActive(true);


        CollectionReference usersRef = db.Collection("users");

        auth = FirebaseAuth.DefaultInstance;
        
        var user = auth.CurrentUser;
        Debug.Log(user.UserId);
        DocumentReference docRef = db.Collection("users").Document(user.UserId);

        userSnap.Clear();
        var getUserInfo = docRef.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            DocumentSnapshot snapshot = querySnapshotTask.Result;
            if (snapshot.Exists) 
            {
                Debug.Log(String.Format("Document data for {0} document:", snapshot.Id));
                userSnap = snapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in userSnap) 
                {
                    Debug.Log(String.Format("{0}: {1}", pair.Key, pair.Value));
                }
            } 
            else 
            {
                Debug.Log(String.Format("Document {0} does not exist!", snapshot.Id));
            }
        });
        
        yield return new WaitUntil(predicate: () => getUserInfo.IsCompleted );
        uSize = (string) userSnap["size"];
        uGender = (string) userSnap["gender"];















        Query firstQuery;

        firstQuery = itemsRef
        .WhereEqualTo("size", uSize)
            .WhereEqualTo("gender", uGender)
            .WhereEqualTo("tryon", true)
            .WhereEqualTo("group_id", groupGarm);


        int index = - 1;
        int keyIndex = -1;
        var getData = firstQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
            {
               
                Debug.Log($"Garment variations");
                Dictionary<string, object> garments = document.ToDictionary();
                itemcol = garments["color_code"].ToString();
                index += 1;
                colsList.Add(itemcol);
                if (itemcol == colorGarm)
                    keyIndex = index;                
            }
            
        });
        yield return new WaitUntil(predicate: () => getData.IsCompleted );
        
        
        Debug.Log($"Index size {colsList.Count}");
        
        if (index > 0)
        {
            cycle.gameObject.SetActive(true);
            colorText.gameObject.SetActive(true);
            canCycle = true;
             
            start = keyIndex;
            next = 0;
        }
    }
    
    void OnDisable()
    {
        if (spawnedObject)
            Destroy(spawnedObject);
        
        garmDict.Clear (); 
    }
 
    
    void Update()
    {
        if (contentPlaced)
        {
            cameraPoint = arCam.transform.position + arCam.transform.forward * distance;
            spawnedObject.transform.position = cameraPoint;
        }
        
            
        

        


    }

    IEnumerator GetAssetBundleExtra(string url, string urlCol) 
    {
        if (bundle)
        {
            Destroy(bundle);
            Caching.ClearAllCachedVersions(bundle.name);
        }

        
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(url);
        
        yield return www.SendWebRequest();
 
        if (www.result != UnityWebRequest.Result.Success) {
            Debug.Log(www.error);
        }
        else {
            bundle = DownloadHandlerAssetBundle.GetContent(www);

            Debug.Log("ASSET BUNDLE HAS BEEN SUCCESSFULLY DOWNLOADED");
        }
        yield return new WaitForEndOfFrame();

            if (bundle == null)
            {    
                Debug.Log("Failed to load AssetBundle!");
            }
            else
                Debug.Log("AssetBundle loaded successfully");

            www.Dispose();

            loadingScr.SetActive(false);
            
            string rootAssetPath = bundle.GetAllAssetNames()[0];
            hbtObj = bundle.LoadAsset(rootAssetPath) as GameObject;
            ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
            olfu.substeps = MainManager.Instance.qty;
            
            
            
            

            
            spawnedObject = Instantiate(hbtObj,cameraPoint, Quaternion.identity);
            garmDict.Add(urlCol, hbtObj);
            bundle.Unload(false);
            

            spawnedObject.transform.rotation = Quaternion.Euler(new Vector3(0,180,0));
            
            
            contentPlaced = true;
            canCycle = true;
            AddDrpOpts();
            
            
    }

    
 
    IEnumerator OnPositionContent()
    {
        yield return null;
 
        if (spawnedObject == null)
        {
            
            Vector3 cameraPoint = arCam.transform.position + arCam.transform.forward * distance;

            
            

        }

 
    }
 
    void DisablePlanes()
    {
 
        Debug.Log("Disabled");
    }
}
