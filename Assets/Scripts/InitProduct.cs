using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitProduct : MonoBehaviour
{
    public delegate void OnInitSingleDocRequested(string id);
    public static OnInitSingleDocRequested InitSDocRequested;
    // Start is called before the first frame update
    void Start()
    {
        InitSDocRequested?.Invoke(PlayerPrefs.GetString("selGarment").ToString());
        Debug.Log(PlayerPrefs.GetString("selGarment"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
