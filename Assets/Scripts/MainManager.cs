using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Firebase.Firestore;
using Firebase.Extensions;

public class MainManager : MonoBehaviour
{
    // Start() and Update() methods deleted - we don't need them right now

    public static MainManager Instance;
    [HideInInspector]
    public List<DocumentSnapshot> cursorListMan = new List<DocumentSnapshot>();
    [HideInInspector]
    public int pageMan;
    [HideInInspector]
    public int totGarms;
    [HideInInspector]
    public bool adjustmentReq;
    [HideInInspector]
    public bool updateReq;
    [HideInInspector]
    public bool stillDwn;
    [HideInInspector]    
    public float scrollPos;
    [HideInInspector]
    public string lastSceneMan;
    [HideInInspector]    
    public string sceneRequestedMan;
    [HideInInspector]    
    public string filtOption;
    [HideInInspector]
    public float recChoice;
    [HideInInspector]
    public bool noMoreGarmentsLocalMan;

    [HideInInspector]
    public string size;
    [HideInInspector]
    public string gender;
    [HideInInspector]
    public string type;
    [HideInInspector]
    public int typeIndx;

    [HideInInspector]
    public int qty;

    [HideInInspector]
    public string fullPath;

    [HideInInspector]
    public bool syncGarm;
    [HideInInspector]
    public bool syncAvatar;
    [HideInInspector]
    public bool isFav;

    [HideInInspector]
    public string title;
    [HideInInspector]
    public string avtrSize;

    [HideInInspector]
    public bool avtrSizeReq;
    [HideInInspector]
    public bool mulQueriesProcessing;
    
    

    private void Awake()
    {
        // start of new code
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // end of new code
        isFav = false;
        title ="";
        avtrSizeReq = false;
        mulQueriesProcessing = false;

        fullPath ="Not Set";
        pageMan = 0;
        totGarms = 0;
        adjustmentReq = false;
        updateReq = false;
        stillDwn = false;
        qty = 4;
        
        scrollPos = 1f;
        lastSceneMan = "LoginScene";
        
        sceneRequestedMan = "AccountScene";
        filtOption = "ViewAll";
        recChoice = 0f;
        noMoreGarmentsLocalMan = false;

        size = "L";
        gender = "male";
        type = "all";
        typeIndx = 0;

        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}