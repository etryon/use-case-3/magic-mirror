using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class AuthLogIn : MonoBehaviour
{
    private GameObject SigninObj;
    [SerializeField]
    private TMP_InputField inputMailObj;
    [SerializeField]
    private TMP_InputField inputPassObj;
    private string mail, pass;
    void Start()
    {

        var inputMail = inputMailObj.GetComponent<TMP_InputField>();
        
        var inputPass = inputPassObj.GetComponent<TMP_InputField>();

        SigninObj = GameObject.Find("ButtonSignIn");
        var signinButton = SigninObj.GetComponent<Button>();

        //Could be also set on change value
        inputMail.onEndEdit.AddListener((string arg0) => { mail = arg0; });
        inputPass.onEndEdit.AddListener((string arg1) => { pass = arg1; });

       
    }
}
