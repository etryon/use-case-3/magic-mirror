using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TextureManager : MonoBehaviour
{

[HideInInspector]
public static UnityWebRequest uwrDel;

public static TextureManager Instance;

    private void Awake()
    {
        // start of new code
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        // end of new code

        Instance = this;
        DontDestroyOnLoad(gameObject);
        
    }

    public static void DestroyTextureOnReceived()
    {
        Debug.Log("Received Textures");
       
    }

    private IEnumerator texHandle()
    {
        Debug.Log("texHandle called");
        yield return new WaitForEndOfFrame();
    }


}
