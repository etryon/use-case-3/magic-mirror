//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class ToggleNavscroll : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject txt;
    private string DefUnitPref = "Metric";
    private string DefGarPref = "Off";
    void Start()
    {
        if (txt.GetComponent<UnityEngine.UI.Text>().text == "Units")
            txt.GetComponent<UnityEngine.UI.Text>().text = DefUnitPref;

        if (txt.GetComponent<UnityEngine.UI.Text>().text == "Garms")
            txt.GetComponent<UnityEngine.UI.Text>().text = DefGarPref;
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SliderValueUnitChanged(float value)
    {
        if (value == 0)
        {
            Debug.Log("Metric");
            txt.GetComponent<UnityEngine.UI.Text>().text = "Metric";
        }
        else
        {
            Debug.Log("Imperial");
            txt.GetComponent<UnityEngine.UI.Text>().text = "Imperial";
        }
    }

    public void SliderValueGarmsChanged(float value)
    {
        if (value == 0)
        {
            Debug.Log("Metric");
            txt.GetComponent<UnityEngine.UI.Text>().text = "Off";
        }
        else
        {
            Debug.Log("Imperial");
            txt.GetComponent<UnityEngine.UI.Text>().text = "On";
        }
    }


}
