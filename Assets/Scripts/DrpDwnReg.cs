using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;

public class DrpDwnReg : MonoBehaviour
{
    List<string> genderOptions = new List<string> { "Gender","Male", "Female"};
    Dictionary<int, string> genderOpts = new Dictionary<int, string>{ { 0, "male" },{ 1, "male" } , { 2, "female" }};
    List<string> sizeOptionsF = new List<string> { "Size","XS", "S", "M", "L", "XL"};
    Dictionary<int, string> sizeOptsF = new Dictionary<int, string>{ { 0, "M" } ,{ 1, "XS" } , { 2, "S" }, { 3, "M" }, { 4, "L" }, { 5, "XL" }};
     List<string> sizeOptionsM = new List<string> { "Size", "S", "M", "L", "XL","XXL"};
    Dictionary<int, string> sizeOptsM = new Dictionary<int, string>{ { 0, "M" } , { 1, "S" }, { 2, "M" }, { 3, "L" }, { 4, "XL" } ,{ 5, "XXL" }};
    List<string> options;
    int g;

    public TMPro.TMP_Dropdown sizeDropdown, genderDropdown;
    // Start is called before the first frame update
    void Start()
    {
        sizeDropdown.ClearOptions();
        sizeDropdown.AddOptions(sizeOptionsM);

        genderDropdown.ClearOptions();
        genderDropdown.AddOptions(genderOptions);

        genderDropdown.onValueChanged.AddListener((int arg0) =>
        {
            g = arg0;
            Debug.Log("gender set to"+ genderOpts[arg0]);
            if (g == 1)
            {
                sizeDropdown.ClearOptions();
                sizeDropdown.AddOptions(sizeOptionsM);
            }
            else if (g == 2)
            {
                sizeDropdown.ClearOptions();
                sizeDropdown.AddOptions(sizeOptionsF);
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
