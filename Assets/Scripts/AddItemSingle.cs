using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Firestore;
using Firebase.Extensions;
using System.IO;

using Firebase;
using Firebase.Auth;
 
public class AddItemSingle : MonoBehaviour
{
    private GameObject cont;
    public GameObject  arSwitch;

    public GameObject  arPrev;

    private Transform ct, bntext, title, imgmain, pricefav, price, fav, imgpts, buy, buyarea;
    private Text titcont, pricetxt;
    private Image favimg;
    public Image img;
    private Button favbut, arSwitchbut, arPrevbut;
    [SerializeField]
    private Button buybut;
    private Texture2D scaledImg;
    private bool imgStillDownloading = false;
    public Button butHome;
    public Button butSignOut;

    private bool downloadComp;
    public FirebaseAuth auth;
    
    void Start()
    {
        downloadComp = false;
        MainManager.Instance.adjustmentReq = true;
        FirebaseLoadProduct.SingleQueryfinished += InitPoPProduct;
        Resources.UnloadUnusedAssets();

        butHome.onClick.AddListener(() =>
        {
            StartCoroutine(ButHomeRoutine());
        });

        butSignOut.onClick.AddListener(() =>
        {
            StartCoroutine(ButSignOutRoutine());
        });
        

    }

    IEnumerator ButHomeRoutine()
    {
        
        
        
        yield return new WaitUntil(() => (downloadComp == true));
        MainManager.Instance.pageMan = 0;
        MainManager.Instance.recChoice = 0;
        
        MainManager.Instance.cursorListMan.Clear();
        SceneManager.LoadScene(Init.Lastscene);
    }


    IEnumerator ButSignOutRoutine()
    {
        
        
        
        yield return new WaitUntil(() => (downloadComp == true));
    
        Destroy(scaledImg);
        Destroy(img);
        Destroy(cont);
        MainManager.Instance.recChoice = 0;
        MainManager.Instance.pageMan = 0;
        MainManager.Instance.cursorListMan.Clear();
        FirebaseManager.auth.SignOut();
        FirebaseManager.user.DeleteAsync();

        
        SceneManager.LoadScene("LogInScene"); 
    }

    IEnumerator CleanTex()
    {
        

        yield return TextureManager.uwrDel.isDone == true;

        if (TextureManager.uwrDel.result != UnityWebRequest.Result.Success)
        {
        }
        else 
        {
            Texture2D scaledImg = new Texture2D(2,2, TextureFormat.ASTC_12x12, false);
            
            
            scaledImg = DownloadHandlerTexture.GetContent(TextureManager.uwrDel);
            Destroy(scaledImg);

        }
    }

    void OnDisable()
    {
        FirebaseLoadProduct.SingleQueryfinished -= InitPoPProduct;
        if(scaledImg)
        {
            Texture2D.DestroyImmediate(scaledImg);
            
        }
        
        Resources.UnloadUnusedAssets();
    }

    IEnumerator ImageAddingProcess(string url)
    {

        
        

        url = url + "?sw=600&sh=600&q=100";
        Davinci.get().load(url).into(img)
        .withErrorAction((error) =>
        {
            Debug.Log("Got error : " + error);
            downloadComp = true;
        })
        .withEndAction(() =>
        {
            Debug.Log("Download has been completed.");
            downloadComp = true;
        }).start();
        yield return new WaitUntil (() => downloadComp == true);
        
        
    }

    void InitPoPProduct(Dictionary<string, object> garment)
    {
        cont = GameObject.Find("Content");
        ct = cont.transform;
        

        
        bntext = ct.Find("BacknText");
        title = bntext.Find("Text");

        titcont = title.GetComponent<Text>();
        try
        {
            titcont.text = garment["title"].ToString();
            MainManager.Instance.title = garment["title"].ToString();

        }
        catch (Exception emptyTit)
        {
            Debug.Log("No Title");
            
        }

        
        
        
        
        
        string url = garment["photo"].ToString();
        img.sprite = Resources.Load<Sprite>("Sprites/etryoncicle");
        
        StartCoroutine(ImageAddingProcess(url));
        Dictionary<string, object> extraInfo = garment["additional_information"] as Dictionary<string,object>;
        Dictionary<string, object> prType = garment["product_type"] as Dictionary<string,object>;
        
        
        
        arSwitchbut = arSwitch.GetComponent<Button>();
        arPrevbut = arPrev.GetComponent<Button>();

        string aRvalue = garment["tryon"].ToString();
        //Show only if the Garment has the corresponding Asset Bundle available
         if (aRvalue == "True")
         {
            arSwitchbut.gameObject.SetActive(true);
            arPrevbut.gameObject.SetActive(true);

         }

        arSwitchbut.onClick.AddListener(() =>
        {
            
            StartCoroutine(ViewLoad("MMARVIEW"));
        });

        arPrevbut.onClick.AddListener(() =>
        {
            
            StartCoroutine(ViewLoad("MM3DVIEW"));
        });

        IEnumerator ViewLoad(string scene)
        {
            yield return new WaitUntil (() => downloadComp == true);
            SceneManager.LoadScene(scene);

        }



        
        
        
         

        buybut.onClick.AddListener(() =>
        {
            
            Application.OpenURL(garment["eshop_link"].ToString());
        });


        
        pricefav = ct.Find("PricenFav");
        price = pricefav.Find("Price");
        pricetxt = price.GetComponent<Text>();

        try
        {
            pricetxt.text = garment["price"].ToString() + "€";
        }
        catch (Exception emptyPrice)
        {
            
            pricetxt.text = "$$";
        }


        fav = pricefav.Find("Fav");
        favbut = fav.GetComponent<Button>();
        favimg = fav.GetComponent<Image>();

        auth = FirebaseAuth.DefaultInstance;
        
        var user = auth.CurrentUser;

        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

        DocumentReference docRef = db.Collection("favourites").Document(user.UserId).Collection("entries").Document(PlayerPrefs.GetString("selGarment").ToString());

        Sprite emptyh = Resources.Load<Sprite>("Sprites/ic_heart_empty");
        Sprite fullh = Resources.Load<Sprite>("Sprites/ic_heart_filled");

        favimg.sprite = emptyh;
        
        try
        {
            if (MainManager.Instance.isFav)
                favimg.sprite = fullh;
            else
                favimg.sprite = emptyh;
        }
        catch (Exception debug)
        {
            favimg.sprite = emptyh;
        }
        favbut.onClick.AddListener(() =>
        {
            try
            {
                if (favimg.sprite == emptyh)
                {
                    favimg.sprite = fullh;
                    Dictionary<string, object> fav = new Dictionary<string, object>
                    {
                            { "photos", "" },
                            
                    };
                    docRef.SetAsync(fav).ContinueWithOnMainThread(task => {
                            Debug.Log("Added data to the document in the favourites collection.");
                    });
                    

                }
                else
                {
                    favimg.sprite = emptyh;
                    docRef.DeleteAsync().ContinueWithOnMainThread(task => {
                    if (task.IsCompleted) {
                        Debug.Log("File deleted successfully.");
                    }
                    else {
                        
                    }
                    });
                    
                }
            }
            catch (Exception debug)
            {
                favimg.sprite = emptyh;
            }
        });
    }

    
    void Update()
    {

    }
}
