using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class CreateAcc : MonoBehaviour
{
    private GameObject DrpbirthObj, DrpHghtObj, DrpWghtObj;
    private GameObject butCrtAccObj;

    private string mail, pass, confpass;
    private string birthopt, hghtopt, wghtopt;

    private GameObject SigninObj;
    [SerializeField]
    private TMP_InputField inputMailObj;
    [SerializeField]
    private TMP_InputField inputPassObj;
    [SerializeField]
    private TMP_InputField inputPassConObj;
    void Start()
    {
        //inputMailObj = GameObject.Find("InputFieldMail");
        var inputMail = inputMailObj.GetComponent<TMP_InputField>();

        //inputPassObj = GameObject.Find("InputFieldPassword");
        var inputPass = inputPassObj.GetComponent<TMP_InputField>();

        //inputPassConObj = GameObject.Find("InputFieldPasswordConfirm");
        var inputPassCon = inputPassConObj.GetComponent<TMP_InputField>();

        butCrtAccObj = GameObject.Find("ButtonCreateAcc");
        var butCrtAcc = butCrtAccObj.GetComponent<Button>();

        DrpbirthObj = GameObject.Find("DropdownBirth");
        var Drpbirth = DrpbirthObj.GetComponent<Dropdown>();

        DrpHghtObj = GameObject.Find("DropdownHeight");
        var Drpheight = DrpHghtObj.GetComponent<Dropdown>();

        DrpWghtObj = GameObject.Find("DropdownWeight");
        var Drpweight = DrpWghtObj.GetComponent<Dropdown>();

        inputMail.onEndEdit.AddListener((string arg0) => { mail = arg0; });
        inputPass.onEndEdit.AddListener((string arg1) => { pass = arg1; });
        inputPassCon.onEndEdit.AddListener((string arg2) => { confpass = arg2; });

        Drpbirth.onValueChanged.AddListener((int arg3) => { birthopt = Init.Birth_options[arg3 - 1]; });
        Drpheight.onValueChanged.AddListener((int arg4) => { hghtopt = Init.Height_options[arg4 - 1]; });
        Drpweight.onValueChanged.AddListener((int arg5) => { wghtopt = Init.Weight_options[arg5 - 1]; });

        butCrtAcc.onClick.AddListener(() =>
        {
            try
            {
                if (mail == null || pass == null || confpass == null || birthopt == null || hghtopt == null || wghtopt == null)
                    throw new Exception("No args entered");

                //Authentication check goes here
                Init.Auth = true;

                if (pass != confpass)
                    throw new Exception("Passwords don't match");
                SceneManager.LoadScene("AccountScene");
            }
            catch (Exception e)
            {
                Debug.Log("Sign In error" + e);

            }

        });

    }

    // Update is called once per frame
    void Update()
    {

    }
}
