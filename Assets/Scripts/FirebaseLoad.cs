using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using System.Linq;
using UnityEngine.UI;
using System;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;




public class FirebaseLoad : MonoBehaviour
{
    public static FirebaseLoad instance;
    public static Dictionary<string,object> city;

    private string category;
    private int garmsindex = 0;
    
    Dictionary<string, object> userSnap = new Dictionary<string, object>();
 
    Dictionary<(string,string), object> garmentlist = new Dictionary<(string,string), object>();
    private List<string> stringKeys = new List<String>();
    
    
    public static int garments_num;
    public delegate void OnQueryfinished(Dictionary<(string,string), object> garment, int count, int dictsize, List<string> listKeys, DocumentSnapshot cursor, bool noMoreGarments, List<string> listfavs);
    public static OnQueryfinished Queryfinished;
    private string itemkey;
    private double doubleItemKey;
    
    private Dictionary<string, string> Usersettings = new Dictionary<string, string>();
    private float recIndx;
    private string type;
    public delegate void OnInitPagingRequested(int step, int limit, string category, Dictionary<string, string> settings);
    public static OnInitPagingRequested InitPagingRequested;
    
    private int dictsize = 0;

    private List<string> favsList = new List<string>();
    List<double> doubleKeys = new List<double>();
    private List<string> curFavsList = new List<string>();
    public FirebaseAuth auth;
    private List<string> curFavGroupid = new List<string>();
    
    
    void Awake()
    {
        
        var req = PlayerPrefs.GetString("sceneRequested");
        
        if (req == "VIEWALL")
            
            
            category = "all";
        if (req == "AR-ENABLED")
            category = "tryon";
        if (req == "Favourites")
            category = "fav";
        
        

        
        AddItemComp.ReadyToReceive += ARDocuments;
        AddItemComp.ReadyToReceiveBatch += ARDocumentsBatch;
        
        AddItemFavB.ReadyToReceiveFav += ARDocumentsFav;

    }
    void Start()
    {


        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        
    }
    void OnDisable()
    {
        AddItemComp.ReadyToReceive -= ARDocuments;
        AddItemComp.ReadyToReceiveBatch -= ARDocumentsBatch;
        
        AddItemFavB.ReadyToReceiveFav -= ARDocumentsFav;
        
        
    }
                                                               
    public void ARDocumentsBatch(DocumentSnapshot cursor, int i)
    {
        

        if (MainManager.Instance.filtOption == "TEMPAR")
            category = "tryon";
        else if (MainManager.Instance.filtOption == "VIEWALL")
            category = "all";

        
        
        
        StartCoroutine(ARDoc(i,category, cursor));
        
        
        
    }

    public void ARDocumentsFav()
    {
        StartCoroutine(ARDocsFV());
    }
    IEnumerator ARDocsFV()
    {
        var i = 8;

        category = "fav";
        DocumentSnapshot cursor = null;

        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        Query firstQuery;

        favsList.Clear();
        





        CollectionReference usersRef = db.Collection("users");

        auth = FirebaseAuth.DefaultInstance;
        
        var user = auth.CurrentUser;
        DocumentReference docRef = db.Collection("users").Document(user.UserId);

        userSnap.Clear();

        var getUserInfo = docRef.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            DocumentSnapshot snapshot = querySnapshotTask.Result;
            if (snapshot.Exists) 
            {
                userSnap = snapshot.ToDictionary();
              
            } 
           
        });
        
        yield return new WaitUntil(predicate: () => getUserInfo.IsCompleted );


        CollectionReference itemsRef = db.Collection("garments");
        
        
        Usersettings["size"] = (string) userSnap["size"];
        MainManager.Instance.size = (string) userSnap["size"];
        
        
    

    
        
        Usersettings["gender"] = (string) userSnap["gender"];
        MainManager.Instance.gender = (string) userSnap["gender"];
        
























        

        
        
        
        



        firstQuery = db.Collection("favourites").Document(user.UserId).Collection("entries");

        var getfavs = firstQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
            {
                favsList.Add(document.Id);
                Dictionary<string, object> favs = document.ToDictionary();
                
            }
                
        });

        yield return new WaitUntil(predicate: () => getfavs.IsCompleted );


        garmentlist.Clear();
        stringKeys.Clear();
        
        garmsindex = 0;
        int j = 0;

        foreach (string garmElem in favsList)
        {
            DocumentReference sndQuery = db.Collection("garments").Document($"{garmElem}");

            var getSData = sndQuery.GetSnapshotAsync().ContinueWithOnMainThread(task =>
            {
                
                DocumentSnapshot snapshot = task.Result;
                
                if (snapshot.Exists)
                {

                    
                    
                    Dictionary<string, object> garments = snapshot.ToDictionary();

                
                    
                    if (garments["gender"].ToString() == Usersettings["gender"])
                    {
                        itemkey = garments["uid"].ToString();
                        stringKeys.Add(itemkey);
                        foreach (KeyValuePair<string, object> entry in garments)
                        {
                            
                            garmentlist.Add((garmsindex.ToString(), entry.Key), entry.Value);
                            garmentlist.Add((itemkey, entry.Key), entry.Value);
                            
                            
                        }
                        garmsindex++;
                        dictsize++;
                    
                        
                        cursor = null;
                    }
                    
                }
            });
            yield return new WaitUntil(predicate: () => getSData.IsCompleted );
        }
        Queryfinished?.Invoke(garmentlist, garmsindex, dictsize, stringKeys, cursor, true, favsList);
        
        
    }

    public void ARDocumentsFiltered(DocumentSnapshot cursor, int i, string filter)
    {
        

        if (MainManager.Instance.filtOption == "TEMPAR")
            category = "tryon";
        else if (MainManager.Instance.filtOption == "VIEWALL")
            category = "all";

        
        
        
        StartCoroutine(ARDoc(i,category, cursor));
        
        
        
    }
    public void ARDocuments(DocumentSnapshot cursor)
    {
        var i = 8;

        if (MainManager.Instance.filtOption == "TEMPAR")
            category = "tryon";
        else if (MainManager.Instance.filtOption == "VIEWALL")
            category = "all";

        
        
        
        StartCoroutine(ARDoc(i,category, cursor));
        
        
        
    }
    


    IEnumerator ARDoc(int step, string category, DocumentSnapshot cursor)
    {
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

        CollectionReference usersRef = db.Collection("users");

        auth = FirebaseAuth.DefaultInstance;
        
        var user = auth.CurrentUser;
        DocumentReference docRef = db.Collection("users").Document(user.UserId);

        userSnap.Clear();

        var getUserInfo = docRef.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            DocumentSnapshot snapshot = querySnapshotTask.Result;
            if (snapshot.Exists) 
            {
                userSnap = snapshot.ToDictionary();
            }
                
               
        });
        
        yield return new WaitUntil(predicate: () => getUserInfo.IsCompleted );


        CollectionReference itemsRef = db.Collection("garments");
        
        
        Usersettings["size"] = (string) userSnap["size"];
        MainManager.Instance.size = (string) userSnap["size"];
        
        
    

    
        
        Usersettings["gender"] = (string) userSnap["gender"];
        MainManager.Instance.gender = (string) userSnap["gender"];
        
        
        type = MainManager.Instance.type;
        recIndx = MainManager.Instance.recChoice;
        
        yield return null;


        favsList.Clear();

        Query favQuery = db.Collection("favourites").Document(user.UserId).Collection("entries");

        var getfavs = favQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
            {
                favsList.Add(document.Id);
                Dictionary<string, object> favs = document.ToDictionary();
                

            }
                
        });
    
        
        yield return new WaitUntil(predicate: () => getfavs.IsCompleted );

        Query firstQuery;
        bool noMoreGarments = true;

        int docForCheckLast = step + 1;
        
         firstQuery = itemsRef
            .WhereEqualTo("gender", Usersettings["gender"])
            
            .WhereEqualTo("size", Usersettings["size"]);
        if (category != "all" && category != "fav")
        { 
            firstQuery = firstQuery
            .WhereEqualTo(category, true);
        }
        
        if (type != "all" && category != "fav")
            firstQuery = firstQuery
            .WhereEqualTo("additional_information.custom_label_3", MainManager.Instance.type);

        
        
        if (recIndx == 1)
            {
                
                curFavsList.Clear();

                Query curFavQuery = db.Collection("favourites").Document(user.UserId).Collection("entries");

                var getcurfavs = curFavQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
                {
                    foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
                    {
                        curFavsList.Add(document.Id);
                        Dictionary<string, object> favs = document.ToDictionary();
                       

                    }
                        
                });

                yield return new WaitUntil(predicate: () => getcurfavs.IsCompleted );


                


                garmentlist.Clear();
                stringKeys.Clear();
                doubleKeys.Clear();
                
                
                garmsindex = 0;

                
                foreach (string garmElem in favsList)
                {
                    
                    DocumentReference sndQuery = db.Collection("garments").Document($"{garmElem}");

                    var getcurSData = sndQuery.GetSnapshotAsync().ContinueWithOnMainThread(task =>
                    {
                        
                        DocumentSnapshot snapshot = task.Result;
                        
                        if (snapshot.Exists)
                        {

                            
                            
                            Dictionary<string, object> garments = snapshot.ToDictionary();

                        
                            
                            if (garments["gender"].ToString() == Usersettings["gender"])
                            {
                                
                                
                                itemkey = garments["group_id"].ToString();
                                doubleItemKey = double.Parse(itemkey);
                                stringKeys.Add(itemkey);
                                doubleKeys.Add(doubleItemKey);
                            }
                            
                        }
                    });
                    yield return new WaitUntil(predicate: () => getcurSData.IsCompleted );
                }
                
                
                
                    
                
                string url = "";
                RecommendationΙnputClass recommendationsObject = new RecommendationΙnputClass();
                recommendationsObject.product_ids = doubleKeys;

                string jsonString = JsonUtility.ToJson(recommendationsObject);
                

                var request = new UnityWebRequest(url, "POST");
                // Mallzee Api Call
                yield return request.SendWebRequest();

                var data = request.downloadHandler.text;

                curFavGroupid.Clear();
                if (request.responseCode == (long)System.Net.HttpStatusCode.OK) 
                {

                    RecommendationΙnputClass returnObj = JsonUtility.FromJson<RecommendationΙnputClass>(data);
                    
                    foreach(double product in returnObj.product_ids)
                    {
                        curFavGroupid.Add(product.ToString());
                    }
                }

               
                
                Query spliceQuery;

                garmentlist.Clear();
                stringKeys.Clear();
                
                
                List<string> splicedList = new List<string>();
                for (var i = 0; i < curFavGroupid.Count(); i++)
                {
                    splicedList.Add(curFavGroupid[i]);
                    if ((i % 10 == 0 || i == curFavGroupid.Count() - 1))
                    {
                        spliceQuery = firstQuery.WhereIn("group_id", splicedList);
                        splicedList.Clear();
                        var getSpData = spliceQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
                        {
                            foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
                            {
                                if (garmsindex < 20)
                                {
                                    Dictionary<string, object> gms = document.ToDictionary();

                                    itemkey = gms["uid"].ToString();
                                    stringKeys.Add(itemkey);
                                    foreach (KeyValuePair<string, object> entry in gms)
                                    {
                        
                                        garmentlist.Add((itemkey, entry.Key), entry.Value);
                                        
                                        
                                    }
                                    garmsindex++;
                                    dictsize++;
                                }
                            
                                
                                
                            
                                
                            }
                        });
                        
                        yield return new WaitUntil(predicate: () => getSpData.IsCompleted );
                    }
                }
                MainManager.Instance.mulQueriesProcessing = false;
                Queryfinished?.Invoke(garmentlist, garmsindex, dictsize, stringKeys, null, true, favsList);
       
            }
        
        if (cursor != null)
        { 
            firstQuery = firstQuery
            .StartAfter(cursor);
        }


        



        
        if (recIndx == 0)
        {
            
            firstQuery = firstQuery.Limit(docForCheckLast);
            garmentlist.Clear();
            stringKeys.Clear();
            
            garmsindex = 0;
            int j = 0;
            
            
            var getData = firstQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
            {
                foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
                {
                
                    
                    Dictionary<string, object> garments = document.ToDictionary();

                    if (garmsindex + 1 == docForCheckLast)
                    {
                        noMoreGarments = false;
                    }
                    else
                    {
                        itemkey = garments["uid"].ToString();
                        stringKeys.Add(itemkey);
                        foreach (KeyValuePair<string, object> entry in garments)
                        {
                            
                            
                            
                            garmentlist.Add((itemkey, entry.Key), entry.Value);
                            
                            
                        }
                        garmsindex++;
                        dictsize++;
                    
                        
                        cursor = document;
                    }
                    
                }
            });
            
            
            yield return new WaitUntil(predicate: () => getData.IsCompleted );
            Queryfinished?.Invoke(garmentlist, garmsindex, dictsize, stringKeys, cursor, noMoreGarments, favsList);
            
            

            
            garmentlist.Clear();
        }
        
    }


}
