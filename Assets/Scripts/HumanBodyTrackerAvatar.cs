using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;
using System.IO;
using Obi;
using System.IO;
using System.Threading.Tasks;
using UnityEngine.Networking;
using System.Collections;

using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;
using Firebase.Storage;
public class HumanBodyTrackerAvatar : MonoBehaviour
{
   

    [SerializeField]
    [Tooltip("Avatar Mode")]
    bool avatarOcc;
    bool flag;

    AssetBundle bundle;
    public GameObject loadingScr;

    GameObject hbtObj;
    GameObject hbtAvObj;
    private bool hasInit;

    string genderGarm;
    string sizeGarm;

    

    [SerializeField]
    [Tooltip("The ARHumanBodyManager which will produce body tracking events.")]
    ARHumanBodyManager m_HumanBodyManager;

    /// <summary>
    /// Get/Set the <c>ARHumanBodyManager</c>.
    /// </summary>
    public ARHumanBodyManager humanBodyManager
    {
        get { return m_HumanBodyManager; }
        set { m_HumanBodyManager = value; }
    }

    private GameObject newSkeletonGO;
    /// <summary>
    /// Get/Set the skeleton prefab.
    /// </summary>
   

    Dictionary<TrackableId, BoneController> m_SkeletonTracker = new Dictionary<TrackableId, BoneController>();
    
    

    
    void OnEnable()
    {
        flag = false;
        MainManager.Instance.syncAvatar = false;
        
        StartCoroutine(InitAr());
        
    }
    

    void OnDisable()
    {
        if (m_HumanBodyManager != null)
            m_HumanBodyManager.humanBodiesChanged -= OnHumanBodiesChanged;


    }

    IEnumerator InitAr()
    {
        yield return null;

        string[] textSplit = MainManager.Instance.fullPath.Split('_');
        genderGarm = textSplit[0];
        sizeGarm = textSplit[1];
        

        //Load the locally stored occluder based on the user preferences
        hbtObj = Resources.Load<GameObject>($"Avatars/{genderGarm}_{sizeGarm}") as GameObject;
        Debug.Log("Avatar loaded successfully");
        MainManager.Instance.syncAvatar = true;

   
        Debug.Assert(m_HumanBodyManager != null, "Human body manager is required.");
        m_HumanBodyManager.humanBodiesChanged += OnHumanBodiesChanged;

        

    }
    

    void OnHumanBodiesChanged(ARHumanBodiesChangedEventArgs eventArgs)
    {
        BoneController boneController;

    
        foreach (var humanBody in eventArgs.added)
        {
            if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController)&& MainManager.Instance.avtrSizeReq == false)
            {
                hasInit = true;
                Debug.Log($"Adding a new skeleton [{humanBody.trackableId}].");
                newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                boneController = newSkeletonGO.GetComponent<BoneController>();
                m_SkeletonTracker.Add(humanBody.trackableId, boneController);
            }
            else if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController)&& MainManager.Instance.avtrSizeReq == true)
            {
                hasInit = true;
                hbtObj = Resources.Load<GameObject>($"Avatars/{genderGarm}_{MainManager.Instance.avtrSize}") as GameObject;
                newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                boneController = newSkeletonGO.GetComponent<BoneController>();
                m_SkeletonTracker.Add(humanBody.trackableId, boneController);
            }

            boneController.InitializeSkeletonJoints();
            boneController.ApplyBodyPose(humanBody);
        }

        foreach (var humanBody in eventArgs.updated)
        {

            if (!hasInit)
            {
                if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController) && MainManager.Instance.avtrSizeReq == false)
                {
                    hasInit = true;
                    Debug.Log($"Adding a new skeleton [{humanBody.trackableId}].");
                    newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                    //newSkeletonGO = Instantiate(m_SkeletonPrefab, humanBody.transform);
                    boneController = newSkeletonGO.GetComponent<BoneController>();
                    m_SkeletonTracker.Add(humanBody.trackableId, boneController);

                    boneController.InitializeSkeletonJoints();
                    boneController.ApplyBodyPose(humanBody);
                }
                else if(!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController) && MainManager.Instance.avtrSizeReq == true)
                {
                    MainManager.Instance.avtrSizeReq = false;
                    hasInit = true;
                    Destroy(boneController.gameObject);
                    m_SkeletonTracker.Remove(humanBody.trackableId);
                    Destroy(newSkeletonGO);
                    hbtObj = Resources.Load<GameObject>($"Avatars/{genderGarm}_{MainManager.Instance.avtrSize}") as GameObject;

                    newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                    boneController = newSkeletonGO.GetComponent<BoneController>();
                    m_SkeletonTracker.Add(humanBody.trackableId, boneController);

                    boneController.InitializeSkeletonJoints();
                    boneController.ApplyBodyPose(humanBody);
                }
                
            }
            else
            {   if(MainManager.Instance.avtrSizeReq == true)
                {
                    MainManager.Instance.avtrSizeReq = false;
                    boneController = newSkeletonGO.GetComponent<BoneController>();
                    Destroy(boneController.gameObject);
                    m_SkeletonTracker.Remove(humanBody.trackableId);
                    Destroy(newSkeletonGO);
                    hbtObj = Resources.Load<GameObject>($"Avatars/{genderGarm}_{MainManager.Instance.avtrSize}") as GameObject;

                    newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                    boneController = newSkeletonGO.GetComponent<BoneController>();
                    m_SkeletonTracker.Add(humanBody.trackableId, boneController);

                    boneController.InitializeSkeletonJoints();
                    boneController.ApplyBodyPose(humanBody);
                }
            }
            if (humanBody.trackingState.ToString() == "None")
            {
                newSkeletonGO.SetActive(false);
            }
            else
            {
                newSkeletonGO.SetActive(true);
                if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
                {
                   
                    boneController.ApplyBodyPose(humanBody);
                }   
            }
            
        }

        foreach (var humanBody in eventArgs.removed)
        {
            Debug.Log($"Removing a skeleton [{humanBody.trackableId}].");
            if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                Destroy(boneController.gameObject);
                m_SkeletonTracker.Remove(humanBody.trackableId);
            }
        }
    }
}