using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;
public class DrpDwn : MonoBehaviour
{
    List<string> genderOptions = new List<string> { "Gender","Male", "Female"};
    Dictionary<int, string> genderOpts = new Dictionary<int, string>{ { 0, "male" },{ 1, "male" } , { 2, "female" }};
    List<string> sizeOptionsF = new List<string> { "Size","XS", "S", "M", "L", "XL"};
    Dictionary<int, string> sizeOptsF = new Dictionary<int, string>{ { 0, "M" } ,{ 1, "XS" } , { 2, "S" }, { 3, "M" }, { 4, "L" }, { 5, "XL" }};
     List<string> sizeOptionsM = new List<string> { "Size", "S", "M", "L", "XL","XXL"};
    Dictionary<int, string> sizeOptsM = new Dictionary<int, string>{ { 0, "M" } , { 1, "S" }, { 2, "M" }, { 3, "L" }, { 4, "XL" } ,{ 5, "XXL" }};
    List<string> options;


    
    public TMPro.TMP_Dropdown sizeDropdown, genderDropdown;
    public Button updateBut;
    public bool autoreload;
    public TMPro.TMP_Text UpdScreen;
    public GameObject loadScreenObj;
    public FirebaseAuth auth;

    private int s = -1, g = -1;
   
    // Start is called before the first frame update
    void Start()
    {
        
        sizeDropdown.ClearOptions();
        sizeDropdown.AddOptions(sizeOptionsM);
        


        genderDropdown.ClearOptions();
        genderDropdown.AddOptions(genderOptions);

        genderDropdown.onValueChanged.AddListener((int arg0) =>
        {
            g = arg0;
            Debug.Log("gender set to"+ genderOpts[arg0]);
            if (g == 1)
            {
                sizeDropdown.ClearOptions();
                sizeDropdown.AddOptions(sizeOptionsM);
            }
            else if (g == 2)
            {
                sizeDropdown.ClearOptions();
                sizeDropdown.AddOptions(sizeOptionsF);
            }
        });
        sizeDropdown.onValueChanged.AddListener((int arg1) =>
        {
            s = arg1;
        });
        updateBut.onClick.AddListener(() => 
        {
            StartCoroutine(UpdateInfo());
            
        }); 
        
    }

    IEnumerator UpdateInfo()
    {
       if ( s == - 1 || g  == -1 || s == 0 || g  == 0)
        {
            UpdScreen.text = "Please select from the Dropdown Menu";
            UpdScreen.gameObject.SetActive(true);
        }
        else
        {
            

            MainManager.Instance.pageMan = 0;
            MainManager.Instance.totGarms = 0;
            MainManager.Instance.updateReq = true;
            MainManager.Instance.adjustmentReq = false;
            MainManager.Instance.cursorListMan.Clear();

            MainManager.Instance.gender = genderOpts[g];
            if (g == 1)
            {
                MainManager.Instance.size = sizeOptsM[s];
            }
            else if (g == 2)
            {
                MainManager.Instance.size = sizeOptsF[s];
            }
            UpdScreen.text = "Information Updated";
            loadScreenObj.SetActive(true);  

            FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

            CollectionReference usersRef = db.Collection("users");

            auth = FirebaseAuth.DefaultInstance;
            
            var user = auth.CurrentUser;
            Debug.Log(user.UserId);

            DocumentReference docRef = db.Collection("users").Document(user.UserId);

            Dictionary<string, object> userInfo = new Dictionary<string, object>
            {
                { "gender", MainManager.Instance.gender },
                { "size", MainManager.Instance.size }
            };

            var updateUserInfo = docRef.UpdateAsync(userInfo).ContinueWith((querySnapshotTask) =>
            {
                Debug.Log("Updated User profile");
            });
            
            yield return new WaitUntil(predicate: () => updateUserInfo.IsCompleted );


            
            UpdScreen.gameObject.SetActive(true);
            loadScreenObj.SetActive(false);  
        } 
    }
    void Update()
    {
        
    }
}
