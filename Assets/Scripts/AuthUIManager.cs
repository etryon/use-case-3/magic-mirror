using TMPro;
using UnityEngine;


public class AuthUIManager : MonoBehaviour
{
    public static AuthUIManager instance;

 
    [SerializeField]
    private GameObject loginUI;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void ClearUI()
    {

    }

    public void LoginScreen()
    {
        ClearUI();
       
    }

    public void RegisterScreen()
    {
        ClearUI();
    }

}
