using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneChangerAcc : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ChangeScene(string name)
    {
        if (name == "Favourites")
        {
            SceneManager.LoadScene("FavBScene");
            PlayerPrefs.SetString("sceneRequested", name);
        }
        else
        {
            PlayerPrefs.SetString("sceneRequested", name);
            MainManager.Instance.sceneRequestedMan = name;
            Init.Accountscene = name;
            SceneManager.LoadScene("ViewAllScene");
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
