using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;

using TMPro;

public class FirebaseManager : MonoBehaviour
{
    public static FirebaseManager instance;

    [Header("Firebase")]
    static public FirebaseAuth auth;
    static public FirebaseUser user;
    [Space(5f)]

    [Header("Login References")]
    [SerializeField]
    private TMP_InputField loginEmail;
    [SerializeField]
    private TMP_InputField loginPassword;
    [SerializeField]
    private TMP_Text loginOutputText;
    [Space(5f)]

    [Header("Register References")]
    [SerializeField]
    private TMP_InputField registerEmail;
    [SerializeField]
    private TMP_InputField registerPassword;
    [SerializeField]
    private TMP_InputField registerConfirmPassword;
    [SerializeField]
    private TMP_Text registerOutputText;
    [SerializeField]
    private TMPro.TMP_Dropdown sizeDropdown;
    [SerializeField]
    private TMPro.TMP_Dropdown genderDropdown;

    [SerializeField]
    private Toggle termsToggle;

    private string gLoc, sLoc;

    Dictionary<int, string> genderOpts = new Dictionary<int, string>{ { 0, "male" },{ 1, "male" } , { 2, "female" }};
    Dictionary<int, string> sizeOptsF = new Dictionary<int, string>{ { 0, "M" } ,{ 1, "XS" } , { 2, "S" }, { 3, "M" }, { 4, "L" }, { 5, "XL" }};
    Dictionary<int, string> sizeOptsM = new Dictionary<int, string>{ { 0, "M" } , { 1, "S" }, { 2, "M" }, { 3, "L" }, { 4, "XL" } ,{ 5, "XXL" }};

    

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(instance.gameObject);
            instance = this;
        }
       
    }

    private void Start()
    {
        StartCoroutine(CheckAndFixDependencies());
    }

    





    private IEnumerator CheckAndFixDependencies()
    {
        var CheckAndFixDependenciesTask = FirebaseApp.CheckAndFixDependenciesAsync();

        yield return new WaitUntil(predicate: () => CheckAndFixDependenciesTask.IsCompleted);

        var dependencyResult = CheckAndFixDependenciesTask.Result;

        if (dependencyResult == DependencyStatus.Available)
        {
            InitializeFirebase();
        }
        else
        {
            Debug.LogError($"Could not resolve all Firebase Dependencies: {dependencyResult}");
        }
    }

    private void InitializeFirebase()
    {
        auth = FirebaseAuth.DefaultInstance;
        StartCoroutine(checkAutoLogin());

        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);

    }

    private IEnumerator checkAutoLogin()
    {
        yield return new WaitForEndOfFrame();
        if (user != null)
        {
            var reloadUserTask = user.ReloadAsync();

            yield return new WaitUntil(predicate: () => reloadUserTask.IsCompleted);

            AutoLogin();

        }
        else
        {
            AuthUIManager.instance.LoginScreen();
        }
    }

    private void AutoLogin()
    {
        if (user != null)
        {

            AppManager.instance.ChangeScene("AccountScene");
        }
        else
        {
            AuthUIManager.instance.LoginScreen();
        }
    }

    private void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;

            if (!signedIn && user != null)
            {
                Debug.Log("Signed Out");
                auth.SignOut();
            }

            user = auth.CurrentUser;

            if (signedIn)
            {
                Debug.Log($"Signed In: {user.DisplayName}");
            }
        }
    }

    public void ClearOutputs()
    {
        loginOutputText.text = "";
        registerOutputText.text = "";
    }

    public void LoginButton()
    {
        StartCoroutine(LoginLogic(loginEmail.text, loginPassword.text));
    }

    public void LogOut()
    {
        auth.SignOut();
    }

    public void RegisterButton()
    {
        StartCoroutine(RegisterLogic(registerEmail.text, registerPassword.text, registerConfirmPassword.text, sizeDropdown.value, genderDropdown.value, termsToggle.isOn));
    }

    private IEnumerator LoginLogic(string _email, string _password)
    {
        Credential credential = EmailAuthProvider.GetCredential(_email, _password);

        var loginTask = auth.SignInWithCredentialAsync(credential);

        yield return new WaitUntil(predicate: () => loginTask.IsCompleted);

        if (loginTask.Exception != null)
        {
            FirebaseException firebaseException = (FirebaseException)loginTask.Exception.GetBaseException();
            AuthError error = (AuthError)firebaseException.ErrorCode;

            string output = "Unknown Error, please try again";

            switch (error)
            {
                case AuthError.MissingEmail:
                    output = "Please Enter your Email";
                    break;
                case AuthError.MissingPassword:
                    output = "Please Enter your Password";
                    break;
                case AuthError.InvalidEmail:
                    output = "Invalid Email";
                    break;
                case AuthError.WrongPassword:
                    output = "Incorrect Password";
                    break;
                case AuthError.UserNotFound:
                    output = "Account Does Not Exist";
                    break;
            }
            loginOutputText.text = output;
        }
        else
        {
            if (user.IsEmailVerified)
            {
                yield return new WaitForSeconds(1f);
                AppManager.instance.ChangeScene("AccountScene"); 
            }
            else
            {
              
                AppManager.instance.ChangeScene("AccountScene");
            }

        }

    }

    private IEnumerator RegisterLogic(string _email, string _password, string _confirmPassword, int size, int gender, bool toggle)
    {
        if (_email == "")
        {
            registerOutputText.text = "Please Enter an Email";
        }
        else if (_password != _confirmPassword)
        {
            registerOutputText.text = "Passwords do not Match";
        }
        else if (size == 0)
            registerOutputText.text = "Enter your size";
        else if (gender == 0)
            registerOutputText.text = "Enter your gender";
        else if (toggle == false)
            registerOutputText.text = "You must agree to the terms and conditions";
        else
        {
            var registerTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);

            yield return new WaitUntil(predicate: () => registerTask.IsCompleted);

            if (registerTask.Exception != null)
            {
                FirebaseException firebaseException = (FirebaseException)registerTask.Exception.GetBaseException();
                AuthError error = (AuthError)firebaseException.ErrorCode;

                string output = "Unknown Error, please try again";

                switch (error)
                {
                    case AuthError.InvalidEmail:
                        output = "Invalid Email";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        output = "Email Already In Use";
                        break;
                    case AuthError.WeakPassword:
                        output = "Weak Password";
                        break;
                    case AuthError.MissingEmail:
                        output = "Please Enter your Email";
                        break;
                    case AuthError.MissingPassword:
                        output = "Please Enter your Password";
                        break;
                }
                registerOutputText.text = output;
            }
            else
            {
                string output = "Creating account...";
                registerOutputText.text = output;
                UserProfile profile = new UserProfile
                {
                    DisplayName = _email,
                };

                var defaultUserTask = user.UpdateUserProfileAsync(profile);

                yield return new WaitUntil(predicate: () => defaultUserTask.IsCompleted);

                FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

                CollectionReference usersRef = db.Collection("users");
;
                Debug.Log(user.UserId);

                DocumentReference docRef = db.Collection("users").Document(user.UserId);
                gLoc = genderOpts[gender];
                if (gender == 1)
                {
                    sLoc = sizeOptsM[size];
                }
                else if (gender == 2)
                {
                    sLoc = sizeOptsF[size];
                }

                Dictionary<string, object> userInfo = new Dictionary<string, object>
                {
                    { "gender", gLoc },
                    { "size", sLoc }
                };

                var setUserInfo = docRef.SetAsync(userInfo).ContinueWith((querySnapshotTask) =>
                {
                    Debug.Log("Updated User profile");
                });
            
                yield return new WaitUntil(predicate: () => setUserInfo.IsCompleted );

                DocumentReference favRef = db.Collection("favourites").Document(user.UserId).Collection("entries").Document("temp");

                Dictionary<string, object> fav = new Dictionary<string, object>
                {
                        { "photos", "" },
                        
                };
                var setFav = favRef.SetAsync(fav).ContinueWithOnMainThread(task => {
                        Debug.Log("Added temp to the document in the favourites collection.");
                });
                yield return new WaitUntil(predicate: () => setFav.IsCompleted );

                var delFav = favRef.DeleteAsync().ContinueWithOnMainThread(task => {
                if (task.IsCompleted) {
                    Debug.Log("File deleted successfully.");
                }
               
                });
                yield return new WaitUntil(predicate: () => delFav.IsCompleted );

                if (defaultUserTask.Exception != null)
                {
                    user.DeleteAsync();
                    FirebaseException firebaseException = (FirebaseException)defaultUserTask.Exception.GetBaseException();
                    AuthError error = (AuthError)firebaseException.ErrorCode;

                    output = "Unknown Error, please try again";

                    switch (error)
                    {
                        case AuthError.Cancelled:
                            output = "Update User Cancelled";
                            break;
                        case AuthError.SessionExpired:
                            output = "Session Expired";
                            break;

                    }
                    registerOutputText.text = output;
                }
                else
                {
                    Debug.Log($"Firebase User Created Successfully: {user.DisplayName} ({user.UserId})");
                    AppManager.instance.ChangeScene("AccountScene"); 
                }

            }
        }
    }
}