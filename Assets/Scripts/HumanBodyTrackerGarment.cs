using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;
using System.IO;
using Obi;

using System.Threading.Tasks;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;
using Firebase.Storage;







public class HumanBodyTrackerGarment : MonoBehaviour
{
   

    [SerializeField]
    [Tooltip("Avatar Mode")]
    bool avatarOcc;
    bool flag;
    public TMPro.TMP_Text textTitle;
    AssetBundle bundle;
    public GameObject loadingScr;
    public FirebaseAuth auth;
    public Button cycle;
    GameObject hbtObj;
    GameObject hbtAvObj;

    private string uSize, uGender;
    private string groupGarm, genderGarm;
    public TMPro.TMP_Dropdown sizeDropdown;
    private string s;
    UnityEngine.XR.ARSubsystems.TrackableId trackID;
    Dictionary<string, GameObject> garmDict = new Dictionary<string, GameObject>();

    Dictionary<string, object> userSnap = new Dictionary<string, object>();
    private List<string> colsList = new List<string>();
    string itemcol;
    
    bool contentPlaced, canCycle;
    private int start, next;
    public TMPro.TMP_Text colorText;
     private string sShown = "Size";

    List<string> sizeOptionsF = new List<string> { "Size","XS", "S", "M", "L", "XL"};
    Dictionary<int, string> sizeOptsF = new Dictionary<int, string>{ { 0, "m" } ,{ 1, "xs" } , { 2, "s" }, { 3, "m" }, { 4, "l" }, { 5, "xl" }};
    List<string> sizeOptionsM = new List<string> { "Size", "S", "M", "L", "XL","XXL"};
    Dictionary<int, string> sizeOptsM = new Dictionary<int, string>{ { 0, "m" } , { 1, "s" }, { 2, "m" }, { 3, "l" }, { 4, "xl" } ,{ 5, "xxl" }};

    [SerializeField]
    [Tooltip("The ARHumanBodyManager which will produce body tracking events.")]
    ARHumanBodyManager m_HumanBodyManager;

    /// <summary>
    /// Get/Set the <c>ARHumanBodyManager</c>.
    /// </summary>
    public ARHumanBodyManager humanBodyManager
    {
        get { return m_HumanBodyManager; }
        set { m_HumanBodyManager = value; }
    }

    private GameObject newSkeletonGO;
    private bool hasInit;
    /// <summary>
    /// Get/Set the skeleton prefab.
    /// </summary>
    
    Dictionary<TrackableId, BoneController> m_SkeletonTracker = new Dictionary<TrackableId, BoneController>();
   
    

    
    void OnEnable()
    {
        hasInit = false;
        StartCoroutine(InitAr());   
    }
    

    void OnDisable()
    {
        if (m_HumanBodyManager != null)
            m_HumanBodyManager.humanBodiesChanged -= OnHumanBodiesChanged;


    }
    IEnumerator AvailableColors()
    {
        canCycle = false;
        
        
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        CollectionReference itemsRef = db.Collection("garments");
        
        string[] textSplit = MainManager.Instance.fullPath.Split('_');
        genderGarm = textSplit[0];
        string sizeGarm = textSplit[1];
        groupGarm = textSplit[2];
        string colorGarm = textSplit[3];
        Debug.Log($"The gender garm is{genderGarm}");

        AddDrpOpts();
        

        sizeDropdown.gameObject.SetActive(true);


        CollectionReference usersRef = db.Collection("users");

        auth = FirebaseAuth.DefaultInstance;
        
        var user = auth.CurrentUser;
        Debug.Log(user.UserId);
        DocumentReference docRef = db.Collection("users").Document(user.UserId);

        userSnap.Clear();
        var getUserInfo = docRef.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            DocumentSnapshot snapshot = querySnapshotTask.Result;
            if (snapshot.Exists) 
            {
                Debug.Log(String.Format("Document data for {0} document:", snapshot.Id));
                userSnap = snapshot.ToDictionary();
                foreach (KeyValuePair<string, object> pair in userSnap) 
                {
                    Debug.Log(String.Format("{0}: {1}", pair.Key, pair.Value));
                }
            } 
            else 
            {
                Debug.Log(String.Format("Document {0} does not exist!", snapshot.Id));
            }
        });
        
        yield return new WaitUntil(predicate: () => getUserInfo.IsCompleted );
        uSize = (string) userSnap["size"];
        uGender = (string) userSnap["gender"];















        Query firstQuery;

        firstQuery = itemsRef
        .WhereEqualTo("size", uSize)
            .WhereEqualTo("gender", uGender)
            .WhereEqualTo("tryon", true)
            .WhereEqualTo("group_id", groupGarm);


        int index = - 1;
        int keyIndex = -1;
        var getData = firstQuery.GetSnapshotAsync().ContinueWith((querySnapshotTask) =>
        {
            foreach (DocumentSnapshot document in querySnapshotTask.Result.Documents)
            {
               
                Debug.Log($"Garment variations");
                Dictionary<string, object> garments = document.ToDictionary();
                itemcol = garments["color_code"].ToString();
                index += 1;
                colsList.Add(itemcol);
                if (itemcol == colorGarm)
                    keyIndex = index;                
            }
        });
        yield return new WaitUntil(predicate: () => getData.IsCompleted );
        
        Debug.Log($"Index size {colsList.Count}");
        
        if (index > 0)
        {
            cycle.gameObject.SetActive(true);
            colorText.gameObject.SetActive(true);
            canCycle = true;
             
            start = keyIndex;
            next = 0;
        }
    }
    void AddDrpOpts()
    {
        if (genderGarm == "m")
        {
            sizeDropdown.ClearOptions();
            sizeDropdown.AddOptions(sizeOptionsM);
        }
        else if (genderGarm == "w")
        {
            sizeDropdown.ClearOptions();
            sizeDropdown.AddOptions(sizeOptionsF);
        }
        sizeDropdown.captionText.text = sShown;
    }
    IEnumerator InitAr()
    {
        yield return null;

        
        loadingScr.SetActive(true);
        StartCoroutine(AvailableColors());
        textTitle.text = MainManager.Instance.title;
        MainManager.Instance.syncGarm = false;
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;

        StorageReference storageRef =
        storage.GetReferenceFromUrl("");
        Debug.Log(storageRef);
            
            
            
        storageRef.Child($"{MainManager.Instance.fullPath}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
        if (!task.IsFaulted && !task.IsCanceled && task.Exception == null) 
        {
            Debug.Log("Download URL: " + task.Result);
            StartCoroutine(GetAssetBundleInit(Convert.ToString(task.Result), MainManager.Instance.fullPath));
        }
        
        else
        {
            loadingScr.SetActive(false);
            Debug.Log(task.Exception);
        }
        });

        cycle.onClick.AddListener(() =>
        {

            
            if (canCycle == true && contentPlaced == true)
            {
                canCycle = false;
                StartCoroutine(RespawnColor());
            }
                    
        });

        sizeDropdown.onValueChanged.AddListener((int arg0)=>
        {
            
            if (canCycle == true && contentPlaced == true)
            {
                canCycle = false;
                if (genderGarm == "m")
                {
                    s = sizeOptsM[arg0];
                    sShown = sizeOptionsM[arg0];
                }
                else if (genderGarm == "w")
                {
                    s = sizeOptsF[arg0];
                    sShown = sizeOptionsF[arg0];
                }
                StartCoroutine(RespawnSize(s));
            }
                    
        });
    }
    IEnumerator GetAssetBundleInit(string url, string urlCol) 
    {
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(url);
        
        yield return www.SendWebRequest();
 
        if (www.result != UnityWebRequest.Result.Success) {
            Debug.Log(www.error);
        }
        else {
            bundle = DownloadHandlerAssetBundle.GetContent(www);

            Debug.Log("ASSET BUNDLE HAS BEEN SUCCESSFULLY DOWNLOADED");
        }
        yield return new WaitForEndOfFrame();

        


        Application.targetFrameRate = 20;

        if (bundle == null)
        {    
            Debug.Log("Failed to load AssetBundle!");
        }
        else
            Debug.Log("AssetBundle loaded successfully");

        www.Dispose();
            
        string rootAssetPath = bundle.GetAllAssetNames()[0];
        hbtObj = bundle.LoadAsset(rootAssetPath) as GameObject;
        bundle.Unload(false);
        ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
        MainManager.Instance.syncGarm = true;

        olfu.substeps = MainManager.Instance.qty;
        loadingScr.SetActive(false);


        Debug.Assert(m_HumanBodyManager != null, "Human body manager is required.");
        garmDict.Add(urlCol, hbtObj);
        m_HumanBodyManager.humanBodiesChanged += OnHumanBodiesChanged;
       
        Debug.Log("Can start tracking");
        contentPlaced = true;
        canCycle = true;
        AddDrpOpts();
        hasInit = false;

        
            
    }

    IEnumerator RespawnColor()
    {
        canCycle = false;
        sizeDropdown.ClearOptions();


        next += 1;
        Debug.Log($"next{uGender}_{uSize}_{groupGarm}");
        Debug.Log($"start{start}");
        if(start + next > colsList.Count - 1)
        {
            start = 0;
            next = 0;
            Debug.Log($"startnext{start + next}");
            Debug.Log(colsList[start + next]);
        }
        else
        {
            Debug.Log($"startnext{start + next}");
            Debug.Log(colsList[start + next]);
        }
        Debug.Log($"next{genderGarm.ToLower()}_{uSize.ToLower()}_{groupGarm}_{colsList[start + next]}");
        
        string urlCol = $"{genderGarm.ToLower()}_{uSize.ToLower()}_{groupGarm}_{colsList[start + next]}";

        
                
        contentPlaced = false;
        yield return null;





        loadingScr.SetActive(true);
    
        textTitle.text = MainManager.Instance.title;
        if (garmDict.ContainsKey(urlCol))
        {
            hbtObj = garmDict[urlCol];
            ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
            olfu.substeps = MainManager.Instance.qty;
            
            contentPlaced = true;
            canCycle = true;
            AddDrpOpts();
            hasInit = false;
            loadingScr.SetActive(false);
        }
        else
        {
            FirebaseStorage storage = FirebaseStorage.DefaultInstance;

            StorageReference storageRef =
            storage.GetReferenceFromUrl("");
            Debug.Log(storageRef);
            
            
            storageRef.Child($"{urlCol}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
            if (!task.IsFaulted && !task.IsCanceled) 
            {
                Debug.Log("Download URL: " + task.Result);
                StartCoroutine(GetAssetBundleExtra(Convert.ToString(task.Result), urlCol));
            }
            
            else
            {
                loadingScr.SetActive(false);
                Debug.Log(task.Exception);
            }
            });
        }
    }

    IEnumerator RespawnSize(string s)
    {
        Debug.Log($"String s{s}");
        canCycle = false;
        sizeDropdown.ClearOptions();


        Debug.Log($"next{uGender}_{uSize}_{groupGarm}");
        Debug.Log($"start{start}");
        if(start + next > colsList.Count - 1)
        {
            start = 0;
            next = 0;
            Debug.Log($"startnext{start + next}");
            Debug.Log(colsList[start + next]);
        }
        else
        {
            Debug.Log($"startnext{start + next}");
            Debug.Log(colsList[start + next]);
        }
        uSize = s.ToLower();
        Debug.Log($"next{genderGarm.ToLower()}_{uSize.ToLower()}_{groupGarm}_{colsList[start + next]}");
      
        string urlCol = $"{genderGarm.ToLower()}_{s.ToLower()}_{groupGarm}_{colsList[start + next]}";

        contentPlaced = false;
        yield return null;





        loadingScr.SetActive(true);
    
        textTitle.text = MainManager.Instance.title;

        if (garmDict.ContainsKey(urlCol))
        {
            hbtObj = garmDict[urlCol];
            ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
            olfu.substeps = MainManager.Instance.qty;
            
            contentPlaced = true;
            canCycle = true;
            AddDrpOpts();
            hasInit = false;
            loadingScr.SetActive(false);
        }
        else
        {
            FirebaseStorage storage = FirebaseStorage.DefaultInstance;

            StorageReference storageRef =
            storage.GetReferenceFromUrl("");
           
            storageRef.Child($"{urlCol}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
            if (!task.IsFaulted && !task.IsCanceled) 
            {
                Debug.Log("Download URL: " + task.Result);
                StartCoroutine(GetAssetBundleExtra(Convert.ToString(task.Result), urlCol));
                MainManager.Instance.avtrSize = uSize.ToLower();
                MainManager.Instance.avtrSizeReq = true;
            }
            
            else
            {
                loadingScr.SetActive(false);
            }
            });
        }
    }



    IEnumerator GetAssetBundleExtra(string url, string urlCol) 
    {
        if (bundle)
        {
            Destroy(bundle);
            Caching.ClearAllCachedVersions(bundle.name);
        }

        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(url);
        
        yield return www.SendWebRequest();
 
        if (www.result != UnityWebRequest.Result.Success) {
            Debug.Log(www.error);
        }
        else {
            bundle = DownloadHandlerAssetBundle.GetContent(www);

            Debug.Log("ASSET BUNDLE HAS BEEN SUCCESSFULLY DOWNLOADED");
        }
        yield return new WaitForEndOfFrame();

        if (bundle == null)
        {    
            Debug.Log("Failed to load AssetBundle!");
        }
        else
            Debug.Log("AssetBundle loaded successfully");

        www.Dispose();

        loadingScr.SetActive(false);
        string rootAssetPath = bundle.GetAllAssetNames()[0];
        hbtObj = bundle.LoadAsset(rootAssetPath) as GameObject;
        ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
        olfu.substeps = MainManager.Instance.qty;
        garmDict.Add(urlCol, hbtObj);
        
        

        bundle.Unload(false);
        
        contentPlaced = true;
        canCycle = true;
        AddDrpOpts();
        hasInit = false;

    }





    void OnHumanBodiesChanged(ARHumanBodiesChangedEventArgs eventArgs)
    {
        BoneController boneController;

    
        foreach (var humanBody in eventArgs.added)
        {
            if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                hasInit = true;
                Debug.Log($"Adding a new skeleton [{humanBody.trackableId}].");
                newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                boneController = newSkeletonGO.GetComponent<BoneController>();
                m_SkeletonTracker.Add(humanBody.trackableId, boneController);
            }

            boneController.InitializeSkeletonJoints();
            boneController.ApplyBodyPose(humanBody);
            Debug.Log("Was called here however");
        }

        foreach (var humanBody in eventArgs.updated)
        {
            
            if (!hasInit)
            {
                if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
                {
                    Debug.Log("!hasInit-def");
                    hasInit = true;
                    Debug.Log($"Adding a new skeleton [{humanBody.trackableId}].");
                    newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                    boneController = newSkeletonGO.GetComponent<BoneController>();
                    m_SkeletonTracker.Add(humanBody.trackableId, boneController);
                    trackID = humanBody.trackableId;

                    boneController.InitializeSkeletonJoints();
                    boneController.ApplyBodyPose(humanBody);
                }
                else
                {
                    Debug.Log("!hasInit-upd");
                    hasInit = true;
                    Destroy(boneController.gameObject);
                    m_SkeletonTracker.Remove(humanBody.trackableId);
                    Destroy(newSkeletonGO);
                    

                    newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                    boneController = newSkeletonGO.GetComponent<BoneController>();
                    m_SkeletonTracker.Add(humanBody.trackableId, boneController);
                    trackID = humanBody.trackableId;

                    boneController.InitializeSkeletonJoints();
                    boneController.ApplyBodyPose(humanBody);
                }
                
            }


            if (humanBody.trackingState.ToString() == "None")
            {
                newSkeletonGO.SetActive(false);
            }
            else
            {
                Debug.Log("Constatnt");
                newSkeletonGO.SetActive(true);
                if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
                {
                    
                    boneController.ApplyBodyPose(humanBody);
                }   
            }
            
        }

        foreach (var humanBody in eventArgs.removed)
        {
            Debug.Log($"Removing a skeleton [{humanBody.trackableId}].");
            if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                Destroy(boneController.gameObject);
                m_SkeletonTracker.Remove(humanBody.trackableId);
            }
        }
    }
}