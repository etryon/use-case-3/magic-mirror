﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

namespace NavigationDrawer.UI
{
    public class NavDrawerPanel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        #region ENUMS

        private enum ENavigation
        {
            Left,
            Right
        }

        #endregion

        #region FIELDS

        [SerializeField, Header("Components")]
        private Image ImgBkg = default;

        [SerializeField]
        private GameObject panLayer = default;

        [SerializeField]
        private Canvas canvas = default;


        [SerializeField, Header("Properties")]
        private ENavigation navType = default;

        [SerializeField]
        private bool darkBkg = true;

        [SerializeField]
        private bool tapBkg = true;

        [SerializeField]
        private bool opStart = default;

        [SerializeField]
        private float animDuration = 0.5f;

        private int animState;
        private float maxPos;
        private float minPos;
        private float animStrt;
        private float animDTime;
        private float bkgAlpha;

        private RectTransform rectTrans;
        private RectTransform rectTransBkg;
        private GameObject bkgGO, BndTopObjrecs, NavDrLeft, NavDrawerRight, BndTopObjfils;
        private CanvasGroup bkgCG;

        private Vector2 pos;
        private Vector2 temVec;
        private Scene m_Scene;
        private string sceneName;

      

        public TMPro.TMP_Dropdown dropdownRecs, typeDropdown;

        #endregion

        #region UNITY_METHODS

        private void Awake()
        {
            rectTrans = gameObject.GetComponent<RectTransform>();
            rectTransBkg = ImgBkg.GetComponent<RectTransform>();
            bkgCG = ImgBkg.GetComponent<CanvasGroup>();
        }

        private void Start()
        {
            m_Scene = SceneManager.GetActiveScene();
            sceneName = m_Scene.name;
            if (sceneName == "ViewAllScene")
            {
                NavDrawerRight = GameObject.Find("NavigationDrawerRight");
                BndTopObjfils = GameObject.Find("ButtonFil");
            }



            if (navType == ENavigation.Left)
            {
                maxPos = rectTrans.rect.width / 2;
            }
            else if (navType == ENavigation.Right)
            {
                maxPos = -rectTrans.rect.width / 2;
            }

            minPos = -maxPos;

            RefreshBackgroundSize();

            bkgGO = ImgBkg.gameObject;



            if (opStart)
            {
                Open();
            }
            else
            {
                bkgGO.SetActive(false);
                panLayer.SetActive(false);
            }

            AddItemComp.CloseNav += Close;
            AddItemFavB.CloseNav += Close;
        }
        void OnDisable()
        {
            AddItemComp.CloseNav -= Close;
            AddItemFavB.CloseNav -= Close;            
        }

        private void Update()
        {
            if (animState == 1)
            {
                animDTime = Time.realtimeSinceStartup - animStrt;

                if (animDTime <= animDuration)
                {
                    rectTrans.anchoredPosition = QuintOut(pos, new Vector2(maxPos, rectTrans.anchoredPosition.y), animDTime, animDuration);
                    if (darkBkg)
                    {
                        bkgCG.alpha = QuintOut(bkgAlpha, 1f, animDTime, animDuration);
                    }
                }
                else
                {
                    rectTrans.anchoredPosition = new Vector2(maxPos, rectTrans.anchoredPosition.y);
                    if (darkBkg)
                    {
                        bkgCG.alpha = 1f;
                    }

                    animState = 0;
                }
            }
            else if (animState == 2)
            {
                animDTime = Time.realtimeSinceStartup - animStrt;
                if (animDTime <= animDuration)
                {
                    rectTrans.anchoredPosition = QuintOut(pos, new Vector2(minPos, rectTrans.anchoredPosition.y), animDTime, animDuration);
                    if (darkBkg)
                    {
                        bkgCG.alpha = QuintOut(bkgAlpha, 0f, animDTime, animDuration);
                    }
                }
                else
                {
                    rectTrans.anchoredPosition = new Vector2(minPos, rectTrans.anchoredPosition.y);
                    if (darkBkg)
                    {
                        bkgCG.alpha = 0f;
                    }

                    bkgGO.SetActive(false);
                    panLayer.SetActive(false);

                    animState = 0;
                }
            }

            if (navType == ENavigation.Left)
            {
                rectTrans.anchoredPosition = new Vector2(Mathf.Clamp(rectTrans.anchoredPosition.x, minPos, maxPos), rectTrans.anchoredPosition.y);
            }
            else if (navType == ENavigation.Right)
            {
                rectTrans.anchoredPosition = new Vector2(Mathf.Clamp(rectTrans.anchoredPosition.x, maxPos, minPos), rectTrans.anchoredPosition.y);
            }
        }

        public void OnBeginDrag(PointerEventData data)
        {
            RefreshBackgroundSize();
            if (sceneName == "ViewAllScene")
            {
                BndTopObjfils.gameObject.SetActive(false);
            }


            animState = 0;

            bkgGO.SetActive(true);
            panLayer.SetActive(true);
        }

        public void OnDrag(PointerEventData data)
        {
            temVec = rectTrans.anchoredPosition;
            temVec.x += data.delta.x;

            rectTrans.anchoredPosition = temVec;

            if (darkBkg)
            {
                bkgCG.alpha = 1 - (maxPos - rectTrans.anchoredPosition.x) / (maxPos - minPos);
            }
        }

        public void OnEndDrag(PointerEventData data)
        {
            if (navType == ENavigation.Left)
            {
                if (Mathf.Abs(data.delta.x) >= 0.5f)
                {
                    if (data.delta.x > 0.5f)
                    {
                        Open();
                    }
                    else
                    {
                        Close();
                    }
                }
                else
                {
                    if ((rectTrans.anchoredPosition.x - minPos) >
                        (maxPos - rectTrans.anchoredPosition.x))
                    {
                        Open();
                    }
                    else
                    {
                        Close();
                    }
                }
            }
            else if (navType == ENavigation.Right)
            {
                if (Mathf.Abs(data.delta.x) >= 0.5f)
                {
                    if (data.delta.x < 0.5f)
                    {
                        Open();
                    }
                    else
                    {
                        Close();
                    }
                }
                else
                {
                    if ((rectTrans.anchoredPosition.x - minPos) <
                        (maxPos - rectTrans.anchoredPosition.x))
                    {
                        Open();
                    }
                    else
                    {
                        Close();
                    }
                }
            }
        }

        #endregion

        #region PUBLIC_METHODS

        public void BackgroundTap()
        {
            if (tapBkg)
            {
                Close();
            }
        }

        public void Open()
        {
            RefreshBackgroundSize();
            if (sceneName == "ViewAllScene")
            {
                BndTopObjfils.gameObject.SetActive(false);
                typeDropdown.gameObject.SetActive(false);
                dropdownRecs.gameObject.SetActive(false);
            }
            bkgGO.SetActive(true);
            panLayer.SetActive(true);
            pos = rectTrans.anchoredPosition;
            bkgAlpha = bkgCG.alpha;
            bkgCG.blocksRaycasts = true;
            animStrt = Time.realtimeSinceStartup;
            animState = 1;

        }
        
        public void Close()
        {
            if (sceneName == "ViewAllScene")
            {
                typeDropdown.gameObject.SetActive(true);
                dropdownRecs.gameObject.SetActive(true);
            
            }
            pos = rectTrans.anchoredPosition;
            bkgAlpha = bkgCG.alpha;
            bkgCG.blocksRaycasts = false;
            animStrt = Time.realtimeSinceStartup;
            animState = 2;
        }

    

        protected virtual float QuintOut(float startValue, float endValue, float time, float duration)
        {
            var differenceValue = endValue - startValue;
            time = Mathf.Clamp(time, 0f, duration);
            time /= duration;

            if (time == 0f)
            {
                return startValue;
            }

            if (time == 1f)
            {
                return endValue;
            }

            time--;
            return differenceValue * (time * time * time * time * time + 1) + startValue;
        }

        #endregion

        #region PRIVATE_METHODS

        private void RefreshBackgroundSize()
        {
            if (navType == ENavigation.Left)
            {
                var width = canvas.GetComponent<RectTransform>().rect.width;
                rectTransBkg.sizeDelta = new Vector2(width, rectTransBkg.sizeDelta.y);
            }
            else if (navType == ENavigation.Right)
            {
                var width = canvas.GetComponent<RectTransform>().rect.width;
                rectTransBkg.sizeDelta = new Vector2(width, rectTransBkg.sizeDelta.y);
                rectTransBkg.localPosition = new Vector2(-(rectTrans.rect.width / 2), 0);
            }
        }

        private Vector2 QuintOut(Vector2 startValue, Vector2 endValue, float time, float duration)
        {
            var tempVector2 = startValue;
            tempVector2.x = QuintOut(startValue.x, endValue.x, time, duration);
            tempVector2.y = QuintOut(startValue.y, endValue.y, time, duration);
            return tempVector2;
        }

        #endregion
    }
}