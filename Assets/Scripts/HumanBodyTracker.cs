﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using System;
using System.IO;
using Obi;

using System.Threading.Tasks;
using UnityEngine.Networking;

using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;
using Firebase.Storage;
public class HumanBodyTracker : MonoBehaviour
{
    

    [SerializeField]
    [Tooltip("Avatar Mode")]
    bool avatarOcc;
    bool flag;

    AssetBundle bundle;
    public GameObject loadingScr;

    GameObject hbtObj;
    GameObject hbtAvObj;

    [SerializeField]
    [Tooltip("The ARHumanBodyManager which will produce body tracking events.")]
    ARHumanBodyManager m_HumanBodyManager;

    /// <summary>
    /// Get/Set the <c>ARHumanBodyManager</c>.
    /// </summary>
    public ARHumanBodyManager humanBodyManager
    {
        get { return m_HumanBodyManager; }
        set { m_HumanBodyManager = value; }
    }

    private GameObject newSkeletonGO;
    /// <summary>
    /// Get/Set the skeleton prefab.
    /// </summary>
   
    Dictionary<TrackableId, BoneController> m_SkeletonTracker = new Dictionary<TrackableId, BoneController>();
    
    

    
    void OnEnable()
    {
        flag = false;
        MainManager.Instance.syncGarm = false;
        MainManager.Instance.syncAvatar = false;
        
        StartCoroutine(InitAr());
        
    }
    

    void OnDisable()
    {
        if (m_HumanBodyManager != null)
            m_HumanBodyManager.humanBodiesChanged -= OnHumanBodiesChanged;


    }

    IEnumerator InitAr()
    {
        yield return null;

        
        if (!avatarOcc)
        {
            loadingScr.SetActive(true);
            MainManager.Instance.syncGarm = false;
            FirebaseStorage storage = FirebaseStorage.DefaultInstance;

            StorageReference storageRef =
            storage.GetReferenceFromUrl("");
            Debug.Log(storageRef);
            
            
            
            storageRef.Child($"{MainManager.Instance.fullPath}").GetDownloadUrlAsync().ContinueWithOnMainThread((Task<Uri> task) => {
            if (!task.IsFaulted && !task.IsCanceled) 
            {
                Debug.Log("Download URL: " + task.Result);
                StartCoroutine(GetAssetBundle(Convert.ToString(task.Result)));
            }
            
            else
            {
                
                Debug.Log(task.Exception);
            }
            
            
            });
            
        }
        else
        {
            hbtObj = Resources.Load<GameObject>("Avatars/M-XXLoccavtrv2") as GameObject;
            Debug.Log("Avatar loaded successfully");
            MainManager.Instance.syncAvatar = true;

            yield return new WaitUntil(() => (MainManager.Instance.syncGarm == true && MainManager.Instance.syncAvatar == true));
            
        }

    }
    IEnumerator GetAssetBundle(string url) 
    {
        UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle(url);
        
        yield return www.SendWebRequest();
 
        if (www.result != UnityWebRequest.Result.Success) {
            Debug.Log(www.error);
        }
        else {
            bundle = DownloadHandlerAssetBundle.GetContent(www);

            Debug.Log("ASSET BUNDLE HAS BEEN SUCCESSFULLY DOWNLOADED");
        }
        yield return new WaitForEndOfFrame();

        


        Application.targetFrameRate = 20;

        if (!avatarOcc)
        {
            if (bundle == null)
            {    
                Debug.Log("Failed to load AssetBundle!");
            }
            else
                Debug.Log("AssetBundle loaded successfully");

            
                
            string rootAssetPath = bundle.GetAllAssetNames()[0];
            hbtObj = bundle.LoadAsset(rootAssetPath) as GameObject;
            bundle.Unload(false);
            ObiLateFixedUpdater olfu = hbtObj.GetComponent<ObiLateFixedUpdater>();
            MainManager.Instance.syncGarm = true;

            yield return new WaitUntil(() => (MainManager.Instance.syncGarm == true && MainManager.Instance.syncAvatar == true));
            yield return new WaitForSeconds(1);

            olfu.substeps = MainManager.Instance.qty;
            loadingScr.SetActive(false);

            Debug.Assert(m_HumanBodyManager != null, "Human body manager is required.");
            m_HumanBodyManager.humanBodiesChanged += OnHumanBodiesChanged;
        }
        
        
            
    }


    void OnHumanBodiesChanged(ARHumanBodiesChangedEventArgs eventArgs)
    {
        BoneController boneController;

        foreach (var humanBody in eventArgs.added)
        {
            if (!m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                Debug.Log($"Adding a new skeleton [{humanBody.trackableId}].");
                newSkeletonGO = Instantiate(hbtObj, humanBody.transform);
                boneController = newSkeletonGO.GetComponent<BoneController>();
                m_SkeletonTracker.Add(humanBody.trackableId, boneController);
            }

            boneController.InitializeSkeletonJoints();
            boneController.ApplyBodyPose(humanBody);
        }

        foreach (var humanBody in eventArgs.updated)
        {
            

            if (humanBody.trackingState.ToString() == "None")
            {
                newSkeletonGO.SetActive(false);
            }
            else
            {
                newSkeletonGO.SetActive(true);
                if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
                {
                    boneController.ApplyBodyPose(humanBody);
                }   
            }
            
        }

        foreach (var humanBody in eventArgs.removed)
        {
            Debug.Log($"Removing a skeleton [{humanBody.trackableId}].");
            if (m_SkeletonTracker.TryGetValue(humanBody.trackableId, out boneController))
            {
                Destroy(boneController.gameObject);
                m_SkeletonTracker.Remove(humanBody.trackableId);
            }
        }
    }
}