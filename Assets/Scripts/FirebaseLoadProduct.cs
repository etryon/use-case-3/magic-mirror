using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using UnityEngine.UI;
using System;

using Firebase.Firestore;
using Firebase.Extensions;

public class FirebaseLoadProduct : MonoBehaviour
{

    Dictionary<string, object> garm = new Dictionary<string, object>();


    public delegate void OnSingleQueryfinished(Dictionary<string, object> garment);
    public static OnSingleQueryfinished SingleQueryfinished;
    
    void Start()
    {
        InitProduct.InitSDocRequested += SingleDocument;
    }
    void OnDisable()
    {
        InitProduct.InitSDocRequested -= SingleDocument;
        
    }



     public void SingleDocument(string id)
    {
        StartCoroutine(SDoc(id));
    }

    IEnumerator SDoc(string id)
    {
        
        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;
        DocumentReference docRef = db.Collection("garments").Document(id);
        
        var getData = docRef.GetSnapshotAsync().ContinueWithOnMainThread(task =>
        {
            
            DocumentSnapshot snapshot = task.Result;
            
            if (snapshot.Exists)
            {
             
                
                garm = snapshot.ToDictionary();
                
                foreach (KeyValuePair<string, object> pair in garm)
                {
                    
                    
                    
                    
                    
                }
                
                SingleQueryfinished?.Invoke(garm);
                
              
                
            }
        
           
            
        });
         
       
        yield return new WaitUntil(predicate: () => getData.IsCompleted);

        
       


    }

    
    void Update()
    {
        
    }
}
