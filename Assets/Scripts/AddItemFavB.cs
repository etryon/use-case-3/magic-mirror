using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;
using Mono.Data.Sqlite;
using System.Linq;


using UnityEngine.SceneManagement;

using System.Data;

using Firebase.Firestore;
using Firebase.Extensions;
using System.IO;

using Firebase;
using Firebase.Auth;
using Firebase.Storage;
using UnityEngine.Networking;
using System.Threading.Tasks;


public class AddItemFavB : MonoBehaviour
{
    public GameObject buttonPrefab;
    
    private GameObject panel;
    public ScrollRect scrollobj;
    public RectTransform layoutrect, scrollrect;
    
    GridLayoutGroup gridcomp;
    
    private bool isLoaded;
    
    private GameObject canvaspanel;
    private float tot_width, tot_height;
    private int itemduos = 0;
    private int spacinggrid = 0;
    private int panelItems = 8;
    static public string SceneId, price;
    private static bool first_run = true;
    private bool firstRun;
    private bool coroutineIsRunning;
    private List<Texture2D> currentTexs= new List<Texture2D>();
    public GameObject loadScreenObj;
    public GameObject loadScreenObjTop;
    
    public TMPro.TMP_Dropdown sizeDropdown, genderDropdown;
    public TMPro.TMP_Text textCat;
    public Button updateBut;
    Dictionary<string, GameObject> catobjs = new Dictionary<string, GameObject>();
    public static Dictionary<string, bool> garfav = new Dictionary<string, bool>();
    private GameObject DrpCatObj;
    private string catopt;
    private int downloadComp;   
    Dictionary<string, object> garments;
    Dictionary<string, object> city;
    Coroutine imageRoutine = null;
    public static int garments_num;
    private bool waitingforUpdate = true;
    private bool imgStillDownloading = false;
    private int totGarms;
    private bool cancelReq = false;
    private string activeUrl;
    
    
    DocumentSnapshot cursorLocal;
    private List<DocumentSnapshot> cursorLocalList = new List<DocumentSnapshot>();
    private IEnumerator coroutineFlag;
    private int garmentsAddedLocal = 0;
    private List<bool> routinesFinished = new List<bool>();
    
    public delegate void OnReadyToReceive(DocumentSnapshot cursor);
    public static OnReadyToReceive ReadyToReceive;

    public delegate void OnReadyToReceiveBatch(DocumentSnapshot cursor, int length);
    public static OnReadyToReceiveBatch ReadyToReceiveBatch;

    public delegate void OnReadyToReceiveFav();
    public static OnReadyToReceiveFav ReadyToReceiveFav;
    private UnityWebRequest uwrDel;
   
    public delegate void OnCloseNav();
    public static OnCloseNav CloseNav;

    public delegate void OnCloseFil();
    public static OnCloseFil CloseFil;
    private bool ordertobreak = false;

    Sprite imgSprite;

    private bool added = false;
    private bool reset = true;
    List<string> sublist;
    public FirebaseAuth auth;
    
    
    private List<string> listKeysLocal = new List<string>();
    
    Dictionary<(string,string), object> garmentlistLocal;

    private Sprite tempimg, loadingimg;
    
    Texture2D scaledImg;
    List<string> recOptions = new List<string> { "Recs: On", "Recs: Off" };
    private int garmentsAdded = 0;
    private int s = -1, g = -1;
    private int typeIndx, recIndx;
    int timesReachedEnd = 0;

    private bool noMoreGarmentsLocal = true;

    
    private bool butprevpressed = false;
    private bool butnxtpressed = true;
    public Button butHome;
    public Button butSignOut;
    public TMPro.TMP_Text UpdScreen;

    List<string> genderOptions = new List<string> { "Gender","Male", "Female"};
    Dictionary<int, string> genderOpts = new Dictionary<int, string>{ { 0, "male" },{ 1, "male" } , { 2, "female" }};
    List<string> sizeOptions = new List<string> { "Size","XS", "S", "M", "L", "XL"};
    Dictionary<int, string> sizeOpts = new Dictionary<int, string>{ { 0, "M" } ,{ 1, "XS" } , { 2, "S" }, { 3, "M" }, { 4, "L" }, { 5, "XL" }};



    List<string> sizeOptionsF = new List<string> { "Size","XS", "S", "M", "L", "XL"};
    Dictionary<int, string> sizeOptsF = new Dictionary<int, string>{ { 0, "M" } ,{ 1, "XS" } , { 2, "S" }, { 3, "M" }, { 4, "L" }, { 5, "XL" }};
     List<string> sizeOptionsM = new List<string> { "Size", "S", "M", "L", "XL","XXL"};
    Dictionary<int, string> sizeOptsM = new Dictionary<int, string>{ { 0, "M" } , { 1, "S" }, { 2, "M" }, { 3, "L" }, { 4, "XL" } ,{ 5, "XXL" }};
    Dictionary<string, string> Colors = new Dictionary<string, string>{ { "black" , "0.0,0.0,0.0,1.0" } , { "white" , "1.0,1.0,1.0,1.0" }};

    List<string> typeOptions = new List<string> { "All","Pants", "T - Shirts"};
    Dictionary<int, string> typeOpts = new Dictionary<int, string>{ { 0, "all" },{ 1, "1f_PantsTights" } , { 2, "1d_TShirtsPolos" }};
    
    
    void Start()
    {     
        downloadComp = 0;
        
        firstRun = true;
        totGarms = 0;  
        coroutineIsRunning = false;     
        
        

        scaledImg = new Texture2D(2, 2);
        tempimg = Resources.Load<Sprite>("Sprites/demoimg2");
        
        loadingimg = Resources.Load<Sprite>("Sprites/etryoncicle");
        

        panel = GameObject.Find("CategoryPanel");
        layoutrect = panel.GetComponent<RectTransform>();
        scrollrect = scrollobj.GetComponent<RectTransform>();
        
        
        tot_width = scrollrect.rect.width;
        tot_height = scrollrect.rect.height - 100;

        
        

        gridcomp = panel.GetComponent<GridLayoutGroup>();

        
        
        
        
        

        gridcomp.cellSize = new Vector2((tot_width), tot_height / 6);
        
        

        
        if (MainManager.Instance.sceneRequestedMan != MainManager.Instance.lastSceneMan)
        {
            
            ManagerReset();
            MainManager.Instance.lastSceneMan = MainManager.Instance.sceneRequestedMan ;
        }

        sizeDropdown.ClearOptions();
        sizeDropdown.AddOptions(sizeOptions);

        genderDropdown.ClearOptions();
        genderDropdown.AddOptions(genderOptions);


        genderDropdown.onValueChanged.AddListener((int arg0) =>
        {
            g = arg0;
            Debug.Log("gender set to"+ genderOpts[arg0]);
            if (g == 1)
            {
                sizeDropdown.ClearOptions();
                sizeDropdown.AddOptions(sizeOptionsM);
            }
            else if (g == 2)
            {
                sizeDropdown.ClearOptions();
                sizeDropdown.AddOptions(sizeOptionsF);
            }
        });
        sizeDropdown.onValueChanged.AddListener((int arg1) =>
        {
            s = arg1;
            
        });
        updateBut.onClick.AddListener(() => 
        {
            if ( s == - 1 || g  == -1|| s == 0 || g  == 0)
            {
                UpdScreen.text = "Please select from the Dropdown Menu";
                UpdScreen.gameObject.SetActive(true);
            }
            else
            {
                
                UpdScreen.text = "Information Updated";
                
                
                
                
                StartCoroutine(FilUpdRoutine(s,g));
               
            }

            
        });  



        butHome.onClick.AddListener(() =>
        {
            StartCoroutine(ButHomeRoutine());
        });

        butSignOut.onClick.AddListener(() =>
        {
            StartCoroutine(ButSignOutRoutine());
        });

        
       
        
        FirebaseLoad.Queryfinished += AddGarms;
        
        
        
        
        
        if (MainManager.Instance.adjustmentReq)
        {
            cursorLocal = null;
            
            
        }
            
            
        else 
        {
            
            
            
        }
        ReadyToReceiveFav?.Invoke();
        
        

    }
    

    IEnumerator SetPrefs(int size,int gender)
    {
        PlayerPrefs.SetString("gender", genderOpts[gender]);
        if (gender == 1)
        {
            PlayerPrefs.SetString("size", sizeOptsM[size]);
        }
        else if (gender == 2)
        {
            PlayerPrefs.SetString("size", sizeOptsF[size]);
        }
        yield return new WaitForEndOfFrame();
    }

  
    IEnumerator ButNextRoutine()
    {
        
        
        
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false));
        
        
        
        
        
        
        
        
        DelItemList(listKeysLocal, currentTexs);
        
        
        
        ReadyToReceive?.Invoke(MainManager.Instance.cursorListMan[MainManager.Instance.pageMan]);
        
        


        if (MainManager.Instance != null)
        {
            MainManager.Instance.pageMan += 1;
        }
        yield return new WaitForEndOfFrame();
        
    }
    IEnumerator DelItemListWait(List<string> listKeysLocal, List<Texture2D> currentTexs)
    {
        DelItemList(listKeysLocal, currentTexs);
        yield return new WaitForEndOfFrame();
    }


    IEnumerator ButHomeRoutine()
    {
        
        
        MainManager.Instance.totGarms = totGarms;
        ToggleLoading(true);
        //To ensure that there are no memory leaks
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false && downloadComp == MainManager.Instance.totGarms));
        ToggleLoading(false);
        
        
        
        
        
        
        
        DelItemList(listKeysLocal, currentTexs);
       

        ManagerReset();
        SceneManager.LoadScene("AccountScene");
    }
    void ManagerReset()
    {
        MainManager.Instance.pageMan = 0;
        MainManager.Instance.recChoice = 0;
        MainManager.Instance.totGarms = 0;
        MainManager.Instance.cursorListMan.Clear();
        MainManager.Instance.type = "all";
        MainManager.Instance.typeIndx = 0;
    }

    IEnumerator ButSignOutRoutine()
    {
        
        
        
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false));

        
        
        
        
        
        
        
        DelItemList(listKeysLocal, currentTexs);
    
        ManagerReset();
        FirebaseManager.auth.SignOut();
        FirebaseManager.user.DeleteAsync();

        
        SceneManager.LoadScene("LogInScene"); 
    }

    IEnumerator ButPrevRoutine()
    {
        
        
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false));
        MainManager.Instance.pageMan -= 1;
        
        
        DelItemList(listKeysLocal, currentTexs);

        if (MainManager.Instance.pageMan == 0)
            ReadyToReceive?.Invoke(null);
        else
        
            
        
            
            ReadyToReceive?.Invoke(MainManager.Instance.cursorListMan[MainManager.Instance.pageMan -1]);
        
        
    }
    private bool IsFinishedDownloading () 
    {
    return MainManager.Instance.stillDwn == true;
    }


    IEnumerator FilSrcRoutine(int tIndx, int recIndx)
    {
        
        
        
        CloseFil?.Invoke();
        loadScreenObj.SetActive(true);     
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false));
        loadScreenObj.SetActive(false);
        
        
        MainManager.Instance.type = typeOpts[tIndx];
        MainManager.Instance.typeIndx = tIndx;

        MainManager.Instance.recChoice = recIndx;
        MainManager.Instance.pageMan = 0;
        MainManager.Instance.totGarms = 0;
        MainManager.Instance.updateReq = true;
        MainManager.Instance.cursorListMan.Clear();
        totGarms = 0;
        
        DelItemList(listKeysLocal, currentTexs);
        
        ReadyToReceiveFav?.Invoke();
        
        UpdScreen.gameObject.SetActive(true);
        
    }



























    IEnumerator FilUpdRoutine(int size,int gender)
    {        
        loadScreenObj.SetActive(true);     
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false));
        loadScreenObj.SetActive(false);
        if ( size == - 1 || gender  == -1 || size == 0 || gender  == 0)
        {
            UpdScreen.text = "Please select from the Dropdown Menu";
            UpdScreen.gameObject.SetActive(true);
        }
        else
        {
            
            
            MainManager.Instance.gender = genderOpts[gender];
            if (gender == 1)
            {
                
                MainManager.Instance.size = sizeOptsM[size];
            }
            else if (gender == 2)
            {
                
                MainManager.Instance.size = sizeOptsF[size];
            }
            
            
            MainManager.Instance.pageMan = 0;
            MainManager.Instance.totGarms = 0;
            MainManager.Instance.updateReq = true;
            MainManager.Instance.cursorListMan.Clear();
            downloadComp = 0;
            totGarms = 0;
            
            DelItemList(listKeysLocal, currentTexs);
            
            CloseNav?.Invoke();


            FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

            CollectionReference usersRef = db.Collection("users");

            auth = FirebaseAuth.DefaultInstance;
            
            var user = auth.CurrentUser;

            DocumentReference docRef = db.Collection("users").Document(user.UserId);

            Dictionary<string, object> userInfo = new Dictionary<string, object>
            {
                { "gender", MainManager.Instance.gender },
                { "size", MainManager.Instance.size }
            };

            var updateUserInfo = docRef.UpdateAsync(userInfo).ContinueWith((querySnapshotTask) =>
            {
                Debug.Log("Updated User profile");
            });
            
            yield return new WaitUntil(predicate: () => updateUserInfo.IsCompleted );










            ReadyToReceiveFav?.Invoke();
            
            UpdScreen.gameObject.SetActive(true);
        }
        
        
    }


   
    void OnDisable()
    {
        if (coroutineIsRunning == true)
            {
                cancelReq = true;
            }
        
        FirebaseLoad.Queryfinished -= AddGarms;
        if (listKeysLocal.Count > 0)
            DelItemList(listKeysLocal, currentTexs);
        
        
        
        

    }
    IEnumerator StopDownloading(string scene = "NoScene")
    {
        cancelReq = true;
        StopCoroutine(imageRoutine);
        yield return new WaitUntil(() => uwrDel.isDone);
        
        Texture2D scaledImg = new Texture2D(2,2, TextureFormat.ASTC_12x12, false);
        
        
        scaledImg = DownloadHandlerTexture.GetContent(uwrDel);
        DestroyImmediate(scaledImg);
        uwrDel.Dispose();
        if (scene != "NoScene")
            SceneManager.LoadScene(scene);

    }

    void Update()
    {
        if (added == true)
        {
            if (garmentsAddedLocal > 0)
            {
                AddImages(listKeysLocal);
                added = false;
            }
            else 
            {
                added = false;
                StartCoroutine(ResendRepeat());
            }
            
        }


    }

    IEnumerator ResendRepeat()
    {
        yield return new WaitForSeconds(2);
        ReadyToReceiveFav?.Invoke();

    }


    void ResetMan()
    {
        MainManager.Instance.pageMan = 0;
        MainManager.Instance.cursorListMan.Clear();
    }
    

    void scrollrectCallBack(Vector2 value)
    {
        MainManager.Instance.scrollPos = scrollobj.verticalNormalizedPosition;
         
        if (value.normalized.y < -0.01f && waitingforUpdate == true && MainManager.Instance.noMoreGarmentsLocalMan == false)
        {
       
            waitingforUpdate = false;
            scrollobj.StopMovement();
            
            StartCoroutine(tmpScroll());
            
        }
        
        
        
        
    }

    IEnumerator tmpScroll()
    {
        yield return new WaitUntil(() => (imgStillDownloading == false));
        
        ReadyToReceive?.Invoke(MainManager.Instance.cursorListMan[MainManager.Instance.pageMan]);
        if (MainManager.Instance != null)
        {
            MainManager.Instance.pageMan += 1;
        }
        
        
        
    }
    IEnumerator BeginGarmentCall()
    {

        
        
        
        ReadyToReceive?.Invoke(cursorLocal);
        waitingforUpdate = false;
        timesReachedEnd += 1;
        yield return new WaitForEndOfFrame();
    }

    void showGarms(Dictionary<(string,string), object> garmentlist, List<string> sublist )
    {
        foreach (string itemkey in sublist)
        {
            catobjs[itemkey].transform.SetParent(panel.transform, false);
            InitItem(catobjs[itemkey], itemkey, garmentlist);
        }
    }

    void AddImages(List<string> listKeys)
    {
        loadScreenObj.SetActive(true);
        string savePath = Path.Combine(Application.persistentDataPath, "Videos");
          
        
        
        if (!Directory.Exists(Path.GetDirectoryName(savePath)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(savePath));
        }
        int numRoutine = 0;
        imageRoutine = StartCoroutine(ImageAddingProcess(listKeysLocal));

        if (routinesFinished.Count >0)
            routinesFinished.Clear();
        
        foreach (string obj in listKeys)
        {
            
            routinesFinished.Add(false);
            
            numRoutine +=1;
        }
        
        if (firstRun)
        {
            if (MainManager.Instance.adjustmentReq == false)
            {
                scrollobj.verticalNormalizedPosition = 1f;
                
            }
            else
            {
                
                
                scrollobj.verticalNormalizedPosition = MainManager.Instance.scrollPos;
            }
        }
        else
        {
            if(MainManager.Instance.updateReq)
            {
                scrollobj.verticalNormalizedPosition = 1f;
                MainManager.Instance.updateReq = false;
            }
        }   
        /*
        if (MainManager.Instance.pageMan == 0)
            {
                scrollobj.verticalNormalizedPosition = 1f;

            }
        */
        
        
        
        loadScreenObj.SetActive(false);

    }

  
    IEnumerator ImageTexHandler(string obj, string savePath, int numRoutine)
    {
        yield return new WaitForEndOfFrame();
        
        
        Transform it = catobjs[obj].transform;
        
        
        

        Transform mimageBut = it.Find("MainImg");
            
        RawImage imgcmp = mimageBut.GetComponent<RawImage>();
        
    

        
        string url = garmentlistLocal[(obj,"photo")].ToString();


        savePath = Path.Combine(savePath, obj + ".jpg");
        if(Directory.Exists(Path.GetDirectoryName(savePath)))
        {
            
        }
        
        if (!File.Exists(Path.GetDirectoryName(savePath)))
        {
            
            using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url))
            {   
                imgStillDownloading = true;
                
                yield return uwr.SendWebRequest();

                if (uwr.result != UnityWebRequest.Result.Success)
                {
                    imgStillDownloading = false;
                    
                }
                else
                {
                    
                    Texture2D scaledImg = new Texture2D(2,2, TextureFormat.ASTC_12x12, false);
                    
                    
                    
                    scaledImg = DownloadHandlerTexture.GetContent(uwr);
                    imgcmp.texture = scaledImg;
                    
                    
                    currentTexs.Add(scaledImg);

                    
                    

                    
                    imgStillDownloading = false;
                }
            }
        }
        
        
        
        


        
        
        
        routinesFinished[numRoutine] = true;

    }
    IEnumerator ImageAddingProcess(List<string> listKeys)
    {
        
        coroutineIsRunning = true;
        MainManager.Instance.stillDwn = true;
        coroutineFlag = null;
        
        foreach (string obj in listKeys)
        {
            
            yield return null;
            Transform it = catobjs[obj].transform;
            it = it.Find("Button");
            
            

            Transform mimageBut = it.Find("MainImg");
            
            Image imgcmp = mimageBut.GetComponent<Image>();
            imgcmp.preserveAspect = true;


        

            
            string url = garmentlistLocal[(obj,"photo")].ToString();
            url = url + "?sw=300&sh=300&q=100";
            Davinci.get().load(url).into(imgcmp)
            .withEndAction(() =>
            {
                Debug.Log("Download has been completed.");
                downloadComp += 1;
            }).start();

            
        }
        loadScreenObj.SetActive(false);
        waitingforUpdate = true;
        
        coroutineIsRunning = false;
        MainManager.Instance.stillDwn = false;
        firstRun = false;
        MainManager.Instance.adjustmentReq = false;
        
        
        
        
        
       
        
        
    }
    
    Texture2D ScaleTexture(Texture2D source,int targetWidth,int targetHeight) 
    {
        Texture2D result=new Texture2D(targetWidth,targetHeight,source.format,true);
        Color[] rpixels=result.GetPixels(0);
        float incX=((float)1/source.width)*((float)source.width/targetWidth);
        float incY=((float)1/source.height)*((float)source.height/targetHeight);
        for(int px=0; px<rpixels.Length; px++) {
                rpixels[px] = source.GetPixelBilinear(incX*((float)px%targetWidth),
                                  incY*((float)Mathf.Floor(px/targetWidth)));
        }
        result.SetPixels(rpixels,0);
        result.Apply();
        return result;
    }

    
    void LayoutFix(int numitems, GameObject canvaspanel)
    {
        var multlay = rectcalc(numitems);
        
        layoutrect = canvaspanel.GetComponent<RectTransform>();
        
        


        layoutrect.offsetMin = new Vector2(layoutrect.offsetMin.x, -multlay * tot_height);
        
        

        
    }
    void ToggleLoading(bool toggle)
    {
        loadScreenObj.gameObject.SetActive(toggle);
        loadScreenObjTop.gameObject.SetActive(toggle);
    }

    IEnumerator ToProduct(string itemkey, string group_id, string color_code)
    {
        
        ToggleLoading(true);
        FirebaseLoad.Queryfinished -= AddGarms;

        string gender = "m";
        if (MainManager.Instance.gender == "male")
            gender = "m";
        if (MainManager.Instance.gender == "female")
            gender = "w";
        string size = MainManager.Instance.size;

        //To link correctly with the AssetBundle
        string fullPath = $"{gender}_{size}_{group_id}_{color_code}";
        fullPath = fullPath.ToLower(); 
        MainManager.Instance.fullPath = fullPath;
        MainManager.Instance.isFav = true;

        //Optimization for better results
        if (group_id == "322982" || group_id == "313791" )
            MainManager.Instance.qty = 8;
        else
            MainManager.Instance.qty = 4;

        SceneId = itemkey;
        ordertobreak = true;
        
        PlayerPrefs.SetString("selGarment", itemkey);
        Init.Lastscene = "FavBScene";
        
        MainManager.Instance.totGarms = totGarms; 
        yield return new WaitUntil(() => (coroutineIsRunning == false && imgStillDownloading == false && downloadComp == MainManager.Instance.totGarms));
        
        ToggleLoading(false);
        SceneManager.LoadScene("ProductScene");
    }

    void MakeGarms(Dictionary<(string,string), object> garmentlist, List<string> listKeys, GameObject panel, int garmentCount)
    {
        
        
        garmentsAdded += listKeys.Count;
        
        layoutrect = panel.GetComponent<RectTransform>();
        
        
        
        foreach (string itemkey in listKeys)
        {
            MakeItem(itemkey, garmentlist, panel);
            
            
            
            
        }


    }
    // The client states that is ready to receive and on receive the list is populated with garments
    void AddGarms(Dictionary<(string,string), object> garmentlist, int garmsToBeAdded, int dictsize, List<string> listKeys, DocumentSnapshot cursor, bool noMoreGarments, List<string> favlist)
    {
        
        textCat.text = typeOptions[MainManager.Instance.typeIndx];

       
        
        
        
        
        
        
        var items = dictsize;
        
        panel = GameObject.Find("CategoryPanel");
        
        

        noMoreGarmentsLocal = noMoreGarments;
        MainManager.Instance.noMoreGarmentsLocalMan = noMoreGarments;
        
        totGarms += garmsToBeAdded;
        garmentsAddedLocal = garmsToBeAdded;


        
        listKeysLocal = listKeys;
        garmentlistLocal = new Dictionary<(string,string), object>(garmentlist);
        
        MakeGarms(garmentlist, listKeys, panel, 4);
        

        List<string> listUrls = new List<string>();
        
        
        added = true;
        first_run = false;
      
        cursorLocal = cursor;
        

        if (MainManager.Instance.cursorListMan.ElementAtOrDefault(MainManager.Instance.pageMan) == null)
        {
            MainManager.Instance.cursorListMan.Insert(MainManager.Instance.pageMan, cursorLocal);
            
        }
        
        butnxtpressed = false;
        butprevpressed = false;
        scrollobj.enabled = true; 
        
    }

    
    void UpdateGarms()
    {
        string[] updkeys = new string[] { "bgo01", "bgo02" };
        
        for (int i = 0; i < updkeys.Length; i++)
        {
            itemduos++;
            string key = updkeys[i];

            

        }
        
        first_run = false;
    }

    void DelItemList(List<string> listKeys, List<Texture2D> listTexs)
    {
        foreach (KeyValuePair<string, GameObject> objectGar in catobjs)
        {
            
            
            
            Destroy(objectGar.Value);

            
            
        }
        
        foreach (string key in listKeys)
        {
            try
            {
                
                Destroy(catobjs[key]);

                
            }
            catch (Exception e)
            {
                Debug.Log("Catalog Empty");
            }
        }
        try
        {
            catobjs.Clear();
        }
        catch (Exception g)
        {
            Debug.Log("Catalog Empty");
        }

        foreach (Texture2D tex in listTexs)
        {
            try
            {
                Destroy(tex);
            }
            catch (Exception g)
            {
                Debug.Log("Catalog Empty");
            }
        }
        try
        {
            listTexs.Clear();
        }
        catch (Exception g)
        {
            Debug.Log("Catalog Empty");
        }

        

        try
        {
            garmentlistLocal = null;
        }
        catch (Exception g)
        {
            Debug.Log("Catalog Empty");
        }
            
    }

    void InitItem(GameObject item, string itemkey, Dictionary<(string,string), object> garmentlist)
    {
        
        

        
        Transform it = item.transform;
        it = it.Find("Button");
        RectTransform itRect = item.GetComponent<RectTransform>();

        Transform nmprice = it.Find("NamePrice");
        
        foreach (Transform eachChild in nmprice.transform)
        {
            if (eachChild.name == "GarmName")
            {
                
                TMPro.TMP_Text ntext = eachChild.GetComponent<TMPro.TextMeshProUGUI>();
                
                
                ntext.text = garmentlist[(itemkey,"title")].ToString();
            }

            if (eachChild.name == "Price")
            {
                
                TMPro.TMP_Text ntext = eachChild.GetComponent<TMPro.TextMeshProUGUI>();
                
                ntext.text = garmentlist[(itemkey,"price")].ToString() + "€";
            }
            
        }


        
        ToggleLoading(true);
        string group_id = garmentlist[(itemkey,"group_id")].ToString();
        string color_code = garmentlist[(itemkey,"color_code")].ToString();
        FirebaseLoad.Queryfinished -= AddGarms;

        string gend = "m";
        if (MainManager.Instance.gender == "male")
            gend = "m";
        if (MainManager.Instance.gender == "female")
            gend= "w";
        string siz = MainManager.Instance.size;

        string fPath = $"{gend}_{siz}_{group_id}_{color_code}";
        fPath = fPath.ToLower(); 
        Transform aRtext = it.Find("ARTextImg");
        string aRvalue = garmentlist[(itemkey,"tryon")].ToString();
        
        if (aRvalue == "False")
            aRtext.gameObject.SetActive(false);
      
        Transform mimageBut = it.Find("MainImg");
        Image imgcmp = mimageBut.GetComponent<Image>();
        

        
        string url = garmentlist[(itemkey,"photo")].ToString();
   
        imgcmp.sprite = loadingimg;
        
        
        Button clickableBut = it.GetComponent<Button>();

     
 
 
        clickableBut.onClick.AddListener(() =>
        {
            StartCoroutine(ToProduct(itemkey,group_id,color_code));
        
        });

        Transform delTrans = item.transform.Find("ButtonDel");
        Button delBut = delTrans.GetComponent<Button>();
 

        auth = FirebaseAuth.DefaultInstance;
        
        var user = auth.CurrentUser;

        FirebaseFirestore db = FirebaseFirestore.DefaultInstance;

        DocumentReference docRef = db.Collection("favourites").Document(user.UserId).Collection("entries").Document(itemkey);
        delBut.onClick.AddListener(() =>
        {
            item.SetActive(false); 
            MainManager.Instance.totGarms -= 1;
            docRef.DeleteAsync().ContinueWithOnMainThread(task => {
            if (task.IsCompleted) {
                Debug.Log("File deleted successfully.");
            }
            else {
                
            }
            });
        });
        
        

        
        
        
    }

    IEnumerator ImportImage(string url, Button imgcmp)
    {
        
        bool isLoaded = false;
        Davinci.get().load(url).setFadeTime(2).withLoadedAction(()=>
        {
            isLoaded = true;
        }).start();
        
        yield return new WaitUntil(predicate: () => isLoaded == true);
    }
    
    IEnumerator tmp()
    {
        yield return new WaitForSeconds(8);
    }
    void MakeItem(string itemkey, Dictionary<(string,string), object> garmentlist, GameObject panel)
    {
        isLoaded = false;
        
        
        
        if (catobjs.ContainsKey(itemkey))
        {
            catobjs[itemkey] = (GameObject)Instantiate(buttonPrefab);
        }
        else
            catobjs.Add(itemkey, (GameObject)Instantiate(buttonPrefab));
        
        
        
        
        

        catobjs[itemkey].transform.SetParent(panel.transform, false);
        InitItem(catobjs[itemkey], itemkey, garmentlist);
   

    }


    void clearManager()
    {
        MainManager.Instance.cursorListMan.Clear();
        MainManager.Instance.pageMan = 0;
    }


    void filterlist(string filterOpt)
    {
        clearManager();
        cursorLocal = null;
        MainManager.Instance.filtOption = filterOpt;
        
    }


    float rectcalc(int newitems)
    {
        int quo, rem;
        float layoutsize;

        if (newitems <= 4)
            layoutsize = 0;

        else
        {
            quo = Math.DivRem(newitems, 4, out rem);
            if (rem > 2)
                layoutsize = quo;
            else
            {
                if (rem != 0)
                    layoutsize = quo - 0.5F;
                else
                    layoutsize = quo - 1;
            }
        }
        return layoutsize;
    }


  
}
