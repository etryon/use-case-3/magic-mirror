using System.Collections;
//using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Auth;
public class SceneChanger : MonoBehaviour
{
    public FirebaseAuth auth;

    public void ChangeScene(string name)
    {
      
        
        if (name == "LogInScene")
        {
          
            FirebaseManager.auth.SignOut();
            FirebaseManager.user.DeleteAsync();

            MainManager.Instance.pageMan = 0;
            MainManager.Instance.cursorListMan.Clear();
            SceneManager.LoadScene(name); 
        }
        else if (name == "AccountScene")
        {
            MainManager.Instance.pageMan = 0;
            MainManager.Instance.recChoice = 0;
            MainManager.Instance.cursorListMan.Clear();
            SceneManager.LoadScene(name);
        }
        else
            SceneManager.LoadScene(name);
        
      
        
    }
    private IEnumerator Logout()
    {
        auth = FirebaseAuth.DefaultInstance;
        auth.SignOut();
        yield return new WaitForSeconds(3);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
